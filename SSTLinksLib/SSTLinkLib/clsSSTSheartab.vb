﻿Public Class clsSSTSheartab
    ''Public StyID As String
    ''Public GridID As String
    ''Public BmInfo As clsSSTBeam

    Public N_horzBolt As Integer
    Public bSize As bSize
    Public bType As bType
    Public Thk As eThickness
    Public WeldSize As eThickness
    Public Pu As Double
    Public V_Gravity As Double
    ''Public Vu_Link As Double '=2*Mpr/Lh
    Public stBolt As SheartabBolt
    '
    'Public sDCR3_YieldRupture As String

    Public sDCR1_BmWeb As String
    Public sDCR2_ShearPLGeometry As String
    Public sDCR3_ShearPL As String
    Public sDCR4_Bolt As String
    Public sDCR5_Weld As String
    'Public sDCR6_BmWeb_SPBearing As String = ""
    'Public sDCR7_BmWeb_SP_Blockshear As String = ""
    Public sDCR_All As String = ""
    '
    Private Wsec As Wsec
    Private LKInfo As LinkInfo
    Private DCR_Limit As Double = 1.03

    Sub New()
    End Sub

    Sub New(_Wsec As Wsec, _LKInfo As LinkInfo, _stBolt As SheartabBolt, _N_horzBolt As Integer, _boltSize As bSize, _boltType As bType,
            _Thk As eThickness, _WeldSize As eThickness, _Pu As Double, _V_Gravity As Double)
        Wsec = _Wsec
        LKInfo = _LKInfo
        stBolt = _stBolt
        N_horzBolt = _N_horzBolt
        bSize = _boltSize
        bType = _boltType
        Thk = _Thk
        WeldSize = _WeldSize
        Pu = _Pu
        V_Gravity = _V_Gravity
        ''Vu_Link = _Vu_Link
    End Sub

End Class
