﻿Public Class clsSSTColumn
    Inherits clsSEFrame

    Public Pu_col_omega As Double
    Public Vu_col_EL As Double
    Public Vu_col_33_36 As Double 'for shear tab check
    'Public TopStiff As eThickness
    Public stiffener_Required As String
    Public StiffMinThk As Double
    Public StiffMinThk_perLink As Double
    Public Stiff_use As Double
    Public StiffMin_use As Double
    Public DoublerPLThk As Double
    Public DoublerPLThk_Geometry As Double
    '
    Public BmLeft As clsSSTBeam
    Public BmRight As clsSSTBeam
    '
    Public status_SCWB As String
    Public status_Pz As String
    Public status_ColFlg As String
    Public statusPZinitial As String
    Public status_b_t As String
    Public status_h_w As String
    Public status_bcf As String
    ''Public status_TopStiff As String
    ''Public status_BotStiff As String
    Public ProvideStiff As String
    Public ProvideDblPL As String
    '
    Private NoBm As Integer
    Private DCRLimit1_00 As Double = 1.0
    Public Hcc_Top As Double
    '
    Public isBraceAtBmBotFKG As Boolean = True
    Public Mu_Top As Double
    Public Mu_bot As Double
    Public Mu_design As Double
    Public Pu_Design As Double
    Public DCR_bf_2tf As Double
    Public DCR_h_tw As Double
    Public DCR_Axial As Double
    Public DCR_Bmaj As Double
    Public DCR_Bmaj_adj As Double
    Public DCR_Bmin As Double
    Public DCR_Total_Adj As Double
    '
    Public isBottom As Boolean
    '
    Public W_ContPlate As String
    Public W_DoublerPlate As String
    Public W_STPtoWebWeld As String
    Public W_STPtoFlangeWeld As String
    Public W_DPtoWeb_weld As String
    Public W_PlugWeldReq As String
    Public W_PlugWeldSize As String
    Public W_PlugWeldDepth As String
    Public W_DPtoFlange_weld As String
    Sub New()
    End Sub

    Sub New(_StyID$, _ElevID$, _GridID_LeftSide$, in_uName$, _Len_Cal#, _Wsec As Wsec, _iPoint As String, _jPoint As String,
            _TopStiffMinThk As Double, _DoublerPLThk As Double, _BmLeft As clsSSTBeam, _BmRight As clsSSTBeam, _Pu_col_omega As Double, _StyAbove$, _StyTop$, _Angle As Double)
        StyID = _StyID
        ElevID = _ElevID
        GridID_LeftSide = _GridID_LeftSide
        _uName = in_uName
        Len_Cal = _Len_Cal
        Wsec = _Wsec
        Pt_I = _iPoint
        Pt_J = _jPoint
        StiffMinThk = _TopStiffMinThk
        DoublerPLThk = _DoublerPLThk
        BmLeft = _BmLeft
        BmRight = _BmRight
        If Not BmLeft Is Nothing Then NoBm += 1
        If Not BmRight Is Nothing Then NoBm += 1
        Pu_col_omega = _Pu_col_omega
        StyAbove = _StyAbove
        topSty = _StyTop
        ProvideStiff = "NO"
        ProvideDblPL = "NO"
        Angle = _Angle
    End Sub

    Sub New(ByRef curJob As clsMRSJob, uniqueName$, cSize$, iPt$, jPt$, Hcc#, sX#, sY#, grid$, elev$, DblerThk#, stiffThk#, isBracingAtBmBotFLG As Boolean, _Grid_Left$)
        If curJob Is Nothing Then
            Dim Arr() As String
            curJob = New clsMRSJob(Arr)
        End If
        Wsec = curJob.AllWsections.Wsections.Find(Function(x) x.Size.Equals(cSize, StringComparison.CurrentCultureIgnoreCase))
        _uName = uniqueName
        Pt_I = iPt
        Pt_J = jPt
        Len_Cal = Hcc
        sPt_X = sX
        sPt_Y = sY
        ElevID = elev
        StiffMinThk = stiffThk
        ProvideStiff = IIf(StiffMinThk > 0, "YES", "NO")
        DoublerPLThk = DblerThk
        ProvideDblPL = IIf(DoublerPLThk > 0, "YES", "NO")
        isBraceAtBmBotFKG = False
        GridID_LeftSide = _Grid_Left
    End Sub
    'col = New clsSSTColumn(fr.StyID, fr.ElevID, fr.GridID_LeftSide, fr._uName, fr.Len_Cal, fr.Wsec, fr.Pt_I, fr.Pt_J,
    'topStiffThk, DoublerPLThk, BmLeft, BmRight, 0, fr.StyAbove, fr.topSty)

    Public Sub UpdateDesign(ByRef curJob As clsMRSJob)

        ''MsgBox(isBraceAtBmBotFKG & vbLf & "Mu_design=" & Mu_design.ToString & "- Mu_top" & Mu_Top.ToString)
        DCR_Bmaj_adj = If(isBraceAtBmBotFKG = False And Math.Abs(Mu_design - Mu_Top) < 0.1, Wsec.Z33 / Wsec.S33 * DCR_Bmaj, DCR_Bmaj)
        DCR_Total_Adj = DCR_Axial + DCR_Bmaj_adj + DCR_Bmin
        '
        If Mu_bot > 0.5 Then
            Dim B2 As String = curJob.FrType
            Dim Fy_col As Double = g50
            Dim G14 As Double = Math.Sqrt(gES / 1.1 / Fy_col)
            'add bf/2tf check
            DCR_bf_2tf = Wsec.bf_2tf / (If(B2 = "SMF", 0.32 * G14, 0.4 * G14))
            'add h/tw check
            'Ca
            Dim G17 = Math.Abs(Pu_col_omega) / (1.1 * 0.9 * Wsec.Area * Fy_col)
            Dim G18 As String  'Case
            G18 = If((B2 = "SMF" And G17 <= 0.114), "1", If((B2 = "SMF" And G17 > 0.114), "2", If((B2 = "IMF" And G17 <= 0.114), "3", "4")))
            Dim h_tw_limit As Double = If(G18 = "1", 2.57 * G14 * (1 - 1.04 * G17), If(G18 = "3", 3.96 * G14 * (1 - 3.04 * G17), If(G18 = "2", Math.Max(0.88 * G14 * (2.68 - G17), 1.57 * G14), Math.Max(1.29 * G14 * (2.12 - G17), 1.57 * G14))))
            DCR_h_tw = Wsec.h_tw / h_tw_limit
        Else
            DCR_bf_2tf = 0
            DCR_h_tw = 0
        End If
    End Sub

    Public Sub CC_Detail_US(ByRef curJob As clsMRSJob, Optional ByRef isILS As Boolean = False, Optional ColID As Integer = 1)
        '3.1 Current member
        Dim C3 As String = Wsec.Size
        Dim C4 As Double = Pu_col_omega
        Dim C5 As Double = Len_Cal
        Dim C6 As Double = Hcc_Top
        Dim C7 As String = ProvideStiff
        Dim C8 As String = ProvideDblPL
        Dim F2 As String = StyID
        Dim F3 As String = StyAbove
        Dim STPperLink_left, STPperLink_right As Double
        Dim isWeak_Dir As Boolean
        '
        Dim F4, F5, F6, F7, F8, F9, F10 As String
        If Not BmLeft Is Nothing Then
            F4 = BmLeft.Wsec.Size
            F5 = BmLeft._uName
            F6 = BmLeft.Wsec.Depth
            F7 = IIf(BmLeft.IsAsignEndJ_K, BmLeft.LKInfo.Size, "Pinned")
            F8 = BmLeft.NoConn ' IIf(BmLeft.IsAsignEndJ_K, "2", "0")
            F9 = Math.Round(BmLeft.Len_Cal, 3)
            F10 = BmLeft.V_bmGravity
            isWeak_Dir = BmLeft.EndJ_IsWeak
        Else
            F4 = "NA"
            F5 = "NA"
            F6 = "NA"
            F7 = "NA"
            F8 = "0"
            F9 = "0"
            F10 = "0"
        End If
        '
        Dim I2 As String = topSty
        Dim I3 As String = ProvideDblPL

        Dim I4, I5, I6, I7, I8, I9, I10 As String
        If Not BmRight Is Nothing Then
            I4 = BmRight.Wsec.Size
            I5 = BmRight._uName
            I6 = BmRight.Wsec.Depth
            I7 = IIf(BmRight.IsAsignEndI_K, BmRight.LKInfo.Size, "Pinned")
            I8 = BmRight.NoConn ' IIf(BmRight.IsAsignEndJ_K, "2", "0")
            I9 = Math.Round(BmRight.Len_Cal, 3)
            I10 = BmRight.V_bmGravity
            isWeak_Dir = BmRight.EndI_IsWeak
        Else
            I4 = "NA"
            I5 = "NA"
            I6 = "NA"
            I7 = "NA"
            I8 = "0"
            I9 = "0"
            I10 = "0"
        End If
        'Defind R_use
        Dim R_use As Double
        If Not isWeak_Dir Then
            R_use = If(Angle > 45 And Angle < 135, curJob.RY, curJob.R)
        Else
            R_use = If(Angle > 45 And Angle < 135, curJob.R, curJob.RY)
        End If

        '3.2 Link properties
        Dim C13, C14, C15, C16, C17, C18, C19, C20, C21 As Double
        If F7 <> "NA" And F7 <> "Pinned" Then
            C13 = BmLeft.LKInfo.tstem
            C14 = BmLeft.LKInfo.b_yield
            C15 = BmLeft.LKInfo.t_flange
            C16 = BmLeft.LKInfo.S_flange
            C17 = BmLeft.LKInfo.g_flange
            C18 = BmLeft.LKInfo.h_flange
            C19 = BmLeft.LKInfo.a
            C20 = If(R_use <= 3, BmLeft.LKInfo.Py_link, BmLeft.LKInfo.Pr_link) ' C13 * C14 * 1.2 * g65
            C21 = C20 * (BmLeft.Wsec.Depth + C13)
            ''Updated
            If F4 = "NA" Or I4 = "NA" Then
                STPperLink_left = Find_MinThk_perLink(F7)
            Else
                STPperLink_left = Find_MinThk_perLink(F7) + 0.125
            End If
        Else
            C13 = 0
            C14 = 0
            C15 = 0
            C16 = 0
            C17 = 0
            C18 = 0
            C19 = 0
            C20 = 0
            C21 = 0
        End If
        '
        Dim D13, D14, D15, D16, D17, D18, D19, D20, D21 As Double
        If I7 <> "NA" And I7 <> "Pinned" Then
            D13 = BmRight.LKInfo.tstem
            D14 = BmRight.LKInfo.b_yield
            D15 = BmRight.LKInfo.t_flange
            D16 = BmRight.LKInfo.S_flange
            D17 = BmRight.LKInfo.g_flange
            D18 = BmRight.LKInfo.h_flange
            D19 = BmRight.LKInfo.a
            D20 = If(R_use <= 3, BmRight.LKInfo.Py_link, BmRight.LKInfo.Pr_link) ' D13 * D14 * 1.2 * g65
            D21 = D20 * (BmRight.Wsec.Depth + D13)
            ''Updated
            If I4 = "NA" Or F4 = "NA" Then
                STPperLink_right = Find_MinThk_perLink(I7)
            Else
                STPperLink_right = Find_MinThk_perLink(I7) + 0.125
            End If
        Else
            D13 = 0
            D14 = 0
            D15 = 0
            D16 = 0
            D17 = 0
            D18 = 0
            D19 = 0
            D20 = 0
            D21 = 0
        End If
        'Find min STP per link
        StiffMinThk_perLink = Math.Max(STPperLink_left, STPperLink_right)


        '3.3 Check strong column weak link requirements
        Dim D23 As String = If(R_use <= 3, "NOT APPLICABLE FOR R=" & R_use & " CONNECTION", "")
        Dim C24 As String = C3
        Dim C25 As Double = Wsec.Depth
        Dim C26 As Double = Wsec.tf
        Dim C27 As Double = g50
        Dim C28 As Double = Wsec.Area
        Dim C29 As Double = Wsec.Z33
        Dim C30 As String = IIf(F2 = I2, "YES", "NO")
        Dim C32 As Double = C4
        Dim C31 As Double = C29 * (C27 - C32 / C28) * IIf(C30 = "YES", 1, 2)
        Dim C33 As Double = C27 * C28
        Dim C34 As String = IIf(C32 <= (0.3 * C33), "YES", "NO")
        'AISC 360 	Table B4.1b	
        Dim ckVal = Wsec.bf_2tf
        Dim lamda_p = 0.38 * Math.Sqrt(29000 / g50)
        Dim lamda_r = 1 * Math.Sqrt(29000 / g50)
        status_b_t = IIf(ckVal <= lamda_p, "Compact", IIf(ckVal >= lamda_r, "Slender", "Non-Compact"))
        '
        ckVal = Wsec.h_tw
        lamda_p = 3.76 * Math.Sqrt(29000 / g50)
        lamda_r = 5.7 * Math.Sqrt(29000 / g50)
        status_h_w = IIf(ckVal <= lamda_p, "Compact", IIf(ckVal >= lamda_r, "Slender", "Non-Compact"))
        '
        Dim C37 As Double = C21
        Dim C38 As Double = IIf(F9 = "0", 0, CDbl(F9))
        Dim C39 As Double = IIf(F10 = "0", 0, CDbl(F10))
        Dim C40 As Double = IIf(C38 = 0, 0, CDbl(F8) * C37 / C38 + C39)
        Dim C41 As Double = C19
        Dim C42 As Double = C40 * (C25 / 2 + C41)
        Dim C43 As Double = C37 + C42
        Dim C44 As Double = C43 / C31

        Dim D37 As Double = D21
        Dim D38 As Double = IIf(I9 = "0", 0, CDbl(I9) - C25 - 2 * D19)
        Dim D39 As Double = CDbl(I10)
        Dim D40 As Double = IIf(D38 = 0, 0, CDbl(I8) * D37 / D38 + D39)
        Dim D41 As Double = D19
        Dim D42 As Double = D40 * (C25 / 2 + D41)
        Dim D43 As Double = D37 + D42
        Dim D44 As Double = D43 / C31
        Dim C45 As String = IIf(R_use <= 3, 0, IIf(C30 = "YES" And C34 = "YES", "NA", NumberFormat(C44 + D44, 3)))
        Dim C46 As String
        If C45 = "NA" Then
            C46 = "NA"
        Else
            C46 = IIf(CDbl(C45) <= curJob.DCRLimits.SCWB_DCR, "OK", "NG")
        End If
        status_SCWB = IIf(curJob.FrType = "IMF" Or R_use <= 3, NA, C45) ' IIf(C40 <= DCR_limit, "OK", Format(C40, "0.###"))
        '3.5 CHECK UNSTIFFENED COLUMN FLANGE AND WEB:
        Dim C129 As Double = IIf(F8 = "0" Or I8 = "0", 1, 2)
        Dim C131, C132, C133, C134, C135 As String
        If F4 <> "NA" Then
            C131 = BmLeft.Wsec.Depth
            C132 = BmLeft.Wsec.tf
        Else
            C131 = "NA"
            C132 = "NA"
        End If
        If F7 <> "NA" Then
            C133 = Find_Nailer(F7) ' 3.5
            C134 = IIf(F2 = I2, C133, 2 * C25)
            C135 = CDbl(C134) + CDbl(C131)
        Else

            C133 = "NA"
            C134 = "NA"
            C135 = "NA"
        End If
        '
        Dim D131, D132, D133, D134, D135 As String
        If I4 <> "NA" Then
            D131 = BmRight.Wsec.Depth
            D132 = BmRight.Wsec.tf
        Else
            D131 = "NA"
            D132 = "NA"
        End If
        If I7 <> "NA" Then
            D133 = Find_Nailer(I7)
            D134 = IIf(F2 = I2, D133, 2 * C25)
            D135 = CDbl(D134) + CDbl(D131)
        Else
            D133 = "NA"
            D134 = "NA"
            D135 = "NA"
        End If
        '3.4 COLUMN PANEL ZONE CHECK:
        Dim C49 As Double = 0.9
        Dim C50 As Double = g50
        Dim C51 As Double = C25
        Dim C52 As Double = Wsec.tw
        Dim C53 As Double = C28
        Dim C54 As Double = C53 * C50
        Dim C55 As Double = C32
        Dim C56 As Double
        C56 = C49 * IIf(C55 < 0.4 * C54, 0.6 * C50 * C51 * C52, 0.6 * C50 * C51 * C52 * (1.4 - C55 / C54))
        Dim C59 As Double = IIf(F2 = I2, 0, C37 / ((C5 + C6) / 2))
        Dim C60 As Double = C20
        Dim C61 As Double = C60 - C59
        Dim C62 As Double = C61 / C56
        Dim D59 As Double = IIf(F2 = I2, 0, D37 / ((C5 + C6) / 2))
        Dim D60 As Double = D20
        Dim D61 As Double = D60 - D59
        Dim D62 As Double = D61 / C56

        Dim C63 As Double = C62 + D62
        Dim C65 As Double
        If F4 <> "NA" And I4 <> "NA" Then
            C65 = ((D131 - (2 * D132)) + (C131 - (2 * C132))) / 2
        ElseIf F4 <> "NA" Then
            C65 = C131 - (2 * C132)
        ElseIf I4 <> "NA" Then
            C65 = D131 - (2 * D132)
        Else
            C65 = 0
        End If
        Dim C66 As Double = C25 - (2 * C26)
        Dim C67 As Double = C52
        Dim C68 As Double = If(R_use <= 3.0, 0, ((C65 + C66) / 90) / C52)

        Dim C70 As Double = MaxAll(C68, C63)
        Dim C71 As String = IIf(C70 < curJob.DCRLimits.Panel_Zone_DCR, "OK", "NG")
        Dim B74ILS As Double = C60 / C56
        Dim C74ILS As Double = D60 / C56
        Dim B75ILS As Double = B74ILS + C74ILS
        Dim ILStotalPz As Double = MaxAll(B75ILS, C68)
        statusPZinitial = NumberFormat(ILStotalPz, 3)

        '3.4.1 COLUMN WEB DOUBLER PLATE CHECK:
        Dim C73 As String = IIf(C71 = "OK", "NO", "YES")
        Dim C74 As Double = curJob.MaterialInfo.DoublerPL_Fy
        Dim C75 As Double = C56
        Dim C76 As Double = IIf(C56 < (C61 + D61), C61 + D61 - C56, 0)
        Dim C77 As Double = C75 + C76
        'Dim C78 As Double = IIf(C71 = "OK", C81, Ceiling(If(C55 <= (0.4 * C54), (C76 / (C49 * 0.6 * C74 * (C51 - 2 * C26))), (C76 / (C49 * 0.6 * C74 * (C51 - 2 * C26) * (1.4 - (C55 / C54))))), 0.125))
        Dim C79 As Double
        If F4 <> "NA" And I4 <> "NA" Then
            C79 = ((D131 - (2 * D132)) + (C131 - (2 * C132))) / 2
        ElseIf F4 <> "NA" Then
            C79 = C131 - (2 * C132)
        ElseIf I4 <> "NA" Then
            C79 = D131 - (2 * D132)
        Else
            C79 = 0
        End If
        Dim G73 As String = IIf(curJob.WeldingInfo.UsePlug_weld, "YES", "NO") 'UsePlugWeld 
        Dim C80 As Double = C25 - (2 * C26)
        Dim C81 As Double = If(R_use <= 3.0, 0, Ceiling(((C79 + C80) / 90), 1 / 8))

        Dim C78 As Double = IIf(C71 = "OK", C81,
                                Ceiling(IIf(C55 <= (0.4 * C54), (C76 / (C49 * 0.6 * C74 * (C51 - 2 * C26))),
                                            (C76 / (C49 * 0.6 * C74 * (C51 - 2 * C26) * (1.4 - (C55 / C54))))), 1 / 8))
        '
        DoublerPLThk_Geometry = Ceiling(Math.Max(C81, curJob.MaterialInfo.DoublerPL_DefThk), 0.125)
        '
        Dim C82 As Double = MaxAll(C78, C81, 0.25)
        Dim C83 As Double = MaxAll((C79 + C80) / 90 - C52, 0.25, C78)
        Dim C84 As Double = IIf(G73 = "YES", C83, C82)
        Dim C85 As Double = C49 * IIf(C55 < 0.4 * C54, 0.6 * (C50 * C25 * C52 + C74 * C84 * (C25 - 2 * C26)), 0.6 * (C50 * C25 * C52 + C74 * C84 * (C25 - 2 * C26) * (1.4 - C55 / C54)))
        Dim C87 As Double = C61 / C85
        Dim D87 As Double = D61 / C85
        Dim C88 As Double = C87 + D87
        Dim D88 As String = IIf(C88 < curJob.DCRLimits.Panel_Zone_DCR, "OK", "NG")
        Dim C89 As Double = IIf(R_use <= 3.0, 0, ((C79 + C80) / 90) / C52)
        Dim C90 As Double = IIf(R_use <= 3.0, 0, ((C79 + C80) / 90) / (C52 + C84))
        Dim C91 As Double = IIf(C8 = "YES" Or C73 = "YES", MaxAll(C88, C90), MaxAll(C70, C88, C89))
        status_Pz = NumberFormat(C91, 3) ' IIf(C56 <= DCR_limit, "OK", Format(C56, "0.###"))
        '3.4.2 DOUBLER TO COLUMN WEB/CONTINUITY PLATE WELD. -              
        'OPTION 1: DOUBER WITHOUT CINTINUITY PLATES
        Dim C95 As Double = C84
        Dim C96 As Double = C52
        Dim C97 As Double = Ceiling(MinAll(C95, C96), 1 / 16)
        Dim C98 As Double = Ceiling(IIf(C97 < 0.25, 0, IIf(C97 > 0.75, 5 / 16, WfilletMin(C97))) * 16, 1)
        'OPTION 2A: EXTENDED DOUBER PLATE
        Dim C101 As Double = C95
        Dim C102 As Double = 0.625 * C101
        Dim C103 As Double = Ceiling(0.625 * C101 * 16, 1)
        Dim C104 As Double = 2 ' Douber plate side
        'OPTION 2B:DOUBLER PLATES PLACES BETWEEN CONTINUITY PLATES
        Dim C108 As Double = C95
        Dim C109 As Double
        Dim C110 As Double
        C109 = 0.75 * 0.6 * C108 * C74 * 1
        C110 = Ceiling(C109 / 1.39, 1)
        Dim C111 As Double = 1 ' Douber plate side 'update on 10/16/2019
        '3.4.3 DOUBER TO WEB PLUG WELD (IF REQUIRED)
        Dim C114 As String = G73
        Dim C115 As Double = 0.5 * C79
        Dim C116 As Double = 0.5 * C80
        Dim C117 As Double = (C115 + C116) / 90
        Dim D117 As String = IIf(C117 <= C84 And C117 <= C52, "OK", "NG")
        Dim C118 As Double = Ceiling((C84 + 5 / 16) * 16, 1)
        Dim G118 As Double = Ceiling(IIf(C108 <= 0.625, C108, MinAll(0.5 * C108, 0.625)) * 16, 1)
        '3.4.4 DOUBER TO COLUMN FLANGE WELD
        'OPTION 1: FILLET WELD TO DEVELOP DOUBER PLATE SHEAR CAPACITY
        Dim C122 As Double = C95
        Dim C123 As Double
        C123 = 0.6 * C74 * C122 * 1
        Dim C124 As Double = Ceiling(C123 / 1.39, 1) ' Ceiling(C123 / 1.39, 1 / 16)
        'OPTION 2: GROOVE WELD PER AWS D1.8 CLAUSE 4.3
        '3.6 COLUMN WEB LOCAL YIELDING (AISC 360 J10.2):
        Dim C138 As Double = 1
        Dim C139 As Double = C51
        Dim C140 As Double = Wsec.kdes
        Dim C142 As Double = C15
        Dim C143 As Double = C13
        Dim C144 As Double = 2 * C142 + C143
        '
        Dim J139, J140, J141, J142, J143, J144 As Double
        Dim K139, K140, K141, K142, K143, K144 As Double
        Dim C145, C146 As String
        If C134 = "NA" Then
            C145 = "NA"
        Else
            C145 = NumberFormat(IIf(StrtoDbl(C134) <= C139, 0.5, 1), 2)
        End If
        If C135 = "NA" Then
            C146 = "NA"
        Else
            C146 = NumberFormat(IIf(StrtoDbl(C135) <= C139, 0.5, 1), 2)
        End If
        Dim C147 As Double = C138 * C50 * C52 * (5 * C140 + C144)
        Dim C148 As Double = C138 * C50 * C52 * (2.5 * C140 + C144)
        Dim C149, C150 As String
        If C131 = "NA" Or C134 = "NA" Or C135 = "NA" Then
            C149 = "NA"
            C150 = "NA"
        Else
            C149 = NumberFormat(IIf(StrtoDbl(C134) > StrtoDbl(C131), C147, C148), 2)
            C150 = NumberFormat(IIf(StrtoDbl(C135) > StrtoDbl(C131), C147, C148), 2)
        End If
        '
        Dim D142 As Double = D15
        Dim D143 As Double = D13
        Dim D144 As Double = 2 * D142 + D143

        Dim D145, D146 As String
        If D134 = "NA" Then
            D145 = "NA"
        Else
            D145 = NumberFormat(IIf(StrtoDbl(D134) <= C139, 0.5, 1), 2)
        End If
        If D135 = "NA" Then
            D146 = "NA"
        Else
            D146 = NumberFormat(IIf(StrtoDbl(D135) <= C139, 0.5, 1), 2)
        End If
        Dim D147 As Double = C138 * C50 * C52 * (5 * C140 + D144)
        Dim D148 As Double = C138 * C50 * C52 * (2.5 * C140 + D144)
        Dim D149, D150 As String
        If D131 = "NA" Or D134 = "NA" Or D135 = "NA" Then
            D149 = "NA"
            D150 = "NA"
        Else
            D149 = NumberFormat(IIf(StrtoDbl(D134) > StrtoDbl(D131), D147, D148), 2)
            D150 = NumberFormat(IIf(StrtoDbl(D135) > StrtoDbl(D131), D147, D148), 2)
        End If

        '3.7 COLUMN WEB LOCAL CRIPPLING (AISC 360 J10.3):
        Dim C153 As Double = 0.75
        Dim C154 As Double = gES
        Dim C155 As Double = Wsec.tf
        Dim C156 As Double = C52
        Dim C157 As Double = C153 * 0.8 * C156 ^ 2 * (1 + 3 * (MaxAll(C144, D144) / C51) * (C156 / C155) ^ 1.5) * Math.Sqrt(C154 * C50 * C155 / C156)

        Dim C158 As Double = 0.5 * C157
        Dim C159 As Double = C153 * 0.4 * C156 ^ 2 * (1 + (4 * MaxAll(C144, D144) / C139 - 0.2) * (C156 / C155) ^ 1.5) * Math.Sqrt(C154 * C50 * C155 / C156)
        Dim C160 As Double = IIf(MaxAll(C144, D144) / C139 <= 0.2, C158, C159)
        Dim C162, C163 As String
        If C131 = "NA" Or C134 = "NA" Or C135 = "NA" Then
            C162 = "NA"
            C163 = "NA"
        Else
            C162 = NumberFormat(IIf(StrtoDbl(C134) > StrtoDbl(C131), C157, C160), 2)
            C163 = NumberFormat(IIf(StrtoDbl(C135) > StrtoDbl(C131), C157, C160), 2)
        End If
        'Dim C114 = IIf(C82 = "NA", "NA", Math.Round(IIf(C84 > C82, C108, C112), 2))
        'Dim C115 = IIf(C82 = "NA", "NA", Math.Round(IIf(C85 > C82, C108, C112), 2))

        Dim D162, D163 As String
        If D131 = "NA" Or D134 = "NA" Or D135 = "NA" Then
            D162 = "NA"
            D163 = "NA"
        Else
            D162 = NumberFormat(IIf(StrtoDbl(D134) > StrtoDbl(D131), C157, C160), 2)
            D163 = NumberFormat(IIf(StrtoDbl(D135) > StrtoDbl(D131), C157, C160), 2)
        End If
        '3.8 COLUMN WEB COMPRESSION BUCKLIING (AISC 360 J10.5):
        Dim C166 As Double = 0.9
        Dim C167 As Double = C139 - 2 * C140
        Dim C168 As Double = C166 * 24 * C156 ^ 3 * Math.Sqrt(C154 * C50) / C167
        Dim C170, C171, D170, D171 As String
        If C131 = "NA" Or C134 = "NA" Or C135 = "NA" Then
            C170 = "NA"
            C171 = "NA"
        Else
            C170 = NumberFormat(IIf(StrtoDbl(C134) < StrtoDbl(C131), C168 * 0.5, C168), 2)
            C171 = NumberFormat(IIf(StrtoDbl(C135) < StrtoDbl(C131), C168 * 0.5, C168), 2)
        End If
        If D131 = "NA" Or D134 = "NA" Or D135 = "NA" Then
            D170 = "NA"
            D171 = "NA"
        Else
            D170 = NumberFormat(IIf(StrtoDbl(D134) < StrtoDbl(D131), C168 * 0.5, C168), 2)
            D171 = NumberFormat(IIf(StrtoDbl(D135) < StrtoDbl(D131), C168 * 0.5, C168), 2)
        End If
        '3.9 SUMMARY OF LOCAL CAPACITIES:
        Dim C176 As String = C149
        Dim C177 As String = C162
        Dim C178 As String = C170
        Dim G176 As String = C150
        Dim G177 As String = C163
        Dim G178 As String = C171

        Dim G181 As Double = IIf(C129 = 1, Math.Min(StrtoDbl(G176), StrtoDbl(G177)), MinAll(StrtoDbl(G176), StrtoDbl(G177), StrtoDbl(G178)))
        Dim G182 As String = IIf(G181 < C60, "YES", "NO")

        Dim C181 As Double = IIf(C129 = 1, Math.Min(StrtoDbl(C176), StrtoDbl(C177)), MinAll(StrtoDbl(C176), StrtoDbl(C177), StrtoDbl(C178)))
        Dim C182 As String = IIf(C181 < C60, "YES", "NO")
        Dim C183 As String = C7
        Dim G183 As String = C183
        Dim G184 As String = IIf(C182 = "YES" Or G182 = "YES", "YES", "NO")
        Dim C187 As String = D149
        Dim C188 As String = D162
        Dim C189 As String = D170

        Dim G187 As String = D150
        Dim G188 As String = D163
        Dim G189 As String = D171
        Dim G192 As Double = IIf(C129 = 1, Math.Min(StrtoDbl(G187), StrtoDbl(G188)), MinAll(StrtoDbl(G187), StrtoDbl(G188), StrtoDbl(G189)))
        Dim G193 As String = IIf(G192 < D60, "YES", "NO")

        Dim C192 As Double = IIf(C129 = 1, Math.Min(StrtoDbl(C187), StrtoDbl(C188)), MinAll(StrtoDbl(C187), StrtoDbl(C188), StrtoDbl(C189)))
        Dim C193 As String = IIf(C192 < D60, "YES", "NO")
        Dim C194 As String = C183
        Dim G194 As String = G183
        Dim G195 As String = IIf(C193 = "YES" Or G193 = "YES", "YES", "NO")

        ''If G184 = "YES" Or G195 = "YES" Then
        ''    stiffener_Required = "YES"
        ''Else
        ''    stiffener_Required = "NO"
        ''End If

        '3.10 WEB SIDESWAY BUCKLING (AISC 360-J10.4):
        Dim C198 As Double = 0.85
        Dim C199 As Double = C5
        Dim C200 As Double = Wsec.bf
        Dim C201 As Double = Wsec.S33
        Dim C202 As String
        If C131 = "NA" Then
            C202 = "NA"
        Else
            C202 = NumberFormat((C199 - StrtoDbl(C131) / 2), 2)
        End If
        'Dim C157 = IIf(C82 = "NA", "NA", Math.Round((C154 - C82 / 2), 2))
        Dim C203 As Double = C201 * C27
        Dim C204 As Double = C37
        Dim C205 As Double = IIf(C204 < C203, 960000, 480000)
        Dim C206, C207 As String
        If C131 = "NA" Then
            C206 = "NA"
            C207 = "NA"
        Else
            C206 = NumberFormat((C167 / C156) / (StrtoDbl(C202) / C200), 4)
            C207 = NumberFormat(((C198 * C205 * C52 ^ 3 * C155) / (C167 ^ 2)) * (0.4 * (StrtoDbl(C206)) ^ 3), 2)
        End If
        Dim C208 As String = NA
        Dim C209 As String = IIf(StrtoDbl(C207) < C60 And C208 = "Applicable", "YES", "NO")

        Dim D198 As Double = 0.85
        Dim D199 As Double = C5
        Dim D200 As Double = Wsec.bf
        Dim D201 As Double = Wsec.S33
        Dim D202 As String
        If D131 = "NA" Then
            D202 = "NA"
        Else
            D202 = NumberFormat((D199 - StrtoDbl(D131) / 2), 2)
        End If

        'Dim D157 = IIf(D82 = "NA", "NA", Math.Round((D154 - D82 / 2), 2))
        Dim D203 As Double = D201 * C27
        Dim D204 As Double = D37
        Dim D205 As Double = IIf(D204 < D203, 960000, 480000)
        Dim D206, D207 As String
        If D131 = "NA" Then
            D206 = "NA"
            D207 = "NA"
        Else
            D206 = NumberFormat((C167 / C156) / (StrtoDbl(D202) / D200), 4)
            D207 = NumberFormat(((D198 * D205 * C52 ^ 3 * C155) / (C167 ^ 2)) * (0.4 * StrtoDbl(D206) ^ 3), 2)
        End If

        Dim D208 As String = NA
        Dim D209 As String = IIf(StrtoDbl(D207) < D60 And D208 = "Applicable", "YES", "NO")

        '3.11 COLUMN STIFFENER DESIGN:
        Dim C214 As Double = IIf(C60 - C181 < 0, 0, C60 - C181)
        Dim C215 As Double = IIf(C60 - G181 < 0, 0, C60 - G181)
        Dim C216 As Double = Math.Max(C214, C215) * C129
        Dim D214 As Double = IIf(D60 - C192 < 0, 0, D60 - C192)
        Dim D215 As Double = IIf(D60 - G192 < 0, 0, D60 - G192)
        Dim D216 As Double = Math.Max(D214, D215) * C129

        '3.12 CONTINUITY PLATE REQUIREMENTS BASED ON GEOMETRY AND TENSION YIELDING:
        Dim C220 As Double = 0.9
        Dim C221 As Double = curJob.MaterialInfo.StiffPL_Fy
        Dim C222 As Double = Wsec.kdet
        Dim C223 As Double = Wsec.k1
        Dim C224 As Double = Wsec.twdet_2
        Dim C225 As Double = Wsec.tfdet

        Dim C227 As Double = C216 / (C220 * C221)
        Dim C228 As Double = C222 - C225 + 1.5
        Dim C229 As Double = Ceiling(C228, 1 / 16)
        Dim C230 As Double = C223 - C224
        Dim C231 As Double = Ceiling(C230, 1 / 16)

        Dim C233 As Double = C200 / 3 - C156 / 2
        Dim C234 As Double = (C200 - C156) / 2
        Dim C235 As Double = Floor(C234, 0.125)
        Dim C236 As String = C7
        Dim C237 As Double = (C227 / (C235 - C231)) / 2 / C129
        Dim C238 As Double = MaxAll(C13 / 2, Ceiling(C235 / 16, 0.125), curJob.MaterialInfo.StiffPL_DefThk, STPperLink_left) 'Updated
        Dim C239 As Double = C13
        Dim C240 As Double = IIf(C13 = 0, 0, IIf(G184 = "YES", MaxAll(curJob.MaterialInfo.StiffPL_DefThk, Math.Min(Ceiling(C237, 0.125), C239), StiffMinThk_perLink, C238), IIf(G184 = "No" And C236 = "Yes", MaxAll(C238, StiffMinThk_perLink), MaxAll(C238, StiffMinThk_perLink)))) 'Updated

        Dim C242 As Double = 0.5 * C25 - C155
        Dim C243 As Double = C25 - 2 * C155
        Dim C244 As Double = IIf(C129 = 1 And curJob.MaterialInfo.StiffPL_1SidedConn = StiffStyle.Partial_Depth, C242, C243)
        Dim C245 As Double = Floor(C244, 0.0625)
        '
        Dim D227 As Double = D216 / (C220 * C221)
        Dim D228 As Double = C222 - C225 + 1.5
        Dim D229 As Double = Ceiling(D228,  1 / 16)
        Dim D230 As Double = C223 - C224
        Dim D231 As Double = Ceiling(D230, 1 / 16)

        Dim D233 As Double = C200 / 3 - C156 / 2
        Dim D234 As Double = (C200 - C156) / 2
        Dim D235 As Double = Floor(D234, 0.125)
        Dim D236 As String = C7
        Dim D237 As Double = (D227 / (D235 - D231)) / 2 / C129
        Dim D238 As Double = MaxAll(D13 / 2, Ceiling(D235 / 16, 0.125), curJob.MaterialInfo.StiffPL_DefThk, STPperLink_right) 'Updated
        Dim D239 As Double = D13
        Dim D240 As Double = IIf(D13 = 0, 0, IIf(G195 = "YES", MaxAll(curJob.MaterialInfo.StiffPL_DefThk, Math.Min(Ceiling(D237, 0.125), D239), StiffMinThk_perLink, D238), IIf(G195 = "No" And D236 = "Yes", MaxAll(D238, StiffMinThk_perLink), MaxAll(D238, StiffMinThk_perLink))))
        Stiff_use = MaxAll(C240, D240)
        StiffMin_use = MaxAll(C238, D238)
        '''Min stiff thickness per Abhishek email on Thu 4/11/2019 12:45 AM
        ''StiffMinThk_Geometry = Ceiling(MaxAll(C238, D238), 0.125)
        '
        '3.13 FULL DEPTH STIFFENER PLATE FOR 2-SIDED MOMENT CONNECTIONS ONLY:
        Dim F247 As String = IIf(C129 = 1, "THIS SECTION NOT APPLICABLE TO 1-SIDED SMF", "")
        Dim C249 As Double = 0.9
        Dim C250 As Double = 0.75
        Dim C251 As Double = 2 * C235 * C240 + 12 * C156 * C156
        Dim C252 As Double = ((12 * C156) * (C156) ^ 3) / 12 + 2 * ((C240 * C235 ^ 3) / 12 + C235 * C240 * (C235 / 2 + C156 / 2) ^ 2)
        Dim C253 As Double = Math.Sqrt(C252 / C251)
        Dim C254 As Double = C250 * C245 / C253
        Dim C255 As Double = Math.PI ^ 2 * gES / C254 ^ 2
        Dim C256 As Double = IIf(C254 <= 4.71 * Math.Sqrt(gES / C221), 0.658 ^ (C221 / C255) * C221, 0.877 * C255)
        Dim C257 As Double = IIf(C254 > 25, C256 * C251, C251 * C221)
        Dim C258 As Double = C257 * C249
        Dim C259 As Double = IIf(C129 = 1, 0, C216 / C258)

        Dim D249 As Double = 0.9
        Dim D250 As Double = 0.75
        Dim D251 As Double = 2 * D235 * D240 + 12 * C156 * C156
        Dim D252 As Double = ((12 * C156) * (C156) ^ 3) / 12 + 2 * ((D240 * D235 ^ 3) / 12 + D235 * D240 * (D235 / 2 + C156 / 2) ^ 2)
        Dim D253 As Double = Math.Sqrt(D252 / D251)
        Dim D254 As Double = D250 * C245 / D253
        Dim D255 As Double = Math.PI ^ 2 * gES / D254 ^ 2
        Dim D256 As Double = IIf(D254 <= 4.71 * Math.Sqrt(gES / C221), 0.658 ^ (C221 / D255) * C221, 0.877 * D255)
        Dim D257 As Double = IIf(D254 > 25, D256 * D251, D251 * C221)
        Dim D258 As Double = D257 * D249
        Dim D259 As Double = IIf(C129 = 1, 0, D216 / D258)

        Dim E259 As String = IIf(C259 < DCRLimit1_00 And D259 < DCRLimit1_00, "OK", "NG")

        '3.14 STIFFENER PLATE DOUBLE SIDE FILLET WELD TO COLUMN FLANGE:
        Dim C262 As Double = 0.75
        Dim C263 As Double = 70
        Dim C264 As Double = Stiff_use
        Dim C265 As Double = WfilletMin(C264)

        Dim C267 As Double = IIf(C13 = 0, 0, (0.5 * C216 / C129) / (C262 * 1.5 * 0.6 * C263 * (C235 - C231 - 0.5) * Math.Sqrt(2)))
        Dim C268 As Double = IIf(C13 = 0, 0, Math.Max(C265, Ceiling(C267, 1 / 16)))
        Dim C269 As Double
        If C268 = 0 Then
            C269 = 0
        Else
            C269 = C267 / C268
        End If
        '
        Dim D267 As Double = IIf(D13 = 0, 0, (0.5 * D216 / C129) / (C262 * 1.5 * 0.6 * C263 * (D235 - D231 - 0.5) * Math.Sqrt(2)))
        Dim D268 As Double = IIf(D13 = 0, 0, Math.Max(C265, Ceiling(D267, 1 / 16)))
        Dim D269 As Double
        If D268 = 0 Then
            D269 = 0
        Else
            D269 = D267 / D268
        End If

        '3.15 STIFFENER PLATE DOUBLE SIDE FILLET WELD TO COLUMN WEB:
        Dim C272 As Double = C265
        Dim C273 As Double = 0.5 * C216 * C129 / (C262 * 0.6 * C263 * (C245 - C229 - 0.5) * Math.Sqrt(2))
        Dim C274 As Double = IIf(C13 = 0, 0, Math.Max(C272, Ceiling(C273, 1 / 16)))
        Dim C275 As Double
        If C274 = 0 Then
            C275 = 0
        Else
            C275 = C273 / C274
        End If
        Dim D272 As Double = C272
        Dim D273 As Double = 0.5 * D216 * C129 / (C262 * 0.6 * C263 * (C245 - D229 - 0.5) * Math.Sqrt(2))
        Dim D274 As Double = IIf(D13 = 0, 0, Math.Max(D272, Ceiling(D273, 1 / 16)))
        Dim D275 As Double = D273 / D274
        If D274 = 0 Then
            D275 = 0
        Else
            D275 = D273 / D274
        End If
        '3.16 CHECK MINIMUM COLUMN FLANGE THICKNESS:
        'left link
        Dim dcr_cf_leftLink, dcr_cf_rightlink As Double
        If Not (F7 = "NA" Or F7 = "Pinned") Then
            dcr_cf_leftLink = BmLeft.LKInfo.bcf_min / Wsec.bf
            If dcr_cf_leftLink > 1 Then
                Dim L_colEdge = (Wsec.bf - BmLeft.LKInfo.S_flange) / 2
                dcr_cf_leftLink = BmLeft.LKInfo.boltDia_Flange / L_colEdge
            End If
        End If
        'right link
        If Not (I7 = "NA" Or I7 = "Pinned") Then
            dcr_cf_leftLink = BmRight.LKInfo.bcf_min / Wsec.bf
            If dcr_cf_leftLink > 1 Then
                Dim L_colEdge = (Wsec.bf - BmRight.LKInfo.S_flange) / 2
                dcr_cf_rightlink = BmRight.LKInfo.boltDia_Flange / L_colEdge
            End If
        End If
        '
        status_bcf = If(Math.Max(dcr_cf_leftLink, dcr_cf_rightlink) > 1, "NG", "OK")

        '3.17 CONNECTION AWAY FROM COLUMN ENDS (SST Step 18 Table 1.1):
        Dim C282 As Double = 0.9
        Dim C283 As Double = C155
        Dim C284, C285, C286, C287, C288 As String
        If F7 = "NA" Or F7 = "Pinned" Then
            C284 = "NA"
            C285 = "NA"
            C286 = "NA"
            C287 = "NA"
            C288 = "NA"
        Else
            C284 = C17
            C285 = C16
            C286 = C18
            C287 = NumberFormat(0.5 * Math.Sqrt(C200 * StrtoDbl(C285)), 3)
            C288 = (StrtoDbl(C285) - C264) / 2
        End If

        Dim C289 As String = C288
        Dim C290 As String = IIf(StrtoDbl(C289) > StrtoDbl(C287), C287, C289)
        Dim C291, C292 As String
        If C131 = "NA" Or C13 = 0 Or C285 = "NA" Then
            C291 = "NA"
            C292 = "NA"
        Else
            C291 = StrtoDbl(C131) + C13 - (StrtoDbl(C285) / 2)
            C292 = StrtoDbl(C131) + C13 + (StrtoDbl(C285) / 2)
        End If

        Dim C294 As Double = IIf(C287 = "NA", 0, (C200 / 2) * (StrtoDbl(C291) * (1 / StrtoDbl(C287)) + StrtoDbl(C292) * (1 / StrtoDbl(C287))) + (2 / StrtoDbl(C285)) *
                                 (StrtoDbl(C291) * (StrtoDbl(C287) + 3 * StrtoDbl(C284) / 4) + StrtoDbl(C292) * (StrtoDbl(C287) + StrtoDbl(C284) / 4) +
                                 StrtoDbl(C284) ^ 2 / 2) + StrtoDbl(C285) / 2)
        Dim C295 As Double = IIf(C294 = 0, 0, Math.Sqrt(1.1 * C37 / (C282 * C27 * C294)))

        Dim C297 As Double = IIf(C287 = "NA", 0, (C200 / 2) * (StrtoDbl(C291) * (1 / StrtoDbl(C287) + 1 / StrtoDbl(C290)) + StrtoDbl(C292) *
                                 (1 / StrtoDbl(C287) + 1 / StrtoDbl(C288))) + (2 / StrtoDbl(C285)) * (StrtoDbl(C291) * (StrtoDbl(C287) + StrtoDbl(C290)) +
                                 StrtoDbl(C292) * (StrtoDbl(C287) + StrtoDbl(C288))))
        Dim C298 As Double = IIf(C297 = 0, 0, Math.Sqrt(1.1 * C37 / (C282 * C27 * C297)))

        Dim C300 As String = C7
        Dim C301 As Double = IIf(C300 = "YES", C298, C295)
        Dim C302 As Double = C301 / C283
        '
        Dim D282 As Double = 0.9
        Dim D283 As Double = C155
        Dim D284, D285, D286, D287, D288 As String
        If I7 = "NA" Or I7 = "Pinned" Then
            D284 = "NA"
            D285 = "NA"
            D286 = "NA"
            D287 = "NA"
            D288 = "NA"
        Else
            D284 = D17
            D285 = D16
            D286 = D18
            D287 = NumberFormat(0.5 * Math.Sqrt(C200 * StrtoDbl(D285)), 3)
            D288 = (D285 - C264) / 2
        End If

        Dim D289 As String = D288
        Dim D290 As String = IIf(D289 > D287, D287, D289)
        Dim D291, D292 As String
        If D131 = "NA" Or D13 = 0 Or D285 = "NA" Then
            D291 = "NA"
            D292 = "NA"
        Else
            D291 = StrtoDbl(D131) + D13 - (StrtoDbl(D285) / 2)
            D292 = StrtoDbl(D131) + D13 + (StrtoDbl(D285) / 2)
        End If

        Dim D294 As Double = IIf(D287 = "NA", 0, (C200 / 2) * (StrtoDbl(D291) * (1 / StrtoDbl(D287)) + StrtoDbl(D292) * (1 / StrtoDbl(D287))) + (2 / StrtoDbl(D285)) *
                                 (StrtoDbl(D291) * (StrtoDbl(D287) + 3 * StrtoDbl(D284) / 4) + StrtoDbl(D292) * (StrtoDbl(D287) +
                                 StrtoDbl(D284) / 4) + StrtoDbl(D284) ^ 2 / 2) + StrtoDbl(D285) / 2)
        Dim D295 As Double = IIf(D294 = 0, 0, Math.Sqrt(1.1 * D37 / (D282 * C27 * D294)))

        Dim D297 As Double = IIf(D287 = "NA", 0, (C200 / 2) * (StrtoDbl(D291) * (1 / StrtoDbl(D287) + 1 / StrtoDbl(D290)) + StrtoDbl(D292) * (1 / StrtoDbl(D287) +
                                 1 / StrtoDbl(D288))) + (2 / StrtoDbl(D285)) * (StrtoDbl(D291) * (StrtoDbl(D287) + StrtoDbl(D290)) + StrtoDbl(D292) *
                                 (StrtoDbl(D287) + StrtoDbl(D288))))
        Dim D298 As Double = IIf(D297 = 0, 0, Math.Sqrt(1.1 * D37 / (D282 * C27 * D297)))

        Dim D300 As String = C7
        Dim D301 As Double = IIf(D300 = "YES", D298, D295)
        Dim D302 As Double = D301 / D283
        '
        Dim E302 As String = IIf(C302 < curJob.DCRLimits.Column_Flange_DCR And D302 < curJob.DCRLimits.Column_Flange_DCR, "OK", "NG")

        '3.18 CONNECTION AT STIFFENED COLUMN END (SST STEP 18, TABLE 1.2, CASE 1):
        Dim C307 As Double = 1.25
        Dim C308 As String
        If C285 = "NA" Then
            C308 = "NA"
        Else
            C308 = (StrtoDbl(C285) - C264) / 2
        End If
        Dim C309 As String = C308
        Dim C310 As Double = IIf(C287 = "NA", 0, (C200 / 2) * (StrtoDbl(C291) * (1 / StrtoDbl(C308) + 1 / StrtoDbl(C287)) + StrtoDbl(C292) *
                                 (1 / StrtoDbl(C309) + 1 / (2 * StrtoDbl(C287)))) + (2 / StrtoDbl(C285)) * (StrtoDbl(C291) * (StrtoDbl(C308) + StrtoDbl(C287)) +
                                 StrtoDbl(C292) * (C307 + StrtoDbl(C309))))
        Dim C311 As Double = IIf(C310 = 0, 0, Math.Sqrt(1.1 * C37 / (C282 * C27 * C310)))
        Dim C312 As Double = C311 / C283
        '
        Dim D307 As Double = 1.25
        Dim D308 As String
        If D285 = "NA" Then
            D308 = "NA"
        Else
            D308 = (StrtoDbl(D285) - C264) / 2
        End If

        Dim D309 As String = D308
        Dim D310 As Double = IIf(D287 = "NA", 0, (C200 / 2) * (StrtoDbl(D291) * (1 / StrtoDbl(D308) + 1 / StrtoDbl(D287)) + StrtoDbl(D292) *
                                 (1 / StrtoDbl(D309) + 1 / (2 * StrtoDbl(D287)))) + (2 / StrtoDbl(D285)) * (StrtoDbl(D291) * (StrtoDbl(D308) + StrtoDbl(D287)) + StrtoDbl(D292) *
                                 (D307 + StrtoDbl(D309))))
        Dim D311 As Double = IIf(D310 = 0, 0, Math.Sqrt(1.1 * D37 / (D282 * C27 * D310)))
        Dim D312 As Double = D311 / D283

        Dim E312 As String = IIf(C312 < curJob.DCRLimits.Column_Flange_DCR And D312 < curJob.DCRLimits.Column_Flange_DCR, "OK", "NG")
        Dim ColFlangeCheck As String

        ColFlangeCheck = IIf(C302 > curJob.DCRLimits.Column_Flange_DCR Or C312 > curJob.DCRLimits.Column_Flange_DCR, "NG", "OK")
        '
        'status_ColFlg = NumberFormat(IIf(F2 = I2, MaxAll(C302, C312, D302, D312), MaxAll(C302, D302)), 3) 'IIf(ColFlangeCheck = "OK", "OK", "NG")
        'check stiff req
        ' Dim I278 As String = IIf(J141 = "YES" Or J152 = "YES", "YES", "NO")
        '3.16 STABILITY BRACING
        Dim F318 As String = F7
        Dim G318 As String = I7
        Dim F319 As String = C14
        Dim G319 As String = D14
        Dim F320 As String = C13
        Dim G320 As String = D13
        Dim F323 As String = F319 * F320 * 50 * 1.1
        Dim G323 As String = G319 * G320 * 50 * 1.1
        Dim F324 As String = F323 * 0.02
        Dim G324 As String = G323 * 0.02
        Dim F325 As String = F324 * 0.7
        Dim G325 As String = G324 * 0.7
        '3.30 DESIGN SUMMARY
        Dim B317 As String = C45
        Dim B318 As Double = C70
        Dim B319 As Double = C88
        Dim B320 As Double = C259
        Dim B321 As Double = C269
        Dim B322 As Double = C275
        Dim B323 As Double = C302
        Dim B324 As Double = C312
        '
        Dim C319 As String = C73
        Dim C320 As Double = D259
        Dim C321 As Double = D269
        Dim C322 As Double = D275
        Dim C323 As Double = D302
        Dim C324 As Double = D312
        '
        Dim D317 As String = B317
        Dim D319 As Double = IIf(C319 = "YES", B319, B318)
        Dim D318 As String = IIf(D117 = "OK" And D319 <= curJob.DCRLimits.SCWB_DCR, "OK", "NG")
        Dim D320 As String = IIf(IsNumeric(B320) And B320 > 0, NumberFormat(B320, 3), IIf(IsNumeric(C320) And C320 > 0, NumberFormat(C320, 3), "NA"))
        Dim D321 As String = IIf(IsNumeric(B321) And B321 > 0, NumberFormat(B321, 3), IIf(IsNumeric(C321) And C321 > 0, NumberFormat(C321, 3), "NA"))
        Dim D322 As String = IIf(IsNumeric(B322) And B322 > 0, NumberFormat(B322, 3), IIf(IsNumeric(C322) And C322 > 0, NumberFormat(C322, 3), "NA"))
        Dim D323 As String = IIf(IsNumeric(B323) And B323 > 0, NumberFormat(B323, 3), IIf(IsNumeric(C323) And C323 > 0, NumberFormat(C323, 3), "NA"))
        Dim D324 As String = IIf(IsNumeric(B324) And B324 > 0, NumberFormat(B324, 3), IIf(IsNumeric(C324) And C324 > 0, NumberFormat(C324, 3), "NA"))
        Dim D325 As String = IIf(F2 = I2, MaxAll(StrtoDbl(D323), StrtoDbl(D324)), StrtoDbl(D323))
        status_ColFlg = NumberFormat(D325, 3)
        '
        Dim H317 As String = C73
        Dim H318 As Double = C84
        Dim H319 As String = IIf(G184 <> "YES" And G184 <> "NO", "NO", G184)
        Dim H320 As Double = IIf(C13 = 0, 0, C240)
        Dim H321 As Double = IIf(C13 = 0, 0, C268)
        Dim H322 As Double = IIf(C13 = 0, 0, C274)
        '
        Dim I319 As String = IIf(G195 = "NA", "NO", G195)
        Dim I320 As Double = IIf(D13 = 0, 0, D240)
        Dim I321 As Double = IIf(D13 = 0, 0, D268)
        Dim I322 As Double = IIf(D13 = 0, 0, D274)
        '
        Dim J317 As String = G73
        Dim J318 As Double = IIf(H317 = "NO", 0, H318)
        Dim J319 As String = IIf(H319 = "YES" Or I319 = "YES", "YES", "NO")

        Dim J320 As Double = IIf(C7 = "NO", 0, MaxAll(H320, I320, curJob.MaterialInfo.StiffPL_DefThk, StiffMinThk_perLink))

        Dim J320_tmp As Double = MaxAll(H320, I320, curJob.MaterialInfo.StiffPL_DefThk, StiffMinThk_perLink)
        Dim J321 As Double = MaxAll(H321, I321) ' IIf(J319 = "NO", 0, MaxAll(H321, I321))
        Dim J322 As Double = MaxAll(H322, I322) ' IIf(J319 = "NO", 0, MaxAll(H322, I322))
        '
        stiffener_Required = J319
        ' ''
        StiffMinThk = J320
        DoublerPLThk = J318
        '
        W_ContPlate = Dec2Frac(J320)
        W_DoublerPlate = Dec2Frac(J318)
        'stiff
        W_STPtoWebWeld = Dec2Frac(J322)
        W_STPtoFlangeWeld = Dec2Frac(J321)
        '
        If curJob.WeldingInfo.DoublerPL2ColWeb = WeldingOption.Option_1 Then
            W_DPtoWeb_weld = IIf(H317 = "YES", C98 & "/16", NA)
        ElseIf curJob.WeldingInfo.DoublerPL2ColWeb = WeldingOption.Option_2A Then
            W_DPtoWeb_weld = IIf(H317 = "YES", C103 & "/16", NA)
        ElseIf curJob.WeldingInfo.DoublerPL2ColWeb = WeldingOption.Option_2B Then
            W_DPtoWeb_weld = IIf(H317 = "YES", C110 & "/16", NA)
        End If
        '
        W_PlugWeldReq = J317
        If W_PlugWeldReq = "YES" Then
            W_PlugWeldSize = C118 & "/16" 'Dec2Frac(C118)
            W_PlugWeldDepth = G118 & "/16" '= Dec2Frac(G118)
        Else
            W_PlugWeldSize = NA
            W_PlugWeldDepth = NA
        End If
        If curJob.WeldingInfo.DoublerPL2ColFLG = WeldingOption.Option_2 Then
            W_DPtoFlange_weld = Dec2Frac(IIf(H317 = "YES", C124 / 16, 0))
        Else
            W_DPtoFlange_weld = IIf(H317 = "YES", "CJP Weld", NA)
        End If
        '
        Dim cnt As Long, prefix As String
        If Not curJob.stlData Is Nothing And isILS Then
            cnt = 0
            '1.11 PRELIMINARY COLUMN PANEL ZONE CHECK
            prefix = "S" & ColID & "S111_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), _uName)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C49, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C50, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C51, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C52, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C53, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C54, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C55, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C56, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), 0)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C60, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C60, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B74ILS, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B75ILS, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), If(C68 > 0, NumberFormat(C68, 3), NA)) '=0, & show NA if R=3 
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(MaxAll(C68, B75ILS), 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(MaxAll(C68, B75ILS) <= curJob.DCRLimits.Panel_Zone_DCR, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C49, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C50, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C51, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C52, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C53, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C54, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C55, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C56, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), 0)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D60, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D60, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C74ILS, 3))

            prefix = "SUM_"
            cnt = 2 + ColID
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), statusPZinitial)
            cnt = 7 + ColID
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Panel_Zone_DCR, 2))
            cnt = 12 + ColID
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(ILStotalPz < curJob.DCRLimits.Panel_Zone_DCR, "OK", "NG"))
        End If
        If Not curJob.stlData Is Nothing And isILS = False Then
            curJob.stlData.Clear()
            cnt = 0
            '3.1 CURRENT MEMBER:
            prefix = "S31_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), _uName)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C3)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C4, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C5, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C6, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C7)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C8)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F2)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F3)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F4)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F5)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F6)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F7)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F8)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F9)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F10)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I2)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I3)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I4)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I5)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I6)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I7)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I8)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I9)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I10)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), R_use)
            '3.2 LINK PROPERTIES:
            cnt = 0 : prefix = "S32_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C13, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C14, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C15, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C16, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C17, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C18, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C19, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C20, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C21, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D13, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D14, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D15, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D16, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D17, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D18, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D19, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D20, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D21, 2))
            '3.3 CHECK STRONG COLUMN WEAK LINK REQUIREMENTS:
            cnt = -1 : prefix = "S33_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D23)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C24)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C25, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C26, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C27)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C28, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C29, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C30)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C31, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C32, 2))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C33, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C34)

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C37, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C38, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C39, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C40, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C41, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C42, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C43, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C44, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C45)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C46)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D37, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D38, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D39, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D40, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D41, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D42, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D43, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D44, 3))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Wsec.bf_2tf, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Wsec.h_tw, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), status_b_t)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), status_h_w)
            '3.4 COLUMN PANEL ZONE CHECK:
            cnt = 0 : prefix = "S34_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C49, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C50, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C51, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C52, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C53, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C54, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C55, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C56, 2))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C59, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C60, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C61, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C62, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C63, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C65, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C66, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C67, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C68, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C70, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C71)

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D59, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D60, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D61, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D62, 3))

            '3.4.1 COLUMN WEB DOUBLER PLATE CHECK:
            cnt = 0 : prefix = "S341_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C73)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C74, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C75, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C76, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C77, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C78, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C79, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C80, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C81, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C82, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C83, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C84, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C85, 2))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C87, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C88, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C89, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C90, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C91, 3))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D87, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D88)
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G73)
            '3.4.2 DOUBER TO COLUMN WEB/ CONTINUITY PLATE WELD
            cnt = 0 : prefix = "S342_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C95, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C96, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C97, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C98 & "/16")
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C101, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C102, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C103 & "/16")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C104, 0))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C108, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C109, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C110 & "/16")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C111, 0))

            '3.4.3 DOUBLER TO WEB PLUG WELD (IF REQUIRED)
            cnt = 0 : prefix = "S343_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C114)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C115, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C116, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C117, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C118 & "/16")
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D117)
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G118 & "/16")
            '3.4.4 DOUBLER TO COLUMN FLANGE WELD
            cnt = 0 : prefix = "S344_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C122, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C123, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C124 & "/16")

            '3.5 CHECK UNSTIFFENED COLUMN FLANGE AND WEB:
            cnt = 0 : prefix = "S35_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C129)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C131, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C132, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C133, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C134, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C135, 1))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D131, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D132, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D133, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D134, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D135, 1))
            '3.6 COLUMN WEB LOCAL YIELDING (AISC 360 J10.2):
            cnt = 0 : prefix = "S36_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C138, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C139, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C140, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C142, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C143, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C144, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C145)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C146)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C147, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C148, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C149)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C150)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D142, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D143, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D144, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D145)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D146)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D147, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D148, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D149)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D150)
            '3.7 COLUMN WEB LOCAL CRIPPLING (AISC 360 J10.3):
            cnt = 0 : prefix = "S37_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C153, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C154, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C155, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C156, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C157, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C158, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C159, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C160, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C162)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C163)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D162)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D163)
            '3.8 COLUMN WEB COMPRESSION BUCKLIING (AISC 360 J10.5):
            cnt = 0 : prefix = "S38_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C166, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C167, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C168, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C170)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C171)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D170)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D171)
            '3.9 SUMMARY OF LOCAL CAPACITIES:
            cnt = 0 : prefix = "S39_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C176)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C177)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C178)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C181, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C182)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C183)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C187)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C188)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C189)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C192, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C193)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C194)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G176)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G177)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G178)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G181, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G182)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G183)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G184)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G187)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G188)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G189)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G192, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G193)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G194)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G195)
            '3.10 WEB SIDESWAY BUCKLING (AISC 360-J10.4):
            cnt = 0 : prefix = "S310_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C198, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C199, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C200, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C201, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C202)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C203, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C204, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C205, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C206)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C207)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C208)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C209)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D198, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D199, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D200, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D201, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D202)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D203, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D204, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D205, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D206)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D207)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D208)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D209)
            '3.11 COLUMN STIFFENER DESIGN:
            cnt = 0 : prefix = "S311_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C214, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C215, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C216, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D214, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D215, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D216, 2))
            '3.12 CONTINUITY PLATE REQUIREMENTS BASED ON GEOMETRY AND TENSION YIELDING:
            cnt = 0 : prefix = "S312_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C220, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C221, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C222, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C223, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C224, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C225, 3))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C227, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C228, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C229, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C230, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C231, 3))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C233, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C234, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C235, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C236)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C237, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C238, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C239, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C240, 3))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C242, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C243, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C244, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C245, 2))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D227, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D228, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D229, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D230, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D231, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D233, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D234, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D235, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D236)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D237, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D238, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D239, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D240, 3))

            '3.13 FULL DEPTH STIFFENER PLATE FOR 2-SIDED MOMENT CONNECTIONS ONLY:
            cnt = 0 : prefix = "S313_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C249, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C250, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C251, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C252, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C253, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C254, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C255, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C256, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C257, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C258, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C259, 3))

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D249, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D250, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D251, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D252, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D253, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D254, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D255, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D256, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D257, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D258, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D259, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E259)
            '3.14 STIFFENER PLATE DOUBLE SIDE FILLET WELD TO COLUMN FLANGE:
            cnt = 0 : prefix = "S314_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C262, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C263, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C264, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C265, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C267, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C268, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C269, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D267, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D268, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D269, 3))

            '3.15 STIFFENER PLATE DOUBLE SIDE FILLET WELD TO COLUMN WEB:
            cnt = 0 : prefix = "S315_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C272, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C273, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C274, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C275, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D272, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D273, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D274, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D275, 3))
            '3.16 CHECK MINIMUM COLUMN FLANGE THICKNESS:


            '3.17 CONNECTION AWAY FROM COLUMN ENDS (SST Step 18 Table 1.1):
            cnt = 0 : prefix = "S317_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C282, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C283, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C284)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C285)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C286)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C287)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C288)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C289)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C290)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C291)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C292)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C294, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C295, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C297, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C298, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C300)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C301, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C302, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D282, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D283, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D284)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D285)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D286)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D287)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D288)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D289)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D290)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D291)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D292)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D294, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D295, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D297, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D298, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D300)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D301, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D302, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E302)
            '3.16 STABILITY BRACING AT BEAM-TO-COLUMN CONNECTIONS  (AISC 341-16 SECTION E3 4.c (b))
            cnt = 0 : prefix = "S3161_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F318)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F319, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F320, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), 50)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), 1.1)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F323, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F324, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G318)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G319, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G320, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), 50)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), 1.1)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G323, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G324, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F325, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G325, 3))
            '3.18 CONNECTION AT STIFFENED COLUMN END (SST STEP 18, TABLE 1.2, CASE 1):
            cnt = 0 : prefix = "S318_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C307, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C308)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C309)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C310, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C311, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C312, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D307, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D308)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D309)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D310, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D311, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D312, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E312)
            'DCR Summary
            cnt = 0 : prefix = "S319_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), B317)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B318, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B319, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B320, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B321, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B322, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B323, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(B324, 3))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C319)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C320, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C321, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C322, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C323, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C324, 3))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D317)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D318)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D319, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D320)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D321)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D322)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D323)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D324)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D325)
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), H317)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(H318, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), H319)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(H320, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(H321, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(H322, 3))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I319)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I320, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I321, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I322, 3))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), J317)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(J318, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), J319)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(J320_tmp, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(J321, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(J322, 3))
        End If
    End Sub

End Class
