﻿Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports YieldLinkLib

Public Module mPubFunc
    Public mPath = IO.Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
    Public gTemp = IO.Path.GetTempPath & "YieldLinkConnection"
    Public toolName$ = "Yield-Link® Connection"
    Public toolVer$ = " v3.0"
    Public Const NA = "N/A"
    Public toolReleaseDate$ = "11/11/2020"
    Public Const defaultLink = "YL6-3"
    'Public gJob As New clsMRSJob
    '
#Region "Enum"

    Public Enum eThickness
        NA = 0
        T1_8 = 2
        T3_16 = 3
        T1_4 = 4
        T5_16 = 5
        T3_8 = 6
        T7_16 = 7
        T1_2 = 8
        T9_16 = 9
        T5_8 = 10
        T11_16 = 11
        T3_4 = 12
        T13_16 = 13
        T7_8 = 14
        T15_16 = 15
        T1 = 16
        T1_1_8 = 18
        '
        T10 = 10
        T12 = 12
        T15 = 15
        '
        T20 = 20
        T25 = 25
    End Enum

    Public Enum aLimit
        Hx10 = 10
        Hx15 = 15
        Hx20 = 20
        Hx25 = 25
    End Enum

    Public Enum bType
        A325X
        A325N
        A490X
        A490N
    End Enum
    Public Function GetbolttypeStr(bolttype As bType) As String
        Select Case bolttype
            Case bType.A325N
                Return "A325_N"
            Case bType.A325X
                Return "A325_X"
            Case bType.A490N
                Return "A490_N"
            Case Else
                Return "A490_X"
        End Select
    End Function
    Public Enum bSize
        B7_8 = 7
        B8_8 = 8
        B9_8 = 9
        B10_8 = 10
    End Enum

    Public Enum frmType
        Any
        JobInfo
        DCRLimit
        DesignParameters
        SlabDepths
        Material
    End Enum

    Public Enum Letter As Integer
        Any
        A
        B
        C
        D
        E
        F
        G
        H
        I
        J
        K
        L
        M
        N
        O
        P
        Q
        R
        S
        T
        U
        V
        W
    End Enum

    Public Enum ReactionCase
        LinkStrength
        SeismicDrift
        BeamDesign
        ColumnDesign
        V_bmGravity
    End Enum

#End Region
    Public Function GetBoltSize(bS As bSize) As String
        If bS = 7 Then
            Return "7/8"
        ElseIf bS = 8 Then
            Return "1"
        Else
            Return "1 1/4"
        End If
    End Function

    Public Function Find_Fnv(btype As bType) As Double
        'TABLE J3.2 AISC14
        If btype = mPubFunc.bType.A325N Then
            Return (54)
        ElseIf btype = mPubFunc.bType.A325X Or btype = mPubFunc.bType.A490N Then
            Return (68)
        ElseIf btype = mPubFunc.bType.A490X Then
            Return (84)
        End If
        Return 0
    End Function

    Public Function aLimit2String(val As aLimit) As String
        If val = aLimit.Hx10 Then : Return "0.01Hx"
        ElseIf val = aLimit.Hx15 Then : Return "0.015Hx"
        ElseIf val = aLimit.Hx20 Then : Return "0.02Hx"
        Else : Return "0.025Hx"
        End If
    End Function
    Public Function String2aLimit(val As String) As aLimit
        If val = "0.01Hx" Then : Return aLimit.Hx10
        ElseIf val = "0.015Hx" Then : Return aLimit.Hx15
        ElseIf val = "0.020Hx" Then : Return aLimit.Hx20
        Else : Return aLimit.Hx25
        End If
    End Function

    Public Function Lvmin(db As Double) As Double
        Select Case db
            Case 0.5
                Return 0.75
            Case 0.625
                Return 0.875
            Case 0.75
                Return 1
            Case 0.875
                Return 1.125
            Case 1
                Return 1.25
            Case 1.125
                Return 1.5
            Case 1.25
                Return 1.625
            Case Else
                Return 1.25 * db
        End Select
    End Function
    Public Function Thk2Status(thk As Double) As String
        Return IIf(thk > 0, "YES", "NO")
    End Function
    Public Function eThickness2String(eThickness As eThickness) As String
        If eThickness = 1 Then : Return ("1/16")
        ElseIf eThickness = 2 Then : Return ("1/8")
        ElseIf eThickness = 3 Then : Return ("3/16")
        ElseIf eThickness = 4 Then : Return ("1/4")
        ElseIf eThickness = 5 Then : Return ("5/16")
        ElseIf eThickness = 6 Then : Return ("3/8")
        ElseIf eThickness = 7 Then : Return ("7/16")
        ElseIf eThickness = 8 Then : Return ("1/2")
        ElseIf eThickness = 9 Then : Return ("9/16")
        ElseIf eThickness = 10 Then : Return ("5/8")
        ElseIf eThickness = 11 Then : Return ("11/16")
        ElseIf eThickness = 12 Then : Return ("3/4")
        ElseIf eThickness = 13 Then : Return ("13/16")
        ElseIf eThickness = 14 Then : Return ("7/8")
        ElseIf eThickness = 15 Then : Return ("15/16")
        ElseIf eThickness = 16 Then : Return ("1")
        ElseIf eThickness = 18 Then : Return ("1 1/8")
        Else : Return NA
        End If
    End Function

    Public Function String2eThickness(val As String) As eThickness
        'NotUse
        If val = "1/8" Then : Return eThickness.T1_8
        ElseIf val = "3/16" Then : Return eThickness.T3_16
        ElseIf val = "1/4" Then : Return eThickness.T1_4
        ElseIf val = "5/16" Then : Return eThickness.T5_16
        ElseIf val = "3/8" Then : Return eThickness.T3_8
        ElseIf val = "7/16" Then : Return eThickness.T7_16
        ElseIf val = "1/2" Then : Return eThickness.T1_2
        ElseIf val = "9/16" Then : Return eThickness.T9_16
        ElseIf val = "5/8" Then : Return eThickness.T5_8
        ElseIf val = "11/16" Then : Return eThickness.T11_16
        ElseIf val = "3/4" Then : Return eThickness.T3_4
        ElseIf val = "13/16" Then : Return eThickness.T13_16
        ElseIf val = "7/8" Then : Return eThickness.T7_8
        ElseIf val = "15/16" Then : Return eThickness.T15_16
        ElseIf val = "1" Then : Return eThickness.T1
        ElseIf val = "1 1/8" Then : Return eThickness.T1_1_8
        Else : Return eThickness.NA
        End If
    End Function

    Public Function Double2eThickness(val As Double) As eThickness
        'NotUse
        If val <= 1 / 8 Then
            Return eThickness.T1_8
        ElseIf val <= 3 / 16 Then
            Return eThickness.T3_16
        ElseIf val <= 1 / 4 Then
            Return eThickness.T1_4
        ElseIf val <= 5 / 16 Then
            Return eThickness.T5_16
        ElseIf val <= 3 / 8 Then
            Return eThickness.T3_8
        ElseIf val <= 7 / 16 Then
            Return eThickness.T7_16
        ElseIf val <= 1 / 2 Then
            Return eThickness.T1_2
        ElseIf val <= 9 / 16 Then
            Return eThickness.T9_16
        ElseIf val <= 5 / 8 Then
            Return eThickness.T5_8
        ElseIf val <= 11 / 16 Then
            Return eThickness.T11_16
        ElseIf val <= 3 / 4 Then
            Return eThickness.T3_4
        ElseIf val <= 13 / 16 Then
            Return eThickness.T13_16
        ElseIf val <= 7 / 8 Then
            Return eThickness.T7_8
        ElseIf val <= 15 / 16 Then
            Return eThickness.T15_16
        ElseIf val <= 1 Then
            Return eThickness.T1
        Else
            Return eThickness.NA
        End If
    End Function

    Public Function String2bSize(val As String) As bSize
        If val = "7/8" Then : Return bSize.B7_8
        ElseIf val = "1" Then : Return bSize.B8_8
        Else : Return bSize.B10_8
        End If
    End Function
    Public Function String2bType(val As String) As bType
        If val = "A325N" Then
            Return bType.A325N
        ElseIf val = "A490X" Then
            Return bType.A490X
        ElseIf val = "A490N" Then
            Return bType.A490N
        Else
            Return bType.A325X
        End If
    End Function

    Public Function StrtoDbl(strs As String) As Double
        If strs = "NA" Then
            Return 0
        Else
            Dim S() As String
            If strs.Contains("/") Then
                S = strs.Split("/")
                Return CDbl(S(0)) / CDbl(S(1))
            Else
                Return CDbl(strs)
            End If
        End If
    End Function

    Public Function NumberFormat(val As Object, digit As Integer) As String
        If Not IsNumeric(val) Then
            Return val
        End If

        Dim dblVal As Double = val
        Select Case digit
            Case -1 : Return Format(dblVal, "0.0##")
            Case -2 : Return Format(dblVal, "0.00#")
            Case 0 : Return Format(dblVal, "0")
            Case 1 : Return Format(dblVal, "0.0")
            Case 2 : Return Format(dblVal, "0.00")
            Case 3 : Return Format(dblVal, "0.000")
            Case 4 : Return Format(dblVal, "0.0000")
            Case 5 : Return Format(dblVal, "0.00000")
            Case 6 : Return Format(dblVal, "0.000000")
        End Select
    End Function

    Public Function SiteClass2Index(val As String) As Integer
        If val = "A" Then
            Return 0
        ElseIf val = "B" Then
            Return 1
        ElseIf val = "C" Then
            Return 2
        ElseIf val = "D" Then
            Return 3
        ElseIf val = "E" Then
            Return 4
        Else
            Return 5
        End If
    End Function
    Public Function DriftLimitToString(val As DriftLimit) As String
        If val = DriftLimit.Hx10 Then
            Return "0.01"
        ElseIf val = DriftLimit.Hx15 Then
            Return "0.15"
        ElseIf val = DriftLimit.Hx20 Then
            Return "0.20"
        ElseIf val = DriftLimit.Hx25 Then
            Return "0.25"
        Else
            Return "0.00"
        End If
    End Function

#Region "XML Seriallize"
    Public Function Serialize_SSTLinks_ToXML(ByVal filename As String, _job As SSTLinks) As Boolean
        Dim writer As IO.StreamWriter = Nothing
        Dim ret = False
        Try
            Dim s As New XmlSerializer(GetType(SSTLinks))
            writer = New IO.StreamWriter(filename)
            s.Serialize(writer, _job)
            ret = True
        Catch ex As Exception
            ret = False
            'MsgBox(ex.Message & ex.StackTrace)
        Finally
            If Not writer Is Nothing Then
                writer.Close()
            End If
        End Try
        '
        Return ret
    End Function

    Public Function DeSerialize_XML_ToSSTLinks(filename As String) As SSTLinks
        Dim job As SSTLinks = Nothing
        Dim textReader As IO.TextReader = Nothing
        Try
            Dim deserializer As New XmlSerializer(GetType(SSTLinks))
            textReader = New IO.StreamReader(filename)
            job = DirectCast(deserializer.Deserialize(textReader), SSTLinks)
            textReader.Close()
            '
        Catch ex As Exception
            If Not textReader Is Nothing Then
                textReader.Close()
            End If
        End Try
        Return job
    End Function

    Public Function Serialize_AllWsections_ToXML(ByVal filename As String, cls As AllWsections) As Boolean
        Dim writer As IO.StreamWriter = Nothing
        Dim ret = False
        Try
            Dim s As New XmlSerializer(GetType(AllWsections))
            writer = New IO.StreamWriter(filename)
            s.Serialize(writer, cls)
            ret = True
        Catch ex As Exception
            ret = False
            'MsgBox(ex.Message & ex.StackTrace)
        Finally
            If Not writer Is Nothing Then
                writer.Close()
            End If
        End Try
        '
        Return ret
    End Function

    Public Function DeSerialize_XML_ToAllWsections(filename As String) As AllWsections
        Dim cls As AllWsections = Nothing
        Dim textReader As IO.TextReader = Nothing
        Try
            Dim deserializer As New XmlSerializer(GetType(AllWsections))
            textReader = New IO.StreamReader(filename)
            cls = DirectCast(deserializer.Deserialize(textReader), AllWsections)
            textReader.Close()
            '
        Catch ex As Exception
            If Not textReader Is Nothing Then
                textReader.Close()
            End If
        End Try
        Return cls
    End Function

    Public Function DeSerialize_XML_ToSSTOthers(filename As String) As SSTOthers
        Dim cls As SSTOthers = Nothing
        Dim textReader As IO.TextReader = Nothing
        Try
            Dim deserializer As New XmlSerializer(GetType(SSTOthers))
            textReader = New IO.StreamReader(filename)
            cls = DirectCast(deserializer.Deserialize(textReader), SSTOthers)
            textReader.Close()
            '
        Catch ex As Exception
            If Not textReader Is Nothing Then
                textReader.Close()
            End If
        End Try
        Return cls
    End Function

    Public Function Serialize_saveInfo_ToXML(ByVal filename As String, _job As saveInfo) As Boolean
        Dim writer As IO.StreamWriter = Nothing
        Dim ret = False
        Try
            Dim s As New XmlSerializer(GetType(saveInfo))
            writer = New IO.StreamWriter(filename)
            s.Serialize(writer, _job)
            ret = True
        Catch ex As Exception
            ret = False
        Finally
            If Not writer Is Nothing Then
                writer.Close()
            End If
        End Try
        Return ret
    End Function

    Public Function DeSerialize_XML_saveInfo(filename As String) As saveInfo
        Dim job As saveInfo = Nothing
        Dim textReader As IO.TextReader = Nothing
        Try
            Dim deserializer As New XmlSerializer(GetType(saveInfo))
            textReader = New IO.StreamReader(filename)
            job = DirectCast(deserializer.Deserialize(textReader), saveInfo)
            textReader.Close()
        Catch ex As Exception
            If Not textReader Is Nothing Then
                textReader.Close()
            End If
        End Try
        Return job
    End Function

    Public Function Serialize_gJob_ToXML(ByVal filename As String, _job As clsMRSJob) As Boolean
        Dim writer As IO.StreamWriter = Nothing
        Dim ret = False
        Try
            Dim curPath As String = IO.Path.GetDirectoryName(filename)
            If Not IO.Directory.Exists(curPath) Then
                IO.Directory.CreateDirectory(curPath)
            End If
            Dim s As New XmlSerializer(GetType(clsMRSJob))
            writer = New IO.StreamWriter(filename)
            s.Serialize(writer, _job)
            ret = True
        Catch ex As Exception
            ret = False
        Finally
            If Not writer Is Nothing Then
                writer.Close()
            End If
        End Try
        Return ret
    End Function

    Public Function DeSerialize_XML_gJob(filename As String) As clsMRSJob
        Dim job As clsMRSJob = Nothing
        Dim textReader As IO.TextReader = Nothing
        Try
            Dim deserializer As New XmlSerializer(GetType(clsMRSJob))
            textReader = New IO.StreamReader(filename)
            job = DirectCast(deserializer.Deserialize(textReader), clsMRSJob)
            textReader.Close()
        Catch ex As Exception
            If Not textReader Is Nothing Then
                textReader.Close()
            End If
        End Try
        Return job
    End Function

#End Region
    Public Function ExportToExcel(dgv1 As DataGridView, dgv2 As DataGridView, dgv3 As DataGridView,
                                  dgv4 As DataGridView, dgv5 As DataGridView, curJob As clsMRSJob) As String

        Dim tempExcel = "D:\YieldLinkConnection.xlsx" ' IO.Path.Combine(mPath, "Data\YieldLinkConnection.xltx")
        Dim miss As Object = Type.Missing 'System.Reflection.Missing.Value
        Dim ExApp As Object = Nothing 'As Microsoft.Office.Interop.Excel.Application = Nothing
        Dim Wsheet As Object = Nothing ' As Microsoft.Office.Interop.Excel.Worksheet = Nothing
        Dim Wbook As Object = Nothing 'As Microsoft.Office.Interop.Excel.Workbook = Nothing
        Dim rr, cc As Integer
        '
        Dim startRow As Integer = 3
        Dim ret As String = ""
        '
        Try
            ExApp = CreateObject("Excel.Application") 'New Microsoft.Office.Interop.Excel.Application
            Wbook = ExApp.Workbooks.Open(tempExcel)
            ExApp.DisplayAlerts = False
            ExApp.Calculation = -4135 'xlCalculationManual ' Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationManual
            ExApp.EnableEvents = False
            'sheet 1
            Wsheet = Wbook.Sheets("Settings")
            Dim cnt, sRow As Integer
            Dim Col As Letter
            'Allowable Drift Limit:
            Wsheet.range("K3").value = LimitToString(curJob.DCRLimits.DriftLimitSelect)
            'Delta Elastic x Cd:
            'Wsheet.range("K5").value = curJob.DCRLimits.PlasticDriftLimit
            ' "tbf_check"
            sRow = 5
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Initial tbf_check"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.tbf_check, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            '"Lyield"
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Initial bf_check"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.bf_check, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Lyield_check
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Initial Lyield_check"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Lyield_check, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Panel_Zone_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Panel_Zone_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Panel_Zone_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Drift_check
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Drift_check"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Drift_check, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Beam_tf_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Beam_tf_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Beam_tf_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Link_strength_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Link_strength_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Link_strength_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            't_BRP_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "t_BRP_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.t_BRP_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'BRP_Bolt_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "BRP_Bolt_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.BRP_Bolt_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'SCWB_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "SCWB_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.SCWB_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Stiffener_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Stiffener_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Stiffener_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Column_Flange_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Column_Flange_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Column_Flange_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Beam_Web_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Beam_Web_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Beam_Web_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Shear_Plate_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Shear_Plate_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Shear_Plate_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Bolt_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Bolt_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Bolt_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Fillet_weld_DCR
            cnt += 1 : sRow += 1 : Col = Letter.H
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Fillet_weld_DCR"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Fillet_weld_DCR, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Max, -1)
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = NumberFormat(curJob.DCRLimits.Min, -1)
            'Project Information
            cnt = 0 : sRow = 3 : Col = Letter.Any
            cnt += 1 : sRow += 1 : Col = Letter.Any
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Job ID"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.JobID
            cnt += 1 : sRow += 1 : Col = Letter.Any
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Job Name"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.Name
            cnt += 1 : sRow += 1 : Col = Letter.Any
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Job Address"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.Address
            cnt += 1 : sRow += 1 : Col = Letter.Any
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "City, State"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.CityState
            cnt += 1 : sRow += 1 : Col = Letter.Any
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Design Firm Name"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.EOR
            cnt += 1 : sRow += 1 : Col = Letter.Any
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Design By"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.EngName
            cnt += 1 : sRow += 1 : Col = Letter.Any
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Email Address"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.EngEmail
            'Design Parameters
            cnt = 0
            sRow = 3
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Importance Factor, I"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.I
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = "Redundancy Factor, Rho"
            Col += 1 : Wsheet.range(Col.ToString & sRow).value = curJob.Rho
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = "Design Spectral Response Acceleration Parameter, SDS"
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = curJob.Sds
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = "Response Modification Coefficient, R"
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = curJob.R
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = "Overstrength Factor, Omega"
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = curJob.Omega
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = "Deflection Amplification factor, Cd"
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = curJob.Cd
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = "Live Load Factor, f1"
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = curJob.f1
            cnt += 1 : Col = Letter.D : sRow += 1
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = cnt
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = "Snow Load Factor, f2"
            Col += 1 : Wsheet.range(Col.ToString & sRow).Value = curJob.f2
            'sheet 1
            Wsheet = Wbook.Sheets("ILS Summary") ' DirectCast(Wbook.Sheets(1), Microsoft.Office.Interop.Excel.Worksheet)
            ''Wsheet.Select()
            For rr = 0 To dgv1.RowCount - 1
                For cc = 0 To dgv1.ColumnCount - 1
                    Wsheet.Cells(rr + startRow, cc + 1).value = dgv1(cc, rr).Value
                Next
            Next
            '
            Wsheet = Wbook.Sheets("BLC Summary")
            For rr = 0 To dgv2.RowCount - 1
                For cc = 0 To dgv2.ColumnCount - 1
                    Wsheet.Cells(rr + startRow, cc + 1).value = dgv2(cc, rr).Value
                Next
            Next
            '
            Wsheet = Wbook.Sheets("CC Summary")
            For rr = 0 To dgv3.RowCount - 1
                For cc = 0 To dgv3.ColumnCount - 1
                    Wsheet.Cells(rr + startRow, cc + 1).value = dgv3(cc, rr).Value
                Next
            Next
            '
            Wsheet = Wbook.Sheets("SPC Summary")
            For rr = 0 To dgv4.RowCount - 1
                For cc = 0 To dgv4.ColumnCount - 1
                    Wsheet.Cells(rr + startRow, cc + 1).value = dgv4(cc, rr).Value
                Next
            Next
            '
            Wsheet = Wbook.Sheets("Drift Summary")
            For rr = 0 To dgv5.RowCount - 1
                For cc = 0 To dgv5.ColumnCount - 1
                    Wsheet.Cells(rr + startRow, cc + 1).value = dgv5(cc, rr).Value
                Next
            Next
            '
            ret = "Done!"
        Catch ex As Exception
            'MsgBox(ex.Message)
            ret = ex.Message.Split(".")(0)
        Finally
            If Wbook Is Nothing Then
                ExApp.Quit()
            Else
                'Wbook.Save()
                If Not ExApp Is Nothing Then
                    ExApp.DisplayAlerts = True
                    ExApp.Calculation = -4105 'xlCalculationAutomatic
                    ExApp.EnableEvents = True
                    ExApp.Visible = True
                    ExApp.WindowState = -4137 'Maximized    |-4143	Normal  |-4140	Minimized
                    '
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ExApp)
                    ExApp = Nothing
                End If
            End If
        End Try
        Return ret
    End Function

    Public Function Ceiling(ByVal value As Double, ByVal significance As Double) As Double
        If value Mod significance <> 0 Then
            Return (Math.Floor(value / significance) * significance) + significance
        End If
        Return Convert.ToDouble(value)
    End Function

    Public Function Floor(ByVal value As Double, ByVal significance As Double) As Double
        If value Mod significance <> 0 Then
            Return (Math.Floor(value / significance) * significance)
        End If

        Return Convert.ToDouble(value)
    End Function

    Public Function MaxAll(ParamArray values()) As Double
        Try
            Return Linq.Enumerable.Max(values)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function MinAll(ParamArray values()) As Double
        Try
            Return Linq.Enumerable.Min(values)
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Function WfilletMin(Tstiff As Double) As Double

        If Tstiff < 0.125 Then
            Return 0
        ElseIf Tstiff <= 0.25 Then
            Return 0.125
        ElseIf Tstiff <= 0.5 Then
            Return 0.1875
        ElseIf Tstiff <= 0.75 Then
            Return 0.25
        Else
            Return 0.3125
        End If

    End Function

    Public Function LimitToString(lm As DriftLimit) As String
        If lm = DriftLimit.Hx10 Then
            Return "0.010Hx"
        ElseIf lm = DriftLimit.Hx15 Then
            Return "0.015Hx"
        ElseIf lm = DriftLimit.Hx25 Then
            Return "0.025Hx"
        ElseIf lm = DriftLimit.Other Then
            Return "Other"
        Else
            Return "0.020Hx"
        End If
    End Function

    Public Function Lookup_Sh_Sv(W_3digit As String, isSv As Boolean) As Double
        Dim ret As Double
        If W_3digit = "W12" Then
            ret = IIf(isSv, 2.25, 2.75)
        ElseIf W_3digit = "W14" Then
            ret = IIf(isSv, 2.75, 2.75)
        ElseIf W_3digit = "W16" Then
            ret = IIf(isSv, 2.75, 2.75)
        ElseIf W_3digit = "W18" Then
            ret = IIf(isSv, 2.75, 2.75)
        ElseIf W_3digit = "W21" Then
            ret = IIf(isSv, 2.375, 2.75)
        ElseIf W_3digit = "W24" Then
            ret = IIf(isSv, 2.75, 2.75)
        ElseIf W_3digit = "W27" Then
            ret = IIf(isSv, 2.375, 2.75)
        ElseIf W_3digit = "W30" Then
            ret = IIf(isSv, 2.75, 2.75)
        ElseIf W_3digit = "W33" Then
            ret = IIf(isSv, 2.5, 2.75)
        ElseIf W_3digit = "W36" Then
            ret = IIf(isSv, 2.875, 2.75)
        End If

        Return ret
    End Function

    Public Function Inches2Fraction(_val As Double) As String
        'MinH \ 12 & "'-" & Format(MinH Mod 12, "#.####")
        Dim pref As String = "("
        If _val < 0 Then
            _val = Math.Abs(_val)
            pref = "-("
        End If
        Dim Val_ft As Double = _val \ 12
        Dim Val_in As Double = Math.Round(_val - Val_ft * 12, 2)

        If Math.Abs(Val_in) < 0.01 Then
            Return pref & Val_ft & "'-0"")"
        Else
            Return pref & Val_ft & "'-" & Format(Val_in, "#.####") & """)"
        End If
    End Function

    Public Function Read_SSTLinks() As SSTLinks
        Dim codeFilename As String
        codeFilename = Reflection.Assembly.GetExecutingAssembly().GetName.Name & ".xml_SSTLinks.xml"
        Dim XmlSer As New System.Xml.Serialization.XmlSerializer(GetType(SSTLinks))
        Dim Reader As System.IO.Stream = GetType(SSTLinks).Assembly.GetManifestResourceStream(codeFilename)
        Try
            Dim code As SSTLinks = DirectCast(XmlSer.Deserialize(Reader), SSTLinks)
            Return code
        Catch ex As Exception
        Finally
            Reader.Close()
        End Try
    End Function

    Public Function Read_SSTOthers() As SSTOthers
        Dim codeFilename As String
        codeFilename = Reflection.Assembly.GetExecutingAssembly().GetName.Name & ".xml_Others.xml"
        Dim XmlSer As New System.Xml.Serialization.XmlSerializer(GetType(SSTOthers))
        Dim Reader As System.IO.Stream = GetType(SSTOthers).Assembly.GetManifestResourceStream(codeFilename)
        Try
            Dim code As SSTOthers = DirectCast(XmlSer.Deserialize(Reader), SSTOthers)
            Return code
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Reader.Close()
        End Try
    End Function

    Public Function Read_AllWsections() As AllWsections
        Dim codeFilename As String '= Reflection.Assembly.GetExecutingAssembly().GetName.Name & ".xml_AllWsections.xml"

        codeFilename = Reflection.Assembly.GetExecutingAssembly().GetName.Name & ".xml_AllWsections.xml"
        Dim XmlSer As New System.Xml.Serialization.XmlSerializer(GetType(AllWsections))
        Dim Reader As System.IO.Stream = GetType(AllWsections).Assembly.GetManifestResourceStream(codeFilename)
        Try
            Dim code As AllWsections = DirectCast(XmlSer.Deserialize(Reader), AllWsections)
            Return code
        Catch ex As Exception
        Finally
            Reader.Close()
        End Try
    End Function

    Public Function FindShearTabBolt(bSize As String, curJob As clsMRSJob, bmDepth As Double) As SheartabBolt
        Dim strW3 As String '= Left(bSize, 3)
        If bSize Like "W###*" Then
            strW3 = Left(bSize, 4)
            Return curJob.SSTOthers.SheartabBolts.Find(Function(x) x.Size = strW3)
        ElseIf bSize Like "W##*" Then
            strW3 = Left(bSize, 3)
            Return curJob.SSTOthers.SheartabBolts.Find(Function(x) x.Size = strW3)
        Else
            Return curJob.SSTOthers.SheartabBolts.Find(Function(x) x.Db_min < bmDepth AndAlso bmDepth <= x.Db_max)
        End If
    End Function

    Public Function Find_Nailer(lk As String) As Double
        If lk Like "YL8*" Then : Return 6.25
        ElseIf lk Like "YL6*" Then : Return 5.5
        Else : Return 3.5
        End If
    End Function
End Module
