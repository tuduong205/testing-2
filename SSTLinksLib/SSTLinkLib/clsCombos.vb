﻿Public Class clsELFCombos
    Public Id As Long
    Public Name As String
    Public fDL As Double
    Public fLL As Double
    Public fLR As Double
    Public fSL As Double
    Public fRL As Double
    'Public fWX As Double
    'Public fWY As Double
    Public fW As Double
    Public fNLX As Double
    Public fNLY As Double
    Public fPDELTA As Double
    '
    Public fEQX As Double
    Public fEQXPY As Double
    Public fEQXNY As Double
    '
    Public fEQY As Double
    Public fEQYPX As Double
    Public fEQYNX As Double
    '
    Public fEQX_D As Double
    Public fEQXPY_D As Double
    Public fEQXNY_D As Double
    '
    Public fEQY_D As Double
    Public fEQYPX_D As Double
    Public fEQYNX_D As Double
    '
    Public fWL As Double
    Public fNL As Double
    Public fEL As Double
    Public fEL_D As Double
    '
    Public fELF100XPY30Y As Double
    Public fELF100XNY30Y As Double
    Public fELF100XPY_30Y As Double
    Public fELF100XNY_30Y As Double
    Public fELF_100XPY30Y As Double
    Public fELF_100XNY30Y As Double
    Public fELF_100XPY_30Y As Double
    Public fELF_100XNY_30Y As Double
    Public fELF30X100YPX As Double
    Public fELF30X100YNX As Double
    Public fELF30X_100YPX As Double
    Public fELF30X_100YNX As Double
    Public fELF_30X100YPX As Double
    Public fELF_30X100YNX As Double
    Public fELF_30X_100YPX As Double
    Public fELF_30X_100YNX As Double
    '
    Public fEL_Omega As Double
    Public fEL_Omega_130 As Double
    '


    Sub New()

    End Sub
    Sub New(_ID As Long, _name As String, _fDL As Double, _fLL As Double, _fLR As Double, _fSL As Double, _fRL As Double,
            _fW As Double, _fWtmp As Double, _fNLX As Double, _fNLY As Double, _fPDELTA As Double,
            _fEQX As Double, _fEQXPY As Double, _fEQXNY As Double, _fEQY As Double, _fEQYPX As Double, _fEQYNX As Double,
            _fEQX_D As Double, _fEQXPY_D As Double, _fEQXNY_D As Double, _fEQY_D As Double, _fEQYPX_D As Double, _fEQYNX_D As Double,
            _fWL As Double, _fNL As Double, _fEL As Double, _fEL_Omega As Double, _fEL_D As Double,
            _fELF100XPY30Y As Double, _fELF100XNY30Y As Double, _fELF100XPY_30Y As Double, _fELF100XNY_30Y As Double, _fELF_100XPY30Y As Double, _fELF_100XNY30Y As Double,
            _fELF_100XPY_30Y As Double, _fELF_100XNY_30Y As Double, _fELF30X100YPX As Double, _fELF30X100YNX As Double, _fELF30X_100YPX As Double, _fELF30X_100YNX As Double, _fELF_30X100YPX As Double,
            _fELF_30X100YNX As Double, _fELF_30X_100YPX As Double, _fELF_30X_100YNX As Double, _fEL_Omega_130 As Double)
        Id = _ID
        Name = _name
        fDL = _fDL
        fLL = _fLL
        fLR = _fLR
        fSL = _fSL
        fRL = _fRL
        fW = _fW
        'fWX = _fWX
        'fWY = _fWY
        fNLX = _fNLX
        fNLY = _fNLY
        fPDELTA = _fPDELTA
        '
        fEQX = _fEQX
        fEQXPY = _fEQXPY
        fEQXNY = _fEQXNY
        '
        fEQY = _fEQY
        fEQYPX = _fEQYPX
        fEQYNX = _fEQYNX
        '
        fEQX_D = _fEQX_D
        fEQXPY_D = _fEQXPY_D
        fEQXNY_D = _fEQXNY_D
        '
        fEQY_D = _fEQY_D
        fEQYPX_D = _fEQYPX_D
        fEQYNX_D = _fEQYNX_D
        '
        fWL = _fWL
        fNL = _fNL
        fEL = _fEL
        fEL_D = _fEL_D
        '
        fELF100XPY30Y = _fELF100XPY30Y
        fELF100XNY30Y = _fELF100XNY30Y
        fELF100XPY_30Y = _fELF100XPY_30Y
        fELF100XNY_30Y = _fELF100XNY_30Y
        fELF_100XPY30Y = _fELF_100XPY30Y
        fELF_100XNY30Y = _fELF_100XNY30Y
        fELF_100XPY_30Y = _fELF_100XPY_30Y
        fELF_100XNY_30Y = _fELF_100XNY_30Y
        fELF30X100YPX = _fELF30X100YPX
        fELF30X100YNX = _fELF30X100YNX
        fELF30X_100YPX = _fELF30X_100YPX
        fELF30X_100YNX = _fELF30X_100YNX
        fELF_30X100YPX = _fELF_30X100YPX
        fELF_30X100YNX = _fELF_30X100YNX
        fELF_30X_100YPX = _fELF_30X_100YPX
        fELF_30X_100YNX = _fELF_30X_100YNX
        fEL_Omega = _fEL_Omega
        fEL_Omega_130 = _fEL_Omega_130
    End Sub
    Sub New(_ID As Long, _name As String, _fDL As Double, _fLL As Double, _fSL As Double, _fW As Double, _fEL As Double)
        Id = _ID
        Name = _name
        fDL = _fDL
        fLL = _fLL
        fSL = _fSL
        fW = _fW
        fEL = _fEL
    End Sub
End Class
Public Class clsMRSACombos
    Public Id As Long
    Public Name As String
    Public fDL As Double
    Public fLL As Double
    Public fLR As Double
    Public fSL As Double
    Public fRL As Double
    Public fW As Double
    'Public fWX As Double
    'Public fWY As Double
    Public fNLX As Double
    Public fNLY As Double
    Public fPDELTA As Double
    '
    Public fEQX As Double
    Public fEQXPY As Double
    Public fEQXNY As Double
    '
    Public fEQY As Double
    Public fEQYPX As Double
    Public fEQYNX As Double
    '
    Public fEQX_D As Double
    Public fEQXPY_D As Double
    Public fEQXNY_D As Double
    '
    Public fEQY_D As Double
    Public fEQYPX_D As Double
    Public fEQYNX_D As Double
    '
    Public fWL As Double
    Public fNL As Double
    Public fEL As Double
    Public fEL_D As Double
    '
    Public fSPECX As Double
    Public fSPECX_PY As Double
    Public fSPECX_NY As Double
    Public fSPECY As Double
    Public fSPECY_PX As Double
    Public fSPECY_NX As Double
    '                    
    Public fSPECX_D As Double
    Public fSPECX_PY_D As Double
    Public fSPECX_NY_D As Double
    Public fSPECY_D As Double
    Public fSPECY_PX_D As Double
    Public fSPECY_NX_D As Double
    '                              
    Public fMRSA100XPY30Y As Double
    Public fMRSA100XNY30Y As Double
    Public fMRSA100XPY_30Y As Double
    Public fMRSA100XNY_30Y As Double
    Public fMRSA_100XPY30Y As Double
    Public fMRSA_100XNY30Y As Double
    Public fMRSA_100XPY_30Y As Double
    Public fMRSA_100XNY_30Y As Double
    Public fMRSA30X100YPX As Double
    Public fMRSA30X100YNX As Double
    Public fMRSA30X_100YPX As Double
    Public fMRSA30X_100YNX As Double
    Public fMRSA_30X100YPX As Double
    Public fMRSA_30X100YNX As Double
    Public fMRS_30X_100YPX As Double
    Public fMRSA_30X_100YNX As Double
    '
    Public fEL_Omega As Double
    Public fEL_Omega_130 As Double
    '
    Sub New()

    End Sub
    Sub New(_ID As Long, _name As String, _fSPECX As Double, _fSPECX_PY As Double, _fSPECX_NY As Double, _fSPECY As Double, _fSPECY_PX As Double, _fSPECY_NX As Double, _fSPECX_D As Double, _fSPECX_PY_D As Double, _fSPECX_NY_D As Double,
            _fSPECY_D As Double, _fSPECY_PX_D As Double, _fSPECY_NX_D As Double)
        Id = _ID
        Name = _name
        fSPECX = _fSPECX
        fSPECX_PY = _fSPECX_PY
        fSPECX_NY = _fSPECX_NY
        fSPECY = _fSPECY
        fSPECY_PX = _fSPECY_PX
        fSPECY_NX = _fSPECY_NX

        fSPECX_D = _fSPECX_D
        fSPECX_PY_D = _fSPECX_PY_D
        fSPECX_NY_D = _fSPECX_NY_D
        fSPECY_D = _fSPECY_D
        fSPECY_PX_D = _fSPECY_PX_D
        fSPECY_NX_D = _fSPECY_NX_D
    End Sub

    Sub New(_ID As Long, _name As String, _fDL As Double, _fLL As Double, _fLR As Double, _fSL As Double, _fRL As Double,
            _fW As Double, _fWtmp As Double, _fNLX As Double, _fNLY As Double, _fPDELTA As Double,
            _fEQX As Double, _fEQXPY As Double, _fEQXNY As Double, _fEQY As Double, _fEQYPX As Double, _fEQYNX As Double,
            _fEQX_D As Double, _fEQXPY_D As Double, _fEQXNY_D As Double, _fEQY_D As Double, _fEQYPX_D As Double, _fEQYNX_D As Double,
            _fWL As Double, _fNL As Double, _fEL As Double, _fEL_Omega As Double, _fEL_D As Double,
            _fMRSA100XPY30Y As Double, _fMRSA100XNY30Y As Double, _fMRSA100XPY_30Y As Double, _fMRSA100XNY_30Y As Double,
            _fMRSA_100XPY30Y As Double, _fMRSA_100XNY30Y As Double, _fMRSA_100XPY_30Y As Double, _fMRSA_100XNY_30Y As Double, _fMRSA30X100YPX As Double, _fMRSA30X100YNX As Double,
            _fMRSA30X_100YPX As Double, _fMRSA30X_100YNX As Double, _fMRSA_30X100YPX As Double, _fMRSA_30X100YNX As Double, _fMRS_30X_100YPX As Double, _fMRSA_30X_100YNX As Double, _fEL_Omega_130 As Double)
        Id = _ID
        Name = _name
        fDL = _fDL
        fLL = _fLL
        fLR = _fLR
        fSL = _fSL
        fRL = _fRL
        fW = _fW
        'fWX = _fWX
        'fWY = _fWY
        fNLX = _fNLX
        fNLY = _fNLY
        fPDELTA = _fPDELTA
        '
        fEQX = _fEQX
        fEQXPY = _fEQXPY
        fEQXNY = _fEQXNY
        '
        fEQY = _fEQY
        fEQYPX = _fEQYPX
        fEQYNX = _fEQYNX
        '
        fEQX_D = _fEQX_D
        fEQXPY_D = _fEQXPY_D
        fEQXNY_D = _fEQXNY_D
        '
        fEQY_D = _fEQY_D
        fEQYPX_D = _fEQYPX_D
        fEQYNX_D = _fEQYNX_D
        '
        fWL = _fWL
        fNL = _fNL
        fEL = _fEL
        fEL_D = _fEL_D
        '

        '
        fMRSA100XPY30Y = _fMRSA100XPY30Y
        fMRSA100XNY30Y = _fMRSA100XNY30Y
        fMRSA100XPY_30Y = _fMRSA100XPY_30Y
        fMRSA100XNY_30Y = _fMRSA100XNY_30Y
        fMRSA_100XPY30Y = _fMRSA_100XPY30Y
        fMRSA_100XNY30Y = _fMRSA_100XNY30Y
        fMRSA_100XPY_30Y = _fMRSA_100XPY_30Y
        fMRSA_100XNY_30Y = _fMRSA_100XNY_30Y
        fMRSA30X100YPX = _fMRSA30X100YPX
        fMRSA30X100YNX = _fMRSA30X100YNX
        fMRSA30X_100YPX = _fMRSA30X_100YPX
        fMRSA30X_100YNX = _fMRSA30X_100YNX
        fMRSA_30X100YPX = _fMRSA_30X100YPX
        fMRSA_30X100YNX = _fMRSA_30X100YNX
        fMRS_30X_100YPX = _fMRS_30X_100YPX
        fMRSA_30X_100YNX = _fMRSA_30X_100YNX
        '
        fEL_Omega = _fEL_Omega
        fEL_Omega_130 = _fEL_Omega_130
    End Sub

End Class
Public Class clsELFLoadCase
    Public ID As Long
    Public Name As String
    Public Load1Name As String
    Public fLoad1 As Double
    Public Load2Name As String
    Public fLoad2 As Double

    Sub New(_ID&, _Name$, _Load1Name$, _fLoad1#, _Load2Name$, _fLoad2#)
        ID = _ID
        Name = _Name
        Load1Name = _Load1Name
        fLoad1 = _fLoad1
        Load2Name = _Load2Name
        fLoad2 = _fLoad2
    End Sub
End Class
Public Class clsRSLoadCase
    Public ID As Long
    Public Name As String
    Public LoadName As String
    Public FunctName As String
    Public ScaleFactor As Double
    Public NewScaleFactor As Double
    Public EccRatio As Double

    Sub New()
    End Sub
    Sub New(_ID&, _Name$, _LoadName$, _FunctName$, _ScaleFactor#, _EccRatio#)
        ID = _ID
        Name = _Name
        LoadName = _LoadName
        FunctName = _FunctName
        ScaleFactor = _ScaleFactor
        EccRatio = _EccRatio
    End Sub

    Sub New(_LoadName$, _ScaleFactor#, _NewScaleFactor#)
        LoadName = _LoadName
        ScaleFactor = _ScaleFactor
        NewScaleFactor = _NewScaleFactor
    End Sub
End Class
