﻿Imports System.Xml.Serialization

<Serializable()> Public Class clsMRSJob
    Public isEtab As Boolean
    Public proID As String
    Public JobID As String = "ES-#"
    Public Name As String = "Test Job Name"
    Public Address As String
    Public CityState As String
    Public Zip As String
    Public EOR As String
    Public EngName As String
    Public EngEmail As String
    Public isFixedBase As Boolean = True
    Public isByGroupName As Boolean
    Public isBaseOnInPlaneShearForce As Boolean 'Updated Axial load estimate for Rigid Diaphragm
    Public isBaseOnLinkSizeAndRValue As Boolean 'Updated Axial load estimate for Rigid Diaphragm
    '
    Public FrType As String = "SMF"
    Public R As Double = 8
    Public Cd As Double = 5.5
    Public f1 As Double = 0.5 'LL load factor
    Public f2 As Double = 0.2 'Snow load factor
    Public I As Double = 1
    Public Rho As Double = 1
    Public Omega As Double = 3
    Public Ss As Double = 2
    Public S1 As Double = 1
    Public TL As Double = 8
    Public userSds As String = "YES"
    Public ASCE As String = "ASCE 7-16"

    Public DesignCode As String ' = IIf(isCanada, "CSA S16-14", "AISC 360-16")
    Public Sds As Double = 1
    Public SiteClass As String = "D"
    'y-dir
    Public FrTypeY As String = "SMF"
    Public RY As Double = 8
    Public OmegaY As Double = 3
    Public CdY As Double = 5.5
    Public IY As Double = 1
    Public RhoY As Double = 1
    ''Public f1Y As Double = 0.5
    ''Public f2Y As Double = 0.2
    '''
    ''Public WindSpeedY As Double = 80
    ''Public ExposureTypeY As String = "B"
    ''Public F_groundEleY As Double = 1.0
    ''Public KztY As Double = 1.0
    ''Public F_gustY As Double = 0.85
    ''Public KdY As Double = 0.85
    'wind US
    Public WindSpeed As Double = 80
    Public ExposureType As String = "B"
    Public F_groundEle As Double = 1.0
    Public Kzt As Double = 1.0
    Public F_gust As Double = 0.85
    Public Kd As Double = 0.85
    '
    Public DCRLimits As DCRLimits '(True, isCanada)
    Public SlabDepths As New List(Of sstSlabDepth)
    Public WeldingInfo As New WeldingInfo(True)
    Public MaterialInfo As MaterialInfo '(True, isCanada)
    '''
    ''Public DgvGridX As New List(Of String)
    ''Public DgvGridY As New List(Of String)
    ''Public DgvGridZ As New List(Of String)
    '
    Public Dgv1Data As New List(Of String)
    Public Dgv2Data As New List(Of String)
    Public Dgv3Data As New List(Of String)
    Public Dgv4Data As New List(Of String)
    Public Dgv5Data As New List(Of String)
    Public Dgv6Data As New List(Of String) 'Beam Design
    Public Dgv7Data As New List(Of String) 'Column Design
    Public Dgv8Data As New List(Of String) 'Weld Summary

    Public k As Double = 0
    Public l As Double = 0
    '
    <XmlIgnore> Public stlData As New SortedList(Of String, String)
    <XmlIgnore> Public PrintedDate As String = Format(Today, "MMM dd, yyyy")
    '
    <XmlIgnore> Public Const splChar = "|"
    <XmlIgnore> Public SSTLinks As SSTLinks
    <XmlIgnore> Public AllWsections As AllWsections
    <XmlIgnore> Public SSTOthers As SSTOthers
    '
    Public versionID As String
    '
    Sub New()
    End Sub
    Sub New(ArrSecsion() As String)
        DesignCode = "AISC 360-16"
        DCRLimits = New DCRLimits(True)
        MaterialInfo = New MaterialInfo(True)
        If Not ArrSecsion Is Nothing AndAlso ArrSecsion.Length > 0 Then
            Dim tmpArr() As String
            AllWsections = New AllWsections()
            For Each str As String In ArrSecsion
                tmpArr = str.Split(splChar)
                If AllWsections.Wsections.Find(Function(x) x.Size = tmpArr(0)) Is Nothing Then
                    AllWsections.Wsections.Add(New Wsec(tmpArr(0), tmpArr(1), tmpArr(2), tmpArr(3), tmpArr(4), tmpArr(5)))
                End If
            Next
        Else
            AllWsections = Read_AllWsections() ' DeSerialize_XML_ToAllWsections(IO.Path.Combine(mPath, "xml_AllWsections.xml"))
        End If
        SSTLinks = Read_SSTLinks() ' DeSerialize_XML_ToSSTLinks(IO.Path.Combine(mPath, "xml_SSTLinks.xml"))
        SSTOthers = Read_SSTOthers() ' DeSerialize_XML_ToSSTOthers(IO.Path.Combine(mPath, "xml_Others.xml"))
    End Sub

    Sub New(Secs As List(Of SecInfo))
        DesignCode = "AISC 360-16"
        DCRLimits = New DCRLimits(True)
        MaterialInfo = New MaterialInfo(True)
        If Not Secs Is Nothing AndAlso Secs.Count > 0 Then
            AllWsections = New AllWsections()
            For Each item As SecInfo In Secs
                If AllWsections.Wsections.Find(Function(x) x.Size = item.Size) Is Nothing Then
                    AllWsections.Wsections.Add(New Wsec(item.Size, item.d, item.d_det, item.bf_det, item.tw, item.tf_det)) ' tmpArr(3), tmpArr(4), tmpArr(5)))
                End If
            Next
        Else
            AllWsections = Read_AllWsections() ' DeSerialize_XML_ToAllWsections(IO.Path.Combine(mPath, "xml_AllWsections.xml"))
        End If
        SSTLinks = Read_SSTLinks() ' DeSerialize_XML_ToSSTLinks(IO.Path.Combine(mPath, "xml_SSTLinks.xml"))
        SSTOthers = Read_SSTOthers() ' DeSerialize_XML_ToSSTOthers(IO.Path.Combine(mPath, "xml_Others.xml"))
    End Sub


    ''Sub New(_versionID As String)
    ''    versionID = "Version ID: " & _versionID
    ''End Sub
    Public Function StringToWeldingOption(value As String) As WeldingOption
        Return CType([Enum].Parse(GetType(WeldingOption), value, ignoreCase:=True), WeldingOption)
    End Function

End Class

Public Class MaterialInfo
    Public Bm_Fu As Double
    Public Bm_Fy As Double
    '
    Public Col_Fu As Double
    Public Col_Fy As Double
    '
    Public ShearPlate_Fu As Double
    Public ShearPlate_Fy As Double
    ''Public ShearPlate_DefThk As Double
    '
    Public StiffPL_Fu As Double
    Public StiffPL_Fy As Double
    Public StiffPL_DefThk As Double
    Public StiffPL_1SidedConn As StiffStyle
    '
    Public DoublerPL_Fu As Double
    Public DoublerPL_Fy As Double
    Public DoublerPL_DefThk As Double
    Sub New()
    End Sub
    Sub New(isDefault As Boolean)
        If isDefault Then
            Bm_Fy = 55 : Bm_Fu = 70
            '
            Col_Fy = 55 : Col_Fu = 70
            '
            ShearPlate_Fy = g50 : ShearPlate_Fu = g65
            '
            StiffPL_Fy = g50 : StiffPL_Fu = g65
            StiffPL_1SidedConn = StiffStyle.Full_Depth
            '
            DoublerPL_Fy = g50 : DoublerPL_Fu = g65
            '
            ''ShearPlate_DefThk = 0.375
            StiffPL_DefThk = 0.25 'Updated 0.375
            DoublerPL_DefThk = 0.25
        End If
    End Sub
End Class
Public Class WeldingInfo
    Public DoublerPL2ColWeb As WeldingOption
    Public DoublerPL2ColFLG As WeldingOption
    Public UsePlug_weld As Boolean
    Sub New()
    End Sub
    Sub New(init As Boolean)
        If init Then
            DoublerPL2ColWeb = WeldingOption.Option_2A
            DoublerPL2ColFLG = WeldingOption.Option_2
            UsePlug_weld = False
        End If
    End Sub
    Sub New(_DoublerPL2ColWeb As WeldingOption, _DoublerPL2ColFLG As WeldingOption, _UsePlug_weld As Boolean)
        DoublerPL2ColWeb = _DoublerPL2ColWeb
        DoublerPL2ColFLG = _DoublerPL2ColFLG
        UsePlug_weld = _UsePlug_weld
    End Sub

End Class

Public Class sstSlabDepth
    Public StoryID As String
    Public StoryElevation As Double
    Public SlabDepth As Double
    Sub New()
    End Sub
    Sub New(_StoryID As String, _StoryElevation As Double, _SlabDepth As Double)
        StoryID = _StoryID
        StoryElevation = _StoryElevation
        SlabDepth = _SlabDepth
    End Sub
End Class
Public Class clsDataRow
    Public Col1 As String
    Public Col2 As String
    Public Col3 As String
    '
    Sub New()
    End Sub
    Sub New(_Col1 As String, _Col2 As String, _Col3 As String)
        Col1 = _Col1
        Col2 = _Col2
        Col3 = _Col3
    End Sub
End Class
Public Enum WeldingOption
    Option_1
    Option_2A
    Option_2B
    Option_2
End Enum
Public Enum ARType
    T0_SumReport
    T1_ILS
    T2_BLC
    T3_CC
    T4_SPC
End Enum

Public Enum DriftLimit As Integer
    Hx10 = 0
    Hx15 = 1
    Hx20 = 2
    Hx25 = 3
    Other
End Enum

Public Enum WDriftLimit As Integer
    Hx050
    Hx100
    Hx175
    Hx200
    Hx300
    Hx400
    Hx500
End Enum

Public Enum R_Cd
    R80Cd55
    R65Cd40
    Other
End Enum

<Serializable()> Public Class DCRLimits
    Public tbf_check As Double
    Public bf_check As Double
    Public Lyield_check As Double
    Public Panel_Zone_DCR As Double
    Public Drift_check As Double
    Public Beam_tf_DCR As Double
    Public Link_strength_DCR As Double
    Public t_BRP_DCR As Double
    Public BRP_Bolt_DCR As Double
    Public SCWB_DCR As Double
    Public Column_Flange_DCR As Double
    Public Stiffener_DCR As Double
    Public Beam_Web_DCR As Double
    Public Shear_Plate_DCR As Double
    Public Bolt_DCR As Double
    Public Fillet_weld_DCR As Double
    '
    Public Max As Double
    Public Min As Double
    Public DriftLimitSelect As DriftLimit
    Public WindDriftLimitSelect As WDriftLimit
    Public DivideByRho As Boolean

    Sub New()
    End Sub
    Sub New(init As Boolean)
        If init Then
            tbf_check = 1
            bf_check = 1
            Lyield_check = 1.0
            Panel_Zone_DCR = 1
            Drift_check = 1.0
            Beam_tf_DCR = 1
            Link_strength_DCR = 1.0
            t_BRP_DCR = 1
            BRP_Bolt_DCR = 1.0
            SCWB_DCR = 1
            Column_Flange_DCR = 1
            Stiffener_DCR = 1
            Beam_Web_DCR = 1
            Shear_Plate_DCR = 1
            Bolt_DCR = 1
            Fillet_weld_DCR = 1
            '
            Max = 1.05
            Min = 0.9 'Updated
            '
            DriftLimitSelect = DriftLimit.Hx20
            WindDriftLimitSelect = WDriftLimit.Hx500
            'PlasticDriftLimit = 0
        End If
    End Sub

End Class