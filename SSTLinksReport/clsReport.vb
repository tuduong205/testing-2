﻿Imports YieldLinkLib
Imports GrapeCity.ActiveReports.Document
Imports GrapeCity.ActiveReports.Export.Pdf.Section


Public Class clsReport

    Private job As clsMRSJob
    Private filePath As String
    Private typ As ARType
    '
    Sub New()
    End Sub
    Sub New(_job As clsMRSJob, _filePath As String, _typ As ARType)
        job = _job
        filePath = _filePath
        typ = _typ
    End Sub

    Public Function Export() As String
        Dim pdfExp As New PdfExport
        Try

            If typ = ARType.T0_SumReport Then
                    Dim srAll_US As New US_Sum0H(job)
                    srAll_US.PageSettings.Margins.Left = 0
                    srAll_US.PageSettings.Margins.Top = 0
                    srAll_US.PageSettings.Margins.Right = 0
                    srAll_US.PageSettings.Margins.Bottom = 0
                    srAll_US.PageSettings.Orientation = Section.PageOrientation.Landscape
                    srAll_US.Run()
                    '
                    Dim sr0_US As New US_Sum0V(job, srAll_US.Document.Pages.Count)
                    sr0_US.PageSettings.Margins.Left = 0
                    sr0_US.PageSettings.Margins.Top = 0
                    sr0_US.PageSettings.Margins.Right = 0
                    sr0_US.PageSettings.Margins.Bottom = 0
                    sr0_US.PageSettings.Orientation = Section.PageOrientation.Portrait
                    sr0_US.Run()
                    '
                    srAll_US.Document.Pages.InsertRange(0, sr0_US.Document.Pages)
                    '
                    Dim totalpages As Integer
                    Dim str As String
                    '
                    totalpages = srAll_US.Document.Pages.Count
                    For i = 0 To totalpages - 1
                        str = String.Format("Page {0} of " + totalpages.ToString(), i + 1)
                        Dim size = New SizeF()
                        Dim tempfont = srAll_US.Document.Pages(i).Font
                        Dim fs = New FontStyle()


                        Dim ft = New Font("Arial", 8.25F, New FontStyle())
                        srAll_US.Document.Pages(i).Font = ft
                        srAll_US.Document.Pages(i).ForeColor = System.Drawing.Color.Black
                        size = srAll_US.Document.Pages(i).MeasureParagraphHeight(str, srAll_US.Document.Pages(i).Width, srAll_US.Document.Pages(i).Font, StringFormat.GenericTypographic)
                        Dim x, y As Single
                        x = (srAll_US.Document.Pages(i).Width - size.Width - 0.5)
                        y = (srAll_US.Document.Pages(i).Height - 0.4875) ' srAll.Document.Pages(i).Margins.Bottom)
                        '
                        srAll_US.Document.Pages(i).DrawText(str, x, y, size.Width, 0.2)
                        srAll_US.Document.Pages(i).Font = tempfont
                        tempfont = Nothing
                        ft = Nothing
                    Next
                    '
                    pdfExp.Export(srAll_US.Document, filePath)
                Else
                    Dim sr0_US As New US_CoverDetailed(job, typ)
                    sr0_US.PageSettings.Margins.Left = 0
                    sr0_US.PageSettings.Margins.Top = 0
                    sr0_US.PageSettings.Margins.Right = 0
                    sr0_US.PageSettings.Margins.Bottom = 0
                    sr0_US.Run()
                    pdfExp.Export(sr0_US.Document, filePath)
                End If
            '
            Return "Done! The pdf file opened in separate window."
        Catch ex As Exception
            ''MsgBox(ex.Message & ex.StackTrace)
            Return "Error: " & ex.Message.Split(vbNewLine)(0)

        Finally
            pdfExp.Dispose()
        End Try
    End Function

End Class
