﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_SPC4
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_SPC4))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S48_068 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_039 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_044 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_052 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_053 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture5 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_061 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_060 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_059 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label279 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label277 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label273 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label271 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label274 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label288 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture3 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label289 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label290 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label292 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label293 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label295 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label296 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label298 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label299 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label301 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label302 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label304 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label305 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label307 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label308 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label310 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label311 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label313 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label314 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label316 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label317 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label319 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label320 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label322 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label323 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label325 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label326 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label327 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label329 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label330 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label332 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label333 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label335 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label336 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label338 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label339 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label341 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label342 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label344 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label345 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label347 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label348 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label350 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label351 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label353 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label354 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label356 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label357 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label359 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture4 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label360 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label361 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label363 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label364 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label366 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label367 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label369 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label370 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label372 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label373 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label375 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label376 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label378 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label379 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label381 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label382 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label384 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label385 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label387 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label388 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label389 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label390 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label392 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label393 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_034 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label395 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label396 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_035 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label398 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label399 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_036 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label401 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label402 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_037 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label404 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label405 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_038 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label407 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label408 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label410 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label411 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label412 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_040 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label414 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label415 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_041 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label417 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label418 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_042 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label420 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label421 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_043 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label423 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label424 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label425 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label426 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label428 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label429 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label430 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_045 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label432 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label433 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_046 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label435 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label436 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_047 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label438 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label439 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_048 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label441 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label442 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_049 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label444 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label445 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_050 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label447 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label448 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label450 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_051 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label452 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label453 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label454 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label455 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label456 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label458 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label459 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label460 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_054 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label462 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label463 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_055 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label465 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label466 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_056 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label468 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label469 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_057 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label471 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label472 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_058 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label495 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label496 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label498 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_067 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label500 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.label111 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.label112 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.label113 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_073 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_069 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_071 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_074 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_070 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_072 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_075 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_066 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_065 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_064 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_063 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_062 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape5 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape8 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape11 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape10 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape9 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape12 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape13 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_076 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S48_077 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape14 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.S48_068, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_039, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_044, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_052, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_053, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_061, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_060, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_059, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label277, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label274, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label288, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label289, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label290, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label292, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label293, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label295, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label296, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label298, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label299, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label301, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label302, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label304, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label305, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label307, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label308, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label310, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label311, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label313, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label314, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label316, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label317, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label319, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label320, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label322, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label323, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label325, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label326, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label327, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label329, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label330, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label332, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label333, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label335, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label336, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label338, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label339, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label341, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label342, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label344, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label345, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label347, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label348, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label350, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label351, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label353, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label354, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label356, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label357, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label359, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label360, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label361, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label363, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label364, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label366, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label367, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label369, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label370, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label372, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label373, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label375, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label376, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label378, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label379, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label381, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label382, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label384, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label385, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label387, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label388, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label389, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label390, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_033, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label392, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label393, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_034, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label395, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label396, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_035, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label398, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label399, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_036, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label401, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label402, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_037, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label404, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label405, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_038, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label407, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label408, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label410, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label411, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label412, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_040, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label414, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label415, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_041, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label417, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label418, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_042, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label420, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label421, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_043, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label423, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label424, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label425, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label426, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label428, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label429, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label430, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_045, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label432, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label433, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_046, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label435, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label436, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_047, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label438, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label439, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_048, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label441, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label442, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_049, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label444, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label445, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_050, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label447, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label448, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label450, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_051, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label452, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label453, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label454, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label455, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label456, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label458, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label459, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label460, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_054, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label462, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label463, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_055, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label465, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label466, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_056, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label468, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label469, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_057, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label471, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label472, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_058, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label495, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label496, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label498, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_067, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label500, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.label111, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.label112, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.label113, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_073, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_069, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_071, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_074, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_070, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_072, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_075, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_066, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_065, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_064, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_063, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_062, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_076, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S48_077, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S48_068, Me.S48_012, Me.S48_023, Me.S48_032, Me.S48_039, Me.S48_044, Me.S48_052, Me.S48_053, Me.Picture5, Me.Label16, Me.Label15, Me.Label14, Me.Label1, Me.S48_061, Me.S48_060, Me.Label2, Me.S48_059, Me.Label279, Me.Label277, Me.Label273, Me.Label271, Me.Label274, Me.Label288, Me.Picture3, Me.Label289, Me.Label290, Me.S48_001, Me.Label292, Me.Label293, Me.S48_002, Me.Label295, Me.Label296, Me.S48_003, Me.Label298, Me.Label299, Me.S48_004, Me.Label301, Me.Label302, Me.S48_005, Me.Label304, Me.Label305, Me.S48_006, Me.Label307, Me.Label308, Me.S48_007, Me.Label310, Me.Label311, Me.S48_008, Me.Label313, Me.Label314, Me.S48_009, Me.Label316, Me.Label317, Me.S48_010, Me.Label319, Me.Label320, Me.S48_011, Me.Label322, Me.Label323, Me.Label325, Me.Label326, Me.Label327, Me.S48_013, Me.Label329, Me.Label330, Me.S48_014, Me.Label332, Me.Label333, Me.S48_015, Me.Label335, Me.Label336, Me.S48_016, Me.Label338, Me.Label339, Me.S48_017, Me.Label341, Me.Label342, Me.S48_018, Me.Label344, Me.Label345, Me.S48_019, Me.Label347, Me.Label348, Me.S48_020, Me.Label350, Me.Label351, Me.S48_021, Me.Label353, Me.Label354, Me.S48_022, Me.Label356, Me.Label357, Me.Label359, Me.Picture4, Me.Label360, Me.Label361, Me.S48_024, Me.Label363, Me.Label364, Me.S48_025, Me.Label366, Me.Label367, Me.S48_026, Me.Label369, Me.Label370, Me.S48_027, Me.Label372, Me.Label373, Me.S48_028, Me.Label375, Me.Label376, Me.S48_029, Me.Label378, Me.Label379, Me.S48_030, Me.Label381, Me.Label382, Me.S48_031, Me.Label384, Me.Label385, Me.Label387, Me.Label388, Me.Label389, Me.Label390, Me.S48_033, Me.Label392, Me.Label393, Me.S48_034, Me.Label395, Me.Label396, Me.S48_035, Me.Label398, Me.Label399, Me.S48_036, Me.Label401, Me.Label402, Me.S48_037, Me.Label404, Me.Label405, Me.S48_038, Me.Label407, Me.Label408, Me.Label410, Me.Label411, Me.Label412, Me.S48_040, Me.Label414, Me.Label415, Me.S48_041, Me.Label417, Me.Label418, Me.S48_042, Me.Label420, Me.Label421, Me.S48_043, Me.Label423, Me.Label424, Me.Label425, Me.Label426, Me.Label428, Me.Label429, Me.Label430, Me.S48_045, Me.Label432, Me.Label433, Me.S48_046, Me.Label435, Me.Label436, Me.S48_047, Me.Label438, Me.Label439, Me.S48_048, Me.Label441, Me.Label442, Me.S48_049, Me.Label444, Me.Label445, Me.S48_050, Me.Label447, Me.Label448, Me.Label450, Me.S48_051, Me.Label452, Me.Label453, Me.Label454, Me.Label455, Me.Label456, Me.Label458, Me.Label459, Me.Label460, Me.S48_054, Me.Label462, Me.Label463, Me.S48_055, Me.Label465, Me.Label466, Me.S48_056, Me.Label468, Me.Label469, Me.S48_057, Me.Label471, Me.Label472, Me.S48_058, Me.Label495, Me.Label496, Me.Label498, Me.S48_067, Me.Label500, Me.label111, Me.label112, Me.label113, Me.Line1, Me.Line2, Me.Label3, Me.Label4, Me.S48_073, Me.S48_069, Me.S48_071, Me.S48_074, Me.S48_070, Me.S48_072, Me.S48_075, Me.Line3, Me.Line4, Me.Line5, Me.Line6, Me.Line7, Me.Line8, Me.Line9, Me.Line10, Me.Line11, Me.Label17, Me.Label18, Me.S48_066, Me.Label20, Me.Label21, Me.S48_065, Me.Label23, Me.Label24, Me.S48_064, Me.Label26, Me.Label27, Me.S48_063, Me.Label29, Me.Label30, Me.S48_062, Me.Shape5, Me.Shape8, Me.Shape11, Me.Shape10, Me.Shape9, Me.Shape12, Me.Shape13, Me.Label5, Me.Label6, Me.S48_076, Me.Label8, Me.Label9, Me.S48_077, Me.Shape14, Me.PageBreak1})
        Me.Detail.Height = 21.45833!
        Me.Detail.Name = "Detail"
        '
        'S48_068
        '
        Me.S48_068.Height = 0.1875!
        Me.S48_068.HyperLink = Nothing
        Me.S48_068.Left = 2.25!
        Me.S48_068.Name = "S48_068"
        Me.S48_068.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_068.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_068.Top = 21.228!
        Me.S48_068.Width = 1.0!
        '
        'S48_012
        '
        Me.S48_012.Height = 0.1875!
        Me.S48_012.HyperLink = Nothing
        Me.S48_012.Left = 2.25!
        Me.S48_012.Name = "S48_012"
        Me.S48_012.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_012.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_012.Top = 4.360998!
        Me.S48_012.Width = 1.0!
        '
        'S48_023
        '
        Me.S48_023.Height = 0.1875!
        Me.S48_023.HyperLink = Nothing
        Me.S48_023.Left = 2.25!
        Me.S48_023.Name = "S48_023"
        Me.S48_023.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_023.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_023.Top = 6.915998!
        Me.S48_023.Width = 1.0!
        '
        'S48_032
        '
        Me.S48_032.Height = 0.1875!
        Me.S48_032.HyperLink = Nothing
        Me.S48_032.Left = 2.25!
        Me.S48_032.Name = "S48_032"
        Me.S48_032.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_032.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_032.Top = 10.42002!
        Me.S48_032.Width = 1.0!
        '
        'S48_039
        '
        Me.S48_039.Height = 0.1875!
        Me.S48_039.HyperLink = Nothing
        Me.S48_039.Left = 2.25!
        Me.S48_039.Name = "S48_039"
        Me.S48_039.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_039.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_039.Top = 11.924!
        Me.S48_039.Width = 1.0!
        '
        'S48_044
        '
        Me.S48_044.Height = 0.1875!
        Me.S48_044.HyperLink = Nothing
        Me.S48_044.Left = 2.25!
        Me.S48_044.Name = "S48_044"
        Me.S48_044.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_044.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_044.Top = 15.097!
        Me.S48_044.Width = 1.0!
        '
        'S48_052
        '
        Me.S48_052.Height = 0.1875!
        Me.S48_052.HyperLink = Nothing
        Me.S48_052.Left = 2.25!
        Me.S48_052.Name = "S48_052"
        Me.S48_052.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_052.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_052.Top = 16.915!
        Me.S48_052.Width = 1.0!
        '
        'S48_053
        '
        Me.S48_053.Height = 0.1875!
        Me.S48_053.HyperLink = Nothing
        Me.S48_053.Left = 2.25!
        Me.S48_053.Name = "S48_053"
        Me.S48_053.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S48_053.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_053.Top = 17.477!
        Me.S48_053.Width = 1.0!
        '
        'Picture5
        '
        Me.Picture5.Height = 1.554001!
        Me.Picture5.HyperLink = Nothing
        Me.Picture5.ImageData = CType(resources.GetObject("Picture5.ImageData"), System.IO.Stream)
        Me.Picture5.Left = 0.0000002384186!
        Me.Picture5.Name = "Picture5"
        Me.Picture5.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture5.Top = 12.416!
        Me.Picture5.Width = 8.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 1.187!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "La_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label16.Top = 19.665!
        Me.Label16.Width = 1.062!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 1.187!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label15.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label15.Top = 19.47701!
        Me.Label15.Width = 1.062!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 1.125!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label14.Text = "max θ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label14.Top = 19.289!
        Me.Label14.Width = 1.061!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 1.125!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "min. θ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 19.10201!
        Me.Label1.Width = 1.061!
        '
        'S48_061
        '
        Me.S48_061.Height = 0.1875!
        Me.S48_061.HyperLink = Nothing
        Me.S48_061.Left = 2.25!
        Me.S48_061.Name = "S48_061"
        Me.S48_061.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_061.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_061.Top = 19.66501!
        Me.S48_061.Width = 1.0!
        '
        'S48_060
        '
        Me.S48_060.Height = 0.1875!
        Me.S48_060.HyperLink = Nothing
        Me.S48_060.Left = 2.25!
        Me.S48_060.Name = "S48_060"
        Me.S48_060.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_060.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_060.Top = 19.47701!
        Me.S48_060.Width = 1.0!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 2.25!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "color: Black; font-family: Symbol; font-size: 8.25pt; font-weight: normal; text-a" &
    "lign: center; vertical-align: middle; ddo-char-set: 2"
        Me.Label2.Text = "q1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 19.289!
        Me.Label2.Width = 1.0!
        '
        'S48_059
        '
        Me.S48_059.Height = 0.1875!
        Me.S48_059.HyperLink = Nothing
        Me.S48_059.Left = 2.25!
        Me.S48_059.Name = "S48_059"
        Me.S48_059.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_059.Text = "0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_059.Top = 19.102!
        Me.S48_059.Width = 1.0!
        '
        'Label279
        '
        Me.Label279.Height = 0.1875!
        Me.Label279.HyperLink = Nothing
        Me.Label279.Left = 5.25!
        Me.Label279.Name = "Label279"
        Me.Label279.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label279.Text = "CASE 4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label279.Top = 18.914!
        Me.Label279.Width = 1.0!
        '
        'Label277
        '
        Me.Label277.Height = 0.1875!
        Me.Label277.HyperLink = Nothing
        Me.Label277.Left = 4.25!
        Me.Label277.Name = "Label277"
        Me.Label277.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label277.Text = "CASE 3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label277.Top = 18.914!
        Me.Label277.Width = 1.0!
        '
        'Label273
        '
        Me.Label273.Height = 0.1875!
        Me.Label273.HyperLink = Nothing
        Me.Label273.Left = 3.25!
        Me.Label273.Name = "Label273"
        Me.Label273.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label273.Text = "CASE 2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label273.Top = 18.914!
        Me.Label273.Width = 1.0!
        '
        'Label271
        '
        Me.Label271.Height = 0.1875!
        Me.Label271.HyperLink = Nothing
        Me.Label271.Left = 2.25!
        Me.Label271.Name = "Label271"
        Me.Label271.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label271.Text = "CASE 1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label271.Top = 18.914!
        Me.Label271.Width = 1.0!
        '
        'Label274
        '
        Me.Label274.Height = 0.1875!
        Me.Label274.HyperLink = Nothing
        Me.Label274.Left = 0!
        Me.Label274.Name = "Label274"
        Me.Label274.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label274.Text = "4.8 BEAM WEB AND SHEAR TAB BEARING:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label274.Top = 0!
        Me.Label274.Width = 8.0!
        '
        'Label288
        '
        Me.Label288.Height = 0.1875!
        Me.Label288.HyperLink = Nothing
        Me.Label288.Left = 0.25!
        Me.Label288.Name = "Label288"
        Me.Label288.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label288.Text = "CASE 1: HORIZONTAL REACTIONS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label288.Top = 0.187!
        Me.Label288.Width = 7.75!
        '
        'Picture3
        '
        Me.Picture3.Height = 1.437!
        Me.Picture3.HyperLink = Nothing
        Me.Picture3.ImageData = CType(resources.GetObject("Picture3.ImageData"), System.IO.Stream)
        Me.Picture3.Left = 0!
        Me.Picture3.Name = "Picture3"
        Me.Picture3.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture3.Top = 0.375!
        Me.Picture3.Width = 7.75!
        '
        'Label289
        '
        Me.Label289.Height = 0.188!
        Me.Label289.HyperLink = Nothing
        Me.Label289.Left = 0.5!
        Me.Label289.Name = "Label289"
        Me.Label289.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label289.Text = "BEAM WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label289.Top = 1.916!
        Me.Label289.Width = 5.0!
        '
        'Label290
        '
        Me.Label290.Height = 0.1875!
        Me.Label290.HyperLink = Nothing
        Me.Label290.Left = 0!
        Me.Label290.Name = "Label290"
        Me.Label290.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label290.Text = "Φbolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label290.Top = 2.103!
        Me.Label290.Width = 2.25!
        '
        'S48_001
        '
        Me.S48_001.Height = 0.1875!
        Me.S48_001.HyperLink = Nothing
        Me.S48_001.Left = 2.25!
        Me.S48_001.Name = "S48_001"
        Me.S48_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_001.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_001.Top = 2.103!
        Me.S48_001.Width = 1.0!
        '
        'Label292
        '
        Me.Label292.Height = 0.1875!
        Me.Label292.HyperLink = Nothing
        Me.Label292.Left = 0!
        Me.Label292.Name = "Label292"
        Me.Label292.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label292.Text = "tbw=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label292.Top = 2.291002!
        Me.Label292.Width = 2.25!
        '
        'Label293
        '
        Me.Label293.Height = 0.1875!
        Me.Label293.HyperLink = Nothing
        Me.Label293.Left = 3.25!
        Me.Label293.Name = "Label293"
        Me.Label293.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label293.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label293.Top = 2.291002!
        Me.Label293.Width = 4.75!
        '
        'S48_002
        '
        Me.S48_002.Height = 0.1875!
        Me.S48_002.HyperLink = Nothing
        Me.S48_002.Left = 2.25!
        Me.S48_002.Name = "S48_002"
        Me.S48_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S48_002.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_002.Top = 2.292001!
        Me.S48_002.Width = 1.0!
        '
        'Label295
        '
        Me.Label295.Height = 0.1875!
        Me.Label295.HyperLink = Nothing
        Me.Label295.Left = 0!
        Me.Label295.Name = "Label295"
        Me.Label295.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label295.Text = "Lb_edge=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label295.Top = 2.48!
        Me.Label295.Width = 2.25!
        '
        'Label296
        '
        Me.Label296.Height = 0.1875!
        Me.Label296.HyperLink = Nothing
        Me.Label296.Left = 3.25!
        Me.Label296.Name = "Label296"
        Me.Label296.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label296.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Edge distance for beam web bolt hole" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label296.Top = 2.48!
        Me.Label296.Width = 4.75!
        '
        'S48_003
        '
        Me.S48_003.Height = 0.1875!
        Me.S48_003.HyperLink = Nothing
        Me.S48_003.Left = 2.25!
        Me.S48_003.Name = "S48_003"
        Me.S48_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S48_003.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_003.Top = 2.481!
        Me.S48_003.Width = 1.0!
        '
        'Label298
        '
        Me.Label298.Height = 0.1875!
        Me.Label298.HyperLink = Nothing
        Me.Label298.Left = 0!
        Me.Label298.Name = "Label298"
        Me.Label298.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label298.Text = "Lcw1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label298.Top = 2.668!
        Me.Label298.Width = 2.25!
        '
        'Label299
        '
        Me.Label299.Height = 0.1875!
        Me.Label299.HyperLink = Nothing
        Me.Label299.Left = 3.25!
        Me.Label299.Name = "Label299"
        Me.Label299.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label299.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lb_edge - (db_sp + 1/16)* 0.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label299.Top = 2.668!
        Me.Label299.Width = 4.75!
        '
        'S48_004
        '
        Me.S48_004.Height = 0.1875!
        Me.S48_004.HyperLink = Nothing
        Me.S48_004.Left = 2.25!
        Me.S48_004.Name = "S48_004"
        Me.S48_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_004.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_004.Top = 2.669!
        Me.S48_004.Width = 1.0!
        '
        'Label301
        '
        Me.Label301.Height = 0.1875!
        Me.Label301.HyperLink = Nothing
        Me.Label301.Left = 0!
        Me.Label301.Name = "Label301"
        Me.Label301.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label301.Text = "Lcw2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label301.Top = 2.856!
        Me.Label301.Width = 2.25!
        '
        'Label302
        '
        Me.Label302.Height = 0.1875!
        Me.Label302.HyperLink = Nothing
        Me.Label302.Left = 3.25!
        Me.Label302.Name = "Label302"
        Me.Label302.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label302.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Shorz - (db_sp + 1/16)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label302.Top = 2.856!
        Me.Label302.Width = 4.75!
        '
        'S48_005
        '
        Me.S48_005.Height = 0.1875!
        Me.S48_005.HyperLink = Nothing
        Me.S48_005.Left = 2.25!
        Me.S48_005.Name = "S48_005"
        Me.S48_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_005.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_005.Top = 2.857!
        Me.S48_005.Width = 1.0!
        '
        'Label304
        '
        Me.Label304.Height = 0.1875!
        Me.Label304.HyperLink = Nothing
        Me.Label304.Left = 0!
        Me.Label304.Name = "Label304"
        Me.Label304.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label304.Text = "Lcw3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label304.Top = 3.044001!
        Me.Label304.Width = 2.25!
        '
        'Label305
        '
        Me.Label305.Height = 0.1875!
        Me.Label305.HyperLink = Nothing
        Me.Label305.Left = 3.25!
        Me.Label305.Name = "Label305"
        Me.Label305.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label305.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (n_Hbot_SST = 2, 0, Lcw2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label305.Top = 3.044001!
        Me.Label305.Width = 4.75!
        '
        'S48_006
        '
        Me.S48_006.Height = 0.1875!
        Me.S48_006.HyperLink = Nothing
        Me.S48_006.Left = 2.25!
        Me.S48_006.Name = "S48_006"
        Me.S48_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_006.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_006.Top = 3.045!
        Me.S48_006.Width = 1.0!
        '
        'Label307
        '
        Me.Label307.Height = 0.1875!
        Me.Label307.HyperLink = Nothing
        Me.Label307.Left = 0!
        Me.Label307.Name = "Label307"
        Me.Label307.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label307.Text = "Lc_bmWeb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label307.Top = 3.417!
        Me.Label307.Width = 2.25!
        '
        'Label308
        '
        Me.Label308.Height = 0.1875!
        Me.Label308.HyperLink = Nothing
        Me.Label308.Left = 3.25!
        Me.Label308.Name = "Label308"
        Me.Label308.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label308.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lcw1 + Lcw2 + Lcw3 + Lcw4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label308.Top = 3.417!
        Me.Label308.Width = 4.75!
        '
        'S48_007
        '
        Me.S48_007.Height = 0.1875!
        Me.S48_007.HyperLink = Nothing
        Me.S48_007.Left = 2.25!
        Me.S48_007.Name = "S48_007"
        Me.S48_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_007.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_007.Top = 3.418!
        Me.S48_007.Width = 1.0!
        '
        'Label310
        '
        Me.Label310.Height = 0.1875!
        Me.Label310.HyperLink = Nothing
        Me.Label310.Left = 0!
        Me.Label310.Name = "Label310"
        Me.Label310.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label310.Text = "ΦRn_beamWeb1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label310.Top = 3.605994!
        Me.Label310.Width = 2.25!
        '
        'Label311
        '
        Me.Label311.Height = 0.1875!
        Me.Label311.HyperLink = Nothing
        Me.Label311.Left = 3.25!
        Me.Label311.Name = "Label311"
        Me.Label311.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label311.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 1.2* Lc_bmWeb* tbw* Fu_bmWeb" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label311.Top = 3.605994!
        Me.Label311.Width = 4.75!
        '
        'S48_008
        '
        Me.S48_008.Height = 0.1875!
        Me.S48_008.HyperLink = Nothing
        Me.S48_008.Left = 2.25!
        Me.S48_008.Name = "S48_008"
        Me.S48_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_008.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_008.Top = 3.606997!
        Me.S48_008.Width = 1.0!
        '
        'Label313
        '
        Me.Label313.Height = 0.1875!
        Me.Label313.HyperLink = Nothing
        Me.Label313.Left = 0!
        Me.Label313.Name = "Label313"
        Me.Label313.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label313.Text = "ΦRn_beamWeb2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label313.Top = 3.794993!
        Me.Label313.Width = 2.25!
        '
        'Label314
        '
        Me.Label314.Height = 0.1875!
        Me.Label314.HyperLink = Nothing
        Me.Label314.Left = 3.25!
        Me.Label314.Name = "Label314"
        Me.Label314.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label314.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 2.4* db_sp* tbw* Fu_bmWeb *n_Hbolt" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label314.Top = 3.794993!
        Me.Label314.Width = 4.75!
        '
        'S48_009
        '
        Me.S48_009.Height = 0.1875!
        Me.S48_009.HyperLink = Nothing
        Me.S48_009.Left = 2.25!
        Me.S48_009.Name = "S48_009"
        Me.S48_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_009.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_009.Top = 3.795997!
        Me.S48_009.Width = 1.0!
        '
        'Label316
        '
        Me.Label316.Height = 0.1875!
        Me.Label316.HyperLink = Nothing
        Me.Label316.Left = 0!
        Me.Label316.Name = "Label316"
        Me.Label316.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label316.Text = "ΦRn_beamWeb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label316.Top = 3.982998!
        Me.Label316.Width = 2.25!
        '
        'Label317
        '
        Me.Label317.Height = 0.1875!
        Me.Label317.HyperLink = Nothing
        Me.Label317.Left = 3.25!
        Me.Label317.Name = "Label317"
        Me.Label317.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label317.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= min (ΦRn_beamWeb1, ΦRn_beamWeb2)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label317.Top = 3.982998!
        Me.Label317.Width = 4.75!
        '
        'S48_010
        '
        Me.S48_010.Height = 0.1875!
        Me.S48_010.HyperLink = Nothing
        Me.S48_010.Left = 2.25!
        Me.S48_010.Name = "S48_010"
        Me.S48_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_010.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_010.Top = 3.983997!
        Me.S48_010.Width = 1.0!
        '
        'Label319
        '
        Me.Label319.Height = 0.1875!
        Me.Label319.HyperLink = Nothing
        Me.Label319.Left = 0!
        Me.Label319.Name = "Label319"
        Me.Label319.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label319.Text = "SUM_bmWebX=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label319.Top = 4.171014!
        Me.Label319.Width = 2.25!
        '
        'Label320
        '
        Me.Label320.Height = 0.1875!
        Me.Label320.HyperLink = Nothing
        Me.Label320.Left = 3.75!
        Me.Label320.Name = "Label320"
        Me.Label320.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label320.Text = " = Pu_SST/ ΦRn_beamWeb" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label320.Top = 4.172998!
        Me.Label320.Width = 4.25!
        '
        'S48_011
        '
        Me.S48_011.Height = 0.1875!
        Me.S48_011.HyperLink = Nothing
        Me.S48_011.Left = 2.25!
        Me.S48_011.Name = "S48_011"
        Me.S48_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_011.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_011.Top = 4.171997!
        Me.S48_011.Width = 1.0!
        '
        'Label322
        '
        Me.Label322.Height = 0.1875!
        Me.Label322.HyperLink = Nothing
        Me.Label322.Left = 0!
        Me.Label322.Name = "Label322"
        Me.Label322.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label322.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label322.Top = 4.360998!
        Me.Label322.Width = 2.25!
        '
        'Label323
        '
        Me.Label323.Height = 0.1875!
        Me.Label323.HyperLink = Nothing
        Me.Label323.Left = 3.75!
        Me.Label323.Name = "Label323"
        Me.Label323.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label323.Text = "OK if SUM_spWeld <= SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label323.Top = 4.360998!
        Me.Label323.Width = 4.25!
        '
        'Label325
        '
        Me.Label325.Height = 0.1875!
        Me.Label325.HyperLink = Nothing
        Me.Label325.Left = 0.5000002!
        Me.Label325.Name = "Label325"
        Me.Label325.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label325.Text = "SHEAR PLATE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label325.Top = 4.654997!
        Me.Label325.Width = 7.75!
        '
        'Label326
        '
        Me.Label326.Height = 0.1875!
        Me.Label326.HyperLink = Nothing
        Me.Label326.Left = 0.0000002384186!
        Me.Label326.Name = "Label326"
        Me.Label326.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label326.Text = "tsp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label326.Top = 4.841998!
        Me.Label326.Width = 2.25!
        '
        'Label327
        '
        Me.Label327.Height = 0.1875!
        Me.Label327.HyperLink = Nothing
        Me.Label327.Left = 3.25!
        Me.Label327.Name = "Label327"
        Me.Label327.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label327.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label327.Top = 4.841998!
        Me.Label327.Width = 4.75!
        '
        'S48_013
        '
        Me.S48_013.Height = 0.1875!
        Me.S48_013.HyperLink = Nothing
        Me.S48_013.Left = 2.25!
        Me.S48_013.Name = "S48_013"
        Me.S48_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S48_013.Text = " 5/8" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_013.Top = 4.842997!
        Me.S48_013.Width = 1.0!
        '
        'Label329
        '
        Me.Label329.Height = 0.1875!
        Me.Label329.HyperLink = Nothing
        Me.Label329.Left = 0.0000002384186!
        Me.Label329.Name = "Label329"
        Me.Label329.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label329.Text = "Lb_edge=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label329.Top = 5.029999!
        Me.Label329.Width = 2.25!
        '
        'Label330
        '
        Me.Label330.Height = 0.1875!
        Me.Label330.HyperLink = Nothing
        Me.Label330.Left = 3.25!
        Me.Label330.Name = "Label330"
        Me.Label330.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label330.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Edge distance for beam web bolt hole" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label330.Top = 5.029999!
        Me.Label330.Width = 4.75!
        '
        'S48_014
        '
        Me.S48_014.Height = 0.1875!
        Me.S48_014.HyperLink = Nothing
        Me.S48_014.Left = 2.25!
        Me.S48_014.Name = "S48_014"
        Me.S48_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S48_014.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_014.Top = 5.030998!
        Me.S48_014.Width = 1.0!
        '
        'Label332
        '
        Me.Label332.Height = 0.1875!
        Me.Label332.HyperLink = Nothing
        Me.Label332.Left = 0.0000002384186!
        Me.Label332.Name = "Label332"
        Me.Label332.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label332.Text = "Lc_sp1 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label332.Top = 5.217999!
        Me.Label332.Width = 2.25!
        '
        'Label333
        '
        Me.Label333.Height = 0.1875!
        Me.Label333.HyperLink = Nothing
        Me.Label333.Left = 3.25!
        Me.Label333.Name = "Label333"
        Me.Label333.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label333.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Sp_edgeR - (db_sp + 1/16)* 0.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label333.Top = 5.217999!
        Me.Label333.Width = 4.75!
        '
        'S48_015
        '
        Me.S48_015.Height = 0.1875!
        Me.S48_015.HyperLink = Nothing
        Me.S48_015.Left = 2.25!
        Me.S48_015.Name = "S48_015"
        Me.S48_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_015.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_015.Top = 5.218999!
        Me.S48_015.Width = 1.0!
        '
        'Label335
        '
        Me.Label335.Height = 0.1875!
        Me.Label335.HyperLink = Nothing
        Me.Label335.Left = 0.0000002384186!
        Me.Label335.Name = "Label335"
        Me.Label335.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label335.Text = "Lc_sp2 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label335.Top = 5.406!
        Me.Label335.Width = 2.25!
        '
        'Label336
        '
        Me.Label336.Height = 0.1875!
        Me.Label336.HyperLink = Nothing
        Me.Label336.Left = 3.25!
        Me.Label336.Name = "Label336"
        Me.Label336.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label336.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Shorz - (db_sp + 1/16)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label336.Top = 5.406!
        Me.Label336.Width = 4.75!
        '
        'S48_016
        '
        Me.S48_016.Height = 0.1875!
        Me.S48_016.HyperLink = Nothing
        Me.S48_016.Left = 2.25!
        Me.S48_016.Name = "S48_016"
        Me.S48_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_016.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_016.Top = 5.406999!
        Me.S48_016.Width = 1.0!
        '
        'Label338
        '
        Me.Label338.Height = 0.1875!
        Me.Label338.HyperLink = Nothing
        Me.Label338.Left = 0.0000002384186!
        Me.Label338.Name = "Label338"
        Me.Label338.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label338.Text = "Lc_sp3 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label338.Top = 5.594!
        Me.Label338.Width = 2.25!
        '
        'Label339
        '
        Me.Label339.Height = 0.1875!
        Me.Label339.HyperLink = Nothing
        Me.Label339.Left = 3.25!
        Me.Label339.Name = "Label339"
        Me.Label339.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label339.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (n_Hbot_SST = 2, 0, Lc_sp2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label339.Top = 5.594!
        Me.Label339.Width = 4.75!
        '
        'S48_017
        '
        Me.S48_017.Height = 0.1875!
        Me.S48_017.HyperLink = Nothing
        Me.S48_017.Left = 2.25!
        Me.S48_017.Name = "S48_017"
        Me.S48_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_017.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_017.Top = 5.594999!
        Me.S48_017.Width = 1.0!
        '
        'Label341
        '
        Me.Label341.Height = 0.1875!
        Me.Label341.HyperLink = Nothing
        Me.Label341.Left = 0.0000002384186!
        Me.Label341.Name = "Label341"
        Me.Label341.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label341.Text = "Lc_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label341.Top = 5.969!
        Me.Label341.Width = 2.25!
        '
        'Label342
        '
        Me.Label342.Height = 0.1875!
        Me.Label342.HyperLink = Nothing
        Me.Label342.Left = 3.25!
        Me.Label342.Name = "Label342"
        Me.Label342.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label342.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lc_sp1 + Lc_sp2 + Lc_sp3 + Lc_sp4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label342.Top = 5.969!
        Me.Label342.Width = 4.75!
        '
        'S48_018
        '
        Me.S48_018.Height = 0.1875!
        Me.S48_018.HyperLink = Nothing
        Me.S48_018.Left = 2.25!
        Me.S48_018.Name = "S48_018"
        Me.S48_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_018.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_018.Top = 5.97!
        Me.S48_018.Width = 1.0!
        '
        'Label344
        '
        Me.Label344.Height = 0.1875!
        Me.Label344.HyperLink = Nothing
        Me.Label344.Left = 0.0000002384186!
        Me.Label344.Name = "Label344"
        Me.Label344.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label344.Text = "ΦRn_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label344.Top = 6.157998!
        Me.Label344.Width = 2.25!
        '
        'Label345
        '
        Me.Label345.Height = 0.1875!
        Me.Label345.HyperLink = Nothing
        Me.Label345.Left = 3.25!
        Me.Label345.Name = "Label345"
        Me.Label345.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label345.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 1.2* Lc_sp* tbw* Fu_bmWeb" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label345.Top = 6.157998!
        Me.Label345.Width = 4.75!
        '
        'S48_019
        '
        Me.S48_019.Height = 0.1875!
        Me.S48_019.HyperLink = Nothing
        Me.S48_019.Left = 2.25!
        Me.S48_019.Name = "S48_019"
        Me.S48_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_019.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_019.Top = 6.159001!
        Me.S48_019.Width = 1.0!
        '
        'Label347
        '
        Me.Label347.Height = 0.1875!
        Me.Label347.HyperLink = Nothing
        Me.Label347.Left = 0.0000002384186!
        Me.Label347.Name = "Label347"
        Me.Label347.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label347.Text = "ΦRn_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label347.Top = 6.346999!
        Me.Label347.Width = 2.25!
        '
        'Label348
        '
        Me.Label348.Height = 0.1875!
        Me.Label348.HyperLink = Nothing
        Me.Label348.Left = 3.25!
        Me.Label348.Name = "Label348"
        Me.Label348.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label348.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 2.4* db_sp* tbw* Fu_bmWeb*n_Hbolt" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label348.Top = 6.346999!
        Me.Label348.Width = 4.75!
        '
        'S48_020
        '
        Me.S48_020.Height = 0.1875!
        Me.S48_020.HyperLink = Nothing
        Me.S48_020.Left = 2.25!
        Me.S48_020.Name = "S48_020"
        Me.S48_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_020.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_020.Top = 6.348001!
        Me.S48_020.Width = 1.0!
        '
        'Label350
        '
        Me.Label350.Height = 0.1875!
        Me.Label350.HyperLink = Nothing
        Me.Label350.Left = 0.0000002384186!
        Me.Label350.Name = "Label350"
        Me.Label350.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label350.Text = "ΦRn_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label350.Top = 6.535998!
        Me.Label350.Width = 2.25!
        '
        'Label351
        '
        Me.Label351.Height = 0.1875!
        Me.Label351.HyperLink = Nothing
        Me.Label351.Left = 3.25!
        Me.Label351.Name = "Label351"
        Me.Label351.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label351.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= min (ΦRn_sp1, ΦRn_sp2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label351.Top = 6.535998!
        Me.Label351.Width = 4.75!
        '
        'S48_021
        '
        Me.S48_021.Height = 0.1875!
        Me.S48_021.HyperLink = Nothing
        Me.S48_021.Left = 2.25!
        Me.S48_021.Name = "S48_021"
        Me.S48_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_021.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_021.Top = 6.536997!
        Me.S48_021.Width = 1.0!
        '
        'Label353
        '
        Me.Label353.Height = 0.1875!
        Me.Label353.HyperLink = Nothing
        Me.Label353.Left = 0.0000002384186!
        Me.Label353.Name = "Label353"
        Me.Label353.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label353.Text = "SUM_spX=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label353.Top = 6.72402!
        Me.Label353.Width = 2.25!
        '
        'Label354
        '
        Me.Label354.Height = 0.1875!
        Me.Label354.HyperLink = Nothing
        Me.Label354.Left = 3.75!
        Me.Label354.Name = "Label354"
        Me.Label354.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label354.Text = "= Pu_SST/ ΦRn_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label354.Top = 6.725996!
        Me.Label354.Width = 4.25!
        '
        'S48_022
        '
        Me.S48_022.Height = 0.1875!
        Me.S48_022.HyperLink = Nothing
        Me.S48_022.Left = 2.25!
        Me.S48_022.Name = "S48_022"
        Me.S48_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_022.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_022.Top = 6.724998!
        Me.S48_022.Width = 1.0!
        '
        'Label356
        '
        Me.Label356.Height = 0.1875!
        Me.Label356.HyperLink = Nothing
        Me.Label356.Left = 0.0000002384186!
        Me.Label356.Name = "Label356"
        Me.Label356.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label356.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label356.Top = 6.915998!
        Me.Label356.Width = 2.25!
        '
        'Label357
        '
        Me.Label357.Height = 0.1875!
        Me.Label357.HyperLink = Nothing
        Me.Label357.Left = 3.75!
        Me.Label357.Name = "Label357"
        Me.Label357.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label357.Text = "OK if SUM_spX <= SUM_Allowed"
        Me.Label357.Top = 6.915998!
        Me.Label357.Width = 4.25!
        '
        'Label359
        '
        Me.Label359.Height = 0.1875!
        Me.Label359.HyperLink = Nothing
        Me.Label359.Left = 0.2500002!
        Me.Label359.Name = "Label359"
        Me.Label359.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label359.Text = "CASE 2: VERTICAL REACTIONS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label359.Top = 7.165998!
        Me.Label359.Width = 7.75!
        '
        'Picture4
        '
        Me.Picture4.Height = 1.374!
        Me.Picture4.HyperLink = Nothing
        Me.Picture4.ImageData = CType(resources.GetObject("Picture4.ImageData"), System.IO.Stream)
        Me.Picture4.Left = 0.0000002384186!
        Me.Picture4.Name = "Picture4"
        Me.Picture4.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture4.Top = 7.353999!
        Me.Picture4.Width = 7.75!
        '
        'Label360
        '
        Me.Label360.Height = 0.1875!
        Me.Label360.HyperLink = Nothing
        Me.Label360.Left = 0.0000002384186!
        Me.Label360.Name = "Label360"
        Me.Label360.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label360.Text = "Φbolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label360.Top = 8.915998!
        Me.Label360.Width = 2.25!
        '
        'Label361
        '
        Me.Label361.Height = 0.1875!
        Me.Label361.HyperLink = Nothing
        Me.Label361.Left = 3.25!
        Me.Label361.Name = "Label361"
        Me.Label361.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label361.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label361.Top = 8.915998!
        Me.Label361.Width = 4.75!
        '
        'S48_024
        '
        Me.S48_024.Height = 0.1875!
        Me.S48_024.HyperLink = Nothing
        Me.S48_024.Left = 2.25!
        Me.S48_024.Name = "S48_024"
        Me.S48_024.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_024.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_024.Top = 8.916998!
        Me.S48_024.Width = 1.0!
        '
        'Label363
        '
        Me.Label363.Height = 0.1875!
        Me.Label363.HyperLink = Nothing
        Me.Label363.Left = 0.0000002384186!
        Me.Label363.Name = "Label363"
        Me.Label363.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label363.Text = "Lc1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label363.Top = 9.103998!
        Me.Label363.Width = 2.25!
        '
        'Label364
        '
        Me.Label364.Height = 0.1875!
        Me.Label364.HyperLink = Nothing
        Me.Label364.Left = 3.25!
        Me.Label364.Name = "Label364"
        Me.Label364.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label364.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Svert - (db_sp + 1/16)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label364.Top = 9.103998!
        Me.Label364.Width = 4.75!
        '
        'S48_025
        '
        Me.S48_025.Height = 0.1875!
        Me.S48_025.HyperLink = Nothing
        Me.S48_025.Left = 2.25!
        Me.S48_025.Name = "S48_025"
        Me.S48_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_025.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_025.Top = 9.104998!
        Me.S48_025.Width = 1.0!
        '
        'Label366
        '
        Me.Label366.Height = 0.1875!
        Me.Label366.HyperLink = Nothing
        Me.Label366.Left = 0.0000002384186!
        Me.Label366.Name = "Label366"
        Me.Label366.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label366.Text = "Lc2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label366.Top = 9.291998!
        Me.Label366.Width = 2.25!
        '
        'Label367
        '
        Me.Label367.Height = 0.1875!
        Me.Label367.HyperLink = Nothing
        Me.Label367.Left = 3.25!
        Me.Label367.Name = "Label367"
        Me.Label367.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label367.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (db - (n_Vbolts - 1)*Svert - 2tbf)*0.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label367.Top = 9.291998!
        Me.Label367.Width = 4.75!
        '
        'S48_026
        '
        Me.S48_026.Height = 0.1875!
        Me.S48_026.HyperLink = Nothing
        Me.S48_026.Left = 2.25!
        Me.S48_026.Name = "S48_026"
        Me.S48_026.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_026.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_026.Top = 9.292998!
        Me.S48_026.Width = 1.0!
        '
        'Label369
        '
        Me.Label369.Height = 0.1875!
        Me.Label369.HyperLink = Nothing
        Me.Label369.Left = 0.0000002384186!
        Me.Label369.Name = "Label369"
        Me.Label369.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label369.Text = "Lcb_Vert=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label369.Top = 9.478998!
        Me.Label369.Width = 2.25!
        '
        'Label370
        '
        Me.Label370.Height = 0.1875!
        Me.Label370.HyperLink = Nothing
        Me.Label370.Left = 3.25!
        Me.Label370.Name = "Label370"
        Me.Label370.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label370.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lc2 + (n_Vbolts - 1)*Lc1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label370.Top = 9.478998!
        Me.Label370.Width = 4.75!
        '
        'S48_027
        '
        Me.S48_027.Height = 0.1875!
        Me.S48_027.HyperLink = Nothing
        Me.S48_027.Left = 2.25!
        Me.S48_027.Name = "S48_027"
        Me.S48_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_027.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_027.Top = 9.479988!
        Me.S48_027.Width = 1.0!
        '
        'Label372
        '
        Me.Label372.Height = 0.1875!
        Me.Label372.HyperLink = Nothing
        Me.Label372.Left = 0.0000002384186!
        Me.Label372.Name = "Label372"
        Me.Label372.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label372.Text = " Φ*Rn_bmWebY1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label372.Top = 9.667007!
        Me.Label372.Width = 2.25!
        '
        'Label373
        '
        Me.Label373.Height = 0.1875!
        Me.Label373.HyperLink = Nothing
        Me.Label373.Left = 3.25!
        Me.Label373.Name = "Label373"
        Me.Label373.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label373.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 1.2* Lcb_Vert* tbw* Fu_bm" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label373.Top = 9.667007!
        Me.Label373.Width = 4.75!
        '
        'S48_028
        '
        Me.S48_028.Height = 0.1875!
        Me.S48_028.HyperLink = Nothing
        Me.S48_028.Left = 2.25!
        Me.S48_028.Name = "S48_028"
        Me.S48_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_028.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_028.Top = 9.668008!
        Me.S48_028.Width = 1.0!
        '
        'Label375
        '
        Me.Label375.Height = 0.1875!
        Me.Label375.HyperLink = Nothing
        Me.Label375.Left = 0.0000002384186!
        Me.Label375.Name = "Label375"
        Me.Label375.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label375.Text = " Φ*Rn_bmWebY2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label375.Top = 9.854988!
        Me.Label375.Width = 2.25!
        '
        'Label376
        '
        Me.Label376.Height = 0.1875!
        Me.Label376.HyperLink = Nothing
        Me.Label376.Left = 3.25!
        Me.Label376.Name = "Label376"
        Me.Label376.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label376.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 2.4* (db_sp* n_bolts_SST)* tbw* Fu_bm" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label376.Top = 9.854988!
        Me.Label376.Width = 4.75!
        '
        'S48_029
        '
        Me.S48_029.Height = 0.1875!
        Me.S48_029.HyperLink = Nothing
        Me.S48_029.Left = 2.25!
        Me.S48_029.Name = "S48_029"
        Me.S48_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_029.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_029.Top = 9.855998!
        Me.S48_029.Width = 1.0!
        '
        'Label378
        '
        Me.Label378.Height = 0.1875!
        Me.Label378.HyperLink = Nothing
        Me.Label378.Left = 0.0000002384186!
        Me.Label378.Name = "Label378"
        Me.Label378.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label378.Text = " Φ*Rn_bmWebY=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label378.Top = 10.04301!
        Me.Label378.Width = 2.25!
        '
        'Label379
        '
        Me.Label379.Height = 0.1875!
        Me.Label379.HyperLink = Nothing
        Me.Label379.Left = 3.25!
        Me.Label379.Name = "Label379"
        Me.Label379.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label379.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= min (Φ*Rn_bmWebY1, Φ*Rn_bmWebY2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label379.Top = 10.04301!
        Me.Label379.Width = 4.75!
        '
        'S48_030
        '
        Me.S48_030.Height = 0.1875!
        Me.S48_030.HyperLink = Nothing
        Me.S48_030.Left = 2.25!
        Me.S48_030.Name = "S48_030"
        Me.S48_030.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_030.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_030.Top = 10.044!
        Me.S48_030.Width = 1.0!
        '
        'Label381
        '
        Me.Label381.Height = 0.1875!
        Me.Label381.HyperLink = Nothing
        Me.Label381.Left = 0.0000002384186!
        Me.Label381.Name = "Label381"
        Me.Label381.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label381.Text = "SUM_bmWebY=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label381.Top = 10.23101!
        Me.Label381.Width = 2.25!
        '
        'Label382
        '
        Me.Label382.Height = 0.1875!
        Me.Label382.HyperLink = Nothing
        Me.Label382.Left = 3.25!
        Me.Label382.Name = "Label382"
        Me.Label382.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label382.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm / (Φ*Rn_bmWebY)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label382.Top = 10.23299!
        Me.Label382.Width = 4.75!
        '
        'S48_031
        '
        Me.S48_031.Height = 0.1875!
        Me.S48_031.HyperLink = Nothing
        Me.S48_031.Left = 2.25!
        Me.S48_031.Name = "S48_031"
        Me.S48_031.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_031.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_031.Top = 10.232!
        Me.S48_031.Width = 1.0!
        '
        'Label384
        '
        Me.Label384.Height = 0.1875!
        Me.Label384.HyperLink = Nothing
        Me.Label384.Left = 0.0000002384186!
        Me.Label384.Name = "Label384"
        Me.Label384.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label384.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label384.Top = 10.42002!
        Me.Label384.Width = 2.25!
        '
        'Label385
        '
        Me.Label385.Height = 0.1875!
        Me.Label385.HyperLink = Nothing
        Me.Label385.Left = 3.75!
        Me.Label385.Name = "Label385"
        Me.Label385.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label385.Text = "OK if SUM_spWeld <= SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label385.Top = 10.42002!
        Me.Label385.Width = 4.25!
        '
        'Label387
        '
        Me.Label387.Height = 0.1875!
        Me.Label387.HyperLink = Nothing
        Me.Label387.Left = 0.5000002!
        Me.Label387.Name = "Label387"
        Me.Label387.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label387.Text = "BEAM WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label387.Top = 8.728998!
        Me.Label387.Width = 7.75!
        '
        'Label388
        '
        Me.Label388.Height = 0.1875!
        Me.Label388.HyperLink = Nothing
        Me.Label388.Left = 0.5000002!
        Me.Label388.Name = "Label388"
        Me.Label388.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label388.Text = "SHEAR PLATE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label388.Top = 10.609!
        Me.Label388.Width = 7.75!
        '
        'Label389
        '
        Me.Label389.Height = 0.1875!
        Me.Label389.HyperLink = Nothing
        Me.Label389.Left = 0.0000002384186!
        Me.Label389.Name = "Label389"
        Me.Label389.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label389.Text = "Lc3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label389.Top = 10.801!
        Me.Label389.Width = 2.25!
        '
        'Label390
        '
        Me.Label390.Height = 0.1875!
        Me.Label390.HyperLink = Nothing
        Me.Label390.Left = 3.25!
        Me.Label390.Name = "Label390"
        Me.Label390.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label390.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lv_sp - (db_sp + 1/16)* 0.5" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label390.Top = 10.801!
        Me.Label390.Width = 4.75!
        '
        'S48_033
        '
        Me.S48_033.Height = 0.1875!
        Me.S48_033.HyperLink = Nothing
        Me.S48_033.Left = 2.25!
        Me.S48_033.Name = "S48_033"
        Me.S48_033.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_033.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_033.Top = 10.80201!
        Me.S48_033.Width = 1.0!
        '
        'Label392
        '
        Me.Label392.Height = 0.1875!
        Me.Label392.HyperLink = Nothing
        Me.Label392.Left = 0.0000002384186!
        Me.Label392.Name = "Label392"
        Me.Label392.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label392.Text = "Lct_Vert=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label392.Top = 10.99!
        Me.Label392.Width = 2.25!
        '
        'Label393
        '
        Me.Label393.Height = 0.1875!
        Me.Label393.HyperLink = Nothing
        Me.Label393.Left = 3.25!
        Me.Label393.Name = "Label393"
        Me.Label393.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label393.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lc4 + (n_Vbolts_SST - 1)*Lc1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label393.Top = 10.99!
        Me.Label393.Width = 4.75!
        '
        'S48_034
        '
        Me.S48_034.Height = 0.1875!
        Me.S48_034.HyperLink = Nothing
        Me.S48_034.Left = 2.25!
        Me.S48_034.Name = "S48_034"
        Me.S48_034.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_034.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_034.Top = 10.99099!
        Me.S48_034.Width = 1.0!
        '
        'Label395
        '
        Me.Label395.Height = 0.1875!
        Me.Label395.HyperLink = Nothing
        Me.Label395.Left = 0.0000002384186!
        Me.Label395.Name = "Label395"
        Me.Label395.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label395.Text = " Φ*Rn_spY1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label395.Top = 11.178!
        Me.Label395.Width = 2.25!
        '
        'Label396
        '
        Me.Label396.Height = 0.1875!
        Me.Label396.HyperLink = Nothing
        Me.Label396.Left = 3.25!
        Me.Label396.Name = "Label396"
        Me.Label396.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label396.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 1.2* Lct_Vert* tsp* Fu_shearTab" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label396.Top = 11.178!
        Me.Label396.Width = 4.75!
        '
        'S48_035
        '
        Me.S48_035.Height = 0.1875!
        Me.S48_035.HyperLink = Nothing
        Me.S48_035.Left = 2.25!
        Me.S48_035.Name = "S48_035"
        Me.S48_035.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_035.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_035.Top = 11.17902!
        Me.S48_035.Width = 1.0!
        '
        'Label398
        '
        Me.Label398.Height = 0.1875!
        Me.Label398.HyperLink = Nothing
        Me.Label398.Left = 0.0000002384186!
        Me.Label398.Name = "Label398"
        Me.Label398.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label398.Text = " Φ*Rn_spY2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label398.Top = 11.367!
        Me.Label398.Width = 2.25!
        '
        'Label399
        '
        Me.Label399.Height = 0.1875!
        Me.Label399.HyperLink = Nothing
        Me.Label399.Left = 3.25!
        Me.Label399.Name = "Label399"
        Me.Label399.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label399.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 2.4* db_sp* n_bolts_SST* tsp* Fu_shearTab" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label399.Top = 11.367!
        Me.Label399.Width = 4.75!
        '
        'S48_036
        '
        Me.S48_036.Height = 0.1875!
        Me.S48_036.HyperLink = Nothing
        Me.S48_036.Left = 2.25!
        Me.S48_036.Name = "S48_036"
        Me.S48_036.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_036.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_036.Top = 11.36802!
        Me.S48_036.Width = 1.0!
        '
        'Label401
        '
        Me.Label401.Height = 0.1875!
        Me.Label401.HyperLink = Nothing
        Me.Label401.Left = 0.0000002384186!
        Me.Label401.Name = "Label401"
        Me.Label401.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label401.Text = " Φ*Rn_spY=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label401.Top = 11.555!
        Me.Label401.Width = 2.25!
        '
        'Label402
        '
        Me.Label402.Height = 0.1875!
        Me.Label402.HyperLink = Nothing
        Me.Label402.Left = 3.25!
        Me.Label402.Name = "Label402"
        Me.Label402.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label402.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= min (Φ*Rn_spY1, Φ*Rn_spY2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label402.Top = 11.555!
        Me.Label402.Width = 4.75!
        '
        'S48_037
        '
        Me.S48_037.Height = 0.1875!
        Me.S48_037.HyperLink = Nothing
        Me.S48_037.Left = 2.25!
        Me.S48_037.Name = "S48_037"
        Me.S48_037.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_037.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_037.Top = 11.556!
        Me.S48_037.Width = 1.0!
        '
        'Label404
        '
        Me.Label404.Height = 0.1875!
        Me.Label404.HyperLink = Nothing
        Me.Label404.Left = 0.0000002384186!
        Me.Label404.Name = "Label404"
        Me.Label404.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label404.Text = "SUM_spY=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label404.Top = 11.74302!
        Me.Label404.Width = 2.25!
        '
        'Label405
        '
        Me.Label405.Height = 0.1875!
        Me.Label405.HyperLink = Nothing
        Me.Label405.Left = 3.75!
        Me.Label405.Name = "Label405"
        Me.Label405.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label405.Text = "= Vu_bm/ (Φ*Rn_spY)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label405.Top = 11.745!
        Me.Label405.Width = 4.25!
        '
        'S48_038
        '
        Me.S48_038.Height = 0.1875!
        Me.S48_038.HyperLink = Nothing
        Me.S48_038.Left = 2.25!
        Me.S48_038.Name = "S48_038"
        Me.S48_038.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_038.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_038.Top = 11.744!
        Me.S48_038.Width = 1.0!
        '
        'Label407
        '
        Me.Label407.Height = 0.1875!
        Me.Label407.HyperLink = Nothing
        Me.Label407.Left = 0.0000002384186!
        Me.Label407.Name = "Label407"
        Me.Label407.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label407.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label407.Top = 11.924!
        Me.Label407.Width = 2.25!
        '
        'Label408
        '
        Me.Label408.Height = 0.1875!
        Me.Label408.HyperLink = Nothing
        Me.Label408.Left = 3.75!
        Me.Label408.Name = "Label408"
        Me.Label408.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label408.Text = "OK if SUM_spY <= SUM_Allowed"
        Me.Label408.Top = 11.924!
        Me.Label408.Width = 4.25!
        '
        'Label410
        '
        Me.Label410.Height = 0.1875!
        Me.Label410.HyperLink = Nothing
        Me.Label410.Left = 0.2500002!
        Me.Label410.Name = "Label410"
        Me.Label410.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label410.Text = "CASE 3: COMBINED AXIAL AND VERTICAL REACTIONS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label410.Top = 12.229!
        Me.Label410.Width = 7.75!
        '
        'Label411
        '
        Me.Label411.Height = 0.1875!
        Me.Label411.HyperLink = Nothing
        Me.Label411.Left = 0.0000002384186!
        Me.Label411.Name = "Label411"
        Me.Label411.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label411.Text = "Px=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label411.Top = 13.969!
        Me.Label411.Width = 2.25!
        '
        'Label412
        '
        Me.Label412.Height = 0.1875!
        Me.Label412.HyperLink = Nothing
        Me.Label412.Left = 3.25!
        Me.Label412.Name = "Label412"
        Me.Label412.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label412.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_SST/ n_Hbolts_SST" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label412.Top = 13.969!
        Me.Label412.Width = 4.75!
        '
        'S48_040
        '
        Me.S48_040.Height = 0.1875!
        Me.S48_040.HyperLink = Nothing
        Me.S48_040.Left = 2.25!
        Me.S48_040.Name = "S48_040"
        Me.S48_040.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S48_040.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_040.Top = 13.97!
        Me.S48_040.Width = 1.0!
        '
        'Label414
        '
        Me.Label414.Height = 0.1875!
        Me.Label414.HyperLink = Nothing
        Me.Label414.Left = 0.0000002384186!
        Me.Label414.Name = "Label414"
        Me.Label414.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label414.Text = "Py=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label414.Top = 14.157!
        Me.Label414.Width = 2.25!
        '
        'Label415
        '
        Me.Label415.Height = 0.1875!
        Me.Label415.HyperLink = Nothing
        Me.Label415.Left = 3.25!
        Me.Label415.Name = "Label415"
        Me.Label415.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label415.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm/ n_Vbolts_SST" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label415.Top = 14.157!
        Me.Label415.Width = 4.75!
        '
        'S48_041
        '
        Me.S48_041.Height = 0.1875!
        Me.S48_041.HyperLink = Nothing
        Me.S48_041.Left = 2.25!
        Me.S48_041.Name = "S48_041"
        Me.S48_041.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S48_041.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_041.Top = 14.158!
        Me.S48_041.Width = 1.0!
        '
        'Label417
        '
        Me.Label417.Height = 0.1875!
        Me.Label417.HyperLink = Nothing
        Me.Label417.Left = 0.0000002384186!
        Me.Label417.Name = "Label417"
        Me.Label417.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label417.Text = "Resultant (Pr)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label417.Top = 14.345!
        Me.Label417.Width = 2.25!
        '
        'Label418
        '
        Me.Label418.Height = 0.1875!
        Me.Label418.HyperLink = Nothing
        Me.Label418.Left = 3.25!
        Me.Label418.Name = "Label418"
        Me.Label418.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label418.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Sqrt (Px^2 + Py^2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label418.Top = 14.345!
        Me.Label418.Width = 4.75!
        '
        'S48_042
        '
        Me.S48_042.Height = 0.1875!
        Me.S48_042.HyperLink = Nothing
        Me.S48_042.Left = 2.25!
        Me.S48_042.Name = "S48_042"
        Me.S48_042.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_042.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_042.Top = 14.346!
        Me.S48_042.Width = 1.0!
        '
        'Label420
        '
        Me.Label420.Height = 0.1875!
        Me.Label420.HyperLink = Nothing
        Me.Label420.Left = 0.0000002384186!
        Me.Label420.Name = "Label420"
        Me.Label420.Style = "font-family: Arial; font-size: 8.25pt; text-align: right; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label420.Text = "θ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label420.Top = 14.534!
        Me.Label420.Width = 2.25!
        '
        'Label421
        '
        Me.Label421.Height = 0.1875!
        Me.Label421.HyperLink = Nothing
        Me.Label421.Left = 3.25!
        Me.Label421.Name = "Label421"
        Me.Label421.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label421.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "= min(Atan (Py/ Px),1.571))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label421.Top = 14.534!
        Me.Label421.Width = 4.75!
        '
        'S48_043
        '
        Me.S48_043.Height = 0.1875!
        Me.S48_043.HyperLink = Nothing
        Me.S48_043.Left = 2.25!
        Me.S48_043.Name = "S48_043"
        Me.S48_043.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_043.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_043.Top = 14.53499!
        Me.S48_043.Width = 1.0!
        '
        'Label423
        '
        Me.Label423.Height = 0.188!
        Me.Label423.HyperLink = Nothing
        Me.Label423.Left = 0.4999999!
        Me.Label423.Name = "Label423"
        Me.Label423.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label423.Text = "BEAM WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label423.Top = 14.722!
        Me.Label423.Width = 1.5!
        '
        'Label424
        '
        Me.Label424.Height = 0.1875!
        Me.Label424.HyperLink = Nothing
        Me.Label424.Left = 0.9999998!
        Me.Label424.Name = "Label424"
        Me.Label424.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label424.Text = "Method 1: T and V circular interaction" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label424.Top = 14.909!
        Me.Label424.Width = 8.323!
        '
        'Label425
        '
        Me.Label425.Height = 0.1875!
        Me.Label425.HyperLink = Nothing
        Me.Label425.Left = 2.842171E-14!
        Me.Label425.Name = "Label425"
        Me.Label425.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label425.Text = "SUM_bmWebθBearing1 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label425.Top = 15.097!
        Me.Label425.Width = 2.187!
        '
        'Label426
        '
        Me.Label426.Height = 0.1875!
        Me.Label426.HyperLink = Nothing
        Me.Label426.Left = 3.75!
        Me.Label426.Name = "Label426"
        Me.Label426.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label426.Text = "= SUM_bmWebX^2 + SUM_bmWebY^2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label426.Top = 15.097!
        Me.Label426.Width = 4.25!
        '
        'Label428
        '
        Me.Label428.Height = 0.1875!
        Me.Label428.HyperLink = Nothing
        Me.Label428.Left = 1.0!
        Me.Label428.Name = "Label428"
        Me.Label428.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label428.Text = "Method 2: Bearing in a diagonal line" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label428.Top = 15.409!
        Me.Label428.Width = 8.323!
        '
        'Label429
        '
        Me.Label429.Height = 0.1875!
        Me.Label429.HyperLink = Nothing
        Me.Label429.Left = 0.0000002384186!
        Me.Label429.Name = "Label429"
        Me.Label429.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label429.Text = "Lvg_web =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label429.Top = 15.596!
        Me.Label429.Width = 2.25!
        '
        'Label430
        '
        Me.Label430.Height = 0.1875!
        Me.Label430.HyperLink = Nothing
        Me.Label430.Left = 3.25!
        Me.Label430.Name = "Label430"
        Me.Label430.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label430.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lb_edge/ (cosθ)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label430.Top = 15.596!
        Me.Label430.Width = 4.75!
        '
        'S48_045
        '
        Me.S48_045.Height = 0.1875!
        Me.S48_045.HyperLink = Nothing
        Me.S48_045.Left = 2.25!
        Me.S48_045.Name = "S48_045"
        Me.S48_045.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap"
        Me.S48_045.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_045.Top = 15.59699!
        Me.S48_045.Width = 1.0!
        '
        'Label432
        '
        Me.Label432.Height = 0.1875!
        Me.Label432.HyperLink = Nothing
        Me.Label432.Left = 0.0000002384186!
        Me.Label432.Name = "Label432"
        Me.Label432.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label432.Text = "Lc_web =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label432.Top = 15.784!
        Me.Label432.Width = 2.25!
        '
        'Label433
        '
        Me.Label433.Height = 0.1875!
        Me.Label433.HyperLink = Nothing
        Me.Label433.Left = 3.25!
        Me.Label433.Name = "Label433"
        Me.Label433.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label433.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_web - dHole_sp/2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label433.Top = 15.784!
        Me.Label433.Width = 4.75!
        '
        'S48_046
        '
        Me.S48_046.Height = 0.1875!
        Me.S48_046.HyperLink = Nothing
        Me.S48_046.Left = 2.25!
        Me.S48_046.Name = "S48_046"
        Me.S48_046.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap"
        Me.S48_046.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_046.Top = 15.78498!
        Me.S48_046.Width = 1.0!
        '
        'Label435
        '
        Me.Label435.Height = 0.1875!
        Me.Label435.HyperLink = Nothing
        Me.Label435.Left = 0.0000002384186!
        Me.Label435.Name = "Label435"
        Me.Label435.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label435.Text = " Φ*Rn_bmWebθ1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label435.Top = 15.97202!
        Me.Label435.Width = 2.25!
        '
        'Label436
        '
        Me.Label436.Height = 0.1875!
        Me.Label436.HyperLink = Nothing
        Me.Label436.Left = 3.25!
        Me.Label436.Name = "Label436"
        Me.Label436.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label436.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 1.2* Lc_web* tbw* Fu_bmWeb" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label436.Top = 15.97202!
        Me.Label436.Width = 4.75!
        '
        'S48_047
        '
        Me.S48_047.Height = 0.1875!
        Me.S48_047.HyperLink = Nothing
        Me.S48_047.Left = 2.25!
        Me.S48_047.Name = "S48_047"
        Me.S48_047.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap"
        Me.S48_047.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_047.Top = 15.97301!
        Me.S48_047.Width = 1.0!
        '
        'Label438
        '
        Me.Label438.Height = 0.1875!
        Me.Label438.HyperLink = Nothing
        Me.Label438.Left = 0.0000002384186!
        Me.Label438.Name = "Label438"
        Me.Label438.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label438.Text = " Φ*Rn_bmWebθ2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label438.Top = 16.161!
        Me.Label438.Width = 2.25!
        '
        'Label439
        '
        Me.Label439.Height = 0.1875!
        Me.Label439.HyperLink = Nothing
        Me.Label439.Left = 3.25!
        Me.Label439.Name = "Label439"
        Me.Label439.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label439.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 2.4* db_sp* tbw* Fu_bmWeb*n_Hbolt" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label439.Top = 16.161!
        Me.Label439.Width = 4.75!
        '
        'S48_048
        '
        Me.S48_048.Height = 0.1875!
        Me.S48_048.HyperLink = Nothing
        Me.S48_048.Left = 2.25!
        Me.S48_048.Name = "S48_048"
        Me.S48_048.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_048.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_048.Top = 16.16199!
        Me.S48_048.Width = 1.0!
        '
        'Label441
        '
        Me.Label441.Height = 0.1875!
        Me.Label441.HyperLink = Nothing
        Me.Label441.Left = 0.0000002384186!
        Me.Label441.Name = "Label441"
        Me.Label441.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label441.Text = " Φ*Rn_bmWebθ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label441.Top = 16.34902!
        Me.Label441.Width = 2.25!
        '
        'Label442
        '
        Me.Label442.Height = 0.1875!
        Me.Label442.HyperLink = Nothing
        Me.Label442.Left = 3.25!
        Me.Label442.Name = "Label442"
        Me.Label442.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label442.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= min (Φ*Rn_bmWebθ1, Φ*Rn_bmWebθ2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label442.Top = 16.34902!
        Me.Label442.Width = 4.75!
        '
        'S48_049
        '
        Me.S48_049.Height = 0.1875!
        Me.S48_049.HyperLink = Nothing
        Me.S48_049.Left = 2.25!
        Me.S48_049.Name = "S48_049"
        Me.S48_049.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_049.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_049.Top = 16.35002!
        Me.S48_049.Width = 1.0!
        '
        'Label444
        '
        Me.Label444.Height = 0.1875!
        Me.Label444.HyperLink = Nothing
        Me.Label444.Left = 0.0000002384186!
        Me.Label444.Name = "Label444"
        Me.Label444.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label444.Text = "SUM_bmWebθBearing2 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label444.Top = 16.538!
        Me.Label444.Width = 2.25!
        '
        'Label445
        '
        Me.Label445.Height = 0.1875!
        Me.Label445.HyperLink = Nothing
        Me.Label445.Left = 3.25!
        Me.Label445.Name = "Label445"
        Me.Label445.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label445.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pr/ Φ*Rn_bmWebθ" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label445.Top = 16.538!
        Me.Label445.Width = 4.75!
        '
        'S48_050
        '
        Me.S48_050.Height = 0.1875!
        Me.S48_050.HyperLink = Nothing
        Me.S48_050.Left = 2.25!
        Me.S48_050.Name = "S48_050"
        Me.S48_050.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_050.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_050.Top = 16.539!
        Me.S48_050.Width = 1.0!
        '
        'Label447
        '
        Me.Label447.Height = 0.1875!
        Me.Label447.HyperLink = Nothing
        Me.Label447.Left = 0.0000002384186!
        Me.Label447.Name = "Label447"
        Me.Label447.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label447.Text = "SUM_bmWebθBearing =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label447.Top = 16.727!
        Me.Label447.Width = 2.25!
        '
        'Label448
        '
        Me.Label448.Height = 0.1875!
        Me.Label448.HyperLink = Nothing
        Me.Label448.Left = 3.75!
        Me.Label448.Name = "Label448"
        Me.Label448.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label448.Text = "OK if SUM_spY <= SUM_Allowed"
        Me.Label448.Top = 16.915!
        Me.Label448.Width = 4.25!
        '
        'Label450
        '
        Me.Label450.Height = 0.1875!
        Me.Label450.HyperLink = Nothing
        Me.Label450.Left = 3.25!
        Me.Label450.Name = "Label450"
        Me.Label450.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label450.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Max(SUM_bmWebθBearing1, SUM_bmWebθBearing2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label450.Top = 16.726!
        Me.Label450.Width = 4.75!
        '
        'S48_051
        '
        Me.S48_051.Height = 0.1875!
        Me.S48_051.HyperLink = Nothing
        Me.S48_051.Left = 2.25!
        Me.S48_051.Name = "S48_051"
        Me.S48_051.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_051.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_051.Top = 16.727!
        Me.S48_051.Width = 1.0!
        '
        'Label452
        '
        Me.Label452.Height = 0.1875!
        Me.Label452.HyperLink = Nothing
        Me.Label452.Left = 0.0000004768372!
        Me.Label452.Name = "Label452"
        Me.Label452.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label452.Text = "Check="
        Me.Label452.Top = 16.915!
        Me.Label452.Width = 2.25!
        '
        'Label453
        '
        Me.Label453.Height = 0.1875!
        Me.Label453.HyperLink = Nothing
        Me.Label453.Left = 0.5000002!
        Me.Label453.Name = "Label453"
        Me.Label453.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label453.Text = "SHEAR PLATE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label453.Top = 17.102!
        Me.Label453.Width = 7.75!
        '
        'Label454
        '
        Me.Label454.Height = 0.1875!
        Me.Label454.HyperLink = Nothing
        Me.Label454.Left = 1.0!
        Me.Label454.Name = "Label454"
        Me.Label454.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label454.Text = "Method 1: T and V circular interaction" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label454.Top = 17.29!
        Me.Label454.Width = 8.0!
        '
        'Label455
        '
        Me.Label455.Height = 0.1875!
        Me.Label455.HyperLink = Nothing
        Me.Label455.Left = 0.0000002384186!
        Me.Label455.Name = "Label455"
        Me.Label455.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label455.Text = "SUM_spθBearing1 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label455.Top = 17.477!
        Me.Label455.Width = 2.187!
        '
        'Label456
        '
        Me.Label456.Height = 0.1875!
        Me.Label456.HyperLink = Nothing
        Me.Label456.Left = 3.75!
        Me.Label456.Name = "Label456"
        Me.Label456.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label456.Text = "= SUM_spX^2 + SUM_spY^2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label456.Top = 17.477!
        Me.Label456.Width = 4.25!
        '
        'Label458
        '
        Me.Label458.Height = 0.1875!
        Me.Label458.HyperLink = Nothing
        Me.Label458.Left = 1.0!
        Me.Label458.Name = "Label458"
        Me.Label458.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label458.Text = "Method 2: Bearing in a diagonal line" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label458.Top = 17.728!
        Me.Label458.Width = 8.0!
        '
        'Label459
        '
        Me.Label459.Height = 0.1875!
        Me.Label459.HyperLink = Nothing
        Me.Label459.Left = 0.0000002384186!
        Me.Label459.Name = "Label459"
        Me.Label459.Style = "font-family: Arial; font-size: 8.25pt; text-align: right; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label459.Text = "θ1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label459.Top = 17.915!
        Me.Label459.Width = 2.25!
        '
        'Label460
        '
        Me.Label460.Height = 0.1875!
        Me.Label460.HyperLink = Nothing
        Me.Label460.Left = 3.25!
        Me.Label460.Name = "Label460"
        Me.Label460.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label460.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "= arctan((0.5*LslotH)/Shorz)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label460.Top = 17.915!
        Me.Label460.Width = 4.75!
        '
        'S48_054
        '
        Me.S48_054.Height = 0.1875!
        Me.S48_054.HyperLink = Nothing
        Me.S48_054.Left = 2.25!
        Me.S48_054.Name = "S48_054"
        Me.S48_054.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_054.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_054.Top = 17.91599!
        Me.S48_054.Width = 1.0!
        '
        'Label462
        '
        Me.Label462.Height = 0.1875!
        Me.Label462.HyperLink = Nothing
        Me.Label462.Left = 0.0000002384186!
        Me.Label462.Name = "Label462"
        Me.Label462.Style = "font-family: Arial; font-size: 8.25pt; text-align: right; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label462.Text = "θ2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label462.Top = 18.10401!
        Me.Label462.Width = 2.25!
        '
        'Label463
        '
        Me.Label463.Height = 0.1875!
        Me.Label463.HyperLink = Nothing
        Me.Label463.Left = 3.25!
        Me.Label463.Name = "Label463"
        Me.Label463.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label463.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "= arctan((0.5*h_sp)/(w_sp-a))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label463.Top = 18.103!
        Me.Label463.Width = 4.75!
        '
        'S48_055
        '
        Me.S48_055.Height = 0.1875!
        Me.S48_055.HyperLink = Nothing
        Me.S48_055.Left = 2.25!
        Me.S48_055.Name = "S48_055"
        Me.S48_055.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_055.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_055.Top = 18.10399!
        Me.S48_055.Width = 1.0!
        '
        'Label465
        '
        Me.Label465.Height = 0.1875!
        Me.Label465.HyperLink = Nothing
        Me.Label465.Left = 0.0000002384186!
        Me.Label465.Name = "Label465"
        Me.Label465.Style = "font-family: Arial; font-size: 8.25pt; text-align: right; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label465.Text = "θ'=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label465.Top = 18.29201!
        Me.Label465.Width = 2.25!
        '
        'Label466
        '
        Me.Label466.Height = 0.1875!
        Me.Label466.HyperLink = Nothing
        Me.Label466.Left = 3.25!
        Me.Label466.Name = "Label466"
        Me.Label466.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label466.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "= arctan((0.5*LslotV)/Svert)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label466.Top = 18.29201!
        Me.Label466.Width = 4.75!
        '
        'S48_056
        '
        Me.S48_056.Height = 0.1875!
        Me.S48_056.HyperLink = Nothing
        Me.S48_056.Left = 2.25!
        Me.S48_056.Name = "S48_056"
        Me.S48_056.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_056.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_056.Top = 18.293!
        Me.S48_056.Width = 1.0!
        '
        'Label468
        '
        Me.Label468.Height = 0.1875!
        Me.Label468.HyperLink = Nothing
        Me.Label468.Left = 0.0000004768372!
        Me.Label468.Name = "Label468"
        Me.Label468.Style = "font-family: Arial; font-size: 8.25pt; text-align: right; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label468.Text = "θ3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label468.Top = 18.47901!
        Me.Label468.Width = 2.25!
        '
        'Label469
        '
        Me.Label469.Height = 0.1875!
        Me.Label469.HyperLink = Nothing
        Me.Label469.Left = 3.25!
        Me.Label469.Name = "Label469"
        Me.Label469.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label469.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "= (PI()*0.5)-θ'" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label469.Top = 18.47901!
        Me.Label469.Width = 4.75!
        '
        'S48_057
        '
        Me.S48_057.Height = 0.1875!
        Me.S48_057.HyperLink = Nothing
        Me.S48_057.Left = 2.25!
        Me.S48_057.Name = "S48_057"
        Me.S48_057.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_057.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_057.Top = 18.47999!
        Me.S48_057.Width = 1.0!
        '
        'Label471
        '
        Me.Label471.Height = 0.1875!
        Me.Label471.HyperLink = Nothing
        Me.Label471.Left = 0.0000004768372!
        Me.Label471.Name = "Label471"
        Me.Label471.Style = "font-family: Arial; font-size: 8.25pt; text-align: right; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label471.Text = "θ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label471.Top = 18.66701!
        Me.Label471.Width = 2.25!
        '
        'Label472
        '
        Me.Label472.Height = 0.1875!
        Me.Label472.HyperLink = Nothing
        Me.Label472.Left = 3.25!
        Me.Label472.Name = "Label472"
        Me.Label472.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label472.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label472.Top = 18.66701!
        Me.Label472.Width = 4.75!
        '
        'S48_058
        '
        Me.S48_058.Height = 0.1875!
        Me.S48_058.HyperLink = Nothing
        Me.S48_058.Left = 2.25!
        Me.S48_058.Name = "S48_058"
        Me.S48_058.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_058.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_058.Top = 18.668!
        Me.S48_058.Width = 1.0!
        '
        'Label495
        '
        Me.Label495.Height = 0.1875!
        Me.Label495.HyperLink = Nothing
        Me.Label495.Left = 0.0000002384186!
        Me.Label495.Name = "Label495"
        Me.Label495.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label495.Text = "SUM_spθbearing=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label495.Top = 21.04!
        Me.Label495.Width = 2.25!
        '
        'Label496
        '
        Me.Label496.Height = 0.1875!
        Me.Label496.HyperLink = Nothing
        Me.Label496.Left = 3.25!
        Me.Label496.Name = "Label496"
        Me.Label496.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label496.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK if SUM_spθbearing <= SUM_spθbearing_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label496.Top = 21.228!
        Me.Label496.Width = 4.75!
        '
        'Label498
        '
        Me.Label498.Height = 0.1875!
        Me.Label498.HyperLink = Nothing
        Me.Label498.Left = 3.25!
        Me.Label498.Name = "Label498"
        Me.Label498.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label498.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= max(SUM_spθBearing1, SUM_spθbearing2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label498.Top = 21.039!
        Me.Label498.Width = 4.75!
        '
        'S48_067
        '
        Me.S48_067.Height = 0.1875!
        Me.S48_067.HyperLink = Nothing
        Me.S48_067.Left = 2.25!
        Me.S48_067.Name = "S48_067"
        Me.S48_067.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S48_067.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_067.Top = 21.03999!
        Me.S48_067.Width = 1.0!
        '
        'Label500
        '
        Me.Label500.Height = 0.1875!
        Me.Label500.HyperLink = Nothing
        Me.Label500.Left = 0.0000002384186!
        Me.Label500.Name = "Label500"
        Me.Label500.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label500.Text = "Adequate =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label500.Top = 21.228!
        Me.Label500.Width = 2.25!
        '
        'label111
        '
        Me.label111.Height = 0.1875!
        Me.label111.HyperLink = Nothing
        Me.label111.Left = 3.25!
        Me.label111.Name = "label111"
        Me.label111.Style = "color: Black; font-family: Symbol; font-size: 8.25pt; font-weight: normal; text-a" &
    "lign: center; vertical-align: middle; ddo-char-set: 2"
        Me.label111.Text = "q1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.label111.Top = 19.102!
        Me.label111.Width = 1.0!
        '
        'label112
        '
        Me.label112.Height = 0.1875!
        Me.label112.HyperLink = Nothing
        Me.label112.Left = 4.25!
        Me.label112.Name = "label112"
        Me.label112.Style = "color: Black; font-family: Symbol; font-size: 8.25pt; font-weight: normal; text-a" &
    "lign: center; vertical-align: middle; ddo-char-set: 2"
        Me.label112.Text = "q2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.label112.Top = 19.102!
        Me.label112.Width = 1.0!
        '
        'label113
        '
        Me.label113.Height = 0.1875!
        Me.label113.HyperLink = Nothing
        Me.label113.Left = 5.25!
        Me.label113.Name = "label113"
        Me.label113.Style = "color: Black; font-family: Symbol; font-size: 8.25pt; font-weight: normal; text-a" &
    "lign: center; vertical-align: middle; ddo-char-set: 2"
        Me.label113.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.label113.Top = 19.102!
        Me.label113.Width = 1.0!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 2.25!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 19.102!
        Me.Line1.Width = 4.0!
        Me.Line1.X1 = 2.25!
        Me.Line1.X2 = 6.25!
        Me.Line1.Y1 = 19.102!
        Me.Line1.Y2 = 19.102!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 2.25!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 18.914!
        Me.Line2.Width = 4.0!
        Me.Line2.X1 = 2.25!
        Me.Line2.X2 = 6.25!
        Me.Line2.Y1 = 18.914!
        Me.Line2.Y2 = 18.914!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 3.25!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "color: Black; font-family: Symbol; font-size: 8.25pt; font-weight: normal; text-a" &
    "lign: center; vertical-align: middle; ddo-char-set: 2"
        Me.Label3.Text = "q2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label3.Top = 19.289!
        Me.Label3.Width = 1.0!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 4.25!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "color: Black; font-family: Symbol; font-size: 8.25pt; font-weight: normal; text-a" &
    "lign: center; vertical-align: middle; ddo-char-set: 2"
        Me.Label4.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 19.289!
        Me.Label4.Width = 1.0!
        '
        'S48_073
        '
        Me.S48_073.Height = 0.1875!
        Me.S48_073.HyperLink = Nothing
        Me.S48_073.Left = 5.25!
        Me.S48_073.Name = "S48_073"
        Me.S48_073.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_073.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_073.Top = 19.289!
        Me.S48_073.Width = 1.0!
        '
        'S48_069
        '
        Me.S48_069.Height = 0.1875!
        Me.S48_069.HyperLink = Nothing
        Me.S48_069.Left = 3.25!
        Me.S48_069.Name = "S48_069"
        Me.S48_069.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_069.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_069.Top = 19.47701!
        Me.S48_069.Width = 1.0!
        '
        'S48_071
        '
        Me.S48_071.Height = 0.1875!
        Me.S48_071.HyperLink = Nothing
        Me.S48_071.Left = 4.25!
        Me.S48_071.Name = "S48_071"
        Me.S48_071.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_071.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_071.Top = 19.47701!
        Me.S48_071.Width = 1.0!
        '
        'S48_074
        '
        Me.S48_074.Height = 0.1875!
        Me.S48_074.HyperLink = Nothing
        Me.S48_074.Left = 5.25!
        Me.S48_074.Name = "S48_074"
        Me.S48_074.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S48_074.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_074.Top = 19.47701!
        Me.S48_074.Width = 1.0!
        '
        'S48_070
        '
        Me.S48_070.Height = 0.1875!
        Me.S48_070.HyperLink = Nothing
        Me.S48_070.Left = 3.25!
        Me.S48_070.Name = "S48_070"
        Me.S48_070.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_070.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_070.Top = 19.66501!
        Me.S48_070.Width = 1.0!
        '
        'S48_072
        '
        Me.S48_072.Height = 0.1875!
        Me.S48_072.HyperLink = Nothing
        Me.S48_072.Left = 4.25!
        Me.S48_072.Name = "S48_072"
        Me.S48_072.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_072.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_072.Top = 19.66501!
        Me.S48_072.Width = 1.0!
        '
        'S48_075
        '
        Me.S48_075.Height = 0.1875!
        Me.S48_075.HyperLink = Nothing
        Me.S48_075.Left = 5.25!
        Me.S48_075.Name = "S48_075"
        Me.S48_075.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_075.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_075.Top = 19.66501!
        Me.S48_075.Width = 1.0!
        '
        'Line3
        '
        Me.Line3.Height = 0.9385109!
        Me.Line3.Left = 2.25!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 18.914!
        Me.Line3.Width = 0!
        Me.Line3.X1 = 2.25!
        Me.Line3.X2 = 2.25!
        Me.Line3.Y1 = 18.914!
        Me.Line3.Y2 = 19.85251!
        '
        'Line4
        '
        Me.Line4.Height = 0.9390011!
        Me.Line4.Left = 3.25!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 18.914!
        Me.Line4.Width = 0!
        Me.Line4.X1 = 3.25!
        Me.Line4.X2 = 3.25!
        Me.Line4.Y1 = 18.914!
        Me.Line4.Y2 = 19.853!
        '
        'Line5
        '
        Me.Line5.Height = 0.9390011!
        Me.Line5.Left = 4.25!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 18.914!
        Me.Line5.Width = 0!
        Me.Line5.X1 = 4.25!
        Me.Line5.X2 = 4.25!
        Me.Line5.Y1 = 18.914!
        Me.Line5.Y2 = 19.853!
        '
        'Line6
        '
        Me.Line6.Height = 0.9390011!
        Me.Line6.Left = 5.25!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 18.914!
        Me.Line6.Width = 0!
        Me.Line6.X1 = 5.25!
        Me.Line6.X2 = 5.25!
        Me.Line6.Y1 = 18.914!
        Me.Line6.Y2 = 19.853!
        '
        'Line7
        '
        Me.Line7.Height = 0.9390011!
        Me.Line7.Left = 6.249002!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 18.914!
        Me.Line7.Width = 0!
        Me.Line7.X1 = 6.249002!
        Me.Line7.X2 = 6.249002!
        Me.Line7.Y1 = 18.914!
        Me.Line7.Y2 = 19.853!
        '
        'Line8
        '
        Me.Line8.Height = 0!
        Me.Line8.Left = 2.249!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 19.289!
        Me.Line8.Width = 4.000001!
        Me.Line8.X1 = 2.249!
        Me.Line8.X2 = 6.249001!
        Me.Line8.Y1 = 19.289!
        Me.Line8.Y2 = 19.289!
        '
        'Line9
        '
        Me.Line9.Height = 0!
        Me.Line9.Left = 2.249!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 19.47701!
        Me.Line9.Width = 4.000002!
        Me.Line9.X1 = 2.249!
        Me.Line9.X2 = 6.249002!
        Me.Line9.Y1 = 19.47701!
        Me.Line9.Y2 = 19.47701!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 2.249!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 19.665!
        Me.Line10.Width = 4.000002!
        Me.Line10.X1 = 2.249!
        Me.Line10.X2 = 6.249002!
        Me.Line10.Y1 = 19.665!
        Me.Line10.Y2 = 19.665!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 2.25!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 19.85201!
        Me.Line11.Width = 4.0!
        Me.Line11.X1 = 2.25!
        Me.Line11.X2 = 6.25!
        Me.Line11.Y1 = 19.85201!
        Me.Line11.Y2 = 19.85201!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 0.0000002384186!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label17.Text = "SUM_spθbearing2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label17.Top = 20.72701!
        Me.Label17.Width = 2.25!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 3.25!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label18.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pr / (Φ*Rn_spθ)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 20.726!
        Me.Label18.Width = 4.75!
        '
        'S48_066
        '
        Me.S48_066.Height = 0.1875!
        Me.S48_066.HyperLink = Nothing
        Me.S48_066.Left = 2.25!
        Me.S48_066.Name = "S48_066"
        Me.S48_066.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_066.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_066.Top = 20.727!
        Me.S48_066.Width = 1.0!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0.0000002384186!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label20.Text = " Φ*Rn_spθ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label20.Top = 20.539!
        Me.Label20.Width = 2.25!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 3.25!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label21.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= min (Φ*Rn_spθ1, Φ*Rn_spθ2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label21.Top = 20.53799!
        Me.Label21.Width = 4.75!
        '
        'S48_065
        '
        Me.S48_065.Height = 0.1875!
        Me.S48_065.HyperLink = Nothing
        Me.S48_065.Left = 2.25!
        Me.S48_065.Name = "S48_065"
        Me.S48_065.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_065.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_065.Top = 20.53898!
        Me.S48_065.Width = 1.0!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 0.0000002384186!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label23.Text = " Φ*Rn_spθ2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label23.Top = 20.352!
        Me.Label23.Width = 2.25!
        '
        'Label24
        '
        Me.Label24.Height = 0.1875!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 3.25!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label24.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 2.4* db_sp* tsp* Fu_sp*n_Hbolts" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label24.Top = 20.351!
        Me.Label24.Width = 4.75!
        '
        'S48_064
        '
        Me.S48_064.Height = 0.1875!
        Me.S48_064.HyperLink = Nothing
        Me.S48_064.Left = 2.25!
        Me.S48_064.Name = "S48_064"
        Me.S48_064.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_064.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_064.Top = 20.35198!
        Me.S48_064.Width = 1.0!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 0.0000002384186!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label26.Text = " Φ*Rn_spθ1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label26.Top = 20.16301!
        Me.Label26.Width = 2.25!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 3.25!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label27.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φbolt* 1.2* La_sp* tsp* Fu_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label27.Top = 20.16202!
        Me.Label27.Width = 4.75!
        '
        'S48_063
        '
        Me.S48_063.Height = 0.1875!
        Me.S48_063.HyperLink = Nothing
        Me.S48_063.Left = 2.25!
        Me.S48_063.Name = "S48_063"
        Me.S48_063.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_063.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_063.Top = 20.16299!
        Me.S48_063.Width = 1.0!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0.0000002384186!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label29.Text = "La_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label29.Top = 19.975!
        Me.Label29.Width = 2.25!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 3.25!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label30.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label30.Top = 19.974!
        Me.Label30.Width = 4.75!
        '
        'S48_062
        '
        Me.S48_062.Height = 0.1875!
        Me.S48_062.HyperLink = Nothing
        Me.S48_062.Left = 2.25!
        Me.S48_062.Name = "S48_062"
        Me.S48_062.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_062.Text = "5.258" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_062.Top = 19.97498!
        Me.S48_062.Width = 1.0!
        '
        'Shape5
        '
        Me.Shape5.Height = 0.1875!
        Me.Shape5.Left = 2.25!
        Me.Shape5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape5.Name = "Shape5"
        Me.Shape5.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape5.Top = 4.361997!
        Me.Shape5.Width = 1.0!
        '
        'Shape8
        '
        Me.Shape8.Height = 0.1875!
        Me.Shape8.Left = 2.25!
        Me.Shape8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape8.Name = "Shape8"
        Me.Shape8.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape8.Top = 6.921!
        Me.Shape8.Width = 1.0!
        '
        'Shape11
        '
        Me.Shape11.Height = 0.1875!
        Me.Shape11.Left = 2.25!
        Me.Shape11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape11.Name = "Shape11"
        Me.Shape11.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape11.Top = 10.42101!
        Me.Shape11.Width = 1.0!
        '
        'Shape10
        '
        Me.Shape10.Height = 0.1875!
        Me.Shape10.Left = 2.25!
        Me.Shape10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape10.Name = "Shape10"
        Me.Shape10.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape10.Top = 11.932!
        Me.Shape10.Width = 1.0!
        '
        'Shape9
        '
        Me.Shape9.Height = 0.1875!
        Me.Shape9.Left = 2.25!
        Me.Shape9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape9.Name = "Shape9"
        Me.Shape9.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape9.Top = 15.097!
        Me.Shape9.Width = 1.0!
        '
        'Shape12
        '
        Me.Shape12.Height = 0.1875!
        Me.Shape12.Left = 2.25!
        Me.Shape12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape12.Name = "Shape12"
        Me.Shape12.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape12.Top = 16.915!
        Me.Shape12.Width = 1.0!
        '
        'Shape13
        '
        Me.Shape13.Height = 0.1875!
        Me.Shape13.Left = 2.25!
        Me.Shape13.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape13.Name = "Shape13"
        Me.Shape13.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape13.Top = 17.477!
        Me.Shape13.Width = 1.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "Lcw4=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.Top = 3.231!
        Me.Label5.Width = 2.25!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.25!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label6.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (n_Hbot_SST = 3, 0, Lcw3)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label6.Top = 3.231!
        Me.Label6.Width = 4.75!
        '
        'S48_076
        '
        Me.S48_076.Height = 0.1875!
        Me.S48_076.HyperLink = Nothing
        Me.S48_076.Left = 2.25!
        Me.S48_076.Name = "S48_076"
        Me.S48_076.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_076.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_076.Top = 3.231998!
        Me.S48_076.Width = 1.0!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label8.Text = "Lc_sp4 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label8.Top = 5.781!
        Me.Label8.Width = 2.25!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 3.25!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label9.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (n_Hbot_SST = 3, 0, Lc_sp3)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 5.781!
        Me.Label9.Width = 4.75!
        '
        'S48_077
        '
        Me.S48_077.Height = 0.1875!
        Me.S48_077.HyperLink = Nothing
        Me.S48_077.Left = 2.25!
        Me.S48_077.Name = "S48_077"
        Me.S48_077.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S48_077.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S48_077.Top = 5.782!
        Me.S48_077.Width = 1.0!
        '
        'Shape14
        '
        Me.Shape14.Height = 0.1875!
        Me.Shape14.Left = 2.25!
        Me.Shape14.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape14.Name = "Shape14"
        Me.Shape14.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape14.Top = 21.22899!
        Me.Shape14.Width = 1.0!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.01!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(9.323!, 0.01!)
        Me.PageBreak1.Top = 15.409!
        Me.PageBreak1.Width = 9.323!
        '
        'US_SPC4
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 9.323!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S48_068, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_039, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_044, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_052, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_053, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_061, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_060, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_059, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label277, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label274, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label288, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label289, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label290, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label292, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label293, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label295, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label296, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label298, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label299, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label301, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label302, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label304, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label305, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label307, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label308, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label310, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label311, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label313, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label314, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label316, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label317, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label319, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label320, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label322, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label323, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label325, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label326, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label327, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label329, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label330, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label332, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label333, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label335, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label336, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label338, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label339, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label341, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label342, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label344, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label345, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label347, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label348, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label350, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label351, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label353, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label354, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label356, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label357, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label359, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label360, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label361, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label363, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label364, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label366, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label367, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label369, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label370, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label372, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label373, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label375, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label376, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label378, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label379, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label381, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label382, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label384, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label385, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label387, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label388, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label389, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label390, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_033, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label392, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label393, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_034, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label395, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label396, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_035, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label398, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label399, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_036, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label401, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label402, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_037, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label404, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label405, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_038, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label407, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label408, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label410, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label411, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label412, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_040, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label414, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label415, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_041, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label417, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label418, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_042, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label420, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label421, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_043, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label423, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label424, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label425, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label426, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label428, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label429, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label430, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_045, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label432, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label433, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_046, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label435, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label436, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_047, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label438, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label439, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_048, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label441, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label442, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_049, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label444, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label445, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_050, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label447, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label448, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label450, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_051, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label452, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label453, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label454, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label455, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label456, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label458, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label459, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label460, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_054, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label462, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label463, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_055, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label465, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label466, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_056, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label468, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label469, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_057, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label471, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label472, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_058, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label495, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label496, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label498, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_067, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label500, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.label111, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.label112, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.label113, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_073, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_069, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_071, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_074, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_070, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_072, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_075, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_066, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_065, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_064, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_063, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_062, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_076, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S48_077, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents S48_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label274 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label288 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture3 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Label289 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label290 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label292 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label293 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label295 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label296 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label298 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label299 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label301 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label302 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label304 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label305 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label307 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label308 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label310 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label311 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label313 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label314 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label316 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label317 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label319 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label320 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label322 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label323 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label325 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label326 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label327 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label329 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label330 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label332 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label333 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label335 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label336 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label338 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label339 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label341 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label342 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label344 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label345 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label347 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label348 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label350 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label351 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label353 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label354 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label356 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label357 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape5 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape8 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label359 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture4 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Label360 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label361 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label363 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label364 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label366 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label367 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label369 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label370 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label372 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label373 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label375 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label376 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label378 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label379 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label381 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label382 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label384 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label385 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label387 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label388 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label389 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label390 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label392 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label393 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_034 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label395 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label396 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_035 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label398 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label399 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_036 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label401 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label402 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_037 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label404 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label405 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_038 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label407 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label408 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_039 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape10 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape11 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label410 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture5 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Label411 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label412 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_040 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label414 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label415 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_041 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label417 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label418 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_042 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label420 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label421 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_043 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label423 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label424 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label425 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label426 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_044 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label428 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label429 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label430 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_045 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label432 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label433 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_046 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label435 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label436 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_047 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label438 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label439 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_048 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label441 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label442 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_049 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label444 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label445 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_050 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label447 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label448 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_052 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label450 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_051 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label452 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape9 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape12 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label453 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label454 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label455 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label456 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_053 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label458 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label459 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label460 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_054 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label462 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label463 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_055 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label466 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_056 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label468 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label469 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_057 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label471 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label472 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_058 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label495 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label496 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_068 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label498 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_067 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label500 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape13 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape14 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_061 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_060 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_059 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label279 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label277 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label273 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label271 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents label111 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents label112 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents label113 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_073 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_069 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_071 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_074 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_070 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_072 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_075 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_066 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_065 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_064 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_063 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_062 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label465 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_076 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S48_077 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak

    '

End Class
