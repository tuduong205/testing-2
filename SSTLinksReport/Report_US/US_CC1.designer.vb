﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_CC1
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_CC1))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S33_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.S33_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label43 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label47 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label334 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label48 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label50 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label56 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label65 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label67 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label69 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label71 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S32_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label73 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label74 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label78 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label80 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label81 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label83 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label84 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label86 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label90 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label91 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label92 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label94 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label96 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label98 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label100 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label107 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label111 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label115 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label119 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label121 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label170 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label171 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label174 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label178 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label179 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label183 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label186 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line17 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line28 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line29 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line30 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line31 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line32 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line33 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line34 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line38 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line39 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line40 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line41 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line42 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line43 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line44 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line45 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line46 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line47 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line48 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line49 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line50 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label55 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line51 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label175 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape19 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Line37 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line36 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line35 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label44 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_000 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label51 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label62 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S33_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label53 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S31_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        CType(Me.S33_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label334, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S32_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label170, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label171, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label174, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label178, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label183, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label186, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label175, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_000, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S33_033, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S31_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S33_021, Me.Line2, Me.S33_028, Me.S33_018, Me.S33_011, Me.S33_007, Me.S33_025, Me.S33_015, Me.S33_020, Me.Label1, Me.S31_001, Me.Label4, Me.Label5, Me.Label6, Me.S31_012, Me.Label8, Me.S31_013, Me.Label14, Me.S31_002, Me.Label18, Me.S32_001, Me.Label23, Me.Label25, Me.S32_002, Me.Label29, Me.Label31, Me.S32_003, Me.Label35, Me.Label37, Me.S32_004, Me.Label41, Me.Label43, Me.S32_005, Me.Label47, Me.Label10, Me.S31_017, Me.Label334, Me.S31_018, Me.Label11, Me.S31_015, Me.Label13, Me.S31_003, Me.Label17, Me.S31_019, Me.Label48, Me.S31_016, Me.Label50, Me.S31_004, Me.Label52, Me.S31_021, Me.Label56, Me.S31_005, Me.Label54, Me.S31_022, Me.Label58, Me.S31_024, Me.Label60, Me.S31_025, Me.S32_010, Me.S32_011, Me.S32_012, Me.S32_013, Me.S32_014, Me.Label28, Me.Label30, Me.Label33, Me.S32_006, Me.Label36, Me.S32_015, Me.Label40, Me.S32_007, Me.Label45, Me.S32_016, Me.Label65, Me.S32_008, Me.Label67, Me.S32_017, Me.Label69, Me.S32_009, Me.Label71, Me.S32_018, Me.Label73, Me.Label74, Me.S33_001, Me.Label78, Me.S33_012, Me.Label80, Me.Label81, Me.S33_013, Me.Label83, Me.Label84, Me.S33_014, Me.Label86, Me.S33_022, Me.S33_023, Me.S33_024, Me.Label90, Me.Label91, Me.Label92, Me.Label94, Me.Label96, Me.S33_016, Me.Label98, Me.S33_026, Me.Label100, Me.S33_004, Me.Label76, Me.S33_005, Me.Label107, Me.S33_006, Me.Label111, Me.S33_002, Me.Label115, Me.S33_003, Me.Label119, Me.S33_009, Me.Label121, Me.Label170, Me.Label171, Me.S33_017, Me.S33_027, Me.Label174, Me.S33_008, Me.Label178, Me.Label179, Me.Label183, Me.S33_019, Me.S33_029, Me.Label186, Me.Line17, Me.Line28, Me.Line29, Me.Line30, Me.Line31, Me.Line32, Me.Line33, Me.Line34, Me.Line38, Me.Line39, Me.Line40, Me.Line41, Me.Line42, Me.Line43, Me.Line44, Me.Line45, Me.Line46, Me.Line47, Me.Line48, Me.Line49, Me.Line50, Me.Label38, Me.S31_006, Me.Label55, Me.S31_010, Me.Label59, Me.S31_009, Me.Label66, Me.S31_008, Me.Line51, Me.Label175, Me.Label9, Me.Label19, Me.Label22, Me.Label24, Me.Shape19, Me.Line37, Me.Line36, Me.Label2, Me.S31_011, Me.Label7, Me.S31_020, Me.Label15, Me.S31_014, Me.Label20, Me.S31_023, Me.Line5, Me.Label3, Me.Label12, Me.Line35, Me.Label16, Me.Label21, Me.S33_010, Me.Line6, Me.Line7, Me.Label27, Me.Label26, Me.Label32, Me.Label34, Me.Label39, Me.Label42, Me.Label44, Me.Label46, Me.Label49, Me.S31_007, Me.S33_000, Me.Label51, Me.S33_030, Me.Label57, Me.S33_032, Me.Label62, Me.S33_031, Me.S33_033, Me.Line1, Me.Label53, Me.S31_026})
        Me.Detail.Height = 8.454857!
        Me.Detail.Name = "Detail"
        '
        'S33_021
        '
        Me.S33_021.Height = 0.1875!
        Me.S33_021.HyperLink = Nothing
        Me.S33_021.Left = 2.252002!
        Me.S33_021.Name = "S33_021"
        Me.S33_021.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S33_021.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_021.Top = 8.190001!
        Me.S33_021.Width = 1.0!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 2.250002!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 8.376!
        Me.Line2.Width = 0.9980001!
        Me.Line2.X1 = 2.250002!
        Me.Line2.X2 = 3.248002!
        Me.Line2.Y1 = 8.376!
        Me.Line2.Y2 = 8.376!
        '
        'S33_028
        '
        Me.S33_028.Height = 0.1875!
        Me.S33_028.HyperLink = Nothing
        Me.S33_028.Left = 3.25!
        Me.S33_028.Name = "S33_028"
        Me.S33_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_028.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_028.Top = 7.626!
        Me.S33_028.Width = 1.0!
        '
        'S33_018
        '
        Me.S33_018.Height = 0.1875!
        Me.S33_018.HyperLink = Nothing
        Me.S33_018.Left = 2.250001!
        Me.S33_018.Name = "S33_018"
        Me.S33_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_018.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_018.Top = 7.626!
        Me.S33_018.Width = 1.0!
        '
        'S33_011
        '
        Me.S33_011.Height = 0.1875!
        Me.S33_011.HyperLink = Nothing
        Me.S33_011.Left = 2.251!
        Me.S33_011.Name = "S33_011"
        Me.S33_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_011.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_011.Top = 6.067!
        Me.S33_011.Width = 1.0!
        '
        'S33_007
        '
        Me.S33_007.Height = 0.1875!
        Me.S33_007.HyperLink = Nothing
        Me.S33_007.Left = 2.251!
        Me.S33_007.Name = "S33_007"
        Me.S33_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_007.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_007.Top = 5.316!
        Me.S33_007.Width = 1.0!
        '
        'S33_025
        '
        Me.S33_025.Height = 0.1875!
        Me.S33_025.HyperLink = Nothing
        Me.S33_025.Left = 3.250002!
        Me.S33_025.Name = "S33_025"
        Me.S33_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_025.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_025.Top = 7.062004!
        Me.S33_025.Width = 1.0!
        '
        'S33_015
        '
        Me.S33_015.Height = 0.1875!
        Me.S33_015.HyperLink = Nothing
        Me.S33_015.Left = 2.250003!
        Me.S33_015.Name = "S33_015"
        Me.S33_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_015.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_015.Top = 7.062004!
        Me.S33_015.Width = 1.0!
        '
        'S33_020
        '
        Me.S33_020.Height = 0.1875!
        Me.S33_020.HyperLink = Nothing
        Me.S33_020.Left = 2.252001!
        Me.S33_020.Name = "S33_020"
        Me.S33_020.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S33_020.Text = "0.377" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_020.Top = 8.002002!
        Me.S33_020.Width = 1.002001!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "Column Unique Name:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 0.188!
        Me.Label1.Width = 1.562!
        '
        'S31_001
        '
        Me.S31_001.Height = 0.1875!
        Me.S31_001.HyperLink = Nothing
        Me.S31_001.Left = 1.562!
        Me.S31_001.Name = "S31_001"
        Me.S31_001.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.S31_001.Text = "299"
        Me.S31_001.Top = 0.188!
        Me.S31_001.Width = 1.0!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label4.Text = "3.1 CURRENT MEMBER:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 0!
        Me.Label4.Width = 3.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.188!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "Thickness (t_stem) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.Top = 2.251!
        Me.Label5.Width = 2.25!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 2.562!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label6.Text = "db_left (in)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label6.Top = 0.936!
        Me.Label6.Width = 1.375!
        '
        'S31_012
        '
        Me.S31_012.Height = 0.1875!
        Me.S31_012.HyperLink = Nothing
        Me.S31_012.Left = 3.937!
        Me.S31_012.Name = "S31_012"
        Me.S31_012.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_012.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_012.Top = 0.936!
        Me.S31_012.Width = 1.0!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 2.562!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label8.Text = "Left Link Size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label8.Top = 1.125!
        Me.Label8.Width = 1.375!
        '
        'S31_013
        '
        Me.S31_013.Height = 0.1875!
        Me.S31_013.HyperLink = Nothing
        Me.S31_013.Left = 3.937!
        Me.S31_013.Name = "S31_013"
        Me.S31_013.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_013.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_013.Top = 1.125!
        Me.S31_013.Width = 1.0!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label14.Text = "Column size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label14.Top = 0.375!
        Me.Label14.Width = 1.562!
        '
        'S31_002
        '
        Me.S31_002.Height = 0.1875!
        Me.S31_002.HyperLink = Nothing
        Me.S31_002.Left = 1.562!
        Me.S31_002.Name = "S31_002"
        Me.S31_002.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_002.Text = "W18X192" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_002.Top = 0.375!
        Me.S31_002.Width = 1.0!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 0!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label18.Text = "3.2 LINK PROPERTIES:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 1.876!
        Me.Label18.Width = 3.0!
        '
        'S32_001
        '
        Me.S32_001.Height = 0.1875!
        Me.S32_001.HyperLink = Nothing
        Me.S32_001.Left = 2.25!
        Me.S32_001.Name = "S32_001"
        Me.S32_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_001.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_001.Top = 2.25!
        Me.S32_001.Width = 1.0!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 4.25!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label23.Text = "in"
        Me.Label23.Top = 2.25!
        Me.Label23.Width = 0.25!
        '
        'Label25
        '
        Me.Label25.Height = 0.188!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label25.Text = " Yield Width (w_stemYield)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label25.Top = 2.437!
        Me.Label25.Width = 2.25!
        '
        'S32_002
        '
        Me.S32_002.Height = 0.1875!
        Me.S32_002.HyperLink = Nothing
        Me.S32_002.Left = 2.25!
        Me.S32_002.Name = "S32_002"
        Me.S32_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_002.Top = 2.437!
        Me.S32_002.Width = 1.0!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 4.25!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label29.Text = "in"
        Me.Label29.Top = 2.437!
        Me.Label29.Width = 0.25!
        '
        'Label31
        '
        Me.Label31.Height = 0.188!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 0!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label31.Text = "Thickness (t_flange)="
        Me.Label31.Top = 2.625!
        Me.Label31.Width = 2.25!
        '
        'S32_003
        '
        Me.S32_003.Height = 0.1875!
        Me.S32_003.HyperLink = Nothing
        Me.S32_003.Left = 2.25!
        Me.S32_003.Name = "S32_003"
        Me.S32_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_003.Top = 2.625001!
        Me.S32_003.Width = 1.0!
        '
        'Label35
        '
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 4.25!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label35.Text = "in"
        Me.Label35.Top = 2.625001!
        Me.Label35.Width = 0.25!
        '
        'Label37
        '
        Me.Label37.Height = 0.188!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 7.5pt; text-align: right; vertical-align: middle; ddo-char-set: 1; ddo" &
    "-shrink-to-fit: none"
        Me.Label37.Text = "Spacing Along Length (horiz)  (bolt_s_flange)="
        Me.Label37.Top = 2.813!
        Me.Label37.Width = 2.25!
        '
        'S32_004
        '
        Me.S32_004.Height = 0.1875!
        Me.S32_004.HyperLink = Nothing
        Me.S32_004.Left = 2.25!
        Me.S32_004.Name = "S32_004"
        Me.S32_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_004.Top = 2.813!
        Me.S32_004.Width = 1.0!
        '
        'Label41
        '
        Me.Label41.Height = 0.1875!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 4.25!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label41.Text = "in"
        Me.Label41.Top = 2.813!
        Me.Label41.Width = 0.25!
        '
        'Label43
        '
        Me.Label43.Height = 0.188!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 0!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle; ddo-char-set: 1; dd" &
    "o-shrink-to-fit: none"
        Me.Label43.Text = "Gauge Along Width (vert.) (bolt_g_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label43.Top = 3.001!
        Me.Label43.Width = 2.25!
        '
        'S32_005
        '
        Me.S32_005.Height = 0.1875!
        Me.S32_005.HyperLink = Nothing
        Me.S32_005.Left = 2.25!
        Me.S32_005.Name = "S32_005"
        Me.S32_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_005.Top = 3.001001!
        Me.S32_005.Width = 1.0!
        '
        'Label47
        '
        Me.Label47.Height = 0.1875!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 4.25!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label47.Text = "in"
        Me.Label47.Top = 3.001001!
        Me.Label47.Width = 0.25!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 5.125!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label10.Text = "Top Story=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 0.187!
        Me.Label10.Width = 1.375!
        '
        'S31_017
        '
        Me.S31_017.Height = 0.1875!
        Me.S31_017.HyperLink = Nothing
        Me.S31_017.Left = 6.5!
        Me.S31_017.Name = "S31_017"
        Me.S31_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S31_017.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_017.Top = 0.187!
        Me.S31_017.Width = 1.0!
        '
        'Label334
        '
        Me.Label334.Height = 0.1875!
        Me.Label334.HyperLink = Nothing
        Me.Label334.Left = 5.125!
        Me.Label334.Name = "Label334"
        Me.Label334.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label334.Text = "Use Doubler PL?=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label334.Top = 0.374!
        Me.Label334.Width = 1.375!
        '
        'S31_018
        '
        Me.S31_018.Height = 0.1875!
        Me.S31_018.HyperLink = Nothing
        Me.S31_018.Left = 6.5!
        Me.S31_018.Name = "S31_018"
        Me.S31_018.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_018.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_018.Top = 0.374!
        Me.S31_018.Width = 1.0!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 2.562!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label11.Text = "Left Beam Lh (in):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label11.Top = 1.5!
        Me.Label11.Width = 1.375!
        '
        'S31_015
        '
        Me.S31_015.Height = 0.1875!
        Me.S31_015.HyperLink = Nothing
        Me.S31_015.Left = 3.937!
        Me.S31_015.Name = "S31_015"
        Me.S31_015.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_015.Text = "0.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_015.Top = 1.5!
        Me.S31_015.Width = 1.0!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label13.Text = "Pu (kips):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label13.Top = 0.5630001!
        Me.Label13.Width = 1.562!
        '
        'S31_003
        '
        Me.S31_003.Height = 0.1875!
        Me.S31_003.HyperLink = Nothing
        Me.S31_003.Left = 1.562!
        Me.S31_003.Name = "S31_003"
        Me.S31_003.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_003.Text = "406.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_003.Top = 0.5630001!
        Me.S31_003.Width = 1.0!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 5.125!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label17.Text = "Right Beam size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label17.Top = 0.562!
        Me.Label17.Width = 1.375!
        '
        'S31_019
        '
        Me.S31_019.Height = 0.1875!
        Me.S31_019.HyperLink = Nothing
        Me.S31_019.Left = 6.5!
        Me.S31_019.Name = "S31_019"
        Me.S31_019.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_019.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_019.Top = 0.562!
        Me.S31_019.Width = 1.0!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 2.563!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label48.Text = "Vu_Gravity (kips):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label48.Top = 1.688!
        Me.Label48.Width = 1.374!
        '
        'S31_016
        '
        Me.S31_016.Height = 0.1875!
        Me.S31_016.HyperLink = Nothing
        Me.S31_016.Left = 3.938!
        Me.S31_016.Name = "S31_016"
        Me.S31_016.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_016.Text = "0.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_016.Top = 1.688!
        Me.S31_016.Width = 1.0!
        '
        'Label50
        '
        Me.Label50.Height = 0.1875!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 0!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label50.Text = "Hcc_b (in):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label50.Top = 0.7510001!
        Me.Label50.Width = 1.562!
        '
        'S31_004
        '
        Me.S31_004.Height = 0.1875!
        Me.S31_004.HyperLink = Nothing
        Me.S31_004.Left = 1.562!
        Me.S31_004.Name = "S31_004"
        Me.S31_004.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_004.Text = "150.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_004.Top = 0.7510001!
        Me.S31_004.Width = 1.0!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 5.125!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label52.Text = "db_right (in)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label52.Top = 0.937!
        Me.Label52.Width = 1.375!
        '
        'S31_021
        '
        Me.S31_021.Height = 0.1875!
        Me.S31_021.HyperLink = Nothing
        Me.S31_021.Left = 6.5!
        Me.S31_021.Name = "S31_021"
        Me.S31_021.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_021.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_021.Top = 0.937!
        Me.S31_021.Width = 1.0!
        '
        'Label56
        '
        Me.Label56.Height = 0.1875!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 0!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label56.Text = "Hcc_t (in):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label56.Top = 0.938!
        Me.Label56.Width = 1.562!
        '
        'S31_005
        '
        Me.S31_005.Height = 0.1875!
        Me.S31_005.HyperLink = Nothing
        Me.S31_005.Left = 1.562!
        Me.S31_005.Name = "S31_005"
        Me.S31_005.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_005.Text = "150.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_005.Top = 0.938!
        Me.S31_005.Width = 1.0!
        '
        'Label54
        '
        Me.Label54.Height = 0.1875!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 4.938002!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label54.Text = "Right Link Size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label54.Top = 1.125!
        Me.Label54.Width = 1.562!
        '
        'S31_022
        '
        Me.S31_022.Height = 0.1875!
        Me.S31_022.HyperLink = Nothing
        Me.S31_022.Left = 6.500001!
        Me.S31_022.Name = "S31_022"
        Me.S31_022.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_022.Text = "NA"
        Me.S31_022.Top = 1.125!
        Me.S31_022.Width = 1.0!
        '
        'Label58
        '
        Me.Label58.Height = 0.1875!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 4.938001!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label58.Text = "Right Beam Lh (in):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label58.Top = 1.5!
        Me.Label58.Width = 1.562!
        '
        'S31_024
        '
        Me.S31_024.Height = 0.1875!
        Me.S31_024.HyperLink = Nothing
        Me.S31_024.Left = 6.5!
        Me.S31_024.Name = "S31_024"
        Me.S31_024.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_024.Text = "0.375" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_024.Top = 1.5!
        Me.S31_024.Width = 1.0!
        '
        'Label60
        '
        Me.Label60.Height = 0.1875!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 4.937!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label60.Text = "Vu_Gravity (kips):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label60.Top = 1.687!
        Me.Label60.Width = 1.563001!
        '
        'S31_025
        '
        Me.S31_025.Height = 0.1875!
        Me.S31_025.HyperLink = Nothing
        Me.S31_025.Left = 6.500001!
        Me.S31_025.Name = "S31_025"
        Me.S31_025.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_025.Text = "150.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_025.Top = 1.688!
        Me.S31_025.Width = 1.0!
        '
        'S32_010
        '
        Me.S32_010.Height = 0.1875!
        Me.S32_010.HyperLink = Nothing
        Me.S32_010.Left = 3.25!
        Me.S32_010.Name = "S32_010"
        Me.S32_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_010.Top = 2.25!
        Me.S32_010.Width = 1.0!
        '
        'S32_011
        '
        Me.S32_011.Height = 0.1875!
        Me.S32_011.HyperLink = Nothing
        Me.S32_011.Left = 3.25!
        Me.S32_011.Name = "S32_011"
        Me.S32_011.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_011.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_011.Top = 2.437!
        Me.S32_011.Width = 1.0!
        '
        'S32_012
        '
        Me.S32_012.Height = 0.1875!
        Me.S32_012.HyperLink = Nothing
        Me.S32_012.Left = 3.25!
        Me.S32_012.Name = "S32_012"
        Me.S32_012.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_012.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_012.Top = 2.625001!
        Me.S32_012.Width = 1.0!
        '
        'S32_013
        '
        Me.S32_013.Height = 0.1875!
        Me.S32_013.HyperLink = Nothing
        Me.S32_013.Left = 3.25!
        Me.S32_013.Name = "S32_013"
        Me.S32_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_013.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_013.Top = 2.813!
        Me.S32_013.Width = 1.0!
        '
        'S32_014
        '
        Me.S32_014.Height = 0.1875!
        Me.S32_014.HyperLink = Nothing
        Me.S32_014.Left = 3.25!
        Me.S32_014.Name = "S32_014"
        Me.S32_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_014.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_014.Top = 3.001001!
        Me.S32_014.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 3.25!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label28.Text = "Right Link" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label28.Top = 2.063!
        Me.Label28.Width = 1.0!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 2.25!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label30.Text = "Left Link" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label30.Top = 2.063!
        Me.Label30.Width = 1.0!
        '
        'Label33
        '
        Me.Label33.Height = 0.188!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 0!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label33.Text = "Flange height (H_flange) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label33.Top = 3.189!
        Me.Label33.Width = 2.25!
        '
        'S32_006
        '
        Me.S32_006.Height = 0.1875!
        Me.S32_006.HyperLink = Nothing
        Me.S32_006.Left = 2.25!
        Me.S32_006.Name = "S32_006"
        Me.S32_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_006.Top = 3.189!
        Me.S32_006.Width = 1.0!
        '
        'Label36
        '
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 4.25!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label36.Text = "in"
        Me.Label36.Top = 3.189!
        Me.Label36.Width = 0.25!
        '
        'S32_015
        '
        Me.S32_015.Height = 0.1875!
        Me.S32_015.HyperLink = Nothing
        Me.S32_015.Left = 3.25!
        Me.S32_015.Name = "S32_015"
        Me.S32_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_015.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_015.Top = 3.189!
        Me.S32_015.Width = 1.0!
        '
        'Label40
        '
        Me.Label40.Height = 0.188!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 0!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label40.Text = "a=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label40.Top = 3.376!
        Me.Label40.Width = 2.25!
        '
        'S32_007
        '
        Me.S32_007.Height = 0.1875!
        Me.S32_007.HyperLink = Nothing
        Me.S32_007.Left = 2.25!
        Me.S32_007.Name = "S32_007"
        Me.S32_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_007.Top = 3.376001!
        Me.S32_007.Width = 1.0!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 4.25!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label45.Text = "in"
        Me.Label45.Top = 3.376001!
        Me.Label45.Width = 0.25!
        '
        'S32_016
        '
        Me.S32_016.Height = 0.1875!
        Me.S32_016.HyperLink = Nothing
        Me.S32_016.Left = 3.25!
        Me.S32_016.Name = "S32_016"
        Me.S32_016.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S32_016.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_016.Top = 3.376001!
        Me.S32_016.Width = 1.0!
        '
        'Label65
        '
        Me.Label65.Height = 0.188!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 0!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label65.Text = "Pcap_Link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label65.Top = 3.564!
        Me.Label65.Width = 2.25!
        '
        'S32_008
        '
        Me.S32_008.Height = 0.1875!
        Me.S32_008.HyperLink = Nothing
        Me.S32_008.Left = 2.25!
        Me.S32_008.Name = "S32_008"
        Me.S32_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S32_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_008.Top = 3.564!
        Me.S32_008.Width = 1.0!
        '
        'Label67
        '
        Me.Label67.Height = 0.1875!
        Me.Label67.HyperLink = Nothing
        Me.Label67.Left = 4.25!
        Me.Label67.Name = "Label67"
        Me.Label67.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label67.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (R=3, Py_link, Pr_Link)"
        Me.Label67.Top = 3.564!
        Me.Label67.Width = 3.313!
        '
        'S32_017
        '
        Me.S32_017.Height = 0.1875!
        Me.S32_017.HyperLink = Nothing
        Me.S32_017.Left = 3.25!
        Me.S32_017.Name = "S32_017"
        Me.S32_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S32_017.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_017.Top = 3.564!
        Me.S32_017.Width = 1.0!
        '
        'Label69
        '
        Me.Label69.Height = 0.188!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 0!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label69.Text = "Mcap=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label69.Top = 3.751!
        Me.Label69.Width = 2.25!
        '
        'S32_009
        '
        Me.S32_009.Height = 0.1875!
        Me.S32_009.HyperLink = Nothing
        Me.S32_009.Left = 2.25!
        Me.S32_009.Name = "S32_009"
        Me.S32_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S32_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_009.Top = 3.751001!
        Me.S32_009.Width = 1.0!
        '
        'Label71
        '
        Me.Label71.Height = 0.1875!
        Me.Label71.HyperLink = Nothing
        Me.Label71.Left = 4.25!
        Me.Label71.Name = "Label71"
        Me.Label71.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label71.Text = "k-in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pcap_Link *(db+tstem)"
        Me.Label71.Top = 3.751001!
        Me.Label71.Width = 3.312001!
        '
        'S32_018
        '
        Me.S32_018.Height = 0.1875!
        Me.S32_018.HyperLink = Nothing
        Me.S32_018.Left = 3.25!
        Me.S32_018.Name = "S32_018"
        Me.S32_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S32_018.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S32_018.Top = 3.751001!
        Me.S32_018.Width = 1.0!
        '
        'Label73
        '
        Me.Label73.Height = 0.1875!
        Me.Label73.HyperLink = Nothing
        Me.Label73.Left = 0!
        Me.Label73.Name = "Label73"
        Me.Label73.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label73.Text = "3.3 CHECK STRONG BEAM WEAK LINK REQUIREMENTS:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label73.Top = 4.001999!
        Me.Label73.Width = 5.0!
        '
        'Label74
        '
        Me.Label74.Height = 0.188!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 0!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label74.Text = "Column Size=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label74.Top = 4.189!
        Me.Label74.Width = 2.25!
        '
        'S33_001
        '
        Me.S33_001.Height = 0.1875!
        Me.S33_001.HyperLink = Nothing
        Me.S33_001.Left = 2.249999!
        Me.S33_001.Name = "S33_001"
        Me.S33_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_001.Text = "W18X192" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_001.Top = 4.189!
        Me.S33_001.Width = 1.0!
        '
        'Label78
        '
        Me.Label78.Height = 0.188!
        Me.Label78.HyperLink = Nothing
        Me.Label78.Left = 0.00000333786!
        Me.Label78.Name = "Label78"
        Me.Label78.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label78.Text = "Mcap=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label78.Top = 6.500001!
        Me.Label78.Width = 2.25!
        '
        'S33_012
        '
        Me.S33_012.Height = 0.1875!
        Me.S33_012.HyperLink = Nothing
        Me.S33_012.Left = 2.250003!
        Me.S33_012.Name = "S33_012"
        Me.S33_012.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_012.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_012.Top = 6.500001!
        Me.S33_012.Width = 1.0!
        '
        'Label80
        '
        Me.Label80.Height = 0.1875!
        Me.Label80.HyperLink = Nothing
        Me.Label80.Left = 4.25!
        Me.Label80.Name = "Label80"
        Me.Label80.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 0"
        Me.Label80.Text = "kip*in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label80.Top = 6.500001!
        Me.Label80.Width = 3.313!
        '
        'Label81
        '
        Me.Label81.Height = 0.188!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 0.00000333786!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label81.Text = "Lh=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label81.Top = 6.687002!
        Me.Label81.Width = 2.25!
        '
        'S33_013
        '
        Me.S33_013.Height = 0.1875!
        Me.S33_013.HyperLink = Nothing
        Me.S33_013.Left = 2.250003!
        Me.S33_013.Name = "S33_013"
        Me.S33_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_013.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_013.Top = 6.687004!
        Me.S33_013.Width = 1.0!
        '
        'Label83
        '
        Me.Label83.Height = 0.1875!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 4.25!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 0"
        Me.Label83.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=Lcc-dc"
        Me.Label83.Top = 6.687004!
        Me.Label83.Width = 3.307002!
        '
        'Label84
        '
        Me.Label84.Height = 0.188!
        Me.Label84.HyperLink = Nothing
        Me.Label84.Left = 0.00000333786!
        Me.Label84.Name = "Label84"
        Me.Label84.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label84.Text = " Vbm_gravity=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label84.Top = 6.875001!
        Me.Label84.Width = 2.25!
        '
        'S33_014
        '
        Me.S33_014.Height = 0.1875!
        Me.S33_014.HyperLink = Nothing
        Me.S33_014.Left = 2.250003!
        Me.S33_014.Name = "S33_014"
        Me.S33_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_014.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_014.Top = 6.875001!
        Me.S33_014.Width = 1.0!
        '
        'Label86
        '
        Me.Label86.Height = 0.1875!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 4.25!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 0"
        Me.Label86.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=Beam shear from SST_LC8"
        Me.Label86.Top = 6.875001!
        Me.Label86.Width = 3.313002!
        '
        'S33_022
        '
        Me.S33_022.Height = 0.1875!
        Me.S33_022.HyperLink = Nothing
        Me.S33_022.Left = 3.250002!
        Me.S33_022.Name = "S33_022"
        Me.S33_022.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_022.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_022.Top = 6.500001!
        Me.S33_022.Width = 1.0!
        '
        'S33_023
        '
        Me.S33_023.Height = 0.1875!
        Me.S33_023.HyperLink = Nothing
        Me.S33_023.Left = 3.250002!
        Me.S33_023.Name = "S33_023"
        Me.S33_023.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_023.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_023.Top = 6.687004!
        Me.S33_023.Width = 1.0!
        '
        'S33_024
        '
        Me.S33_024.Height = 0.1875!
        Me.S33_024.HyperLink = Nothing
        Me.S33_024.Left = 3.250002!
        Me.S33_024.Name = "S33_024"
        Me.S33_024.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_024.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_024.Top = 6.875001!
        Me.S33_024.Width = 1.0!
        '
        'Label90
        '
        Me.Label90.Height = 0.1875!
        Me.Label90.HyperLink = Nothing
        Me.Label90.Left = 3.252002!
        Me.Label90.Name = "Label90"
        Me.Label90.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label90.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label90.Top = 6.313!
        Me.Label90.Width = 1.0!
        '
        'Label91
        '
        Me.Label91.Height = 0.1875!
        Me.Label91.HyperLink = Nothing
        Me.Label91.Left = 2.252002!
        Me.Label91.Name = "Label91"
        Me.Label91.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label91.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label91.Top = 6.313!
        Me.Label91.Width = 1.0!
        '
        'Label92
        '
        Me.Label92.Height = 0.188!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 0.00000333786!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label92.Text = "Vu_bm=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label92.Top = 7.062002!
        Me.Label92.Width = 2.25!
        '
        'Label94
        '
        Me.Label94.Height = 0.1875!
        Me.Label94.HyperLink = Nothing
        Me.Label94.Left = 4.250001!
        Me.Label94.Name = "Label94"
        Me.Label94.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 0"
        Me.Label94.Text = "kip" & Global.Microsoft.VisualBasic.ChrW(9) & "= (No. of conn.* Mpr)/ Lh + Vbm_gravity"
        Me.Label94.Top = 7.062004!
        Me.Label94.Width = 3.313!
        '
        'Label96
        '
        Me.Label96.Height = 0.188!
        Me.Label96.HyperLink = Nothing
        Me.Label96.Left = 0.00000333786!
        Me.Label96.Name = "Label96"
        Me.Label96.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label96.Text = "a=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label96.Top = 7.250001!
        Me.Label96.Width = 2.25!
        '
        'S33_016
        '
        Me.S33_016.Height = 0.1875!
        Me.S33_016.HyperLink = Nothing
        Me.S33_016.Left = 2.250002!
        Me.S33_016.Name = "S33_016"
        Me.S33_016.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_016.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_016.Top = 7.250001!
        Me.S33_016.Width = 1.0!
        '
        'Label98
        '
        Me.Label98.Height = 0.1875!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 4.25!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 0"
        Me.Label98.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label98.Top = 7.250001!
        Me.Label98.Width = 3.313001!
        '
        'S33_026
        '
        Me.S33_026.Height = 0.1875!
        Me.S33_026.HyperLink = Nothing
        Me.S33_026.Left = 3.250002!
        Me.S33_026.Name = "S33_026"
        Me.S33_026.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_026.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_026.Top = 7.250001!
        Me.S33_026.Width = 1.0!
        '
        'Label100
        '
        Me.Label100.Height = 0.188!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 0!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label100.Text = "Fyc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label100.Top = 4.751999!
        Me.Label100.Width = 2.25!
        '
        'S33_004
        '
        Me.S33_004.Height = 0.1875!
        Me.S33_004.HyperLink = Nothing
        Me.S33_004.Left = 2.249999!
        Me.S33_004.Name = "S33_004"
        Me.S33_004.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S33_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_004.Top = 4.752!
        Me.S33_004.Width = 1.0!
        '
        'Label76
        '
        Me.Label76.Height = 0.188!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 0!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label76.Text = "Agc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label76.Top = 4.939999!
        Me.Label76.Width = 2.25!
        '
        'S33_005
        '
        Me.S33_005.Height = 0.1875!
        Me.S33_005.HyperLink = Nothing
        Me.S33_005.Left = 2.249999!
        Me.S33_005.Name = "S33_005"
        Me.S33_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S33_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_005.Top = 4.94!
        Me.S33_005.Width = 1.0!
        '
        'Label107
        '
        Me.Label107.Height = 0.188!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 0!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label107.Text = "Zcx=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label107.Top = 5.127998!
        Me.Label107.Width = 2.25!
        '
        'S33_006
        '
        Me.S33_006.Height = 0.1875!
        Me.S33_006.HyperLink = Nothing
        Me.S33_006.Left = 2.249999!
        Me.S33_006.Name = "S33_006"
        Me.S33_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S33_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_006.Top = 5.127998!
        Me.S33_006.Width = 1.0!
        '
        'Label111
        '
        Me.Label111.Height = 0.188!
        Me.Label111.HyperLink = Nothing
        Me.Label111.Left = 0!
        Me.Label111.Name = "Label111"
        Me.Label111.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label111.Text = "dc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label111.Top = 4.377!
        Me.Label111.Width = 2.25!
        '
        'S33_002
        '
        Me.S33_002.Height = 0.1875!
        Me.S33_002.HyperLink = Nothing
        Me.S33_002.Left = 2.249999!
        Me.S33_002.Name = "S33_002"
        Me.S33_002.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S33_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_002.Top = 4.377!
        Me.S33_002.Width = 1.0!
        '
        'Label115
        '
        Me.Label115.Height = 0.188!
        Me.Label115.HyperLink = Nothing
        Me.Label115.Left = 0!
        Me.Label115.Name = "Label115"
        Me.Label115.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label115.Text = "tcf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label115.Top = 4.565!
        Me.Label115.Width = 2.25!
        '
        'S33_003
        '
        Me.S33_003.Height = 0.1875!
        Me.S33_003.HyperLink = Nothing
        Me.S33_003.Left = 2.249999!
        Me.S33_003.Name = "S33_003"
        Me.S33_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S33_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_003.Top = 4.565!
        Me.S33_003.Width = 1.0!
        '
        'Label119
        '
        Me.Label119.Height = 0.188!
        Me.Label119.HyperLink = Nothing
        Me.Label119.Left = 0!
        Me.Label119.Name = "Label119"
        Me.Label119.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label119.Text = "Column Pu="
        Me.Label119.Top = 5.69!
        Me.Label119.Width = 2.25!
        '
        'S33_009
        '
        Me.S33_009.Height = 0.1875!
        Me.S33_009.HyperLink = Nothing
        Me.S33_009.Left = 2.249999!
        Me.S33_009.Name = "S33_009"
        Me.S33_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S33_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_009.Top = 5.69!
        Me.S33_009.Width = 1.0!
        '
        'Label121
        '
        Me.Label121.Height = 0.188!
        Me.Label121.HyperLink = Nothing
        Me.Label121.Left = 3.251002!
        Me.Label121.Name = "Label121"
        Me.Label121.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 0"
        Me.Label121.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Max Axial Force of column per Omega Combo (LC31-34) From ETABS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label121.Top = 5.689!
        Me.Label121.Width = 4.305998!
        '
        'Label170
        '
        Me.Label170.Height = 0.1875!
        Me.Label170.HyperLink = Nothing
        Me.Label170.Left = 4.243999!
        Me.Label170.Name = "Label170"
        Me.Label170.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label170.Text = "kips*in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm* (dc/2 + a)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label170.Top = 7.437999!
        Me.Label170.Width = 3.313!
        '
        'Label171
        '
        Me.Label171.Height = 0.188!
        Me.Label171.HyperLink = Nothing
        Me.Label171.Left = 0.000002148561!
        Me.Label171.Name = "Label171"
        Me.Label171.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label171.Text = "Muv=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label171.Top = 7.438!
        Me.Label171.Width = 2.25!
        '
        'S33_017
        '
        Me.S33_017.Height = 0.1875!
        Me.S33_017.HyperLink = Nothing
        Me.S33_017.Left = 2.247002!
        Me.S33_017.Name = "S33_017"
        Me.S33_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_017.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_017.Top = 7.438!
        Me.S33_017.Width = 1.0!
        '
        'S33_027
        '
        Me.S33_027.Height = 0.1875!
        Me.S33_027.HyperLink = Nothing
        Me.S33_027.Left = 3.247002!
        Me.S33_027.Name = "S33_027"
        Me.S33_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_027.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_027.Top = 7.438!
        Me.S33_027.Width = 1.0!
        '
        'Label174
        '
        Me.Label174.Height = 0.1875!
        Me.Label174.HyperLink = Nothing
        Me.Label174.Left = 3.251002!
        Me.Label174.Name = "Label174"
        Me.Label174.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label174.Text = "kip*in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Zcx* (Fyc - Pu_colPositive/ Agc) * if(top story,1 else 2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label174.Top = 5.502!
        Me.Label174.Width = 4.305999!
        '
        'S33_008
        '
        Me.S33_008.Height = 0.1875!
        Me.S33_008.HyperLink = Nothing
        Me.S33_008.Left = 2.249999!
        Me.S33_008.Name = "S33_008"
        Me.S33_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_008.Top = 5.502001!
        Me.S33_008.Width = 1.0!
        '
        'Label178
        '
        Me.Label178.Height = 0.1875!
        Me.Label178.HyperLink = Nothing
        Me.Label178.Left = 4.249999!
        Me.Label178.Name = "Label178"
        Me.Label178.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label178.Text = "kip*in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Mpr + Muv" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label178.Top = 7.626!
        Me.Label178.Width = 3.313!
        '
        'Label179
        '
        Me.Label179.Height = 0.188!
        Me.Label179.HyperLink = Nothing
        Me.Label179.Left = 0.000002145767!
        Me.Label179.Name = "Label179"
        Me.Label179.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label179.Text = "ΣMpb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label179.Top = 7.626!
        Me.Label179.Width = 2.2!
        '
        'Label183
        '
        Me.Label183.Height = 0.188!
        Me.Label183.HyperLink = Nothing
        Me.Label183.Left = 0.000002145767!
        Me.Label183.Name = "Label183"
        Me.Label183.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label183.Text = "SCWB_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label183.Top = 7.814002!
        Me.Label183.Width = 2.25!
        '
        'S33_019
        '
        Me.S33_019.Height = 0.1875!
        Me.S33_019.HyperLink = Nothing
        Me.S33_019.Left = 2.251001!
        Me.S33_019.Name = "S33_019"
        Me.S33_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_019.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_019.Top = 7.814002!
        Me.S33_019.Width = 1.0!
        '
        'S33_029
        '
        Me.S33_029.Height = 0.1875!
        Me.S33_029.HyperLink = Nothing
        Me.S33_029.Left = 3.249001!
        Me.S33_029.Name = "S33_029"
        Me.S33_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_029.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_029.Top = 7.814002!
        Me.S33_029.Width = 1.0!
        '
        'Label186
        '
        Me.Label186.Height = 0.188!
        Me.Label186.HyperLink = Nothing
        Me.Label186.Left = 0.000002145767!
        Me.Label186.Name = "Label186"
        Me.Label186.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label186.Text = "SCWB_SUM_total=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label186.Top = 8.002002!
        Me.Label186.Width = 2.25!
        '
        'Line17
        '
        Me.Line17.Height = 0!
        Me.Line17.Left = 2.251001!
        Me.Line17.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 7.814002!
        Me.Line17.Width = 1.999999!
        Me.Line17.X1 = 2.251001!
        Me.Line17.X2 = 4.251!
        Me.Line17.Y1 = 7.814002!
        Me.Line17.Y2 = 7.814002!
        '
        'Line28
        '
        Me.Line28.Height = 0!
        Me.Line28.Left = 2.250002!
        Me.Line28.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line28.LineWeight = 1.0!
        Me.Line28.Name = "Line28"
        Me.Line28.Top = 7.436001!
        Me.Line28.Width = 1.999999!
        Me.Line28.X1 = 2.250002!
        Me.Line28.X2 = 4.250001!
        Me.Line28.Y1 = 7.436001!
        Me.Line28.Y2 = 7.436001!
        '
        'Line29
        '
        Me.Line29.Height = 0!
        Me.Line29.Left = 2.250002!
        Me.Line29.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line29.LineWeight = 1.0!
        Me.Line29.Name = "Line29"
        Me.Line29.Top = 7.250001!
        Me.Line29.Width = 1.999999!
        Me.Line29.X1 = 2.250002!
        Me.Line29.X2 = 4.250001!
        Me.Line29.Y1 = 7.250001!
        Me.Line29.Y2 = 7.250001!
        '
        'Line30
        '
        Me.Line30.Height = 0!
        Me.Line30.Left = 2.250002!
        Me.Line30.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line30.LineWeight = 1.0!
        Me.Line30.Name = "Line30"
        Me.Line30.Top = 7.062004!
        Me.Line30.Width = 1.999999!
        Me.Line30.X1 = 2.250002!
        Me.Line30.X2 = 4.250001!
        Me.Line30.Y1 = 7.062004!
        Me.Line30.Y2 = 7.062004!
        '
        'Line31
        '
        Me.Line31.Height = 0!
        Me.Line31.Left = 2.250002!
        Me.Line31.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line31.LineWeight = 1.0!
        Me.Line31.Name = "Line31"
        Me.Line31.Top = 6.875001!
        Me.Line31.Width = 1.999999!
        Me.Line31.X1 = 2.250002!
        Me.Line31.X2 = 4.250001!
        Me.Line31.Y1 = 6.875001!
        Me.Line31.Y2 = 6.875001!
        '
        'Line32
        '
        Me.Line32.Height = 0!
        Me.Line32.Left = 2.250002!
        Me.Line32.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line32.LineWeight = 1.0!
        Me.Line32.Name = "Line32"
        Me.Line32.Top = 6.687004!
        Me.Line32.Width = 1.999999!
        Me.Line32.X1 = 2.250002!
        Me.Line32.X2 = 4.250001!
        Me.Line32.Y1 = 6.687004!
        Me.Line32.Y2 = 6.687004!
        '
        'Line33
        '
        Me.Line33.Height = 0!
        Me.Line33.Left = 2.252002!
        Me.Line33.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line33.LineWeight = 1.0!
        Me.Line33.Name = "Line33"
        Me.Line33.Top = 6.501!
        Me.Line33.Width = 1.999999!
        Me.Line33.X1 = 2.252002!
        Me.Line33.X2 = 4.252001!
        Me.Line33.Y1 = 6.501!
        Me.Line33.Y2 = 6.501!
        '
        'Line34
        '
        Me.Line34.Height = 0!
        Me.Line34.Left = 2.252002!
        Me.Line34.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line34.LineWeight = 1.0!
        Me.Line34.Name = "Line34"
        Me.Line34.Top = 6.313!
        Me.Line34.Width = 1.999999!
        Me.Line34.X1 = 2.252002!
        Me.Line34.X2 = 4.252001!
        Me.Line34.Y1 = 6.313!
        Me.Line34.Y2 = 6.313!
        '
        'Line38
        '
        Me.Line38.Height = 0!
        Me.Line38.Left = 2.25!
        Me.Line38.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line38.LineWeight = 1.0!
        Me.Line38.Name = "Line38"
        Me.Line38.Top = 3.938!
        Me.Line38.Width = 2.0!
        Me.Line38.X1 = 2.25!
        Me.Line38.X2 = 4.25!
        Me.Line38.Y1 = 3.938!
        Me.Line38.Y2 = 3.938!
        '
        'Line39
        '
        Me.Line39.Height = 0!
        Me.Line39.Left = 2.25!
        Me.Line39.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line39.LineWeight = 1.0!
        Me.Line39.Name = "Line39"
        Me.Line39.Top = 3.751001!
        Me.Line39.Width = 2.0!
        Me.Line39.X1 = 2.25!
        Me.Line39.X2 = 4.25!
        Me.Line39.Y1 = 3.751001!
        Me.Line39.Y2 = 3.751001!
        '
        'Line40
        '
        Me.Line40.Height = 0!
        Me.Line40.Left = 2.25!
        Me.Line40.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line40.LineWeight = 1.0!
        Me.Line40.Name = "Line40"
        Me.Line40.Top = 3.564!
        Me.Line40.Width = 2.0!
        Me.Line40.X1 = 2.25!
        Me.Line40.X2 = 4.25!
        Me.Line40.Y1 = 3.564!
        Me.Line40.Y2 = 3.564!
        '
        'Line41
        '
        Me.Line41.Height = 0!
        Me.Line41.Left = 2.25!
        Me.Line41.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line41.LineWeight = 1.0!
        Me.Line41.Name = "Line41"
        Me.Line41.Top = 3.376001!
        Me.Line41.Width = 2.0!
        Me.Line41.X1 = 2.25!
        Me.Line41.X2 = 4.25!
        Me.Line41.Y1 = 3.376001!
        Me.Line41.Y2 = 3.376001!
        '
        'Line42
        '
        Me.Line42.Height = 0!
        Me.Line42.Left = 2.25!
        Me.Line42.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line42.LineWeight = 1.0!
        Me.Line42.Name = "Line42"
        Me.Line42.Top = 3.189!
        Me.Line42.Width = 2.0!
        Me.Line42.X1 = 2.25!
        Me.Line42.X2 = 4.25!
        Me.Line42.Y1 = 3.189!
        Me.Line42.Y2 = 3.189!
        '
        'Line43
        '
        Me.Line43.Height = 0!
        Me.Line43.Left = 2.25!
        Me.Line43.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line43.LineWeight = 1.0!
        Me.Line43.Name = "Line43"
        Me.Line43.Top = 3.000001!
        Me.Line43.Width = 2.0!
        Me.Line43.X1 = 2.25!
        Me.Line43.X2 = 4.25!
        Me.Line43.Y1 = 3.000001!
        Me.Line43.Y2 = 3.000001!
        '
        'Line44
        '
        Me.Line44.Height = 0!
        Me.Line44.Left = 2.25!
        Me.Line44.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line44.LineWeight = 1.0!
        Me.Line44.Name = "Line44"
        Me.Line44.Top = 2.813!
        Me.Line44.Width = 2.0!
        Me.Line44.X1 = 2.25!
        Me.Line44.X2 = 4.25!
        Me.Line44.Y1 = 2.813!
        Me.Line44.Y2 = 2.813!
        '
        'Line45
        '
        Me.Line45.Height = 0!
        Me.Line45.Left = 2.25!
        Me.Line45.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line45.LineWeight = 1.0!
        Me.Line45.Name = "Line45"
        Me.Line45.Top = 2.625001!
        Me.Line45.Width = 2.0!
        Me.Line45.X1 = 2.25!
        Me.Line45.X2 = 4.25!
        Me.Line45.Y1 = 2.625001!
        Me.Line45.Y2 = 2.625001!
        '
        'Line46
        '
        Me.Line46.Height = 0!
        Me.Line46.Left = 2.25!
        Me.Line46.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line46.LineWeight = 1.0!
        Me.Line46.Name = "Line46"
        Me.Line46.Top = 2.438!
        Me.Line46.Width = 2.0!
        Me.Line46.X1 = 2.25!
        Me.Line46.X2 = 4.25!
        Me.Line46.Y1 = 2.438!
        Me.Line46.Y2 = 2.438!
        '
        'Line47
        '
        Me.Line47.Height = 0!
        Me.Line47.Left = 2.25!
        Me.Line47.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line47.LineWeight = 1.0!
        Me.Line47.Name = "Line47"
        Me.Line47.Top = 2.25!
        Me.Line47.Width = 2.0!
        Me.Line47.X1 = 2.25!
        Me.Line47.X2 = 4.25!
        Me.Line47.Y1 = 2.25!
        Me.Line47.Y2 = 2.25!
        '
        'Line48
        '
        Me.Line48.Height = 0!
        Me.Line48.Left = 2.25!
        Me.Line48.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line48.LineWeight = 1.0!
        Me.Line48.Name = "Line48"
        Me.Line48.Top = 2.063!
        Me.Line48.Width = 2.0!
        Me.Line48.X1 = 2.25!
        Me.Line48.X2 = 4.25!
        Me.Line48.Y1 = 2.063!
        Me.Line48.Y2 = 2.063!
        '
        'Line49
        '
        Me.Line49.Height = 1.875!
        Me.Line49.Left = 2.25!
        Me.Line49.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line49.LineWeight = 1.0!
        Me.Line49.Name = "Line49"
        Me.Line49.Top = 2.063!
        Me.Line49.Width = 0!
        Me.Line49.X1 = 2.25!
        Me.Line49.X2 = 2.25!
        Me.Line49.Y1 = 2.063!
        Me.Line49.Y2 = 3.938!
        '
        'Line50
        '
        Me.Line50.Height = 1.875!
        Me.Line50.Left = 3.25!
        Me.Line50.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line50.LineWeight = 1.0!
        Me.Line50.Name = "Line50"
        Me.Line50.Top = 2.063!
        Me.Line50.Width = 0!
        Me.Line50.X1 = 3.25!
        Me.Line50.X2 = 3.25!
        Me.Line50.Y1 = 2.063!
        Me.Line50.Y2 = 3.938!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 0!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label38.Text = "Stiffener (Y/N):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label38.Top = 1.125!
        Me.Label38.Width = 1.562!
        '
        'S31_006
        '
        Me.S31_006.Height = 0.1875!
        Me.S31_006.HyperLink = Nothing
        Me.S31_006.Left = 1.562!
        Me.S31_006.Name = "S31_006"
        Me.S31_006.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_006.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_006.Top = 1.125!
        Me.S31_006.Width = 1.0!
        '
        'Label55
        '
        Me.Label55.Height = 0.1875!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 2.562001!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label55.Text = "Left Beam size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label55.Top = 0.56!
        Me.Label55.Width = 1.375!
        '
        'S31_010
        '
        Me.S31_010.Height = 0.1875!
        Me.S31_010.HyperLink = Nothing
        Me.S31_010.Left = 3.937!
        Me.S31_010.Name = "S31_010"
        Me.S31_010.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_010.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_010.Top = 0.56!
        Me.S31_010.Width = 1.0!
        '
        'Label59
        '
        Me.Label59.Height = 0.1875!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 2.562!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label59.Text = "Story Above=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label59.Top = 0.375!
        Me.Label59.Width = 1.375!
        '
        'S31_009
        '
        Me.S31_009.Height = 0.1875!
        Me.S31_009.HyperLink = Nothing
        Me.S31_009.Left = 3.937!
        Me.S31_009.Name = "S31_009"
        Me.S31_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S31_009.Text = "G1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_009.Top = 0.375!
        Me.S31_009.Width = 1.0!
        '
        'Label66
        '
        Me.Label66.Height = 0.1875!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 2.563!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label66.Text = "Current Story=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label66.Top = 0.187!
        Me.Label66.Width = 1.375!
        '
        'S31_008
        '
        Me.S31_008.Height = 0.1875!
        Me.S31_008.HyperLink = Nothing
        Me.S31_008.Left = 3.938!
        Me.S31_008.Name = "S31_008"
        Me.S31_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S31_008.Text = "5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_008.Top = 0.187!
        Me.S31_008.Width = 1.0!
        '
        'Line51
        '
        Me.Line51.Height = 1.875!
        Me.Line51.Left = 4.25!
        Me.Line51.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line51.LineWeight = 1.0!
        Me.Line51.Name = "Line51"
        Me.Line51.Top = 2.063!
        Me.Line51.Width = 0!
        Me.Line51.X1 = 4.25!
        Me.Line51.X2 = 4.25!
        Me.Line51.Y1 = 2.063!
        Me.Line51.Y2 = 3.938!
        '
        'Label175
        '
        Me.Label175.Height = 0.1875!
        Me.Label175.HyperLink = Nothing
        Me.Label175.Left = 0!
        Me.Label175.Name = "Label175"
        Me.Label175.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label175.Text = "ΣMp_col=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label175.Top = 5.503!
        Me.Label175.Width = 2.25!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 3.25!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label9.Text = "AISC 341-16 E3.4a Exception (a)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 6.065!
        Me.Label9.Width = 3.313!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 4.244002!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label19.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= ΣMpb / ΣMp_col "
        Me.Label19.Top = 7.813!
        Me.Label19.Width = 3.623!
        '
        'Label22
        '
        Me.Label22.Height = 0.188!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 0!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label22.Text = "Top Story?=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label22.Top = 5.316!
        Me.Label22.Width = 2.25!
        '
        'Label24
        '
        Me.Label24.Height = 0.188!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 0!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label24.Text = "Pu<0.3*Pc?=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label24.Top = 6.068!
        Me.Label24.Width = 2.25!
        '
        'Shape19
        '
        Me.Shape19.Height = 0.1875!
        Me.Shape19.Left = 1.563!
        Me.Shape19.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape19.Name = "Shape19"
        Me.Shape19.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape19.Top = 0.188!
        Me.Shape19.Width = 1.0!
        '
        'Line37
        '
        Me.Line37.Height = 2.062997!
        Me.Line37.Left = 3.250002!
        Me.Line37.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line37.LineWeight = 1.0!
        Me.Line37.Name = "Line37"
        Me.Line37.Top = 6.313003!
        Me.Line37.Width = 0.000001192093!
        Me.Line37.X1 = 3.250002!
        Me.Line37.X2 = 3.250003!
        Me.Line37.Y1 = 6.313003!
        Me.Line37.Y2 = 8.376!
        '
        'Line36
        '
        Me.Line36.Height = 1.688999!
        Me.Line36.Left = 4.250001!
        Me.Line36.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line36.LineWeight = 1.0!
        Me.Line36.Name = "Line36"
        Me.Line36.Top = 6.313003!
        Me.Line36.Width = 0.000001907349!
        Me.Line36.X1 = 4.250001!
        Me.Line36.X2 = 4.250003!
        Me.Line36.Y1 = 6.313003!
        Me.Line36.Y2 = 8.002002!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 2.562!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label2.Text = "Left Beam ID=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 0.751!
        Me.Label2.Width = 1.375!
        '
        'S31_011
        '
        Me.S31_011.Height = 0.1875!
        Me.S31_011.HyperLink = Nothing
        Me.S31_011.Left = 3.937!
        Me.S31_011.Name = "S31_011"
        Me.S31_011.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_011.Text = "31"
        Me.S31_011.Top = 0.751!
        Me.S31_011.Width = 1.0!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 5.125!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "Right Beam ID=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 0.752!
        Me.Label7.Width = 1.375!
        '
        'S31_020
        '
        Me.S31_020.Height = 0.1875!
        Me.S31_020.HyperLink = Nothing
        Me.S31_020.Left = 6.5!
        Me.S31_020.Name = "S31_020"
        Me.S31_020.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_020.Text = "32"
        Me.S31_020.Top = 0.752!
        Me.S31_020.Width = 1.0!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 2.563!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label15.Text = "No. of conn.=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label15.Top = 1.312!
        Me.Label15.Width = 1.375!
        '
        'S31_014
        '
        Me.S31_014.Height = 0.1875!
        Me.S31_014.HyperLink = Nothing
        Me.S31_014.Left = 3.938!
        Me.S31_014.Name = "S31_014"
        Me.S31_014.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_014.Text = "2"
        Me.S31_014.Top = 1.312!
        Me.S31_014.Width = 1.0!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 4.939001!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label20.Text = "No. of conn.=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label20.Top = 1.312!
        Me.Label20.Width = 1.562!
        '
        'S31_023
        '
        Me.S31_023.Height = 0.1875!
        Me.S31_023.HyperLink = Nothing
        Me.S31_023.Left = 6.501!
        Me.S31_023.Name = "S31_023"
        Me.S31_023.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_023.Text = "2"
        Me.S31_023.Top = 1.312!
        Me.S31_023.Width = 1.0!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 2.249!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 12.25!
        Me.Line5.Width = 0.9979999!
        Me.Line5.X1 = 2.249!
        Me.Line5.X2 = 3.247!
        Me.Line5.Y1 = 12.25!
        Me.Line5.Y2 = 12.25!
        '
        'Label3
        '
        Me.Label3.Height = 0.188!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.000002145767!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label3.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label3.Top = 8.189002!
        Me.Label3.Width = 2.25!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 4.250002!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label12.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Note: SCWB not applicable at top story or IMF Frame or R=3"
        Me.Label12.Top = 8.001002!
        Me.Label12.Width = 3.626001!
        '
        'Line35
        '
        Me.Line35.Height = 2.061998!
        Me.Line35.Left = 2.252003!
        Me.Line35.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line35.LineWeight = 1.0!
        Me.Line35.Name = "Line35"
        Me.Line35.Top = 6.314002!
        Me.Line35.Width = 0!
        Me.Line35.X1 = 2.252003!
        Me.Line35.X2 = 2.252003!
        Me.Line35.Y1 = 6.314002!
        Me.Line35.Y2 = 8.376!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.248002!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label16.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=YES, IF Current Story = Top Story,  Note: AISC 341-16 E3.4a Exception (a) (1)"
        Me.Label16.Top = 5.313!
        Me.Label16.Width = 4.302998!
        '
        'Label21
        '
        Me.Label21.Height = 0.188!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label21.Text = "Column Pc="
        Me.Label21.Top = 5.879!
        Me.Label21.Width = 2.25!
        '
        'S33_010
        '
        Me.S33_010.Height = 0.1875!
        Me.S33_010.HyperLink = Nothing
        Me.S33_010.Left = 2.249999!
        Me.S33_010.Name = "S33_010"
        Me.S33_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S33_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_010.Top = 5.879!
        Me.S33_010.Width = 1.0!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 2.247002!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 7.625001!
        Me.Line6.Width = 2.000001!
        Me.Line6.X1 = 2.247002!
        Me.Line6.X2 = 4.247003!
        Me.Line6.Y1 = 7.625001!
        Me.Line6.Y2 = 7.625001!
        '
        'Line7
        '
        Me.Line7.Height = 0!
        Me.Line7.Left = 2.250002!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 8.002002!
        Me.Line7.Width = 2.0!
        Me.Line7.X1 = 2.250002!
        Me.Line7.X2 = 4.250002!
        Me.Line7.Y1 = 8.002002!
        Me.Line7.Y2 = 8.002002!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 4.241002!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label27.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Ok, if SCWB_SUM_total <= SCWB_SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label27.Top = 8.189!
        Me.Label27.Width = 3.626001!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 3.254!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label26.Text = "in^3" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label26.Top = 5.129!
        Me.Label26.Width = 1.746!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 3.254!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label32.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label32.Top = 4.942!
        Me.Label32.Width = 1.746!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 3.254!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label34.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined"
        Me.Label34.Top = 4.754!
        Me.Label34.Width = 1.746!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 3.254!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label39.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label39.Top = 4.566!
        Me.Label39.Width = 1.746!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 3.254!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label42.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label42.Top = 4.378!
        Me.Label42.Width = 1.746!
        '
        'Label44
        '
        Me.Label44.Height = 0.1875!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 3.254!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label44.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined"
        Me.Label44.Top = 4.19!
        Me.Label44.Width = 1.746!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 3.251002!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label46.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Fyc* Agc"
        Me.Label46.Top = 5.875!
        Me.Label46.Width = 3.313!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 0.001000047!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label49.Text = "Doubler (Y/N):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label49.Top = 1.312!
        Me.Label49.Width = 1.562!
        '
        'S31_007
        '
        Me.S31_007.Height = 0.1875!
        Me.S31_007.HyperLink = Nothing
        Me.S31_007.Left = 1.563!
        Me.S31_007.Name = "S31_007"
        Me.S31_007.Style = "color: Brown; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S31_007.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_007.Top = 1.312!
        Me.S31_007.Width = 1.0!
        '
        'S33_000
        '
        Me.S33_000.Height = 0.1875!
        Me.S33_000.HyperLink = Nothing
        Me.S33_000.Left = 3.254!
        Me.S33_000.Name = "S33_000"
        Me.S33_000.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: justify; vertical-" &
    "align: middle; ddo-char-set: 0"
        Me.S33_000.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_000.Top = 4.001!
        Me.S33_000.Width = 4.309!
        '
        'Label51
        '
        Me.Label51.Height = 0.188!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 5.0!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label51.Text = "Col b/2/t="
        Me.Label51.Top = 4.376!
        Me.Label51.Width = 0.7390013!
        '
        'S33_030
        '
        Me.S33_030.Height = 0.188!
        Me.S33_030.HyperLink = Nothing
        Me.S33_030.Left = 5.739!
        Me.S33_030.Name = "S33_030"
        Me.S33_030.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S33_030.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_030.Top = 4.378!
        Me.S33_030.Width = 0.75!
        '
        'Label57
        '
        Me.Label57.Height = 0.188!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 5.0!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "font-size: 8.25pt; text-align: center; vertical-align: middle"
        Me.Label57.Text = "AISC 360     Table B4.1b"
        Me.Label57.Top = 4.19!
        Me.Label57.Width = 2.739!
        '
        'S33_032
        '
        Me.S33_032.Height = 0.188!
        Me.S33_032.HyperLink = Nothing
        Me.S33_032.Left = 6.501!
        Me.S33_032.Name = "S33_032"
        Me.S33_032.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: left; vertical-a" &
    "lign: middle; ddo-char-set: 1"
        Me.S33_032.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_032.Top = 4.374!
        Me.S33_032.Width = 1.238!
        '
        'Label62
        '
        Me.Label62.Height = 0.188!
        Me.Label62.HyperLink = Nothing
        Me.Label62.Left = 5.0!
        Me.Label62.Name = "Label62"
        Me.Label62.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label62.Text = "Col h/tw="
        Me.Label62.Top = 4.566!
        Me.Label62.Width = 0.7390014!
        '
        'S33_031
        '
        Me.S33_031.Height = 0.188!
        Me.S33_031.HyperLink = Nothing
        Me.S33_031.Left = 5.739!
        Me.S33_031.Name = "S33_031"
        Me.S33_031.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S33_031.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_031.Top = 4.564!
        Me.S33_031.Width = 0.75!
        '
        'S33_033
        '
        Me.S33_033.Height = 0.188!
        Me.S33_033.HyperLink = Nothing
        Me.S33_033.Left = 6.501!
        Me.S33_033.Name = "S33_033"
        Me.S33_033.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: left; vertical-a" &
    "lign: middle; ddo-char-set: 1"
        Me.S33_033.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S33_033.Top = 4.564!
        Me.S33_033.Width = 1.238!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 2.256002!
        Me.Line1.LineColor = System.Drawing.Color.DimGray
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 8.190001!
        Me.Line1.Width = 0.9980001!
        Me.Line1.X1 = 2.256002!
        Me.Line1.X2 = 3.254002!
        Me.Line1.Y1 = 8.190001!
        Me.Line1.Y2 = 8.190001!
        '
        'Label53
        '
        Me.Label53.Height = 0.1875!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 5.126!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label53.Text = "R=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label53.Top = 0!
        Me.Label53.Width = 1.375!
        '
        'S31_026
        '
        Me.S31_026.Height = 0.1875!
        Me.S31_026.HyperLink = Nothing
        Me.S31_026.Left = 6.489!
        Me.S31_026.Name = "S31_026"
        Me.S31_026.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S31_026.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S31_026.Top = 0.0009999275!
        Me.S31_026.Width = 1.0!
        '
        'US_CC1
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.75!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S33_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label334, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S32_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label170, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label171, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label174, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label178, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label183, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label186, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label175, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_000, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S33_033, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S31_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label43 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label47 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label334 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label48 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label50 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label56 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label65 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label67 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label69 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label71 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S32_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label73 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label74 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label78 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label81 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label83 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label84 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label86 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label90 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label91 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label92 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label96 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label98 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label100 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label107 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label111 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label115 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label119 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label121 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label170 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label171 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label174 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label175 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label178 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label179 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label183 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label186 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line17 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line28 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line29 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line30 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line31 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line32 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line33 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line34 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line38 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line39 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line40 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line41 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line42 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line43 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line44 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line45 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line46 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line47 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line48 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line49 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line50 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line51 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label94 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label55 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape19 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S31_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S33_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label80 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line37 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line36 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line35 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label44 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_000 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label51 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label62 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S33_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label53 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S31_026 As GrapeCity.ActiveReports.SectionReportModel.Label
End Class
