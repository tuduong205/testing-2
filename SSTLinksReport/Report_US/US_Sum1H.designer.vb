﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class US_Sum1H
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    Private WithEvents PageFooter As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_Sum1H))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.sub2 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.GroupHeader1 = New GrapeCity.ActiveReports.SectionReportModel.GroupHeader()
        Me.lbDGVName = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbJobID = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbJobName = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbPrintedDate = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.GroupFooter1 = New GrapeCity.ActiveReports.SectionReportModel.GroupFooter()
        Me.PageHeader1 = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.PageFooter1 = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        Me.GroupHeader2 = New GrapeCity.ActiveReports.SectionReportModel.GroupHeader()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.GroupFooter2 = New GrapeCity.ActiveReports.SectionReportModel.GroupFooter()
        CType(Me.lbDGVName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.sub2})
        Me.Detail.Height = 0.25!
        Me.Detail.Name = "Detail"
        '
        'sub2
        '
        Me.sub2.CloseBorder = False
        Me.sub2.Height = 0.25!
        Me.sub2.Left = 0!
        Me.sub2.Name = "sub2"
        Me.sub2.Report = Nothing
        Me.sub2.ReportName = "sub2"
        Me.sub2.Top = 0!
        Me.sub2.Width = 10.0!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.lbDGVName, Me.lbJobID, Me.lbJobName, Me.Label2, Me.Label3, Me.lbPrintedDate})
        Me.GroupHeader1.Height = 0.7499999!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'lbDGVName
        '
        Me.lbDGVName.Height = 0.202!
        Me.lbDGVName.HyperLink = Nothing
        Me.lbDGVName.Left = 0!
        Me.lbDGVName.Name = "lbDGVName"
        Me.lbDGVName.Style = "font-size: 9.75pt; font-weight: bold"
        Me.lbDGVName.Text = "Initial Link Selection"
        Me.lbDGVName.Top = 0.527!
        Me.lbDGVName.Width = 8.0!
        '
        'lbJobID
        '
        Me.lbJobID.Height = 0.2!
        Me.lbJobID.HyperLink = Nothing
        Me.lbJobID.Left = 0.5!
        Me.lbJobID.Name = "lbJobID"
        Me.lbJobID.Style = "font-size: 9.75pt; font-weight: normal"
        Me.lbJobID.Text = "ES-123456" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobID.Top = 0.187!
        Me.lbJobID.Width = 1.75!
        '
        'lbJobName
        '
        Me.lbJobName.Height = 0.2!
        Me.lbJobName.HyperLink = Nothing
        Me.lbJobName.Left = 0.75!
        Me.lbJobName.Name = "lbJobName"
        Me.lbJobName.Style = "font-size: 9.75pt; font-weight: normal"
        Me.lbJobName.Text = "Test Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobName.Top = 0!
        Me.lbJobName.Width = 4.063!
        '
        'Label2
        '
        Me.Label2.Height = 0.2!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 9.75pt; font-weight: normal"
        Me.Label2.Text = "Job ID: " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 0.187!
        Me.Label2.Width = 0.5!
        '
        'Label3
        '
        Me.Label3.Height = 0.2!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 9.75pt; font-weight: normal"
        Me.Label3.Text = "Job Name:"
        Me.Label3.Top = 0!
        Me.Label3.Width = 0.75!
        '
        'lbPrintedDate
        '
        Me.lbPrintedDate.Height = 0.2!
        Me.lbPrintedDate.HyperLink = Nothing
        Me.lbPrintedDate.Left = 8.0!
        Me.lbPrintedDate.Name = "lbPrintedDate"
        Me.lbPrintedDate.Style = "font-size: 9.75pt; font-weight: normal; text-align: right"
        Me.lbPrintedDate.Text = "Date Printed: Jun 17, 2018" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbPrintedDate.Top = 0!
        Me.lbPrintedDate.Width = 1.999999!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.01041666!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'PageHeader1
        '
        Me.PageHeader1.Height = 0!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'PageFooter1
        '
        Me.PageFooter1.Height = 0!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Line1, Me.Line2, Me.Line4})
        Me.GroupHeader2.Height = 0.625!
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0.625!
        Me.Line1.Width = 10.0!
        Me.Line1.X1 = 0!
        Me.Line1.X2 = 10.0!
        Me.Line1.Y1 = 0.625!
        Me.Line1.Y2 = 0.625!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 0!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0!
        Me.Line2.Width = 10.5!
        Me.Line2.X1 = 0!
        Me.Line2.X2 = 10.5!
        Me.Line2.Y1 = 0!
        Me.Line2.Y2 = 0!
        '
        'Line4
        '
        Me.Line4.Height = 0.625!
        Me.Line4.Left = 10.0!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 0!
        Me.Line4.Width = 0!
        Me.Line4.X1 = 10.0!
        Me.Line4.X2 = 10.0!
        Me.Line4.Y1 = 0!
        Me.Line4.Y2 = 0.625!
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Height = 0!
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'US_Sum1H
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 11.0!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.GroupHeader2)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter2)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.lbDGVName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents GroupHeader1 As GrapeCity.ActiveReports.SectionReportModel.GroupHeader
    Private WithEvents GroupFooter1 As GrapeCity.ActiveReports.SectionReportModel.GroupFooter
    Private WithEvents sub2 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageHeader1 As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents PageFooter1 As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    Private WithEvents lbDGVName As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobID As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobName As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbPrintedDate As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents GroupHeader2 As GrapeCity.ActiveReports.SectionReportModel.GroupHeader
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents GroupFooter2 As GrapeCity.ActiveReports.SectionReportModel.GroupFooter
End Class
