﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_SPC1
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_SPC1))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S42_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label48 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label51 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label55 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label61 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label64 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label67 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label70 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label72 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label63 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label73 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label74 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label77 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label79 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label80 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label82 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label83 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label85 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label86 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label88 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label725 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label726 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5S41_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S42_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label53 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S41_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        CType(Me.S42_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label725, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label726, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5S41_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S42_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S41_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S42_025, Me.S41_001, Me.S41_002, Me.S41_003, Me.S41_004, Me.S41_005, Me.S41_007, Me.S41_008, Me.S41_009, Me.Label1, Me.headerS41, Me.Label6, Me.Label8, Me.Label14, Me.Label16, Me.Label3, Me.Label18, Me.Label10, Me.Label11, Me.Label12, Me.Label13, Me.Label21, Me.S41_011, Me.Label23, Me.S41_010, Me.headerS42, Me.Label28, Me.S42_001, Me.Label30, Me.Label31, Me.S42_002, Me.Label33, Me.Label34, Me.S42_003, Me.Label36, Me.Label37, Me.S42_004, Me.Label39, Me.Label46, Me.S42_005, Me.Label48, Me.Label49, Me.S42_006, Me.Label51, Me.Label52, Me.S42_007, Me.Label54, Me.Label55, Me.S42_013, Me.Label57, Me.Label58, Me.S42_014, Me.Label60, Me.Label61, Me.S42_015, Me.Label64, Me.S42_016, Me.Label66, Me.Label67, Me.S42_017, Me.Label70, Me.S42_018, Me.Label72, Me.Label63, Me.S42_019, Me.Label73, Me.Label74, Me.S42_020, Me.Label76, Me.Label77, Me.S42_021, Me.Label79, Me.Label80, Me.S42_022, Me.Label82, Me.Label83, Me.S42_023, Me.Label85, Me.Label86, Me.S42_024, Me.Label88, Me.Label725, Me.Label726, Me.S41_006, Me.Label4, Me.Label5S41_012, Me.S41_013, Me.S41_014, Me.Label15, Me.Label17, Me.Label19, Me.Label20, Me.Label22, Me.Label26, Me.S41_015, Me.S41_016, Me.Label25, Me.Label2, Me.S42_008, Me.Label7, Me.Label9, Me.S42_009, Me.Label27, Me.Label32, Me.S42_010, Me.Label38, Me.Label40, Me.S42_012, Me.Label42, Me.Label5, Me.S42_011, Me.Label29, Me.Label35, Me.Shape2, Me.Shape1, Me.Label53, Me.S41_017})
        Me.Detail.Height = 7.5!
        Me.Detail.Name = "Detail"
        '
        'S42_025
        '
        Me.S42_025.Height = 0.1875!
        Me.S42_025.HyperLink = Nothing
        Me.S42_025.Left = 2.25!
        Me.S42_025.Name = "S42_025"
        Me.S42_025.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S42_025.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_025.Top = 6.377!
        Me.S42_025.Width = 1.0!
        '
        'S41_001
        '
        Me.S41_001.Height = 0.1875!
        Me.S41_001.HyperLink = Nothing
        Me.S41_001.Left = 1.562!
        Me.S41_001.Name = "S41_001"
        Me.S41_001.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.S41_001.Tag = "1"
        Me.S41_001.Text = "299"
        Me.S41_001.Top = 0.374!
        Me.S41_001.Width = 1.0!
        '
        'S41_002
        '
        Me.S41_002.Height = 0.1875!
        Me.S41_002.HyperLink = Nothing
        Me.S41_002.Left = 1.562!
        Me.S41_002.Name = "S41_002"
        Me.S41_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: bold; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 0"
        Me.S41_002.Tag = "2"
        Me.S41_002.Text = "W18X192" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_002.Top = 0.562!
        Me.S41_002.Width = 1.0!
        '
        'S41_003
        '
        Me.S41_003.Height = 0.1875!
        Me.S41_003.HyperLink = Nothing
        Me.S41_003.Left = 1.562!
        Me.S41_003.Name = "S41_003"
        Me.S41_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_003.Tag = "3"
        Me.S41_003.Text = "206.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_003.Top = 0.75!
        Me.S41_003.Width = 1.0!
        '
        'S41_004
        '
        Me.S41_004.Height = 0.1875!
        Me.S41_004.HyperLink = Nothing
        Me.S41_004.Left = 1.562!
        Me.S41_004.Name = "S41_004"
        Me.S41_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_004.Tag = "4"
        Me.S41_004.Text = "206.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_004.Top = 0.937!
        Me.S41_004.Width = 1.0!
        '
        'S41_005
        '
        Me.S41_005.Height = 0.1875!
        Me.S41_005.HyperLink = Nothing
        Me.S41_005.Left = 1.562!
        Me.S41_005.Name = "S41_005"
        Me.S41_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_005.Tag = "5"
        Me.S41_005.Text = "YL4-3.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_005.Top = 1.124!
        Me.S41_005.Width = 1.0!
        '
        'S41_007
        '
        Me.S41_007.Height = 0.1875!
        Me.S41_007.HyperLink = Nothing
        Me.S41_007.Left = 3.812!
        Me.S41_007.Name = "S41_007"
        Me.S41_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_007.Text = "0.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_007.Top = 0.375!
        Me.S41_007.Width = 1.0!
        '
        'S41_008
        '
        Me.S41_008.Height = 0.1875!
        Me.S41_008.HyperLink = Nothing
        Me.S41_008.Left = 3.812!
        Me.S41_008.Name = "S41_008"
        Me.S41_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_008.Text = "0.875" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_008.Top = 0.562!
        Me.S41_008.Width = 1.0!
        '
        'S41_009
        '
        Me.S41_009.Height = 0.1875!
        Me.S41_009.HyperLink = Nothing
        Me.S41_009.Left = 3.812!
        Me.S41_009.Name = "S41_009"
        Me.S41_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_009.Text = "A325_X" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_009.Top = 0.7500001!
        Me.S41_009.Width = 1.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "Beam Unique Name:"
        Me.Label1.Top = 0.374!
        Me.Label1.Width = 1.562!
        '
        'headerS41
        '
        Me.headerS41.Height = 0.1875!
        Me.headerS41.HyperLink = Nothing
        Me.headerS41.Left = 0!
        Me.headerS41.Name = "headerS41"
        Me.headerS41.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS41.Text = "4.1 CURRENT MEMBER:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS41.Top = 0!
        Me.headerS41.Width = 3.0!
        '
        'Label6
        '
        Me.Label6.Height = 0.188!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 2.562!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label6.Text = "Thickness (tsp) (in):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label6.Top = 0.375!
        Me.Label6.Width = 1.25!
        '
        'Label8
        '
        Me.Label8.Height = 0.188!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 2.562!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label8.Text = "Bolt Dia (db_sp):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label8.Top = 0.562!
        Me.Label8.Width = 1.25!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label14.Text = "Beam Size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label14.Top = 0.562!
        Me.Label14.Width = 1.562!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "Axial Pu (kips):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label16.Top = 0.75!
        Me.Label16.Width = 1.562!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label3.Text = "Shear Vu_bm (kips):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label3.Top = 0.937!
        Me.Label3.Width = 1.562!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 0!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label18.Text = "Link ID:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 1.124!
        Me.Label18.Width = 1.562!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 2.562!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label10.Text = "Shear Plate Info:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 0.187!
        Me.Label10.Width = 1.375!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 4.812!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label11.Text = "in"
        Me.Label11.Top = 0.375!
        Me.Label11.Width = 0.25!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 4.812!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label12.Text = "in"
        Me.Label12.Top = 0.5620003!
        Me.Label12.Width = 0.25!
        '
        'Label13
        '
        Me.Label13.Height = 0.188!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 2.562!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label13.Text = "Bolt Type:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label13.Top = 0.7500001!
        Me.Label13.Width = 1.25!
        '
        'Label21
        '
        Me.Label21.Height = 0.188!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 2.562001!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label21.Text = "n_Hbolts_SST:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label21.Top = 1.125!
        Me.Label21.Width = 1.25!
        '
        'S41_011
        '
        Me.S41_011.Height = 0.1875!
        Me.S41_011.HyperLink = Nothing
        Me.S41_011.Left = 3.812!
        Me.S41_011.Name = "S41_011"
        Me.S41_011.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_011.Text = "2.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_011.Top = 1.125!
        Me.S41_011.Width = 1.0!
        '
        'Label23
        '
        Me.Label23.Height = 0.188!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 2.562!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label23.Text = "n_Vbolts_SST:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label23.Top = 0.9380001!
        Me.Label23.Width = 1.25!
        '
        'S41_010
        '
        Me.S41_010.Height = 0.1875!
        Me.S41_010.HyperLink = Nothing
        Me.S41_010.Left = 3.812!
        Me.S41_010.Name = "S41_010"
        Me.S41_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_010.Text = "3.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_010.Top = 0.9380001!
        Me.S41_010.Width = 1.0!
        '
        'headerS42
        '
        Me.headerS42.Height = 0.1875!
        Me.headerS42.HyperLink = Nothing
        Me.headerS42.Left = 0!
        Me.headerS42.Name = "headerS42"
        Me.headerS42.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS42.Text = "4.2 SHEAR PLATE BOLT SIZE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS42.Top = 1.684!
        Me.headerS42.Width = 3.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 0!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label28.Text = "Beam Section (bmSize) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label28.Top = 1.872!
        Me.Label28.Width = 2.25!
        '
        'S42_001
        '
        Me.S42_001.Height = 0.1875!
        Me.S42_001.HyperLink = Nothing
        Me.S42_001.Left = 2.25!
        Me.S42_001.Name = "S42_001"
        Me.S42_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_001.Text = "W21X48" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_001.Top = 1.872!
        Me.S42_001.Width = 1.0!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 3.25!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label30.Text = "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label30.Top = 1.872!
        Me.Label30.Width = 4.75!
        '
        'Label31
        '
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 0!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label31.Text = "Beam Depth (db)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label31.Top = 2.059!
        Me.Label31.Width = 2.25!
        '
        'S42_002
        '
        Me.S42_002.Height = 0.1875!
        Me.S42_002.HyperLink = Nothing
        Me.S42_002.Left = 2.25!
        Me.S42_002.Name = "S42_002"
        Me.S42_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_002.Text = "20.40" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_002.Top = 2.059!
        Me.S42_002.Width = 1.0!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 3.25!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label33.Text = "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label33.Top = 2.059!
        Me.Label33.Width = 4.75!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 0!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label34.Text = "tbf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label34.Top = 2.247!
        Me.Label34.Width = 2.25!
        '
        'S42_003
        '
        Me.S42_003.Height = 0.1875!
        Me.S42_003.HyperLink = Nothing
        Me.S42_003.Left = 2.25!
        Me.S42_003.Name = "S42_003"
        Me.S42_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_003.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_003.Top = 2.247!
        Me.S42_003.Width = 1.0!
        '
        'Label36
        '
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 3.25!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label36.Text = "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label36.Top = 2.247!
        Me.Label36.Width = 4.75!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label37.Text = "tbw=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label37.Top = 2.434!
        Me.Label37.Width = 2.25!
        '
        'S42_004
        '
        Me.S42_004.Height = 0.1875!
        Me.S42_004.HyperLink = Nothing
        Me.S42_004.Left = 2.25!
        Me.S42_004.Name = "S42_004"
        Me.S42_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_004.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_004.Top = 2.434!
        Me.S42_004.Width = 1.0!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 3.25!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label39.Text = "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label39.Top = 2.434!
        Me.Label39.Width = 4.75!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 0!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label46.Text = "Fy_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label46.Top = 2.622!
        Me.Label46.Width = 2.25!
        '
        'S42_005
        '
        Me.S42_005.Height = 0.1875!
        Me.S42_005.HyperLink = Nothing
        Me.S42_005.Left = 2.25!
        Me.S42_005.Name = "S42_005"
        Me.S42_005.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S42_005.Text = "50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_005.Top = 2.622!
        Me.S42_005.Width = 1.0!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 3.25!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label48.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label48.Top = 2.622!
        Me.Label48.Width = 4.75!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 0!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label49.Text = "Fu_sp =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label49.Top = 2.81!
        Me.Label49.Width = 2.25!
        '
        'S42_006
        '
        Me.S42_006.Height = 0.1875!
        Me.S42_006.HyperLink = Nothing
        Me.S42_006.Left = 2.25!
        Me.S42_006.Name = "S42_006"
        Me.S42_006.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S42_006.Text = "65"
        Me.S42_006.Top = 2.81!
        Me.S42_006.Width = 1.0!
        '
        'Label51
        '
        Me.Label51.Height = 0.1875!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 3.25!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label51.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label51.Top = 2.81!
        Me.Label51.Width = 4.75!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 0!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label52.Text = "Axial Load (Pu_sp)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label52.Top = 2.997!
        Me.Label52.Width = 2.25!
        '
        'S42_007
        '
        Me.S42_007.Height = 0.1875!
        Me.S42_007.HyperLink = Nothing
        Me.S42_007.Left = 2.25!
        Me.S42_007.Name = "S42_007"
        Me.S42_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_007.Text = "206.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_007.Top = 2.997!
        Me.S42_007.Width = 1.0!
        '
        'Label54
        '
        Me.Label54.Height = 0.1875!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 3.25!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label54.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Max axial force per LC 31-34 from ETABS"
        Me.Label54.Top = 2.997!
        Me.Label54.Width = 4.75!
        '
        'Label55
        '
        Me.Label55.Height = 0.1875!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 0!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label55.Text = "Vertical Load (Vu_bm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label55.Top = 4.125!
        Me.Label55.Width = 2.25!
        '
        'S42_013
        '
        Me.S42_013.Height = 0.1875!
        Me.S42_013.HyperLink = Nothing
        Me.S42_013.Left = 2.25!
        Me.S42_013.Name = "S42_013"
        Me.S42_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_013.Text = "206.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_013.Top = 4.125!
        Me.S42_013.Width = 1.0!
        '
        'Label57
        '
        Me.Label57.Height = 0.1875!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 3.25!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label57.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vcap_link + Vbm_gravity"
        Me.Label57.Top = 4.125!
        Me.Label57.Width = 4.75!
        '
        'Label58
        '
        Me.Label58.Height = 0.1875!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 0!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label58.Text = "No of vertical Bolts (n_Vbolts_SST)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label58.Top = 4.312!
        Me.Label58.Width = 2.25!
        '
        'S42_014
        '
        Me.S42_014.Height = 0.1875!
        Me.S42_014.HyperLink = Nothing
        Me.S42_014.Left = 2.25!
        Me.S42_014.Name = "S42_014"
        Me.S42_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_014.Text = "3"
        Me.S42_014.Top = 4.312!
        Me.S42_014.Width = 1.0!
        '
        'Label60
        '
        Me.Label60.Height = 0.1875!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 3.25!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label60.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & " Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label60.Top = 4.312!
        Me.Label60.Width = 4.75!
        '
        'Label61
        '
        Me.Label61.Height = 0.1875!
        Me.Label61.HyperLink = Nothing
        Me.Label61.Left = 0!
        Me.Label61.Name = "Label61"
        Me.Label61.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label61.Text = "No of horizontal Bolts (n_Hbolts_SST)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label61.Top = 4.5!
        Me.Label61.Width = 2.25!
        '
        'S42_015
        '
        Me.S42_015.Height = 0.1875!
        Me.S42_015.HyperLink = Nothing
        Me.S42_015.Left = 2.25!
        Me.S42_015.Name = "S42_015"
        Me.S42_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_015.Text = "2"
        Me.S42_015.Top = 4.5!
        Me.S42_015.Width = 1.0!
        '
        'Label64
        '
        Me.Label64.Height = 0.1875!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 0!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label64.Text = "Vu_bolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label64.Top = 4.687!
        Me.Label64.Width = 2.25!
        '
        'S42_016
        '
        Me.S42_016.Height = 0.1875!
        Me.S42_016.HyperLink = Nothing
        Me.S42_016.Left = 2.25!
        Me.S42_016.Name = "S42_016"
        Me.S42_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S42_016.Text = "145.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_016.Top = 4.687!
        Me.S42_016.Width = 1.0!
        '
        'Label66
        '
        Me.Label66.Height = 0.1875!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 3.25!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label66.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Sqrt ( (Pu_sp/ n_Hbolt_SST)^2 + (Vu_bm/ n_Vbolt_SST)^2 )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label66.Top = 4.687!
        Me.Label66.Width = 4.75!
        '
        'Label67
        '
        Me.Label67.Height = 0.1875!
        Me.Label67.HyperLink = Nothing
        Me.Label67.Left = 0!
        Me.Label67.Name = "Label67"
        Me.Label67.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label67.Text = "Φbolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label67.Top = 4.875!
        Me.Label67.Width = 2.25!
        '
        'S42_017
        '
        Me.S42_017.Height = 0.1875!
        Me.S42_017.HyperLink = Nothing
        Me.S42_017.Left = 2.25!
        Me.S42_017.Name = "S42_017"
        Me.S42_017.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S42_017.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_017.Top = 4.875!
        Me.S42_017.Width = 1.0!
        '
        'Label70
        '
        Me.Label70.Height = 0.1875!
        Me.Label70.HyperLink = Nothing
        Me.Label70.Left = 0!
        Me.Label70.Name = "Label70"
        Me.Label70.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label70.Text = "Bolt Type (Bolt_Gr_shearTab) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label70.Top = 5.063!
        Me.Label70.Width = 2.25!
        '
        'S42_018
        '
        Me.S42_018.Height = 0.1875!
        Me.S42_018.HyperLink = Nothing
        Me.S42_018.Left = 2.25!
        Me.S42_018.Name = "S42_018"
        Me.S42_018.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_018.Text = "A490X" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_018.Top = 5.063!
        Me.S42_018.Width = 1.0!
        '
        'Label72
        '
        Me.Label72.Height = 0.1875!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 3.25!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label72.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "[A325-X default]" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label72.Top = 5.063!
        Me.Label72.Width = 4.75!
        '
        'Label63
        '
        Me.Label63.Height = 0.1875!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 0!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label63.Text = "Bolt Dia (db_sp)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label63.Top = 5.25!
        Me.Label63.Width = 2.25!
        '
        'S42_019
        '
        Me.S42_019.Height = 0.1875!
        Me.S42_019.HyperLink = Nothing
        Me.S42_019.Left = 2.25!
        Me.S42_019.Name = "S42_019"
        Me.S42_019.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_019.Text = " 7/8" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_019.Top = 5.25!
        Me.S42_019.Width = 1.0!
        '
        'Label73
        '
        Me.Label73.Height = 0.1875!
        Me.Label73.HyperLink = Nothing
        Me.Label73.Left = 3.25!
        Me.Label73.Name = "Label73"
        Me.Label73.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label73.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label73.Top = 5.25!
        Me.Label73.Width = 4.75!
        '
        'Label74
        '
        Me.Label74.Height = 0.1875!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 0!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label74.Text = "Fnv_bolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label74.Top = 5.438!
        Me.Label74.Width = 2.25!
        '
        'S42_020
        '
        Me.S42_020.Height = 0.1875!
        Me.S42_020.HyperLink = Nothing
        Me.S42_020.Left = 2.25!
        Me.S42_020.Name = "S42_020"
        Me.S42_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S42_020.Text = "68" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_020.Top = 5.438!
        Me.S42_020.Width = 1.0!
        '
        'Label76
        '
        Me.Label76.Height = 0.1875!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 3.25!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label76.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label76.Top = 5.438!
        Me.Label76.Width = 4.75!
        '
        'Label77
        '
        Me.Label77.Height = 0.1875!
        Me.Label77.HyperLink = Nothing
        Me.Label77.Left = 0!
        Me.Label77.Name = "Label77"
        Me.Label77.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label77.Text = "Anb_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label77.Top = 5.626!
        Me.Label77.Width = 2.25!
        '
        'S42_021
        '
        Me.S42_021.Height = 0.1875!
        Me.S42_021.HyperLink = Nothing
        Me.S42_021.Left = 2.25!
        Me.S42_021.Name = "S42_021"
        Me.S42_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S42_021.Text = "68" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_021.Top = 5.626!
        Me.S42_021.Width = 1.0!
        '
        'Label79
        '
        Me.Label79.Height = 0.1875!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 3.25!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label79.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pi* (db_sp/2)^2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label79.Top = 5.626!
        Me.Label79.Width = 4.75!
        '
        'Label80
        '
        Me.Label80.Height = 0.1875!
        Me.Label80.HyperLink = Nothing
        Me.Label80.Left = 0!
        Me.Label80.Name = "Label80"
        Me.Label80.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label80.Text = "Rn_stBolt_shear=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label80.Top = 5.814001!
        Me.Label80.Width = 2.25!
        '
        'S42_022
        '
        Me.S42_022.Height = 0.1875!
        Me.S42_022.HyperLink = Nothing
        Me.S42_022.Left = 2.25!
        Me.S42_022.Name = "S42_022"
        Me.S42_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S42_022.Text = "68" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_022.Top = 5.814001!
        Me.S42_022.Width = 1.0!
        '
        'Label82
        '
        Me.Label82.Height = 0.1875!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 3.25!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label82.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Fnv_bolt* Anb_sb" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label82.Top = 5.814001!
        Me.Label82.Width = 4.75!
        '
        'Label83
        '
        Me.Label83.Height = 0.1875!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 0!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label83.Text = "ΦRn_stBolt_Shear=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label83.Top = 6.002!
        Me.Label83.Width = 2.25!
        '
        'S42_023
        '
        Me.S42_023.Height = 0.1875!
        Me.S42_023.HyperLink = Nothing
        Me.S42_023.Left = 2.25!
        Me.S42_023.Name = "S42_023"
        Me.S42_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S42_023.Text = "68" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_023.Top = 6.002!
        Me.S42_023.Width = 1.0!
        '
        'Label85
        '
        Me.Label85.Height = 0.1875!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 3.25!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label85.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Rn_stBolt_shear* Φbolt" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label85.Top = 6.002!
        Me.Label85.Width = 4.75!
        '
        'Label86
        '
        Me.Label86.Height = 0.1875!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 0!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label86.Text = "SUM_shearTab_Bolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label86.Top = 6.189001!
        Me.Label86.Width = 2.25!
        '
        'S42_024
        '
        Me.S42_024.Height = 0.1875!
        Me.S42_024.HyperLink = Nothing
        Me.S42_024.Left = 2.25!
        Me.S42_024.Name = "S42_024"
        Me.S42_024.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S42_024.Text = "0.816" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_024.Top = 6.189001!
        Me.S42_024.Width = 1.0!
        '
        'Label88
        '
        Me.Label88.Height = 0.1875!
        Me.Label88.HyperLink = Nothing
        Me.Label88.Left = 3.75!
        Me.Label88.Name = "Label88"
        Me.Label88.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label88.Text = "= Vu_bolt/ ΦRn_stBolt_shear" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label88.Top = 6.189001!
        Me.Label88.Width = 4.25!
        '
        'Label725
        '
        Me.Label725.Height = 0.1875!
        Me.Label725.HyperLink = Nothing
        Me.Label725.Left = 3.75!
        Me.Label725.Name = "Label725"
        Me.Label725.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label725.Text = "OK if SUM_shearTabl_Cbolt <= SUM_Allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label725.Top = 6.377!
        Me.Label725.Width = 4.25!
        '
        'Label726
        '
        Me.Label726.Height = 0.1875!
        Me.Label726.HyperLink = Nothing
        Me.Label726.Left = 0!
        Me.Label726.Name = "Label726"
        Me.Label726.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label726.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label726.Top = 6.377!
        Me.Label726.Width = 2.25!
        '
        'S41_006
        '
        Me.S41_006.Height = 0.1875!
        Me.S41_006.HyperLink = Nothing
        Me.S41_006.Left = 1.562!
        Me.S41_006.Name = "S41_006"
        Me.S41_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S41_006.Tag = "5"
        Me.S41_006.Text = "YL4-3.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_006.Top = 1.312!
        Me.S41_006.Width = 1.0!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label4.Text = "tstem (in):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 1.312!
        Me.Label4.Width = 1.562!
        '
        'Label5S41_012
        '
        Me.Label5S41_012.Height = 0.1875!
        Me.Label5S41_012.HyperLink = Nothing
        Me.Label5S41_012.Left = 6.437!
        Me.Label5S41_012.Name = "Label5S41_012"
        Me.Label5S41_012.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label5S41_012.Text = "50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5S41_012.Top = 0.374!
        Me.Label5S41_012.Width = 1.0!
        '
        'S41_013
        '
        Me.S41_013.Height = 0.1875!
        Me.S41_013.HyperLink = Nothing
        Me.S41_013.Left = 6.437!
        Me.S41_013.Name = "S41_013"
        Me.S41_013.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S41_013.Text = "65" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_013.Top = 0.561!
        Me.S41_013.Width = 1.0!
        '
        'S41_014
        '
        Me.S41_014.Height = 0.1875!
        Me.S41_014.HyperLink = Nothing
        Me.S41_014.Left = 6.437!
        Me.S41_014.Name = "S41_014"
        Me.S41_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_014.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_014.Top = 0.7490001!
        Me.S41_014.Width = 1.0!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 5.062!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label15.Text = "Fy_bm=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label15.Top = 0.374!
        Me.Label15.Width = 1.375!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 5.062!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label17.Text = "Fu_bm=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label17.Top = 0.561!
        Me.Label17.Width = 1.375!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 7.437!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label19.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label19.Top = 0.374!
        Me.Label19.Width = 0.25!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 7.437!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label20.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label20.Top = 0.5610002!
        Me.Label20.Width = 0.25!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 5.062!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label22.Text = "Left Conn=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label22.Top = 0.7490001!
        Me.Label22.Width = 1.375!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 5.062!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label26.Text = "Right_Conn=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label26.Top = 0.937!
        Me.Label26.Width = 1.375!
        '
        'S41_015
        '
        Me.S41_015.Height = 0.1875!
        Me.S41_015.HyperLink = Nothing
        Me.S41_015.Left = 6.437!
        Me.S41_015.Name = "S41_015"
        Me.S41_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S41_015.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_015.Top = 0.937!
        Me.S41_015.Width = 1.0!
        '
        'S41_016
        '
        Me.S41_016.Height = 0.1875!
        Me.S41_016.HyperLink = Nothing
        Me.S41_016.Left = 6.437!
        Me.S41_016.Name = "S41_016"
        Me.S41_016.Style = "color: Red; font-size: 8.25pt; font-weight: normal; text-align: center; vertical-" &
    "align: middle; ddo-char-set: 1"
        Me.S41_016.Text = "2"
        Me.S41_016.Top = 1.125!
        Me.S41_016.Width = 1.0!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 5.062002!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label25.Text = "No. of Conn =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label25.Top = 1.125!
        Me.Label25.Width = 1.375!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label2.Text = "Lcc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 3.185!
        Me.Label2.Width = 2.25!
        '
        'S42_008
        '
        Me.S42_008.Height = 0.1875!
        Me.S42_008.HyperLink = Nothing
        Me.S42_008.Left = 2.25!
        Me.S42_008.Name = "S42_008"
        Me.S42_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_008.Text = "200.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_008.Top = 3.185!
        Me.S42_008.Width = 1.0!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 3.25!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label7.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 3.185!
        Me.Label7.Width = 4.75!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label9.Text = "Lh=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 3.373!
        Me.Label9.Width = 2.25!
        '
        'S42_009
        '
        Me.S42_009.Height = 0.1875!
        Me.S42_009.HyperLink = Nothing
        Me.S42_009.Left = 2.25!
        Me.S42_009.Name = "S42_009"
        Me.S42_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S42_009.Text = "172.60" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_009.Top = 3.373!
        Me.S42_009.Width = 1.0!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 3.25!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label27.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=Lcc-dc_left/2 - dc_right/2-2*a"
        Me.Label27.Top = 3.373!
        Me.Label27.Width = 4.75!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 0!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label32.Text = "Pcap_link="
        Me.Label32.Top = 3.56!
        Me.Label32.Width = 2.25!
        '
        'S42_010
        '
        Me.S42_010.Height = 0.1875!
        Me.S42_010.HyperLink = Nothing
        Me.S42_010.Left = 2.25!
        Me.S42_010.Name = "S42_010"
        Me.S42_010.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S42_010.Text = "172.60" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_010.Top = 3.56!
        Me.S42_010.Width = 1.0!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 3.25!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label38.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=If (R=3, Py_link, Pr_link)"
        Me.Label38.Top = 3.56!
        Me.Label38.Width = 4.75!
        '
        'Label40
        '
        Me.Label40.Height = 0.1875!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 0!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label40.Text = "Vcap_link="
        Me.Label40.Top = 3.938!
        Me.Label40.Width = 2.25!
        '
        'S42_012
        '
        Me.S42_012.Height = 0.1875!
        Me.S42_012.HyperLink = Nothing
        Me.S42_012.Left = 2.25!
        Me.S42_012.Name = "S42_012"
        Me.S42_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S42_012.Text = "172.60" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_012.Top = 3.938!
        Me.S42_012.Width = 1.0!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 3.25!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label42.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=Mcap'*(No. of Connections) / Lh"
        Me.Label42.Top = 3.938!
        Me.Label42.Width = 4.75!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "Mcap="
        Me.Label5.Top = 3.75!
        Me.Label5.Width = 2.25!
        '
        'S42_011
        '
        Me.S42_011.Height = 0.1875!
        Me.S42_011.HyperLink = Nothing
        Me.S42_011.Left = 2.25!
        Me.S42_011.Name = "S42_011"
        Me.S42_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S42_011.Text = "172.60" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S42_011.Top = 3.75!
        Me.S42_011.Width = 1.0!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 3.25!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label29.Text = "kip-in" & Global.Microsoft.VisualBasic.ChrW(9) & "=Pcap_link*(db + tstem)"
        Me.Label29.Top = 3.75!
        Me.Label29.Width = 4.75!
        '
        'Label35
        '
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 3.25!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label35.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & " User Input" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label35.Top = 4.499!
        Me.Label35.Width = 4.75!
        '
        'Shape2
        '
        Me.Shape2.Height = 0.1875!
        Me.Shape2.Left = 1.562!
        Me.Shape2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 0.375!
        Me.Shape2.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.25!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 6.377!
        Me.Shape1.Width = 1.0!
        '
        'Label53
        '
        Me.Label53.Height = 0.1875!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 5.061999!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label53.Text = "R=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label53.Top = 0.187!
        Me.Label53.Width = 1.375!
        '
        'S41_017
        '
        Me.S41_017.Height = 0.1875!
        Me.S41_017.HyperLink = Nothing
        Me.S41_017.Left = 6.425!
        Me.S41_017.Name = "S41_017"
        Me.S41_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S41_017.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S41_017.Top = 0.188!
        Me.S41_017.Width = 1.0!
        '
        'US_SPC1
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.75!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S42_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label725, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label726, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5S41_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S42_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S41_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label48 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label51 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label55 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label61 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label64 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label67 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label70 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label72 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label63 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label73 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label74 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label77 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label79 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label80 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label82 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label83 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label85 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label86 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label88 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label725 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label726 As GrapeCity.ActiveReports.SectionReportModel.Label
    '
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S41_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5S41_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S42_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label53 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S41_017 As GrapeCity.ActiveReports.SectionReportModel.Label
End Class
