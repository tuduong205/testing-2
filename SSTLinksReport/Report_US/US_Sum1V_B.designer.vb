﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class US_Sum1V_B
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    Private WithEvents PageFooter As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_Sum1V_B))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.Label127 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbSeismicDriftLimit = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbWindDriftLimit = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDGVName = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line12 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line13 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line14 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line21 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label70 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line44 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line43 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label88 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbILS1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbILS2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbILS3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbILS4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbILS5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label89 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label90 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbBLC1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label92 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label93 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label94 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label95 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbBLC2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label97 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label98 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label99 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label100 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbBLC3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label102 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label103 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label104 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label105 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbBLC4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label107 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label108 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label109 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label110 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbBLC5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label61 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbCC1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label63 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label64 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label65 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbCC2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label68 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label69 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label73 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label74 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbCC3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label77 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label78 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label79 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbCC4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label81 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label82 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label83 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label84 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label85 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbST1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label87 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label112 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label113 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label114 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbST2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label116 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label117 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label118 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label119 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbST3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label121 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label122 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label123 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label124 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbST4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line15 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line16 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line17 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line18 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line19 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line20 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line22 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line23 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line24 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line25 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line26 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line36 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line37 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line38 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line39 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line40 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line45 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line46 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line47 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line48 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line49 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line50 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line51 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line52 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line53 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line54 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line55 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line56 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line57 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line58 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line59 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line60 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbWeldWeb = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbWeldFLG = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbWeld3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbWeld1_B = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbWeld2_B = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.pic1 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Line27 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label47 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line28 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line29 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line30 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line31 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line32 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line33 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line34 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line35 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line41 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line42 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line61 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line62 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line63 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line64 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line65 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line66 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line67 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line68 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.GroupHeader1 = New GrapeCity.ActiveReports.SectionReportModel.GroupHeader()
        Me.lbJobID = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbJobName = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbPrintedDate = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.GroupFooter1 = New GrapeCity.ActiveReports.SectionReportModel.GroupFooter()
        Me.PageHeader1 = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.PageFooter1 = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbSeismicDriftLimit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWindDriftLimit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDGVName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbILS1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbILS2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbILS3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbILS4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbILS5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbBLC1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbBLC2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbBLC3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbBLC4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbBLC5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbCC1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbCC2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbCC3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbCC4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbST1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbST2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbST3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbST4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWeldWeb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWeldFLG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWeld3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWeld1_B, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWeld2_B, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label127, Me.lbSeismicDriftLimit, Me.lbWindDriftLimit, Me.Label12, Me.Label1, Me.Label5, Me.Label6, Me.Label7, Me.Label8, Me.Label9, Me.Label10, Me.lbDGVName, Me.Line12, Me.Line13, Me.Line14, Me.Line21, Me.Label70, Me.Line44, Me.Line43, Me.Label88, Me.Label11, Me.lbILS1, Me.Label13, Me.Line1, Me.Line2, Me.Label14, Me.Label15, Me.Label16, Me.Label17, Me.lbILS2, Me.Label19, Me.Label20, Me.Label21, Me.Label22, Me.lbILS3, Me.Label24, Me.Label25, Me.Label26, Me.Label27, Me.lbILS4, Me.Label29, Me.Label30, Me.Label31, Me.Label32, Me.lbILS5, Me.Label34, Me.Label35, Me.Label36, Me.Label89, Me.Label90, Me.lbBLC1, Me.Label92, Me.Label93, Me.Label94, Me.Label95, Me.lbBLC2, Me.Label97, Me.Label98, Me.Label99, Me.Label100, Me.lbBLC3, Me.Label102, Me.Label103, Me.Label104, Me.Label105, Me.lbBLC4, Me.Label107, Me.Label108, Me.Label109, Me.Label110, Me.lbBLC5, Me.Label57, Me.Label58, Me.Label59, Me.Label60, Me.Label61, Me.lbCC1, Me.Label63, Me.Label64, Me.Label65, Me.Label66, Me.lbCC2, Me.Label68, Me.Label69, Me.Label73, Me.Label74, Me.lbCC3, Me.Label76, Me.Label77, Me.Label78, Me.Label79, Me.lbCC4, Me.Label81, Me.Label82, Me.Label83, Me.Label84, Me.Label85, Me.lbST1, Me.Label87, Me.Label112, Me.Label113, Me.Label114, Me.lbST2, Me.Label116, Me.Label117, Me.Label118, Me.Label119, Me.lbST3, Me.Label121, Me.Label122, Me.Label123, Me.Label124, Me.lbST4, Me.Line3, Me.Line4, Me.Line5, Me.Line6, Me.Line7, Me.Line8, Me.Line9, Me.Line10, Me.Line11, Me.Line15, Me.Line16, Me.Line17, Me.Line18, Me.Line19, Me.Line20, Me.Line22, Me.Line23, Me.Line24, Me.Line25, Me.Line26, Me.Line36, Me.Line37, Me.Line38, Me.Line39, Me.Line40, Me.Line45, Me.Line46, Me.Line47, Me.Line48, Me.Line49, Me.Line50, Me.Line51, Me.Line52, Me.Line53, Me.Line54, Me.Line55, Me.Line56, Me.Line57, Me.Line58, Me.Line59, Me.Line60, Me.Label37, Me.lbWeldWeb, Me.Label39, Me.lbWeldFLG, Me.Label41, Me.lbWeld3, Me.lbWeld1_B, Me.lbWeld2_B, Me.pic1, Me.Line27, Me.Label45, Me.Label46, Me.Label47, Me.Line28, Me.Line29, Me.Line30, Me.Line31, Me.Line32, Me.Line33, Me.Line34, Me.Line35, Me.Line41, Me.Line42, Me.Line61, Me.Line62, Me.Line63, Me.Line64, Me.Line65, Me.Line66, Me.Line67, Me.Line68})
        Me.Detail.Height = 8.552083!
        Me.Detail.Name = "Detail"
        '
        'Label127
        '
        Me.Label127.Height = 0.2!
        Me.Label127.HyperLink = Nothing
        Me.Label127.Left = 0.25!
        Me.Label127.Name = "Label127"
        Me.Label127.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label127.Text = "Allowable Seismic Drift Limit: "
        Me.Label127.Top = 0.2650003!
        Me.Label127.Width = 2.0!
        '
        'lbSeismicDriftLimit
        '
        Me.lbSeismicDriftLimit.Height = 0.2!
        Me.lbSeismicDriftLimit.HyperLink = Nothing
        Me.lbSeismicDriftLimit.Left = 2.25!
        Me.lbSeismicDriftLimit.Name = "lbSeismicDriftLimit"
        Me.lbSeismicDriftLimit.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbSeismicDriftLimit.Text = "Value"
        Me.lbSeismicDriftLimit.Top = 0.265!
        Me.lbSeismicDriftLimit.Width = 1.25!
        '
        'lbWindDriftLimit
        '
        Me.lbWindDriftLimit.Height = 0.2!
        Me.lbWindDriftLimit.HyperLink = Nothing
        Me.lbWindDriftLimit.Left = 6.0!
        Me.lbWindDriftLimit.Name = "lbWindDriftLimit"
        Me.lbWindDriftLimit.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbWindDriftLimit.Text = "Value"
        Me.lbWindDriftLimit.Top = 0.2680001!
        Me.lbWindDriftLimit.Width = 1.25!
        '
        'Label12
        '
        Me.Label12.Height = 0.2!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 4.0!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label12.Text = "Allowable Wind Drift Limit: "
        Me.Label12.Top = 0.2680006!
        Me.Label12.Width = 2.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.2!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.2500001!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label1.Text = "No." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.Label1.Top = 0.604!
        Me.Label1.Width = 1.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 3.5!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label5.Text = "Allowable DCR"
        Me.Label5.Top = 0.6070001!
        Me.Label5.Width = 1.25!
        '
        'Label6
        '
        Me.Label6.Height = 0.2!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 6.0!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label6.Text = "Min."
        Me.Label6.Top = 0.604!
        Me.Label6.Width = 1.25!
        '
        'Label7
        '
        Me.Label7.Height = 0.2!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0.2500001!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "color: DarkRed; font-size: 9pt; font-weight: bold; vertical-align: middle; ddo-ch" &
    "ar-set: 1"
        Me.Label7.Text = "     ILS (INITIAL LINK SELECTION)"
        Me.Label7.Top = 0.8040001!
        Me.Label7.Width = 7.0!
        '
        'Label8
        '
        Me.Label8.Height = 0.2!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0.2500001!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label8.Text = "1"
        Me.Label8.Top = 1.004!
        Me.Label8.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.2!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 1.25!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label9.Text = "Initial tbf check="
        Me.Label9.Top = 1.004!
        Me.Label9.Width = 2.25!
        '
        'Label10
        '
        Me.Label10.Height = 0.2!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 6.0!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label10.Text = "0.90"
        Me.Label10.Top = 1.004!
        Me.Label10.Width = 1.25!
        '
        'lbDGVName
        '
        Me.lbDGVName.Height = 0.265!
        Me.lbDGVName.HyperLink = Nothing
        Me.lbDGVName.Left = 0!
        Me.lbDGVName.Name = "lbDGVName"
        Me.lbDGVName.Style = "font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.lbDGVName.Text = "DESIGN DCR's"
        Me.lbDGVName.Top = 0!
        Me.lbDGVName.Width = 8.0!
        '
        'Line12
        '
        Me.Line12.Height = 0!
        Me.Line12.Left = 0.2499999!
        Me.Line12.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line12.LineWeight = 1.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 1.004!
        Me.Line12.Width = 7.0!
        Me.Line12.X1 = 0.2499999!
        Me.Line12.X2 = 7.25!
        Me.Line12.Y1 = 1.004!
        Me.Line12.Y2 = 1.004!
        '
        'Line13
        '
        Me.Line13.Height = 0.003000021!
        Me.Line13.Left = 0.2499999!
        Me.Line13.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line13.LineWeight = 1.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 0.8040001!
        Me.Line13.Width = 7.0!
        Me.Line13.X1 = 0.2499999!
        Me.Line13.X2 = 7.25!
        Me.Line13.Y1 = 0.8040001!
        Me.Line13.Y2 = 0.8070001!
        '
        'Line14
        '
        Me.Line14.Height = 0!
        Me.Line14.Left = 0.2499999!
        Me.Line14.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line14.LineWeight = 1.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 0.6!
        Me.Line14.Width = 7.0!
        Me.Line14.X1 = 0.2499999!
        Me.Line14.X2 = 7.25!
        Me.Line14.Y1 = 0.6!
        Me.Line14.Y2 = 0.6!
        '
        'Line21
        '
        Me.Line21.Height = 0.2029994!
        Me.Line21.Left = 1.25!
        Me.Line21.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line21.LineWeight = 1.0!
        Me.Line21.Name = "Line21"
        Me.Line21.Top = 0.601!
        Me.Line21.Width = 0.0000009536743!
        Me.Line21.X1 = 1.25!
        Me.Line21.X2 = 1.250001!
        Me.Line21.Y1 = 0.601!
        Me.Line21.Y2 = 0.8039994!
        '
        'Label70
        '
        Me.Label70.Height = 0.265!
        Me.Label70.HyperLink = Nothing
        Me.Label70.Left = 0!
        Me.Label70.Name = "Label70"
        Me.Label70.Style = "font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.Label70.Text = "WELDING PREFERENCES"
        Me.Label70.Top = 5.26!
        Me.Label70.Width = 8.0!
        '
        'Line44
        '
        Me.Line44.Height = 0.2029994!
        Me.Line44.Left = 4.75!
        Me.Line44.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line44.LineWeight = 1.0!
        Me.Line44.Name = "Line44"
        Me.Line44.Top = 0.601!
        Me.Line44.Width = 0.0000009536743!
        Me.Line44.X1 = 4.75!
        Me.Line44.X2 = 4.750001!
        Me.Line44.Y1 = 0.601!
        Me.Line44.Y2 = 0.8039994!
        '
        'Line43
        '
        Me.Line43.Height = 0.2029991!
        Me.Line43.Left = 6.0!
        Me.Line43.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line43.LineWeight = 1.0!
        Me.Line43.Name = "Line43"
        Me.Line43.Top = 0.604!
        Me.Line43.Width = 0.0000009536743!
        Me.Line43.X1 = 6.0!
        Me.Line43.X2 = 6.000001!
        Me.Line43.Y1 = 0.604!
        Me.Line43.Y2 = 0.8069991!
        '
        'Label88
        '
        Me.Label88.Height = 0.2!
        Me.Label88.HyperLink = Nothing
        Me.Label88.Left = 4.75!
        Me.Label88.Name = "Label88"
        Me.Label88.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label88.Text = "Max."
        Me.Label88.Top = 0.601!
        Me.Label88.Width = 1.25!
        '
        'Label11
        '
        Me.Label11.Height = 0.2!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 4.75!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label11.Text = "1.05"
        Me.Label11.Top = 1.004!
        Me.Label11.Width = 1.25!
        '
        'lbILS1
        '
        Me.lbILS1.Height = 0.2!
        Me.lbILS1.HyperLink = Nothing
        Me.lbILS1.Left = 3.5!
        Me.lbILS1.Name = "lbILS1"
        Me.lbILS1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbILS1.Text = "Value"
        Me.lbILS1.Top = 1.004!
        Me.lbILS1.Width = 1.25!
        '
        'Label13
        '
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 1.25!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label13.Text = "Item"
        Me.Label13.Top = 0.6070001!
        Me.Label13.Width = 2.25!
        '
        'Line1
        '
        Me.Line1.Height = 0.2029991!
        Me.Line1.Left = 4.75!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0.601!
        Me.Line1.Width = 0!
        Me.Line1.X1 = 4.75!
        Me.Line1.X2 = 4.75!
        Me.Line1.Y1 = 0.601!
        Me.Line1.Y2 = 0.8039991!
        '
        'Line2
        '
        Me.Line2.Height = 0.2029991!
        Me.Line2.Left = 3.5!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0.598!
        Me.Line2.Width = 0!
        Me.Line2.X1 = 3.5!
        Me.Line2.X2 = 3.5!
        Me.Line2.Y1 = 0.598!
        Me.Line2.Y2 = 0.8009991!
        '
        'Label14
        '
        Me.Label14.Height = 0.2!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.2500001!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label14.Text = "2"
        Me.Label14.Top = 1.204!
        Me.Label14.Width = 1.0!
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 1.25!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label15.Text = "Initial bf check="
        Me.Label15.Top = 1.204!
        Me.Label15.Width = 2.25!
        '
        'Label16
        '
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 6.0!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label16.Text = "0.90"
        Me.Label16.Top = 1.204!
        Me.Label16.Width = 1.25!
        '
        'Label17
        '
        Me.Label17.Height = 0.2!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 4.75!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label17.Text = "1.05"
        Me.Label17.Top = 1.204!
        Me.Label17.Width = 1.25!
        '
        'lbILS2
        '
        Me.lbILS2.Height = 0.2!
        Me.lbILS2.HyperLink = Nothing
        Me.lbILS2.Left = 3.5!
        Me.lbILS2.Name = "lbILS2"
        Me.lbILS2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbILS2.Text = "Value"
        Me.lbILS2.Top = 1.204!
        Me.lbILS2.Width = 1.25!
        '
        'Label19
        '
        Me.Label19.Height = 0.2!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 0.2499999!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label19.Text = "3"
        Me.Label19.Top = 1.404!
        Me.Label19.Width = 1.0!
        '
        'Label20
        '
        Me.Label20.Height = 0.2!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 1.25!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label20.Text = "Initial Lyield check="
        Me.Label20.Top = 1.404!
        Me.Label20.Width = 2.25!
        '
        'Label21
        '
        Me.Label21.Height = 0.2!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 6.0!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label21.Text = "0.90"
        Me.Label21.Top = 1.404!
        Me.Label21.Width = 1.25!
        '
        'Label22
        '
        Me.Label22.Height = 0.2!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 4.75!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label22.Text = "1.05"
        Me.Label22.Top = 1.404!
        Me.Label22.Width = 1.25!
        '
        'lbILS3
        '
        Me.lbILS3.Height = 0.2!
        Me.lbILS3.HyperLink = Nothing
        Me.lbILS3.Left = 3.5!
        Me.lbILS3.Name = "lbILS3"
        Me.lbILS3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbILS3.Text = "Value"
        Me.lbILS3.Top = 1.404!
        Me.lbILS3.Width = 1.25!
        '
        'Label24
        '
        Me.Label24.Height = 0.2!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 0.2500001!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label24.Text = "4"
        Me.Label24.Top = 1.604!
        Me.Label24.Width = 1.0!
        '
        'Label25
        '
        Me.Label25.Height = 0.2!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 1.25!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label25.Text = "Panel Zone DCR="
        Me.Label25.Top = 1.604!
        Me.Label25.Width = 2.25!
        '
        'Label26
        '
        Me.Label26.Height = 0.2!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 6.0!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label26.Text = "0.90"
        Me.Label26.Top = 1.604!
        Me.Label26.Width = 1.25!
        '
        'Label27
        '
        Me.Label27.Height = 0.2!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 4.75!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label27.Text = "1.05"
        Me.Label27.Top = 1.604!
        Me.Label27.Width = 1.25!
        '
        'lbILS4
        '
        Me.lbILS4.Height = 0.2!
        Me.lbILS4.HyperLink = Nothing
        Me.lbILS4.Left = 3.5!
        Me.lbILS4.Name = "lbILS4"
        Me.lbILS4.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbILS4.Text = "Value"
        Me.lbILS4.Top = 1.604!
        Me.lbILS4.Width = 1.25!
        '
        'Label29
        '
        Me.Label29.Height = 0.2!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0.2499999!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label29.Text = "5"
        Me.Label29.Top = 1.804!
        Me.Label29.Width = 1.0!
        '
        'Label30
        '
        Me.Label30.Height = 0.2!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 1.25!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label30.Text = "Drift DCR="
        Me.Label30.Top = 1.804!
        Me.Label30.Width = 2.25!
        '
        'Label31
        '
        Me.Label31.Height = 0.2!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 6.0!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label31.Text = "0.90"
        Me.Label31.Top = 1.804!
        Me.Label31.Width = 1.25!
        '
        'Label32
        '
        Me.Label32.Height = 0.2!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 4.75!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label32.Text = "1.05"
        Me.Label32.Top = 1.804!
        Me.Label32.Width = 1.25!
        '
        'lbILS5
        '
        Me.lbILS5.Height = 0.2!
        Me.lbILS5.HyperLink = Nothing
        Me.lbILS5.Left = 3.5!
        Me.lbILS5.Name = "lbILS5"
        Me.lbILS5.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbILS5.Text = "Value"
        Me.lbILS5.Top = 1.804!
        Me.lbILS5.Width = 1.25!
        '
        'Label34
        '
        Me.Label34.Height = 0.2!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 0.25!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "color: DarkRed; font-size: 9pt; font-weight: bold; vertical-align: middle; ddo-ch" &
    "ar-set: 1"
        Me.Label34.Text = "     BLC (BEAM & LINK CHECK)"
        Me.Label34.Top = 2.004!
        Me.Label34.Width = 7.0!
        '
        'Label35
        '
        Me.Label35.Height = 0.2!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 0.25!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label35.Text = "1"
        Me.Label35.Top = 2.204!
        Me.Label35.Width = 1.0!
        '
        'Label36
        '
        Me.Label36.Height = 0.2!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 1.25!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label36.Text = "Beam tf DCR="
        Me.Label36.Top = 2.204!
        Me.Label36.Width = 2.25!
        '
        'Label89
        '
        Me.Label89.Height = 0.2!
        Me.Label89.HyperLink = Nothing
        Me.Label89.Left = 6.0!
        Me.Label89.Name = "Label89"
        Me.Label89.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label89.Text = "0.90"
        Me.Label89.Top = 2.204!
        Me.Label89.Width = 1.25!
        '
        'Label90
        '
        Me.Label90.Height = 0.2!
        Me.Label90.HyperLink = Nothing
        Me.Label90.Left = 4.75!
        Me.Label90.Name = "Label90"
        Me.Label90.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label90.Text = "1.05"
        Me.Label90.Top = 2.204!
        Me.Label90.Width = 1.25!
        '
        'lbBLC1
        '
        Me.lbBLC1.Height = 0.2!
        Me.lbBLC1.HyperLink = Nothing
        Me.lbBLC1.Left = 3.5!
        Me.lbBLC1.Name = "lbBLC1"
        Me.lbBLC1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbBLC1.Text = "Value"
        Me.lbBLC1.Top = 2.204!
        Me.lbBLC1.Width = 1.25!
        '
        'Label92
        '
        Me.Label92.Height = 0.2!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 0.25!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label92.Text = "2"
        Me.Label92.Top = 2.404!
        Me.Label92.Width = 1.0!
        '
        'Label93
        '
        Me.Label93.Height = 0.2!
        Me.Label93.HyperLink = Nothing
        Me.Label93.Left = 1.25!
        Me.Label93.Name = "Label93"
        Me.Label93.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label93.Text = "Link strength DCR="
        Me.Label93.Top = 2.404!
        Me.Label93.Width = 2.25!
        '
        'Label94
        '
        Me.Label94.Height = 0.2!
        Me.Label94.HyperLink = Nothing
        Me.Label94.Left = 6.0!
        Me.Label94.Name = "Label94"
        Me.Label94.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label94.Text = "0.90"
        Me.Label94.Top = 2.404!
        Me.Label94.Width = 1.25!
        '
        'Label95
        '
        Me.Label95.Height = 0.2!
        Me.Label95.HyperLink = Nothing
        Me.Label95.Left = 4.75!
        Me.Label95.Name = "Label95"
        Me.Label95.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label95.Text = "1.05"
        Me.Label95.Top = 2.404!
        Me.Label95.Width = 1.25!
        '
        'lbBLC2
        '
        Me.lbBLC2.Height = 0.2!
        Me.lbBLC2.HyperLink = Nothing
        Me.lbBLC2.Left = 3.5!
        Me.lbBLC2.Name = "lbBLC2"
        Me.lbBLC2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbBLC2.Text = "Value"
        Me.lbBLC2.Top = 2.404!
        Me.lbBLC2.Width = 1.25!
        '
        'Label97
        '
        Me.Label97.Height = 0.2!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 0.25!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label97.Text = "3"
        Me.Label97.Top = 2.604!
        Me.Label97.Width = 1.0!
        '
        'Label98
        '
        Me.Label98.Height = 0.2!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 1.25!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label98.Text = "Lyield check="
        Me.Label98.Top = 2.604!
        Me.Label98.Width = 2.25!
        '
        'Label99
        '
        Me.Label99.Height = 0.2!
        Me.Label99.HyperLink = Nothing
        Me.Label99.Left = 6.0!
        Me.Label99.Name = "Label99"
        Me.Label99.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label99.Text = "0.90"
        Me.Label99.Top = 2.604!
        Me.Label99.Width = 1.25!
        '
        'Label100
        '
        Me.Label100.Height = 0.2!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 4.75!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label100.Text = "1.05"
        Me.Label100.Top = 2.604!
        Me.Label100.Width = 1.25!
        '
        'lbBLC3
        '
        Me.lbBLC3.Height = 0.2!
        Me.lbBLC3.HyperLink = Nothing
        Me.lbBLC3.Left = 3.5!
        Me.lbBLC3.Name = "lbBLC3"
        Me.lbBLC3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbBLC3.Text = "Value"
        Me.lbBLC3.Top = 2.604!
        Me.lbBLC3.Width = 1.25!
        '
        'Label102
        '
        Me.Label102.Height = 0.2!
        Me.Label102.HyperLink = Nothing
        Me.Label102.Left = 0.25!
        Me.Label102.Name = "Label102"
        Me.Label102.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label102.Text = "4"
        Me.Label102.Top = 2.804!
        Me.Label102.Width = 1.0!
        '
        'Label103
        '
        Me.Label103.Height = 0.2!
        Me.Label103.HyperLink = Nothing
        Me.Label103.Left = 1.25!
        Me.Label103.Name = "Label103"
        Me.Label103.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label103.Text = "t_BRP DCR="
        Me.Label103.Top = 2.804!
        Me.Label103.Width = 2.25!
        '
        'Label104
        '
        Me.Label104.Height = 0.2!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 6.0!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label104.Text = "0.90"
        Me.Label104.Top = 2.804!
        Me.Label104.Width = 1.25!
        '
        'Label105
        '
        Me.Label105.Height = 0.2!
        Me.Label105.HyperLink = Nothing
        Me.Label105.Left = 4.75!
        Me.Label105.Name = "Label105"
        Me.Label105.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label105.Text = "1.05"
        Me.Label105.Top = 2.804!
        Me.Label105.Width = 1.25!
        '
        'lbBLC4
        '
        Me.lbBLC4.Height = 0.2!
        Me.lbBLC4.HyperLink = Nothing
        Me.lbBLC4.Left = 3.5!
        Me.lbBLC4.Name = "lbBLC4"
        Me.lbBLC4.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbBLC4.Text = "Value"
        Me.lbBLC4.Top = 2.804!
        Me.lbBLC4.Width = 1.25!
        '
        'Label107
        '
        Me.Label107.Height = 0.2!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 0.25!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label107.Text = "5"
        Me.Label107.Top = 3.004!
        Me.Label107.Width = 1.0!
        '
        'Label108
        '
        Me.Label108.Height = 0.2!
        Me.Label108.HyperLink = Nothing
        Me.Label108.Left = 1.25!
        Me.Label108.Name = "Label108"
        Me.Label108.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label108.Text = "BRP Bolt DCR="
        Me.Label108.Top = 3.004!
        Me.Label108.Width = 2.25!
        '
        'Label109
        '
        Me.Label109.Height = 0.2!
        Me.Label109.HyperLink = Nothing
        Me.Label109.Left = 6.0!
        Me.Label109.Name = "Label109"
        Me.Label109.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label109.Text = "0.90"
        Me.Label109.Top = 3.004!
        Me.Label109.Width = 1.25!
        '
        'Label110
        '
        Me.Label110.Height = 0.2!
        Me.Label110.HyperLink = Nothing
        Me.Label110.Left = 4.75!
        Me.Label110.Name = "Label110"
        Me.Label110.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label110.Text = "1.05"
        Me.Label110.Top = 3.004!
        Me.Label110.Width = 1.25!
        '
        'lbBLC5
        '
        Me.lbBLC5.Height = 0.2!
        Me.lbBLC5.HyperLink = Nothing
        Me.lbBLC5.Left = 3.5!
        Me.lbBLC5.Name = "lbBLC5"
        Me.lbBLC5.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbBLC5.Text = "Value"
        Me.lbBLC5.Top = 3.004!
        Me.lbBLC5.Width = 1.25!
        '
        'Label57
        '
        Me.Label57.Height = 0.2!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 0.2500001!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "color: DarkRed; font-size: 9pt; font-weight: bold; vertical-align: middle; ddo-ch" &
    "ar-set: 1"
        Me.Label57.Text = "     CC (COLUMN CHECK)"
        Me.Label57.Top = 3.204!
        Me.Label57.Width = 7.0!
        '
        'Label58
        '
        Me.Label58.Height = 0.2!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 0.2500001!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label58.Text = "1"
        Me.Label58.Top = 3.404!
        Me.Label58.Width = 1.0!
        '
        'Label59
        '
        Me.Label59.Height = 0.2!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 1.25!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label59.Text = "SCWB DCR="
        Me.Label59.Top = 3.404!
        Me.Label59.Width = 2.25!
        '
        'Label60
        '
        Me.Label60.Height = 0.2!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 6.0!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label60.Text = "0.90"
        Me.Label60.Top = 3.404!
        Me.Label60.Width = 1.25!
        '
        'Label61
        '
        Me.Label61.Height = 0.2!
        Me.Label61.HyperLink = Nothing
        Me.Label61.Left = 4.75!
        Me.Label61.Name = "Label61"
        Me.Label61.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label61.Text = "1.05"
        Me.Label61.Top = 3.404!
        Me.Label61.Width = 1.25!
        '
        'lbCC1
        '
        Me.lbCC1.Height = 0.2!
        Me.lbCC1.HyperLink = Nothing
        Me.lbCC1.Left = 3.5!
        Me.lbCC1.Name = "lbCC1"
        Me.lbCC1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbCC1.Text = "Value"
        Me.lbCC1.Top = 3.404!
        Me.lbCC1.Width = 1.25!
        '
        'Label63
        '
        Me.Label63.Height = 0.2!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 0.2500001!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label63.Text = "2"
        Me.Label63.Top = 3.604!
        Me.Label63.Width = 1.0!
        '
        'Label64
        '
        Me.Label64.Height = 0.2!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 1.25!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label64.Text = "Panel Zone DCR="
        Me.Label64.Top = 3.604!
        Me.Label64.Width = 2.25!
        '
        'Label65
        '
        Me.Label65.Height = 0.2!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 6.0!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label65.Text = "0.90"
        Me.Label65.Top = 3.604!
        Me.Label65.Width = 1.25!
        '
        'Label66
        '
        Me.Label66.Height = 0.2!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 4.75!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label66.Text = "1.05"
        Me.Label66.Top = 3.604!
        Me.Label66.Width = 1.25!
        '
        'lbCC2
        '
        Me.lbCC2.Height = 0.2!
        Me.lbCC2.HyperLink = Nothing
        Me.lbCC2.Left = 3.5!
        Me.lbCC2.Name = "lbCC2"
        Me.lbCC2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbCC2.Text = "Value"
        Me.lbCC2.Top = 3.604!
        Me.lbCC2.Width = 1.25!
        '
        'Label68
        '
        Me.Label68.Height = 0.2!
        Me.Label68.HyperLink = Nothing
        Me.Label68.Left = 0.2500001!
        Me.Label68.Name = "Label68"
        Me.Label68.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label68.Text = "3"
        Me.Label68.Top = 3.804!
        Me.Label68.Width = 1.0!
        '
        'Label69
        '
        Me.Label69.Height = 0.2!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 1.25!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label69.Text = "Column Flange DCR="
        Me.Label69.Top = 3.804!
        Me.Label69.Width = 2.25!
        '
        'Label73
        '
        Me.Label73.Height = 0.2!
        Me.Label73.HyperLink = Nothing
        Me.Label73.Left = 6.0!
        Me.Label73.Name = "Label73"
        Me.Label73.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label73.Text = "0.90"
        Me.Label73.Top = 3.804!
        Me.Label73.Width = 1.25!
        '
        'Label74
        '
        Me.Label74.Height = 0.2!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 4.75!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label74.Text = "1.05"
        Me.Label74.Top = 3.804!
        Me.Label74.Width = 1.25!
        '
        'lbCC3
        '
        Me.lbCC3.Height = 0.2!
        Me.lbCC3.HyperLink = Nothing
        Me.lbCC3.Left = 3.5!
        Me.lbCC3.Name = "lbCC3"
        Me.lbCC3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbCC3.Text = "Value"
        Me.lbCC3.Top = 3.804!
        Me.lbCC3.Width = 1.25!
        '
        'Label76
        '
        Me.Label76.Height = 0.2!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 0.2500001!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label76.Text = "4"
        Me.Label76.Top = 4.004!
        Me.Label76.Width = 1.0!
        '
        'Label77
        '
        Me.Label77.Height = 0.2!
        Me.Label77.HyperLink = Nothing
        Me.Label77.Left = 1.25!
        Me.Label77.Name = "Label77"
        Me.Label77.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label77.Text = "Stiffener DCR="
        Me.Label77.Top = 4.004!
        Me.Label77.Width = 2.25!
        '
        'Label78
        '
        Me.Label78.Height = 0.2!
        Me.Label78.HyperLink = Nothing
        Me.Label78.Left = 6.0!
        Me.Label78.Name = "Label78"
        Me.Label78.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label78.Text = "0.90"
        Me.Label78.Top = 4.004!
        Me.Label78.Width = 1.25!
        '
        'Label79
        '
        Me.Label79.Height = 0.2!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 4.75!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label79.Text = "1.05"
        Me.Label79.Top = 4.004!
        Me.Label79.Width = 1.25!
        '
        'lbCC4
        '
        Me.lbCC4.Height = 0.2!
        Me.lbCC4.HyperLink = Nothing
        Me.lbCC4.Left = 3.5!
        Me.lbCC4.Name = "lbCC4"
        Me.lbCC4.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbCC4.Text = "Value"
        Me.lbCC4.Top = 4.004!
        Me.lbCC4.Width = 1.25!
        '
        'Label81
        '
        Me.Label81.Height = 0.2!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 0.25!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "color: DarkRed; font-size: 9pt; font-weight: bold; vertical-align: middle; ddo-ch" &
    "ar-set: 1"
        Me.Label81.Text = "     ST (SHEAR TAB CHECK)"
        Me.Label81.Top = 4.204!
        Me.Label81.Width = 7.0!
        '
        'Label82
        '
        Me.Label82.Height = 0.2!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 0.25!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label82.Text = "1"
        Me.Label82.Top = 4.404!
        Me.Label82.Width = 1.0!
        '
        'Label83
        '
        Me.Label83.Height = 0.2!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 1.25!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label83.Text = "Beam Web DCR="
        Me.Label83.Top = 4.404!
        Me.Label83.Width = 2.25!
        '
        'Label84
        '
        Me.Label84.Height = 0.2!
        Me.Label84.HyperLink = Nothing
        Me.Label84.Left = 6.0!
        Me.Label84.Name = "Label84"
        Me.Label84.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label84.Text = "0.90"
        Me.Label84.Top = 4.404!
        Me.Label84.Width = 1.25!
        '
        'Label85
        '
        Me.Label85.Height = 0.2!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 4.75!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label85.Text = "1.05"
        Me.Label85.Top = 4.404!
        Me.Label85.Width = 1.25!
        '
        'lbST1
        '
        Me.lbST1.Height = 0.2!
        Me.lbST1.HyperLink = Nothing
        Me.lbST1.Left = 3.5!
        Me.lbST1.Name = "lbST1"
        Me.lbST1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbST1.Text = "Value"
        Me.lbST1.Top = 4.404!
        Me.lbST1.Width = 1.25!
        '
        'Label87
        '
        Me.Label87.Height = 0.2!
        Me.Label87.HyperLink = Nothing
        Me.Label87.Left = 0.25!
        Me.Label87.Name = "Label87"
        Me.Label87.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label87.Text = "2"
        Me.Label87.Top = 4.604!
        Me.Label87.Width = 1.0!
        '
        'Label112
        '
        Me.Label112.Height = 0.2!
        Me.Label112.HyperLink = Nothing
        Me.Label112.Left = 1.25!
        Me.Label112.Name = "Label112"
        Me.Label112.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label112.Text = "Shear Plate DCR="
        Me.Label112.Top = 4.604!
        Me.Label112.Width = 2.25!
        '
        'Label113
        '
        Me.Label113.Height = 0.2!
        Me.Label113.HyperLink = Nothing
        Me.Label113.Left = 6.0!
        Me.Label113.Name = "Label113"
        Me.Label113.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label113.Text = "0.90"
        Me.Label113.Top = 4.604!
        Me.Label113.Width = 1.25!
        '
        'Label114
        '
        Me.Label114.Height = 0.2!
        Me.Label114.HyperLink = Nothing
        Me.Label114.Left = 4.75!
        Me.Label114.Name = "Label114"
        Me.Label114.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label114.Text = "1.05"
        Me.Label114.Top = 4.604!
        Me.Label114.Width = 1.25!
        '
        'lbST2
        '
        Me.lbST2.Height = 0.2!
        Me.lbST2.HyperLink = Nothing
        Me.lbST2.Left = 3.5!
        Me.lbST2.Name = "lbST2"
        Me.lbST2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbST2.Text = "Value"
        Me.lbST2.Top = 4.604!
        Me.lbST2.Width = 1.25!
        '
        'Label116
        '
        Me.Label116.Height = 0.2!
        Me.Label116.HyperLink = Nothing
        Me.Label116.Left = 0.25!
        Me.Label116.Name = "Label116"
        Me.Label116.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label116.Text = "3"
        Me.Label116.Top = 4.804!
        Me.Label116.Width = 1.0!
        '
        'Label117
        '
        Me.Label117.Height = 0.2!
        Me.Label117.HyperLink = Nothing
        Me.Label117.Left = 1.25!
        Me.Label117.Name = "Label117"
        Me.Label117.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label117.Text = "Bolt DCR="
        Me.Label117.Top = 4.804!
        Me.Label117.Width = 2.25!
        '
        'Label118
        '
        Me.Label118.Height = 0.2!
        Me.Label118.HyperLink = Nothing
        Me.Label118.Left = 6.0!
        Me.Label118.Name = "Label118"
        Me.Label118.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label118.Text = "0.90"
        Me.Label118.Top = 4.804!
        Me.Label118.Width = 1.25!
        '
        'Label119
        '
        Me.Label119.Height = 0.2!
        Me.Label119.HyperLink = Nothing
        Me.Label119.Left = 4.75!
        Me.Label119.Name = "Label119"
        Me.Label119.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label119.Text = "1.05"
        Me.Label119.Top = 4.804!
        Me.Label119.Width = 1.25!
        '
        'lbST3
        '
        Me.lbST3.Height = 0.2!
        Me.lbST3.HyperLink = Nothing
        Me.lbST3.Left = 3.5!
        Me.lbST3.Name = "lbST3"
        Me.lbST3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbST3.Text = "Value"
        Me.lbST3.Top = 4.804!
        Me.lbST3.Width = 1.25!
        '
        'Label121
        '
        Me.Label121.Height = 0.2!
        Me.Label121.HyperLink = Nothing
        Me.Label121.Left = 0.25!
        Me.Label121.Name = "Label121"
        Me.Label121.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label121.Text = "4"
        Me.Label121.Top = 5.004!
        Me.Label121.Width = 1.0!
        '
        'Label122
        '
        Me.Label122.Height = 0.2!
        Me.Label122.HyperLink = Nothing
        Me.Label122.Left = 1.25!
        Me.Label122.Name = "Label122"
        Me.Label122.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label122.Text = "Fillet Weld DCR="
        Me.Label122.Top = 5.004!
        Me.Label122.Width = 2.25!
        '
        'Label123
        '
        Me.Label123.Height = 0.2!
        Me.Label123.HyperLink = Nothing
        Me.Label123.Left = 6.0!
        Me.Label123.Name = "Label123"
        Me.Label123.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label123.Text = "0.90"
        Me.Label123.Top = 5.004!
        Me.Label123.Width = 1.25!
        '
        'Label124
        '
        Me.Label124.Height = 0.2!
        Me.Label124.HyperLink = Nothing
        Me.Label124.Left = 4.75!
        Me.Label124.Name = "Label124"
        Me.Label124.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label124.Text = "1.05"
        Me.Label124.Top = 5.004!
        Me.Label124.Width = 1.25!
        '
        'lbST4
        '
        Me.lbST4.Height = 0.2!
        Me.lbST4.HyperLink = Nothing
        Me.lbST4.Left = 3.5!
        Me.lbST4.Name = "lbST4"
        Me.lbST4.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbST4.Text = "Value"
        Me.lbST4.Top = 5.004!
        Me.lbST4.Width = 1.25!
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 0.2500001!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.8010001!
        Me.Line3.Width = 7.0!
        Me.Line3.X1 = 0.2500001!
        Me.Line3.X2 = 7.25!
        Me.Line3.Y1 = 0.8010001!
        Me.Line3.Y2 = 0.8010001!
        '
        'Line4
        '
        Me.Line4.Height = 0!
        Me.Line4.Left = 0.2499999!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 1.004!
        Me.Line4.Width = 7.0!
        Me.Line4.X1 = 0.2499999!
        Me.Line4.X2 = 7.25!
        Me.Line4.Y1 = 1.004!
        Me.Line4.Y2 = 1.004!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 0.2500001!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 1.204!
        Me.Line5.Width = 7.0!
        Me.Line5.X1 = 0.2500001!
        Me.Line5.X2 = 7.25!
        Me.Line5.Y1 = 1.204!
        Me.Line5.Y2 = 1.204!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 0.2499999!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 1.404!
        Me.Line6.Width = 7.0!
        Me.Line6.X1 = 0.2499999!
        Me.Line6.X2 = 7.25!
        Me.Line6.Y1 = 1.404!
        Me.Line6.Y2 = 1.404!
        '
        'Line7
        '
        Me.Line7.Height = 0!
        Me.Line7.Left = 0.2500001!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 1.604!
        Me.Line7.Width = 7.0!
        Me.Line7.X1 = 0.2500001!
        Me.Line7.X2 = 7.25!
        Me.Line7.Y1 = 1.604!
        Me.Line7.Y2 = 1.604!
        '
        'Line8
        '
        Me.Line8.Height = 0!
        Me.Line8.Left = 0.2499999!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 1.804!
        Me.Line8.Width = 7.0!
        Me.Line8.X1 = 0.2499999!
        Me.Line8.X2 = 7.25!
        Me.Line8.Y1 = 1.804!
        Me.Line8.Y2 = 1.804!
        '
        'Line9
        '
        Me.Line9.Height = 0!
        Me.Line9.Left = 0.2500001!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 2.004!
        Me.Line9.Width = 7.0!
        Me.Line9.X1 = 0.2500001!
        Me.Line9.X2 = 7.25!
        Me.Line9.Y1 = 2.004!
        Me.Line9.Y2 = 2.004!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 0.2499999!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 2.204!
        Me.Line10.Width = 7.0!
        Me.Line10.X1 = 0.2499999!
        Me.Line10.X2 = 7.25!
        Me.Line10.Y1 = 2.204!
        Me.Line10.Y2 = 2.204!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 0.2500001!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 2.404!
        Me.Line11.Width = 7.0!
        Me.Line11.X1 = 0.2500001!
        Me.Line11.X2 = 7.25!
        Me.Line11.Y1 = 2.404!
        Me.Line11.Y2 = 2.404!
        '
        'Line15
        '
        Me.Line15.Height = 0!
        Me.Line15.Left = 0.2499999!
        Me.Line15.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line15.LineWeight = 1.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 2.604!
        Me.Line15.Width = 7.0!
        Me.Line15.X1 = 0.2499999!
        Me.Line15.X2 = 7.25!
        Me.Line15.Y1 = 2.604!
        Me.Line15.Y2 = 2.604!
        '
        'Line16
        '
        Me.Line16.Height = 0!
        Me.Line16.Left = 0.2500001!
        Me.Line16.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line16.LineWeight = 1.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 2.804!
        Me.Line16.Width = 7.0!
        Me.Line16.X1 = 0.2500001!
        Me.Line16.X2 = 7.25!
        Me.Line16.Y1 = 2.804!
        Me.Line16.Y2 = 2.804!
        '
        'Line17
        '
        Me.Line17.Height = 0!
        Me.Line17.Left = 0.2499999!
        Me.Line17.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 3.004!
        Me.Line17.Width = 7.0!
        Me.Line17.X1 = 0.2499999!
        Me.Line17.X2 = 7.25!
        Me.Line17.Y1 = 3.004!
        Me.Line17.Y2 = 3.004!
        '
        'Line18
        '
        Me.Line18.Height = 0!
        Me.Line18.Left = 0.2500001!
        Me.Line18.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line18.LineWeight = 1.0!
        Me.Line18.Name = "Line18"
        Me.Line18.Top = 3.204!
        Me.Line18.Width = 7.0!
        Me.Line18.X1 = 0.2500001!
        Me.Line18.X2 = 7.25!
        Me.Line18.Y1 = 3.204!
        Me.Line18.Y2 = 3.204!
        '
        'Line19
        '
        Me.Line19.Height = 0!
        Me.Line19.Left = 0.2499999!
        Me.Line19.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line19.LineWeight = 1.0!
        Me.Line19.Name = "Line19"
        Me.Line19.Top = 3.404001!
        Me.Line19.Width = 7.0!
        Me.Line19.X1 = 0.2499999!
        Me.Line19.X2 = 7.25!
        Me.Line19.Y1 = 3.404001!
        Me.Line19.Y2 = 3.404001!
        '
        'Line20
        '
        Me.Line20.Height = 0!
        Me.Line20.Left = 0.2500001!
        Me.Line20.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line20.LineWeight = 1.0!
        Me.Line20.Name = "Line20"
        Me.Line20.Top = 3.604!
        Me.Line20.Width = 7.0!
        Me.Line20.X1 = 0.2500001!
        Me.Line20.X2 = 7.25!
        Me.Line20.Y1 = 3.604!
        Me.Line20.Y2 = 3.604!
        '
        'Line22
        '
        Me.Line22.Height = 0!
        Me.Line22.Left = 0.2499999!
        Me.Line22.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line22.LineWeight = 1.0!
        Me.Line22.Name = "Line22"
        Me.Line22.Top = 3.804!
        Me.Line22.Width = 7.0!
        Me.Line22.X1 = 0.2499999!
        Me.Line22.X2 = 7.25!
        Me.Line22.Y1 = 3.804!
        Me.Line22.Y2 = 3.804!
        '
        'Line23
        '
        Me.Line23.Height = 0!
        Me.Line23.Left = 0.2500001!
        Me.Line23.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line23.LineWeight = 1.0!
        Me.Line23.Name = "Line23"
        Me.Line23.Top = 4.004!
        Me.Line23.Width = 7.0!
        Me.Line23.X1 = 0.2500001!
        Me.Line23.X2 = 7.25!
        Me.Line23.Y1 = 4.004!
        Me.Line23.Y2 = 4.004!
        '
        'Line24
        '
        Me.Line24.Height = 0!
        Me.Line24.Left = 0.2499999!
        Me.Line24.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line24.LineWeight = 1.0!
        Me.Line24.Name = "Line24"
        Me.Line24.Top = 4.204!
        Me.Line24.Width = 7.0!
        Me.Line24.X1 = 0.2499999!
        Me.Line24.X2 = 7.25!
        Me.Line24.Y1 = 4.204!
        Me.Line24.Y2 = 4.204!
        '
        'Line25
        '
        Me.Line25.Height = 0!
        Me.Line25.Left = 0.2500001!
        Me.Line25.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line25.LineWeight = 1.0!
        Me.Line25.Name = "Line25"
        Me.Line25.Top = 4.404!
        Me.Line25.Width = 7.0!
        Me.Line25.X1 = 0.2500001!
        Me.Line25.X2 = 7.25!
        Me.Line25.Y1 = 4.404!
        Me.Line25.Y2 = 4.404!
        '
        'Line26
        '
        Me.Line26.Height = 0!
        Me.Line26.Left = 0.2499999!
        Me.Line26.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line26.LineWeight = 1.0!
        Me.Line26.Name = "Line26"
        Me.Line26.Top = 4.604!
        Me.Line26.Width = 7.0!
        Me.Line26.X1 = 0.2499999!
        Me.Line26.X2 = 7.25!
        Me.Line26.Y1 = 4.604!
        Me.Line26.Y2 = 4.604!
        '
        'Line36
        '
        Me.Line36.Height = 0!
        Me.Line36.Left = 0.2500001!
        Me.Line36.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line36.LineWeight = 1.0!
        Me.Line36.Name = "Line36"
        Me.Line36.Top = 4.804!
        Me.Line36.Width = 7.0!
        Me.Line36.X1 = 0.2500001!
        Me.Line36.X2 = 7.25!
        Me.Line36.Y1 = 4.804!
        Me.Line36.Y2 = 4.804!
        '
        'Line37
        '
        Me.Line37.Height = 0!
        Me.Line37.Left = 0.2499999!
        Me.Line37.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line37.LineWeight = 1.0!
        Me.Line37.Name = "Line37"
        Me.Line37.Top = 5.004!
        Me.Line37.Width = 7.0!
        Me.Line37.X1 = 0.2499999!
        Me.Line37.X2 = 7.25!
        Me.Line37.Y1 = 5.004!
        Me.Line37.Y2 = 5.004!
        '
        'Line38
        '
        Me.Line38.Height = 0!
        Me.Line38.Left = 0.2500001!
        Me.Line38.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line38.LineWeight = 1.0!
        Me.Line38.Name = "Line38"
        Me.Line38.Top = 5.204!
        Me.Line38.Width = 7.0!
        Me.Line38.X1 = 0.2500001!
        Me.Line38.X2 = 7.25!
        Me.Line38.Y1 = 5.204!
        Me.Line38.Y2 = 5.204!
        '
        'Line39
        '
        Me.Line39.Height = 4.597!
        Me.Line39.Left = 0.2500001!
        Me.Line39.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line39.LineWeight = 1.0!
        Me.Line39.Name = "Line39"
        Me.Line39.Top = 0.6070001!
        Me.Line39.Width = 0!
        Me.Line39.X1 = 0.2500001!
        Me.Line39.X2 = 0.2500001!
        Me.Line39.Y1 = 0.6070001!
        Me.Line39.Y2 = 5.204!
        '
        'Line40
        '
        Me.Line40.Height = 4.597001!
        Me.Line40.Left = 7.25!
        Me.Line40.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line40.LineWeight = 1.0!
        Me.Line40.Name = "Line40"
        Me.Line40.Top = 0.6070001!
        Me.Line40.Width = 0!
        Me.Line40.X1 = 7.25!
        Me.Line40.X2 = 7.25!
        Me.Line40.Y1 = 0.6070001!
        Me.Line40.Y2 = 5.204001!
        '
        'Line45
        '
        Me.Line45.Height = 0.8000011!
        Me.Line45.Left = 6.0!
        Me.Line45.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line45.LineWeight = 1.0!
        Me.Line45.Name = "Line45"
        Me.Line45.Top = 4.404!
        Me.Line45.Width = 0!
        Me.Line45.X1 = 6.0!
        Me.Line45.X2 = 6.0!
        Me.Line45.Y1 = 4.404!
        Me.Line45.Y2 = 5.204001!
        '
        'Line46
        '
        Me.Line46.Height = 0.8000011!
        Me.Line46.Left = 4.75!
        Me.Line46.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line46.LineWeight = 1.0!
        Me.Line46.Name = "Line46"
        Me.Line46.Top = 4.404!
        Me.Line46.Width = 0!
        Me.Line46.X1 = 4.75!
        Me.Line46.X2 = 4.75!
        Me.Line46.Y1 = 4.404!
        Me.Line46.Y2 = 5.204001!
        '
        'Line47
        '
        Me.Line47.Height = 0.8000011!
        Me.Line47.Left = 3.5!
        Me.Line47.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line47.LineWeight = 1.0!
        Me.Line47.Name = "Line47"
        Me.Line47.Top = 4.404!
        Me.Line47.Width = 0!
        Me.Line47.X1 = 3.5!
        Me.Line47.X2 = 3.5!
        Me.Line47.Y1 = 4.404!
        Me.Line47.Y2 = 5.204001!
        '
        'Line48
        '
        Me.Line48.Height = 0.8000011!
        Me.Line48.Left = 1.25!
        Me.Line48.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line48.LineWeight = 1.0!
        Me.Line48.Name = "Line48"
        Me.Line48.Top = 4.404!
        Me.Line48.Width = 0!
        Me.Line48.X1 = 1.25!
        Me.Line48.X2 = 1.25!
        Me.Line48.Y1 = 4.404!
        Me.Line48.Y2 = 5.204001!
        '
        'Line49
        '
        Me.Line49.Height = 0.8000009!
        Me.Line49.Left = 1.25!
        Me.Line49.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line49.LineWeight = 1.0!
        Me.Line49.Name = "Line49"
        Me.Line49.Top = 3.404!
        Me.Line49.Width = 0!
        Me.Line49.X1 = 1.25!
        Me.Line49.X2 = 1.25!
        Me.Line49.Y1 = 3.404!
        Me.Line49.Y2 = 4.204001!
        '
        'Line50
        '
        Me.Line50.Height = 0.8000009!
        Me.Line50.Left = 3.5!
        Me.Line50.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line50.LineWeight = 1.0!
        Me.Line50.Name = "Line50"
        Me.Line50.Top = 3.404!
        Me.Line50.Width = 0!
        Me.Line50.X1 = 3.5!
        Me.Line50.X2 = 3.5!
        Me.Line50.Y1 = 3.404!
        Me.Line50.Y2 = 4.204001!
        '
        'Line51
        '
        Me.Line51.Height = 0.8000009!
        Me.Line51.Left = 4.750001!
        Me.Line51.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line51.LineWeight = 1.0!
        Me.Line51.Name = "Line51"
        Me.Line51.Top = 3.404!
        Me.Line51.Width = 0!
        Me.Line51.X1 = 4.750001!
        Me.Line51.X2 = 4.750001!
        Me.Line51.Y1 = 3.404!
        Me.Line51.Y2 = 4.204001!
        '
        'Line52
        '
        Me.Line52.Height = 0.8000009!
        Me.Line52.Left = 6.0!
        Me.Line52.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line52.LineWeight = 1.0!
        Me.Line52.Name = "Line52"
        Me.Line52.Top = 3.404!
        Me.Line52.Width = 0!
        Me.Line52.X1 = 6.0!
        Me.Line52.X2 = 6.0!
        Me.Line52.Y1 = 3.404!
        Me.Line52.Y2 = 4.204001!
        '
        'Line53
        '
        Me.Line53.Height = 1.0!
        Me.Line53.Left = 6.0!
        Me.Line53.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line53.LineWeight = 1.0!
        Me.Line53.Name = "Line53"
        Me.Line53.Top = 2.204!
        Me.Line53.Width = 0!
        Me.Line53.X1 = 6.0!
        Me.Line53.X2 = 6.0!
        Me.Line53.Y1 = 2.204!
        Me.Line53.Y2 = 3.204!
        '
        'Line54
        '
        Me.Line54.Height = 1.0!
        Me.Line54.Left = 4.75!
        Me.Line54.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line54.LineWeight = 1.0!
        Me.Line54.Name = "Line54"
        Me.Line54.Top = 2.204!
        Me.Line54.Width = 0!
        Me.Line54.X1 = 4.75!
        Me.Line54.X2 = 4.75!
        Me.Line54.Y1 = 2.204!
        Me.Line54.Y2 = 3.204!
        '
        'Line55
        '
        Me.Line55.Height = 1.0!
        Me.Line55.Left = 3.5!
        Me.Line55.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line55.LineWeight = 1.0!
        Me.Line55.Name = "Line55"
        Me.Line55.Top = 2.204!
        Me.Line55.Width = 0!
        Me.Line55.X1 = 3.5!
        Me.Line55.X2 = 3.5!
        Me.Line55.Y1 = 2.204!
        Me.Line55.Y2 = 3.204!
        '
        'Line56
        '
        Me.Line56.Height = 1.0!
        Me.Line56.Left = 1.25!
        Me.Line56.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line56.LineWeight = 1.0!
        Me.Line56.Name = "Line56"
        Me.Line56.Top = 2.204!
        Me.Line56.Width = 0!
        Me.Line56.X1 = 1.25!
        Me.Line56.X2 = 1.25!
        Me.Line56.Y1 = 2.204!
        Me.Line56.Y2 = 3.204!
        '
        'Line57
        '
        Me.Line57.Height = 1.0!
        Me.Line57.Left = 1.25!
        Me.Line57.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line57.LineWeight = 1.0!
        Me.Line57.Name = "Line57"
        Me.Line57.Top = 1.004!
        Me.Line57.Width = 0!
        Me.Line57.X1 = 1.25!
        Me.Line57.X2 = 1.25!
        Me.Line57.Y1 = 1.004!
        Me.Line57.Y2 = 2.004!
        '
        'Line58
        '
        Me.Line58.Height = 1.0!
        Me.Line58.Left = 3.5!
        Me.Line58.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line58.LineWeight = 1.0!
        Me.Line58.Name = "Line58"
        Me.Line58.Top = 1.004!
        Me.Line58.Width = 0!
        Me.Line58.X1 = 3.5!
        Me.Line58.X2 = 3.5!
        Me.Line58.Y1 = 1.004!
        Me.Line58.Y2 = 2.004!
        '
        'Line59
        '
        Me.Line59.Height = 1.0!
        Me.Line59.Left = 4.750001!
        Me.Line59.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line59.LineWeight = 1.0!
        Me.Line59.Name = "Line59"
        Me.Line59.Top = 1.004!
        Me.Line59.Width = 0!
        Me.Line59.X1 = 4.750001!
        Me.Line59.X2 = 4.750001!
        Me.Line59.Y1 = 1.004!
        Me.Line59.Y2 = 2.004!
        '
        'Line60
        '
        Me.Line60.Height = 1.0!
        Me.Line60.Left = 6.0!
        Me.Line60.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line60.LineWeight = 1.0!
        Me.Line60.Name = "Line60"
        Me.Line60.Top = 1.004!
        Me.Line60.Width = 0!
        Me.Line60.X1 = 6.0!
        Me.Line60.X2 = 6.0!
        Me.Line60.Y1 = 1.004!
        Me.Line60.Y2 = 2.004!
        '
        'Label37
        '
        Me.Label37.Height = 0.2!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 1.25!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label37.Text = "Doubler PL to col web/Cont plate Weld:"
        Me.Label37.Top = 5.525!
        Me.Label37.Width = 2.25!
        '
        'lbWeldWeb
        '
        Me.lbWeldWeb.Height = 0.2!
        Me.lbWeldWeb.HyperLink = Nothing
        Me.lbWeldWeb.Left = 3.5!
        Me.lbWeldWeb.Name = "lbWeldWeb"
        Me.lbWeldWeb.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbWeldWeb.Text = "Value"
        Me.lbWeldWeb.Top = 5.525!
        Me.lbWeldWeb.Width = 1.25!
        '
        'Label39
        '
        Me.Label39.Height = 0.2!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 1.25!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label39.Text = "Doubler PL to col flange weld:"
        Me.Label39.Top = 5.725!
        Me.Label39.Width = 2.25!
        '
        'lbWeldFLG
        '
        Me.lbWeldFLG.Height = 0.2!
        Me.lbWeldFLG.HyperLink = Nothing
        Me.lbWeldFLG.Left = 3.5!
        Me.lbWeldFLG.Name = "lbWeldFLG"
        Me.lbWeldFLG.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbWeldFLG.Text = "Value"
        Me.lbWeldFLG.Top = 5.725!
        Me.lbWeldFLG.Width = 1.25!
        '
        'Label41
        '
        Me.Label41.Height = 0.2!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 1.25!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label41.Text = "Use of Plug-weld for Doubler Plates?"
        Me.Label41.Top = 5.925001!
        Me.Label41.Width = 2.25!
        '
        'lbWeld3
        '
        Me.lbWeld3.Height = 0.2!
        Me.lbWeld3.HyperLink = Nothing
        Me.lbWeld3.Left = 3.5!
        Me.lbWeld3.Name = "lbWeld3"
        Me.lbWeld3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbWeld3.Text = "Value"
        Me.lbWeld3.Top = 5.925001!
        Me.lbWeld3.Width = 1.25!
        '
        'lbWeld1_B
        '
        Me.lbWeld1_B.Height = 0.2!
        Me.lbWeld1_B.HyperLink = Nothing
        Me.lbWeld1_B.Left = 4.75!
        Me.lbWeld1_B.Name = "lbWeld1_B"
        Me.lbWeld1_B.Style = "font-size: 9pt; font-weight: normal; text-align: left; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.lbWeld1_B.Text = "  Doubler without continuity plates"
        Me.lbWeld1_B.Top = 5.525!
        Me.lbWeld1_B.Width = 2.5!
        '
        'lbWeld2_B
        '
        Me.lbWeld2_B.Height = 0.2!
        Me.lbWeld2_B.HyperLink = Nothing
        Me.lbWeld2_B.Left = 4.75!
        Me.lbWeld2_B.Name = "lbWeld2_B"
        Me.lbWeld2_B.Style = "font-size: 9pt; font-weight: normal; text-align: left; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.lbWeld2_B.Text = "  CJP Weld"
        Me.lbWeld2_B.Top = 5.725!
        Me.lbWeld2_B.Width = 2.5!
        '
        'pic1
        '
        Me.pic1.Height = 2.375!
        Me.pic1.ImageData = Nothing
        Me.pic1.Left = 0.25!
        Me.pic1.Name = "pic1"
        Me.pic1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.pic1.Top = 6.125!
        Me.pic1.Width = 7.0!
        '
        'Line27
        '
        Me.Line27.Height = 0!
        Me.Line27.Left = 0.25!
        Me.Line27.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line27.LineWeight = 1.0!
        Me.Line27.Name = "Line27"
        Me.Line27.Top = 5.525!
        Me.Line27.Width = 7.0!
        Me.Line27.X1 = 0.25!
        Me.Line27.X2 = 7.25!
        Me.Line27.Y1 = 5.525!
        Me.Line27.Y2 = 5.525!
        '
        'Label45
        '
        Me.Label45.Height = 0.2!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 0.25!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label45.Text = "1"
        Me.Label45.Top = 5.525!
        Me.Label45.Width = 1.0!
        '
        'Label46
        '
        Me.Label46.Height = 0.2!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 0.25!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label46.Text = "2"
        Me.Label46.Top = 5.725!
        Me.Label46.Width = 1.0!
        '
        'Label47
        '
        Me.Label47.Height = 0.2!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 0.25!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label47.Text = "3"
        Me.Label47.Top = 5.925!
        Me.Label47.Width = 1.0!
        '
        'Line28
        '
        Me.Line28.Height = 0!
        Me.Line28.Left = 0.25!
        Me.Line28.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line28.LineWeight = 1.0!
        Me.Line28.Name = "Line28"
        Me.Line28.Top = 5.725!
        Me.Line28.Width = 7.0!
        Me.Line28.X1 = 0.25!
        Me.Line28.X2 = 7.25!
        Me.Line28.Y1 = 5.725!
        Me.Line28.Y2 = 5.725!
        '
        'Line29
        '
        Me.Line29.Height = 0!
        Me.Line29.Left = 0.25!
        Me.Line29.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line29.LineWeight = 1.0!
        Me.Line29.Name = "Line29"
        Me.Line29.Top = 5.925001!
        Me.Line29.Width = 7.0!
        Me.Line29.X1 = 0.25!
        Me.Line29.X2 = 7.25!
        Me.Line29.Y1 = 5.925001!
        Me.Line29.Y2 = 5.925001!
        '
        'Line30
        '
        Me.Line30.Height = 0!
        Me.Line30.Left = 0.25!
        Me.Line30.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line30.LineWeight = 1.0!
        Me.Line30.Name = "Line30"
        Me.Line30.Top = 6.125!
        Me.Line30.Width = 7.0!
        Me.Line30.X1 = 0.25!
        Me.Line30.X2 = 7.25!
        Me.Line30.Y1 = 6.125!
        Me.Line30.Y2 = 6.125!
        '
        'Line31
        '
        Me.Line31.Height = 0.5999999!
        Me.Line31.Left = 0.25!
        Me.Line31.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line31.LineWeight = 1.0!
        Me.Line31.Name = "Line31"
        Me.Line31.Top = 5.525!
        Me.Line31.Width = 0.0000002086163!
        Me.Line31.X1 = 0.2500002!
        Me.Line31.X2 = 0.25!
        Me.Line31.Y1 = 5.525!
        Me.Line31.Y2 = 6.125!
        '
        'Line32
        '
        Me.Line32.Height = 0.5999999!
        Me.Line32.Left = 1.25!
        Me.Line32.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line32.LineWeight = 1.0!
        Me.Line32.Name = "Line32"
        Me.Line32.Top = 5.525!
        Me.Line32.Width = 0!
        Me.Line32.X1 = 1.25!
        Me.Line32.X2 = 1.25!
        Me.Line32.Y1 = 5.525!
        Me.Line32.Y2 = 6.125!
        '
        'Line33
        '
        Me.Line33.Height = 0.5999999!
        Me.Line33.Left = 3.5!
        Me.Line33.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line33.LineWeight = 1.0!
        Me.Line33.Name = "Line33"
        Me.Line33.Top = 5.525!
        Me.Line33.Width = 0!
        Me.Line33.X1 = 3.5!
        Me.Line33.X2 = 3.5!
        Me.Line33.Y1 = 5.525!
        Me.Line33.Y2 = 6.125!
        '
        'Line34
        '
        Me.Line34.Height = 0.5999999!
        Me.Line34.Left = 4.75!
        Me.Line34.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line34.LineWeight = 1.0!
        Me.Line34.Name = "Line34"
        Me.Line34.Top = 5.525!
        Me.Line34.Width = 0!
        Me.Line34.X1 = 4.75!
        Me.Line34.X2 = 4.75!
        Me.Line34.Y1 = 5.525!
        Me.Line34.Y2 = 6.125!
        '
        'Line35
        '
        Me.Line35.Height = 0.5999999!
        Me.Line35.Left = 7.25!
        Me.Line35.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line35.LineWeight = 1.0!
        Me.Line35.Name = "Line35"
        Me.Line35.Top = 5.525!
        Me.Line35.Width = 0!
        Me.Line35.X1 = 7.25!
        Me.Line35.X2 = 7.25!
        Me.Line35.Y1 = 5.525!
        Me.Line35.Y2 = 6.125!
        '
        'Line41
        '
        Me.Line41.Height = 0.0000002980232!
        Me.Line41.Left = 0.25!
        Me.Line41.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line41.LineWeight = 1.0!
        Me.Line41.Name = "Line41"
        Me.Line41.Top = 0.465!
        Me.Line41.Width = 3.25!
        Me.Line41.X1 = 0.25!
        Me.Line41.X2 = 3.5!
        Me.Line41.Y1 = 0.465!
        Me.Line41.Y2 = 0.4650003!
        '
        'Line42
        '
        Me.Line42.Height = 0!
        Me.Line42.Left = 0.25!
        Me.Line42.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line42.LineWeight = 1.0!
        Me.Line42.Name = "Line42"
        Me.Line42.Top = 0.265!
        Me.Line42.Width = 3.25!
        Me.Line42.X1 = 0.25!
        Me.Line42.X2 = 3.5!
        Me.Line42.Y1 = 0.265!
        Me.Line42.Y2 = 0.265!
        '
        'Line61
        '
        Me.Line61.Height = 0.2029993!
        Me.Line61.Left = 3.5!
        Me.Line61.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line61.LineWeight = 1.0!
        Me.Line61.Name = "Line61"
        Me.Line61.Top = 0.2620003!
        Me.Line61.Width = 0!
        Me.Line61.X1 = 3.5!
        Me.Line61.X2 = 3.5!
        Me.Line61.Y1 = 0.2620003!
        Me.Line61.Y2 = 0.4649996!
        '
        'Line62
        '
        Me.Line62.Height = 0.2029993!
        Me.Line62.Left = 2.25!
        Me.Line62.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line62.LineWeight = 1.0!
        Me.Line62.Name = "Line62"
        Me.Line62.Top = 0.262!
        Me.Line62.Width = 0!
        Me.Line62.X1 = 2.25!
        Me.Line62.X2 = 2.25!
        Me.Line62.Y1 = 0.262!
        Me.Line62.Y2 = 0.4649993!
        '
        'Line63
        '
        Me.Line63.Height = 0.2029993!
        Me.Line63.Left = 0.25!
        Me.Line63.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line63.LineWeight = 1.0!
        Me.Line63.Name = "Line63"
        Me.Line63.Top = 0.2620003!
        Me.Line63.Width = 0!
        Me.Line63.X1 = 0.25!
        Me.Line63.X2 = 0.25!
        Me.Line63.Y1 = 0.2620003!
        Me.Line63.Y2 = 0.4649996!
        '
        'Line64
        '
        Me.Line64.Height = 0.0000004768372!
        Me.Line64.Left = 4.0!
        Me.Line64.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line64.LineWeight = 1.0!
        Me.Line64.Name = "Line64"
        Me.Line64.Top = 0.4680004!
        Me.Line64.Width = 3.25!
        Me.Line64.X1 = 4.0!
        Me.Line64.X2 = 7.25!
        Me.Line64.Y1 = 0.4680004!
        Me.Line64.Y2 = 0.4680009!
        '
        'Line65
        '
        Me.Line65.Height = 0.2029991!
        Me.Line65.Left = 7.25!
        Me.Line65.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line65.LineWeight = 1.0!
        Me.Line65.Name = "Line65"
        Me.Line65.Top = 0.2650008!
        Me.Line65.Width = 0!
        Me.Line65.X1 = 7.25!
        Me.Line65.X2 = 7.25!
        Me.Line65.Y1 = 0.2650008!
        Me.Line65.Y2 = 0.4679999!
        '
        'Line66
        '
        Me.Line66.Height = 0.2029992!
        Me.Line66.Left = 6.0!
        Me.Line66.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line66.LineWeight = 1.0!
        Me.Line66.Name = "Line66"
        Me.Line66.Top = 0.2650003!
        Me.Line66.Width = 0!
        Me.Line66.X1 = 6.0!
        Me.Line66.X2 = 6.0!
        Me.Line66.Y1 = 0.2650003!
        Me.Line66.Y2 = 0.4679995!
        '
        'Line67
        '
        Me.Line67.Height = 0.2029993!
        Me.Line67.Left = 4.0!
        Me.Line67.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line67.LineWeight = 1.0!
        Me.Line67.Name = "Line67"
        Me.Line67.Top = 0.268!
        Me.Line67.Width = 0!
        Me.Line67.X1 = 4.0!
        Me.Line67.X2 = 4.0!
        Me.Line67.Y1 = 0.268!
        Me.Line67.Y2 = 0.4709993!
        '
        'Line68
        '
        Me.Line68.Height = 0.0000004172325!
        Me.Line68.Left = 4.0!
        Me.Line68.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line68.LineWeight = 1.0!
        Me.Line68.Name = "Line68"
        Me.Line68.Top = 0.2679999!
        Me.Line68.Width = 3.25!
        Me.Line68.X1 = 4.0!
        Me.Line68.X2 = 7.25!
        Me.Line68.Y1 = 0.2679999!
        Me.Line68.Y2 = 0.2680003!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.lbJobID, Me.lbJobName, Me.Label2, Me.Label3, Me.lbPrintedDate})
        Me.GroupHeader1.Height = 0.625!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'lbJobID
        '
        Me.lbJobID.Height = 0.2!
        Me.lbJobID.HyperLink = Nothing
        Me.lbJobID.Left = 0.5!
        Me.lbJobID.Name = "lbJobID"
        Me.lbJobID.Style = "font-size: 9.75pt; font-weight: normal"
        Me.lbJobID.Text = "ES-123456" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobID.Top = 0.187!
        Me.lbJobID.Width = 1.75!
        '
        'lbJobName
        '
        Me.lbJobName.Height = 0.2!
        Me.lbJobName.HyperLink = Nothing
        Me.lbJobName.Left = 0.75!
        Me.lbJobName.Name = "lbJobName"
        Me.lbJobName.Style = "font-size: 9.75pt; font-weight: normal"
        Me.lbJobName.Text = "Test Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobName.Top = 0!
        Me.lbJobName.Width = 4.063!
        '
        'Label2
        '
        Me.Label2.Height = 0.2!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 9.75pt; font-weight: normal"
        Me.Label2.Text = "Job ID: " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 0.187!
        Me.Label2.Width = 0.5!
        '
        'Label3
        '
        Me.Label3.Height = 0.2!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 9.75pt; font-weight: normal"
        Me.Label3.Text = "Job Name:"
        Me.Label3.Top = 0!
        Me.Label3.Width = 0.75!
        '
        'lbPrintedDate
        '
        Me.lbPrintedDate.Height = 0.2!
        Me.lbPrintedDate.HyperLink = Nothing
        Me.lbPrintedDate.Left = 4.813!
        Me.lbPrintedDate.Name = "lbPrintedDate"
        Me.lbPrintedDate.Style = "font-size: 9.75pt; font-weight: normal; text-align: right"
        Me.lbPrintedDate.Text = "Date Printed: Jun 17, 2018" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbPrintedDate.Top = 0!
        Me.lbPrintedDate.Width = 2.687!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.01041666!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'PageHeader1
        '
        Me.PageHeader1.Height = 0!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'PageFooter1
        '
        Me.PageFooter1.Height = 0!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'US_Sum1V_B
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.0!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbSeismicDriftLimit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWindDriftLimit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDGVName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbILS1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbILS2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbILS3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbILS4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbILS5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbBLC1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbBLC2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbBLC3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbBLC4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbBLC5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbCC1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbCC2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbCC3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbCC4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbST1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbST2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbST3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbST4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWeldWeb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWeldFLG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWeld3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWeld1_B, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWeld2_B, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents GroupHeader1 As GrapeCity.ActiveReports.SectionReportModel.GroupHeader
    Private WithEvents GroupFooter1 As GrapeCity.ActiveReports.SectionReportModel.GroupFooter
    Private WithEvents PageHeader1 As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents PageFooter1 As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    Private WithEvents lbDGVName As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobID As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobName As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbPrintedDate As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line12 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line13 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line14 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line21 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label70 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line44 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line43 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label88 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbILS1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbILS2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbILS3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbILS4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbILS5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label89 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label90 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbBLC1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label92 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label93 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label94 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label95 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbBLC2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label97 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label98 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label99 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label100 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbBLC3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label102 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label103 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label104 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label105 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbBLC4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label107 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label108 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label109 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label110 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbBLC5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label61 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbCC1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label63 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label64 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label65 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbCC2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label68 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label69 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label73 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label74 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbCC3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label77 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label78 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label79 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbCC4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label81 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label82 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label83 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label84 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label85 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbST1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label87 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label112 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label113 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label114 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbST2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label116 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label117 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label118 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label119 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbST3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label121 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label122 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label123 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label124 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbST4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line15 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line16 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line17 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line18 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line19 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line20 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line22 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line23 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line24 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line25 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line26 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line36 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line37 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line38 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line39 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line40 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line45 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line46 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line47 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line48 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line49 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line50 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line51 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line52 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line53 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line54 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line55 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line56 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line57 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line58 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line59 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line60 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents lbSeismicDriftLimit As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label127 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbWeldWeb As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbWeldFLG As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbWeld3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbWeld1_B As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbWeld2_B As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents pic1 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Line27 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label47 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line28 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line29 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line30 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line31 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line32 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line33 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line34 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line35 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line41 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line42 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line61 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line62 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line63 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents lbWindDriftLimit As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line64 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line65 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line66 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line67 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line68 As GrapeCity.ActiveReports.SectionReportModel.Line
End Class
