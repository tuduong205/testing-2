﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_SPC7
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_SPC7))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S49_C3_054 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label613 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture8 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label614 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label615 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label616 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label618 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label619 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label620 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label622 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label623 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label625 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label626 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label628 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label629 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label631 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label632 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label634 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label635 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label637 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label638 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label640 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label641 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label643 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label644 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label646 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label647 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label649 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label650 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label652 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label653 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label655 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label656 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label658 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label659 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label661 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label662 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label669 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label664 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label665 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label667 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label668 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label682 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label671 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label672 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label673 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label675 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label44 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_034 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label48 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_035 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label51 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_036 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label55 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_037 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_038 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label61 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_039 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label63 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label64 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_040 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label67 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_041 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label69 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label70 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_042 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label72 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label73 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_043 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label75 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label77 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_044 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label79 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label80 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_045 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label82 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label83 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_046 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label85 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label86 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_047 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label88 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label89 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_048 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label91 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label92 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_049 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label94 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label95 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_050 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label97 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label98 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_051 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label100 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label101 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_052 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label106 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label107 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C3_053 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label104 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label105 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label109 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label279 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label277 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label273 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label271 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line12 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label56 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label62 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line13 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line14 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line15 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line16 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape20 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape19 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape18 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.S49_C3_054, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label613, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label614, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label615, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label616, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label618, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label619, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label620, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label622, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label623, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label625, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label626, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label628, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label629, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label631, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label632, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label634, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label635, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label637, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label638, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label640, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label641, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label643, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label644, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label646, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label647, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label649, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label650, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label652, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label653, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label655, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label656, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label658, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label659, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label661, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label662, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label669, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label664, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label665, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label667, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label668, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label682, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label671, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label672, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label673, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label675, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_033, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_034, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_035, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_036, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_037, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_038, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_039, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_040, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_041, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_042, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_043, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_044, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_045, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_046, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_047, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_048, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_049, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_050, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_051, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_052, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C3_053, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label277, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S49_C3_054, Me.S49_C3_020, Me.S49_C3_019, Me.S49_C3_001, Me.Label30, Me.SUM_032, Me.SUM_031, Me.SUM_030, Me.SUM_029, Me.Label3, Me.Label27, Me.Label24, Me.Label14, Me.SUM_025, Me.SUM_026, Me.SUM_027, Me.SUM_028, Me.SUM_024, Me.SUM_023, Me.SUM_022, Me.SUM_021, Me.SUM_017, Me.SUM_018, Me.SUM_019, Me.SUM_020, Me.SUM_014, Me.SUM_015, Me.SUM_011, Me.SUM_007, Me.SUM_003, Me.SUM_016, Me.SUM_012, Me.SUM_008, Me.SUM_004, Me.Label21, Me.Label613, Me.Picture8, Me.Label614, Me.Label615, Me.Label616, Me.Label618, Me.Label619, Me.Label620, Me.S49_C3_003, Me.Label622, Me.Label623, Me.S49_C3_004, Me.Label625, Me.Label626, Me.S49_C3_005, Me.Label628, Me.Label629, Me.S49_C3_006, Me.Label631, Me.Label632, Me.S49_C3_007, Me.Label634, Me.Label635, Me.S49_C3_008, Me.Label637, Me.Label638, Me.S49_C3_009, Me.Label640, Me.Label641, Me.S49_C3_010, Me.Label643, Me.Label644, Me.S49_C3_011, Me.Label646, Me.Label647, Me.S49_C3_012, Me.Label649, Me.Label650, Me.S49_C3_013, Me.Label652, Me.Label653, Me.S49_C3_014, Me.Label655, Me.Label656, Me.S49_C3_015, Me.Label658, Me.Label659, Me.S49_C3_016, Me.Label661, Me.Label662, Me.S49_C3_017, Me.Label669, Me.Label1, Me.Label2, Me.S49_C3_002, Me.Label664, Me.Label665, Me.S49_C3_018, Me.Label667, Me.Label668, Me.Label682, Me.Label671, Me.Label672, Me.Label673, Me.Label675, Me.Label12, Me.Label13, Me.S49_C3_021, Me.Label46, Me.Label4, Me.Label5, Me.S49_C3_022, Me.Label7, Me.Label8, Me.S49_C3_023, Me.Label10, Me.Label11, Me.S49_C3_024, Me.Label16, Me.Label17, Me.S49_C3_025, Me.Label19, Me.Label20, Me.S49_C3_026, Me.Label22, Me.Label23, Me.S49_C3_027, Me.Label25, Me.Label26, Me.S49_C3_028, Me.Label28, Me.Label29, Me.S49_C3_029, Me.Label31, Me.Label32, Me.S49_C3_030, Me.Label34, Me.Label35, Me.S49_C3_031, Me.Label37, Me.Label38, Me.Label39, Me.S49_C3_032, Me.Label41, Me.Label42, Me.S49_C3_033, Me.Label44, Me.Label45, Me.S49_C3_034, Me.Label48, Me.Label49, Me.S49_C3_035, Me.Label51, Me.Label52, Me.S49_C3_036, Me.Label54, Me.Label55, Me.S49_C3_037, Me.Label57, Me.Label58, Me.S49_C3_038, Me.Label60, Me.Label61, Me.S49_C3_039, Me.Label63, Me.Label64, Me.S49_C3_040, Me.Label66, Me.Label67, Me.S49_C3_041, Me.Label69, Me.Label70, Me.S49_C3_042, Me.Label72, Me.Label73, Me.S49_C3_043, Me.Label75, Me.Label76, Me.Label77, Me.S49_C3_044, Me.Label79, Me.Label80, Me.S49_C3_045, Me.Label82, Me.Label83, Me.S49_C3_046, Me.Label85, Me.Label86, Me.S49_C3_047, Me.Label88, Me.Label89, Me.S49_C3_048, Me.Label91, Me.Label92, Me.S49_C3_049, Me.Label94, Me.Label95, Me.S49_C3_050, Me.Label97, Me.Label98, Me.S49_C3_051, Me.Label100, Me.Label101, Me.S49_C3_052, Me.Label106, Me.Label107, Me.S49_C3_053, Me.Label104, Me.Label105, Me.Label109, Me.SUM_002, Me.SUM_001, Me.Label279, Me.Label277, Me.Label273, Me.Label271, Me.SUM_005, Me.SUM_009, Me.SUM_013, Me.SUM_006, Me.SUM_010, Me.Line9, Me.Line3, Me.Line4, Me.Line5, Me.Line6, Me.Line12, Me.Label56, Me.Label59, Me.Label62, Me.Line7, Me.Line2, Me.Line1, Me.Line13, Me.Line14, Me.Line15, Me.Line8, Me.Line10, Me.Line11, Me.Line16, Me.Shape1, Me.Shape20, Me.Shape19, Me.Shape18, Me.PageBreak1})
        Me.Detail.Height = 19.375!
        Me.Detail.Name = "Detail"
        '
        'S49_C3_054
        '
        Me.S49_C3_054.Height = 0.1875!
        Me.S49_C3_054.HyperLink = Nothing
        Me.S49_C3_054.Left = 2.25!
        Me.S49_C3_054.Name = "S49_C3_054"
        Me.S49_C3_054.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C3_054.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_054.Top = 17.875!
        Me.S49_C3_054.Width = 1.0!
        '
        'S49_C3_020
        '
        Me.S49_C3_020.Height = 0.1875!
        Me.S49_C3_020.HyperLink = Nothing
        Me.S49_C3_020.Left = 2.25!
        Me.S49_C3_020.Name = "S49_C3_020"
        Me.S49_C3_020.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C3_020.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_020.Top = 9.875!
        Me.S49_C3_020.Width = 1.0!
        '
        'S49_C3_019
        '
        Me.S49_C3_019.Height = 0.1875!
        Me.S49_C3_019.HyperLink = Nothing
        Me.S49_C3_019.Left = 2.25!
        Me.S49_C3_019.Name = "S49_C3_019"
        Me.S49_C3_019.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C3_019.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_019.Top = 9.25!
        Me.S49_C3_019.Width = 1.0!
        '
        'S49_C3_001
        '
        Me.S49_C3_001.Height = 0.1875!
        Me.S49_C3_001.HyperLink = Nothing
        Me.S49_C3_001.Left = 2.25!
        Me.S49_C3_001.Name = "S49_C3_001"
        Me.S49_C3_001.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C3_001.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_001.Top = 5.375!
        Me.S49_C3_001.Width = 1.0!
        '
        'Label30
        '
        Me.Label30.Height = 0.188!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 6.75!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label30.Text = "Check"
        Me.Label30.Top = 18.375!
        Me.Label30.Width = 0.75!
        '
        'SUM_032
        '
        Me.SUM_032.Height = 0.188!
        Me.SUM_032.HyperLink = Nothing
        Me.SUM_032.Left = 6.750001!
        Me.SUM_032.Name = "SUM_032"
        Me.SUM_032.Style = "background-color: Yellow; color: Green; font-size: 8.25pt; font-weight: normal; t" &
    "ext-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_032.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_032.Top = 19.125!
        Me.SUM_032.Width = 0.75!
        '
        'SUM_031
        '
        Me.SUM_031.Height = 0.188!
        Me.SUM_031.HyperLink = Nothing
        Me.SUM_031.Left = 6.751003!
        Me.SUM_031.Name = "SUM_031"
        Me.SUM_031.Style = "background-color: Yellow; color: Green; font-size: 8.25pt; font-weight: normal; t" &
    "ext-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_031.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_031.Top = 18.937!
        Me.SUM_031.Width = 0.75!
        '
        'SUM_030
        '
        Me.SUM_030.Height = 0.188!
        Me.SUM_030.HyperLink = Nothing
        Me.SUM_030.Left = 6.751003!
        Me.SUM_030.Name = "SUM_030"
        Me.SUM_030.Style = "background-color: Yellow; color: Green; font-size: 8.25pt; font-weight: normal; t" &
    "ext-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_030.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_030.Top = 18.75!
        Me.SUM_030.Width = 0.75!
        '
        'SUM_029
        '
        Me.SUM_029.Height = 0.188!
        Me.SUM_029.HyperLink = Nothing
        Me.SUM_029.Left = 6.751003!
        Me.SUM_029.Name = "SUM_029"
        Me.SUM_029.Style = "background-color: Yellow; color: Green; font-family: Arial; font-size: 8.25pt; fo" &
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_029.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_029.Top = 18.563!
        Me.SUM_029.Width = 0.75!
        '
        'Label3
        '
        Me.Label3.Height = 0.188!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.4980006!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "background-color: Gold; font-size: 8.25pt; font-weight: bold; text-align: right; " &
    "vertical-align: middle"
        Me.Label3.Text = "Beam Web:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label3.Top = 18.562!
        Me.Label3.Width = 1.0!
        '
        'Label27
        '
        Me.Label27.Height = 0.188!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 0.4970006!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "background-color: Gold; font-size: 8.25pt; font-weight: bold; text-align: right; " &
    "vertical-align: middle"
        Me.Label27.Text = "SP Fillet Weld:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label27.Top = 19.12399!
        Me.Label27.Width = 1.0!
        '
        'Label24
        '
        Me.Label24.Height = 0.188!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 0.4980006!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "background-color: Gold; font-size: 8.25pt; font-weight: bold; text-align: right; " &
    "vertical-align: middle"
        Me.Label24.Text = "Shear Plate Bolt:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label24.Top = 18.93599!
        Me.Label24.Width = 1.0!
        '
        'Label14
        '
        Me.Label14.Height = 0.188!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.4980006!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "background-color: Gold; font-size: 8.25pt; font-weight: bold; text-align: right; " &
    "vertical-align: middle"
        Me.Label14.Text = "Shear Plate:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label14.Top = 18.74899!
        Me.Label14.Width = 1.0!
        '
        'SUM_025
        '
        Me.SUM_025.Height = 0.188!
        Me.SUM_025.HyperLink = Nothing
        Me.SUM_025.Left = 6.005002!
        Me.SUM_025.Name = "SUM_025"
        Me.SUM_025.Style = "background-color: Honeydew; color: Green; font-family: Arial; font-size: 8.25pt; " &
    "font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 0" &
    ""
        Me.SUM_025.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_025.Top = 18.56299!
        Me.SUM_025.Width = 0.75!
        '
        'SUM_026
        '
        Me.SUM_026.Height = 0.188!
        Me.SUM_026.HyperLink = Nothing
        Me.SUM_026.Left = 6.005002!
        Me.SUM_026.Name = "SUM_026"
        Me.SUM_026.Style = "background-color: Honeydew; color: Green; font-size: 8.25pt; font-weight: normal;" &
    " text-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_026.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_026.Top = 18.74999!
        Me.SUM_026.Width = 0.75!
        '
        'SUM_027
        '
        Me.SUM_027.Height = 0.188!
        Me.SUM_027.HyperLink = Nothing
        Me.SUM_027.Left = 6.005002!
        Me.SUM_027.Name = "SUM_027"
        Me.SUM_027.Style = "background-color: Honeydew; color: Green; font-size: 8.25pt; font-weight: normal;" &
    " text-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_027.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_027.Top = 18.93699!
        Me.SUM_027.Width = 0.75!
        '
        'SUM_028
        '
        Me.SUM_028.Height = 0.188!
        Me.SUM_028.HyperLink = Nothing
        Me.SUM_028.Left = 6.004002!
        Me.SUM_028.Name = "SUM_028"
        Me.SUM_028.Style = "background-color: Honeydew; color: Green; font-size: 8.25pt; font-weight: normal;" &
    " text-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_028.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_028.Top = 19.12499!
        Me.SUM_028.Width = 0.75!
        '
        'SUM_024
        '
        Me.SUM_024.Height = 0.188!
        Me.SUM_024.HyperLink = Nothing
        Me.SUM_024.Left = 5.253003!
        Me.SUM_024.Name = "SUM_024"
        Me.SUM_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_024.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_024.Top = 19.12499!
        Me.SUM_024.Width = 0.75!
        '
        'SUM_023
        '
        Me.SUM_023.Height = 0.188!
        Me.SUM_023.HyperLink = Nothing
        Me.SUM_023.Left = 5.252002!
        Me.SUM_023.Name = "SUM_023"
        Me.SUM_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_023.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_023.Top = 18.93699!
        Me.SUM_023.Width = 0.75!
        '
        'SUM_022
        '
        Me.SUM_022.Height = 0.188!
        Me.SUM_022.HyperLink = Nothing
        Me.SUM_022.Left = 5.252002!
        Me.SUM_022.Name = "SUM_022"
        Me.SUM_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_022.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_022.Top = 18.74999!
        Me.SUM_022.Width = 0.75!
        '
        'SUM_021
        '
        Me.SUM_021.Height = 0.188!
        Me.SUM_021.HyperLink = Nothing
        Me.SUM_021.Left = 5.252002!
        Me.SUM_021.Name = "SUM_021"
        Me.SUM_021.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_021.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_021.Top = 18.56299!
        Me.SUM_021.Width = 0.75!
        '
        'SUM_017
        '
        Me.SUM_017.Height = 0.188!
        Me.SUM_017.HyperLink = Nothing
        Me.SUM_017.Left = 4.503003!
        Me.SUM_017.Name = "SUM_017"
        Me.SUM_017.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_017.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_017.Top = 18.56299!
        Me.SUM_017.Width = 0.75!
        '
        'SUM_018
        '
        Me.SUM_018.Height = 0.188!
        Me.SUM_018.HyperLink = Nothing
        Me.SUM_018.Left = 4.503003!
        Me.SUM_018.Name = "SUM_018"
        Me.SUM_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_018.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_018.Top = 18.74999!
        Me.SUM_018.Width = 0.75!
        '
        'SUM_019
        '
        Me.SUM_019.Height = 0.188!
        Me.SUM_019.HyperLink = Nothing
        Me.SUM_019.Left = 4.503003!
        Me.SUM_019.Name = "SUM_019"
        Me.SUM_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_019.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_019.Top = 18.93699!
        Me.SUM_019.Width = 0.75!
        '
        'SUM_020
        '
        Me.SUM_020.Height = 0.188!
        Me.SUM_020.HyperLink = Nothing
        Me.SUM_020.Left = 4.502003!
        Me.SUM_020.Name = "SUM_020"
        Me.SUM_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_020.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_020.Top = 19.12499!
        Me.SUM_020.Width = 0.75!
        '
        'SUM_014
        '
        Me.SUM_014.Height = 0.188!
        Me.SUM_014.HyperLink = Nothing
        Me.SUM_014.Left = 3.752002!
        Me.SUM_014.Name = "SUM_014"
        Me.SUM_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_014.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_014.Top = 18.74899!
        Me.SUM_014.Width = 0.75!
        '
        'SUM_015
        '
        Me.SUM_015.Height = 0.188!
        Me.SUM_015.HyperLink = Nothing
        Me.SUM_015.Left = 3.752002!
        Me.SUM_015.Name = "SUM_015"
        Me.SUM_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_015.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_015.Top = 18.93599!
        Me.SUM_015.Width = 0.75!
        '
        'SUM_011
        '
        Me.SUM_011.Height = 0.188!
        Me.SUM_011.HyperLink = Nothing
        Me.SUM_011.Left = 3.001002!
        Me.SUM_011.Name = "SUM_011"
        Me.SUM_011.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_011.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_011.Top = 18.93599!
        Me.SUM_011.Width = 0.7499998!
        '
        'SUM_007
        '
        Me.SUM_007.Height = 0.188!
        Me.SUM_007.HyperLink = Nothing
        Me.SUM_007.Left = 2.250002!
        Me.SUM_007.Name = "SUM_007"
        Me.SUM_007.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_007.Text = "q2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_007.Top = 18.93599!
        Me.SUM_007.Width = 0.75!
        '
        'SUM_003
        '
        Me.SUM_003.Height = 0.188!
        Me.SUM_003.HyperLink = Nothing
        Me.SUM_003.Left = 1.499002!
        Me.SUM_003.Name = "SUM_003"
        Me.SUM_003.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_003.Text = "q1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_003.Top = 18.93599!
        Me.SUM_003.Width = 0.75!
        '
        'SUM_016
        '
        Me.SUM_016.Height = 0.188!
        Me.SUM_016.HyperLink = Nothing
        Me.SUM_016.Left = 3.751002!
        Me.SUM_016.Name = "SUM_016"
        Me.SUM_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_016.Text = "1.571" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_016.Top = 19.12399!
        Me.SUM_016.Width = 0.75!
        '
        'SUM_012
        '
        Me.SUM_012.Height = 0.188!
        Me.SUM_012.HyperLink = Nothing
        Me.SUM_012.Left = 3.000002!
        Me.SUM_012.Name = "SUM_012"
        Me.SUM_012.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_012.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_012.Top = 19.12399!
        Me.SUM_012.Width = 0.7500002!
        '
        'SUM_008
        '
        Me.SUM_008.Height = 0.188!
        Me.SUM_008.HyperLink = Nothing
        Me.SUM_008.Left = 2.249002!
        Me.SUM_008.Name = "SUM_008"
        Me.SUM_008.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_008.Text = "q2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_008.Top = 19.12399!
        Me.SUM_008.Width = 0.75!
        '
        'SUM_004
        '
        Me.SUM_004.Height = 0.188!
        Me.SUM_004.HyperLink = Nothing
        Me.SUM_004.Left = 1.498002!
        Me.SUM_004.Name = "SUM_004"
        Me.SUM_004.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_004.Text = "q1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_004.Top = 19.12399!
        Me.SUM_004.Width = 0.75!
        '
        'Label21
        '
        Me.Label21.Height = 0.188!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0.4980006!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label21.Text = "Component" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label21.Top = 18.37499!
        Me.Label21.Width = 1.0!
        '
        'Label613
        '
        Me.Label613.Height = 0.1875!
        Me.Label613.HyperLink = Nothing
        Me.Label613.Left = 0.25!
        Me.Label613.Name = "Label613"
        Me.Label613.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label613.Text = "CASE 3: COMBINED AXIAL AND VERTICAL REACTIONS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label613.Top = 0!
        Me.Label613.Width = 7.75!
        '
        'Picture8
        '
        Me.Picture8.Height = 5.125!
        Me.Picture8.HyperLink = Nothing
        Me.Picture8.ImageData = CType(resources.GetObject("Picture8.ImageData"), System.IO.Stream)
        Me.Picture8.Left = 0!
        Me.Picture8.Name = "Picture8"
        Me.Picture8.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture8.Top = 0.187!
        Me.Picture8.Width = 8.0!
        '
        'Label614
        '
        Me.Label614.Height = 0.1875!
        Me.Label614.HyperLink = Nothing
        Me.Label614.Left = 1.0!
        Me.Label614.Name = "Label614"
        Me.Label614.Style = "font-size: 8.25pt; font-style: normal; font-weight: bold; text-align: left; verti" &
    "cal-align: middle; ddo-char-set: 0"
        Me.Label614.Text = "Method 1: N and V circular interaction" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label614.Top = 5.187967!
        Me.Label614.Width = 7.0!
        '
        'Label615
        '
        Me.Label615.Height = 0.1875!
        Me.Label615.HyperLink = Nothing
        Me.Label615.Left = 0!
        Me.Label615.Name = "Label615"
        Me.Label615.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label615.Text = "SUM_bmWebθ_BS1 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label615.Top = 5.375!
        Me.Label615.Width = 2.187!
        '
        'Label616
        '
        Me.Label616.Height = 0.1875!
        Me.Label616.HyperLink = Nothing
        Me.Label616.Left = 3.75!
        Me.Label616.Name = "Label616"
        Me.Label616.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label616.Text = "= SUM_bmWebHorz_BS2 + SUM_bmWebVert_BS2"
        Me.Label616.Top = 5.375!
        Me.Label616.Width = 4.25!
        '
        'Label618
        '
        Me.Label618.Height = 0.1875!
        Me.Label618.HyperLink = Nothing
        Me.Label618.Left = 1.0!
        Me.Label618.Name = "Label618"
        Me.Label618.Style = "font-size: 8.25pt; font-style: normal; font-weight: bold; text-align: left; verti" &
    "cal-align: middle; ddo-char-set: 0"
        Me.Label618.Text = "Method 2: Failure along hole edges" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label618.Top = 5.688001!
        Me.Label618.Width = 7.0!
        '
        'Label619
        '
        Me.Label619.Height = 0.1875!
        Me.Label619.HyperLink = Nothing
        Me.Label619.Left = 0!
        Me.Label619.Name = "Label619"
        Me.Label619.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label619.Text = "θ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label619.Top = 6.064071!
        Me.Label619.Width = 2.25!
        '
        'Label620
        '
        Me.Label620.Height = 0.1875!
        Me.Label620.HyperLink = Nothing
        Me.Label620.Left = 3.25!
        Me.Label620.Name = "Label620"
        Me.Label620.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label620.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label620.Top = 6.064071!
        Me.Label620.Width = 4.75!
        '
        'S49_C3_003
        '
        Me.S49_C3_003.Height = 0.1875!
        Me.S49_C3_003.HyperLink = Nothing
        Me.S49_C3_003.Left = 2.25!
        Me.S49_C3_003.Name = "S49_C3_003"
        Me.S49_C3_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C3_003.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_003.Top = 6.065112!
        Me.S49_C3_003.Width = 1.0!
        '
        'Label622
        '
        Me.Label622.Height = 0.1875!
        Me.Label622.HyperLink = Nothing
        Me.Label622.Left = 0!
        Me.Label622.Name = "Label622"
        Me.Label622.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label622.Text = "Lb_edge=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label622.Top = 6.25307!
        Me.Label622.Width = 2.25!
        '
        'Label623
        '
        Me.Label623.Height = 0.1875!
        Me.Label623.HyperLink = Nothing
        Me.Label623.Left = 3.25!
        Me.Label623.Name = "Label623"
        Me.Label623.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label623.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label623.Top = 6.25307!
        Me.Label623.Width = 4.75!
        '
        'S49_C3_004
        '
        Me.S49_C3_004.Height = 0.1875!
        Me.S49_C3_004.HyperLink = Nothing
        Me.S49_C3_004.Left = 2.25!
        Me.S49_C3_004.Name = "S49_C3_004"
        Me.S49_C3_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C3_004.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_004.Top = 6.25405!
        Me.S49_C3_004.Width = 1.0!
        '
        'Label625
        '
        Me.Label625.Height = 0.1875!
        Me.Label625.HyperLink = Nothing
        Me.Label625.Left = 0!
        Me.Label625.Name = "Label625"
        Me.Label625.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label625.Text = "Lbw_diag=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label625.Top = 6.442035!
        Me.Label625.Width = 2.25!
        '
        'Label626
        '
        Me.Label626.Height = 0.1875!
        Me.Label626.HyperLink = Nothing
        Me.Label626.Left = 3.25!
        Me.Label626.Name = "Label626"
        Me.Label626.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label626.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "min( sqrt(Svert2+Shorz2) + Lb_edge, (Lb_edge + Shorz)/cosq)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label626.Top = 6.442035!
        Me.Label626.Width = 4.75!
        '
        'S49_C3_005
        '
        Me.S49_C3_005.Height = 0.1875!
        Me.S49_C3_005.HyperLink = Nothing
        Me.S49_C3_005.Left = 2.25!
        Me.S49_C3_005.Name = "S49_C3_005"
        Me.S49_C3_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_005.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_005.Top = 6.443076!
        Me.S49_C3_005.Width = 1.0!
        '
        'Label628
        '
        Me.Label628.Height = 0.1875!
        Me.Label628.HyperLink = Nothing
        Me.Label628.Left = 0!
        Me.Label628.Name = "Label628"
        Me.Label628.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label628.Text = "Lvg_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label628.Top = 6.630973!
        Me.Label628.Width = 2.25!
        '
        'Label629
        '
        Me.Label629.Height = 0.1875!
        Me.Label629.HyperLink = Nothing
        Me.Label629.Left = 3.25!
        Me.Label629.Name = "Label629"
        Me.Label629.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label629.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2*Lb_edge + (n_Hbolt_SST - 1)*Shorz + sqrt(Svert^2 + ((n_Hbolt_SST - 1)*Shor" &
    "z))^2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label629.Top = 6.630973!
        Me.Label629.Width = 6.186999!
        '
        'S49_C3_006
        '
        Me.S49_C3_006.Height = 0.1875!
        Me.S49_C3_006.HyperLink = Nothing
        Me.S49_C3_006.Left = 2.25!
        Me.S49_C3_006.Name = "S49_C3_006"
        Me.S49_C3_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_006.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_006.Top = 6.632077!
        Me.S49_C3_006.Width = 1.0!
        '
        'Label631
        '
        Me.Label631.Height = 0.1875!
        Me.Label631.HyperLink = Nothing
        Me.Label631.Left = 0!
        Me.Label631.Name = "Label631"
        Me.Label631.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label631.Text = "Lvn_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label631.Top = 6.818959!
        Me.Label631.Width = 2.25!
        '
        'Label632
        '
        Me.Label632.Height = 0.1875!
        Me.Label632.HyperLink = Nothing
        Me.Label632.Left = 3.25!
        Me.Label632.Name = "Label632"
        Me.Label632.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label632.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_web - 2*dHole_sp - (n_Hbolt - 1)*dHole_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label632.Top = 6.818959!
        Me.Label632.Width = 4.75!
        '
        'S49_C3_007
        '
        Me.S49_C3_007.Height = 0.1875!
        Me.S49_C3_007.HyperLink = Nothing
        Me.S49_C3_007.Left = 2.25!
        Me.S49_C3_007.Name = "S49_C3_007"
        Me.S49_C3_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_007.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_007.Top = 6.820071!
        Me.S49_C3_007.Width = 1.0!
        '
        'Label634
        '
        Me.Label634.Height = 0.1875!
        Me.Label634.HyperLink = Nothing
        Me.Label634.Left = 0!
        Me.Label634.Name = "Label634"
        Me.Label634.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label634.Text = "Ltg_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label634.Top = 7.007848!
        Me.Label634.Width = 2.25!
        '
        'Label635
        '
        Me.Label635.Height = 0.1875!
        Me.Label635.HyperLink = Nothing
        Me.Label635.Left = 3.25!
        Me.Label635.Name = "Label635"
        Me.Label635.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label635.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (n_Vbolt_SST - 2)*Svert" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label635.Top = 7.007848!
        Me.Label635.Width = 4.75!
        '
        'S49_C3_008
        '
        Me.S49_C3_008.Height = 0.1875!
        Me.S49_C3_008.HyperLink = Nothing
        Me.S49_C3_008.Left = 2.25!
        Me.S49_C3_008.Name = "S49_C3_008"
        Me.S49_C3_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_008.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_008.Top = 7.00896!
        Me.S49_C3_008.Width = 1.0!
        '
        'Label637
        '
        Me.Label637.Height = 0.1875!
        Me.Label637.HyperLink = Nothing
        Me.Label637.Left = 0!
        Me.Label637.Name = "Label637"
        Me.Label637.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label637.Text = "Ltn_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label637.Top = 7.196828!
        Me.Label637.Width = 2.25!
        '
        'Label638
        '
        Me.Label638.Height = 0.1875!
        Me.Label638.HyperLink = Nothing
        Me.Label638.Left = 3.25!
        Me.Label638.Name = "Label638"
        Me.Label638.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label638.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ltg_web - (n_Vbolt_SST - 2)*dHole_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label638.Top = 7.196828!
        Me.Label638.Width = 4.75!
        '
        'S49_C3_009
        '
        Me.S49_C3_009.Height = 0.1875!
        Me.S49_C3_009.HyperLink = Nothing
        Me.S49_C3_009.Left = 2.25!
        Me.S49_C3_009.Name = "S49_C3_009"
        Me.S49_C3_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_009.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_009.Top = 7.197925!
        Me.S49_C3_009.Width = 1.0!
        '
        'Label640
        '
        Me.Label640.Height = 0.1875!
        Me.Label640.HyperLink = Nothing
        Me.Label640.Left = 0!
        Me.Label640.Name = "Label640"
        Me.Label640.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label640.Text = "Agv_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label640.Top = 7.385869!
        Me.Label640.Width = 2.25!
        '
        'Label641
        '
        Me.Label641.Height = 0.1875!
        Me.Label641.HyperLink = Nothing
        Me.Label641.Left = 3.25!
        Me.Label641.Name = "Label641"
        Me.Label641.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label641.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_web* tbw" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label641.Top = 7.385869!
        Me.Label641.Width = 4.75!
        '
        'S49_C3_010
        '
        Me.S49_C3_010.Height = 0.1875!
        Me.S49_C3_010.HyperLink = Nothing
        Me.S49_C3_010.Left = 2.25!
        Me.S49_C3_010.Name = "S49_C3_010"
        Me.S49_C3_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_010.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_010.Top = 7.386973!
        Me.S49_C3_010.Width = 1.0!
        '
        'Label643
        '
        Me.Label643.Height = 0.1875!
        Me.Label643.HyperLink = Nothing
        Me.Label643.Left = 0!
        Me.Label643.Name = "Label643"
        Me.Label643.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label643.Text = "Anv_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label643.Top = 7.572959!
        Me.Label643.Width = 2.25!
        '
        'Label644
        '
        Me.Label644.Height = 0.1875!
        Me.Label644.HyperLink = Nothing
        Me.Label644.Left = 3.25!
        Me.Label644.Name = "Label644"
        Me.Label644.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label644.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvn_web* tbw" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label644.Top = 7.572959!
        Me.Label644.Width = 4.75!
        '
        'S49_C3_011
        '
        Me.S49_C3_011.Height = 0.1875!
        Me.S49_C3_011.HyperLink = Nothing
        Me.S49_C3_011.Left = 2.25!
        Me.S49_C3_011.Name = "S49_C3_011"
        Me.S49_C3_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_011.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_011.Top = 7.574069!
        Me.S49_C3_011.Width = 1.0!
        '
        'Label646
        '
        Me.Label646.Height = 0.1875!
        Me.Label646.HyperLink = Nothing
        Me.Label646.Left = 0!
        Me.Label646.Name = "Label646"
        Me.Label646.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label646.Text = "Agt_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label646.Top = 7.76187!
        Me.Label646.Width = 2.25!
        '
        'Label647
        '
        Me.Label647.Height = 0.1875!
        Me.Label647.HyperLink = Nothing
        Me.Label647.Left = 3.25!
        Me.Label647.Name = "Label647"
        Me.Label647.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label647.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ltg_web* tbw" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label647.Top = 7.76187!
        Me.Label647.Width = 4.75!
        '
        'S49_C3_012
        '
        Me.S49_C3_012.Height = 0.1875!
        Me.S49_C3_012.HyperLink = Nothing
        Me.S49_C3_012.Left = 2.25!
        Me.S49_C3_012.Name = "S49_C3_012"
        Me.S49_C3_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_012.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_012.Top = 7.762967!
        Me.S49_C3_012.Width = 1.0!
        '
        'Label649
        '
        Me.Label649.Height = 0.1875!
        Me.Label649.HyperLink = Nothing
        Me.Label649.Left = 0!
        Me.Label649.Name = "Label649"
        Me.Label649.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label649.Text = "Ant_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label649.Top = 7.95087!
        Me.Label649.Width = 2.25!
        '
        'Label650
        '
        Me.Label650.Height = 0.1875!
        Me.Label650.HyperLink = Nothing
        Me.Label650.Left = 3.25!
        Me.Label650.Name = "Label650"
        Me.Label650.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label650.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ltn_web* tbw" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label650.Top = 7.95087!
        Me.Label650.Width = 4.75!
        '
        'S49_C3_013
        '
        Me.S49_C3_013.Height = 0.1875!
        Me.S49_C3_013.HyperLink = Nothing
        Me.S49_C3_013.Left = 2.25!
        Me.S49_C3_013.Name = "S49_C3_013"
        Me.S49_C3_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_013.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_013.Top = 7.951966!
        Me.S49_C3_013.Width = 1.0!
        '
        'Label652
        '
        Me.Label652.Height = 0.1875!
        Me.Label652.HyperLink = Nothing
        Me.Label652.Left = 0!
        Me.Label652.Name = "Label652"
        Me.Label652.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label652.Text = "Rn_bmWeb_BS1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label652.Top = 8.136972!
        Me.Label652.Width = 2.25!
        '
        'Label653
        '
        Me.Label653.Height = 0.1875!
        Me.Label653.HyperLink = Nothing
        Me.Label653.Left = 3.25!
        Me.Label653.Name = "Label653"
        Me.Label653.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label653.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_bmWeb* Agv_web + Ubs* Fu_bmWeb* Ant_web" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label653.Top = 8.136972!
        Me.Label653.Width = 4.75!
        '
        'S49_C3_014
        '
        Me.S49_C3_014.Height = 0.1875!
        Me.S49_C3_014.HyperLink = Nothing
        Me.S49_C3_014.Left = 2.25!
        Me.S49_C3_014.Name = "S49_C3_014"
        Me.S49_C3_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_014.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_014.Top = 8.138069!
        Me.S49_C3_014.Width = 1.0!
        '
        'Label655
        '
        Me.Label655.Height = 0.1875!
        Me.Label655.HyperLink = Nothing
        Me.Label655.Left = 0!
        Me.Label655.Name = "Label655"
        Me.Label655.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label655.Text = "Rn_bmWeb_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label655.Top = 8.324831!
        Me.Label655.Width = 2.25!
        '
        'Label656
        '
        Me.Label656.Height = 0.1875!
        Me.Label656.HyperLink = Nothing
        Me.Label656.Left = 3.25!
        Me.Label656.Name = "Label656"
        Me.Label656.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label656.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_bmWeb* Agv_web + Ubs* Fu_bmWeb* Ant_web" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label656.Top = 8.324831!
        Me.Label656.Width = 4.75!
        '
        'S49_C3_015
        '
        Me.S49_C3_015.Height = 0.1875!
        Me.S49_C3_015.HyperLink = Nothing
        Me.S49_C3_015.Left = 2.25!
        Me.S49_C3_015.Name = "S49_C3_015"
        Me.S49_C3_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_015.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_015.Top = 8.325941!
        Me.S49_C3_015.Width = 1.0!
        '
        'Label658
        '
        Me.Label658.Height = 0.1875!
        Me.Label658.HyperLink = Nothing
        Me.Label658.Left = 0!
        Me.Label658.Name = "Label658"
        Me.Label658.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label658.Text = "ΦRn_bmWeb_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label658.Top = 8.51296!
        Me.Label658.Width = 2.25!
        '
        'Label659
        '
        Me.Label659.Height = 0.1875!
        Me.Label659.HyperLink = Nothing
        Me.Label659.Left = 3.25!
        Me.Label659.Name = "Label659"
        Me.Label659.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label659.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_bmWeb_BS1, Rn_bmWeb_BS2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label659.Top = 8.51296!
        Me.Label659.Width = 4.75!
        '
        'S49_C3_016
        '
        Me.S49_C3_016.Height = 0.1875!
        Me.S49_C3_016.HyperLink = Nothing
        Me.S49_C3_016.Left = 2.25!
        Me.S49_C3_016.Name = "S49_C3_016"
        Me.S49_C3_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_016.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_016.Top = 8.514071!
        Me.S49_C3_016.Width = 1.0!
        '
        'Label661
        '
        Me.Label661.Height = 0.1875!
        Me.Label661.HyperLink = Nothing
        Me.Label661.Left = 0!
        Me.Label661.Name = "Label661"
        Me.Label661.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label661.Text = "SUM_bmWebθ_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label661.Top = 8.70183!
        Me.Label661.Width = 2.25!
        '
        'Label662
        '
        Me.Label662.Height = 0.1875!
        Me.Label662.HyperLink = Nothing
        Me.Label662.Left = 3.25!
        Me.Label662.Name = "Label662"
        Me.Label662.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label662.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2*Pr/ ΦRn_bmWeb_BS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label662.Top = 8.70183!
        Me.Label662.Width = 4.75!
        '
        'S49_C3_017
        '
        Me.S49_C3_017.Height = 0.1875!
        Me.S49_C3_017.HyperLink = Nothing
        Me.S49_C3_017.Left = 2.25!
        Me.S49_C3_017.Name = "S49_C3_017"
        Me.S49_C3_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_017.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_017.Top = 8.70293!
        Me.S49_C3_017.Width = 1.0!
        '
        'Label669
        '
        Me.Label669.Height = 0.1875!
        Me.Label669.HyperLink = Nothing
        Me.Label669.Left = 0.5000001!
        Me.Label669.Name = "Label669"
        Me.Label669.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label669.Text = "BEAM WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label669.Top = 5.0!
        Me.Label669.Width = 7.75!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "Pr_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 5.876001!
        Me.Label1.Width = 2.25!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 3.25!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label2.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=SQRT((Pu_sp^2)+(Vu_bm)^2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 5.876001!
        Me.Label2.Width = 4.75!
        '
        'S49_C3_002
        '
        Me.S49_C3_002.Height = 0.1875!
        Me.S49_C3_002.HyperLink = Nothing
        Me.S49_C3_002.Left = 2.25!
        Me.S49_C3_002.Name = "S49_C3_002"
        Me.S49_C3_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_002.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_002.Top = 5.877043!
        Me.S49_C3_002.Width = 1.0!
        '
        'Label664
        '
        Me.Label664.Height = 0.1875!
        Me.Label664.HyperLink = Nothing
        Me.Label664.Left = 0!
        Me.Label664.Name = "Label664"
        Me.Label664.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label664.Text = "SUM_bmWebq_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label664.Top = 9.062759!
        Me.Label664.Width = 2.25!
        '
        'Label665
        '
        Me.Label665.Height = 0.1875!
        Me.Label665.HyperLink = Nothing
        Me.Label665.Left = 3.25!
        Me.Label665.Name = "Label665"
        Me.Label665.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label665.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= max(SUM_bmWebθ_BS1, SUM_bmWebθ_BS2)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label665.Top = 9.062759!
        Me.Label665.Width = 4.75!
        '
        'S49_C3_018
        '
        Me.S49_C3_018.Height = 0.1875!
        Me.S49_C3_018.HyperLink = Nothing
        Me.S49_C3_018.Left = 2.25!
        Me.S49_C3_018.Name = "S49_C3_018"
        Me.S49_C3_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_018.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_018.Top = 9.06386!
        Me.S49_C3_018.Width = 1.0!
        '
        'Label667
        '
        Me.Label667.Height = 0.1875!
        Me.Label667.HyperLink = Nothing
        Me.Label667.Left = 0!
        Me.Label667.Name = "Label667"
        Me.Label667.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label667.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label667.Top = 9.247921!
        Me.Label667.Width = 2.25!
        '
        'Label668
        '
        Me.Label668.Height = 0.1875!
        Me.Label668.HyperLink = Nothing
        Me.Label668.Left = 3.25!
        Me.Label668.Name = "Label668"
        Me.Label668.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label668.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= OK if SUM_bmWebθ_BS <= SUM_bmWebθ_BS_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label668.Top = 9.247921!
        Me.Label668.Width = 4.75!
        '
        'Label682
        '
        Me.Label682.Height = 0.1875!
        Me.Label682.HyperLink = Nothing
        Me.Label682.Left = 0.312!
        Me.Label682.Name = "Label682"
        Me.Label682.Style = "font-size: 8.25pt; font-style: normal; font-weight: bold; text-align: left; text-" &
    "decoration: none; vertical-align: middle; ddo-char-set: 0"
        Me.Label682.Text = "SHEAR PLATE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label682.Top = 9.438!
        Me.Label682.Width = 7.75!
        '
        'Label671
        '
        Me.Label671.Height = 0.1875!
        Me.Label671.HyperLink = Nothing
        Me.Label671.Left = 1.0!
        Me.Label671.Name = "Label671"
        Me.Label671.Style = "font-size: 8.25pt; font-style: normal; font-weight: bold; text-align: left; verti" &
    "cal-align: middle; ddo-char-set: 0"
        Me.Label671.Text = "Method 1: N and V circular interaction" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label671.Top = 9.625!
        Me.Label671.Width = 8.0!
        '
        'Label672
        '
        Me.Label672.Height = 0.1875!
        Me.Label672.HyperLink = Nothing
        Me.Label672.Left = 0!
        Me.Label672.Name = "Label672"
        Me.Label672.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label672.Text = "SUM_spθ_BS1 = " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label672.Top = 9.875!
        Me.Label672.Width = 2.187!
        '
        'Label673
        '
        Me.Label673.Height = 0.1875!
        Me.Label673.HyperLink = Nothing
        Me.Label673.Left = 3.75!
        Me.Label673.Name = "Label673"
        Me.Label673.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label673.Text = "= SUM_spVert_BS^2 + SUM_spHorz_BS^2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label673.Top = 9.875!
        Me.Label673.Width = 4.25!
        '
        'Label675
        '
        Me.Label675.Height = 0.1875!
        Me.Label675.HyperLink = Nothing
        Me.Label675.Left = 1.0!
        Me.Label675.Name = "Label675"
        Me.Label675.Style = "font-size: 8.25pt; font-style: normal; font-weight: bold; text-align: left; verti" &
    "cal-align: middle; ddo-char-set: 0"
        Me.Label675.Text = "Method 2: Failure along hole edges" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label675.Top = 10.25!
        Me.Label675.Width = 8.0!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label12.Text = "Lvg_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label12.Top = 10.75!
        Me.Label12.Width = 2.25!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 3.25!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label13.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= h_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label13.Top = 10.75!
        Me.Label13.Width = 4.75!
        '
        'S49_C3_021
        '
        Me.S49_C3_021.Height = 0.1875!
        Me.S49_C3_021.HyperLink = Nothing
        Me.S49_C3_021.Left = 2.25!
        Me.S49_C3_021.Name = "S49_C3_021"
        Me.S49_C3_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_021.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_021.Top = 10.75097!
        Me.S49_C3_021.Width = 1.0!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 0!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label46.Text = "CASE 1:"
        Me.Label46.Top = 10.56203!
        Me.Label46.Width = 2.25!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label4.Text = "Lvg_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 10.938!
        Me.Label4.Width = 2.25!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 3.25!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label5.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_sp1 - (n_Vbolts*dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.Top = 10.938!
        Me.Label5.Width = 4.75!
        '
        'S49_C3_022
        '
        Me.S49_C3_022.Height = 0.1875!
        Me.S49_C3_022.HyperLink = Nothing
        Me.S49_C3_022.Left = 2.25!
        Me.S49_C3_022.Name = "S49_C3_022"
        Me.S49_C3_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_022.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_022.Top = 10.93897!
        Me.S49_C3_022.Width = 1.0!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "Ltn_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 11.125!
        Me.Label7.Width = 2.25!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 3.25!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label8.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label8.Top = 11.125!
        Me.Label8.Width = 4.75!
        '
        'S49_C3_023
        '
        Me.S49_C3_023.Height = 0.1875!
        Me.S49_C3_023.HyperLink = Nothing
        Me.S49_C3_023.Left = 2.25!
        Me.S49_C3_023.Name = "S49_C3_023"
        Me.S49_C3_023.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_023.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_023.Top = 11.12597!
        Me.S49_C3_023.Width = 1.0!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label10.Text = "Agv_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 11.31203!
        Me.Label10.Width = 2.25!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 3.25!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label11.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label11.Top = 11.31203!
        Me.Label11.Width = 4.75!
        '
        'S49_C3_024
        '
        Me.S49_C3_024.Height = 0.1875!
        Me.S49_C3_024.HyperLink = Nothing
        Me.S49_C3_024.Left = 2.25!
        Me.S49_C3_024.Name = "S49_C3_024"
        Me.S49_C3_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_024.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_024.Top = 11.313!
        Me.S49_C3_024.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "Anv_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label16.Top = 11.50003!
        Me.Label16.Width = 2.25!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 3.25!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label17.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvn_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label17.Top = 11.50003!
        Me.Label17.Width = 4.75!
        '
        'S49_C3_025
        '
        Me.S49_C3_025.Height = 0.1875!
        Me.S49_C3_025.HyperLink = Nothing
        Me.S49_C3_025.Left = 2.25!
        Me.S49_C3_025.Name = "S49_C3_025"
        Me.S49_C3_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_025.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_025.Top = 11.501!
        Me.S49_C3_025.Width = 1.0!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 0!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label19.Text = "Ant_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label19.Top = 11.68803!
        Me.Label19.Width = 2.25!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 3.25!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label20.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ltn_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label20.Top = 11.68803!
        Me.Label20.Width = 4.75!
        '
        'S49_C3_026
        '
        Me.S49_C3_026.Height = 0.1875!
        Me.S49_C3_026.HyperLink = Nothing
        Me.S49_C3_026.Left = 2.25!
        Me.S49_C3_026.Name = "S49_C3_026"
        Me.S49_C3_026.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_026.Text = "0.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_026.Top = 11.689!
        Me.S49_C3_026.Width = 1.0!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 0!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label22.Text = "Rn_sp_BS1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label22.Top = 11.87503!
        Me.Label22.Width = 2.25!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 3.25!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label23.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_sp* Anv_sp1 + Ubs* Fu_sp* Ant_sp1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label23.Top = 11.87503!
        Me.Label23.Width = 4.75!
        '
        'S49_C3_027
        '
        Me.S49_C3_027.Height = 0.1875!
        Me.S49_C3_027.HyperLink = Nothing
        Me.S49_C3_027.Left = 2.25!
        Me.S49_C3_027.Name = "S49_C3_027"
        Me.S49_C3_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_027.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_027.Top = 11.876!
        Me.S49_C3_027.Width = 1.0!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label25.Text = "Rn_sp_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label25.Top = 12.06303!
        Me.Label25.Width = 2.25!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 3.25!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label26.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_sp* Agv_sp1 + Ubs* Fu_sp* Ant_sp1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label26.Top = 12.06303!
        Me.Label26.Width = 4.75!
        '
        'S49_C3_028
        '
        Me.S49_C3_028.Height = 0.1875!
        Me.S49_C3_028.HyperLink = Nothing
        Me.S49_C3_028.Left = 2.25!
        Me.S49_C3_028.Name = "S49_C3_028"
        Me.S49_C3_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_028.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_028.Top = 12.064!
        Me.S49_C3_028.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 0!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label28.Text = "ΦRn_sp_BS1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label28.Top = 12.25103!
        Me.Label28.Width = 2.25!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 3.25!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label29.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_sp_BS1, Rn_sp_BS2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label29.Top = 12.25103!
        Me.Label29.Width = 4.75!
        '
        'S49_C3_029
        '
        Me.S49_C3_029.Height = 0.1875!
        Me.S49_C3_029.HyperLink = Nothing
        Me.S49_C3_029.Left = 2.25!
        Me.S49_C3_029.Name = "S49_C3_029"
        Me.S49_C3_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_029.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_029.Top = 12.252!
        Me.S49_C3_029.Width = 1.0!
        '
        'Label31
        '
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 0!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label31.Text = "SUM_spθ_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label31.Top = 12.43903!
        Me.Label31.Width = 2.25!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 3.25!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label32.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pr_BS / ΦRn_sp_BS1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label32.Top = 12.43903!
        Me.Label32.Width = 4.75!
        '
        'S49_C3_030
        '
        Me.S49_C3_030.Height = 0.1875!
        Me.S49_C3_030.HyperLink = Nothing
        Me.S49_C3_030.Left = 2.25!
        Me.S49_C3_030.Name = "S49_C3_030"
        Me.S49_C3_030.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_030.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_030.Top = 12.44!
        Me.S49_C3_030.Width = 1.0!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 0!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label34.Text = "Pv=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label34.Top = 13.0!
        Me.Label34.Width = 2.25!
        '
        'Label35
        '
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 3.25!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label35.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=Ceiling(n_vBolts/2)/n_vBolts*Vu_bm" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label35.Top = 13.0!
        Me.Label35.Width = 4.75!
        '
        'S49_C3_031
        '
        Me.S49_C3_031.Height = 0.1875!
        Me.S49_C3_031.HyperLink = Nothing
        Me.S49_C3_031.Left = 2.25!
        Me.S49_C3_031.Name = "S49_C3_031"
        Me.S49_C3_031.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_031.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_031.Top = 13.00097!
        Me.S49_C3_031.Width = 1.0!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label37.Text = "CASE 2:"
        Me.Label37.Top = 12.81203!
        Me.Label37.Width = 2.25!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 0!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label38.Text = "Pr_BS1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label38.Top = 13.188!
        Me.Label38.Width = 2.25!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 3.25!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label39.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=SQRT(Pv^2 + Pu_sp^2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label39.Top = 13.188!
        Me.Label39.Width = 4.75!
        '
        'S49_C3_032
        '
        Me.S49_C3_032.Height = 0.1875!
        Me.S49_C3_032.HyperLink = Nothing
        Me.S49_C3_032.Left = 2.25!
        Me.S49_C3_032.Name = "S49_C3_032"
        Me.S49_C3_032.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_032.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_032.Top = 13.18897!
        Me.S49_C3_032.Width = 1.0!
        '
        'Label41
        '
        Me.Label41.Height = 0.1875!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 0!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label41.Text = "Lvg_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label41.Top = 13.375!
        Me.Label41.Width = 2.25!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 3.25!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label42.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= h_sp/2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label42.Top = 13.375!
        Me.Label42.Width = 4.75!
        '
        'S49_C3_033
        '
        Me.S49_C3_033.Height = 0.1875!
        Me.S49_C3_033.HyperLink = Nothing
        Me.S49_C3_033.Left = 2.25!
        Me.S49_C3_033.Name = "S49_C3_033"
        Me.S49_C3_033.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_033.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_033.Top = 13.37597!
        Me.S49_C3_033.Width = 1.0!
        '
        'Label44
        '
        Me.Label44.Height = 0.1875!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 0!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label44.Text = "Lvn_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label44.Top = 13.56203!
        Me.Label44.Width = 2.25!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 3.25!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label45.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_sp2 - (((n_Vbolt_SST-1)/2)*dHole_sp) - (0.5*dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label45.Top = 13.56203!
        Me.Label45.Width = 4.75!
        '
        'S49_C3_034
        '
        Me.S49_C3_034.Height = 0.1875!
        Me.S49_C3_034.HyperLink = Nothing
        Me.S49_C3_034.Left = 2.25!
        Me.S49_C3_034.Name = "S49_C3_034"
        Me.S49_C3_034.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_034.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_034.Top = 13.563!
        Me.S49_C3_034.Width = 1.0!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 0!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label48.Text = "Ltn_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label48.Top = 13.75003!
        Me.Label48.Width = 2.25!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 3.25!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label49.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (W_sp - a) - ((n_Hbolts-1)*dHole_sp) - (0.5*dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label49.Top = 13.75003!
        Me.Label49.Width = 4.75!
        '
        'S49_C3_035
        '
        Me.S49_C3_035.Height = 0.1875!
        Me.S49_C3_035.HyperLink = Nothing
        Me.S49_C3_035.Left = 2.25!
        Me.S49_C3_035.Name = "S49_C3_035"
        Me.S49_C3_035.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_035.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_035.Top = 13.751!
        Me.S49_C3_035.Width = 1.0!
        '
        'Label51
        '
        Me.Label51.Height = 0.1875!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 0!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label51.Text = "Agv_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label51.Top = 13.93803!
        Me.Label51.Width = 2.25!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 3.25!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label52.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label52.Top = 13.93803!
        Me.Label52.Width = 4.75!
        '
        'S49_C3_036
        '
        Me.S49_C3_036.Height = 0.1875!
        Me.S49_C3_036.HyperLink = Nothing
        Me.S49_C3_036.Left = 2.25!
        Me.S49_C3_036.Name = "S49_C3_036"
        Me.S49_C3_036.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_036.Text = "0.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_036.Top = 13.939!
        Me.S49_C3_036.Width = 1.0!
        '
        'Label54
        '
        Me.Label54.Height = 0.1875!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 0!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label54.Text = "Anv_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label54.Top = 14.12503!
        Me.Label54.Width = 2.25!
        '
        'Label55
        '
        Me.Label55.Height = 0.1875!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 3.25!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label55.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvn_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label55.Top = 14.12503!
        Me.Label55.Width = 4.75!
        '
        'S49_C3_037
        '
        Me.S49_C3_037.Height = 0.1875!
        Me.S49_C3_037.HyperLink = Nothing
        Me.S49_C3_037.Left = 2.25!
        Me.S49_C3_037.Name = "S49_C3_037"
        Me.S49_C3_037.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_037.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_037.Top = 14.126!
        Me.S49_C3_037.Width = 1.0!
        '
        'Label57
        '
        Me.Label57.Height = 0.1875!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 0!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label57.Text = "Ant_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label57.Top = 14.31303!
        Me.Label57.Width = 2.25!
        '
        'Label58
        '
        Me.Label58.Height = 0.1875!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 3.25!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label58.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ltn_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label58.Top = 14.31303!
        Me.Label58.Width = 4.75!
        '
        'S49_C3_038
        '
        Me.S49_C3_038.Height = 0.1875!
        Me.S49_C3_038.HyperLink = Nothing
        Me.S49_C3_038.Left = 2.25!
        Me.S49_C3_038.Name = "S49_C3_038"
        Me.S49_C3_038.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_038.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_038.Top = 14.314!
        Me.S49_C3_038.Width = 1.0!
        '
        'Label60
        '
        Me.Label60.Height = 0.1875!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 0!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label60.Text = "Rn_sp_BS3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label60.Top = 14.50103!
        Me.Label60.Width = 2.25!
        '
        'Label61
        '
        Me.Label61.Height = 0.1875!
        Me.Label61.HyperLink = Nothing
        Me.Label61.Left = 3.25!
        Me.Label61.Name = "Label61"
        Me.Label61.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label61.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_sp* Anv_sp2 + Ubs* Fu_sp* Ant_sp2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label61.Top = 14.50103!
        Me.Label61.Width = 4.75!
        '
        'S49_C3_039
        '
        Me.S49_C3_039.Height = 0.1875!
        Me.S49_C3_039.HyperLink = Nothing
        Me.S49_C3_039.Left = 2.25!
        Me.S49_C3_039.Name = "S49_C3_039"
        Me.S49_C3_039.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_039.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_039.Top = 14.502!
        Me.S49_C3_039.Width = 1.0!
        '
        'Label63
        '
        Me.Label63.Height = 0.1875!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 0!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label63.Text = "Rn_sp_BS4=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label63.Top = 14.68903!
        Me.Label63.Width = 2.25!
        '
        'Label64
        '
        Me.Label64.Height = 0.1875!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 3.25!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label64.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_sp* Agv_sp2 + Ubs* Fu_sp* Ant_sp2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label64.Top = 14.68903!
        Me.Label64.Width = 4.75!
        '
        'S49_C3_040
        '
        Me.S49_C3_040.Height = 0.1875!
        Me.S49_C3_040.HyperLink = Nothing
        Me.S49_C3_040.Left = 2.25!
        Me.S49_C3_040.Name = "S49_C3_040"
        Me.S49_C3_040.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_040.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_040.Top = 14.69!
        Me.S49_C3_040.Width = 1.0!
        '
        'Label66
        '
        Me.Label66.Height = 0.1875!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 0!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label66.Text = "ΦRn_sp_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label66.Top = 14.877!
        Me.Label66.Width = 2.25!
        '
        'Label67
        '
        Me.Label67.Height = 0.1875!
        Me.Label67.HyperLink = Nothing
        Me.Label67.Left = 3.25!
        Me.Label67.Name = "Label67"
        Me.Label67.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label67.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_sp_BS1, Rn_sp_BS2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label67.Top = 14.877!
        Me.Label67.Width = 4.75!
        '
        'S49_C3_041
        '
        Me.S49_C3_041.Height = 0.1875!
        Me.S49_C3_041.HyperLink = Nothing
        Me.S49_C3_041.Left = 2.25!
        Me.S49_C3_041.Name = "S49_C3_041"
        Me.S49_C3_041.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_041.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_041.Top = 14.87797!
        Me.S49_C3_041.Width = 1.0!
        '
        'Label69
        '
        Me.Label69.Height = 0.1875!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 0!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label69.Text = "SUM_spθ_BS3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label69.Top = 15.065!
        Me.Label69.Width = 2.25!
        '
        'Label70
        '
        Me.Label70.Height = 0.1875!
        Me.Label70.HyperLink = Nothing
        Me.Label70.Left = 3.25!
        Me.Label70.Name = "Label70"
        Me.Label70.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label70.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pr_BS / ΦRn_sp_BS1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label70.Top = 15.065!
        Me.Label70.Width = 4.75!
        '
        'S49_C3_042
        '
        Me.S49_C3_042.Height = 0.1875!
        Me.S49_C3_042.HyperLink = Nothing
        Me.S49_C3_042.Left = 2.25!
        Me.S49_C3_042.Name = "S49_C3_042"
        Me.S49_C3_042.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_042.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_042.Top = 15.065!
        Me.S49_C3_042.Width = 1.0!
        '
        'Label72
        '
        Me.Label72.Height = 0.1875!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 0!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label72.Text = "Lvg_sp3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label72.Top = 15.625!
        Me.Label72.Width = 2.25!
        '
        'Label73
        '
        Me.Label73.Height = 0.1875!
        Me.Label73.HyperLink = Nothing
        Me.Label73.Left = 3.25!
        Me.Label73.Name = "Label73"
        Me.Label73.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label73.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= W_sp - a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label73.Top = 15.625!
        Me.Label73.Width = 4.75!
        '
        'S49_C3_043
        '
        Me.S49_C3_043.Height = 0.1875!
        Me.S49_C3_043.HyperLink = Nothing
        Me.S49_C3_043.Left = 2.25!
        Me.S49_C3_043.Name = "S49_C3_043"
        Me.S49_C3_043.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_043.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_043.Top = 15.62597!
        Me.S49_C3_043.Width = 1.0!
        '
        'Label75
        '
        Me.Label75.Height = 0.1875!
        Me.Label75.HyperLink = Nothing
        Me.Label75.Left = 0!
        Me.Label75.Name = "Label75"
        Me.Label75.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label75.Text = "CASE 3:"
        Me.Label75.Top = 15.43703!
        Me.Label75.Width = 2.25!
        '
        'Label76
        '
        Me.Label76.Height = 0.1875!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 0!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label76.Text = "Lvn_sp3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label76.Top = 15.813!
        Me.Label76.Width = 2.25!
        '
        'Label77
        '
        Me.Label77.Height = 0.1875!
        Me.Label77.HyperLink = Nothing
        Me.Label77.Left = 3.25!
        Me.Label77.Name = "Label77"
        Me.Label77.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label77.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (W_sp - a) - ((n_Hbolts-0.5)*dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label77.Top = 15.813!
        Me.Label77.Width = 4.75!
        '
        'S49_C3_044
        '
        Me.S49_C3_044.Height = 0.1875!
        Me.S49_C3_044.HyperLink = Nothing
        Me.S49_C3_044.Left = 2.25!
        Me.S49_C3_044.Name = "S49_C3_044"
        Me.S49_C3_044.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_044.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_044.Top = 15.81397!
        Me.S49_C3_044.Width = 1.0!
        '
        'Label79
        '
        Me.Label79.Height = 0.1875!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 0!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label79.Text = "Ltn_sp3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label79.Top = 16.0!
        Me.Label79.Width = 2.25!
        '
        'Label80
        '
        Me.Label80.Height = 0.1875!
        Me.Label80.HyperLink = Nothing
        Me.Label80.Left = 3.25!
        Me.Label80.Name = "Label80"
        Me.Label80.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label80.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_sp2 - (((n_Vbolt_SST-1)/2)*dHole_sp) - (0.5*dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label80.Top = 16.0!
        Me.Label80.Width = 4.75!
        '
        'S49_C3_045
        '
        Me.S49_C3_045.Height = 0.1875!
        Me.S49_C3_045.HyperLink = Nothing
        Me.S49_C3_045.Left = 2.25!
        Me.S49_C3_045.Name = "S49_C3_045"
        Me.S49_C3_045.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_045.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_045.Top = 16.00097!
        Me.S49_C3_045.Width = 1.0!
        '
        'Label82
        '
        Me.Label82.Height = 0.1875!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 0!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label82.Text = "Agv_sp3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label82.Top = 16.18703!
        Me.Label82.Width = 2.25!
        '
        'Label83
        '
        Me.Label83.Height = 0.1875!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 3.25!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label83.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvg_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label83.Top = 16.18703!
        Me.Label83.Width = 4.75!
        '
        'S49_C3_046
        '
        Me.S49_C3_046.Height = 0.1875!
        Me.S49_C3_046.HyperLink = Nothing
        Me.S49_C3_046.Left = 2.25!
        Me.S49_C3_046.Name = "S49_C3_046"
        Me.S49_C3_046.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_046.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_046.Top = 16.188!
        Me.S49_C3_046.Width = 1.0!
        '
        'Label85
        '
        Me.Label85.Height = 0.1875!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 0!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label85.Text = "Anv_sp3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label85.Top = 16.37503!
        Me.Label85.Width = 2.25!
        '
        'Label86
        '
        Me.Label86.Height = 0.1875!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 3.25!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label86.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lvn_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label86.Top = 16.37503!
        Me.Label86.Width = 4.75!
        '
        'S49_C3_047
        '
        Me.S49_C3_047.Height = 0.1875!
        Me.S49_C3_047.HyperLink = Nothing
        Me.S49_C3_047.Left = 2.25!
        Me.S49_C3_047.Name = "S49_C3_047"
        Me.S49_C3_047.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_047.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_047.Top = 16.376!
        Me.S49_C3_047.Width = 1.0!
        '
        'Label88
        '
        Me.Label88.Height = 0.1875!
        Me.Label88.HyperLink = Nothing
        Me.Label88.Left = 0!
        Me.Label88.Name = "Label88"
        Me.Label88.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label88.Text = "Ant_sp3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label88.Top = 16.56303!
        Me.Label88.Width = 2.25!
        '
        'Label89
        '
        Me.Label89.Height = 0.1875!
        Me.Label89.HyperLink = Nothing
        Me.Label89.Left = 3.25!
        Me.Label89.Name = "Label89"
        Me.Label89.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label89.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ltn_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label89.Top = 16.56303!
        Me.Label89.Width = 4.75!
        '
        'S49_C3_048
        '
        Me.S49_C3_048.Height = 0.1875!
        Me.S49_C3_048.HyperLink = Nothing
        Me.S49_C3_048.Left = 2.25!
        Me.S49_C3_048.Name = "S49_C3_048"
        Me.S49_C3_048.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C3_048.Text = "0.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_048.Top = 16.564!
        Me.S49_C3_048.Width = 1.0!
        '
        'Label91
        '
        Me.Label91.Height = 0.1875!
        Me.Label91.HyperLink = Nothing
        Me.Label91.Left = 0!
        Me.Label91.Name = "Label91"
        Me.Label91.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label91.Text = "Rn_sp_BS5=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label91.Top = 16.75003!
        Me.Label91.Width = 2.25!
        '
        'Label92
        '
        Me.Label92.Height = 0.1875!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 3.25!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label92.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_sp* Anv_sp3 + Ubs* Fu_sp* Ant_sp3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label92.Top = 16.75003!
        Me.Label92.Width = 4.75!
        '
        'S49_C3_049
        '
        Me.S49_C3_049.Height = 0.1875!
        Me.S49_C3_049.HyperLink = Nothing
        Me.S49_C3_049.Left = 2.25!
        Me.S49_C3_049.Name = "S49_C3_049"
        Me.S49_C3_049.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_049.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_049.Top = 16.751!
        Me.S49_C3_049.Width = 1.0!
        '
        'Label94
        '
        Me.Label94.Height = 0.1875!
        Me.Label94.HyperLink = Nothing
        Me.Label94.Left = 0!
        Me.Label94.Name = "Label94"
        Me.Label94.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label94.Text = "Rn_sp_BS6=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label94.Top = 16.93803!
        Me.Label94.Width = 2.25!
        '
        'Label95
        '
        Me.Label95.Height = 0.1875!
        Me.Label95.HyperLink = Nothing
        Me.Label95.Left = 3.25!
        Me.Label95.Name = "Label95"
        Me.Label95.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label95.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_sp* Agv_sp3 + Ubs* Fu_sp* Ant_sp3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label95.Top = 16.93803!
        Me.Label95.Width = 4.75!
        '
        'S49_C3_050
        '
        Me.S49_C3_050.Height = 0.1875!
        Me.S49_C3_050.HyperLink = Nothing
        Me.S49_C3_050.Left = 2.25!
        Me.S49_C3_050.Name = "S49_C3_050"
        Me.S49_C3_050.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_050.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_050.Top = 16.939!
        Me.S49_C3_050.Width = 1.0!
        '
        'Label97
        '
        Me.Label97.Height = 0.1875!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 0!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label97.Text = "ΦRn_sp_BS3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label97.Top = 17.12603!
        Me.Label97.Width = 2.25!
        '
        'Label98
        '
        Me.Label98.Height = 0.1875!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 3.25!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label98.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_sp_BS5, Rn_sp_BS6)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label98.Top = 17.12603!
        Me.Label98.Width = 4.75!
        '
        'S49_C3_051
        '
        Me.S49_C3_051.Height = 0.1875!
        Me.S49_C3_051.HyperLink = Nothing
        Me.S49_C3_051.Left = 2.25!
        Me.S49_C3_051.Name = "S49_C3_051"
        Me.S49_C3_051.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_051.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_051.Top = 17.127!
        Me.S49_C3_051.Width = 1.0!
        '
        'Label100
        '
        Me.Label100.Height = 0.1875!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 0!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label100.Text = "SUM_spθ_BS4=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label100.Top = 17.31403!
        Me.Label100.Width = 2.25!
        '
        'Label101
        '
        Me.Label101.Height = 0.1875!
        Me.Label101.HyperLink = Nothing
        Me.Label101.Left = 3.25!
        Me.Label101.Name = "Label101"
        Me.Label101.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label101.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pr_BS1 / ΦRn_sp_BS3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label101.Top = 17.31403!
        Me.Label101.Width = 4.75!
        '
        'S49_C3_052
        '
        Me.S49_C3_052.Height = 0.1875!
        Me.S49_C3_052.HyperLink = Nothing
        Me.S49_C3_052.Left = 2.25!
        Me.S49_C3_052.Name = "S49_C3_052"
        Me.S49_C3_052.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C3_052.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_052.Top = 17.315!
        Me.S49_C3_052.Width = 1.0!
        '
        'Label106
        '
        Me.Label106.Height = 0.1875!
        Me.Label106.HyperLink = Nothing
        Me.Label106.Left = 0!
        Me.Label106.Name = "Label106"
        Me.Label106.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label106.Text = "SUM_spθ_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label106.Top = 17.69!
        Me.Label106.Width = 2.25!
        '
        'Label107
        '
        Me.Label107.Height = 0.1875!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 3.25!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label107.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= max( SUM_spθ_BS1, SUM_spθ_BS2, SUM_spθ_BS3, SUM_spθ_BS4)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label107.Top = 17.69!
        Me.Label107.Width = 4.75!
        '
        'S49_C3_053
        '
        Me.S49_C3_053.Height = 0.1875!
        Me.S49_C3_053.HyperLink = Nothing
        Me.S49_C3_053.Left = 2.25!
        Me.S49_C3_053.Name = "S49_C3_053"
        Me.S49_C3_053.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S49_C3_053.Text = "4.063" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C3_053.Top = 17.69097!
        Me.S49_C3_053.Width = 1.0!
        '
        'Label104
        '
        Me.Label104.Height = 0.1875!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 0!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label104.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label104.Top = 17.875!
        Me.Label104.Width = 2.25!
        '
        'Label105
        '
        Me.Label105.Height = 0.1875!
        Me.Label105.HyperLink = Nothing
        Me.Label105.Left = 3.25!
        Me.Label105.Name = "Label105"
        Me.Label105.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label105.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= OK, if SUM_spθ_BS <= SUM_spθ_BS_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label105.Top = 17.875!
        Me.Label105.Width = 4.75!
        '
        'Label109
        '
        Me.Label109.Height = 0.1875!
        Me.Label109.HyperLink = Nothing
        Me.Label109.Left = 0.4980006!
        Me.Label109.Name = "Label109"
        Me.Label109.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label109.Text = "DCR Summary:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label109.Top = 18.125!
        Me.Label109.Width = 1.0!
        '
        'SUM_002
        '
        Me.SUM_002.Height = 0.188!
        Me.SUM_002.HyperLink = Nothing
        Me.SUM_002.Left = 1.499002!
        Me.SUM_002.Name = "SUM_002"
        Me.SUM_002.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_002.Text = "q1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_002.Top = 18.74899!
        Me.SUM_002.Width = 0.75!
        '
        'SUM_001
        '
        Me.SUM_001.Height = 0.188!
        Me.SUM_001.HyperLink = Nothing
        Me.SUM_001.Left = 1.499002!
        Me.SUM_001.Name = "SUM_001"
        Me.SUM_001.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 0"
        Me.SUM_001.Text = "0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_001.Top = 18.56199!
        Me.SUM_001.Width = 0.75!
        '
        'Label279
        '
        Me.Label279.Height = 0.188!
        Me.Label279.HyperLink = Nothing
        Me.Label279.Left = 3.752002!
        Me.Label279.Name = "Label279"
        Me.Label279.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label279.Text = "P+M" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label279.Top = 18.37399!
        Me.Label279.Width = 0.75!
        '
        'Label277
        '
        Me.Label277.Height = 0.188!
        Me.Label277.HyperLink = Nothing
        Me.Label277.Left = 3.001002!
        Me.Label277.Name = "Label277"
        Me.Label277.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label277.Text = "Rupture" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label277.Top = 18.37399!
        Me.Label277.Width = 0.7499998!
        '
        'Label273
        '
        Me.Label273.Height = 0.188!
        Me.Label273.HyperLink = Nothing
        Me.Label273.Left = 2.250002!
        Me.Label273.Name = "Label273"
        Me.Label273.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label273.Text = "Yielding" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label273.Top = 18.37399!
        Me.Label273.Width = 0.75!
        '
        'Label271
        '
        Me.Label271.Height = 0.188!
        Me.Label271.HyperLink = Nothing
        Me.Label271.Left = 1.499002!
        Me.Label271.Name = "Label271"
        Me.Label271.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label271.Text = "Shear" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label271.Top = 18.37399!
        Me.Label271.Width = 0.75!
        '
        'SUM_005
        '
        Me.SUM_005.Height = 0.188!
        Me.SUM_005.HyperLink = Nothing
        Me.SUM_005.Left = 2.250002!
        Me.SUM_005.Name = "SUM_005"
        Me.SUM_005.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_005.Text = "q1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_005.Top = 18.56199!
        Me.SUM_005.Width = 0.75!
        '
        'SUM_009
        '
        Me.SUM_009.Height = 0.188!
        Me.SUM_009.HyperLink = Nothing
        Me.SUM_009.Left = 3.001002!
        Me.SUM_009.Name = "SUM_009"
        Me.SUM_009.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_009.Text = "q2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_009.Top = 18.56199!
        Me.SUM_009.Width = 0.7499998!
        '
        'SUM_013
        '
        Me.SUM_013.Height = 0.188!
        Me.SUM_013.HyperLink = Nothing
        Me.SUM_013.Left = 3.752002!
        Me.SUM_013.Name = "SUM_013"
        Me.SUM_013.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_013.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_013.Top = 18.56199!
        Me.SUM_013.Width = 0.75!
        '
        'SUM_006
        '
        Me.SUM_006.Height = 0.188!
        Me.SUM_006.HyperLink = Nothing
        Me.SUM_006.Left = 2.250002!
        Me.SUM_006.Name = "SUM_006"
        Me.SUM_006.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_006.Text = "q2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_006.Top = 18.74899!
        Me.SUM_006.Width = 0.75!
        '
        'SUM_010
        '
        Me.SUM_010.Height = 0.188!
        Me.SUM_010.HyperLink = Nothing
        Me.SUM_010.Left = 3.001002!
        Me.SUM_010.Name = "SUM_010"
        Me.SUM_010.Style = "color: Blue; font-family: Arial; font-size: 8.25pt; font-weight: normal; text-ali" &
    "gn: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_010.Text = "q3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_010.Top = 18.74899!
        Me.SUM_010.Width = 0.7499998!
        '
        'Line9
        '
        Me.Line9.Height = 0.938509!
        Me.Line9.Left = 0.4980006!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 18.374!
        Me.Line9.Width = 0!
        Me.Line9.X1 = 0.4980006!
        Me.Line9.X2 = 0.4980006!
        Me.Line9.Y1 = 18.374!
        Me.Line9.Y2 = 19.31251!
        '
        'Line3
        '
        Me.Line3.Height = 0.9385109!
        Me.Line3.Left = 1.499002!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 18.37399!
        Me.Line3.Width = 0!
        Me.Line3.X1 = 1.499002!
        Me.Line3.X2 = 1.499002!
        Me.Line3.Y1 = 18.37399!
        Me.Line3.Y2 = 19.3125!
        '
        'Line4
        '
        Me.Line4.Height = 0.9390011!
        Me.Line4.Left = 2.249002!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 18.37399!
        Me.Line4.Width = 0!
        Me.Line4.X1 = 2.249002!
        Me.Line4.X2 = 2.249002!
        Me.Line4.Y1 = 18.37399!
        Me.Line4.Y2 = 19.31299!
        '
        'Line5
        '
        Me.Line5.Height = 0.9390011!
        Me.Line5.Left = 3.000002!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 18.37399!
        Me.Line5.Width = 0!
        Me.Line5.X1 = 3.000002!
        Me.Line5.X2 = 3.000002!
        Me.Line5.Y1 = 18.37399!
        Me.Line5.Y2 = 19.31299!
        '
        'Line6
        '
        Me.Line6.Height = 0.9390011!
        Me.Line6.Left = 3.751002!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 18.37399!
        Me.Line6.Width = 0!
        Me.Line6.X1 = 3.751002!
        Me.Line6.X2 = 3.751002!
        Me.Line6.Y1 = 18.37399!
        Me.Line6.Y2 = 19.31299!
        '
        'Line12
        '
        Me.Line12.Height = 0!
        Me.Line12.Left = 0.4980006!
        Me.Line12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line12.LineWeight = 1.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 19.31299!
        Me.Line12.Width = 7.0!
        Me.Line12.X1 = 0.4980006!
        Me.Line12.X2 = 7.498001!
        Me.Line12.Y1 = 19.31299!
        Me.Line12.Y2 = 19.31299!
        '
        'Label56
        '
        Me.Label56.Height = 0.188!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 4.503003!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label56.Text = "Bearing" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label56.Top = 18.37399!
        Me.Label56.Width = 0.75!
        '
        'Label59
        '
        Me.Label59.Height = 0.188!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 5.253003!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label59.Text = "BS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label59.Top = 18.37399!
        Me.Label59.Width = 0.75!
        '
        'Label62
        '
        Me.Label62.Height = 0.188!
        Me.Label62.HyperLink = Nothing
        Me.Label62.Left = 6.003003!
        Me.Label62.Name = "Label62"
        Me.Label62.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label62.Text = "Max" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label62.Top = 18.37399!
        Me.Label62.Width = 0.75!
        '
        'Line7
        '
        Me.Line7.Height = 0.9389992!
        Me.Line7.Left = 6.754003!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 18.37499!
        Me.Line7.Width = 0!
        Me.Line7.X1 = 6.754003!
        Me.Line7.X2 = 6.754003!
        Me.Line7.Y1 = 18.37499!
        Me.Line7.Y2 = 19.31399!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 0.4980006!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 18.37399!
        Me.Line2.Width = 7.0!
        Me.Line2.X1 = 0.4980006!
        Me.Line2.X2 = 7.498001!
        Me.Line2.Y1 = 18.37399!
        Me.Line2.Y2 = 18.37399!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0.4980006!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 18.56199!
        Me.Line1.Width = 7.0!
        Me.Line1.X1 = 0.4980006!
        Me.Line1.X2 = 7.498001!
        Me.Line1.Y1 = 18.56199!
        Me.Line1.Y2 = 18.56199!
        '
        'Line13
        '
        Me.Line13.Height = 0.9389992!
        Me.Line13.Left = 4.502002!
        Me.Line13.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line13.LineWeight = 1.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 18.37499!
        Me.Line13.Width = 0!
        Me.Line13.X1 = 4.502002!
        Me.Line13.X2 = 4.502002!
        Me.Line13.Y1 = 18.37499!
        Me.Line13.Y2 = 19.31399!
        '
        'Line14
        '
        Me.Line14.Height = 0.9389992!
        Me.Line14.Left = 5.253003!
        Me.Line14.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line14.LineWeight = 1.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 18.37499!
        Me.Line14.Width = 0!
        Me.Line14.X1 = 5.253003!
        Me.Line14.X2 = 5.253003!
        Me.Line14.Y1 = 18.37499!
        Me.Line14.Y2 = 19.31399!
        '
        'Line15
        '
        Me.Line15.Height = 0.9389992!
        Me.Line15.Left = 6.003003!
        Me.Line15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line15.LineWeight = 1.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 18.37499!
        Me.Line15.Width = 0!
        Me.Line15.X1 = 6.003003!
        Me.Line15.X2 = 6.003003!
        Me.Line15.Y1 = 18.37499!
        Me.Line15.Y2 = 19.31399!
        '
        'Line8
        '
        Me.Line8.Height = 0!
        Me.Line8.Left = 0.4980006!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 18.74899!
        Me.Line8.Width = 7.0!
        Me.Line8.X1 = 0.4980006!
        Me.Line8.X2 = 7.498001!
        Me.Line8.Y1 = 18.74899!
        Me.Line8.Y2 = 18.74899!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 0.4980006!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 18.93599!
        Me.Line10.Width = 7.0!
        Me.Line10.X1 = 0.4980006!
        Me.Line10.X2 = 7.498001!
        Me.Line10.Y1 = 18.93599!
        Me.Line10.Y2 = 18.93599!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 0.4980006!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 19.12399!
        Me.Line11.Width = 7.0!
        Me.Line11.X1 = 0.4980006!
        Me.Line11.X2 = 7.498001!
        Me.Line11.Y1 = 19.12399!
        Me.Line11.Y2 = 19.12399!
        '
        'Line16
        '
        Me.Line16.Height = 0.9389992!
        Me.Line16.Left = 7.500001!
        Me.Line16.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line16.LineWeight = 1.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 18.375!
        Me.Line16.Width = 0!
        Me.Line16.X1 = 7.500001!
        Me.Line16.X2 = 7.500001!
        Me.Line16.Y1 = 18.375!
        Me.Line16.Y2 = 19.314!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.25!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 17.875!
        Me.Shape1.Width = 1.0!
        '
        'Shape20
        '
        Me.Shape20.Height = 0.1875!
        Me.Shape20.Left = 2.25!
        Me.Shape20.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape20.Name = "Shape20"
        Me.Shape20.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape20.Top = 9.875!
        Me.Shape20.Width = 1.0!
        '
        'Shape19
        '
        Me.Shape19.Height = 0.1875!
        Me.Shape19.Left = 2.25!
        Me.Shape19.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape19.Name = "Shape19"
        Me.Shape19.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape19.Top = 9.25!
        Me.Shape19.Width = 1.0!
        '
        'Shape18
        '
        Me.Shape18.Height = 0.1875!
        Me.Shape18.Left = 2.25!
        Me.Shape18.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape18.Name = "Shape18"
        Me.Shape18.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape18.Top = 5.375!
        Me.Shape18.Width = 1.0!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.2!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(7.75!, 0.2!)
        Me.PageBreak1.Top = 15.437!
        Me.PageBreak1.Width = 7.75!
        '
        'US_SPC7
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.75!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S49_C3_054, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label613, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label614, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label615, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label616, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label618, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label619, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label620, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label622, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label623, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label625, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label626, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label628, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label629, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label631, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label632, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label634, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label635, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label637, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label638, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label640, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label641, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label643, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label644, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label646, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label647, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label649, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label650, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label652, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label653, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label655, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label656, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label658, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label659, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label661, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label662, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label669, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label664, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label665, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label667, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label668, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label682, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label671, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label672, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label673, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label675, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_033, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_034, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_035, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_036, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_037, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_038, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_039, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_040, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_041, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_042, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_043, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_044, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_045, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_046, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_047, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_048, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_049, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_050, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_051, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_052, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C3_053, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label277, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label613 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture8 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Label614 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label615 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label616 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label618 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label619 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label620 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label622 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label623 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label625 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label626 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label628 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label629 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label631 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label632 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label634 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label635 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label637 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label638 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label640 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label641 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label643 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label644 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label646 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label647 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label649 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label650 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label652 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label653 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label655 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label656 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label658 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label659 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label661 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label662 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label669 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape18 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label664 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label665 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label667 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label668 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape19 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label682 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label671 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label672 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label673 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape20 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label675 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label44 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_034 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label48 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_035 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label51 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_036 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label55 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_037 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_038 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label61 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_039 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label63 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label64 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_040 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label67 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_041 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label69 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label70 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_042 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label72 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label73 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_043 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label75 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label77 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_044 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label79 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label80 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_045 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label82 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label83 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_046 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label85 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label86 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_047 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label88 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label89 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_048 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label91 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label92 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_049 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label94 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label95 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_050 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label97 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label98 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_051 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label100 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label101 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_052 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label106 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label107 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_053 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C3_054 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label104 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label105 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label109 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label279 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label277 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label273 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label271 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents SUM_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line12 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents SUM_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label56 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label62 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line13 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line14 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line15 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents SUM_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line16 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak

    '

End Class
