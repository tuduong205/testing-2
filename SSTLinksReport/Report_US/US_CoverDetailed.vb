﻿Imports YieldLinkLib

Public Class US_CoverDetailed

    Private typ As ARType
    Private job As clsMRSJob

    '
    Sub New(_job As clsMRSJob, _typ As ARType)
        ' This call is required by the designer.
        InitializeComponent()
        '
        job = _job
        lbVersion.Text = job.versionID
        lbJobName.Text = job.Name
        lbJobID.Text = job.JobID
        lbPrintedDate.Text = "Date Printed: " & job.PrintedDate
        '
        typ = _typ
        If typ = ARType.T1_ILS Then
            lbARType.Text = "INITIAL LINK SELECTION DESIGN DETAILS"
        ElseIf typ = ARType.T2_BLC Then
            lbARType.Text = "BEAM & YIELD-LINK® DESIGN DETAILS"
        ElseIf typ = ARType.T3_CC Then
            lbARType.Text = "COLUMN DESIGN DETAILS"
        ElseIf typ = ARType.T4_SPC Then
            lbARType.Text = "SHEAR PLATE DESIGN DETAILS"
        End If
    End Sub

    Private Sub SR_p1_ReportStart(sender As Object, e As EventArgs) Handles MyBase.ReportStart
        If typ = ARType.T1_ILS Then
            SubReport1.Report = New US_Detail_ILS(job.stlData)
            SubReport2.Visible = False
            SubReport3.Visible = False
            SubReport4.Visible = False
            SubReport5.Visible = False
        ElseIf typ = ARType.T2_BLC Then
            SubReport1.Report = New US_Detail_BLC(job.stlData)
            SubReport2.Visible = False
            SubReport3.Visible = False
            SubReport4.Visible = False
            SubReport5.Visible = False
        ElseIf typ = ARType.T3_CC Then
            SubReport1.Report = New US_CC1(job.stlData)
            SubReport2.Report = New US_CC2(job.stlData)
            SubReport3.Report = New US_CC3(job.stlData)
            SubReport4.Report = New US_CC4(job.stlData)
            SubReport5.Report = New US_CC5(job.stlData)
        ElseIf typ = ARType.T4_SPC Then
            SubReport1.Report = New US_SPC1(job.stlData)
            SubReport2.Report = New US_SPC2(job.stlData)
            SubReport3.Report = New US_SPC3(job.stlData)
            SubReport4.Report = New US_SPC4(job.stlData)
            SubReport5.Report = New US_SPC5(job.stlData)
            SubReport6.Report = New US_SPC6(job.stlData)
            SubReport7.Report = New US_SPC7(job.stlData)
            SubReport6.Visible = True
            SubReport7.Visible = True
        End If
    End Sub

End Class
