﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_CoverDetailed
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_CoverDetailed))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.SubReport1 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.SubReport2 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.SubReport3 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.SubReport4 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.SubReport5 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.SubReport6 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.SubReport7 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.lbVersion = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.PageHeader1 = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.Picture = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Line = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.RichTextBox = New GrapeCity.ActiveReports.SectionReportModel.RichTextBox()
        Me.Label = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.PageFooter1 = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        Me.reportInfo1 = New GrapeCity.ActiveReports.SectionReportModel.ReportInfo()
        Me.GroupHeader1 = New GrapeCity.ActiveReports.SectionReportModel.GroupHeader()
        Me.lbARType = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbJobID = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbJobName = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbPrintedDate = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.GroupFooter1 = New GrapeCity.ActiveReports.SectionReportModel.GroupFooter()
        CType(Me.lbVersion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.reportInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbARType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.SubReport1, Me.SubReport2, Me.SubReport3, Me.SubReport4, Me.SubReport5, Me.SubReport6, Me.SubReport7})
        Me.Detail.Height = 7.0!
        Me.Detail.Name = "Detail"
        '
        'SubReport1
        '
        Me.SubReport1.CloseBorder = False
        Me.SubReport1.Height = 0.25!
        Me.SubReport1.Left = 0.5!
        Me.SubReport1.Name = "SubReport1"
        Me.SubReport1.Report = Nothing
        Me.SubReport1.ReportName = "sub1"
        Me.SubReport1.Top = 0.06200001!
        Me.SubReport1.Width = 7.75!
        '
        'SubReport2
        '
        Me.SubReport2.CloseBorder = False
        Me.SubReport2.Height = 0.25!
        Me.SubReport2.Left = 0.5!
        Me.SubReport2.Name = "SubReport2"
        Me.SubReport2.Report = Nothing
        Me.SubReport2.ReportName = "sub2"
        Me.SubReport2.Top = 0.312!
        Me.SubReport2.Width = 7.75!
        '
        'SubReport3
        '
        Me.SubReport3.CloseBorder = False
        Me.SubReport3.Height = 0.25!
        Me.SubReport3.Left = 0.5!
        Me.SubReport3.Name = "SubReport3"
        Me.SubReport3.Report = Nothing
        Me.SubReport3.ReportName = "sub3"
        Me.SubReport3.Top = 0.562!
        Me.SubReport3.Width = 7.75!
        '
        'SubReport4
        '
        Me.SubReport4.CloseBorder = False
        Me.SubReport4.Height = 0.25!
        Me.SubReport4.Left = 0.5!
        Me.SubReport4.Name = "SubReport4"
        Me.SubReport4.Report = Nothing
        Me.SubReport4.ReportName = "sub4"
        Me.SubReport4.Top = 0.812!
        Me.SubReport4.Width = 7.75!
        '
        'SubReport5
        '
        Me.SubReport5.CloseBorder = False
        Me.SubReport5.Height = 0.25!
        Me.SubReport5.Left = 0.5!
        Me.SubReport5.Name = "SubReport5"
        Me.SubReport5.Report = Nothing
        Me.SubReport5.ReportName = "sub5"
        Me.SubReport5.Top = 1.062!
        Me.SubReport5.Width = 7.75!
        '
        'SubReport6
        '
        Me.SubReport6.CloseBorder = False
        Me.SubReport6.Height = 0.25!
        Me.SubReport6.Left = 0.5!
        Me.SubReport6.Name = "SubReport6"
        Me.SubReport6.Report = Nothing
        Me.SubReport6.ReportName = "sub5"
        Me.SubReport6.Top = 1.312!
        Me.SubReport6.Visible = False
        Me.SubReport6.Width = 7.75!
        '
        'SubReport7
        '
        Me.SubReport7.CloseBorder = False
        Me.SubReport7.Height = 0.25!
        Me.SubReport7.Left = 0.5!
        Me.SubReport7.Name = "SubReport7"
        Me.SubReport7.Report = Nothing
        Me.SubReport7.ReportName = "sub5"
        Me.SubReport7.Top = 1.562!
        Me.SubReport7.Visible = False
        Me.SubReport7.Width = 7.75!
        '
        'lbVersion
        '
        Me.lbVersion.Height = 0.188!
        Me.lbVersion.HyperLink = Nothing
        Me.lbVersion.Left = 0.5!
        Me.lbVersion.Name = "lbVersion"
        Me.lbVersion.Style = "font-size: 8.25pt; ddo-char-set: 1"
        Me.lbVersion.Text = "Version ID"
        Me.lbVersion.Top = 0.013!
        Me.lbVersion.Width = 2.75!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 0.5!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 0!
        Me.Line5.Width = 7.75!
        Me.Line5.X1 = 0.5!
        Me.Line5.X2 = 8.25!
        Me.Line5.Y1 = 0!
        Me.Line5.Y2 = 0!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Picture, Me.Line, Me.RichTextBox, Me.Label, Me.Label1, Me.label6})
        Me.PageHeader1.Height = 1.375!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Picture
        '
        Me.Picture.Height = 1.005!
        Me.Picture.HyperLink = Nothing
        Me.Picture.ImageData = CType(resources.GetObject("Picture.ImageData"), System.IO.Stream)
        Me.Picture.Left = 0.5!
        Me.Picture.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Picture.Name = "Picture"
        Me.Picture.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture.Top = 0.25!
        Me.Picture.Width = 1.3125!
        '
        'Line
        '
        Me.Line.Height = 0!
        Me.Line.Left = 0.5!
        Me.Line.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 1.25!
        Me.Line.Width = 7.75!
        Me.Line.X1 = 0.5!
        Me.Line.X2 = 8.25!
        Me.Line.Y1 = 1.25!
        Me.Line.Y2 = 1.25!
        '
        'RichTextBox
        '
        Me.RichTextBox.AutoReplaceFields = True
        Me.RichTextBox.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.RichTextBox.Height = 0.1875!
        Me.RichTextBox.Left = 1.937!
        Me.RichTextBox.Name = "RichTextBox"
        Me.RichTextBox.RTF = resources.GetString("RichTextBox.RTF")
        Me.RichTextBox.Top = 0.312!
        Me.RichTextBox.Width = 3.6875!
        '
        'Label
        '
        Me.Label.Height = 0.2!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 1.937!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.Label.Text = "5956 W. Las Positas Blvd.,  Pleasanton, CA 94588."
        Me.Label.Top = 0.562!
        Me.Label.Width = 3.625!
        '
        'Label1
        '
        Me.Label1.Height = 0.2!
        Me.Label1.HyperLink = ""
        Me.Label1.Left = 1.937!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "text-decoration: underline; ddo-char-set: 0"
        Me.Label1.Text = "www.strongtie.com"
        Me.Label1.Top = 0.937!
        Me.Label1.Width = 1.5!
        '
        'label6
        '
        Me.label6.Height = 0.2!
        Me.label6.HyperLink = Nothing
        Me.label6.Left = 1.937!
        Me.label6.Name = "label6"
        Me.label6.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.label6.Text = "(800) 999-5099"
        Me.label6.Top = 0.7495!
        Me.label6.Width = 3.625!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.reportInfo1, Me.lbVersion, Me.Line5})
        Me.PageFooter1.Height = 0.3125!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'reportInfo1
        '
        Me.reportInfo1.FormatString = "Page {PageNumber} of {PageCount}"
        Me.reportInfo1.Height = 0.1875!
        Me.reportInfo1.Left = 5.5!
        Me.reportInfo1.Name = "reportInfo1"
        Me.reportInfo1.Style = "font-size: 8.25pt; text-align: right; vertical-align: top; ddo-char-set: 0"
        Me.reportInfo1.Top = 0.013!
        Me.reportInfo1.Width = 2.75!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.lbARType, Me.lbJobID, Me.lbJobName, Me.lbPrintedDate, Me.Label2, Me.Label3})
        Me.GroupHeader1.Height = 0.8020833!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'lbARType
        '
        Me.lbARType.Height = 0.25!
        Me.lbARType.HyperLink = Nothing
        Me.lbARType.Left = 0.5!
        Me.lbARType.Name = "lbARType"
        Me.lbARType.Style = "font-size: 10.5pt; font-weight: bold; vertical-align: middle; ddo-char-set: 1"
        Me.lbARType.Text = "INITIAL LINK SELECTION DESIGN DETAILS"
        Me.lbARType.Top = 0.552!
        Me.lbARType.Width = 8.0!
        '
        'lbJobID
        '
        Me.lbJobID.Height = 0.2!
        Me.lbJobID.HyperLink = Nothing
        Me.lbJobID.Left = 1.0!
        Me.lbJobID.Name = "lbJobID"
        Me.lbJobID.Style = "font-size: 9pt; font-weight: normal; ddo-char-set: 1"
        Me.lbJobID.Text = "ES-123456" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobID.Top = 0.187!
        Me.lbJobID.Width = 1.75!
        '
        'lbJobName
        '
        Me.lbJobName.Height = 0.2!
        Me.lbJobName.HyperLink = Nothing
        Me.lbJobName.Left = 1.25!
        Me.lbJobName.Name = "lbJobName"
        Me.lbJobName.Style = "font-size: 9pt; font-weight: normal; ddo-char-set: 1"
        Me.lbJobName.Text = "Test Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobName.Top = 0!
        Me.lbJobName.Width = 4.063!
        '
        'lbPrintedDate
        '
        Me.lbPrintedDate.Height = 0.2!
        Me.lbPrintedDate.HyperLink = Nothing
        Me.lbPrintedDate.Left = 6.0!
        Me.lbPrintedDate.Name = "lbPrintedDate"
        Me.lbPrintedDate.Style = "font-size: 9.75pt; font-weight: normal; text-align: right"
        Me.lbPrintedDate.Text = "Date Printed: Jun 17, 2018" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbPrintedDate.Top = 0!
        Me.lbPrintedDate.Width = 2.25!
        '
        'Label2
        '
        Me.Label2.Height = 0.2!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.4999999!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 9.75pt; font-weight: normal"
        Me.Label2.Text = "Job ID: " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 0.187!
        Me.Label2.Width = 0.5!
        '
        'Label3
        '
        Me.Label3.Height = 0.2!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.4999999!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 9pt; font-weight: normal; ddo-char-set: 1"
        Me.Label3.Text = "Job Name:"
        Me.Label3.Top = 0!
        Me.Label3.Width = 0.75!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'SR_CoverDetailed
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.5!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        Me.WatermarkAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft
        CType(Me.lbVersion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.reportInfo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbARType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents SR_LineInfo_OnPage As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents lbVersion As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents PageHeader1 As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents PageFooter1 As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    Private WithEvents reportInfo1 As GrapeCity.ActiveReports.SectionReportModel.ReportInfo
    Private WithEvents Picture As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Line As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents RichTextBox As GrapeCity.ActiveReports.SectionReportModel.RichTextBox
    Private WithEvents Label As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents GroupHeader1 As GrapeCity.ActiveReports.SectionReportModel.GroupHeader
    Private WithEvents GroupFooter1 As GrapeCity.ActiveReports.SectionReportModel.GroupFooter
    Private WithEvents lbARType As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobID As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobName As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbPrintedDate As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SubReport1 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SubReport2 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents SubReport3 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents SubReport4 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents SubReport5 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents SubReport6 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents SubReport7 As GrapeCity.ActiveReports.SectionReportModel.SubReport
End Class
