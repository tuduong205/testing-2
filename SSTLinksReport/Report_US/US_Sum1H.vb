﻿Imports GrapeCity.ActiveReports
Imports YieldLinkLib

Public Class US_Sum1H

    Private index As Integer = 0
    Private DGV_flag As Integer
    Private job As clsMRSJob
    Private headers() As String
    Private Locs() As Double

    Sub New(_job As clsMRSJob, _DGV_flag As Integer)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        job = _job
        DGV_flag = _DGV_flag
        '
        lbJobName.Text = job.Name
        lbJobID.Text = job.JobID
        lbPrintedDate.Text = "Date Printed: " & job.PrintedDate
        ''
        Dim lb As SectionReportModel.Label
        Dim ln As SectionReportModel.Line
        'Dim w As Single = 1
        Dim X As Double
        If DGV_flag = 1 Then
            lbDGVName.Text = "Initial Yield-Link® Selection Summary"
            headers = {"Elev. ID", "Grid ID", "Story", "Beam Unique Name", "Beam Size", "Yield-Link® Size", "K_rot (kip-in/rad)", "I_End Col Size", "J_End Col Size", "Assign K at I_End", "Assign K at J_End", "Initial tbf Check", "Initial bf Check", "Initial Lyield Check", "PZ I_End Col", "PZ J_End Col", "Drift DCR"}
            Locs = {0.5, 0.5, 0.625, 0.625, 0.75, 0.75, 0.75, 0.75, 0.75, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5}
        ElseIf DGV_flag = 2 Then
            lbDGVName.Text = "Beam & Yield-Link® Check Summary"
            headers = {"Elev. ID", "Grid ID", "Story", "Beam Unique Name", "Beam Size", "Yield-Link® Size", "BRP Size", "MU_max (kips.in)", "Beam tf_DCR", "Link Strength DCR", "Lyield DCR", "BRP DCR", "BRP Bolt DCR"}
            Locs = {0.75, 0.75, 0.75, 0.75, 1, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75}
        ElseIf DGV_flag = 3 Then
            lbDGVName.Text = "Column Check Summary"
            headers = {"Elev. ID", "Grid ID", "Story", "Column Unique Name", "Column Size", "Yield- Link® Left Side", "Yield- Link® Right Side", "Col Pu (kips)", "V_bm Gravity (kips)", "Stiffener Provider", "Doubler Plate Provider", "Stiffener Req.", "Min. Stiff. Thk (in)", "Min. Dblr Plate Thk (in)", "SCWB DCR", "Col PZ DCR", "Col Flange Check"}
            Locs = {0.5, 0.5, 0.75, 0.625, 0.875, 0.75, 0.75, 0.5, 0.5, 0.625, 0.625, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5}
        ElseIf DGV_flag = 4 Then
            lbDGVName.Text = "Shear Tab Check Summary"
            headers = {"Elev. ID", "Grid ID", "Story", "Beam Unique Name", "Beam Size", "Yield-Link® Size", "Axial Pu (kips)", "Shear Vu_bm (kips)", "No. Vert. Bolts", "No. Horz. Bolts", "Bolt Size", "Bolt Type", "Shear Plate Thk (in)", "Fillet Weld Size (in)", "Beam Web DCR", "Shear PL  Geo. Check", "Shear Plate DCR", "Bolt DCR", "Fillet Weld DCR (in)"}
            Locs = {0.5, 0.5, 0.5, 0.5, 0.75, 0.75, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5}
        ElseIf DGV_flag = 5 Then
            lbDGVName.Text = "Drift Summary"
            'headers = {"Elev. ID", "Grid ID", "Story", "Beam Unique Name", "Beam Size", "Yield-Link® Size", "I_End Col Size", "J_End Col Size", "Assign K at I_End", "Assign K at J_End", "Node ID @LeftEnd", "Node ID Below", "Delta_x (in)", "Story Height (in)", "Allowable Drift (in)", "Drift DCR"}
            'Locs = {0.5, 0.5, 0.5, 0.5, 1, 0.75, 0.625, 0.625, 0.625, 0.625, 0.625, 0.625, 0.625, 0.625, 0.625, 0.625}
            headers = {"Elev. ID", "Grid ID", "Story", "Beam Unique Name", "Beam Size", "Yield-Link® Size", "Assign K at I_End", "Assign K at J_End", "Node ID @LeftEnd", "Node ID Below", "Seismic Delta_x (in)", "Wind Delta_x (in)", "Story Height (in)", "Allowable Seismic Drift (in)", "Allowable Wind Drift (in)", "Seismic Drift DCR", "Wind Drift DCR"}
            Locs = {0.5, 0.5, 0.5, 0.5, 1, 0.75, 0.625, 0.625, 0.625, 0.625, 0.5, 0.5, 0.5, 0.625, 0.625, 0.5, 0.5}
        ElseIf DGV_flag = 6 Then
            lbDGVName.Text = "Beam Design"
            headers = {"Elev. ID", "Grid ID", "Story", "Beam Unique Name", "Beam Size", "Yield-Link® ID", "Pcap_Link (kips)", "Mcap_Link (kips.in)", "Mu_max (kips.in)", "Mu_max/ Pcap_Link", "Mu_Adj. Fact", "Axial DCR", "B-maj DCR", "B_min DCR", "PMM DCR", "Adj. B_maj DCR", "Total DCR"}
            Locs = {0.5, 0.5, 0.625, 0.5, 0.875, 0.75, 0.75, 0.625, 0.625, 0.625, 0.625, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5}
        ElseIf DGV_flag = 7 Then
            lbDGVName.Text = "Column Design"
            headers = {"Elev. ID", "Grid ID", "Story", "Column Unique Name", "Column Size", "Mu_Top (kips.in)", "Mu_Bot (kips.in)", "Mu_ Omega (kips.in)", "Pu_ Omega (kips)", "Bracing at Beam Bot. FLG", "bf/tf DCR", "h/tw DCR", "Axial DCR", "B_maj DCR", "B_min DCR", "PMM DCR", "Adj. B_maj DCR", "Total DCR"}
            Locs = {0.5, 0.5, 0.625, 0.5, 0.75, 0.625, 0.625, 0.625, 0.625, 0.625, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5}
        ElseIf DGV_flag = 8 Then
            If job.l <> 0 Then
                lbDGVName.Text = "Weld Summary"
                headers = {"Elev. ID", "Grid ID", "Story", "Col. Unique Name", "Column Size", "Left ST Thk (in)", "Right ST Thk (in)", "STP Thk (in)", "DP Thk (in)", "Left ST Weld W1A (in)", "No. sides of W1A", "Left ST Weld W1B (in)", "No. sides of W1B", "STP to Col. Web W2 (in)", "No. sides of W2", "STP to Col. Flg. W3 (in)", "No. sides of W3", "DP to Col. Web W4 (in)", "No. sides of W4", "Plug Weld Dia. (in)", "Plug Weld Depth. (in)", "DP to Col. Flg. W5 (in)", "No. sides of W5"}
                Locs = {0.375, 0.375, 0.5, 0.4375, 0.75, 0.4375, 0.4375, 0.375, 0.375, 0.44, 0.375, 0.4375, 0.44, 0.4375, 0.375, 0.4375, 0.375, 0.4375, 0.4375, 0.4375, 0.4375, 0.4375, 0.4375}
            Else
                lbDGVName.Text = "Weld Summary"
                headers = {"Elev. ID", "Grid ID", "Story", "Col. Unique Name", "Column Size", "Left ST Thk (in)", "Right ST Thk (in)", "STP Thk (in)", "DP Thk (in)", "Left ST Weld W1A (in)", "No. sides of W1A", "Left ST Weld W1B (in)", "No. sides of W1B", "STP to Col. Web W2 (in)", "No. sides of W2", "STP to Col. Flg. W3 (in)", "No. sides of W3"}
                Locs = {0.375, 0.375, 0.5, 0.4375, 0.75, 0.65, 0.65, 0.65, 0.65, 0.65, 0.65, 0.65, 0.65, 0.6, 0.6, 0.6, 0.6}
            End If
        End If

        '
        For i = 0 To headers.Count - 1
            lb = New SectionReportModel.Label
            lb.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
            lb.Location = New PointF(X, 0)
            lb.Size = New SizeF(Locs(i), GroupHeader2.Height)
            '
            lb.Text = headers(i)
            GroupHeader2.Controls.Add(lb)
            '
            ln = New SectionReportModel.Line
            ln.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
            ln.Width = 1
            ln.X1 = X
            ln.X2 = X
            ln.Y1 = 0.0!
            ln.Y2 = GroupHeader2.Height
            GroupHeader2.Controls.Add(ln)
            '
            X += Locs(i)
        Next
    End Sub

    Private Sub subReport1_FetchData(sender As Object, eArgs As FetchEventArgs) Handles MyBase.FetchData
        If DGV_flag = 1 Then
            If index > job.Dgv1Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        ElseIf DGV_flag = 2 Then
            If index > job.Dgv2Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        ElseIf DGV_flag = 3 Then
            If index > job.Dgv3Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        ElseIf DGV_flag = 4 Then
            If index > job.Dgv4Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        ElseIf DGV_flag = 5 Then
            If index > job.Dgv5Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        ElseIf DGV_flag = 6 Then
            If index > job.Dgv6Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        ElseIf DGV_flag = 7 Then
            If index > job.Dgv7Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        Else
            If index > job.Dgv8Data.Count - 1 Then
                eArgs.EOF = True
            Else
                eArgs.EOF = False
            End If
        End If
    End Sub

    Private Sub Detail_Format(sender As Object, e As EventArgs) Handles Detail.Format
        If DGV_flag = 1 Then
            If job.Dgv1Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv1Data(index), Locs, DGV_flag)
            End If
        ElseIf DGV_flag = 2 Then
            If job.Dgv2Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv2Data(index), Locs, DGV_flag)
            End If
        ElseIf DGV_flag = 3 Then
            If job.Dgv3Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv3Data(index), Locs, DGV_flag)
            End If
        ElseIf DGV_flag = 4 Then
            If job.Dgv4Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv4Data(index), Locs, DGV_flag)
            End If
        ElseIf DGV_flag = 5 Then
            If job.Dgv5Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv5Data(index), Locs, DGV_flag)
            End If
        ElseIf DGV_flag = 6 Then
            If job.Dgv6Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv6Data(index), Locs, DGV_flag)
            End If
        ElseIf DGV_flag = 7 Then
            If job.Dgv7Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv7Data(index), Locs, DGV_flag)
            End If
        Else
            If job.Dgv8Data.Count > index Then
                sub2.Report = New US_Sum2H(index, job.DCRLimits, job.Dgv8Data(index), Locs, DGV_flag)
            End If
        End If

        index += 1
    End Sub

End Class
