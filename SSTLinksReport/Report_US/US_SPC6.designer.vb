﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_SPC6
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_SPC6))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S49_C2_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape3 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label575 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture7 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label576 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label577 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label578 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label580 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label581 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label583 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label584 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label586 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label587 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label589 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label590 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label592 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label593 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label595 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label596 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label601 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label602 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label604 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label605 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label607 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label609 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label610 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label611 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label47 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label50 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label56 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label65 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label68 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label71 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label75 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label81 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label84 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C2_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label48 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape4 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.S49_C2_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label575, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label576, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label577, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label578, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label580, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label581, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label583, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label584, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label586, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label587, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label589, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label590, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label592, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label593, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label595, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label596, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label601, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label602, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label604, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label605, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label607, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label609, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label610, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label611, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C2_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S49_C2_031, Me.S49_C2_011, Me.Shape3, Me.Label575, Me.Picture7, Me.Label576, Me.Label577, Me.Label578, Me.S49_C2_002, Me.Label580, Me.Label581, Me.S49_C2_003, Me.Label583, Me.Label584, Me.S49_C2_004, Me.Label586, Me.Label587, Me.S49_C2_005, Me.Label589, Me.Label590, Me.S49_C2_006, Me.Label592, Me.Label593, Me.S49_C2_007, Me.Label595, Me.Label596, Me.S49_C2_008, Me.Label601, Me.Label602, Me.S49_C2_009, Me.Label604, Me.Label605, Me.Label607, Me.S49_C2_010, Me.Label609, Me.Label610, Me.Label611, Me.S49_C2_001, Me.Label2, Me.Label20, Me.Label22, Me.S49_C2_012, Me.Label29, Me.Label32, Me.S49_C2_013, Me.Label38, Me.Label41, Me.S49_C2_014, Me.Label47, Me.Label50, Me.S49_C2_015, Me.Label56, Me.Label59, Me.S49_C2_016, Me.Label65, Me.Label68, Me.S49_C2_017, Me.Label71, Me.Label75, Me.S49_C2_018, Me.Label81, Me.Label84, Me.S49_C2_019, Me.Label1, Me.Label4, Me.Label6, Me.S49_C2_021, Me.Label9, Me.Label10, Me.S49_C2_022, Me.Label12, Me.Label13, Me.S49_C2_020, Me.Label16, Me.Label18, Me.S49_C2_023, Me.Label21, Me.Label23, Me.S49_C2_024, Me.Label25, Me.Label26, Me.S49_C2_025, Me.Label28, Me.Label30, Me.S49_C2_026, Me.Label33, Me.Label34, Me.S49_C2_027, Me.Label36, Me.Label37, Me.S49_C2_028, Me.Label40, Me.Label42, Me.S49_C2_029, Me.Label46, Me.Label7, Me.Label15, Me.S49_C2_030, Me.Label48, Me.Label49, Me.Shape4, Me.Shape1, Me.PageBreak1})
        Me.Detail.Height = 10.30208!
        Me.Detail.Name = "Detail"
        '
        'S49_C2_031
        '
        Me.S49_C2_031.Height = 0.1875!
        Me.S49_C2_031.HyperLink = Nothing
        Me.S49_C2_031.Left = 2.25!
        Me.S49_C2_031.Name = "S49_C2_031"
        Me.S49_C2_031.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C2_031.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_031.Top = 10.055!
        Me.S49_C2_031.Width = 1.0!
        '
        'S49_C2_011
        '
        Me.S49_C2_011.Height = 0.1875!
        Me.S49_C2_011.HyperLink = Nothing
        Me.S49_C2_011.Left = 2.25!
        Me.S49_C2_011.Name = "S49_C2_011"
        Me.S49_C2_011.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C2_011.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_011.Top = 5.316!
        Me.S49_C2_011.Width = 1.0!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.1875!
        Me.Shape3.Left = 2.25!
        Me.Shape3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape3.Top = 10.055!
        Me.Shape3.Width = 1.0!
        '
        'Label575
        '
        Me.Label575.Height = 0.1875!
        Me.Label575.HyperLink = Nothing
        Me.Label575.Left = 0.25!
        Me.Label575.Name = "Label575"
        Me.Label575.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label575.Text = "CASE 2: VERTICAL REACTIONS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label575.Top = 0!
        Me.Label575.Width = 7.75!
        '
        'Picture7
        '
        Me.Picture7.Height = 3.063!
        Me.Picture7.HyperLink = Nothing
        Me.Picture7.ImageData = CType(resources.GetObject("Picture7.ImageData"), System.IO.Stream)
        Me.Picture7.Left = 0!
        Me.Picture7.Name = "Picture7"
        Me.Picture7.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture7.Top = 0.187!
        Me.Picture7.Width = 8.0!
        '
        'Label576
        '
        Me.Label576.Height = 0.188!
        Me.Label576.HyperLink = Nothing
        Me.Label576.Left = 0.5!
        Me.Label576.Name = "Label576"
        Me.Label576.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label576.Text = "BEAM WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label576.Top = 3.187!
        Me.Label576.Width = 5.0!
        '
        'Label577
        '
        Me.Label577.Height = 0.1875!
        Me.Label577.HyperLink = Nothing
        Me.Label577.Left = 0!
        Me.Label577.Name = "Label577"
        Me.Label577.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label577.Text = "Lb_edge=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label577.Top = 3.568001!
        Me.Label577.Width = 2.25!
        '
        'Label578
        '
        Me.Label578.Height = 0.1875!
        Me.Label578.HyperLink = Nothing
        Me.Label578.Left = 3.25!
        Me.Label578.Name = "Label578"
        Me.Label578.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label578.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label578.Top = 3.568001!
        Me.Label578.Width = 4.75!
        '
        'S49_C2_002
        '
        Me.S49_C2_002.Height = 0.1875!
        Me.S49_C2_002.HyperLink = Nothing
        Me.S49_C2_002.Left = 2.25!
        Me.S49_C2_002.Name = "S49_C2_002"
        Me.S49_C2_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_002.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_002.Top = 3.56897!
        Me.S49_C2_002.Width = 1.0!
        '
        'Label580
        '
        Me.Label580.Height = 0.1875!
        Me.Label580.HyperLink = Nothing
        Me.Label580.Left = 0!
        Me.Label580.Name = "Label580"
        Me.Label580.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label580.Text = "h_bmWeb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label580.Top = 3.755998!
        Me.Label580.Width = 2.25!
        '
        'Label581
        '
        Me.Label581.Height = 0.1875!
        Me.Label581.HyperLink = Nothing
        Me.Label581.Left = 3.25!
        Me.Label581.Name = "Label581"
        Me.Label581.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label581.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (d- (n_Vbolt_SST - 1)*Svert - 2tbf)*0.5 + (n_Vbolt_SST - 1)*Svert)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label581.Top = 3.755998!
        Me.Label581.Width = 4.75!
        '
        'S49_C2_003
        '
        Me.S49_C2_003.Height = 0.1875!
        Me.S49_C2_003.HyperLink = Nothing
        Me.S49_C2_003.Left = 2.25!
        Me.S49_C2_003.Name = "S49_C2_003"
        Me.S49_C2_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_003.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_003.Top = 3.756972!
        Me.S49_C2_003.Width = 1.0!
        '
        'Label583
        '
        Me.Label583.Height = 0.1875!
        Me.Label583.HyperLink = Nothing
        Me.Label583.Left = 0!
        Me.Label583.Name = "Label583"
        Me.Label583.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label583.Text = "Agv_bmWeb1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label583.Top = 3.943001!
        Me.Label583.Width = 2.25!
        '
        'Label584
        '
        Me.Label584.Height = 0.1875!
        Me.Label584.HyperLink = Nothing
        Me.Label584.Left = 3.25!
        Me.Label584.Name = "Label584"
        Me.Label584.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label584.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= tbw* h_bmWeb" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label584.Top = 3.943001!
        Me.Label584.Width = 4.75!
        '
        'S49_C2_004
        '
        Me.S49_C2_004.Height = 0.1875!
        Me.S49_C2_004.HyperLink = Nothing
        Me.S49_C2_004.Left = 2.25!
        Me.S49_C2_004.Name = "S49_C2_004"
        Me.S49_C2_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_004.Text = "9.0"
        Me.S49_C2_004.Top = 3.94397!
        Me.S49_C2_004.Width = 1.0!
        '
        'Label586
        '
        Me.Label586.Height = 0.1875!
        Me.Label586.HyperLink = Nothing
        Me.Label586.Left = 0!
        Me.Label586.Name = "Label586"
        Me.Label586.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label586.Text = "Anv_bmWeb1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label586.Top = 4.129999!
        Me.Label586.Width = 2.25!
        '
        'Label587
        '
        Me.Label587.Height = 0.1875!
        Me.Label587.HyperLink = Nothing
        Me.Label587.Left = 3.25!
        Me.Label587.Name = "Label587"
        Me.Label587.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label587.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= tbw* (h_bmWeb - (n_Vbolt_SST-0.5)* dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label587.Top = 4.129999!
        Me.Label587.Width = 4.75!
        '
        'S49_C2_005
        '
        Me.S49_C2_005.Height = 0.1875!
        Me.S49_C2_005.HyperLink = Nothing
        Me.S49_C2_005.Left = 2.25!
        Me.S49_C2_005.Name = "S49_C2_005"
        Me.S49_C2_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_005.Text = "9.0"
        Me.S49_C2_005.Top = 4.130973!
        Me.S49_C2_005.Width = 1.0!
        '
        'Label589
        '
        Me.Label589.Height = 0.1875!
        Me.Label589.HyperLink = Nothing
        Me.Label589.Left = 0!
        Me.Label589.Name = "Label589"
        Me.Label589.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label589.Text = "Ant_bmWeb1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label589.Top = 4.316999!
        Me.Label589.Width = 2.25!
        '
        'Label590
        '
        Me.Label590.Height = 0.1875!
        Me.Label590.HyperLink = Nothing
        Me.Label590.Left = 3.25!
        Me.Label590.Name = "Label590"
        Me.Label590.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label590.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= (Lb_edge - dHole_sp/2)* tbw" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label590.Top = 4.316999!
        Me.Label590.Width = 4.75!
        '
        'S49_C2_006
        '
        Me.S49_C2_006.Height = 0.1875!
        Me.S49_C2_006.HyperLink = Nothing
        Me.S49_C2_006.Left = 2.25!
        Me.S49_C2_006.Name = "S49_C2_006"
        Me.S49_C2_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_006.Text = "9.0"
        Me.S49_C2_006.Top = 4.317971!
        Me.S49_C2_006.Width = 1.0!
        '
        'Label592
        '
        Me.Label592.Height = 0.1875!
        Me.Label592.HyperLink = Nothing
        Me.Label592.Left = 0!
        Me.Label592.Name = "Label592"
        Me.Label592.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label592.Text = "Rn_bmWebVert1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label592.Top = 4.561!
        Me.Label592.Width = 2.25!
        '
        'Label593
        '
        Me.Label593.Height = 0.1875!
        Me.Label593.HyperLink = Nothing
        Me.Label593.Left = 3.25!
        Me.Label593.Name = "Label593"
        Me.Label593.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label593.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_bmWeb* Anv_bmWeb1 + Ubs*Fu_bmWeb* Ant_bmWeb1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label593.Top = 4.561!
        Me.Label593.Width = 4.75!
        '
        'S49_C2_007
        '
        Me.S49_C2_007.Height = 0.1875!
        Me.S49_C2_007.HyperLink = Nothing
        Me.S49_C2_007.Left = 2.25!
        Me.S49_C2_007.Name = "S49_C2_007"
        Me.S49_C2_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_007.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_007.Top = 4.561974!
        Me.S49_C2_007.Width = 1.0!
        '
        'Label595
        '
        Me.Label595.Height = 0.1875!
        Me.Label595.HyperLink = Nothing
        Me.Label595.Left = 0!
        Me.Label595.Name = "Label595"
        Me.Label595.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label595.Text = "Rn_bmWebVert2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label595.Top = 4.749002!
        Me.Label595.Width = 2.25!
        '
        'Label596
        '
        Me.Label596.Height = 0.1875!
        Me.Label596.HyperLink = Nothing
        Me.Label596.Left = 3.25!
        Me.Label596.Name = "Label596"
        Me.Label596.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label596.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_bmWeb* Agv_bmWeb1 + Ubs*Fu_bmWeb* Ant_bmWeb1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label596.Top = 4.749002!
        Me.Label596.Width = 4.75!
        '
        'S49_C2_008
        '
        Me.S49_C2_008.Height = 0.1875!
        Me.S49_C2_008.HyperLink = Nothing
        Me.S49_C2_008.Left = 2.25!
        Me.S49_C2_008.Name = "S49_C2_008"
        Me.S49_C2_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_008.Text = "9.0"
        Me.S49_C2_008.Top = 4.75005!
        Me.S49_C2_008.Width = 1.0!
        '
        'Label601
        '
        Me.Label601.Height = 0.1875!
        Me.Label601.HyperLink = Nothing
        Me.Label601.Left = 0!
        Me.Label601.Name = "Label601"
        Me.Label601.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label601.Text = "ΦRn_bmWebVert=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label601.Top = 4.938002!
        Me.Label601.Width = 2.25!
        '
        'Label602
        '
        Me.Label602.Height = 0.1875!
        Me.Label602.HyperLink = Nothing
        Me.Label602.Left = 3.25!
        Me.Label602.Name = "Label602"
        Me.Label602.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label602.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_bmWebVert1, Rn_bmWebVert2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label602.Top = 4.938002!
        Me.Label602.Width = 4.75!
        '
        'S49_C2_009
        '
        Me.S49_C2_009.Height = 0.1875!
        Me.S49_C2_009.HyperLink = Nothing
        Me.S49_C2_009.Left = 2.25!
        Me.S49_C2_009.Name = "S49_C2_009"
        Me.S49_C2_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_009.Text = "9.0"
        Me.S49_C2_009.Top = 4.938972!
        Me.S49_C2_009.Width = 1.0!
        '
        'Label604
        '
        Me.Label604.Height = 0.1875!
        Me.Label604.HyperLink = Nothing
        Me.Label604.Left = 0!
        Me.Label604.Name = "Label604"
        Me.Label604.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label604.Text = "SUM_bmWebVert_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label604.Top = 5.128001!
        Me.Label604.Width = 2.25!
        '
        'Label605
        '
        Me.Label605.Height = 0.1875!
        Me.Label605.HyperLink = Nothing
        Me.Label605.Left = 3.75!
        Me.Label605.Name = "Label605"
        Me.Label605.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label605.Text = "OK if SUM_bmWebVert_BS <= SUM_Allowed"
        Me.Label605.Top = 5.315028!
        Me.Label605.Width = 4.25!
        '
        'Label607
        '
        Me.Label607.Height = 0.1875!
        Me.Label607.HyperLink = Nothing
        Me.Label607.Left = 3.25!
        Me.Label607.Name = "Label607"
        Me.Label607.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label607.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm/ ΦRn_bmWebVert" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label607.Top = 5.125992!
        Me.Label607.Width = 4.75!
        '
        'S49_C2_010
        '
        Me.S49_C2_010.Height = 0.1875!
        Me.S49_C2_010.HyperLink = Nothing
        Me.S49_C2_010.Left = 2.25!
        Me.S49_C2_010.Name = "S49_C2_010"
        Me.S49_C2_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_010.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_010.Top = 5.126959!
        Me.S49_C2_010.Width = 1.0!
        '
        'Label609
        '
        Me.Label609.Height = 0.1875!
        Me.Label609.HyperLink = Nothing
        Me.Label609.Left = 0!
        Me.Label609.Name = "Label609"
        Me.Label609.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label609.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label609.Top = 5.315028!
        Me.Label609.Width = 2.25!
        '
        'Label610
        '
        Me.Label610.Height = 0.1875!
        Me.Label610.HyperLink = Nothing
        Me.Label610.Left = 0!
        Me.Label610.Name = "Label610"
        Me.Label610.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label610.Text = "S_vert=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label610.Top = 3.380999!
        Me.Label610.Width = 2.25!
        '
        'Label611
        '
        Me.Label611.Height = 0.1875!
        Me.Label611.HyperLink = Nothing
        Me.Label611.Left = 3.25!
        Me.Label611.Name = "Label611"
        Me.Label611.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label611.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Svert" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label611.Top = 3.380999!
        Me.Label611.Width = 4.75!
        '
        'S49_C2_001
        '
        Me.S49_C2_001.Height = 0.1875!
        Me.S49_C2_001.HyperLink = Nothing
        Me.S49_C2_001.Left = 2.25!
        Me.S49_C2_001.Name = "S49_C2_001"
        Me.S49_C2_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_001.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_001.Top = 3.380999!
        Me.S49_C2_001.Width = 1.0!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.5!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label2.Text = "SHEAR PLATE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 5.503001!
        Me.Label2.Width = 7.75!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label20.Text = "hsp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label20.Top = 5.877003!
        Me.Label20.Width = 2.25!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 3.25!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label22.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label22.Top = 5.877003!
        Me.Label22.Width = 4.75!
        '
        'S49_C2_012
        '
        Me.S49_C2_012.Height = 0.1875!
        Me.S49_C2_012.HyperLink = Nothing
        Me.S49_C2_012.Left = 2.25!
        Me.S49_C2_012.Name = "S49_C2_012"
        Me.S49_C2_012.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_012.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_012.Top = 5.877974!
        Me.S49_C2_012.Width = 1.0!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label29.Text = "Agv_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label29.Top = 6.065029!
        Me.Label29.Width = 2.25!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 3.25!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label32.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= hsp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label32.Top = 6.065029!
        Me.Label32.Width = 4.75!
        '
        'S49_C2_013
        '
        Me.S49_C2_013.Height = 0.1875!
        Me.S49_C2_013.HyperLink = Nothing
        Me.S49_C2_013.Left = 2.25!
        Me.S49_C2_013.Name = "S49_C2_013"
        Me.S49_C2_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_013.Text = "9.0"
        Me.S49_C2_013.Top = 6.066!
        Me.S49_C2_013.Width = 1.0!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 0!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label38.Text = "Anv_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label38.Top = 6.253062!
        Me.Label38.Width = 2.25!
        '
        'Label41
        '
        Me.Label41.Height = 0.1875!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 3.25!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label41.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= tsp* (hsp - (n_Vbolt_SST-0.5)* dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label41.Top = 6.253062!
        Me.Label41.Width = 4.75!
        '
        'S49_C2_014
        '
        Me.S49_C2_014.Height = 0.1875!
        Me.S49_C2_014.HyperLink = Nothing
        Me.S49_C2_014.Left = 2.25!
        Me.S49_C2_014.Name = "S49_C2_014"
        Me.S49_C2_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_014.Text = "9.0"
        Me.S49_C2_014.Top = 6.254035!
        Me.S49_C2_014.Width = 1.0!
        '
        'Label47
        '
        Me.Label47.Height = 0.1875!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 0!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label47.Text = "Ant_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label47.Top = 6.442036!
        Me.Label47.Width = 2.25!
        '
        'Label50
        '
        Me.Label50.Height = 0.1875!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 3.25!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label50.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= (Lsp_edge - dHole_sp/2)* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label50.Top = 6.442036!
        Me.Label50.Width = 4.75!
        '
        'S49_C2_015
        '
        Me.S49_C2_015.Height = 0.1875!
        Me.S49_C2_015.HyperLink = Nothing
        Me.S49_C2_015.Left = 2.25!
        Me.S49_C2_015.Name = "S49_C2_015"
        Me.S49_C2_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_015.Text = "9.0"
        Me.S49_C2_015.Top = 6.442999!
        Me.S49_C2_015.Width = 1.0!
        '
        'Label56
        '
        Me.Label56.Height = 0.1875!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 0!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label56.Text = "Rn_spVert1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label56.Top = 6.630084!
        Me.Label56.Width = 2.25!
        '
        'Label59
        '
        Me.Label59.Height = 0.1875!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 3.25!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label59.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_shearTab* Anv_sp1 + Ubs* Fu_shearTab* Ant_sp1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label59.Top = 6.630084!
        Me.Label59.Width = 4.75!
        '
        'S49_C2_016
        '
        Me.S49_C2_016.Height = 0.1875!
        Me.S49_C2_016.HyperLink = Nothing
        Me.S49_C2_016.Left = 2.25!
        Me.S49_C2_016.Name = "S49_C2_016"
        Me.S49_C2_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_016.Text = "9.0"
        Me.S49_C2_016.Top = 6.631049!
        Me.S49_C2_016.Width = 1.0!
        '
        'Label65
        '
        Me.Label65.Height = 0.1875!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 0!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label65.Text = "Rn_spVert2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label65.Top = 6.819035!
        Me.Label65.Width = 2.25!
        '
        'Label68
        '
        Me.Label68.Height = 0.1875!
        Me.Label68.HyperLink = Nothing
        Me.Label68.Left = 3.25!
        Me.Label68.Name = "Label68"
        Me.Label68.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label68.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_shearTab* Agv_sp1 + Ubs* Fu_shearTab* Ant_sp1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label68.Top = 6.819035!
        Me.Label68.Width = 4.75!
        '
        'S49_C2_017
        '
        Me.S49_C2_017.Height = 0.1875!
        Me.S49_C2_017.HyperLink = Nothing
        Me.S49_C2_017.Left = 2.25!
        Me.S49_C2_017.Name = "S49_C2_017"
        Me.S49_C2_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_017.Text = "9.0"
        Me.S49_C2_017.Top = 6.82!
        Me.S49_C2_017.Width = 1.0!
        '
        'Label71
        '
        Me.Label71.Height = 0.1875!
        Me.Label71.HyperLink = Nothing
        Me.Label71.Left = 0!
        Me.Label71.Name = "Label71"
        Me.Label71.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label71.Text = "ΦRn_spVert=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label71.Top = 7.007014!
        Me.Label71.Width = 2.25!
        '
        'Label75
        '
        Me.Label75.Height = 0.1875!
        Me.Label75.HyperLink = Nothing
        Me.Label75.Left = 3.25!
        Me.Label75.Name = "Label75"
        Me.Label75.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label75.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_spVert1, Rn_spVert2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label75.Top = 7.007014!
        Me.Label75.Width = 4.75!
        '
        'S49_C2_018
        '
        Me.S49_C2_018.Height = 0.1875!
        Me.S49_C2_018.HyperLink = Nothing
        Me.S49_C2_018.Left = 2.25!
        Me.S49_C2_018.Name = "S49_C2_018"
        Me.S49_C2_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_018.Text = "9.0"
        Me.S49_C2_018.Top = 7.007992!
        Me.S49_C2_018.Width = 1.0!
        '
        'Label81
        '
        Me.Label81.Height = 0.1875!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 0!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label81.Text = "SUM_spVert_BS1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label81.Top = 7.193062!
        Me.Label81.Width = 2.25!
        '
        'Label84
        '
        Me.Label84.Height = 0.1875!
        Me.Label84.HyperLink = Nothing
        Me.Label84.Left = 3.25!
        Me.Label84.Name = "Label84"
        Me.Label84.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label84.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm/ ΦRn_spVert" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label84.Top = 7.193062!
        Me.Label84.Width = 4.75!
        '
        'S49_C2_019
        '
        Me.S49_C2_019.Height = 0.1875!
        Me.S49_C2_019.HyperLink = Nothing
        Me.S49_C2_019.Left = 2.25!
        Me.S49_C2_019.Name = "S49_C2_019"
        Me.S49_C2_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_019.Text = "9.0"
        Me.S49_C2_019.Top = 7.194035!
        Me.S49_C2_019.Width = 1.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label1.Text = "CASE 1:"
        Me.Label1.Top = 5.690001!
        Me.Label1.Width = 2.25!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label4.Text = "hsp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 7.950001!
        Me.Label4.Width = 2.25!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.25!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label6.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label6.Top = 7.950001!
        Me.Label6.Width = 4.75!
        '
        'S49_C2_021
        '
        Me.S49_C2_021.Height = 0.1875!
        Me.S49_C2_021.HyperLink = Nothing
        Me.S49_C2_021.Left = 2.25!
        Me.S49_C2_021.Name = "S49_C2_021"
        Me.S49_C2_021.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_021.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_021.Top = 7.950982!
        Me.S49_C2_021.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label9.Text = "Lslot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 8.13796!
        Me.Label9.Width = 2.25!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 3.25!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label10.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 8.13796!
        Me.Label10.Width = 4.75!
        '
        'S49_C2_022
        '
        Me.S49_C2_022.Height = 0.1875!
        Me.S49_C2_022.HyperLink = Nothing
        Me.S49_C2_022.Left = 2.25!
        Me.S49_C2_022.Name = "S49_C2_022"
        Me.S49_C2_022.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_022.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_022.Top = 8.138931!
        Me.S49_C2_022.Width = 1.0!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label12.Text = "Lv_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label12.Top = 7.761029!
        Me.Label12.Width = 2.25!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 3.25!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label13.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label13.Top = 7.761029!
        Me.Label13.Width = 4.75!
        '
        'S49_C2_020
        '
        Me.S49_C2_020.Height = 0.1875!
        Me.S49_C2_020.HyperLink = Nothing
        Me.S49_C2_020.Left = 2.25!
        Me.S49_C2_020.Name = "S49_C2_020"
        Me.S49_C2_020.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C2_020.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C2_020.Top = 7.762002!
        Me.S49_C2_020.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "Agv_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label16.Top = 8.326031!
        Me.Label16.Width = 2.25!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 3.25!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label18.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= hsp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 8.326031!
        Me.Label18.Width = 4.75!
        '
        'S49_C2_023
        '
        Me.S49_C2_023.Height = 0.1875!
        Me.S49_C2_023.HyperLink = Nothing
        Me.S49_C2_023.Left = 2.25!
        Me.S49_C2_023.Name = "S49_C2_023"
        Me.S49_C2_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_023.Text = "9.0"
        Me.S49_C2_023.Top = 8.327!
        Me.S49_C2_023.Width = 1.0!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label21.Text = "Anv_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label21.Top = 8.514061!
        Me.Label21.Width = 2.25!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 3.25!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label23.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= tsp* (hsp - (n_Vbolt_SST-0.5)* dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label23.Top = 8.514061!
        Me.Label23.Width = 4.75!
        '
        'S49_C2_024
        '
        Me.S49_C2_024.Height = 0.1875!
        Me.S49_C2_024.HyperLink = Nothing
        Me.S49_C2_024.Left = 2.25!
        Me.S49_C2_024.Name = "S49_C2_024"
        Me.S49_C2_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_024.Text = "9.0"
        Me.S49_C2_024.Top = 8.515041!
        Me.S49_C2_024.Width = 1.0!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label25.Text = "Ant_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label25.Top = 8.703041!
        Me.Label25.Width = 2.25!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 3.25!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label26.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= (W_sp - a - LslotH/2)* tsp"
        Me.Label26.Top = 8.703041!
        Me.Label26.Width = 4.75!
        '
        'S49_C2_025
        '
        Me.S49_C2_025.Height = 0.1875!
        Me.S49_C2_025.HyperLink = Nothing
        Me.S49_C2_025.Left = 2.25!
        Me.S49_C2_025.Name = "S49_C2_025"
        Me.S49_C2_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_025.Text = "9.0"
        Me.S49_C2_025.Top = 8.704!
        Me.S49_C2_025.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 0!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label28.Text = "Rn_spVert1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label28.Top = 8.891081!
        Me.Label28.Width = 2.25!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 3.25!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label30.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_shearTab* Anv_sp1 + Ubs* Fu_shearTab* Ant_sp1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label30.Top = 8.891081!
        Me.Label30.Width = 4.75!
        '
        'S49_C2_026
        '
        Me.S49_C2_026.Height = 0.1875!
        Me.S49_C2_026.HyperLink = Nothing
        Me.S49_C2_026.Left = 2.25!
        Me.S49_C2_026.Name = "S49_C2_026"
        Me.S49_C2_026.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_026.Text = "9.0"
        Me.S49_C2_026.Top = 8.89205!
        Me.S49_C2_026.Width = 1.0!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 0!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label33.Text = "Rn_spVert2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label33.Top = 9.08004!
        Me.Label33.Width = 2.25!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 3.25!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label34.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_shearTab* Agv_sp1 + Ubs* Fu_shearTab* Ant_sp1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label34.Top = 9.08004!
        Me.Label34.Width = 4.75!
        '
        'S49_C2_027
        '
        Me.S49_C2_027.Height = 0.1875!
        Me.S49_C2_027.HyperLink = Nothing
        Me.S49_C2_027.Left = 2.25!
        Me.S49_C2_027.Name = "S49_C2_027"
        Me.S49_C2_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_027.Text = "9.0"
        Me.S49_C2_027.Top = 9.081001!
        Me.S49_C2_027.Width = 1.0!
        '
        'Label36
        '
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 0!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label36.Text = "ΦRn_spVert=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label36.Top = 9.268011!
        Me.Label36.Width = 2.25!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 3.25!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label37.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_spVert1, Rn_spVert2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label37.Top = 9.268011!
        Me.Label37.Width = 4.75!
        '
        'S49_C2_028
        '
        Me.S49_C2_028.Height = 0.1875!
        Me.S49_C2_028.HyperLink = Nothing
        Me.S49_C2_028.Left = 2.25!
        Me.S49_C2_028.Name = "S49_C2_028"
        Me.S49_C2_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_028.Text = "9.0"
        Me.S49_C2_028.Top = 9.26899!
        Me.S49_C2_028.Width = 1.0!
        '
        'Label40
        '
        Me.Label40.Height = 0.1875!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 0!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label40.Text = "SUM_spVert_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label40.Top = 9.45406!
        Me.Label40.Width = 2.25!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 3.25!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label42.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm/ ΦRn_spVert" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label42.Top = 9.45406!
        Me.Label42.Width = 4.75!
        '
        'S49_C2_029
        '
        Me.S49_C2_029.Height = 0.1875!
        Me.S49_C2_029.HyperLink = Nothing
        Me.S49_C2_029.Left = 2.25!
        Me.S49_C2_029.Name = "S49_C2_029"
        Me.S49_C2_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_029.Text = "9.0"
        Me.S49_C2_029.Top = 9.45504!
        Me.S49_C2_029.Width = 1.0!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 0!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label46.Text = "CASE 2:"
        Me.Label46.Top = 7.574028!
        Me.Label46.Width = 2.25!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "SUM_spVert_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 9.867031!
        Me.Label7.Width = 2.25!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 3.25!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label15.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= MAX( SUM_spVert_BS1, SUM_spVert_BS1)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label15.Top = 9.867031!
        Me.Label15.Width = 4.75!
        '
        'S49_C2_030
        '
        Me.S49_C2_030.Height = 0.1875!
        Me.S49_C2_030.HyperLink = Nothing
        Me.S49_C2_030.Left = 2.25!
        Me.S49_C2_030.Name = "S49_C2_030"
        Me.S49_C2_030.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C2_030.Text = "9.0"
        Me.S49_C2_030.Top = 9.86801!
        Me.S49_C2_030.Width = 1.0!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 0!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label48.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label48.Top = 10.06001!
        Me.Label48.Width = 2.25!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 3.25!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label49.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= OK if SUM_spVert_BS <= SUM_spVert_BS_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label49.Top = 10.06001!
        Me.Label49.Width = 4.75!
        '
        'Shape4
        '
        Me.Shape4.Height = 0.1875!
        Me.Shape4.Left = 2.25!
        Me.Shape4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape4.Name = "Shape4"
        Me.Shape4.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape4.Top = 10.05904!
        Me.Shape4.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.25!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 5.316!
        Me.Shape1.Width = 1.0!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.2!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(7.75!, 0.2!)
        Me.PageBreak1.Top = 0!
        Me.PageBreak1.Width = 7.75!
        '
        'US_SPC6
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.75!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S49_C2_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label575, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label576, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label577, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label578, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label580, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label581, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label583, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label584, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label586, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label587, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label589, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label590, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label592, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label593, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label595, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label596, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label601, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label602, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label604, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label605, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label607, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label609, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label610, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label611, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C2_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label575 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture7 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents S49_C2_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label576 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label577 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label578 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label580 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label581 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label583 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label584 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label586 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label587 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label589 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label590 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label592 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label593 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label595 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label596 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label601 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label602 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label604 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label605 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label607 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label609 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label610 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label611 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label47 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label50 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label56 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label65 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label68 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label71 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label75 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label81 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label84 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape3 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C2_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label48 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape4 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak

    '

End Class
