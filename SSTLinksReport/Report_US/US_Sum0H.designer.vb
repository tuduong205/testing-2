﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_Sum0H
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_Sum0H))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.SubReport1 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.PageBreak2 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport2 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.PageBreak3 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport3 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.PageBreak4 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport4 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.PageBreak5 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport5 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.PageBreak6 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport6 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.PageBreak7 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport7 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.PageBreak8 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport8 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.lbVersion = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.PageHeader1 = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.Picture = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Line = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.RichTextBox = New GrapeCity.ActiveReports.SectionReportModel.RichTextBox()
        Me.Label = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.PageFooter1 = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        CType(Me.lbVersion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.SubReport1, Me.PageBreak2, Me.SubReport2, Me.PageBreak3, Me.SubReport3, Me.PageBreak4, Me.SubReport4, Me.PageBreak5, Me.SubReport5, Me.PageBreak6, Me.SubReport6, Me.PageBreak7, Me.SubReport7, Me.PageBreak8, Me.SubReport8})
        Me.Detail.Height = 9.75!
        Me.Detail.Name = "Detail"
        '
        'SubReport1
        '
        Me.SubReport1.CloseBorder = False
        Me.SubReport1.Height = 0.4375!
        Me.SubReport1.Left = 0.5!
        Me.SubReport1.Name = "SubReport1"
        Me.SubReport1.Report = Nothing
        Me.SubReport1.Top = 0!
        Me.SubReport1.Width = 10.0!
        '
        'PageBreak2
        '
        Me.PageBreak2.Height = 0.01!
        Me.PageBreak2.Left = 0!
        Me.PageBreak2.Name = "PageBreak2"
        Me.PageBreak2.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak2.Top = 0.5!
        Me.PageBreak2.Width = 6.5!
        '
        'SubReport2
        '
        Me.SubReport2.CloseBorder = False
        Me.SubReport2.Height = 0.4375!
        Me.SubReport2.Left = 0.5!
        Me.SubReport2.Name = "SubReport2"
        Me.SubReport2.Report = Nothing
        Me.SubReport2.Top = 0.5!
        Me.SubReport2.Width = 10.0!
        '
        'PageBreak3
        '
        Me.PageBreak3.Height = 0.01!
        Me.PageBreak3.Left = 0!
        Me.PageBreak3.Name = "PageBreak3"
        Me.PageBreak3.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak3.Top = 1.0!
        Me.PageBreak3.Width = 6.5!
        '
        'SubReport3
        '
        Me.SubReport3.CloseBorder = False
        Me.SubReport3.Height = 0.4375!
        Me.SubReport3.Left = 0.5!
        Me.SubReport3.Name = "SubReport3"
        Me.SubReport3.Report = Nothing
        Me.SubReport3.Top = 1.0!
        Me.SubReport3.Width = 10.0!
        '
        'PageBreak4
        '
        Me.PageBreak4.Height = 0.01!
        Me.PageBreak4.Left = 0!
        Me.PageBreak4.Name = "PageBreak4"
        Me.PageBreak4.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak4.Top = 1.5!
        Me.PageBreak4.Width = 6.5!
        '
        'SubReport4
        '
        Me.SubReport4.CloseBorder = False
        Me.SubReport4.Height = 0.4375!
        Me.SubReport4.Left = 0.5!
        Me.SubReport4.Name = "SubReport4"
        Me.SubReport4.Report = Nothing
        Me.SubReport4.Top = 1.5!
        Me.SubReport4.Width = 10.0!
        '
        'PageBreak5
        '
        Me.PageBreak5.Height = 0.01!
        Me.PageBreak5.Left = 0!
        Me.PageBreak5.Name = "PageBreak5"
        Me.PageBreak5.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak5.Top = 2.0!
        Me.PageBreak5.Width = 6.5!
        '
        'SubReport5
        '
        Me.SubReport5.CloseBorder = False
        Me.SubReport5.Height = 0.4375!
        Me.SubReport5.Left = 0.5!
        Me.SubReport5.Name = "SubReport5"
        Me.SubReport5.Report = Nothing
        Me.SubReport5.Top = 2.0!
        Me.SubReport5.Width = 10.0!
        '
        'PageBreak6
        '
        Me.PageBreak6.Height = 0.01!
        Me.PageBreak6.Left = 0!
        Me.PageBreak6.Name = "PageBreak6"
        Me.PageBreak6.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak6.Top = 2.5!
        Me.PageBreak6.Width = 6.5!
        '
        'SubReport6
        '
        Me.SubReport6.CloseBorder = False
        Me.SubReport6.Height = 0.4375!
        Me.SubReport6.Left = 0.5!
        Me.SubReport6.Name = "SubReport6"
        Me.SubReport6.Report = Nothing
        Me.SubReport6.Top = 2.5!
        Me.SubReport6.Width = 10.0!
        '
        'PageBreak7
        '
        Me.PageBreak7.Height = 0.01!
        Me.PageBreak7.Left = 0!
        Me.PageBreak7.Name = "PageBreak7"
        Me.PageBreak7.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak7.Top = 3.0!
        Me.PageBreak7.Width = 6.5!
        '
        'SubReport7
        '
        Me.SubReport7.CloseBorder = False
        Me.SubReport7.Height = 0.4375!
        Me.SubReport7.Left = 0.5!
        Me.SubReport7.Name = "SubReport7"
        Me.SubReport7.Report = Nothing
        Me.SubReport7.Top = 3.0!
        Me.SubReport7.Width = 10.0!
        '
        'PageBreak8
        '
        Me.PageBreak8.Height = 0.01!
        Me.PageBreak8.Left = 0!
        Me.PageBreak8.Name = "PageBreak8"
        Me.PageBreak8.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak8.Top = 3.5!
        Me.PageBreak8.Width = 6.5!
        '
        'SubReport8
        '
        Me.SubReport8.CloseBorder = False
        Me.SubReport8.Height = 0.4375!
        Me.SubReport8.Left = 0.5!
        Me.SubReport8.Name = "SubReport8"
        Me.SubReport8.Report = Nothing
        Me.SubReport8.Top = 3.5!
        Me.SubReport8.Width = 10.0!
        '
        'lbVersion
        '
        Me.lbVersion.Height = 0.188!
        Me.lbVersion.HyperLink = Nothing
        Me.lbVersion.Left = 0.5!
        Me.lbVersion.Name = "lbVersion"
        Me.lbVersion.Style = "font-size: 8.25pt; ddo-char-set: 1"
        Me.lbVersion.Text = "Version ID"
        Me.lbVersion.Top = 0.013!
        Me.lbVersion.Width = 2.75!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 0.5!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 0!
        Me.Line5.Width = 10.0!
        Me.Line5.X1 = 0.5!
        Me.Line5.X2 = 10.5!
        Me.Line5.Y1 = 0!
        Me.Line5.Y2 = 0!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Picture, Me.Line, Me.RichTextBox, Me.Label, Me.Label1, Me.label6})
        Me.PageHeader1.Height = 1.25!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Picture
        '
        Me.Picture.Height = 1.005!
        Me.Picture.HyperLink = Nothing
        Me.Picture.ImageData = CType(resources.GetObject("Picture.ImageData"), System.IO.Stream)
        Me.Picture.Left = 0.5!
        Me.Picture.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Picture.Name = "Picture"
        Me.Picture.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture.Top = 0.25!
        Me.Picture.Width = 1.3125!
        '
        'Line
        '
        Me.Line.Height = 0!
        Me.Line.Left = 0.5!
        Me.Line.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 1.25!
        Me.Line.Width = 10.0!
        Me.Line.X1 = 0.5!
        Me.Line.X2 = 10.5!
        Me.Line.Y1 = 1.25!
        Me.Line.Y2 = 1.25!
        '
        'RichTextBox
        '
        Me.RichTextBox.AutoReplaceFields = True
        Me.RichTextBox.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.RichTextBox.Height = 0.1875!
        Me.RichTextBox.Left = 1.937!
        Me.RichTextBox.Name = "RichTextBox"
        Me.RichTextBox.RTF = resources.GetString("RichTextBox.RTF")
        Me.RichTextBox.Top = 0.312!
        Me.RichTextBox.Width = 3.6875!
        '
        'Label
        '
        Me.Label.Height = 0.2!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 1.937!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.Label.Text = "5956 W. Las Positas Blvd.,  Pleasanton, CA 94588."
        Me.Label.Top = 0.562!
        Me.Label.Width = 3.625!
        '
        'Label1
        '
        Me.Label1.Height = 0.2!
        Me.Label1.HyperLink = ""
        Me.Label1.Left = 1.937!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "text-decoration: underline; ddo-char-set: 0"
        Me.Label1.Text = "www.strongtie.com"
        Me.Label1.Top = 0.937!
        Me.Label1.Width = 1.5!
        '
        'label6
        '
        Me.label6.Height = 0.2!
        Me.label6.HyperLink = Nothing
        Me.label6.Left = 1.937!
        Me.label6.Name = "label6"
        Me.label6.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.label6.Text = "(800) 999-5099"
        Me.label6.Top = 0.7495!
        Me.label6.Width = 3.625!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.lbVersion, Me.Line5})
        Me.PageFooter1.Height = 0.5!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'SR_Sum0H
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 11.0!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        Me.WatermarkAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft
        CType(Me.lbVersion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents SR_LineInfo_OnPage As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents lbVersion As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents PageHeader1 As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents PageFooter1 As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    Private WithEvents Picture As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Line As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents RichTextBox As GrapeCity.ActiveReports.SectionReportModel.RichTextBox
    Private WithEvents Label As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SubReport1 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak2 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport2 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak3 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport3 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak4 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport4 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak5 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport5 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak6 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport6 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak7 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport7 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak8 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport8 As GrapeCity.ActiveReports.SectionReportModel.SubReport
End Class
