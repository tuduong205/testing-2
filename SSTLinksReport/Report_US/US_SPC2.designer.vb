﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_SPC2
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_SPC2))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S43_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label89 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture1 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label90 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label92 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label99 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label101 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label102 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label104 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label105 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label107 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label108 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label110 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label112 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label114 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label116 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label118 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label119 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label121 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label122 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label124 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label125 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label127 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label128 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label130 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label133 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label135 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label136 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label138 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label140 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label141 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label143 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label144 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label146 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label148 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label150 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label151 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label153 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label154 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label156 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label158 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label163 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label168 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label159 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label160 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S44_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label165 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S44_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label169 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label164 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S44_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label171 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label172 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S44_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label174 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label175 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label176 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label178 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label179 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S44_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S44_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape3 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S45_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label181 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label182 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S45_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label184 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S45_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label186 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label187 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S45_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label189 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label190 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label191 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label192 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label193 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S45_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape4 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S43_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape5 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape6 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape7 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        CType(Me.S43_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label138, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label140, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label143, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label150, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label153, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label168, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label159, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S44_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label165, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S44_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label169, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S44_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label171, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label172, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S44_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label174, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label175, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label176, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label178, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S44_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S44_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S45_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label181, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label182, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S45_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label184, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S45_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label186, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label187, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S45_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label189, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label190, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label192, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S45_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S43_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S43_021, Me.S43_023, Me.S43_022, Me.S43_024, Me.S43_020, Me.S45_005, Me.S44_006, Me.Label89, Me.Picture1, Me.Label90, Me.S43_001, Me.Label92, Me.Label99, Me.S43_002, Me.Label101, Me.Label102, Me.S43_003, Me.Label104, Me.Label105, Me.S43_004, Me.Label107, Me.Label108, Me.S43_006, Me.Label110, Me.Label112, Me.S43_007, Me.Label114, Me.Label116, Me.S43_008, Me.Label118, Me.Label119, Me.S43_009, Me.Label121, Me.Label122, Me.S43_010, Me.Label124, Me.Label125, Me.S43_011, Me.Label127, Me.Label128, Me.S43_012, Me.Label130, Me.Label133, Me.S43_013, Me.Label135, Me.Label136, Me.S43_014, Me.Label138, Me.Label140, Me.Label141, Me.S43_015, Me.Label143, Me.Label144, Me.S43_016, Me.Label146, Me.Label148, Me.S43_017, Me.Label150, Me.Label151, Me.S43_018, Me.Label153, Me.Label154, Me.S43_019, Me.Label156, Me.Label158, Me.Label163, Me.Label168, Me.Label159, Me.Label160, Me.S44_001, Me.Label165, Me.S44_002, Me.Label169, Me.Label164, Me.S44_003, Me.Label171, Me.Label172, Me.S44_004, Me.Label174, Me.Label175, Me.Label176, Me.Label178, Me.Label179, Me.S44_005, Me.Label181, Me.Label182, Me.S45_001, Me.Label184, Me.S45_002, Me.Label186, Me.Label187, Me.S45_003, Me.Label189, Me.Label190, Me.Label191, Me.Label192, Me.Label193, Me.S45_004, Me.Label1, Me.S43_005, Me.Label3, Me.Label2, Me.Shape4, Me.Shape3, Me.Shape2, Me.Shape7, Me.Shape6, Me.Shape5, Me.Shape1})
        Me.Detail.Height = 9.25!
        Me.Detail.Name = "Detail"
        '
        'S43_020
        '
        Me.S43_020.Height = 0.1875!
        Me.S43_020.HyperLink = Nothing
        Me.S43_020.Left = 2.25!
        Me.S43_020.Name = "S43_020"
        Me.S43_020.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S43_020.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_020.Top = 6.064!
        Me.S43_020.Width = 1.0!
        '
        'Label89
        '
        Me.Label89.Height = 0.1875!
        Me.Label89.HyperLink = Nothing
        Me.Label89.Left = 0!
        Me.Label89.Name = "Label89"
        Me.Label89.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label89.Text = "4.3 SHEAR PLATE GEOMETRY:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label89.Top = 0!
        Me.Label89.Width = 3.0!
        '
        'Picture1
        '
        Me.Picture1.Height = 2.25!
        Me.Picture1.HyperLink = Nothing
        Me.Picture1.ImageData = CType(resources.GetObject("Picture1.ImageData"), System.IO.Stream)
        Me.Picture1.Left = 2.25!
        Me.Picture1.Name = "Picture1"
        Me.Picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture1.Top = 0.187!
        Me.Picture1.Width = 2.437!
        '
        'Label90
        '
        Me.Label90.Height = 0.1875!
        Me.Label90.HyperLink = Nothing
        Me.Label90.Left = 0!
        Me.Label90.Name = "Label90"
        Me.Label90.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label90.Text = "a=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label90.Top = 2.5!
        Me.Label90.Width = 2.25!
        '
        'S43_001
        '
        Me.S43_001.Height = 0.1875!
        Me.S43_001.HyperLink = Nothing
        Me.S43_001.Left = 2.25!
        Me.S43_001.Name = "S43_001"
        Me.S43_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S43_001.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_001.Top = 2.5!
        Me.S43_001.Width = 1.0!
        '
        'Label92
        '
        Me.Label92.Height = 0.1875!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 3.25!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label92.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label92.Top = 2.5!
        Me.Label92.Width = 4.75!
        '
        'Label99
        '
        Me.Label99.Height = 0.1875!
        Me.Label99.HyperLink = Nothing
        Me.Label99.Left = 0!
        Me.Label99.Name = "Label99"
        Me.Label99.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label99.Text = "tsp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label99.Top = 2.687!
        Me.Label99.Width = 2.25!
        '
        'S43_002
        '
        Me.S43_002.Height = 0.1875!
        Me.S43_002.HyperLink = Nothing
        Me.S43_002.Left = 2.25!
        Me.S43_002.Name = "S43_002"
        Me.S43_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S43_002.Text = "0.5"
        Me.S43_002.Top = 2.687!
        Me.S43_002.Width = 1.0!
        '
        'Label101
        '
        Me.Label101.Height = 0.1875!
        Me.Label101.HyperLink = Nothing
        Me.Label101.Left = 3.25!
        Me.Label101.Name = "Label101"
        Me.Label101.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label101.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "[default to try and match beam web thickness]" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label101.Top = 2.687!
        Me.Label101.Width = 4.75!
        '
        'Label102
        '
        Me.Label102.Height = 0.1875!
        Me.Label102.HyperLink = Nothing
        Me.Label102.Left = 0!
        Me.Label102.Name = "Label102"
        Me.Label102.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label102.Text = "db =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label102.Top = 2.875!
        Me.Label102.Width = 2.25!
        '
        'S43_003
        '
        Me.S43_003.Height = 0.1875!
        Me.S43_003.HyperLink = Nothing
        Me.S43_003.Left = 2.25!
        Me.S43_003.Name = "S43_003"
        Me.S43_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S43_003.Text = "20.40" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_003.Top = 2.875!
        Me.S43_003.Width = 1.0!
        '
        'Label104
        '
        Me.Label104.Height = 0.1875!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 3.25!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label104.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label104.Top = 2.875!
        Me.Label104.Width = 4.75!
        '
        'Label105
        '
        Me.Label105.Height = 0.1875!
        Me.Label105.HyperLink = Nothing
        Me.Label105.Left = 0!
        Me.Label105.Name = "Label105"
        Me.Label105.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label105.Text = "Bolt Spacing (S_min)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label105.Top = 3.063!
        Me.Label105.Width = 2.25!
        '
        'S43_004
        '
        Me.S43_004.Height = 0.1875!
        Me.S43_004.HyperLink = Nothing
        Me.S43_004.Left = 2.25!
        Me.S43_004.Name = "S43_004"
        Me.S43_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S43_004.Text = "20.40" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_004.Top = 3.063!
        Me.S43_004.Width = 1.0!
        '
        'Label107
        '
        Me.Label107.Height = 0.1875!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 3.25!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label107.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2 x db_sp-hole See AISC 360 J3.3"
        Me.Label107.Top = 3.063!
        Me.Label107.Width = 4.75!
        '
        'Label108
        '
        Me.Label108.Height = 0.1875!
        Me.Label108.HyperLink = Nothing
        Me.Label108.Left = 0!
        Me.Label108.Name = "Label108"
        Me.Label108.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label108.Text = "Bolt Spacing (Svert)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label108.Top = 3.438!
        Me.Label108.Width = 2.25!
        '
        'S43_006
        '
        Me.S43_006.Height = 0.1875!
        Me.S43_006.HyperLink = Nothing
        Me.S43_006.Left = 2.25!
        Me.S43_006.Name = "S43_006"
        Me.S43_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S43_006.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_006.Top = 3.438!
        Me.S43_006.Width = 1.0!
        '
        'Label110
        '
        Me.Label110.Height = 0.1875!
        Me.Label110.HyperLink = Nothing
        Me.Label110.Left = 3.25!
        Me.Label110.Name = "Label110"
        Me.Label110.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label110.Text = "in"
        Me.Label110.Top = 3.438!
        Me.Label110.Width = 0.75!
        '
        'S43_021
        '
        Me.S43_021.Height = 0.1875!
        Me.S43_021.HyperLink = Nothing
        Me.S43_021.Left = 4.0!
        Me.S43_021.Name = "S43_021"
        Me.S43_021.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S43_021.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_021.Top = 3.438!
        Me.S43_021.Width = 1.0!
        '
        'Label112
        '
        Me.Label112.Height = 0.1875!
        Me.Label112.HyperLink = Nothing
        Me.Label112.Left = 0!
        Me.Label112.Name = "Label112"
        Me.Label112.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label112.Text = "Bolt Spacing (Shorz)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label112.Top = 3.626!
        Me.Label112.Width = 2.25!
        '
        'S43_007
        '
        Me.S43_007.Height = 0.1875!
        Me.S43_007.HyperLink = Nothing
        Me.S43_007.Left = 2.25!
        Me.S43_007.Name = "S43_007"
        Me.S43_007.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S43_007.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_007.Top = 3.626!
        Me.S43_007.Width = 1.0!
        '
        'Label114
        '
        Me.Label114.Height = 0.1875!
        Me.Label114.HyperLink = Nothing
        Me.Label114.Left = 3.25!
        Me.Label114.Name = "Label114"
        Me.Label114.Style = "font-size: 8.25pt; font-style: italic; vertical-align: middle"
        Me.Label114.Text = "in"
        Me.Label114.Top = 3.626!
        Me.Label114.Width = 0.75!
        '
        'Label116
        '
        Me.Label116.Height = 0.1875!
        Me.Label116.HyperLink = Nothing
        Me.Label116.Left = 0!
        Me.Label116.Name = "Label116"
        Me.Label116.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label116.Text = "h_flange =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label116.Top = 3.813!
        Me.Label116.Width = 2.25!
        '
        'S43_008
        '
        Me.S43_008.Height = 0.1875!
        Me.S43_008.HyperLink = Nothing
        Me.S43_008.Left = 2.25!
        Me.S43_008.Name = "S43_008"
        Me.S43_008.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S43_008.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_008.Top = 3.813!
        Me.S43_008.Width = 1.0!
        '
        'Label118
        '
        Me.Label118.Height = 0.188!
        Me.Label118.HyperLink = Nothing
        Me.Label118.Left = 3.25!
        Me.Label118.Name = "Label118"
        Me.Label118.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label118.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label118.Top = 3.813!
        Me.Label118.Width = 4.5!
        '
        'Label119
        '
        Me.Label119.Height = 0.1875!
        Me.Label119.HyperLink = Nothing
        Me.Label119.Left = 0!
        Me.Label119.Name = "Label119"
        Me.Label119.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label119.Text = "t_stem =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label119.Top = 4.0!
        Me.Label119.Width = 2.25!
        '
        'S43_009
        '
        Me.S43_009.Height = 0.1875!
        Me.S43_009.HyperLink = Nothing
        Me.S43_009.Left = 2.25!
        Me.S43_009.Name = "S43_009"
        Me.S43_009.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S43_009.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_009.Top = 4.0!
        Me.S43_009.Width = 1.0!
        '
        'Label121
        '
        Me.Label121.Height = 0.1875!
        Me.Label121.HyperLink = Nothing
        Me.Label121.Left = 3.25!
        Me.Label121.Name = "Label121"
        Me.Label121.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label121.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label121.Top = 4.0!
        Me.Label121.Width = 4.75!
        '
        'Label122
        '
        Me.Label122.Height = 0.1875!
        Me.Label122.HyperLink = Nothing
        Me.Label122.Left = 0!
        Me.Label122.Name = "Label122"
        Me.Label122.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label122.Text = "h_clear =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label122.Top = 4.188!
        Me.Label122.Width = 2.25!
        '
        'S43_010
        '
        Me.S43_010.Height = 0.1875!
        Me.S43_010.HyperLink = Nothing
        Me.S43_010.Left = 2.25!
        Me.S43_010.Name = "S43_010"
        Me.S43_010.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S43_010.Text = "0.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_010.Top = 4.188!
        Me.S43_010.Width = 1.0!
        '
        'Label124
        '
        Me.Label124.Height = 0.1875!
        Me.Label124.HyperLink = Nothing
        Me.Label124.Left = 3.25!
        Me.Label124.Name = "Label124"
        Me.Label124.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label124.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "minimum clearance between link flange and shear tab" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label124.Top = 4.188!
        Me.Label124.Width = 4.75!
        '
        'Label125
        '
        Me.Label125.Height = 0.1875!
        Me.Label125.HyperLink = Nothing
        Me.Label125.Left = 0!
        Me.Label125.Name = "Label125"
        Me.Label125.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label125.Text = "Bolt Vert. Edge dist (Lv_min)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label125.Top = 4.375!
        Me.Label125.Width = 2.25!
        '
        'S43_011
        '
        Me.S43_011.Height = 0.1875!
        Me.S43_011.HyperLink = Nothing
        Me.S43_011.Left = 2.25!
        Me.S43_011.Name = "S43_011"
        Me.S43_011.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S43_011.Text = "1.125" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_011.Top = 4.375!
        Me.S43_011.Width = 1.0!
        '
        'Label127
        '
        Me.Label127.Height = 0.1875!
        Me.Label127.HyperLink = Nothing
        Me.Label127.Left = 3.25!
        Me.Label127.Name = "Label127"
        Me.Label127.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label127.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Minimum edge distance equal to one bolt diameter per AISC Table J3.4 note [a]" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label127.Top = 4.375!
        Me.Label127.Width = 4.75!
        '
        'Label128
        '
        Me.Label128.Height = 0.1875!
        Me.Label128.HyperLink = Nothing
        Me.Label128.Left = 0!
        Me.Label128.Name = "Label128"
        Me.Label128.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label128.Text = "Lv_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label128.Top = 4.563!
        Me.Label128.Width = 2.25!
        '
        'S43_012
        '
        Me.S43_012.Height = 0.1875!
        Me.S43_012.HyperLink = Nothing
        Me.S43_012.Left = 2.25!
        Me.S43_012.Name = "S43_012"
        Me.S43_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S43_012.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_012.Top = 4.563!
        Me.S43_012.Width = 1.0!
        '
        'Label130
        '
        Me.Label130.Height = 0.1875!
        Me.Label130.HyperLink = Nothing
        Me.Label130.Left = 3.25!
        Me.Label130.Name = "Label130"
        Me.Label130.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label130.Text = "in"
        Me.Label130.Top = 4.563!
        Me.Label130.Width = 0.75!
        '
        'Label133
        '
        Me.Label133.Height = 0.1875!
        Me.Label133.HyperLink = Nothing
        Me.Label133.Left = 0!
        Me.Label133.Name = "Label133"
        Me.Label133.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label133.Text = "h_sp max =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label133.Top = 4.751!
        Me.Label133.Width = 2.25!
        '
        'S43_013
        '
        Me.S43_013.Height = 0.1875!
        Me.S43_013.HyperLink = Nothing
        Me.S43_013.Left = 2.25!
        Me.S43_013.Name = "S43_013"
        Me.S43_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S43_013.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_013.Top = 4.751!
        Me.S43_013.Width = 1.0!
        '
        'Label135
        '
        Me.Label135.Height = 0.1875!
        Me.Label135.HyperLink = Nothing
        Me.Label135.Left = 3.25!
        Me.Label135.Name = "Label135"
        Me.Label135.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label135.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= db + tstem - h_flange - (2*h_clear)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label135.Top = 4.751!
        Me.Label135.Width = 4.75!
        '
        'Label136
        '
        Me.Label136.Height = 0.1875!
        Me.Label136.HyperLink = Nothing
        Me.Label136.Left = 0!
        Me.Label136.Name = "Label136"
        Me.Label136.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label136.Text = "Plate Depth (h_sp)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label136.Top = 4.938!
        Me.Label136.Width = 2.25!
        '
        'S43_014
        '
        Me.S43_014.Height = 0.1875!
        Me.S43_014.HyperLink = Nothing
        Me.S43_014.Left = 2.25!
        Me.S43_014.Name = "S43_014"
        Me.S43_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S43_014.Text = "8"
        Me.S43_014.Top = 4.938!
        Me.S43_014.Width = 1.0!
        '
        'Label138
        '
        Me.Label138.Height = 0.1875!
        Me.Label138.HyperLink = Nothing
        Me.Label138.Left = 3.25!
        Me.Label138.Name = "Label138"
        Me.Label138.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label138.Text = "in"
        Me.Label138.Top = 4.938!
        Me.Label138.Width = 0.75!
        '
        'S43_022
        '
        Me.S43_022.Height = 0.1875!
        Me.S43_022.HyperLink = Nothing
        Me.S43_022.Left = 4.0!
        Me.S43_022.Name = "S43_022"
        Me.S43_022.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S43_022.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_022.Top = 4.938!
        Me.S43_022.Width = 1.0!
        '
        'Label140
        '
        Me.Label140.Height = 0.1875!
        Me.Label140.HyperLink = Nothing
        Me.Label140.Left = 5.0!
        Me.Label140.Name = "Label140"
        Me.Label140.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label140.Text = "= (n_Vbolt_SST - 1)*Svert + Lv_sp* 2 <= SUM_Plate Depth" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label140.Top = 4.938!
        Me.Label140.Width = 3.0!
        '
        'Label141
        '
        Me.Label141.Height = 0.1875!
        Me.Label141.HyperLink = Nothing
        Me.Label141.Left = 0!
        Me.Label141.Name = "Label141"
        Me.Label141.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label141.Text = "Bolt Horz. Edge dist (Lh_min)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label141.Top = 5.126!
        Me.Label141.Width = 2.25!
        '
        'S43_015
        '
        Me.S43_015.Height = 0.1875!
        Me.S43_015.HyperLink = Nothing
        Me.S43_015.Left = 2.25!
        Me.S43_015.Name = "S43_015"
        Me.S43_015.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S43_015.Text = "1.125" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_015.Top = 5.126!
        Me.S43_015.Width = 1.0!
        '
        'Label143
        '
        Me.Label143.Height = 0.1875!
        Me.Label143.HyperLink = Nothing
        Me.Label143.Left = 3.25!
        Me.Label143.Name = "Label143"
        Me.Label143.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label143.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Minimum edge distance for 7/8"" bolts per AISC Table J3.4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label143.Top = 5.126!
        Me.Label143.Width = 4.75!
        '
        'Label144
        '
        Me.Label144.Height = 0.1875!
        Me.Label144.HyperLink = Nothing
        Me.Label144.Left = 0!
        Me.Label144.Name = "Label144"
        Me.Label144.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label144.Text = "Lh_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label144.Top = 5.314!
        Me.Label144.Width = 2.25!
        '
        'S43_016
        '
        Me.S43_016.Height = 0.1875!
        Me.S43_016.HyperLink = Nothing
        Me.S43_016.Left = 2.25!
        Me.S43_016.Name = "S43_016"
        Me.S43_016.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S43_016.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_016.Top = 5.314!
        Me.S43_016.Width = 1.0!
        '
        'Label146
        '
        Me.Label146.Height = 0.1875!
        Me.Label146.HyperLink = Nothing
        Me.Label146.Left = 3.25!
        Me.Label146.Name = "Label146"
        Me.Label146.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label146.Text = "in"
        Me.Label146.Top = 5.314!
        Me.Label146.Width = 0.75!
        '
        'S43_023
        '
        Me.S43_023.Height = 0.1875!
        Me.S43_023.HyperLink = Nothing
        Me.S43_023.Left = 4.0!
        Me.S43_023.Name = "S43_023"
        Me.S43_023.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S43_023.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_023.Top = 5.314!
        Me.S43_023.Width = 1.0!
        '
        'Label148
        '
        Me.Label148.Height = 0.1875!
        Me.Label148.HyperLink = Nothing
        Me.Label148.Left = 0!
        Me.Label148.Name = "Label148"
        Me.Label148.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label148.Text = "Plate Width (W_sp) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label148.Top = 5.501!
        Me.Label148.Width = 2.25!
        '
        'S43_017
        '
        Me.S43_017.Height = 0.1875!
        Me.S43_017.HyperLink = Nothing
        Me.S43_017.Left = 2.25!
        Me.S43_017.Name = "S43_017"
        Me.S43_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S43_017.Text = "1.125" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_017.Top = 5.501!
        Me.S43_017.Width = 1.0!
        '
        'Label150
        '
        Me.Label150.Height = 0.1875!
        Me.Label150.HyperLink = Nothing
        Me.Label150.Left = 3.25!
        Me.Label150.Name = "Label150"
        Me.Label150.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label150.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (n_Hbolt_SST - 1)*Shorz + Lh_sp + a" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label150.Top = 5.501!
        Me.Label150.Width = 4.75!
        '
        'Label151
        '
        Me.Label151.Height = 0.1875!
        Me.Label151.HyperLink = Nothing
        Me.Label151.Left = 0!
        Me.Label151.Name = "Label151"
        Me.Label151.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label151.Text = "Lslot_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label151.Top = 5.689!
        Me.Label151.Width = 2.25!
        '
        'S43_018
        '
        Me.S43_018.Height = 0.1875!
        Me.S43_018.HyperLink = Nothing
        Me.S43_018.Left = 2.25!
        Me.S43_018.Name = "S43_018"
        Me.S43_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S43_018.Text = "1.125" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_018.Top = 5.689!
        Me.S43_018.Width = 1.0!
        '
        'Label153
        '
        Me.Label153.Height = 0.1875!
        Me.Label153.HyperLink = Nothing
        Me.Label153.Left = 3.25!
        Me.Label153.Name = "Label153"
        Me.Label153.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label153.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= db_sp + 1/8 + 0.14* Svert*((n_Vbolts_SST - 1)/2)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label153.Top = 5.689!
        Me.Label153.Width = 4.75!
        '
        'Label154
        '
        Me.Label154.Height = 0.1875!
        Me.Label154.HyperLink = Nothing
        Me.Label154.Left = 0!
        Me.Label154.Name = "Label154"
        Me.Label154.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label154.Text = "LslotH = LslotV =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label154.Top = 5.877!
        Me.Label154.Width = 2.25!
        '
        'S43_019
        '
        Me.S43_019.Height = 0.1875!
        Me.S43_019.HyperLink = Nothing
        Me.S43_019.Left = 2.25!
        Me.S43_019.Name = "S43_019"
        Me.S43_019.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S43_019.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_019.Top = 5.877!
        Me.S43_019.Width = 1.0!
        '
        'Label156
        '
        Me.Label156.Height = 0.1875!
        Me.Label156.HyperLink = Nothing
        Me.Label156.Left = 3.25!
        Me.Label156.Name = "Label156"
        Me.Label156.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label156.Text = "in"
        Me.Label156.Top = 5.877!
        Me.Label156.Width = 0.75!
        '
        'S43_024
        '
        Me.S43_024.Height = 0.1875!
        Me.S43_024.HyperLink = Nothing
        Me.S43_024.Left = 4.0!
        Me.S43_024.Name = "S43_024"
        Me.S43_024.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S43_024.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_024.Top = 5.877!
        Me.S43_024.Width = 1.0!
        '
        'Label158
        '
        Me.Label158.Height = 0.1875!
        Me.Label158.HyperLink = Nothing
        Me.Label158.Left = 5.0!
        Me.Label158.Name = "Label158"
        Me.Label158.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label158.Text = "OK if Lslot_min/ Lslot <= 1.03" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label158.Top = 5.877!
        Me.Label158.Width = 3.0!
        '
        'Label163
        '
        Me.Label163.Height = 0.1875!
        Me.Label163.HyperLink = Nothing
        Me.Label163.Left = 0!
        Me.Label163.Name = "Label163"
        Me.Label163.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label163.Text = " Shear Plate Geometry Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label163.Top = 6.064!
        Me.Label163.Width = 2.25!
        '
        'Label168
        '
        Me.Label168.Height = 0.1875!
        Me.Label168.HyperLink = Nothing
        Me.Label168.Left = 3.25!
        Me.Label168.Name = "Label168"
        Me.Label168.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label168.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK if And (Svert = OK, Lv_sp = OK, Lh_sp = OK, Lslot = OK)"
        Me.Label168.Top = 6.064!
        Me.Label168.Width = 4.75!
        '
        'Label159
        '
        Me.Label159.Height = 0.1875!
        Me.Label159.HyperLink = Nothing
        Me.Label159.Left = 0!
        Me.Label159.Name = "Label159"
        Me.Label159.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label159.Text = "4.4 SHEAR PLATE YIELDING (VERTICAL):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label159.Top = 6.375!
        Me.Label159.Width = 3.0!
        '
        'Label160
        '
        Me.Label160.Height = 0.1875!
        Me.Label160.HyperLink = Nothing
        Me.Label160.Left = 0!
        Me.Label160.Name = "Label160"
        Me.Label160.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label160.Text = "Φyield=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label160.Top = 6.563!
        Me.Label160.Width = 2.25!
        '
        'S44_001
        '
        Me.S44_001.Height = 0.1875!
        Me.S44_001.HyperLink = Nothing
        Me.S44_001.Left = 2.25!
        Me.S44_001.Name = "S44_001"
        Me.S44_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S44_001.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S44_001.Top = 6.563!
        Me.S44_001.Width = 1.0!
        '
        'Label165
        '
        Me.Label165.Height = 0.1875!
        Me.Label165.HyperLink = Nothing
        Me.Label165.Left = 0!
        Me.Label165.Name = "Label165"
        Me.Label165.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label165.Text = "dHole_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label165.Top = 6.75!
        Me.Label165.Width = 2.25!
        '
        'S44_002
        '
        Me.S44_002.Height = 0.1875!
        Me.S44_002.HyperLink = Nothing
        Me.S44_002.Left = 2.25!
        Me.S44_002.Name = "S44_002"
        Me.S44_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S44_002.Text = "1.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S44_002.Top = 6.75!
        Me.S44_002.Width = 1.0!
        '
        'Label169
        '
        Me.Label169.Height = 0.1875!
        Me.Label169.HyperLink = Nothing
        Me.Label169.Left = 3.25!
        Me.Label169.Name = "Label169"
        Me.Label169.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label169.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= db_sp + 1/8" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label169.Top = 6.75!
        Me.Label169.Width = 4.75!
        '
        'Label164
        '
        Me.Label164.Height = 0.1875!
        Me.Label164.HyperLink = Nothing
        Me.Label164.Left = 0!
        Me.Label164.Name = "Label164"
        Me.Label164.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label164.Text = "Asp_Agv=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label164.Top = 6.937!
        Me.Label164.Width = 2.25!
        '
        'S44_003
        '
        Me.S44_003.Height = 0.1875!
        Me.S44_003.HyperLink = Nothing
        Me.S44_003.Left = 2.25!
        Me.S44_003.Name = "S44_003"
        Me.S44_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S44_003.Text = "4.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S44_003.Top = 6.937!
        Me.S44_003.Width = 1.0!
        '
        'Label171
        '
        Me.Label171.Height = 0.1875!
        Me.Label171.HyperLink = Nothing
        Me.Label171.Left = 3.25!
        Me.Label171.Name = "Label171"
        Me.Label171.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label171.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= tsp* h_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label171.Top = 6.937!
        Me.Label171.Width = 4.75!
        '
        'Label172
        '
        Me.Label172.Height = 0.1875!
        Me.Label172.HyperLink = Nothing
        Me.Label172.Left = 0!
        Me.Label172.Name = "Label172"
        Me.Label172.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label172.Text = "ΦVy_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label172.Top = 7.125!
        Me.Label172.Width = 2.25!
        '
        'S44_004
        '
        Me.S44_004.Height = 0.1875!
        Me.S44_004.HyperLink = Nothing
        Me.S44_004.Left = 2.25!
        Me.S44_004.Name = "S44_004"
        Me.S44_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S44_004.Text = "108.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S44_004.Top = 7.125!
        Me.S44_004.Width = 1.0!
        '
        'Label174
        '
        Me.Label174.Height = 0.1875!
        Me.Label174.HyperLink = Nothing
        Me.Label174.Left = 3.25!
        Me.Label174.Name = "Label174"
        Me.Label174.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label174.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φyield* 0.6* Asp_Agv* Fy_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label174.Top = 7.125!
        Me.Label174.Width = 4.75!
        '
        'Label175
        '
        Me.Label175.Height = 0.1875!
        Me.Label175.HyperLink = Nothing
        Me.Label175.Left = 0!
        Me.Label175.Name = "Label175"
        Me.Label175.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label175.Text = "SUM_spYield=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label175.Top = 7.312!
        Me.Label175.Width = 2.25!
        '
        'Label176
        '
        Me.Label176.Height = 0.1875!
        Me.Label176.HyperLink = Nothing
        Me.Label176.Left = 3.75!
        Me.Label176.Name = "Label176"
        Me.Label176.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label176.Text = "= Vu_bm/ ΦVy_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label176.Top = 7.312!
        Me.Label176.Width = 4.25!
        '
        'Label178
        '
        Me.Label178.Height = 0.1875!
        Me.Label178.HyperLink = Nothing
        Me.Label178.Left = 0!
        Me.Label178.Name = "Label178"
        Me.Label178.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label178.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label178.Top = 7.5!
        Me.Label178.Width = 2.25!
        '
        'Label179
        '
        Me.Label179.Height = 0.1875!
        Me.Label179.HyperLink = Nothing
        Me.Label179.Left = 3.75!
        Me.Label179.Name = "Label179"
        Me.Label179.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label179.Text = "OK if SUM_spYield <= SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label179.Top = 7.5!
        Me.Label179.Width = 4.25!
        '
        'S44_006
        '
        Me.S44_006.Height = 0.1875!
        Me.S44_006.HyperLink = Nothing
        Me.S44_006.Left = 2.25!
        Me.S44_006.Name = "S44_006"
        Me.S44_006.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S44_006.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S44_006.Top = 7.5!
        Me.S44_006.Width = 1.0!
        '
        'S44_005
        '
        Me.S44_005.Height = 0.1875!
        Me.S44_005.HyperLink = Nothing
        Me.S44_005.Left = 2.25!
        Me.S44_005.Name = "S44_005"
        Me.S44_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S44_005.Text = "108.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S44_005.Top = 7.312!
        Me.S44_005.Width = 1.0!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.1875!
        Me.Shape3.Left = 2.25!
        Me.Shape3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape3.Top = 7.5!
        Me.Shape3.Width = 1.0!
        '
        'S45_005
        '
        Me.S45_005.Height = 0.1875!
        Me.S45_005.HyperLink = Nothing
        Me.S45_005.Left = 2.25!
        Me.S45_005.Name = "S45_005"
        Me.S45_005.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S45_005.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S45_005.Top = 8.749001!
        Me.S45_005.Width = 1.0!
        '
        'Label181
        '
        Me.Label181.Height = 0.1875!
        Me.Label181.HyperLink = Nothing
        Me.Label181.Left = 0!
        Me.Label181.Name = "Label181"
        Me.Label181.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label181.Text = "4.5 SHEAR PLATE RUPTURE (VERTICAL):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label181.Top = 7.812!
        Me.Label181.Width = 3.0!
        '
        'Label182
        '
        Me.Label182.Height = 0.1875!
        Me.Label182.HyperLink = Nothing
        Me.Label182.Left = 0!
        Me.Label182.Name = "Label182"
        Me.Label182.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label182.Text = "Φrupture=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label182.Top = 7.999001!
        Me.Label182.Width = 2.25!
        '
        'S45_001
        '
        Me.S45_001.Height = 0.1875!
        Me.S45_001.HyperLink = Nothing
        Me.S45_001.Left = 2.25!
        Me.S45_001.Name = "S45_001"
        Me.S45_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S45_001.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S45_001.Top = 7.999001!
        Me.S45_001.Width = 1.0!
        '
        'Label184
        '
        Me.Label184.Height = 0.1875!
        Me.Label184.HyperLink = Nothing
        Me.Label184.Left = 0!
        Me.Label184.Name = "Label184"
        Me.Label184.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label184.Text = "Asp_nv=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label184.Top = 8.187!
        Me.Label184.Width = 2.25!
        '
        'S45_002
        '
        Me.S45_002.Height = 0.1875!
        Me.S45_002.HyperLink = Nothing
        Me.S45_002.Left = 2.25!
        Me.S45_002.Name = "S45_002"
        Me.S45_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S45_002.Text = "2.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S45_002.Top = 8.187!
        Me.S45_002.Width = 1.0!
        '
        'Label186
        '
        Me.Label186.Height = 0.1875!
        Me.Label186.HyperLink = Nothing
        Me.Label186.Left = 3.25!
        Me.Label186.Name = "Label186"
        Me.Label186.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label186.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= hsp* tsp - dHole_sp* n_Vbolts_SST* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label186.Top = 8.187!
        Me.Label186.Width = 4.75!
        '
        'Label187
        '
        Me.Label187.Height = 0.1875!
        Me.Label187.HyperLink = Nothing
        Me.Label187.Left = 0!
        Me.Label187.Name = "Label187"
        Me.Label187.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label187.Text = "ΦVrupture_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label187.Top = 8.374001!
        Me.Label187.Width = 2.25!
        '
        'S45_003
        '
        Me.S45_003.Height = 0.1875!
        Me.S45_003.HyperLink = Nothing
        Me.S45_003.Left = 2.25!
        Me.S45_003.Name = "S45_003"
        Me.S45_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S45_003.Text = "73.13" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S45_003.Top = 8.374001!
        Me.S45_003.Width = 1.0!
        '
        'Label189
        '
        Me.Label189.Height = 0.1875!
        Me.Label189.HyperLink = Nothing
        Me.Label189.Left = 3.25!
        Me.Label189.Name = "Label189"
        Me.Label189.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label189.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φrupture* 0.6* Fu_shearTab* Asp_nv" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label189.Top = 8.374001!
        Me.Label189.Width = 4.75!
        '
        'Label190
        '
        Me.Label190.Height = 0.1875!
        Me.Label190.HyperLink = Nothing
        Me.Label190.Left = 0!
        Me.Label190.Name = "Label190"
        Me.Label190.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label190.Text = "SUM_spRupture=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label190.Top = 8.562!
        Me.Label190.Width = 2.25!
        '
        'Label191
        '
        Me.Label191.Height = 0.1875!
        Me.Label191.HyperLink = Nothing
        Me.Label191.Left = 3.75!
        Me.Label191.Name = "Label191"
        Me.Label191.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label191.Text = "= Vu_bm/ ΦVrupture_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label191.Top = 8.562!
        Me.Label191.Width = 4.25!
        '
        'Label192
        '
        Me.Label192.Height = 0.1875!
        Me.Label192.HyperLink = Nothing
        Me.Label192.Left = 0!
        Me.Label192.Name = "Label192"
        Me.Label192.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label192.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label192.Top = 8.749001!
        Me.Label192.Width = 2.25!
        '
        'Label193
        '
        Me.Label193.Height = 0.1875!
        Me.Label193.HyperLink = Nothing
        Me.Label193.Left = 3.75!
        Me.Label193.Name = "Label193"
        Me.Label193.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label193.Text = "OK if SUM_spRupture <= SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label193.Top = 8.749001!
        Me.Label193.Width = 4.25!
        '
        'S45_004
        '
        Me.S45_004.Height = 0.1875!
        Me.S45_004.HyperLink = Nothing
        Me.S45_004.Left = 2.25!
        Me.S45_004.Name = "S45_004"
        Me.S45_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S45_004.Text = "108.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S45_004.Top = 8.562!
        Me.S45_004.Width = 1.0!
        '
        'Shape4
        '
        Me.Shape4.Height = 0.1875!
        Me.Shape4.Left = 2.25!
        Me.Shape4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape4.Name = "Shape4"
        Me.Shape4.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape4.Top = 8.749001!
        Me.Shape4.Width = 1.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "S_max=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 3.25!
        Me.Label1.Width = 2.25!
        '
        'S43_005
        '
        Me.S43_005.Height = 0.1875!
        Me.S43_005.HyperLink = Nothing
        Me.S43_005.Left = 2.25!
        Me.S43_005.Name = "S43_005"
        Me.S43_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S43_005.Text = "20.40" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S43_005.Top = 3.25!
        Me.S43_005.Width = 1.0!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 3.25!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label3.Text = "in"
        Me.Label3.Top = 3.25!
        Me.Label3.Width = 4.75!
        '
        'Shape2
        '
        Me.Shape2.Height = 0.1875!
        Me.Shape2.Left = 2.25!
        Me.Shape2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 6.064!
        Me.Shape2.Width = 1.0!
        '
        'Label2
        '
        Me.Label2.Height = 0.188!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 5.0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label2.Text = "Check Table J3.4 Note [a], See Section 4.4, 4.8 & 4.9 below" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 3.437001!
        Me.Label2.Width = 3.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 4.0!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 3.438!
        Me.Shape1.Width = 1.0!
        '
        'Shape5
        '
        Me.Shape5.Height = 0.1875!
        Me.Shape5.Left = 4.0!
        Me.Shape5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape5.Name = "Shape5"
        Me.Shape5.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape5.Top = 4.937!
        Me.Shape5.Width = 1.0!
        '
        'Shape6
        '
        Me.Shape6.Height = 0.1875!
        Me.Shape6.Left = 4.0!
        Me.Shape6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape6.Name = "Shape6"
        Me.Shape6.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape6.Top = 5.312!
        Me.Shape6.Width = 1.0!
        '
        'Shape7
        '
        Me.Shape7.Height = 0.1875!
        Me.Shape7.Left = 4.0!
        Me.Shape7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape7.Name = "Shape7"
        Me.Shape7.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape7.Top = 5.875!
        Me.Shape7.Width = 1.0!
        '
        'US_SPC2
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.75!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S43_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label138, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label140, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label143, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label150, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label153, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label168, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label159, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S44_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label165, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S44_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label169, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S44_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label171, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label172, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S44_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label174, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label175, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label176, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label178, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S44_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S44_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S45_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label181, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label182, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S45_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label184, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S45_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label186, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label187, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S45_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label189, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label190, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label192, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S45_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S43_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents S43_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label89 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture1 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Label90 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label92 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label99 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label101 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label102 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label104 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label105 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label107 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label108 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label110 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label112 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label114 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label116 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label118 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label119 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label121 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label122 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label124 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label125 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label127 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label128 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label130 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label133 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label135 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label136 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label138 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label140 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label141 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label143 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label144 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label146 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label148 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label150 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label151 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label153 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label154 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label156 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label158 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label163 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label168 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label159 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label160 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S44_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label165 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S44_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label169 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label164 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S44_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label171 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label172 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S44_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label174 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label175 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label176 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label178 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label179 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S44_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S44_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape3 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S45_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label181 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label182 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S45_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label184 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S45_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label186 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label187 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S45_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label189 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label190 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label191 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label192 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label193 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S45_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape4 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S43_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape5 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape6 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape7 As GrapeCity.ActiveReports.SectionReportModel.Shape

    '

End Class
