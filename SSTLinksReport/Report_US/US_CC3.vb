﻿Imports GrapeCity.ActiveReports
Imports GrapeCity.ActiveReports.Document
Imports SST.SFStudioLib
Imports SSTLinksLib

Public Class US_CC3
    Sub New(lst As SortedList(Of String, String))
        InitializeComponent()
        '
        For Each ctrl In Detail.Controls
            If TypeOf (ctrl) Is SectionReportModel.Label Then
                Dim id = lst.Keys.IndexOf(ctrl.Name)
                If ctrl.Name Like "S*" Then
                    ctrl.Text = "/"
                    If id >= 0 Then
                        ctrl.Text = lst.Values(id)
                        If lst.Values(id) = "NG" Then
                            ctrl.ForeColor = Color.Red
                        End If
                    End If
                End If
            End If
        Next
    End Sub


End Class
