﻿Imports YieldLinkLib

Public Class frmMsgboxRS_SF

    Public Property isUpdate As Boolean
    Sub New(SFX#, SFY#, SFX_D#, SFY_D#, NewSFX#, NewSFY#, NewSFX_D#, NewSFY_D#)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        Dim cnt As Integer = 0
        cnt += 1 : dgvSF.Rows.Add(cnt, "SPECX*", SFX, NewSFX)
        cnt += 1 : dgvSF.Rows.Add(cnt, "SPECY*", SFX, NewSFY)
        cnt += 1 : dgvSF.Rows.Add(cnt, "SPECX*_D", SFX, NewSFX_D)
        cnt += 1 : dgvSF.Rows.Add(cnt, "SPECY*_D", SFX, NewSFY_D)
    End Sub

    Private Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        isUpdate = False
        Me.Close()
    End Sub

    Private Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        isUpdate = True
        Me.Close()
    End Sub
End Class