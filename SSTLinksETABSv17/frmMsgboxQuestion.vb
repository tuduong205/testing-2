﻿Public Class frmMsgboxQuestion
    Public YNC As Integer
    Sub New(strQuestion As String, _DefaultYNC As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        txtQuestion.Text = strQuestion
        '
        If _DefaultYNC = 1 Then
            btnYes.Focus()
        ElseIf _DefaultYNC = 2 Then
            btnNo.Focus()
        Else
            btnCancel.Focus()
        End If
    End Sub
End Class