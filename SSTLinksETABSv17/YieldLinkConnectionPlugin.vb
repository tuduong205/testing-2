Imports System.Runtime.InteropServices
Imports YieldLinkReport
Imports YieldLinkLib
Imports System.Windows.Forms
Imports System.Linq
Imports System.Drawing
Imports System.IO
Imports SAP2000v1
Imports SAP2000v20
Imports ETABSv17

Public Class YieldLinkConnectionPlugin
    Private clsSE As clsSAP_Etabs
    Private isUserEdit As Boolean = False
    Private isPrevent As Boolean = False
    Private SelectedFrames As New List(Of clsSEFrame)
    Private SelectedBeams As New List(Of clsSSTBeam)
    Private SelectedColumns As New List(Of clsSSTColumn)
    Public Shared gJob As clsMRSJob

    '\\ Private saveInfo As saveInfo
    '\\ Private saveName As String = "saveInfo.dt"C:\_Tool\05. CSI Plugin\Working 2018-11-28\_Data\xml_SSTLinks.xml
    '\\ Private preJob As String = "preJob.dt"
    '\\ Private projID As String

    ''Private isHideBeamDesign As Boolean = False
    Private isDebug As Boolean = True

    <DllImport("user32.dll", EntryPoint:="BringWindowToTop")>
    Private Shared Function BringWindowToTop(hWnd As Integer) As Integer
    End Function
    Sub New()
        ' This call is required by the designer.
        InitializeComponent()
        '
        Me.C05.Items.AddRange(New Object() {"W44X335", "W44X290", "W44X262", "W44X230", "W40X593", "W40X503", "W40X431", "W40X397", "W40X372", "W40X362", "W40X324", "W40X297", "W40X277", "W40X249", "W40X215", "W40X199", "W40X392", "W40X331", "W40X327", "W40X294", "W40X278", "W40X264", "W40X235", "W40X211", "W40X183", "W40X167", "W40X149", "W36X652", "W36X529", "W36X487", "W36X441", "W36X395", "W36X361", "W36X330", "W36X302", "W36X282", "W36X262", "W36X247", "W36X231", "W36X256", "W36X232", "W36X210", "W36X194", "W36X182", "W36X170", "W36X160", "W36X150", "W36X135", "W33X387", "W33X354", "W33X318", "W33X291", "W33X263", "W33X241", "W33X221", "W33X201", "W33X169", "W33X152", "W33X141", "W33X130", "W33X118", "W30X391", "W30X357", "W30X326", "W30X292", "W30X261", "W30X235", "W30X211", "W30X191", "W30X173", "W30X148", "W30X132", "W30X124", "W30X116", "W30X108", "W30X99", "W30X90", "W27X539", "W27X368", "W27X336", "W27X307", "W27X281", "W27X258", "W27X235", "W27X217", "W27X194", "W27X178", "W27X161", "W27X146", "W27X129", "W27X114", "W27X102", "W27X94", "W27X84", "W24X370", "W24X335", "W24X306", "W24X279", "W24X250", "W24X229", "W24X207", "W24X192", "W24X176", "W24X162", "W24X146", "W24X131", "W24X117", "W24X104", "W24X103", "W24X94", "W24X84", "W24X76", "W24X68", "W24X62", "W24X55", "W21X201", "W21X182", "W21X166", "W21X147", "W21X132", "W21X122", "W21X111", "W21X101", "W21X93", "W21X83", "W21X73", "W21X68", "W21X62", "W21X55", "W21X48", "W21X57", "W21X50", "W21X44", "W18X311", "W18X283", "W18X258", "W18X234", "W18X211", "W18X192", "W18X175", "W18X158", "W18X143", "W18X130", "W18X119", "W18X106", "W18X97", "W18X86", "W18X76", "W18X71", "W18X65", "W18X60", "W18X55", "W18X50", "W18X46", "W18X40", "W18X35", "W16X100", "W16X89", "W16X77", "W16X67", "W16X57", "W16X50", "W16X45", "W16X40", "W16X36", "W16X31", "W16X26", "W14X730", "W14X665", "W14X605", "W14X550", "W14X500", "W14X455", "W14X426", "W14X398", "W14X370", "W14X342", "W14X311", "W14X283", "W14X257", "W14X233", "W14X211", "W14X193", "W14X176", "W14X159", "W14X145", "W14X132", "W14X120", "W14X109", "W14X99", "W14X90", "W14X82", "W14X74", "W14X68", "W14X61", "W14X53", "W14X48", "W14X43", "W14X38", "W14X34", "W14X30", "W14X26", "W14X22", "W12X336", "W12X305", "W12X279", "W12X252", "W12X230", "W12X210", "W12X190", "W12X170", "W12X152", "W12X136", "W12X120", "W12X106", "W12X96", "W12X87", "W12X79", "W12X72", "W12X65", "W12X58", "W12X53", "W12X50", "W12X45", "W12X40", "W12X35", "W12X30", "W12X26", "W12X22", "W12X19", "W12X16", "W12X14", "W10X112", "W10X100", "W10X88", "W10X77", "W10X68", "W10X60", "W10X54", "W10X49", "W10X45", "W10X39", "W10X33", "W10X30", "W10X26", "W10X22", "W10X19", "W10X17", "W10X15", "W10X12", "W8X67", "W8X58", "W8X48", "W8X40", "W8X35", "W8X31", "W8X28", "W8X24", "W8X21", "W8X18", "W8X15", "W8X13", "W8X10"})
        Me.C08.Items.AddRange(New Object() {"W44X335", "W44X290", "W44X262", "W44X230", "W40X593", "W40X503", "W40X431", "W40X397", "W40X372", "W40X362", "W40X324", "W40X297", "W40X277", "W40X249", "W40X215", "W40X199", "W40X392", "W40X331", "W40X327", "W40X294", "W40X278", "W40X264", "W40X235", "W40X211", "W40X183", "W40X167", "W40X149", "W36X652", "W36X529", "W36X487", "W36X441", "W36X395", "W36X361", "W36X330", "W36X302", "W36X282", "W36X262", "W36X247", "W36X231", "W36X256", "W36X232", "W36X210", "W36X194", "W36X182", "W36X170", "W36X160", "W36X150", "W36X135", "W33X387", "W33X354", "W33X318", "W33X291", "W33X263", "W33X241", "W33X221", "W33X201", "W33X169", "W33X152", "W33X141", "W33X130", "W33X118", "W30X391", "W30X357", "W30X326", "W30X292", "W30X261", "W30X235", "W30X211", "W30X191", "W30X173", "W30X148", "W30X132", "W30X124", "W30X116", "W30X108", "W30X99", "W30X90", "W27X539", "W27X368", "W27X336", "W27X307", "W27X281", "W27X258", "W27X235", "W27X217", "W27X194", "W27X178", "W27X161", "W27X146", "W27X129", "W27X114", "W27X102", "W27X94", "W27X84", "W24X370", "W24X335", "W24X306", "W24X279", "W24X250", "W24X229", "W24X207", "W24X192", "W24X176", "W24X162", "W24X146", "W24X131", "W24X117", "W24X104", "W24X103", "W24X94", "W24X84", "W24X76", "W24X68", "W24X62", "W24X55", "W21X201", "W21X182", "W21X166", "W21X147", "W21X132", "W21X122", "W21X111", "W21X101", "W21X93", "W21X83", "W21X73", "W21X68", "W21X62", "W21X55", "W21X48", "W21X57", "W21X50", "W21X44", "W18X311", "W18X283", "W18X258", "W18X234", "W18X211", "W18X192", "W18X175", "W18X158", "W18X143", "W18X130", "W18X119", "W18X106", "W18X97", "W18X86", "W18X76", "W18X71", "W18X65", "W18X60", "W18X55", "W18X50", "W18X46", "W18X40", "W18X35", "W16X100", "W16X89", "W16X77", "W16X67", "W16X57", "W16X50", "W16X45", "W16X40", "W16X36", "W16X31", "W16X26", "W14X730", "W14X665", "W14X605", "W14X550", "W14X500", "W14X455", "W14X426", "W14X398", "W14X370", "W14X342", "W14X311", "W14X283", "W14X257", "W14X233", "W14X211", "W14X193", "W14X176", "W14X159", "W14X145", "W14X132", "W14X120", "W14X109", "W14X99", "W14X90", "W14X82", "W14X74", "W14X68", "W14X61", "W14X53", "W14X48", "W14X43", "W14X38", "W14X34", "W14X30", "W14X26", "W14X22", "W12X336", "W12X305", "W12X279", "W12X252", "W12X230", "W12X210", "W12X190", "W12X170", "W12X152", "W12X136", "W12X120", "W12X106", "W12X96", "W12X87", "W12X79", "W12X72", "W12X65", "W12X58", "W12X53", "W12X50", "W12X45", "W12X40", "W12X35", "W12X30", "W12X26", "W12X22", "W12X19", "W12X16", "W12X14", "W10X112", "W10X100", "W10X88", "W10X77", "W10X68", "W10X60", "W10X54", "W10X49", "W10X45", "W10X39", "W10X33", "W10X30", "W10X26", "W10X22", "W10X19", "W10X17", "W10X15", "W10X12", "W8X67", "W8X58", "W8X48", "W8X40", "W8X35", "W8X31", "W8X28", "W8X24", "W8X21", "W8X18", "W8X15", "W8X13", "W8X10"})
        Me.C09.Items.AddRange(New Object() {"W44X335", "W44X290", "W44X262", "W44X230", "W40X593", "W40X503", "W40X431", "W40X397", "W40X372", "W40X362", "W40X324", "W40X297", "W40X277", "W40X249", "W40X215", "W40X199", "W40X392", "W40X331", "W40X327", "W40X294", "W40X278", "W40X264", "W40X235", "W40X211", "W40X183", "W40X167", "W40X149", "W36X652", "W36X529", "W36X487", "W36X441", "W36X395", "W36X361", "W36X330", "W36X302", "W36X282", "W36X262", "W36X247", "W36X231", "W36X256", "W36X232", "W36X210", "W36X194", "W36X182", "W36X170", "W36X160", "W36X150", "W36X135", "W33X387", "W33X354", "W33X318", "W33X291", "W33X263", "W33X241", "W33X221", "W33X201", "W33X169", "W33X152", "W33X141", "W33X130", "W33X118", "W30X391", "W30X357", "W30X326", "W30X292", "W30X261", "W30X235", "W30X211", "W30X191", "W30X173", "W30X148", "W30X132", "W30X124", "W30X116", "W30X108", "W30X99", "W30X90", "W27X539", "W27X368", "W27X336", "W27X307", "W27X281", "W27X258", "W27X235", "W27X217", "W27X194", "W27X178", "W27X161", "W27X146", "W27X129", "W27X114", "W27X102", "W27X94", "W27X84", "W24X370", "W24X335", "W24X306", "W24X279", "W24X250", "W24X229", "W24X207", "W24X192", "W24X176", "W24X162", "W24X146", "W24X131", "W24X117", "W24X104", "W24X103", "W24X94", "W24X84", "W24X76", "W24X68", "W24X62", "W24X55", "W21X201", "W21X182", "W21X166", "W21X147", "W21X132", "W21X122", "W21X111", "W21X101", "W21X93", "W21X83", "W21X73", "W21X68", "W21X62", "W21X55", "W21X48", "W21X57", "W21X50", "W21X44", "W18X311", "W18X283", "W18X258", "W18X234", "W18X211", "W18X192", "W18X175", "W18X158", "W18X143", "W18X130", "W18X119", "W18X106", "W18X97", "W18X86", "W18X76", "W18X71", "W18X65", "W18X60", "W18X55", "W18X50", "W18X46", "W18X40", "W18X35", "W16X100", "W16X89", "W16X77", "W16X67", "W16X57", "W16X50", "W16X45", "W16X40", "W16X36", "W16X31", "W16X26", "W14X730", "W14X665", "W14X605", "W14X550", "W14X500", "W14X455", "W14X426", "W14X398", "W14X370", "W14X342", "W14X311", "W14X283", "W14X257", "W14X233", "W14X211", "W14X193", "W14X176", "W14X159", "W14X145", "W14X132", "W14X120", "W14X109", "W14X99", "W14X90", "W14X82", "W14X74", "W14X68", "W14X61", "W14X53", "W14X48", "W14X43", "W14X38", "W14X34", "W14X30", "W14X26", "W14X22", "W12X336", "W12X305", "W12X279", "W12X252", "W12X230", "W12X210", "W12X190", "W12X170", "W12X152", "W12X136", "W12X120", "W12X106", "W12X96", "W12X87", "W12X79", "W12X72", "W12X65", "W12X58", "W12X53", "W12X50", "W12X45", "W12X40", "W12X35", "W12X30", "W12X26", "W12X22", "W12X19", "W12X16", "W12X14", "W10X112", "W10X100", "W10X88", "W10X77", "W10X68", "W10X60", "W10X54", "W10X49", "W10X45", "W10X39", "W10X33", "W10X30", "W10X26", "W10X22", "W10X19", "W10X17", "W10X15", "W10X12", "W8X67", "W8X58", "W8X48", "W8X40", "W8X35", "W8X31", "W8X28", "W8X24", "W8X21", "W8X18", "W8X15", "W8X13", "W8X10"})

        clsSE = New clsSAP_Etabs
        ''clsSE.isCanada = g_isCanada

        If gJob Is Nothing Then
            Dim Arr() As String
            gJob = New clsMRSJob(Arr)
        End If
        gJob.isEtab = g_isEtab
        '\\ saveInfo = New saveInfo
        '
        If gJob.isEtab Then
            gJob.versionID = "Version ID: ETABS Plug-in" & toolVer
            Me.Text = toolName & "_ETABS" & toolVer
            btnModifyS2k.Visible = False
        Else
            gJob.versionID = "Version ID: SAP2000 Plug-in" & toolVer
            Me.Text = toolName & "_SAP2000" & toolVer
            btnModifyE2k.Visible = False
        End If
        '
        UpdateDisplay()
        '
        '\\ Save_Load_gJob(False)
        '
        isUserEdit = True

    End Sub
    Sub New(_clsSAP As clsSAP_Etabs, _isEtabs As Boolean)
        ' This call is required by the designer.
        InitializeComponent()

        Me.C05.Items.AddRange(New Object() {"W44X335", "W44X290", "W44X262", "W44X230", "W40X593", "W40X503", "W40X431", "W40X397", "W40X372", "W40X362", "W40X324", "W40X297", "W40X277", "W40X249", "W40X215", "W40X199", "W40X392", "W40X331", "W40X327", "W40X294", "W40X278", "W40X264", "W40X235", "W40X211", "W40X183", "W40X167", "W40X149", "W36X652", "W36X529", "W36X487", "W36X441", "W36X395", "W36X361", "W36X330", "W36X302", "W36X282", "W36X262", "W36X247", "W36X231", "W36X256", "W36X232", "W36X210", "W36X194", "W36X182", "W36X170", "W36X160", "W36X150", "W36X135", "W33X387", "W33X354", "W33X318", "W33X291", "W33X263", "W33X241", "W33X221", "W33X201", "W33X169", "W33X152", "W33X141", "W33X130", "W33X118", "W30X391", "W30X357", "W30X326", "W30X292", "W30X261", "W30X235", "W30X211", "W30X191", "W30X173", "W30X148", "W30X132", "W30X124", "W30X116", "W30X108", "W30X99", "W30X90", "W27X539", "W27X368", "W27X336", "W27X307", "W27X281", "W27X258", "W27X235", "W27X217", "W27X194", "W27X178", "W27X161", "W27X146", "W27X129", "W27X114", "W27X102", "W27X94", "W27X84", "W24X370", "W24X335", "W24X306", "W24X279", "W24X250", "W24X229", "W24X207", "W24X192", "W24X176", "W24X162", "W24X146", "W24X131", "W24X117", "W24X104", "W24X103", "W24X94", "W24X84", "W24X76", "W24X68", "W24X62", "W24X55", "W21X201", "W21X182", "W21X166", "W21X147", "W21X132", "W21X122", "W21X111", "W21X101", "W21X93", "W21X83", "W21X73", "W21X68", "W21X62", "W21X55", "W21X48", "W21X57", "W21X50", "W21X44", "W18X311", "W18X283", "W18X258", "W18X234", "W18X211", "W18X192", "W18X175", "W18X158", "W18X143", "W18X130", "W18X119", "W18X106", "W18X97", "W18X86", "W18X76", "W18X71", "W18X65", "W18X60", "W18X55", "W18X50", "W18X46", "W18X40", "W18X35", "W16X100", "W16X89", "W16X77", "W16X67", "W16X57", "W16X50", "W16X45", "W16X40", "W16X36", "W16X31", "W16X26", "W14X730", "W14X665", "W14X605", "W14X550", "W14X500", "W14X455", "W14X426", "W14X398", "W14X370", "W14X342", "W14X311", "W14X283", "W14X257", "W14X233", "W14X211", "W14X193", "W14X176", "W14X159", "W14X145", "W14X132", "W14X120", "W14X109", "W14X99", "W14X90", "W14X82", "W14X74", "W14X68", "W14X61", "W14X53", "W14X48", "W14X43", "W14X38", "W14X34", "W14X30", "W14X26", "W14X22", "W12X336", "W12X305", "W12X279", "W12X252", "W12X230", "W12X210", "W12X190", "W12X170", "W12X152", "W12X136", "W12X120", "W12X106", "W12X96", "W12X87", "W12X79", "W12X72", "W12X65", "W12X58", "W12X53", "W12X50", "W12X45", "W12X40", "W12X35", "W12X30", "W12X26", "W12X22", "W12X19", "W12X16", "W12X14", "W10X112", "W10X100", "W10X88", "W10X77", "W10X68", "W10X60", "W10X54", "W10X49", "W10X45", "W10X39", "W10X33", "W10X30", "W10X26", "W10X22", "W10X19", "W10X17", "W10X15", "W10X12", "W8X67", "W8X58", "W8X48", "W8X40", "W8X35", "W8X31", "W8X28", "W8X24", "W8X21", "W8X18", "W8X15", "W8X13", "W8X10"})
        Me.C08.Items.AddRange(New Object() {"W44X335", "W44X290", "W44X262", "W44X230", "W40X593", "W40X503", "W40X431", "W40X397", "W40X372", "W40X362", "W40X324", "W40X297", "W40X277", "W40X249", "W40X215", "W40X199", "W40X392", "W40X331", "W40X327", "W40X294", "W40X278", "W40X264", "W40X235", "W40X211", "W40X183", "W40X167", "W40X149", "W36X652", "W36X529", "W36X487", "W36X441", "W36X395", "W36X361", "W36X330", "W36X302", "W36X282", "W36X262", "W36X247", "W36X231", "W36X256", "W36X232", "W36X210", "W36X194", "W36X182", "W36X170", "W36X160", "W36X150", "W36X135", "W33X387", "W33X354", "W33X318", "W33X291", "W33X263", "W33X241", "W33X221", "W33X201", "W33X169", "W33X152", "W33X141", "W33X130", "W33X118", "W30X391", "W30X357", "W30X326", "W30X292", "W30X261", "W30X235", "W30X211", "W30X191", "W30X173", "W30X148", "W30X132", "W30X124", "W30X116", "W30X108", "W30X99", "W30X90", "W27X539", "W27X368", "W27X336", "W27X307", "W27X281", "W27X258", "W27X235", "W27X217", "W27X194", "W27X178", "W27X161", "W27X146", "W27X129", "W27X114", "W27X102", "W27X94", "W27X84", "W24X370", "W24X335", "W24X306", "W24X279", "W24X250", "W24X229", "W24X207", "W24X192", "W24X176", "W24X162", "W24X146", "W24X131", "W24X117", "W24X104", "W24X103", "W24X94", "W24X84", "W24X76", "W24X68", "W24X62", "W24X55", "W21X201", "W21X182", "W21X166", "W21X147", "W21X132", "W21X122", "W21X111", "W21X101", "W21X93", "W21X83", "W21X73", "W21X68", "W21X62", "W21X55", "W21X48", "W21X57", "W21X50", "W21X44", "W18X311", "W18X283", "W18X258", "W18X234", "W18X211", "W18X192", "W18X175", "W18X158", "W18X143", "W18X130", "W18X119", "W18X106", "W18X97", "W18X86", "W18X76", "W18X71", "W18X65", "W18X60", "W18X55", "W18X50", "W18X46", "W18X40", "W18X35", "W16X100", "W16X89", "W16X77", "W16X67", "W16X57", "W16X50", "W16X45", "W16X40", "W16X36", "W16X31", "W16X26", "W14X730", "W14X665", "W14X605", "W14X550", "W14X500", "W14X455", "W14X426", "W14X398", "W14X370", "W14X342", "W14X311", "W14X283", "W14X257", "W14X233", "W14X211", "W14X193", "W14X176", "W14X159", "W14X145", "W14X132", "W14X120", "W14X109", "W14X99", "W14X90", "W14X82", "W14X74", "W14X68", "W14X61", "W14X53", "W14X48", "W14X43", "W14X38", "W14X34", "W14X30", "W14X26", "W14X22", "W12X336", "W12X305", "W12X279", "W12X252", "W12X230", "W12X210", "W12X190", "W12X170", "W12X152", "W12X136", "W12X120", "W12X106", "W12X96", "W12X87", "W12X79", "W12X72", "W12X65", "W12X58", "W12X53", "W12X50", "W12X45", "W12X40", "W12X35", "W12X30", "W12X26", "W12X22", "W12X19", "W12X16", "W12X14", "W10X112", "W10X100", "W10X88", "W10X77", "W10X68", "W10X60", "W10X54", "W10X49", "W10X45", "W10X39", "W10X33", "W10X30", "W10X26", "W10X22", "W10X19", "W10X17", "W10X15", "W10X12", "W8X67", "W8X58", "W8X48", "W8X40", "W8X35", "W8X31", "W8X28", "W8X24", "W8X21", "W8X18", "W8X15", "W8X13", "W8X10"})
        Me.C09.Items.AddRange(New Object() {"W44X335", "W44X290", "W44X262", "W44X230", "W40X593", "W40X503", "W40X431", "W40X397", "W40X372", "W40X362", "W40X324", "W40X297", "W40X277", "W40X249", "W40X215", "W40X199", "W40X392", "W40X331", "W40X327", "W40X294", "W40X278", "W40X264", "W40X235", "W40X211", "W40X183", "W40X167", "W40X149", "W36X652", "W36X529", "W36X487", "W36X441", "W36X395", "W36X361", "W36X330", "W36X302", "W36X282", "W36X262", "W36X247", "W36X231", "W36X256", "W36X232", "W36X210", "W36X194", "W36X182", "W36X170", "W36X160", "W36X150", "W36X135", "W33X387", "W33X354", "W33X318", "W33X291", "W33X263", "W33X241", "W33X221", "W33X201", "W33X169", "W33X152", "W33X141", "W33X130", "W33X118", "W30X391", "W30X357", "W30X326", "W30X292", "W30X261", "W30X235", "W30X211", "W30X191", "W30X173", "W30X148", "W30X132", "W30X124", "W30X116", "W30X108", "W30X99", "W30X90", "W27X539", "W27X368", "W27X336", "W27X307", "W27X281", "W27X258", "W27X235", "W27X217", "W27X194", "W27X178", "W27X161", "W27X146", "W27X129", "W27X114", "W27X102", "W27X94", "W27X84", "W24X370", "W24X335", "W24X306", "W24X279", "W24X250", "W24X229", "W24X207", "W24X192", "W24X176", "W24X162", "W24X146", "W24X131", "W24X117", "W24X104", "W24X103", "W24X94", "W24X84", "W24X76", "W24X68", "W24X62", "W24X55", "W21X201", "W21X182", "W21X166", "W21X147", "W21X132", "W21X122", "W21X111", "W21X101", "W21X93", "W21X83", "W21X73", "W21X68", "W21X62", "W21X55", "W21X48", "W21X57", "W21X50", "W21X44", "W18X311", "W18X283", "W18X258", "W18X234", "W18X211", "W18X192", "W18X175", "W18X158", "W18X143", "W18X130", "W18X119", "W18X106", "W18X97", "W18X86", "W18X76", "W18X71", "W18X65", "W18X60", "W18X55", "W18X50", "W18X46", "W18X40", "W18X35", "W16X100", "W16X89", "W16X77", "W16X67", "W16X57", "W16X50", "W16X45", "W16X40", "W16X36", "W16X31", "W16X26", "W14X730", "W14X665", "W14X605", "W14X550", "W14X500", "W14X455", "W14X426", "W14X398", "W14X370", "W14X342", "W14X311", "W14X283", "W14X257", "W14X233", "W14X211", "W14X193", "W14X176", "W14X159", "W14X145", "W14X132", "W14X120", "W14X109", "W14X99", "W14X90", "W14X82", "W14X74", "W14X68", "W14X61", "W14X53", "W14X48", "W14X43", "W14X38", "W14X34", "W14X30", "W14X26", "W14X22", "W12X336", "W12X305", "W12X279", "W12X252", "W12X230", "W12X210", "W12X190", "W12X170", "W12X152", "W12X136", "W12X120", "W12X106", "W12X96", "W12X87", "W12X79", "W12X72", "W12X65", "W12X58", "W12X53", "W12X50", "W12X45", "W12X40", "W12X35", "W12X30", "W12X26", "W12X22", "W12X19", "W12X16", "W12X14", "W10X112", "W10X100", "W10X88", "W10X77", "W10X68", "W10X60", "W10X54", "W10X49", "W10X45", "W10X39", "W10X33", "W10X30", "W10X26", "W10X22", "W10X19", "W10X17", "W10X15", "W10X12", "W8X67", "W8X58", "W8X48", "W8X40", "W8X35", "W8X31", "W8X28", "W8X24", "W8X21", "W8X18", "W8X15", "W8X13", "W8X10"})
        '
        Try
            If gJob Is Nothing Then
                Dim Arr() As String
                gJob = New clsMRSJob(Arr)
            End If
            clsSE = _clsSAP
            ''clsSE.isCanada = _isCanada
            gJob.isEtab = _isEtabs
            If gJob.isEtab Then
                gJob.versionID = "Version ID: ETABS Plug-in" & toolVer
                Me.Text = toolName & toolVer
                btnModifyS2k.Visible = False
            Else
                gJob.versionID = "Version ID: SAP2000 Plug-in" & toolVer
                Me.Text = toolName & toolVer
                btnModifyE2k.Visible = False
            End If
            '
            Me.FileToolStripMenuItem.ShowDropDown() 'help to avoid giat-lag
            btnByGridlines.Checked = Not gJob.isByGroupName
            btnByGroupNames.Checked = gJob.isByGroupName
            '
            isDebug = False
            UpdateDisplay()
            '
            isUserEdit = True

        Catch ex As Exception
            MsgBox(ex.Message & ex.StackTrace,, "Yield-Link� Connection")
        End Try
    End Sub
    Private Sub InitDataForDebug()
        'Debug only
        If isDebug Then
            If SelectedFrames.Count > 0 Then Exit Sub
            Dim ws1 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr1 As New clsSEFrame(5, 65, "G1", "G1", "1", 2, 298, False, 0, 200, 37, 65, ws1, 0, 0, "G1", "G2", 0, 720)
            '
            Dim ws2 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr2 As New clsSEFrame(5, 65, "G1", "G1", "1", 2, 299, False, 0, 150, 65, 93, ws2, 0, 0, "G1", "G2", 200, 720)

            Dim ws3 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr3 As New clsSEFrame(5, 65, "G1", "G1", "1", 2, 82, True, 0, 30 * 12, 56, 37, ws3, 0, 0, "G1", "G2", 0, 360)

            Dim ws4 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr4 As New clsSEFrame(5, 65, "G1", "G1", "2", "3", 123, True, 0, 30 * 12, 84, 65, ws4, 0, 0, "G1", "G2", 200, 360)

            Dim ws5 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr5 As New clsSEFrame(5, 65, "G1", "G1", "2", "3", 164, True, 0, 30 * 12, 112, 93, ws5, 0, 0, "G1", "G2", 350, 360)

            Dim ws6 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr6 As New clsSEFrame(4, 52, "G1", "G1", "2", "3", 294, False, 0, 200, 56, 84, ws6, 0, 0, "G1", "G2", 0, 360)

            Dim ws7 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr7 As New clsSEFrame(4, 52, "G1", "G1", "2", "3", 295, False, 0, 150, 84, 112, ws7, 0, 0, "G1", "G2", 200, 360)

            Dim ws8 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr8 As New clsSEFrame(4, 52, "G1", "G1", "2", "3", 81, True, 0, 30 * 12, 52, 56, ws8, 0, 0, "G1", "G2", 0, 0)

            Dim ws9 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr9 As New clsSEFrame(4, 52, "G1", "G1", "3", "4", 122, True, 0, 30 * 12, 80, 84, ws9, 0, 0, "G1", "G2", 200, 0)

            Dim ws10 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr10 As New clsSEFrame(4, 52, "G1", "G1", "3", "4", 163, True, 0, 30 * 12, 108, 112, ws10, 0, 0, "G1", "G2", 350, 0)

            Dim ws11 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr11 As New clsSEFrame(4, 52, "G1", "G1", "1", "1", 300, False, 0, 300, 93, 3, ws11, 0, 0, "G1", "G2", 350, 720)
            Dim ws12 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr12 As New clsSEFrame(4, 52, "G1", "G1", "2", "2", 296, False, 0, 300, 112, 2, ws12, 0, 0, "G1", "G2", 350, 360)
            Dim ws13 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr13 As New clsSEFrame(4, 52, "G1", "G1", "3", "3", 205, True, 0, 30 * 12, 2, 3, ws13, 0, 0, "G1", "G2", 650, 360)
            Dim ws14 As Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = ("W18X192"))
            Dim fr14 As New clsSEFrame(4, 52, "G1", "G1", "4", "4", 204, True, 0, 30 * 12, 1, 2, ws14, 0, 0, "G1", "G2", 650, 0)
            '
            SelectedFrames.Add(fr1)
            SelectedFrames.Add(fr2)
            SelectedFrames.Add(fr3)
            SelectedFrames.Add(fr4)
            SelectedFrames.Add(fr5)
            SelectedFrames.Add(fr6)
            SelectedFrames.Add(fr7)
            SelectedFrames.Add(fr8)
            SelectedFrames.Add(fr9)
            SelectedFrames.Add(fr10)
            SelectedFrames.Add(fr11)
            SelectedFrames.Add(fr12)
            SelectedFrames.Add(fr13)
            SelectedFrames.Add(fr14)
        End If
    End Sub
    Sub UpdateDisplay()
        Dim dgv As DataGridView
        If tabMain.SelectedIndex = 1 Then : dgv = dgvStep2
        ElseIf tabMain.SelectedIndex = 2 Then : dgv = dgvStep3
        ElseIf tabMain.SelectedIndex = 3 Then : dgv = dgvStep4
        Else
            dgv = dgvStep1
        End If
        Try
            '
            lbStatus.Text = "Working..."
            Cursor = Cursors.WaitCursor
            dgv.Visible = False
            SelectedFrames.Clear()
            '' ''SelectedBeams.Clear()
            SelectedColumns.Clear()
            '
            InitDataForDebug()
            'Update all steps
            Update_Step1()
            'Get previous force
            Dim Str = Get_IntForces(False)
            Update_Step2(True)
            ''Update_Step3(True) 'add to step 1
            Update_Step4(True)
            Update_Step5()
            'Update_Step6()
            lblReactionUpdate.Text = Str
            BtnDesign.Enabled = clsSE.isModelLocked
        Catch ex As Exception
            'lbStatus.Text = "Error: " & ex.Message.Split(".")(0)
            MsgBox(ex.Message & ex.StackTrace)
        Finally
            Cursor = Cursors.Arrow
            dgv.Visible = True
            dgv = Nothing
            BringWindowToTop(Me.Handle)
        End Try
    End Sub
    'Initial Link Selection
    Private Sub Update_Step1(Optional isAdd2ColList As Boolean = True)

        Dim oldElevation As String = cbElevStep1.Text
        Dim oldStory As String = cbStoryStep1.Text
        '
        If Not isKeepRows Then
            dgvStep1.Rows.Clear()
            '
            cbElevStep1.Items.Clear()
            cbElevStep1.Items.Add("All Elevations")
            cbStoryStep1.Items.Clear()
            cbStoryStep1.Items.Add("All Stories")
        End If
        '
        Dim tmpBmIDs As New List(Of String)
        Dim i, col, fIndex As Integer
        Dim NoConn As Integer
        Dim changedBeams As String = ""
        '
        If isAdd2ColList Then
            Try
                clsSE.GetSelectedFrames(gJob, SelectedFrames)
            Catch ex As Exception
            End Try
            ''Dim SSTBm As clsSSTBeam
            Dim saveBm As modObj = Nothing
            Dim lkinfo As LinkInfo
            Dim brpi As BRPInfo
            'Dim SMFi, SMFj As Integer
            Dim isKi, isKj As Boolean
            Dim tmpBotColi, tmpBotColj, tmpTopColi, tmpTopColj As clsSEFrame
            Dim isPinned_I, isPinned_J As Boolean

            'Order by Level top to bottom
            ''SelectedFrames.Sort(Function(x, y) x.LevelHeight.CompareTo(y.LevelHeight))
            ''SelectedFrames.Reverse()
            SelectedFrames = SelectedFrames.OrderBy(Function(x) x.ElevID).ThenByDescending(Function(x) x.LevelHeight).ThenBy(Function(x) x.GridID_LeftSide).ToList
            '
            For Each fr As clsSEFrame In SelectedFrames
                If fr.isColumn = False Then
                    lkinfo = Nothing

                    Dim lkEndI As LinkInfo = clsSE.Get_Link(gJob, fr.oldKi, fr.Wsec.Depth, fr._uName, changedBeams)
                    ' MsgBox(lkEndI.Size)
                    isKi = False
                    Dim lkEndJ As LinkInfo = clsSE.Get_Link(gJob, fr.oldKj, fr.Wsec.Depth, fr._uName, changedBeams)
                    isKj = False
                    isPinned_I = False
                    isPinned_J = False
                    NoConn = 0
                    '
                    If lkEndI IsNot Nothing Then
                        lkinfo = lkEndI
                        isPinned_I = lkEndI.Size = "Pinned"
                        isKi = Not isPinned_I
                        If Not isPinned_I Then NoConn += 1
                    End If
                    If lkEndJ IsNot Nothing Then
                        isPinned_J = lkEndJ.Size = "Pinned"
                        isKj = Not isPinned_J
                        If Not isPinned_J Then
                            lkinfo = lkEndJ 'loi tu dong ta ve pinned neu chon pinn @right end
                            NoConn += 1
                        End If
                    End If
                    '
                    If isPinned_I And isPinned_J Then
                        isKi = False
                        isKj = False
                        NoConn = 0
                    End If
                    'Case user inputted different K
                    If lkinfo Is Nothing Then
                        lkinfo = gJob.SSTLinks.Links(11) 'default YL6-3
                        If fr.oldKi > 0 Then isKi = True
                        If fr.oldKj > 0 Then isKj = True
                    End If
                    '
                    brpi = gJob.SSTLinks.BRPs.Find(Function(x) x.Size = lkinfo.Size)
                    'Bottom Column
                    tmpBotColi = SelectedFrames.Find(Function(x) x.isColumn AndAlso (x.ElevID = fr.ElevID Or x.ElevID_WeakAxis = fr.ElevID) AndAlso x.Pt_J = fr.Pt_I) 'can add Elev ID condition?

                    tmpBotColj = SelectedFrames.Find(Function(x) x.isColumn AndAlso (x.ElevID = fr.ElevID Or x.ElevID_WeakAxis = fr.ElevID) AndAlso x.Pt_J = fr.Pt_J)
                    'Top Column
                    tmpTopColi = SelectedFrames.Find(Function(x) x.isColumn AndAlso (x.ElevID = fr.ElevID Or x.ElevID_WeakAxis = fr.ElevID) AndAlso x.Pt_I = fr.Pt_I)
                    tmpTopColj = SelectedFrames.Find(Function(x) x.isColumn AndAlso (x.ElevID = fr.ElevID Or x.ElevID_WeakAxis = fr.ElevID) AndAlso x.Pt_I = fr.Pt_J)
                    '
                    If tmpBotColi Is Nothing Or tmpBotColj Is Nothing Then
                        dgvStep1.Rows.Clear()
                        Exit For
                    Else
                        'SSTBm = New clsSSTBeam(fr.StyID, fr.ElevID, fr.GridID, fr._uName, fr.Len_Cal, fr.Wsec, fr.Pt_I, fr.Pt_J, lkinfo, brpi, tmpBotColi, tmpBotColj, tmpTopColi, tmpTopColj, isKi, isKj)
                        tmpBmIDs.Add(fr._uName)
                        fIndex = SelectedBeams.FindIndex(Function(x) x._uName = fr._uName)
                        Dim IsIWeak, IsJWeak As Boolean
                        IsIWeak = clsSE.IsWeakAxis(fr._uName, tmpBotColi._uName)
                        IsJWeak = clsSE.IsWeakAxis(fr._uName, tmpBotColj._uName)
                        If fIndex < 0 Then
                            SelectedBeams.Add(New clsSSTBeam(fr.StyID, fr.ElevID, fr.GridID_LeftSide, fr.GridID_RightSide, fr._uName, fr.Len_Cal, fr.Wsec, fr.Pt_I, fr.Pt_J,
                                                             lkinfo, brpi, tmpBotColi, tmpBotColj, tmpTopColi, tmpTopColj, isKi, isKj, fr.LevelHeight, NoConn, fr.sPt_X, fr.sPt_Y, IsIWeak, IsJWeak, fr.Angle))
                        Else
                            SelectedBeams(fIndex).UpdateBeamData(fr.StyID, fr.ElevID, fr.GridID_LeftSide, fr.GridID_RightSide, fr._uName, fr.Len_Cal, fr.Wsec, fr.Pt_I, fr.Pt_J,
                                                               SelectedBeams(fIndex).LKInfo, SelectedBeams(fIndex).BRPInfo, tmpBotColi, tmpBotColj, tmpTopColi,
                                                               tmpTopColj, SelectedBeams(fIndex).IsAsignEndI_K, SelectedBeams(fIndex).IsAsignEndJ_K, SelectedBeams(fIndex).LevelHeight, NoConn, fr.sPt_X, fr.sPt_Y, IsIWeak, IsJWeak)
                        End If

                    End If
                End If
            Next
            '
            If changedBeams <> "" Then
                MsgBox("Some beams have been changed outside of this plugin UI." & vbLf &
                       "The End Released values are not still correct" & vbLf & "Please re-assign K value for """ & changedBeams.Remove(changedBeams.Length - 2, 2) & """",
                       MsgBoxStyle.Exclamation, toolName)
            End If
            '
            For i = SelectedBeams.Count - 1 To 0 Step -1
                If Not tmpBmIDs.Contains(SelectedBeams(i)._uName) Then
                    SelectedBeams.RemoveAt(i)
                End If
            Next
            ''SelectedBeams.Sort(Function(x, y) x.LevelHeight.CompareTo(y.LevelHeight))
            ''SelectedBeams.Reverse()
            SelectedBeams = SelectedBeams.OrderBy(Function(x) x.ElevID).ThenByDescending(Function(x) x.LevelHeight).ThenBy(Function(x) x.GridID_LeftSide).ToList
        End If
        '
        'Update Pz
        Update_Step3(isAdd2ColList)
        'Update Drift
        Update_Step5()
        '
        For i = 0 To SelectedBeams.Count - 1
            '
            SelectedBeams(i).Step1_InitialDesign(gJob)
            '
            If SelectedBeams(i).IsAsignEndI_K = False AndAlso SelectedBeams(i).IsAsignEndJ_K = False Then
                SelectedBeams(i).status_endIPz = NA
                SelectedBeams(i).status_endJPz = NA
            Else
                SelectedBeams(i).status_endIPz = SelectedColumns.Find(Function(x) x._uName = SelectedBeams(i).EndI_ColBottom._uName).statusPZinitial
                SelectedBeams(i).status_endJPz = SelectedColumns.Find(Function(x) x._uName = SelectedBeams(i).EndJ_ColBottom._uName).statusPZinitial
            End If
            'Update No of connection
            NoConn = 0
            If SelectedBeams(i).IsAsignEndI_K Then NoConn += 1
            If SelectedBeams(i).IsAsignEndJ_K Then NoConn += 1
            SelectedBeams(i).NoConn = NoConn
            Dim maxDriftDCR As String
            Dim valSeismicDrift, valWindDCR As Double
            Double.TryParse(SelectedBeams(i).status_SeismicDrift, valSeismicDrift)
            Double.TryParse(SelectedBeams(i).status_WindDrift, valWindDCR)
            If valSeismicDrift > valWindDCR Then
                maxDriftDCR = SelectedBeams(i).status_SeismicDrift
            ElseIf valSeismicDrift = 0 And valWindDCR = 0 Then
                maxDriftDCR = NA
            Else
                maxDriftDCR = SelectedBeams(i).status_WindDrift
            End If
            '
            If isKeepRows Then
                'Bi sai vi tri neu User chon sort tren UI
                col = -1
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).ElevID
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).GridID_LeftSide
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).StyID
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i)._uName
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).Wsec.Size
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).LKInfo.Size
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = NumberFormat(SelectedBeams(i).K_rot, 0)
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).EndI_ColBottom.Wsec.Size
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).EndJ_ColBottom.Wsec.Size
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).IsAsignEndI_K
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).IsAsignEndJ_K
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).status_tbf
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).status_bf
                dgvStep1.Rows(i).Cells(col).ToolTipText = IIf(SelectedBeams(i).status_ClipWasher Like "R*", "Clip washers are required", "")
                '
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).status_Lyield
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).status_endIPz
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).status_endJPz

                col += 1 : dgvStep1.Rows(i).Cells(col).Value = maxDriftDCR
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).EndI_ColBottom._uName
                col += 1 : dgvStep1.Rows(i).Cells(col).Value = SelectedBeams(i).EndJ_ColBottom._uName
            Else
                dgvStep1.Rows.Add(SelectedBeams(i).ElevID, SelectedBeams(i).GridID_LeftSide, SelectedBeams(i).StyID, SelectedBeams(i)._uName, SelectedBeams(i).Wsec.Size, SelectedBeams(i).LKInfo.Size,
                                  NumberFormat(SelectedBeams(i).K_rot, 0), SelectedBeams(i).EndI_ColBottom.Wsec.Size, SelectedBeams(i).EndJ_ColBottom.Wsec.Size,
                                  SelectedBeams(i).IsAsignEndI_K, SelectedBeams(i).IsAsignEndJ_K, SelectedBeams(i).status_tbf,
                                  SelectedBeams(i).status_bf, SelectedBeams(i).status_Lyield, SelectedBeams(i).status_endIPz,
                                  SelectedBeams(i).status_endJPz, maxDriftDCR, SelectedBeams(i).EndI_ColBottom._uName, SelectedBeams(i).EndJ_ColBottom._uName)
                If (SelectedBeams(i).status_ClipWasher Like "R*") Then
                    dgvStep1(12, i).ToolTipText = "Clip washers are required"
                End If
                If Not cbElevStep1.Items.Contains(SelectedBeams(i).ElevID) Then
                    cbElevStep1.Items.Add(SelectedBeams(i).ElevID)
                End If
                If Not cbStoryStep1.Items.Contains(SelectedBeams(i).StyID) Then
                    cbStoryStep1.Items.Add(SelectedBeams(i).StyID)
                End If
            End If
        Next
        '
        If cbElevStep1.Items.Contains(oldElevation) Then
            cbElevStep1.Text = oldElevation
        ElseIf cbElevStep1.Items.Count > 0 Then
            cbElevStep1.SelectedIndex = 0
        End If
        If cbStoryStep1.Items.Contains(oldStory) Then
            cbStoryStep1.Text = oldStory
        ElseIf cbStoryStep1.Items.Count > 0 Then
            cbStoryStep1.SelectedIndex = 0
        End If
        '
        If SelectedBeams.Count > 0 Then
            lbStatus.Text = "Total =" & SelectedBeams.Count & " Beams/Columns"
        Else
            lbStatus.Text = IIf(isUserEdit, "Invalid selection/ Invalid sections. Please select valid Moment frame in the ETABS/SAP UI first", "Ready")
        End If
        ' 
        Update_StatusColor_LastColumn(1)
    End Sub
    'Beam & Link Check
    Private Sub Update_Step2(Optional loadNew As Boolean = False)
        dgvStep2.Rows.Clear()
        For Each bm As clsSSTBeam In SelectedBeams
            If bm.IsAsignEndI_K = False AndAlso bm.IsAsignEndJ_K = False Then Continue For
            '
            bm.Step2_BeamLinkCheck(gJob)
            '
            dgvStep2.Rows.Add(bm.ElevID, bm.GridID_LeftSide, bm.StyID, bm._uName, bm.Wsec.Size, bm.LKInfo.Size, bm.BRPInfo.Size_BRP, NumberFormat(bm.Mu_max, 2),
                               bm.status_bmFlgThk, bm.status_linkDCR, bm.status_Lyield, bm.status_brptDCR, bm.status_brpBolts) ', bm.status_S2)
        Next
        '

        '
        Update_StatusColor_LastColumn(2)
    End Sub
    'Column Check
    Private Sub Update_Step3(Optional loadNew As Boolean = False)
        Dim oldElevation As String = cbElevStep3.Text
        Dim oldStory As String = cbStoryStep3.Text
        '
        cbElevStep3.Items.Clear()
        cbElevStep3.Items.Add("All Elevations")
        cbStoryStep3.Items.Clear()
        cbStoryStep3.Items.Add("All Stories")
        '
        dgvStep3.Rows.Clear()
        '
        Dim col As clsSSTColumn
        Dim saveCol As modObj = Nothing
        ''Dim topStiff As eThickness
        Dim topStiffThk, botStiffThk, DoublerPLThk As eThickness
        Dim BmLeft, BmRight As clsSSTBeam, Col_Top As clsSEFrame, Hcc_top As Double
        Dim lk_left As String = "-", lk_right As String = "-", EleId As String = "NA", GridID As String = "NA"
        Dim LeftBmSize, RightBmSize, LeftBmLh, RightBmLh, LeftBmVuGravity, RightBmVuGravity, leftBmUname, RightBmUname As String
        Dim maxVu_bmGravity As Double
        Dim isHasLink As Boolean
        Dim NoConnLeft, NoConnRight As Integer
        '
        If loadNew Then
            For Each fr As clsSEFrame In SelectedFrames
                If fr.isColumn Then
                    '\\ saveCol = saveInfo.modObjs.Find(Function(x) x._uName = fr._uName)
                    'col=New clsSSTColumn(fr.StyID,fr.GridID,
                    If Not saveCol Is Nothing Then
                        topStiffThk = saveCol.TopStiffThk
                        botStiffThk = saveCol.BotStiffThk
                        DoublerPLThk = saveCol.DoublerPLThk
                    Else
                        'topStiff = eThickness.T3_8
                        topStiffThk = eThickness.T3_8
                        botStiffThk = eThickness.T3_8
                        DoublerPLThk = eThickness.NA
                    End If
                    '
                    BmLeft = SelectedBeams.Find(Function(x) x.Pt_J = fr.Pt_J AndAlso x.ElevID <> "NA" AndAlso x.ElevID = fr.ElevID)
                    BmRight = SelectedBeams.Find(Function(x) x.Pt_I = fr.Pt_J AndAlso x.ElevID <> "NA" AndAlso x.ElevID = fr.ElevID)
                    '
                    lk_left = "Pinned"
                    lk_right = "Pinned"
                    LeftBmSize = "NA"
                    RightBmSize = "NA"
                    LeftBmLh = "NA"
                    RightBmLh = "NA"
                    LeftBmVuGravity = "0"
                    RightBmVuGravity = "0"
                    leftBmUname = "NA"
                    RightBmUname = "NA"
                    isHasLink = False
                    NoConnLeft = 0
                    NoConnRight = 0
                    EleId = "NA"

                    If BmLeft Is Nothing Then
                        lk_left = "NA"
                    Else
                        NoConnLeft = BmLeft.NoConn
                        EleId = BmLeft.ElevID
                        GridID = BmLeft.GridID_RightSide
                        '
                        If BmLeft.IsAsignEndJ_K Then
                            lk_left = BmLeft.LKInfo.Size
                            If lk_left = "Pinned" Then
                                lk_left = "NA"
                            End If
                            LeftBmSize = BmLeft.Wsec.Size
                            LeftBmLh = NumberFormat(BmLeft.Len_Cal, 2)
                            LeftBmVuGravity = Format(BmLeft.V_bmGravity, "0.##")
                            leftBmUname = BmLeft._uName
                            isHasLink = True
                        End If
                    End If
                    '
                    If BmRight Is Nothing Then
                        lk_right = "NA"
                    Else
                        NoConnRight = BmRight.NoConn
                        EleId = BmRight.ElevID
                        GridID = BmRight.GridID_LeftSide
                        '
                        If BmRight.IsAsignEndI_K Then
                            lk_right = BmRight.LKInfo.Size
                            If lk_right = "Pinned" Then
                                lk_right = "NA"
                            End If
                            RightBmSize = BmRight.Wsec.Size
                            RightBmLh = NumberFormat(BmRight.Len_Cal, 2)
                            RightBmVuGravity = Format(BmRight.V_bmGravity, "0.##")
                            RightBmUname = BmRight._uName
                            isHasLink = True
                            ''NoConnRight += 1
                        End If
                    End If
                    'Update elevation ID and grid ID for colulumn
                    fr.ElevID = EleId
                    fr.GridID_LeftSide = GridID
                    '
                    col = New clsSSTColumn(fr.StyID, fr.ElevID, fr.GridID_LeftSide, fr._uName, fr.Len_Cal, fr.Wsec, fr.Pt_I, fr.Pt_J,
                                           topStiffThk, DoublerPLThk, BmLeft, BmRight, 0, fr.StyAbove, fr.topSty, fr.Angle)
                    'get Top column
                    Col_Top = SelectedFrames.Find(Function(x) x.isColumn AndAlso x.Pt_I = col.Pt_J)
                    If Col_Top Is Nothing Then
                        Hcc_top = 0
                    Else
                        Hcc_top = Col_Top.Len_Cal
                    End If
                    col.Hcc_Top = Hcc_top
                    col.CC_Detail_US(gJob)

                    '
                    SelectedColumns.Add(col)
                    '
                    maxVu_bmGravity = 0
                    If Not col.BmLeft Is Nothing Then
                        maxVu_bmGravity = col.BmLeft.V_bmGravity
                    End If
                    If Not col.BmRight Is Nothing Then
                        maxVu_bmGravity = Math.Max(maxVu_bmGravity, col.BmRight.V_bmGravity)
                    End If
                    '
                    If isHasLink = False Then Continue For
                    '
                    col.isBottom = SelectedBeams.Find(Function(x) x.Pt_I = fr.Pt_I) Is Nothing AndAlso
                                   SelectedBeams.Find(Function(x) x.Pt_J = fr.Pt_I) Is Nothing
                    'if required, alwat set to YES, user cannot change
                    If col.stiffener_Required = "YES" Then
                        col.ProvideStiff = "YES"
                    End If
                    If col.DoublerPLThk > 0 Then
                        col.ProvideDblPL = "YES"
                    End If
                    '
                    dgvStep3.Rows.Add(col.ElevID, col.GridID_LeftSide, col.StyID, col._uName, col.Wsec.Size,
                                      lk_left, lk_right, NumberFormat(col.Pu_col_omega, 2), NumberFormat(maxVu_bmGravity, 2), col.ProvideStiff,
                                       col.ProvideDblPL, col.stiffener_Required, "0.000", "0.000", col.status_SCWB,
                                      col.status_Pz, col.status_ColFlg, NumberFormat(col.Len_Cal, 2), NumberFormat(col.Hcc_Top, 2), LeftBmSize,
                                      NoConnLeft, LeftBmLh, LeftBmVuGravity, RightBmSize, NoConnRight,
                                      RightBmLh, RightBmVuGravity, col.StyAbove, col.topSty, leftBmUname,
                                      RightBmUname)
                    'Update filter list
                    If Not cbElevStep3.Items.Contains(col.ElevID) Then
                        cbElevStep3.Items.Add(col.ElevID)
                    End If
                    If Not cbStoryStep3.Items.Contains(col.StyID) Then
                        cbStoryStep3.Items.Add(col.StyID)
                    End If
                End If
            Next
        Else
            For Each col In SelectedColumns
                maxVu_bmGravity = 0
                lk_left = "Pinned"
                lk_right = "Pinned"
                LeftBmSize = "NA"
                RightBmSize = "NA"
                LeftBmLh = "NA"
                RightBmLh = "NA"
                LeftBmVuGravity = "0"
                RightBmVuGravity = "0"
                leftBmUname = "NA"
                RightBmUname = "NA"
                isHasLink = False
                NoConnLeft = 0
                NoConnRight = 0
                '
                If col.BmLeft Is Nothing Then
                    lk_left = "NA"
                Else
                    NoConnLeft = col.BmLeft.NoConn
                    '
                    If col.BmLeft.IsAsignEndJ_K Then
                        lk_left = col.BmLeft.LKInfo.Size
                        If lk_left = "Pinned" Then
                            lk_left = "NA"
                        End If
                        isHasLink = True
                        ''NoConnLeft += 1
                    End If
                End If
                If Not col.BmLeft Is Nothing Then
                    maxVu_bmGravity = col.BmLeft.V_bmGravity
                    LeftBmSize = col.BmLeft.Wsec.Size
                    LeftBmLh = NumberFormat(col.BmLeft.Len_Cal, 2)
                    LeftBmVuGravity = Format(col.BmLeft.V_bmGravity, "0.##")
                    leftBmUname = col.BmLeft._uName
                End If

                If col.BmRight Is Nothing Then
                    lk_right = "NA"
                Else
                    NoConnRight = col.BmRight.NoConn
                    '
                    If col.BmRight.IsAsignEndI_K Then
                        lk_right = col.BmRight.LKInfo.Size
                        If lk_right = "Pinned" Then
                            lk_right = "NA"
                        End If
                        isHasLink = True
                        ''NoConnRight += 1
                    End If
                End If
                If Not col.BmRight Is Nothing Then
                    maxVu_bmGravity = Math.Max(maxVu_bmGravity, col.BmRight.V_bmGravity)
                    RightBmSize = col.BmRight.Wsec.Size
                    RightBmLh = NumberFormat(col.BmRight.Len_Cal, 2)
                    RightBmVuGravity = Format(col.BmRight.V_bmGravity, "0.##")
                    RightBmUname = col.BmRight._uName
                End If
                '
                If isHasLink = False Then Continue For
                '
                Col_Top = SelectedFrames.Find(Function(x) x.isColumn AndAlso x.Pt_I = col.Pt_J)
                If Col_Top Is Nothing Then
                    Hcc_top = 0
                Else
                    Hcc_top = Col_Top.Len_Cal
                End If
                col.Hcc_Top = Hcc_top
                col.CC_Detail_US(gJob)
                '
                If col.stiffener_Required = "YES" Then
                    col.ProvideStiff = "YES"
                End If
                If col.DoublerPLThk > 0 Then
                    col.ProvideDblPL = "YES"
                End If
                ''min stiff plate
                Dim strStiff, strDblr As String
                If col.StiffMinThk < 0.1 And col.ProvideStiff = "YES" Then
                    strStiff = NumberFormat(MaxAll(gJob.MaterialInfo.StiffPL_DefThk, col.StiffMinThk_perLink, col.StiffMin_use), (3))
                Else
                    strStiff = NumberFormat(col.StiffMinThk, (3))
                End If
                ''min double plate
                If col.DoublerPLThk < 0.1 And col.ProvideDblPL = "YES" Then
                    strDblr = NumberFormat(col.DoublerPLThk_Geometry, (3))
                Else
                    strDblr = NumberFormat(col.DoublerPLThk, (3))
                End If
                '
                dgvStep3.Rows.Add(col.ElevID, col.GridID_LeftSide, col.StyID, col._uName, col.Wsec.Size,
                                  lk_left, lk_right, NumberFormat(col.Pu_col_omega, 2), NumberFormat(maxVu_bmGravity, 2), col.ProvideStiff,
                                  col.ProvideDblPL, col.stiffener_Required,
                                  strStiff, strDblr, col.status_SCWB, col.status_Pz, col.status_ColFlg, col.Len_Cal.ToString, NumberFormat(col.Hcc_Top, 2), LeftBmSize,
                                  NoConnLeft, LeftBmLh, LeftBmVuGravity, RightBmSize, NoConnRight,
                                  RightBmLh, RightBmVuGravity, col.StyAbove, col.topSty, leftBmUname,
                                  RightBmUname)
                'Change the dropdown list
                'stiff
                Dim tmpCell As DataGridViewComboBoxCell = dgvStep3(9, dgvStep3.Rows.Count - 1)
                tmpCell.Items.Clear()
                '
                If col.StiffMinThk > 0.2 Then
                    tmpCell.Items.AddRange("YES")
                Else
                    tmpCell.Items.AddRange("YES", "NO")
                End If
                'Doubler
                Dim tmpCell2 As DataGridViewComboBoxCell = dgvStep3(10, dgvStep3.Rows.Count - 1)
                tmpCell2.Items.Clear()
                If col.DoublerPLThk > 0.2 Then
                    tmpCell2.Items.AddRange("YES")
                Else
                    tmpCell2.Items.AddRange("YES", "NO")
                End If
                'Update filter list
                If Not cbElevStep3.Items.Contains(col.ElevID) Then
                    cbElevStep3.Items.Add(col.ElevID)
                End If
                If Not cbStoryStep3.Items.Contains(col.StyID) Then
                    cbStoryStep3.Items.Add(col.StyID)
                End If
            Next
        End If
        '
        If cbElevStep3.Items.Contains(oldElevation) Then
            cbElevStep3.Text = oldElevation
        ElseIf cbElevStep3.Items.Count > 0 Then
            cbElevStep3.SelectedIndex = 0
        End If
        If cbStoryStep3.Items.Contains(oldStory) Then
            cbStoryStep3.Text = oldStory
        ElseIf cbStoryStep3.Items.Count > 0 Then
            cbStoryStep3.SelectedIndex = 0
        End If
        '
        Update_StatusColor_LastColumn(3)
    End Sub
    'Sheartab Check
    Private Sub Update_Step4(Optional loadNew As Boolean = False)
        Dim tmpList As New SortedList(Of String, String)
        For rr As Integer = 0 To dgvStep4.Rows.Count - 1
            tmpList.Add(dgvStep4(3, rr).Value,
                        dgvStep4(9, rr).Value & gJob.splChar & dgvStep4(10, rr).Value & gJob.splChar & dgvStep4(11, rr).Value & gJob.splChar & dgvStep4(12, rr).Value)
        Next
        '
        dgvStep4.Rows.Clear()
        ' ''
        Dim saveBm As modObj = Nothing
        Dim N_horzB As Integer
        Dim bSize As bSize
        Dim bTyp As bType
        Dim STThick, WeldThk As eThickness
        '
        If loadNew Then
            For Each bm As clsSSTBeam In SelectedBeams
                If saveBm Is Nothing Then
                    If tmpList.ContainsKey(bm._uName) Then
                        Dim Arr() As String = (tmpList(bm._uName).Split(gJob.splChar))
                        Integer.TryParse(Arr(0), N_horzB)
                        bSize = String2bSize(Arr(1))
                        bTyp = String2bType(Arr(2))
                        STThick = String2eThickness(Arr(3))
                        WeldThk = Math.Ceiling(5 / 8 * STThick)
                    Else
                        N_horzB = 2
                        bSize = mPubFunc.bSize.B7_8
                        bTyp = bType.A325X
                        STThick = eThickness.T1_2
                        WeldThk = Math.Ceiling(5 / 8 * STThick)
                    End If
                Else
                    N_horzB = saveBm.N_horzHole
                    bSize = saveBm.boltSize
                    bTyp = saveBm.boltType
                    STThick = saveBm.SheartabThickness
                    WeldThk = saveBm.WeldSize
                End If
                'Dim strW3 As String = Microsoft.VisualBasic.Left(bm.Wsec.Size, 3)
                Dim stBolt As SheartabBolt = FindShearTabBolt(bm.Wsec.Size, gJob, bm.Wsec.Depth) ' gJob.SSTOthers.SheartabBolts.Find(Function(x) x.Size = strW3)
                If stBolt Is Nothing Then
                    lbStatus.Text = "Error: Cannot find sheartab bolts for " & bm.Wsec.Size
                    Exit For
                End If
                ' change default Svert and Shorz shear bolt
                'stBolt.Svert = Ceiling(2.6667 * stBolt.db_sp, 1 / 16)
                'stBolt.Shorz = Ceiling(3 * stBolt.db_sp, 1 / 16)
                '
                ''Vu_Link = 2 * bm.LKInfo.Pr_link * (bm.Wsec.Depth + bm.LKInfo.tstem) / bm.Len_Cal  '2*Mpr/Lh
                bm.Sheartab = New clsSSTSheartab(bm.Wsec, bm.LKInfo, stBolt, N_horzB, bSize, bTyp, STThick, WeldThk, bm.Pu_bm, bm.V_bmGravity)
                '
                bm.SPC_Detail(gJob)
                '
                dgvStep4.Rows.Add(bm.ElevID, bm.GridID_LeftSide, bm.StyID, bm._uName, bm.Wsec.Size, bm.LKInfo.Size, NumberFormat(bm.Pu_bm, 2), NumberFormat(bm.V_bmGravity, 2),
                                  bm.Sheartab.stBolt.n_Vbolts_SST, bm.Sheartab.N_horzBolt.ToString, GetBoltSize(bm.Sheartab.bSize), bm.Sheartab.bType.ToString,
                                  eThickness2String(bm.Sheartab.Thk), eThickness2String(bm.Sheartab.WeldSize),
                                  bm.Sheartab.sDCR1_BmWeb, bm.Sheartab.sDCR2_ShearPLGeometry, bm.Sheartab.sDCR3_ShearPL, bm.Sheartab.sDCR4_Bolt, bm.Sheartab.sDCR5_Weld,
                                   IIf(bm.IsAsignEndI_K, "YES", "NO"), IIf(bm.IsAsignEndJ_K, "YES", "NO"), bm.Len_LCC, bm.Len_Cal)
            Next
        Else
            For Each bm As clsSSTBeam In SelectedBeams
                If bm.IsAsignEndI_K = False AndAlso bm.IsAsignEndJ_K = False Then Continue For
                '
                'bm.Sheartab.Pu = bm.Pu_bm
                'bm.Sheartab.V_Gravity = bm.V_bmGravity
                'bm.Sheartab.CheckAll()
                bm.SPC_Detail(gJob)
                ' ''Vu_Link = 2 * bm.LKInfo.Pr_link * (bm.Wsec.Depth + bm.LKInfo.tstem) / bm.Len_Cal  '2*Mpr/Lh
                dgvStep4.Rows.Add(bm.ElevID, bm.GridID_LeftSide, bm.StyID, bm._uName, bm.Wsec.Size, bm.LKInfo.Size, NumberFormat(bm.Pu_bm, 2), NumberFormat(bm.V_bmGravity, 2),
                                  bm.Sheartab.stBolt.n_Vbolts_SST, bm.Sheartab.N_horzBolt.ToString, GetBoltSize(bm.Sheartab.bSize), bm.Sheartab.bType.ToString,
                                     eThickness2String(bm.Sheartab.Thk), eThickness2String(bm.Sheartab.WeldSize),
                                     bm.Sheartab.sDCR1_BmWeb, bm.Sheartab.sDCR2_ShearPLGeometry, bm.Sheartab.sDCR3_ShearPL, bm.Sheartab.sDCR4_Bolt, bm.Sheartab.sDCR5_Weld,
                                     IIf(bm.IsAsignEndI_K, "YES", "NO"), IIf(bm.IsAsignEndJ_K, "YES", "NO"), bm.Len_LCC, bm.Len_Cal)

            Next
        End If
        '
        tmpList = Nothing
        '
        Update_StatusColor_LastColumn(4)
    End Sub
    'Drift Summary
    Private Sub Update_Step5(Optional rIndex As Integer = -1)
        isUserEdit = False
        '
        Dim disp As Double = 0, dX_seismic As Double = 0, dX_wind As Double = 0, H_use As Double
        Dim col As clsSEFrame
        Dim tmpVal As Double
        Dim tmpBeams As New List(Of clsSSTBeam)
        If rIndex < 0 Then
            dgvStep5.Rows.Clear()
        End If
        'Dim ret As Long
        For i As Integer = 0 To SelectedBeams.Count - 1
            If rIndex >= 0 AndAlso SelectedBeams(i)._uName <> dgvStep5(3, rIndex).Value Then Continue For
            '
            If SelectedBeams(i).IsAsignEndI_K = False AndAlso SelectedBeams(i).IsAsignEndJ_K = False Then
                SelectedBeams(i).status_SeismicDrift = NA
                Continue For
            End If
            '
            col = SelectedFrames.Find(Function(x) x.isColumn AndAlso x.Pt_J = SelectedBeams(i).Pt_I)
            SelectedBeams(i).HeightDrift = col.Len_Cal
            If SelectedBeams(i).Height_user <= 0 Or Math.Abs(SelectedBeams(i).HeightDrift - SelectedBeams(i).Height_user) < 1 Then
                H_use = SelectedBeams(i).HeightDrift
                SelectedBeams(i).Height_user = -1
            Else
                H_use = SelectedBeams(i).Height_user
            End If
            '
            SelectedBeams(i).aSeismicDriftLimit = (H_use * GetSeismicDriftLimit()) / If(gJob.DCRLimits.DivideByRho, gJob.Rho, 1)

            Dim nodeBelow As String
            If SelectedBeams(i).NodeBelow_user = "" Then
                nodeBelow = col.Pt_I
            ElseIf SelectedBeams(i).NodeBelow_user = col.Pt_I Then
                nodeBelow = nodeBelow = col.Pt_I
                SelectedBeams(i).NodeBelow_user = ""
            Else
                nodeBelow = SelectedBeams(i).NodeBelow_user
            End If
            'get data
            Dim nodeBelowOK As Boolean = True
            If clsSE.isModelLocked Then
                If clsSE.GetLineDrift(SelectedBeams(i)._uName, SelectedBeams(i).Pt_I, nodeBelow, disp, dX_seismic, True) = -2 Then
                    lbStatus.Text = "Error: Node below is invalid"
                    nodeBelowOK = False
                Else
                    'get wind drift
                    clsSE.GetLineDrift(SelectedBeams(i)._uName, SelectedBeams(i).Pt_I, nodeBelow, disp, dX_wind, False)
                    lbStatus.Text = "Updated"
                End If
            Else
                lbStatus.Text = "Warning: Please ""Run Analysis"" to get correct Drift Summary"
            End If
            'delta_x include Cd and Ie per ASCE 7-16 equation 12.8 15 below
            If SelectedBeams(i).Angle > 45 And SelectedBeams(i).Angle < 135 Then
                dX_seismic = gJob.CdY * dX_seismic / gJob.I
            Else
                dX_seismic = gJob.Cd * dX_seismic / gJob.I
            End If
            '
            SelectedBeams(i).status_SeismicDrift = NumberFormat(dX_seismic / SelectedBeams(i).aSeismicDriftLimit, 3)
            '
            '-----------------------------------------------------------------WIND
            Dim curWindLimit = GetWindDriftLimit()
            Dim strWindLimit As String
            If curWindLimit > 0 Then
                SelectedBeams(i).aWindDriftLimit = (H_use / curWindLimit)
                strWindLimit = NumberFormat(SelectedBeams(i).aWindDriftLimit, 3)
                SelectedBeams(i).status_WindDrift = NumberFormat(dX_wind / SelectedBeams(i).aWindDriftLimit, 3)
            Else
                SelectedBeams(i).aWindDriftLimit = 9999
                strWindLimit = NA
                SelectedBeams(i).status_WindDrift = NA
            End If
            '
            If rIndex >= 0 Then
                dgvStep5(10, rIndex).Value = IIf(nodeBelowOK, NumberFormat(dX_seismic, 3), NA) 'deltaX_Seismic,
                dgvStep5(11, rIndex).Value = IIf(nodeBelowOK, NumberFormat(dX_wind, 3), NA) 'deltaX_Wind
                dgvStep5(12, rIndex).Value = IIf(nodeBelowOK, NumberFormat(H_use, 2), NA)
                dgvStep5(13, rIndex).Value = IIf(nodeBelowOK, NumberFormat(SelectedBeams(i).aSeismicDriftLimit, 3), NA)
                dgvStep5(14, rIndex).Value = IIf(nodeBelowOK, strWindLimit, NA)
                dgvStep5(15, rIndex).Value = IIf(nodeBelowOK, SelectedBeams(i).status_SeismicDrift, NA)
                dgvStep5(16, rIndex).Value = IIf(nodeBelowOK, SelectedBeams(i).status_WindDrift, NA)
                '
                If SelectedBeams(i).NodeBelow_user <> "" Then
                    dgvStep5(dgvStep5.ColumnCount - 5, rIndex).Style.ForeColor = Color.DarkRed
                Else
                    dgvStep5(dgvStep5.ColumnCount - 5, rIndex).Style.ForeColor = Nothing
                End If
            Else
                dgvStep5.Rows.Add(SelectedBeams(i).ElevID, SelectedBeams(i).GridID_LeftSide, SelectedBeams(i).StyID, SelectedBeams(i)._uName,
                                  SelectedBeams(i).Wsec.Size, SelectedBeams(i).LKInfo.Size,
                                  IIf(SelectedBeams(i).IsAsignEndI_K, "YES", "NO"), IIf(SelectedBeams(i).IsAsignEndJ_K, "YES", "NO"), SelectedBeams(i).Pt_I,
                                  nodeBelow, NumberFormat(dX_seismic, 3), NumberFormat(dX_wind, 3), NumberFormat(H_use, 2),
                                  NumberFormat(SelectedBeams(i).aSeismicDriftLimit, 3), strWindLimit,
                                  SelectedBeams(i).status_SeismicDrift, SelectedBeams(i).status_WindDrift)
                'Node below
                If SelectedBeams(i).NodeBelow_user <> "" Then
                    dgvStep5(9, dgvStep5.RowCount - 1).Style.ForeColor = Color.DarkRed
                Else
                    dgvStep5(9, dgvStep5.RowCount - 1).Style.ForeColor = Nothing
                End If
                'Story height
                If SelectedBeams(i).Height_user > 0 Then
                    dgvStep5(12, dgvStep5.RowCount - 1).Style.ForeColor = Color.DarkRed
                Else
                    dgvStep5(12, dgvStep5.RowCount - 1).Style.ForeColor = Nothing
                End If
            End If
        Next
        '
        Update_StatusColor_LastColumn(5)
        isUserEdit = True
    End Sub
    'Beam Design
    Private Sub Update_Step6()
        dgvStep6.Rows.Clear()
        For Each bm As clsSSTBeam In SelectedBeams
            If bm.IsAsignEndI_K = False AndAlso bm.IsAsignEndJ_K = False Then Continue For
            '
            Dim Mu_ends, Mu_Mid, Axial, B_maj, B_min, PMM As Double, lcm As String
            Dim ret As Long
            ret = clsSE.getBeamDesignResult(bm._uName, lcm, Mu_ends, Mu_Mid, Axial, B_maj, B_min, PMM)

            Dim R_use = If(bm.Angle > 45 And bm.Angle < 135, gJob.RY, gJob.R)
            '
            Dim curPcaplink As Double = IIf(R_use <= 3, bm.LKInfo.Py_link, bm.LKInfo.Pr_link)
            Dim Mcap_link As Double
            Mcap_link = curPcaplink * (bm.LKInfo.tstem + bm.Wsec.Depth)
            '
            Dim SF As Double
            If R_use <= 3 Then
                SF = 1
            Else
                If Mu_ends < Mu_Mid Then 'case 3
                    If Mu_Mid >= Mcap_link Then
                        SF = 1
                    Else
                        SF = Mcap_link / Mu_Mid
                    End If
                Else 'case 1,2
                    SF = Mcap_link / Mu_ends
                End If
            End If
            '
            If ret = 0 Then
                dgvStep6.Rows.Add(bm.ElevID, bm.GridID_LeftSide, bm.StyID, bm._uName, bm.Wsec.Size, bm.LKInfo.Size, NumberFormat(curPcaplink, 2), bm.LKInfo.tstem, bm.Wsec.Depth, NumberFormat(Mcap_link, 2), NumberFormat(Math.Max(Mu_ends, Mu_Mid), 2),
                                  NumberFormat(Math.Max(Mu_ends, Mu_Mid) / Mcap_link, 2), NumberFormat((SF), 2), NumberFormat(Axial, 3), NumberFormat(B_maj, 3), NumberFormat(B_min, 3),
                                  NumberFormat(PMM, 3), NumberFormat((SF) * B_maj, 3), NumberFormat(Axial + B_min + (SF) * B_maj, 3))
            Else
                dgvStep6.Rows.Add(bm.ElevID, bm.GridID_LeftSide, bm.StyID, bm._uName, bm.Wsec.Size, bm.LKInfo.Size, curPcaplink, bm.LKInfo.tstem, bm.Wsec.Depth, NumberFormat(Mcap_link, 1), NA,
                                  NA, NA, NA, NA, NA,
                                  NA, NA, NA)
            End If
        Next
        '
        Update_StatusColor_LastColumn(6)
    End Sub
    'Column Design
    Private Sub Update_Step7()
        Dim tmpList As New SortedList(Of String, String)
        For rr As Integer = 0 To dgvStep7.Rows.Count - 1
            tmpList.Add(dgvStep7(3, rr).Value, dgvStep7(11, rr).Value)
        Next
        dgvStep7.Rows.Clear()
        '
        Dim tmp_tf_2bf, tmp_h_tw As String

        If isDebug Then
            For i As Integer = 1 To 5
                dgvStep7.Rows.Add("1", "A", "Level2", i, "W16X57", NA, NA, NA, NA, "No", NA, NA, NA, NA, NA, NA, NA, NA)
            Next
            Exit Sub
        End If
        'need to keep previous selection!!!
        For Each Col As clsSSTColumn In SelectedColumns
            Dim lcm As String
            Dim ret As Long
            ret = clsSE.getColumnDesignResult(Col._uName, lcm, Col.Mu_design, Col.Mu_Top, Col.Mu_bot, Col.Pu_Design, Col.DCR_Axial, Col.DCR_Bmaj, Col.DCR_Bmin)
            'Get the last selecttion for Bracing
            If tmpList.ContainsKey(Col._uName) Then
                Col.isBraceAtBmBotFKG = (tmpList(Col._uName).Equals("YES", StringComparer.CurrentCultureIgnoreCase))
            End If
            '
            If ret = 0 Then
                'Update DCR
                Col.UpdateDesign(gJob)
                If Col.isBottom = False Or Math.Abs(Col.Mu_bot) < 0.01 Then
                    tmp_tf_2bf = NA
                    tmp_h_tw = NA
                Else
                    tmp_tf_2bf = NumberFormat(Col.DCR_bf_2tf, 3)
                    tmp_h_tw = NumberFormat(Col.DCR_h_tw, 3)
                End If
                ''MsgBox(Col.isBraceAtBmBotFKG)
                ''SF = Col.Wsec.Z33 / Col.Wsec.S33
                dgvStep7.Rows.Add(Col.ElevID, Col.GridID_LeftSide, Col.StyID, Col._uName, Col.Wsec.Size, NumberFormat(Col.Mu_Top, 2), NumberFormat(Col.Mu_bot, 2), NumberFormat(Col.Mu_design, 2),
                                  NumberFormat(Col.Pu_Design, 2), If(Col.isBraceAtBmBotFKG, "YES", "NO"), tmp_tf_2bf, tmp_h_tw, NumberFormat(Col.DCR_Axial, 3),
                                  NumberFormat(Col.DCR_Bmaj, 3), NumberFormat(Col.DCR_Bmin, 3), NumberFormat(Col.DCR_Axial + Col.DCR_Bmaj + Col.DCR_Bmin, 3), NumberFormat(Col.DCR_Bmaj_adj, 3), NumberFormat(Col.DCR_Total_Adj, 3))
            Else 'Error from ETAB
                dgvStep7.Rows.Add(Col.ElevID, Col.GridID_LeftSide, Col.StyID, Col._uName, Col.Wsec.Size, NA, NA, NA, NA, If(Col.isBraceAtBmBotFKG, "YES", "NO"), NA, NA, NA, NA, NA, NA, NA, NA)
            End If
        Next
        '
        Update_StatusColor_LastColumn(7)
    End Sub
    'Weld Summary
    Private Sub Update_Step8(Optional _lstWeld As List(Of String) = Nothing)
        dgvStep8.Rows.Clear()
        '
        'Dim totalDblThick As Double = 0
        Dim tmpBm As clsSSTBeam
        'need to keep previous selection!!!
        For Each Col As clsSSTColumn In SelectedColumns
            Col.CC_Detail_US(gJob)
            '
            Dim isCenter As Boolean = False
            If Not Col.BmLeft Is Nothing AndAlso Not Col.BmRight Is Nothing Then
                isCenter = True
            End If
            '
            Dim strSTP As String
            Dim STP_thk As Double = Find_Stiff_Thickness(Col.StiffMinThk, gJob.MaterialInfo.StiffPL_DefThk, Col.StiffMinThk_perLink, Col.Stiff_use) ' Math.Max(Col.StiffMinThk, FindDefault_StiffPLThickness(IIf(Col.BmLeft Is Nothing, Col.BmRight, Col.BmLeft)))
            Dim topSTP, botSTP As StiffStyle
            Find_STP_Style(topSTP, botSTP, Col, False, gJob.MaterialInfo.StiffPL_1SidedConn)
            Dim W2weld As String = Col.W_STPtoWebWeld
            Dim N_W2weld As String = IIf(W2weld = NA, NA, "2")
            Dim W3weld As String = Col.W_STPtoFlangeWeld
            Dim N_W3weld As String = IIf(W3weld = NA, NA, "2")
            '
            If topSTP = StiffStyle.NotRequired Then
                strSTP = NA
            Else
                strSTP = Dec2Frac(STP_thk)
                If Col.StiffMinThk = 0 Then
                    'show default weld (not requuired case) for default stiff
                    W2weld = "3/16"
                    N_W2weld = "2"
                    '
                    W3weld = "3/16"
                    N_W3weld = "2"
                End If
            End If
            '
            'Dim DP_Thk = IIf(Col.ProvideDblPL = "YES", Math.Max(Col.DoublerPLThk, FindDefault_DoublerPLThickness(Col.Wsec.twdet_2)), 0)
            Dim DP_Thk As Double = Find_Dblr_Thickness(Col, gJob.MaterialInfo.DoublerPL_DefThk)
            'totalDblThick += DP_Thk
            '
            Dim strW1A As String = FindShearPL(Col.BmLeft, True)
            Dim strW1B As String = FindShearPL(Col.BmRight, True)
            Dim N_w1A As String = 2
            Dim N_w1B As String = 2
            If strW1A = NA Then N_w1A = NA
            If strW1B = NA Then N_w1B = NA
            '
            dgvStep8.Rows.Add(Col.ElevID, Col.GridID_LeftSide, Col.StyID, Col._uName, Col.Wsec.Size,
              FindShearPL(Col.BmLeft, False), FindShearPL(Col.BmRight, False), strSTP, Dec2Frac(DP_Thk),
              strW1A, N_w1A, strW1B, N_w1B,
              W2weld, N_W2weld, W3weld, N_W3weld,
              IIf(DP_Thk > 0, Col.W_DPtoWeb_weld, NA), IIf(DP_Thk = 0 Or Col.W_DPtoWeb_weld = NA, NA, 1),
              Col.W_PlugWeldSize, Col.W_PlugWeldDepth,
              Col.W_DPtoFlange_weld, IIf(Col.W_DPtoFlange_weld = NA, NA, 1))
            If Dec2Frac(DP_Thk) <> NA Then
                gJob.k = gJob.k + 1
            End If
        Next
        If gJob.k = 0 Then
            DataGridViewTextBoxColumn80.Visible = False
            Column37.Visible = False
            Column38.Visible = False
            Column39.Visible = False
            Column40.Visible = False
            Column41.Visible = False
        Else
            DataGridViewTextBoxColumn80.Visible = True
            Column37.Visible = True
            Column38.Visible = True
            Column39.Visible = True
            Column40.Visible = True
            Column41.Visible = True
        End If
        'Next
        '''visible doubler
        ''For j As Integer = dgvStep8.ColumnCount - 6 To dgvStep8.ColumnCount - 1
        ''    dgvStep8.Columns(j).Visible = (totalDblThick > 0)
        ''Next
        '
        If Not _lstWeld Is Nothing Then
            _lstWeld.Clear()
            '
            Dim strData As String
            For j As Integer = 0 To dgvStep8.ColumnCount - 1
                If dgvStep8.Columns(j).Visible = False Then Continue For
                strData += dgvStep8.Columns(j).HeaderText & gJob.splChar
            Next
            _lstWeld.Add(strData)
            '
            For i As Integer = 0 To dgvStep8.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep8.ColumnCount - 1
                    If dgvStep8.Columns(j).Visible = False Then Continue For
                    strData += dgvStep8(j, i).Value & gJob.splChar
                Next
                _lstWeld.Add(strData)
            Next
        End If
        gJob.l = gJob.k
        gJob.k = 0
    End Sub
    '
    Private Function FindShearPL(bm As clsSSTBeam, isWeld As Boolean) As String
        Try
            If isWeld Then
                Return eThickness2String(bm.Sheartab.WeldSize)
            Else
                Return eThickness2String(bm.Sheartab.Thk)
            End If
        Catch ex As Exception
            Return NA
        End Try
    End Function
    Private Sub Get_Relative_BeamsAndColumns()
        For Each fr As clsSEFrame In SelectedFrames
            If fr.isColumn Then
                SelectedColumns.Add(New clsSSTColumn(fr.StyID, fr.ElevID, fr.GridID_LeftSide, fr._uName, fr.Len_Cal, fr.Wsec, fr.Pt_I, fr.Pt_J, Nothing, Nothing, Nothing, Nothing, 0, fr.StyAbove, fr.topSty, fr.Angle))
            Else
                SelectedBeams.Add(New clsSSTBeam(fr.StyID, fr.ElevID, fr.GridID_LeftSide, fr.GridID_RightSide, fr._uName, fr.Len_Cal, fr.Wsec, fr.Pt_I, fr.Pt_J, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, True, True, fr.LevelHeight, 0, fr.sPt_X, fr.sPt_Y, False, False, fr.Angle))
            End If
        Next
    End Sub
    Private Sub tabMain_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabMain.SelectedIndexChanged
        If Not isUserEdit Then Return
        '
        Try
            If tabMain.SelectedIndex = 0 Then
            ElseIf tabMain.SelectedIndex = 1 Then
                Update_Step2()
                '
                cbElevStep2.Items.Clear()
                cbStoryStep2.Items.Clear()
                For Each item As String In cbElevStep1.Items
                    cbElevStep2.Items.Add(item)
                Next
                For Each item As String In cbStoryStep1.Items
                    cbStoryStep2.Items.Add(item)
                Next
                cbElevStep2.Text = cbElevStep1.Text
                cbStoryStep2.Text = cbStoryStep1.Text
            ElseIf tabMain.SelectedIndex = 2 Then
                Update_Step3()
                '
                cbElevStep3.Items.Clear()
                cbStoryStep3.Items.Clear()
                For Each item As String In cbElevStep1.Items
                    cbElevStep3.Items.Add(item)
                Next
                For Each item As String In cbStoryStep1.Items
                    cbStoryStep3.Items.Add(item)
                Next
                cbElevStep3.Text = cbElevStep1.Text
                cbStoryStep3.Text = cbStoryStep1.Text
            ElseIf tabMain.SelectedIndex = 3 Then
                Update_Step4()
                '
                cbElevStep4.Items.Clear()
                cbStoryStep4.Items.Clear()
                For Each item As String In cbElevStep1.Items
                    cbElevStep4.Items.Add(item)
                Next
                For Each item As String In cbStoryStep1.Items
                    cbStoryStep4.Items.Add(item)
                Next
                cbElevStep4.Text = cbElevStep1.Text
                cbStoryStep4.Text = cbStoryStep1.Text
            ElseIf tabMain.SelectedIndex = 4 Then
                'Update_Step5()
                '
                cbElevStep5.Items.Clear()
                cbStoryStep5.Items.Clear()
                For Each item As String In cbElevStep1.Items
                    cbElevStep5.Items.Add(item)
                Next
                For Each item As String In cbStoryStep1.Items
                    cbStoryStep5.Items.Add(item)
                Next
                cbElevStep5.Text = cbElevStep1.Text
                cbStoryStep5.Text = cbStoryStep1.Text
            ElseIf tabMain.SelectedIndex = 5 Then
                'Update_Step6()
                '
                cbElevStep6.Items.Clear()
                cbStoryStep6.Items.Clear()
                For Each item As String In cbElevStep1.Items
                    cbElevStep6.Items.Add(item)
                Next
                For Each item As String In cbStoryStep1.Items
                    cbStoryStep6.Items.Add(item)
                Next
                cbElevStep6.Text = cbElevStep1.Text
                cbStoryStep6.Text = cbStoryStep1.Text
            ElseIf tabMain.SelectedIndex = 6 Then
                'Update_Step7()
                '
                cbElevStep7.Items.Clear()
                cbStoryStep7.Items.Clear()
                For Each item As String In cbElevStep1.Items
                    cbElevStep7.Items.Add(item)
                Next
                For Each item As String In cbStoryStep1.Items
                    cbStoryStep7.Items.Add(item)
                Next
                cbElevStep7.Text = cbElevStep1.Text
                cbStoryStep7.Text = cbStoryStep1.Text
            ElseIf tabMain.SelectedIndex = 7 Then
                ''Update_Step8
                cbElevStep8.Items.Clear()
                cbStoryStep8.Items.Clear()
                For Each item As String In cbElevStep1.Items
                    cbElevStep8.Items.Add(item)
                Next
                For Each item As String In cbStoryStep1.Items
                    cbStoryStep8.Items.Add(item)
                Next
                cbElevStep8.Text = cbElevStep1.Text
                cbStoryStep8.Text = cbStoryStep1.Text
                '
                Update_Step8()
            End If
            ''lbStatus.Text = "Ready"
        Catch ex As Exception
            lbStatus.Text = ex.Message.Split(".")(0)
            ''MsgBox(ex.StackTrace)
        End Try
    End Sub
    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        'Dim ask As MsgBoxResult = MsgBox("Current inputs will be lost. Would you like to save them first? ", MsgBoxStyle.YesNo + MsgBoxStyle.Question)
        If (dgvStep1.RowCount > 0 AndAlso MsgBox("Would you like to save current inputs before reloading? ", MsgBoxStyle.YesNo + MsgBoxStyle.Question + MsgBoxStyle.DefaultButton2) = MsgBoxResult.Yes) Then
            If SaveInputs(True) Then
                SelectedBeams.Clear()
                isKeepRows = False
                tabMain.SelectedIndex = 0
                UpdateDisplay()
            End If
        Else
            SelectedBeams.Clear()
            isKeepRows = False
            tabMain.SelectedIndex = 0
            UpdateDisplay()
        End If
    End Sub
    Private Sub btnRunAnalysis_Click(sender As Object, e As EventArgs) Handles btnRunAnalysis.Click
        Try
            If clsSE.CheckFileSave = False Then
                MsgBox("Error. Please save model before runing analysis", MsgBoxStyle.Critical, toolName)
                Return
            End If
            '
            lbStatus.Text = "Please wait..."
            Me.Enabled = False

            If MsgBox("This will take a while based on the model." & vbLf & "Do you want to continue?",
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, toolName) = MsgBoxResult.Yes Then
                '
                Dim str = Get_IntForces(True)
                '
                Update_Step2()
                Update_Step3()
                Update_Step4()
                Update_Step5()
                Update_Step1(False)
                'Update_Step6()
                '
                lblReactionUpdate.Text = str
                '
                BtnDesign.Enabled = True
            End If
            'show warning for RSA factor
            Dim isUpdateSF As Boolean
            Dim SFX, SFY, SFX_D, SFY_D, NewSFX, NewSFY, NewSFX_D, NewSFY_D As Double
            Dim RSAFlag As Integer = clsSE.Get_RSA_Factor(SFX, SFY, SFX_D, SFY_D, NewSFX, NewSFY, NewSFX_D, NewSFY_D)
            If RSAFlag = -1 Then
                lbStatus.Text = "Error: Cannot get Scale factor"
                Return
            ElseIf RSAFlag = -2 Then
                lbStatus.Text = "Error: Cannot get base shear. Please check the Load Patterns for EQ* load"
                Return
            End If

            isUpdateSF = Math.Abs(SFX - NewSFX) > 0.1 Or Math.Abs(SFY - NewSFY) > 0.1 Or
                         Math.Abs(SFX_D - NewSFX_D) > 0.1 Or Math.Abs(SFY_D - NewSFY_D) > 0.1
            If RSAFlag = 0 AndAlso isUpdateSF Then
                Dim frm As New frmMsgboxRS_SF(SFX, SFY, SFX_D, SFY_D, NewSFX, NewSFY, NewSFX_D, NewSFY_D)
                frm.ShowDialog()
                isUpdateSF = frm.isUpdate
                frm = Nothing
            End If
            ' 
            Dim isOK As Boolean = True
            If RSAFlag = 0 AndAlso isUpdateSF Then
                If clsSE.Modify_RSA_Factor(NewSFX, NewSFY, NewSFX_D, NewSFY_D) Then
                    lbStatus.Text = "Scale factors have been updated successfully"
                Else
                    isOK = False
                    lbStatus.Text = "Fail to update Scale factor for RSA load cases!"
                End If
            End If
            ''Step 3: Check if Modal Mass Participation is above 90%
            If RSAFlag = 0 AndAlso isOK Then
                Dim ret As Integer = clsSE.Check_ModalMassParticipation()
                If ret = 0 Then
                    lbStatus.Text = "Done."
                ElseIf ret = 1 Then
                    lbStatus.Text = "Error: No ""Model"" case is found."
                ElseIf ret = 2 Then
                    MsgBox("All Modal Participation Mass Ratios are under 90%" & vbLf & vbLf & "Please increase the number of modes for the modal analysis",
                           MsgBoxStyle.Exclamation, toolName)
                ElseIf ret = 3 Then
                    lbStatus.Text = "Error: Cannot check Modal Participation Mass Ratios"
                Else
                    lbStatus.Text = "Warning: Modal Participation Mass Ratios are not checked. Please ""Run Analysis"" again"
                End If
            End If
        Catch ex As Exception
            lbStatus.Text = "Error"
        Finally
            Me.Enabled = True
            BringWindowToTop(Me.Handle)
        End Try
    End Sub

    Private Function Get_IntForces(RunIfUnlock As Boolean) As String
        Dim originalModelLocked As Boolean
        If g_isEtab = False Then 'check  & set endlength for SAP only
            Dim offsetI, offsetJ As Double
            If clsSE.isModelLocked() Then
                'check end length
                For Each bm As clsSSTBeam In SelectedBeams
                    If clsSE.GetEndLengthOffsetforSAP(bm._uName) = False Then
                        Return "The end length offsets are not found. Unlock model and run analysis again to get correct reactions. "
                    End If
                Next
            Else
                'set endlength
                For Each bm As clsSSTBeam In SelectedBeams
                    clsSE.SetEndLengthOffsetforSAP(bm._uName)
                Next
            End If
        End If
        '
        If Not isDebug Then
            originalModelLocked = clsSE.RunAnalysis(RunIfUnlock)
        End If
        '
        If RunIfUnlock = False AndAlso originalModelLocked = False Then
            Return "Column reactions are not updated. Run ""Run Analysis"" to update now"
        End If
        '
        For Each col As clsSSTColumn In SelectedColumns
            clsSE.Get_Mu_Pu_Vu(col._uName, 0, col.Pu_Design, col.Pu_col_omega, 0, col.Vu_col_EL, col.Vu_col_33_36, isDebug)
        Next
        '
        For Each bm As clsSSTBeam In SelectedBeams
            'find Pu_bm
            Dim L1, L2, L3, Vi_top, Vj_top, Vi_bot, Vj_bot, Pi, Pj As Double
            Dim leftBm As clsSSTBeam = SelectedBeams.Find(Function(x) x.Pt_J = bm.Pt_I)
            Dim RightBm As clsSSTBeam = SelectedBeams.Find(Function(x) x.Pt_I = bm.Pt_J)
            '
            If leftBm IsNot Nothing Then
                L1 = leftBm.Len_LCC
            Else
                L1 = 0
            End If
            L2 = bm.Len_LCC
            If RightBm IsNot Nothing Then
                L3 = RightBm.Len_LCC
            Else
                L3 = 0
            End If
            'Top column
            If bm.EndI_ColTop IsNot Nothing Then
                Vi_top = SelectedColumns.Find(Function(x) x._uName = bm.EndI_ColTop._uName).Vu_col_33_36
            Else
                Vi_top = 0
            End If
            '
            If bm.EndI_ColBottom IsNot Nothing Then
                Vi_bot = SelectedColumns.Find(Function(x) x._uName = bm.EndI_ColBottom._uName).Vu_col_33_36
            Else
                Vi_bot = 0
            End If
            'Bottom
            If bm.EndJ_ColTop IsNot Nothing Then
                Vj_top = SelectedColumns.Find(Function(x) x._uName = bm.EndJ_ColTop._uName).Vu_col_33_36
            Else
                Vj_top = 0
            End If
            '
            If bm.EndJ_ColBottom IsNot Nothing Then
                Vj_bot = SelectedColumns.Find(Function(x) x._uName = bm.EndJ_ColBottom._uName).Vu_col_33_36
            Else
                Vj_bot = 0
            End If
            '
            Pi = Math.Abs(Vi_top - Vi_bot) * L2 / (L1 + L2)
            Pj = Math.Abs(Vj_top - Vj_bot) * L2 / (L2 + L3)
            'max bm axial
            Dim Bm_axial As Double
            clsSE.BeamReac(bm._uName, 0, Bm_axial, 0)
            If gJob.isBaseOnLinkSizeAndRValue = False Then 'Updated Rigid Diaphragm Beam Pu Estimate
                If Bm_axial <> 0 Then
                    bm.Pu_bm = Bm_axial
                Else
                    bm.Pu_bm = MaxAll(Pi, Pj)
                End If
            Else
                If Bm_axial <> 0 Then
                    bm.Pu_bm = Bm_axial
                Else
                    Dim R_use = If(bm.Angle > 45 And bm.Angle < 135, gJob.RY, gJob.R)
                    '
                    If R_use <= 3 Then
                        bm.Pu_bm = bm.LKInfo.Pu_Rigid_R3
                    Else
                        bm.Pu_bm = bm.LKInfo.Pu_Rigid
                    End If
                End If
            End If
            '
            clsSE.Get_Mu_Pu_Vu(bm._uName, bm.Mu_max, 0, 0, bm.V_bmGravity, 0, 0, isDebug)
        Next
        Return "Beam/ Column reactions are updated at " & Date.Now
    End Function
    Private Sub btnApply_Click(sender As Object, e As EventArgs) Handles btnApply.Click
        lbStatus.Text = clsSE.ApplyKvalue(SelectedBeams)
        Dim strName As String = PushDataToGjob(True, True)
        BringWindowToTop(Me.Handle)
        '
        ' ''btnRunAnalysis_Click(sender, e)
    End Sub
    Private Sub btnReassign_Click(sender As Object, e As EventArgs) Handles btnReassign.Click
        Dim stl As New SortedList(Of String, String)
        For Each fm As clsSEFrame In SelectedFrames
            stl.Add(fm._uName, fm.Wsec.Size)
            'clsSE.ReAssignMembersize(fm._uName, fm.Wsec.Size)
        Next
        clsSE.ReAssignMembersize(stl)
        '
        BringWindowToTop(Me.Handle)
    End Sub
    Private regExt As String = ".xml"
    Private Function SaveInputs(isCurrent As Boolean) As Boolean
        Try
            Dim strName = PushDataToGjob(True, isCurrent)
            Dim ret As Boolean
            '
            Dim sfd As SaveFileDialog = New SaveFileDialog
            'sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            sfd.FileName = strName
            sfd.Filter = "XML file (*.xml)|*" & regExt
            If sfd.ShowDialog = DialogResult.OK Then
                ret = Serialize_gJob_ToXML(sfd.FileName, gJob)
            Else
                ret = False
                lbStatus.Text = "Canceled"
            End If
            Return ret
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnExit.Click, btnClose.Click
        Dim msg As MsgBoxResult = MsgBox("Do you want to save your inputs?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.Question, toolName)
        If SelectedBeams.Count > 0 AndAlso msg = MsgBoxResult.Yes Then
            If SaveInputs(True) Then
                lbStatus.Text = "Saved at " & Now.ToShortTimeString
                Me.Close()
            Else
                lbStatus.Text = "An error occurred while saving data"
            End If
            '
            'gJob = Nothing
        ElseIf msg = MsgBoxResult.Cancel Then
        Else
            'gJob = Nothing
            Me.Close()
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If SaveInputs(False) Then
            lbStatus.Text = "Saved at " & Now.ToShortTimeString
        Else
            lbStatus.Text = "An error occurred while saving data"
        End If
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        ''If MsgBox("Do you want to load information?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, toolName) <> MsgBoxResult.Yes Then
        ''    Return
        ''End If
        '''
        Try
            Me.Enabled = False
            Cursor = Cursors.WaitCursor
            Dim ofd As New OpenFileDialog
            ofd.Filter = "(XML files)|*" & regExt
            ofd.Multiselect = False
            If ofd.ShowDialog = DialogResult.OK Then
                gJob = DeSerialize_XML_gJob(ofd.FileName)
                If gJob.AllWsections Is Nothing Then
                    gJob.AllWsections = Read_AllWsections()
                End If
                If gJob.SSTLinks Is Nothing Then
                    gJob.SSTLinks = Read_SSTLinks()
                End If
                If gJob.SSTOthers Is Nothing Then
                    gJob.SSTOthers = Read_SSTOthers()
                End If
                '
                Dim Arr() As String
                ' Update
                For Each bm As clsSSTBeam In SelectedBeams
                    For Each item As String In gJob.Dgv1Data
                        ' 'Step 1
                        Arr = Split(item, gJob.splChar)
                        If Arr(3) = bm._uName Then 'Check unique name
                            bm.LKInfo = gJob.SSTLinks.Links.Find(Function(x) x.Size.Equals(Arr(5), StringComparison.CurrentCultureIgnoreCase))
                            bm.BRPInfo = gJob.SSTLinks.BRPs.Find(Function(x) x.Size.Equals(Arr(5), StringComparison.CurrentCultureIgnoreCase))
                            '
                            bm.IsAsignEndI_K = Arr(9)
                            bm.IsAsignEndJ_K = Arr(10)
                        End If
                    Next
                    '' 'Step 4
                    For Each item As String In gJob.Dgv4Data
                        Arr = Split(item, gJob.splChar)
                        If Arr(3) = bm._uName Then 'Check unique name
                            bm.Sheartab.N_horzBolt = Arr(9)
                            '
                            Dim intVal As Integer
                            Integer.TryParse(Arr(10).Split("/")(0), intVal)
                            If intVal = 1 Then intVal = 8
                            bm.Sheartab.bSize = intVal
                            bm.Sheartab.bType = String2bType(Arr(11))
                            bm.Sheartab.Thk = String2eThickness(Arr(12))
                            'update weld size 
                            bm.Sheartab.WeldSize = Math.Max(Ceiling(5 / 8 * bm.Sheartab.Thk / 16, 1 / 16), 0.25) * 16
                        End If
                    Next
                    '' 'Step 5
                    Dim curH As Double
                    For Each item As String In gJob.Dgv5Data
                        Arr = Split(item, gJob.splChar)
                        If Arr(3) = bm._uName Then 'Check unique name
                            'adjust column index for v2.0c and below
                            If gJob.versionID Like "*0" Or gJob.versionID Like "0a*" Or gJob.versionID Like "0b*" Or
                               gJob.versionID Like "0c*" Or gJob.versionID Like "0d*" Then
                                bm.NodeBelow_user = Arr(11)
                                Double.TryParse(Arr(13), curH)
                                bm.Height_user = curH
                            Else
                                bm.NodeBelow_user = Arr(9)
                                Double.TryParse(Arr(12), curH)
                                bm.Height_user = curH
                            End If
                        End If
                    Next
                Next
                ' 'Step 3
                For Each col As clsSSTColumn In SelectedColumns
                    For Each item As String In gJob.Dgv3Data
                        Arr = Split(item, gJob.splChar)
                        If Arr(3) = col._uName Then 'Check unique name
                            col.ProvideStiff = Arr(9)
                            col.ProvideDblPL = Arr(10)
                        End If
                    Next
                Next
                ' 'Step 7
                For Each col As clsSSTColumn In SelectedColumns
                    For Each item As String In gJob.Dgv7Data
                        Arr = Split(item, gJob.splChar)
                        If Arr(3) = col._uName Then 'Check unique name
                            ''MsgBox(Arr(9))
                            col.isBraceAtBmBotFKG = (Arr(9) = "YES")
                        End If
                    Next
                Next
                '
                isKeepRows = True
                ''isPrevent = True
                '''
                Update_Step1(False)
                ''isPrevent = False
                tabMain.SelectedIndex = 0
                If dgvStep1.RowCount > 0 Then
                    dgvStep1.CurrentCell = dgvStep1(0, 0)
                End If
            End If
            '
            lbStatus.Text = "Information loaded successfully. Please re-design to update results."
        Catch ex As Exception
            lbStatus.Text = ex.Message.Split(".")(0)
            'MsgBox(ex.Message & ex.StackTrace,, "Yield-Link� Connection")
        Finally
            Me.Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub SSTLinksPlugin_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        '\\ Try
        '\\ saveInfo.projID = projID
        '\\ Serialize_saveInfo_ToXML(IO.Path.Combine(gTemp, saveName), saveInfo)
        '
        '\\ gJob.proID = projID
        '\\ Save_Load_gJob(True)
        '\\ Catch ex As Exception
        '\\ End Try
        Try
            ' It is very important to call ISapPlugin.Finish(0) when form closes, !!!
            ' otherwise, SAP2000 will wait and be hung !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            clsSE.FinishPlugIn()
        Catch ex As Exception
        Finally
            Me.Dispose()
        End Try
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
        Dim wlink As String = "http://www.strongtie.com/products/strongframe/index.asp?source=topnav"
        Try
            System.Diagnostics.Process.Start(wlink)
        Catch ex As System.Exception
            MsgBox("Exception:" & ex.Message,, "Yield-Link� Connection")
        End Try
    End Sub
    'Update 03/29
    Private Sub dgvStepAll_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvStep7.CurrentCellDirtyStateChanged, dgvStep6.CurrentCellDirtyStateChanged, dgvStep5.CurrentCellDirtyStateChanged, dgvStep4.CurrentCellDirtyStateChanged, dgvStep3.CurrentCellDirtyStateChanged, dgvStep2.CurrentCellDirtyStateChanged, dgvStep1.CurrentCellDirtyStateChanged
        If sender.IsCurrentCellDirty Then
            sender.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub
    '
    Private isKeepRows As Boolean
    Private multiChange As Boolean = False
    Private Sub dgvStep1_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStep1.CellValueChanged
        If Not isUserEdit Or isPrevent Then Return
        '
        Try
            Dim uniqueName As String = dgvStep1(3, e.RowIndex).Value
            Dim newVal = dgvStep1(e.ColumnIndex, e.RowIndex).Value
            Dim isEditCol As Boolean = True
            '
            Dim val As Long = SelectedBeams.FindIndex(Function(x) x._uName = uniqueName)

            If e.ColumnIndex = 5 Then 'Link csize
                Dim isPinned As Boolean = (newVal = "Pinned")
                If isPinned Then
                    ''dgvStep1(9, e.RowIndex).Value = False
                    ''dgvStep1(10, e.RowIndex).Value = False
                    '
                    SelectedBeams(val).IsAsignEndI_K = False
                    SelectedBeams(val).IsAsignEndJ_K = False
                ElseIf dgvStep1(9, e.RowIndex).Value = False And dgvStep1(10, e.RowIndex).Value = False Then
                    ''dgvStep1(9, e.RowIndex).Value = True
                    ''dgvStep1(10, e.RowIndex).Value = True
                    '
                    SelectedBeams(val).IsAsignEndI_K = True
                    SelectedBeams(val).IsAsignEndJ_K = True
                End If
                dgvStep1(9, e.RowIndex).ReadOnly = isPinned
                dgvStep1(10, e.RowIndex).ReadOnly = isPinned
                '
                SelectedBeams(val).LKInfo = gJob.SSTLinks.Links.Find(Function(x) x.Size = newVal)
                SelectedBeams(val).BRPInfo = gJob.SSTLinks.BRPs.Find(Function(x) x.Size = newVal)
            ElseIf e.ColumnIndex = 9 Then 'Assign K end I
                SelectedBeams(val).IsAsignEndI_K = (newVal = True)
                If newVal = True AndAlso dgvStep1(5, e.RowIndex).Value = "Pinned" Then
                    dgvStep1(5, e.RowIndex).Value = defaultLink
                End If
            ElseIf e.ColumnIndex = 10 Then 'Assign K end J
                SelectedBeams(val).IsAsignEndJ_K = (newVal = True)
                If newVal = True AndAlso dgvStep1(5, e.RowIndex).Value = "Pinned" Then
                    dgvStep1(5, e.RowIndex).Value = defaultLink
                End If
            ElseIf e.ColumnIndex = 4 Then 'change beam size
                SelectedBeams(val).Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = newVal)
                Dim stl As New SortedList(Of String, String)
                Dim ret As Long = 0
                Dim curKey = SelectedBeams(val)._uName
                If Not multiChange Then
                    stl.Add(curKey, newVal)
                    ret = clsSE.ReAssignMembersize(stl)
                Else
                    If stlChangedFrames.ContainsKey(curKey) Then
                        stlChangedFrames(curKey) = newVal
                    Else
                        stlChangedFrames.Add(curKey, newVal)
                    End If
                End If
                If ret = 0 Then
                    'update shear plate properties
                    Dim N_horzB As Integer
                    Dim bSize As bSize
                    Dim bTyp As bType
                    Dim STThick, WeldThk As eThickness
                    N_horzB = 2
                    bSize = mPubFunc.bSize.B7_8
                    bTyp = bType.A325X
                    STThick = eThickness.T1_2
                    WeldThk = Math.Ceiling(5 / 8 * STThick)
                    'Dim strW3 As String = Microsoft.VisualBasic.Left(SelectedBeams(val).Wsec.Size, 3)
                    Dim stBolt As SheartabBolt = FindShearTabBolt(SelectedBeams(val).Wsec.Size, gJob, SelectedBeams(val).Wsec.Depth) ' gJob.SSTOthers.SheartabBolts.Find(Function(x) x.Size = strW3)
                    SelectedBeams(val).Sheartab = New clsSSTSheartab(SelectedBeams(val).Wsec, SelectedBeams(val).LKInfo, stBolt, N_horzB, bSize, bTyp, STThick, WeldThk, SelectedBeams(val).Pu_bm, SelectedBeams(val).V_bmGravity)
                    SelectedBeams(val).Step1_InitialDesign(gJob)
                    clsSE.ApplyKvalue1Beam(SelectedBeams(val))
                    '
                    lbStatus.Text = "Changed!"
                Else
                    lbStatus.Text = "Error: Cannot change beam size!"
                End If
            ElseIf e.ColumnIndex = 7 Then 'change I_end column size
                SelectedBeams(val).EndI_ColBottom.Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = newVal)
                Dim stl As New SortedList(Of String, String)
                Dim ret As Long = 0
                Dim curKey = SelectedBeams(val).EndI_ColBottom._uName
                If Not multiChange Then
                    stl.Add(curKey, newVal)
                    ret = clsSE.ReAssignMembersize(stl)
                Else
                    If stlChangedFrames.ContainsKey(curKey) Then
                        stlChangedFrames(curKey) = newVal
                    Else
                        stlChangedFrames.Add(curKey, newVal)
                    End If
                End If
                If ret = 0 Then
                    SelectedColumns.Find(Function(x) x._uName = SelectedBeams(val).EndI_ColBottom._uName).Wsec = SelectedBeams(val).EndI_ColBottom.Wsec
                    lbStatus.Text = "Changed!"
                Else
                    lbStatus.Text = "Error: Cannot change column size!"
                End If
            ElseIf e.ColumnIndex = 8 Then 'change J_end column size
                SelectedBeams(val).EndJ_ColBottom.Wsec = gJob.AllWsections.Wsections.Find(Function(x) x.Size = newVal)
                Dim stl As New SortedList(Of String, String)
                Dim ret As Long = 0
                Dim curKey = SelectedBeams(val).EndJ_ColBottom._uName
                If Not multiChange Then
                    stl.Add(curKey, newVal)
                    ret = clsSE.ReAssignMembersize(stl)
                Else
                    If stlChangedFrames.ContainsKey(curKey) Then
                        stlChangedFrames(curKey) = newVal
                    Else
                        stlChangedFrames.Add(curKey, newVal)
                    End If
                End If
                If ret = 0 Then
                    SelectedColumns.Find(Function(x) x._uName = SelectedBeams(val).EndJ_ColBottom._uName).Wsec = SelectedBeams(val).EndJ_ColBottom.Wsec
                    lbStatus.Text = "Changed!"
                Else
                    lbStatus.Text = "Error: Cannot change column size!"
                End If
            Else
                isEditCol = False
            End If
            '
            If isEditCol Then
                If SelectedBeams(val).IsAsignEndI_K = False And SelectedBeams(val).IsAsignEndJ_K = False Then SelectedBeams(val).LKInfo = gJob.SSTLinks.Links.Find(Function(x) x.Size = "Pinned")

                SelectedBeams(val).Step1_InitialDesign(gJob) ''''
                SelectedBeams(val).Step2_BeamLinkCheck(gJob) ''''
                Dim LeftColUname, RightColUname As String
                LeftColUname = SelectedBeams(val).EndI_ColBottom._uName
                RightColUname = SelectedBeams(val).EndJ_ColBottom._uName
                Dim LeftColVal, RightColVal As Long
                LeftColVal = SelectedColumns.FindIndex(Function(x) x._uName = LeftColUname)
                RightColVal = SelectedColumns.FindIndex(Function(x) x._uName = RightColUname)
                SelectedColumns(LeftColVal).CC_Detail_US(gJob) ''''
                SelectedColumns(RightColVal).CC_Detail_US(gJob) ''''


                'SelectedBeams(val).status_endIPz = SelectedColumns(LeftColVal).statusPZinitial

                ' SelectedBeams(val).status_endJPz = SelectedColumns(RightColVal).statusPZinitial
                For i As Integer = 0 To dgvStep1.RowCount - 1
                    val = SelectedBeams.FindIndex(Function(x) x._uName = dgvStep1(3, i).Value)
                    If SelectedBeams(val).EndI_ColBottom._uName = LeftColUname Then
                        SelectedBeams(val).status_endIPz = SelectedColumns(LeftColVal).statusPZinitial
                    ElseIf SelectedBeams(val).EndI_ColBottom._uName = RightColUname Then
                        SelectedBeams(val).status_endIPz = SelectedColumns(RightColVal).statusPZinitial
                    End If
                    If SelectedBeams(val).EndJ_ColBottom._uName = LeftColUname Then
                        SelectedBeams(val).status_endJPz = SelectedColumns(LeftColVal).statusPZinitial
                    ElseIf SelectedBeams(val).EndJ_ColBottom._uName = RightColUname Then
                        SelectedBeams(val).status_endJPz = SelectedColumns(RightColVal).statusPZinitial
                    End If
                    dgvStep1(5, i).Value = SelectedBeams(val).LKInfo.Size
                    dgvStep1(6, i).Value = NumberFormat(SelectedBeams(val).K_rot, 0)
                    dgvStep1(7, i).Value = SelectedBeams(val).EndI_ColBottom.Wsec.Size
                    dgvStep1(8, i).Value = SelectedBeams(val).EndJ_ColBottom.Wsec.Size
                    dgvStep1(9, i).Value = SelectedBeams(val).IsAsignEndI_K
                    dgvStep1(10, i).Value = SelectedBeams(val).IsAsignEndJ_K
                    dgvStep1(11, i).Value = SelectedBeams(val).status_tbf
                    dgvStep1(12, i).Value = SelectedBeams(val).status_bf
                    dgvStep1(12, i).ToolTipText = IIf(SelectedBeams(val).status_ClipWasher Like "R*", "Clip washers are required", "")
                    '
                    dgvStep1(13, i).Value = SelectedBeams(val).status_Lyield
                    dgvStep1(14, i).Value = SelectedBeams(val).status_endIPz
                    dgvStep1(15, i).Value = SelectedBeams(val).status_endJPz
                    dgvStep1(16, i).Value = SelectedBeams(val).status_SeismicDrift
                    dgvStep1(17, i).Value = SelectedBeams(val).EndI_ColBottom._uName
                    dgvStep1(18, i).Value = SelectedBeams(val).EndJ_ColBottom._uName
                Next
                '
                Update_StatusColor_LastColumn(1)
            End If
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical, "Yield-Link� Connection")
            MsgBox(ex.Message & ex.StackTrace, MsgBoxStyle.Critical, "Yield-Link� Connection")
        End Try
    End Sub

    'Column check
    Private Sub dgvStep3_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStep3.CellValueChanged
        If Not isUserEdit Or isPrevent Then Return
        '
        Dim uniqueName As String = dgvStep3(3, e.RowIndex).Value
        Dim newVal = dgvStep3(e.ColumnIndex, e.RowIndex).Value
        Dim newPu, newVu As Double
        Dim topS As Integer
        Dim tmpStr As String
        '
        Dim val As Long = SelectedColumns.FindIndex(Function(x) x._uName = uniqueName)
        ''Dim isCalProvideStiff As String = SelectedColumns(val).ProvideStiff
        ''Dim isCalProvideDblr As String = SelectedColumns(val).ProvideDblPL

        If e.ColumnIndex = 9 Then 'Stiffener Provider
            tmpStr = dgvStep3(e.ColumnIndex, e.RowIndex).Value
            SelectedColumns(val).ProvideStiff = tmpStr
        ElseIf e.ColumnIndex = 10 Then 'Double plate Provider
            tmpStr = dgvStep3(e.ColumnIndex, e.RowIndex).Value
            SelectedColumns(val).ProvideDblPL = tmpStr
        Else
            Return
        End If
        '
        SelectedColumns(val).CC_Detail_US(gJob)

        ''min stiff plate
        If SelectedColumns(val).StiffMinThk <= 0 And SelectedColumns(val).ProvideStiff = "YES" Then
            dgvStep3(12, e.RowIndex).Value = NumberFormat(MaxAll(gJob.MaterialInfo.StiffPL_DefThk, SelectedColumns(val).StiffMinThk_perLink, SelectedColumns(val).StiffMin_use), (3))
        Else
            dgvStep3(12, e.RowIndex).Value = NumberFormat(SelectedColumns(val).StiffMinThk, (3))
        End If
        ''min double plate
        If SelectedColumns(val).DoublerPLThk = 0 And SelectedColumns(val).ProvideDblPL = "YES" Then
            dgvStep3(13, e.RowIndex).Value = NumberFormat(SelectedColumns(val).DoublerPLThk_Geometry, (3))
        Else
            dgvStep3(13, e.RowIndex).Value = NumberFormat(SelectedColumns(val).DoublerPLThk, (3))
        End If
        'SCWB check
        dgvStep3(14, e.RowIndex).Value = SelectedColumns(val).status_SCWB
        'Pz DCR
        dgvStep3(15, e.RowIndex).Value = SelectedColumns(val).status_Pz
        'Status Final
        dgvStep3(16, e.RowIndex).Value = SelectedColumns(val).status_ColFlg
        '
        Update_StatusColor_LastColumn(3)
    End Sub
    'Sheartab
    Private Sub dgvStep4_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStep4.CellValueChanged
        If Not isUserEdit Or isPrevent Then Return
        '
        Try
            isUserEdit = False
            '
            Dim uniqueName As String = dgvStep4(3, e.RowIndex).Value
            Dim newVal = dgvStep4(e.ColumnIndex, e.RowIndex).Value
            Dim intVal As Integer
            'Dim tmpStr As String
            '
            Dim val As Long = SelectedBeams.FindIndex(Function(x) x._uName = uniqueName)
            If e.ColumnIndex = 9 Then 'No. Horz bolts
                Integer.TryParse(dgvStep4(e.ColumnIndex, e.RowIndex).Value, intVal)
                SelectedBeams(val).Sheartab.N_horzBolt = intVal
            ElseIf e.ColumnIndex = 10 Then 'Bolt size
                SelectedBeams(val).Sheartab.bSize = String2bSize(dgvStep4(e.ColumnIndex, e.RowIndex).Value)
            ElseIf e.ColumnIndex = 11 Then 'bolttype
                SelectedBeams(val).Sheartab.bType = String2bType(dgvStep4(e.ColumnIndex, e.RowIndex).Value)
            ElseIf e.ColumnIndex = 12 Then 'Thickness
                SelectedBeams(val).Sheartab.Thk = String2eThickness(dgvStep4(e.ColumnIndex, e.RowIndex).Value)
                'update weld size 
                SelectedBeams(val).Sheartab.WeldSize = Math.Max(Ceiling(5 / 8 * SelectedBeams(val).Sheartab.Thk / 16, 1 / 16), 0.25) * 16
                ''Update_Step4()
                'ElseIf e.ColumnIndex = 4 Then 'change beam size
                '    SelectedBeams(val).Wsec = clsSE.AllWsections.Wsections.Find(Function(x) x.Size = newVal)
                '    clsSE.ReAssignMembersize(SelectedBeams(val)._uName, newVal)
                '    'update shear plate properties
                '    Dim N_horzB As Integer
                '    Dim bSize As bSize
                '    Dim bTyp As bType
                '    Dim STThick, WeldThk As eThickness
                '    N_horzB = 2
                '    bSize = mPubFunc.bSize.B7_8
                '    bTyp = bType.A325X
                '    STThick = eThickness.T1_2
                '    WeldThk = Math.Ceiling(5 / 8 * STThick)
                '    Dim strW3 As String = Microsoft.VisualBasic.Left(SelectedBeams(val).Wsec.Size, 3)
                '    Dim stBolt As SheartabBolt = clsSE.SSTOthers.SheartabBolts.Find(Function(x) x.Size = strW3)
                '    SelectedBeams(val).Sheartab = New clsSSTSheartab(SelectedBeams(val).Wsec, SelectedBeams(val).LKInfo, stBolt, N_horzB, bSize, bTyp, STThick, WeldThk, SelectedBeams(val).Pu_bm, SelectedBeams(val).V_bmGravity)

                '    dgvStep4(8, e.RowIndex).Value = SelectedBeams(val).Sheartab.stBolt.n_Vbolts_SST
                '    dgvStep4(9, e.RowIndex).Value = SelectedBeams(val).Sheartab.N_horzBolt.ToString
                '    dgvStep4(10, e.RowIndex).Value = GetBoltSize(SelectedBeams(val).Sheartab.bSize)
                '    dgvStep4(11, e.RowIndex).Value = SelectedBeams(val).Sheartab.bType.ToString
                '    dgvStep4(12, e.RowIndex).Value = eThickness2String(SelectedBeams(val).Sheartab.Thk)
                '    Update_Step1(False)
            Else
                Return
            End If
            '
            SelectedBeams(val).SPC_Detail(gJob)
            'Check

            dgvStep4(13, e.RowIndex).Value = eThickness2String(SelectedBeams(val).Sheartab.WeldSize)
            dgvStep4(14, e.RowIndex).Value = SelectedBeams(val).Sheartab.sDCR1_BmWeb
            dgvStep4(15, e.RowIndex).Value = SelectedBeams(val).Sheartab.sDCR2_ShearPLGeometry
            dgvStep4(16, e.RowIndex).Value = SelectedBeams(val).Sheartab.sDCR3_ShearPL
            dgvStep4(17, e.RowIndex).Value = SelectedBeams(val).Sheartab.sDCR4_Bolt
            dgvStep4(18, e.RowIndex).Value = SelectedBeams(val).Sheartab.sDCR5_Weld
            'Update Color
            Update_StatusColor_LastColumn(4)
        Catch ex As Exception
        Finally
            isUserEdit = True
        End Try
    End Sub

    Private Sub dgvStep5_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStep5.CellValueChanged
        If Not isUserEdit Then Return
        '
        Dim nodeBelowCol As Integer = 9
        Dim dxSeismicCol As Integer = 10
        Dim dxWindCol As Integer = 11
        Dim styHCol As Integer = 12
        Dim aSeismicCol As Integer = 13
        Dim aWindCol As Integer = 14
        Dim dcrSeismicCol As Integer = 15
        Dim dcrWindCol As Integer = 16
        Try
            If e.ColumnIndex = nodeBelowCol Then 'Node below
                Dim bm As clsSSTBeam = SelectedBeams.Find(Function(x) x._uName = dgvStep5(3, e.RowIndex).Value)
                Dim nodeBelow As String = dgvStep5(e.ColumnIndex, e.RowIndex).Value
                '
                If nodeBelow <> bm.EndI_ColBottom.Pt_I Then
                    bm.NodeBelow_user = nodeBelow
                Else
                    bm.NodeBelow_user = ""
                End If
                '
                Update_Step5(e.RowIndex)
            ElseIf e.ColumnIndex = styHCol Then 'Story Height
                Dim newHeight, dx_Seismic, dx_Wind As Double
                If Double.TryParse(dgvStep5(e.ColumnIndex, e.RowIndex).Value, newHeight) Then
                    Dim bm As clsSSTBeam = SelectedBeams.Find(Function(x) x._uName = dgvStep5(3, e.RowIndex).Value)
                    'Get current dX_Seismic
                    Double.TryParse(dgvStep5(dxSeismicCol, e.RowIndex).Value, dx_Seismic)
                    'Get current dX_Wind
                    Double.TryParse(dgvStep5(dxWindCol, e.RowIndex).Value, dx_Wind)
                    '
                    bm.aSeismicDriftLimit = newHeight * GetSeismicDriftLimit() / If(gJob.DCRLimits.DivideByRho, gJob.Rho, 1)
                    bm.aWindDriftLimit = newHeight / GetWindDriftLimit()
                    '
                    If Math.Abs(newHeight - bm.HeightDrift) < 0.001 Then
                        bm.Height_user = -1
                        With dgvStep5(e.ColumnIndex, e.RowIndex)
                            isUserEdit = False
                            .Value = NumberFormat(newHeight, 2)
                            .Style.ForeColor = Nothing
                            isUserEdit = True
                        End With
                    ElseIf bm.Height_user > 0 Then 'already existing user height
                        bm.Height_user = newHeight
                        With dgvStep5(e.ColumnIndex, e.RowIndex)
                            .Value = NumberFormat(newHeight, 2)
                            .Style.ForeColor = Color.DarkRed
                        End With
                    Else
                        bm.Height_user = newHeight
                        With dgvStep5(e.ColumnIndex, e.RowIndex)
                            isUserEdit = False
                            .Value = NumberFormat(newHeight, 2)
                            .Style.ForeColor = Nothing
                            isUserEdit = True
                        End With
                    End If
                    '
                    Dim SeismicDriftDCR As Double = dx_Seismic / bm.aSeismicDriftLimit
                    bm.status_SeismicDrift = NumberFormat(SeismicDriftDCR, 3)
                    '
                    Dim WindDrifDCR As Double = dx_Wind / bm.aWindDriftLimit
                    bm.status_WindDrift = NumberFormat(WindDrifDCR, 3)
                    '
                    dgvStep5(aSeismicCol, e.RowIndex).Value = NumberFormat(bm.aSeismicDriftLimit, 3)
                    dgvStep5(aWindCol, e.RowIndex).Value = NumberFormat(bm.aWindDriftLimit, 3)
                    dgvStep5(dcrSeismicCol, e.RowIndex).Value = bm.status_SeismicDrift
                    dgvStep5(dcrWindCol, e.RowIndex).Value = bm.status_WindDrift
                    '
                    Update_StatusColor_LastColumn(5)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvStep7_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStep7.CellValueChanged
        If Not isUserEdit Or isPrevent Then Return
        Try
            Dim newVal = dgvStep7(e.ColumnIndex, e.RowIndex).Value
            Dim col_uName = dgvStep7(3, e.RowIndex).Value
            ''Dim FL As Double
            If e.ColumnIndex = 9 Then
                Dim curCol As clsSSTColumn = SelectedColumns.Find(Function(x) x._uName = col_uName)
                curCol.isBraceAtBmBotFKG = (newVal = "YES")
                '
                curCol.UpdateDesign(gJob)
                '
                dgvStep7(16, e.RowIndex).Value = NumberFormat(curCol.DCR_Bmaj_adj, 3)
                dgvStep7(17, e.RowIndex).Value = NumberFormat(curCol.DCR_Total_Adj, 3)
                '
                Update_StatusColor_LastColumn(7)
            End If
        Catch ex As Exception
        Finally
            isUserEdit = True
        End Try
    End Sub
    Private Sub dgvStep1_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvStep1.RowsAdded
        Dim newVal = dgvStep1(5, e.RowIndex).Value
        Dim isPinned As Boolean = (newVal = "Pinned")
        dgvStep1(9, e.RowIndex).ReadOnly = isPinned
        dgvStep1(10, e.RowIndex).ReadOnly = isPinned
        '
        DisableButton()
    End Sub
    Private Sub dgvStep1_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dgvStep1.RowsRemoved
        DisableButton()
    End Sub

    Private Sub DisableButton()
        Dim isEnable As Boolean = dgvStep1.RowCount > 0
        btnPDFSummary.Enabled = isEnable
        btnPDFSummary2.Enabled = isEnable
        btnExportDXF.Enabled = isEnable
        btnExportDXF2.Enabled = isEnable
    End Sub
    Private Sub Update_StatusColor_LastColumn(flag As Integer)
        BringWindowToTop(Me.Handle)
        ''Dim dgv As DataGridView
        Dim Arr() As Integer
        Dim tmpVal As Double
        Dim DCRlimit As Double
        If flag = 1 Then
            Arr = New Integer() {11, 12, 13, 14, 15, 16}
            For row As Integer = 0 To dgvStep1.RowCount - 1
                For Each col As Integer In Arr
                    If dgvStep1(col, row).ToolTipText <> "" Then
                        dgvStep1(col, row).Style.BackColor = Color.Orange
                    Else
                        dgvStep1(col, row).Style.BackColor = Nothing
                    End If

                    If dgvStep1(col, row).Value Like "*C" Or dgvStep1(col, row).Value Like "*A" Then
                        dgvStep1(col, row).Style.ForeColor = Color.Black
                        Continue For
                    ElseIf dgvStep1(col, row).Value Like "*G" Then
                        dgvStep1(col, row).Style.ForeColor = Color.Red
                        Continue For
                    ElseIf dgvStep1(col, row).Value = "OK" Then
                        dgvStep1(col, row).Style.ForeColor = Color.Blue
                        Continue For
                    End If
                    '
                    If col = 13 Then
                        DCRlimit = gJob.DCRLimits.Lyield_check
                    ElseIf col = 14 Or col = 15 Then
                        DCRlimit = gJob.DCRLimits.Panel_Zone_DCR
                    ElseIf col = 16 Then
                        DCRlimit = gJob.DCRLimits.Drift_check
                    Else
                        DCRlimit = 1
                    End If

                    If Double.TryParse(dgvStep1(col, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                        dgvStep1(col, row).Style.ForeColor = Color.Blue
                    Else
                        dgvStep1(col, row).Style.ForeColor = Color.Red
                    End If
                Next
            Next
        ElseIf flag = 2 Then
            Arr = New Integer() {8, 9, 10, 11, 12}
            For row As Integer = 0 To dgvStep2.RowCount - 1
                For Each col As Integer In Arr
                    If dgvStep2(col, row).Value Like "*C" Or dgvStep2(col, row).Value Like "*A" Then
                        dgvStep2(col, row).Style.ForeColor = Color.Black
                        Continue For
                    ElseIf dgvStep2(col, row).Value Like "*G" Then
                        dgvStep2(col, row).Style.ForeColor = Color.Red
                        Continue For
                    ElseIf dgvStep2(col, row).Value = "OK" Then
                        dgvStep2(col, row).Style.ForeColor = Color.Blue
                        Continue For
                    End If
                    '
                    If col = 9 Then
                        DCRlimit = gJob.DCRLimits.Link_strength_DCR
                    ElseIf col = 10 Then
                        DCRlimit = gJob.DCRLimits.Panel_Zone_DCR
                    Else
                        DCRlimit = 1
                    End If

                    If Double.TryParse(dgvStep2(col, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                        dgvStep2(col, row).Style.ForeColor = Color.Blue
                    Else
                        dgvStep2(col, row).Style.ForeColor = Color.Red
                    End If
                Next
            Next
        ElseIf flag = 3 Then
            Arr = New Integer() {14, 15, 16}
            For row As Integer = 0 To dgvStep3.RowCount - 1
                ''Stiff required
                'Try
                '    Dim tmpCell As DataGridViewComboBoxCell = dgvStep3(9, row)
                '    tmpCell.Items.Clear()
                If dgvStep3(11, row).Value.ToString = "YES" Then
                    dgvStep3(11, row).Style.ForeColor = Color.Blue
                    '        tmpCell.Items.AddRange("YES")
                Else
                    dgvStep3(11, row).Style.ForeColor = Color.Black
                    '        tmpCell.Items.AddRange("YES", "NO")
                End If

                '    Dim tmpCell2 As DataGridViewComboBoxCell = dgvStep3(10, row)
                '    tmpCell2.Items.Clear()
                '    If dgvStep3(12, row).Value > 0 Then
                '        tmpCell2.Items.AddRange("YES")
                '    Else
                '        tmpCell2.Items.AddRange("YES", "NO")
                '    End If
                'Catch ex As Exception
                'End Try
                '
                For Each col As Integer In Arr
                    If dgvStep3(col, row).Value Like "*C" Or dgvStep3(col, row).Value Like "*A" Then
                        dgvStep3(col, row).Style.ForeColor = Color.Black
                        Continue For
                    ElseIf dgvStep3(col, row).Value Like "*G" Then
                        dgvStep3(col, row).Style.ForeColor = Color.Red
                        Continue For
                    ElseIf dgvStep3(col, row).Value = "OK" Then
                        dgvStep3(col, row).Style.ForeColor = Color.Blue
                        Continue For
                    End If
                    '
                    If col = 16 Then
                        DCRlimit = gJob.DCRLimits.Panel_Zone_DCR
                    Else
                        DCRlimit = 1
                    End If

                    If Double.TryParse(dgvStep3(col, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                        dgvStep3(col, row).Style.ForeColor = Color.Blue
                    Else
                        dgvStep3(col, row).Style.ForeColor = Color.Red
                    End If
                Next
            Next
        ElseIf flag = 4 Then
            Arr = New Integer() {14, 15, 16, 17, 18}
            For row As Integer = 0 To dgvStep4.RowCount - 1
                For Each col As Integer In Arr
                    If dgvStep4(col, row).Value Like "*C" Or dgvStep4(col, row).Value Like "*A" Then
                        dgvStep4(col, row).Style.ForeColor = Color.Black
                        Continue For
                    ElseIf dgvStep4(col, row).Value Like "*G" Then
                        dgvStep4(col, row).Style.ForeColor = Color.Red
                        Continue For
                    ElseIf dgvStep4(col, row).Value = "OK" Then
                        dgvStep4(col, row).Style.ForeColor = Color.Blue
                        Continue For
                    End If
                    '
                    DCRlimit = 1

                    If Double.TryParse(dgvStep4(col, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                        dgvStep4(col, row).Style.ForeColor = Color.Blue
                    Else
                        dgvStep4(col, row).Style.ForeColor = Color.Red
                    End If
                Next
            Next
        ElseIf flag = 5 Then
            Dim SeismicCol As Integer = 15 'seismic
            Dim WindCol As Integer = 16 'seismic
            For row As Integer = 0 To dgvStep5.RowCount - 1
                If dgvStep5(WindCol, row).Value Like "*C" Or dgvStep5(WindCol, row).Value Like "*A" Then
                    dgvStep5(WindCol, row).Style.ForeColor = Color.Black
                    Continue For
                ElseIf dgvStep5(WindCol, row).Value Like "*G" Then
                    dgvStep5(WindCol, row).Style.ForeColor = Color.Red
                    Continue For
                ElseIf dgvStep5(WindCol, row).Value = "OK" Then
                    dgvStep5(WindCol, row).Style.ForeColor = Color.Blue
                    Continue For
                End If
                '
                DCRlimit = gJob.DCRLimits.Drift_check
                '
                If Double.TryParse(dgvStep5(SeismicCol, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                    dgvStep5(SeismicCol, row).Style.ForeColor = Color.Blue
                Else
                    dgvStep5(SeismicCol, row).Style.ForeColor = Color.Red
                End If
                'Wind---------------------
                If Double.TryParse(dgvStep5(WindCol, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                    dgvStep5(WindCol, row).Style.ForeColor = Color.Blue
                Else
                    dgvStep5(WindCol, row).Style.ForeColor = Color.Red
                End If
            Next
        ElseIf flag = 6 Then
            Arr = New Integer() {13, 14, 15, 16, 17, 18} 'two hidden columns
            For row As Integer = 0 To dgvStep6.RowCount - 1
                For Each col As Integer In Arr
                    If dgvStep6(col, row).Value Like NA Then
                        dgvStep6(col, row).Style.ForeColor = Color.Black
                        Continue For
                    End If
                    '
                    DCRlimit = 1
                    '
                    If Double.TryParse(dgvStep6(col, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                        dgvStep6(col, row).Style.ForeColor = Color.Blue
                    Else
                        dgvStep6(col, row).Style.ForeColor = Color.Red
                    End If
                Next
            Next
        ElseIf flag = 7 Then
            Arr = New Integer() {10, 11, 12, 13, 14, 15, 16, 17}
            For row As Integer = 0 To dgvStep7.RowCount - 1
                For Each col As Integer In Arr
                    If dgvStep7(col, row).Value Like NA Then
                        dgvStep7(col, row).Style.ForeColor = Color.Black
                        Continue For
                    End If
                    '
                    DCRlimit = 1
                    '
                    If Double.TryParse(dgvStep7(col, row).Value, tmpVal) AndAlso tmpVal <= DCRlimit Then
                        dgvStep7(col, row).Style.ForeColor = Color.Blue
                    Else
                        dgvStep7(col, row).Style.ForeColor = Color.Red
                    End If
                Next
            Next
        Else
            Return
        End If
    End Sub

    Private dgvCell As DataGridViewCell
    Private Sub dgv_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvStep7.CellMouseDown, dgvStep5.CellMouseDown, dgvStep4.CellMouseDown, dgvStep3.CellMouseDown, dgvStep2.CellMouseDown, dgvStep1.CellMouseDown
        dgvCell = Nothing
        If e.Button = Windows.Forms.MouseButtons.Right Then
            If Not sender.CurrentCell.ReadOnly Then
                cmRighClick.Items(0).Visible = True
                cmRighClick.Items(1).Visible = True
                cmRighClick.Items(2).Visible = False
                Dim isVisible = (sender.CurrentCell.columnindex = 4 Or sender.CurrentCell.columnindex = 5 Or sender.CurrentCell.columnindex = 7 Or sender.CurrentCell.columnindex = 8)
                cmRighClick.Items(3).Visible = isVisible
                cmRighClick.Items(4).Visible = isVisible
                Dim str = Clipboard.GetText
                If sender.CurrentCell.columnindex = 5 Then
                    cmRighClick.Items(4).Enabled = Not gJob.SSTLinks.Links.Find(Function(x) x.Size = str) Is Nothing
                Else
                    'cmRighClick.Items(0).Visible = False
                    'cmRighClick.Items(1).Visible = False
                    cmRighClick.Items(4).Enabled = Not gJob.AllWsections.Wsections.Find(Function(x) x.Size = str) Is Nothing
                End If
                ''righClickCol = e.ColumnIndex
                dgvCell = sender.CurrentCell
                '''MsgBox(sender.name & " - " & dgvCell.Value & " - Col=" & sender.CurrentCell.columnindex & " - Row=" & sender.CurrentCell.rowindex)
                '
                cmRighClick.Show(MousePosition)
            ElseIf sender.CurrentCell.ColumnIndex = 3 Then
                If sender.name <> "dgvStep5" And sender.name <> "dgvStep6" And sender.name <> "dgvStep7" And sender.name <> "dgvStep8" Then
                    'Detailled report
                    cmRighClick.Items(0).Visible = False
                    cmRighClick.Items(1).Visible = False
                    cmRighClick.Items(2).Visible = True
                    cmRighClick.Items(2).Enabled = True
                    cmRighClick.Items(3).Visible = False
                    cmRighClick.Items(4).Visible = False
                    If sender.name = "dgvStep1" Then
                        cmRighClick.Items(1).Enabled = dgvStep1(5, dgvStep1.CurrentCell.RowIndex).Value <> "Pinned"
                    ElseIf sender.name = "dgvStep3" Then
                        cmRighClick.Items(1).Enabled = Not (dgvStep3(5, dgvStep3.CurrentCell.RowIndex).Value = "Pinned" AndAlso dgvStep3(6, dgvStep3.CurrentCell.RowIndex).Value = "Pinned")
                    End If
                    '
                    cmRighClick.Show(MousePosition)
                End If
            End If
        End If
    End Sub
    'Private isupdateDgv As Boolean
    Private Sub btnApplyLevel_Click(sender As Object, e As EventArgs) Handles btnApplyLevel.Click
        Apply_Selected(False, "")
    End Sub
    Private Sub btnApplyElevation_Click(sender As Object, e As EventArgs) Handles btnApplyElevation.Click
        Apply_Selected(True, "")
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        Clipboard.SetText(dgvStep1.CurrentCell.Value)
    End Sub

    Private stlChangedFrames As New SortedList(Of String, String)
    Private Sub btnPaste_Click(sender As Object, e As EventArgs) Handles btnPaste.Click
        Apply_Selected(False, Clipboard.GetText)
    End Sub

    Private Sub Apply_Selected(isAllElevation As Boolean, clipboardVal As String)
        Try
            Dim cnt As Integer = 0
            '
            'isupdateDgv = False
            Cursor = Cursors.WaitCursor
            lbStatus.Text = "Please wait..."
            If dgvCell Is Nothing Then Return
            '
            Dim dgv As DataGridView = Nothing
            If tabMain.SelectedIndex = 1 Then
                dgv = dgvStep2
            ElseIf tabMain.SelectedIndex = 2 Then
                dgv = dgvStep3
            ElseIf tabMain.SelectedIndex = 3 Then
                dgv = dgvStep4
            ElseIf tabMain.SelectedIndex = 4 Then
                dgv = dgvStep5
            ElseIf tabMain.SelectedIndex = 6 Then
                dgv = dgvStep7
            Else
                dgv = dgvStep1
            End If
            '
            multiChange = (dgvCell.ColumnIndex = 4 Or dgvCell.ColumnIndex = 7 Or dgvCell.ColumnIndex = 8)
            '
            If clipboardVal = "" Then
                For i As Integer = 0 To dgv.RowCount - 1
                    If dgv(dgvCell.ColumnIndex, i).Visible = False Then Continue For
                    If i = dgvCell.RowIndex Then Continue For
                    '
                    If dgv(0, i).Value <> dgv(0, dgvCell.RowIndex).Value Then Continue For 'filter Elevation
                    If isAllElevation = False Then
                        If dgv(2, i).Value <> dgv(2, dgvCell.RowIndex).Value Then Continue For 'filter Story
                    End If
                    '
                    dgv(dgvCell.ColumnIndex, i).Value = dgvCell.Value
                    '
                    cnt += 1
                Next
            Else
                For Each cc As DataGridViewCell In dgv.SelectedCells
                    cc.Value = clipboardVal
                    '
                    cnt += 1
                Next
            End If
            '
            If multiChange And stlChangedFrames.Count > 0 Then
                clsSE.ReAssignMembersize(stlChangedFrames)
                stlChangedFrames.Clear()
            End If
            '
            dgv = Nothing
            'isupdateDgv = True
            If tabMain.SelectedIndex = 0 Then Update_Step1(False)
            lbStatus.Text = "Done: (" & cnt & ") items have been changed"

        Catch ex As Exception
            lbStatus.Text = "Error!"
            ''MsgBox(ex.Message & ex.StackTrace)
        Finally
            multiChange = False
            dgvCell = Nothing
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub btnApplySelected_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnCreateLoadCombinations_Click(sender As Object, e As EventArgs) Handles btnLoadCombos2.Click
        Dim frm As frmMsgboxLcm
        Try
            lbStatus.Text = "Waiting..."
            ' lbStatus.Text = clsSE.CreateLoadConbination()
            frm = New frmMsgboxLcm(gJob.isEtab)
            frm.ShowDialog()
            '
            If frm.resVal = 1 Then
                lbStatus.Text = clsSE.CreateELFLoadConbination(gJob, False, frm.iskeepLP_LC)

            ElseIf frm.resVal = 2 Then
                lbStatus.Text = clsSE.CreateELFLoadConbination(gJob, True, frm.iskeepLP_LC)
            ElseIf frm.resVal = 3 Then
                lbStatus.Text = clsSE.CreateMRSALoadConbination(gJob, False, frm.iskeepLP_LC)
            ElseIf frm.resVal = 4 Then
                lbStatus.Text = clsSE.CreateMRSALoadConbination(gJob, True, frm.iskeepLP_LC)
            Else
                lbStatus.Text = "Cancelled!!!"
            End If
        Catch ex As Exception
        Finally
            frm = Nothing
        End Try
    End Sub

    'Private Sub btnExportExcel_Click(sender As Object, e As EventArgs) Handles btnExportExcel.Click
    '    If dgvStep1.RowCount > 0 Then
    '        lbStatus.Text = "Exporting..."
    '        lbStatus.Text = ExportToExcel(dgvStep1, dgvStep2, dgvStep3, dgvStep4, dgvStep5, gJob)
    '    End If
    'End Sub

    Private Sub btnAbout_Click(sender As Object, e As EventArgs) Handles btnAbout.Click
        'MsgBox(toolName & vbLf & "Version " & toolVer & vbLf & vbLf & "� Copyright 2018 Simpson Strong-Tie", , toolName)
        Dim frm As New frmAbout(True)
        frm.ShowDialog()
    End Sub

    Private Sub btnUsersManual_Click(sender As Object, e As EventArgs) Handles btnUsersManual.Click
        Try
            Dim tempFile = IO.Path.Combine(mPath, "Data/Yield-Link Plugin User Guide.pdf")
            Process.Start(tempFile)
            lbStatus.Text = "The pdf file opened in separate window"
        Catch ex As Exception
            lbStatus.Text = "Error: " & ex.Message.Split(".")(0)
        End Try
    End Sub
    Private Sub btnInitialSizes_Click(sender As Object, e As EventArgs) Handles btnInitialSizes.Click
        Try
            Dim tempFile = IO.Path.Combine(mPath, "Data/YLMC Initial Beam-Col Matches.pdf")
            Process.Start(tempFile)
            lbStatus.Text = "The pdf file opened in separate window"
        Catch ex As Exception
            lbStatus.Text = "Error: " & ex.Message.Split(".")(0)
        End Try
    End Sub

    Private Sub btnCh12AISC358_20_Click(sender As Object, e As EventArgs) Handles btnESR_2802.Click, btnCh12AISC358_20.Click
        Process.Start(sender.ToolTipText)
        'Try
        '    Dim tempFile = IO.Path.Combine(mPath, "Data/CH12_AISC358-16.pdf")
        '    Process.Start(tempFile)
        '    lbStatus.Text = "The pdf file opened in separate window"
        'Catch ex As Exception
        '    lbStatus.Text = "Error: " & ex.Message.Split(".")(0)
        'End Try
    End Sub

    'Private Sub btnESR_2802_Click(sender As Object, e As EventArgs) Handles btnESR_2802.Click
    '    Try
    '        Dim tempFile = IO.Path.Combine(mPath, "Data/ESR-2802.pdf")
    '        Process.Start(tempFile)
    '        lbStatus.Text = "The pdf file opened in separate window"
    '    Catch ex As Exception
    '        lbStatus.Text = "Error: " & ex.Message.Split(".")(0)
    '    End Try
    'End Sub

    Private Sub SSTLinksPlugin_MouseHover(sender As Object, e As EventArgs) Handles dgvStep4.MouseHover, dgvStep3.MouseHover, dgvStep2.MouseHover, dgvStep1.MouseHover
        Cursor = Cursors.Arrow
    End Sub

    Private Sub cbGridcbStoryStep1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep1.SelectedIndexChanged, cbElevStep1.SelectedIndexChanged
        If Not isUserEdit Then Return
        '
        If cbElevStep1.SelectedIndex < 1 And cbStoryStep1.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep1.Rows
                r.Visible = True
            Next
            For Each r As DataGridViewRow In dgvStep2.Rows
                r.Visible = True
            Next
            For Each r As DataGridViewRow In dgvStep3.Rows
                r.Visible = True
            Next
            For Each r As DataGridViewRow In dgvStep4.Rows
                r.Visible = True
            Next
            For Each r As DataGridViewRow In dgvStep5.Rows
                r.Visible = True
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep1.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep1.Text Or cbElevStep1.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep1.Text Or cbStoryStep1.Text Like "All*")
        Next
        For Each r As DataGridViewRow In dgvStep2.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep1.Text Or cbElevStep1.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep1.Text Or cbStoryStep1.Text Like "All*")
        Next
        For Each r As DataGridViewRow In dgvStep3.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep1.Text Or cbElevStep1.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep1.Text Or cbStoryStep1.Text Like "All*")
        Next
        '
        For Each r As DataGridViewRow In dgvStep4.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep1.Text Or cbElevStep1.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep1.Text Or cbStoryStep1.Text Like "All*")
        Next
        For Each r As DataGridViewRow In dgvStep5.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep1.Text Or cbElevStep1.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep1.Text Or cbStoryStep1.Text Like "All*")
        Next
    End Sub
    Private Sub cbGridcbStoryStep2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep2.SelectedIndexChanged, cbElevStep2.SelectedIndexChanged
        If cbElevStep2.SelectedIndex < 1 And cbStoryStep2.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep2.Rows
                r.Visible = True '(r.Cells(1).Value = cbGrid.Text Or cbGrid.Text Like "All*") AndAlso (r.Cells(2).Value = cbStory.Text Or cbStory.Text Like "All*")
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep2.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep2.Text Or cbElevStep2.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep2.Text Or cbStoryStep2.Text Like "All*")
        Next
    End Sub
    Private Sub cbGridcbStoryStep3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep3.SelectedIndexChanged, cbElevStep3.SelectedIndexChanged
        If cbElevStep3.SelectedIndex < 1 And cbStoryStep3.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep3.Rows
                r.Visible = True '(r.Cells(1).Value = cbGrid.Text Or cbGrid.Text Like "All*") AndAlso (r.Cells(2).Value = cbStory.Text Or cbStory.Text Like "All*")
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep3.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep3.Text Or cbElevStep3.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep3.Text Or cbStoryStep3.Text Like "All*")
        Next
    End Sub
    Private Sub cbGridcbStoryStep4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep4.SelectedIndexChanged, cbElevStep4.SelectedIndexChanged
        If cbElevStep4.SelectedIndex < 1 And cbStoryStep4.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep4.Rows
                r.Visible = True '(r.Cells(1).Value = cbGrid.Text Or cbGrid.Text Like "All*") AndAlso (r.Cells(2).Value = cbStory.Text Or cbStory.Text Like "All*")
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep4.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep4.Text Or cbElevStep4.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep4.Text Or cbStoryStep4.Text Like "All*")
        Next
    End Sub
    Private Sub cbGridcbStoryStep5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep5.SelectedIndexChanged, cbElevStep5.SelectedIndexChanged
        If cbElevStep5.SelectedIndex < 1 And cbStoryStep5.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep5.Rows
                r.Visible = True '(r.Cells(1).Value = cbGrid.Text Or cbGrid.Text Like "All*") AndAlso (r.Cells(2).Value = cbStory.Text Or cbStory.Text Like "All*")
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep5.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep5.Text Or cbElevStep5.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep5.Text Or cbStoryStep5.Text Like "All*")
        Next
    End Sub
    Private Sub cbGridcbStoryStep6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep6.SelectedIndexChanged, cbElevStep6.SelectedIndexChanged
        If cbElevStep6.SelectedIndex < 1 And cbStoryStep6.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep6.Rows
                r.Visible = True '(r.Cells(1).Value = cbGrid.Text Or cbGrid.Text Like "All*") AndAlso (r.Cells(2).Value = cbStory.Text Or cbStory.Text Like "All*")
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep6.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep6.Text Or cbElevStep6.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep6.Text Or cbStoryStep6.Text Like "All*")
        Next
    End Sub
    Private Sub cbGridcbStoryStep7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep7.SelectedIndexChanged, cbElevStep7.SelectedIndexChanged
        If cbElevStep7.SelectedIndex < 1 And cbStoryStep7.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep7.Rows
                r.Visible = True '(r.Cells(1).Value = cbGrid.Text Or cbGrid.Text Like "All*") AndAlso (r.Cells(2).Value = cbStory.Text Or cbStory.Text Like "All*")
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep7.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep7.Text Or cbElevStep7.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep7.Text Or cbStoryStep7.Text Like "All*")
        Next
    End Sub
    Private Sub cbGridcbStoryStep8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbStoryStep8.SelectedIndexChanged, cbElevStep8.SelectedIndexChanged
        If cbElevStep8.SelectedIndex < 1 And cbStoryStep8.SelectedIndex < 1 Then
            For Each r As DataGridViewRow In dgvStep8.Rows
                r.Visible = True '(r.Cells(1).Value = cbGrid.Text Or cbGrid.Text Like "All*") AndAlso (r.Cells(2).Value = cbStory.Text Or cbStory.Text Like "All*")
            Next
            '
            Return
        End If
        '
        For Each r As DataGridViewRow In dgvStep8.Rows
            r.Visible = (r.Cells(0).Value = cbElevStep8.Text Or cbElevStep8.Text Like "All*") AndAlso (r.Cells(2).Value = cbStoryStep8.Text Or cbStoryStep8.Text Like "All*")
        Next
    End Sub

    Public Function GenerateNewName(sPath As String, Optional suggestName As String = "Combined", Optional suggestExt As String = ".pdf") As String
        Dim newName = IO.Path.Combine(sPath, suggestName & suggestExt)
        Try
            IO.File.Delete(newName)
        Catch ex As Exception
            Dim cnt = 1
            Do
                newName = IO.Path.Combine(sPath, suggestName & " (" & cnt & ")" & suggestExt)
                cnt += 1
            Loop While IO.File.Exists(newName)
        End Try
        '
        Return newName
    End Function

    Private Sub picLogo_Click(sender As Object, e As EventArgs) Handles picLogo.Click
        Process.Start("https://www.strongtie.com/")
    End Sub

    Private Function PushDataToGjob(isSave As Boolean, isCurrent As Boolean) As String
        Dim strData, strGridEle As String
        strGridEle = "Elevation "
        '
        'gJob.DgvGridX.Clear()
        'gJob.DgvGridY.Clear()
        'gJob.DgvGridZ.Clear()
        gJob.Dgv1Data.Clear()
        gJob.Dgv2Data.Clear()
        gJob.Dgv3Data.Clear()
        gJob.Dgv4Data.Clear()
        gJob.Dgv5Data.Clear()
        gJob.Dgv6Data.Clear()
        gJob.Dgv7Data.Clear()
        gJob.Dgv8Data.Clear()
        ''
        'For i As Integer = 0 To clsSE.Grids_X.Count - 1
        '    gJob.DgvGridX.Add(clsSE.Grids_X(i).GridName & gJob.splChar & clsSE.Grids_X(i).GridX1)
        'Next
        'For i As Integer = 0 To clsSE.Grids_Y.Count - 1
        '    gJob.DgvGridY.Add(clsSE.Grids_Y(i).GridName & gJob.splChar & clsSE.Grids_Y(i).GridY1)
        'Next
        'For i As Integer = 0 To clsSE.Stories.Count - 1
        '    gJob.DgvGridZ.Add(clsSE.Stories.Values(i) & gJob.splChar & clsSE.Stories.Keys(i))
        'Next
        '
        Dim lstEle As New List(Of String)
        Try
            For i As Integer = 0 To dgvStep1.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep1.ColumnCount - 1
                    If j = 0 Then If Not lstEle.Contains(dgvStep1(j, i).Value) Then lstEle.Add(dgvStep1(j, i).Value)
                    '
                    strData += dgvStep1(j, i).Value & gJob.splChar
                Next
                gJob.Dgv1Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv1Data.Clear()
        End Try
        For Each item As String In lstEle
            strGridEle += item & ","
        Next
        Try
            For i As Integer = 0 To dgvStep2.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep2.ColumnCount - 1
                    strData += dgvStep2(j, i).Value & gJob.splChar
                Next
                gJob.Dgv2Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv2Data.Clear()
        End Try
        Try
            For i As Integer = 0 To dgvStep3.RowCount - 1
                strData = ""
                For j As Integer = 0 To IIf(isSave, dgvStep3.ColumnCount - 1, Math.Min(dgvStep3.ColumnCount - 1, 18))
                    strData += dgvStep3(j, i).Value & gJob.splChar
                Next
                gJob.Dgv3Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv3Data.Clear()
        End Try
        Try
            For i As Integer = 0 To dgvStep4.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep4.ColumnCount - 1
                    strData += dgvStep4(j, i).Value & gJob.splChar
                Next
                gJob.Dgv4Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv4Data.Clear()
        End Try
        'DGV5
        Try
            For i As Integer = 0 To dgvStep5.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep5.ColumnCount - 1
                    'cu 11-13, moi 9-12
                    If isCurrent = False AndAlso j = 11 Then
                        strData += dgvStep5(9, i).Value & gJob.splChar 'lay gia tri col9
                        strData += dgvStep5(12, i).Value & gJob.splChar 'lay gia tri col9
                    Else
                        strData += dgvStep5(j, i).Value & gJob.splChar
                    End If
                Next
                gJob.Dgv5Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv5Data.Clear()
        End Try '
        'DGV6 -Beam Design
        Try
            For i As Integer = 0 To dgvStep6.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep6.ColumnCount - 1
                    If dgvStep6.Columns(j).Visible = False Then Continue For
                    '
                    strData += dgvStep6(j, i).Value & gJob.splChar
                Next
                gJob.Dgv6Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv6Data.Clear()
        End Try
        'DGV7 -Column Design
        Try
            For i As Integer = 0 To dgvStep7.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep7.ColumnCount - 1
                    strData += dgvStep7(j, i).Value & gJob.splChar
                Next
                gJob.Dgv7Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv7Data.Clear()
        End Try
        'DGV8 -Weld Summary
        Try
            For i As Integer = 0 To dgvStep8.RowCount - 1
                strData = ""
                For j As Integer = 0 To dgvStep8.ColumnCount - 1
                    strData += dgvStep8(j, i).Value & gJob.splChar
                Next
                gJob.Dgv8Data.Add(strData)
            Next
        Catch ex As Exception
            gJob.Dgv8Data.Clear()
        End Try
        '
        If strGridEle.EndsWith(",") Then strGridEle = strGridEle.Remove(strGridEle.Length - 1, 1)
        Return strGridEle
    End Function

    Private Sub btnExportPDFSummary_Click(sender As Object, e As EventArgs) Handles btnPDFSummary2.Click, btnPDFSummary.Click
        Dim strF As String = GenerateNewName(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Summary_Combined")
        '
        Dim tmpCursor As Cursor
        Dim status As String = "Error!"
        Try
            lbStatus.Text = "Exporting..."
            tmpCursor = Cursor
            Cursor = Cursors.WaitCursor
            Application.DoEvents()
            '
            If Not isDebug Then
                PushDataToGjob(False, True)
            End If

            '
            Dim pdf As New clsReport(gJob, strF, ARType.T0_SumReport)
            status = pdf.Export()
            '
            If System.IO.File.Exists(strF) Then
                Process.Start(strF)
            End If
            '
            lbStatus.Text = status
        Catch ex As Exception
            lbStatus.Text = "Error: " & ex.Message.ToString.Split(".")(0) & "."
            'MsgBox(ex.Message & ex.StackTrace)
        Finally
            ' Replace the cursor
            Cursor = tmpCursor
            Application.DoEvents()
        End Try
    End Sub

    Private Sub ViewDetailedReport()

        Dim strF, retStatus As String
        '
        ''Dim tmpCursor As Cursor
        Try
            lbStatus.Text = "Exporting..."
            ''tmpCursor = Cursor
            Cursor = Cursors.WaitCursor
            Application.DoEvents()
            'k
            Dim cnt As Long
            If tabMain.SelectedIndex = 0 Then
                strF = GenerateNewName(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Bm" & dgvStep1.CurrentCell.Value & "_ILS_Detailed")
                For Each bm As clsSSTBeam In SelectedBeams
                    If bm._uName = dgvStep1.CurrentCell.Value Then
                        bm.ILS_Detail(gJob)
                        '
                        For Each col As clsSSTColumn In SelectedColumns
                            If col._uName = bm.EndI_ColBottom._uName Then
                                col.CC_Detail_US(gJob, True, 1)
                                '
                                Exit For
                            End If
                        Next
                        For Each col As clsSSTColumn In SelectedColumns
                            If col._uName = bm.EndJ_ColBottom._uName Then
                                col.CC_Detail_US(gJob, True, 2)
                                '
                                Exit For
                            End If
                        Next
                        Dim pdf As New clsReport(gJob, strF, ARType.T1_ILS)
                        retStatus = pdf.Export()

                        Exit For
                    End If
                Next
            ElseIf tabMain.SelectedIndex = 1 Then
                strF = GenerateNewName(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Bm" & dgvStep2.CurrentCell.Value & "_BLC_Detailed")
                For Each bm As clsSSTBeam In SelectedBeams
                    If bm._uName = dgvStep2.CurrentCell.Value Then
                        bm.BLC_Detail(gJob)
                        '
                        Dim pdf As New clsReport(gJob, strF, ARType.T2_BLC)
                        retStatus = pdf.Export()
                        Exit For
                    End If
                Next
            ElseIf tabMain.SelectedIndex = 2 Then
                strF = GenerateNewName(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Col" & dgvStep3.CurrentCell.Value & "_CC_Detailed")
                For Each col As clsSSTColumn In SelectedColumns
                    If col._uName = dgvStep3.CurrentCell.Value Then
                        col.CC_Detail_US(gJob)
                        Dim pdf As New clsReport(gJob, strF, ARType.T3_CC)
                        retStatus = pdf.Export()
                        Exit For
                    End If
                Next
            ElseIf tabMain.SelectedIndex = 3 Then
                strF = GenerateNewName(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Bm" & dgvStep4.CurrentCell.Value & "_SPC_Detailed")
                For Each bm As clsSSTBeam In SelectedBeams
                    If bm._uName = dgvStep4.CurrentCell.Value Then
                        bm.SPC_Detail(gJob)
                        '
                        Dim pdf As New clsReport(gJob, strF, ARType.T4_SPC)
                        retStatus = pdf.Export()
                        Exit For
                    End If
                Next
            Else

            End If
            '
            If System.IO.File.Exists(strF) Then
                Process.Start(strF)
            End If
            '
            lbStatus.Text = retStatus
        Catch ex As Exception
            MsgBox(ex.Message,, "Yield-Link� Connection")
            'MsgBox(ex.Message & ex.StackTrace,, "Yield-Link� Connection")
        Finally
            ' Replace the cursor
            Cursor = Cursors.Arrow
            Application.DoEvents()
        End Try
    End Sub

    Private Sub btnDetailedReport_Click(sender As Object, e As EventArgs) Handles btnDetailedReport.Click
        ViewDetailedReport()
    End Sub

    Private Sub btnJobInfo_Click(sender As Object, e As EventArgs) Handles btnJobInfo.Click
        Dim frm As New frmDesignInfo(gJob, frmType.JobInfo)
        frm.ShowDialog()
        If Not frm.curJob Is Nothing Then
            gJob.JobID = frm.curJob.JobID
            gJob.Name = frm.curJob.Name
            gJob.Address = frm.curJob.Address
            gJob.CityState = frm.curJob.CityState
            gJob.EOR = frm.curJob.EOR
            gJob.EngName = frm.curJob.EngName
            gJob.EngEmail = frm.curJob.EngEmail
        End If
    End Sub

    Private Sub btnAllowDriftLimit_Click(sender As Object, e As EventArgs) Handles btnDCRLimit2.Click
        Dim frm As New frmDesignInfo(gJob, frmType.DCRLimit)
        frm.ShowDialog()
        If Not frm.curJob Is Nothing Then
            gJob.DCRLimits.DriftLimitSelect = frm.curJob.DCRLimits.DriftLimitSelect
            gJob.DCRLimits.WindDriftLimitSelect = frm.curJob.DCRLimits.WindDriftLimitSelect
            gJob.DCRLimits.DivideByRho = frm.curJob.DCRLimits.DivideByRho
            '
            gJob.DCRLimits.tbf_check = frm.curJob.DCRLimits.tbf_check
            gJob.DCRLimits.bf_check = frm.curJob.DCRLimits.bf_check
            gJob.DCRLimits.Lyield_check = frm.curJob.DCRLimits.Lyield_check
            gJob.DCRLimits.Panel_Zone_DCR = frm.curJob.DCRLimits.Panel_Zone_DCR
            gJob.DCRLimits.Drift_check = frm.curJob.DCRLimits.Drift_check
            gJob.DCRLimits.Beam_tf_DCR = frm.curJob.DCRLimits.Beam_tf_DCR
            gJob.DCRLimits.Link_strength_DCR = frm.curJob.DCRLimits.Link_strength_DCR
            gJob.DCRLimits.t_BRP_DCR = frm.curJob.DCRLimits.t_BRP_DCR
            gJob.DCRLimits.BRP_Bolt_DCR = frm.curJob.DCRLimits.BRP_Bolt_DCR
            gJob.DCRLimits.SCWB_DCR = frm.curJob.DCRLimits.SCWB_DCR
            gJob.DCRLimits.Column_Flange_DCR = frm.curJob.DCRLimits.Column_Flange_DCR
            gJob.DCRLimits.Stiffener_DCR = frm.curJob.DCRLimits.Stiffener_DCR
            gJob.DCRLimits.Beam_Web_DCR = frm.curJob.DCRLimits.Beam_Web_DCR
            gJob.DCRLimits.Shear_Plate_DCR = frm.curJob.DCRLimits.Shear_Plate_DCR
            gJob.DCRLimits.Bolt_DCR = frm.curJob.DCRLimits.Bolt_DCR
            gJob.DCRLimits.Fillet_weld_DCR = frm.curJob.DCRLimits.Fillet_weld_DCR
            '
            isKeepRows = True
            isPrevent = True
            Update_Step1(False)
            isPrevent = False
            '
            ''tabMain.TabPages(0).Select()
        End If
    End Sub



    '\\ Private Sub Save_Load_gJob(isSave As Boolean)
    '\\   If isSave Then
    '\\       Serialize_gJob_ToXML(IO.Path.Combine(gTemp, preJob), gJob)
    '\\   Else
    '\\      gJob = DeSerialize_XML_gJob(IO.Path.Combine(gTemp, preJob))
    '\\      If gJob Is Nothing Then
    '\\           gJob = New clsMRSJob(toolVer)
    '\\ End If
    '\\ End If
    '\\ End Sub
#Region "DXF"
    Private Sub btnExportDXF_Click(sender As Object, e As EventArgs) Handles btnExportDXF2.Click, btnExportDXF.Click
        Dim etabPath As String
        If isDebug Then
            gJob.SlabDepths = New List(Of sstSlabDepth)
            gJob.SlabDepths.Add(New sstSlabDepth("1", 120, 6.25))
            gJob.SlabDepths.Add(New sstSlabDepth("2", 120, 6.25))
            gJob.SlabDepths.Add(New sstSlabDepth("3", 120, 6.25))
            gJob.SlabDepths.Add(New sstSlabDepth("4", 120, 6.25))
            gJob.SlabDepths.Add(New sstSlabDepth("5", 120, 6.25))
            '
            etabPath = Application.StartupPath
        Else
            If gJob.SlabDepths.Count <> clsSE.Stories.Count Then
                SetDefault_SlabDepths()
            End If
            '
            etabPath = clsSE.Get_ETABS_Folderpath()
        End If
        'Update Weld Summary
        Dim lstWels As New List(Of String)
        Update_Step8(lstWels)
        'Get saved location of this ETABSS model

        Dim clsDXF As New clsExportDXF(gJob, etabPath, SelectedBeams, SelectedColumns, lstWels)
        clsDXF.Export_Elevation_To_DXF()
        '
        lbStatus.Text = clsDXF.strStatus
        If IO.File.Exists(clsDXF.strStatus) Then
            Process.Start(IO.Path.GetDirectoryName(clsDXF.strStatus))
        End If
        clsDXF = Nothing
    End Sub

#End Region
    Private Sub btnChangeSlab_Click(sender As Object, e As EventArgs) Handles btnSlabDepths2.Click
        Try
            If gJob.SlabDepths.Count <> clsSE.Stories.Count Then
                SetDefault_SlabDepths()
            End If
            Dim frm As New frmDesignInfo(gJob, frmType.SlabDepths)
            frm.ShowDialog()
            If Not frm.curJob Is Nothing Then
                gJob.SlabDepths = frm.curJob.SlabDepths
                gJob.isFixedBase = frm.curJob.isFixedBase
            End If
        Catch ex As Exception
            lbStatus.Text = "Invalid selection. Please select Moment frame in the ETABS/SAP UI first"
        End Try

    End Sub

    Private Sub SetDefault_SlabDepths()
        'Init slab
        gJob.SlabDepths.Clear()
        For i As Integer = 0 To clsSE.Stories.Count - 1
            gJob.SlabDepths.Add(New sstSlabDepth(clsSE.Stories.Values(i), clsSE.Stories.Keys(i), (6.25)))
        Next
        '
        gJob.SlabDepths = gJob.SlabDepths.OrderByDescending(Function(x) x.StoryElevation).ToList
        gJob.SlabDepths(gJob.SlabDepths.Count - 1).SlabDepth = (18)
    End Sub
    Private Sub BtnDesign_Click(sender As Object, e As EventArgs) Handles BtnDesign.Click

        Try
            If Not clsSE.isModelLocked() Then
                MsgBox("Please run Analysis before Member design", MsgBoxStyle.Exclamation, toolName)
                Return
            End If
            '
            If Not clsSE.IsExistLcm35 Then
                'Design all, Column has same combos as Beam
                clsSE.RunSteelDesign(gJob, True)
                Update_Step6()
                '
                Update_Step7()
            Else
                'Design column
                clsSE.RunSteelDesign(gJob, False)
                Update_Step7()
                'Design beam
                clsSE.RunSteelDesign(gJob, True)
                Update_Step6()
            End If
            '
            tabMain.SelectedIndex = 5
            lbStatus.Text = "Design Results have been updated!"
        Catch ex As Exception
        Finally
            Me.Enabled = True
            BringWindowToTop(Me.Handle)
        End Try
    End Sub

    Private Sub YieldLinkConnectionPlugin_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            btnClose_Click(sender, e)
        End If
    End Sub

    Private Sub dgv_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs) Handles dgvStep7.DataError, dgvStep6.DataError, dgvStep5.DataError, dgvStep4.DataError, dgvStep3.DataError, dgvStep2.DataError, dgvStep1.DataError
        If e.Exception.Message = "DataGridViewComboBoxCell value is not valid." Then
            Dim value As Object = sender.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            '
            If Not (CType(sender.Columns(e.ColumnIndex), DataGridViewComboBoxColumn)).Items.Contains(value) Then
                '(CType(sender.Columns(e.ColumnIndex), DataGridViewComboBoxColumn)).Items.Add(value)
                e.ThrowException = False
            End If
        End If
    End Sub

    Private Sub btnWeldPref_Click(sender As Object, e As EventArgs) Handles btnWeldPref.Click
        Dim frm As New frmWeldPref(gJob)
        frm.ShowDialog()
        If Not frm.curJob Is Nothing Then
            gJob.WeldingInfo.DoublerPL2ColFLG = frm.curJob.WeldingInfo.DoublerPL2ColFLG
            gJob.WeldingInfo.DoublerPL2ColWeb = frm.curJob.WeldingInfo.DoublerPL2ColWeb
            gJob.WeldingInfo.UsePlug_weld = frm.curJob.WeldingInfo.UsePlug_weld
        End If
    End Sub

    Private Sub btnChangematerial_Click(sender As Object, e As EventArgs) Handles btnChangematerial.Click
        Dim frm As New frmDesignInfo(gJob, frmType.Material)
        frm.ShowDialog()
        If Not frm.curJob Is Nothing Then
            gJob.MaterialInfo.Bm_Fu = frm.curJob.MaterialInfo.Bm_Fu
            gJob.MaterialInfo.Bm_Fy = frm.curJob.MaterialInfo.Bm_Fy
            '
            gJob.MaterialInfo.Col_Fu = frm.curJob.MaterialInfo.Col_Fu
            gJob.MaterialInfo.Col_Fy = frm.curJob.MaterialInfo.Col_Fy
            '
            gJob.MaterialInfo.ShearPlate_Fu = frm.curJob.MaterialInfo.ShearPlate_Fu
            gJob.MaterialInfo.ShearPlate_Fy = frm.curJob.MaterialInfo.ShearPlate_Fy
            '
            gJob.MaterialInfo.StiffPL_Fu = frm.curJob.MaterialInfo.StiffPL_Fu
            gJob.MaterialInfo.StiffPL_Fy = frm.curJob.MaterialInfo.StiffPL_Fy
            '
            gJob.MaterialInfo.DoublerPL_Fu = frm.curJob.MaterialInfo.DoublerPL_Fu
            gJob.MaterialInfo.DoublerPL_Fy = frm.curJob.MaterialInfo.DoublerPL_Fy
            '
            If tabMain.SelectedIndex = 2 Then
                Update_Step3(False)
            ElseIf tabMain.SelectedIndex = 3 Then
                Update_Step4(False)
            End If
        End If
    End Sub

    Private Sub btnLegend_Click(sender As Object, e As EventArgs) Handles btnLegend.Click
        Dim frm As New frmAbout(False)
        frm.ShowDialog()
    End Sub
    Private Sub btnModifyE2k_Click(sender As Object, e As EventArgs) Handles btnModifyE2k.Click
        Try
            'Seismic parameter
            Dim SS As Double, S1 As Double, siteclass As Char, TL As Double, Designcode, frTyp As String
            SS = gJob.Ss
            S1 = gJob.S1
            siteclass = gJob.SiteClass
            TL = gJob.TL
            Designcode = gJob.ASCE
            frTyp = gJob.FrType
            'Wind parameter
            '
            Dim openfileDialog1 As New OpenFileDialog()
            With openfileDialog1
                .FileName = "Select a e2k file"
                .Filter = "Text files (*.e2k)|*.e2k"
                .Title = toolName & ": Open file"
            End With

            If openfileDialog1.ShowDialog <> DialogResult.OK Then Return
            '
            Dim IsMyKey As Boolean
            Dim filePath As String = openfileDialog1.FileName
            Dim lstText As New List(Of String)
            Dim myListPatts As New List(Of String)
            myListPatts.Add("LOADPATTERN ""DL""")
            myListPatts.Add("LOADPATTERN ""LL""")
            myListPatts.Add("LOADPATTERN ""LR""")
            myListPatts.Add("LOADPATTERN ""SL""")
            myListPatts.Add("LOADPATTERN ""RL""")
            myListPatts.Add("LOADPATTERN ""W""")
            myListPatts.Add("LOADPATTERN ""NLX""")
            myListPatts.Add("LOADPATTERN ""NLY""")
            myListPatts.Add("LOADPATTERN ""EQX""")
            myListPatts.Add("LOADPATTERN ""EQXPY""")
            myListPatts.Add("LOADPATTERN ""EQXNY""")
            myListPatts.Add("LOADPATTERN ""EQY""")
            myListPatts.Add("LOADPATTERN ""EQYPX""")
            myListPatts.Add("LOADPATTERN ""EQYNX""")
            myListPatts.Add("LOADPATTERN ""EQX_D""")
            myListPatts.Add("LOADPATTERN ""EQXPY_D""")
            myListPatts.Add("LOADPATTERN ""EQXNY_D""")
            myListPatts.Add("LOADPATTERN ""EQY_D""")
            myListPatts.Add("LOADPATTERN ""EQYPX_D""")
            myListPatts.Add("LOADPATTERN ""EQYNX_D""")
            'While objWriter.
            Dim str As String = ""
            Dim reader As StreamReader
            reader = My.Computer.FileSystem.OpenTextFileReader(filePath)
            '
            Dim Stories As New List(Of String)
            Dim tmpArr() As String
            Dim suffEQ_X, suffEQ_D_X, suffEQ_Y, suffEQ_D_Y As String
            Dim SuffixStringWind As String
            '
            Do
                str = reader.ReadLine
                '
                If str.StartsWith("$ STORIES") Then
                    lstText.Add(str)
                    '
                    Do
                        str = reader.ReadLine
                        tmpArr = str.Split(Chr(34))
                        If tmpArr.Length > 1 Then
                            Stories.Add(tmpArr(1))
                        End If
                        '
                        lstText.Add(str)
                    Loop Until str.StartsWith("$")
                    'Seismic X
                    suffEQ_X = "    TOPSTORY " & Chr(34) &
                        Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                        "APPROXIMATE" & Chr(34) & "   CTTYPE 0  R " & gJob.R & "  OMEGA " & gJob.Omega & "  CD " & gJob.Cd & "  I " & gJob.I & "  SITECLASS " &
                        Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Seismic Y
                    suffEQ_Y = "    TOPSTORY " & Chr(34) &
                        Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                        "APPROXIMATE" & Chr(34) & "   CTTYPE 0  R " & gJob.RY & "  OMEGA " & gJob.OmegaY & "  CD " & gJob.CdY & "  I " & gJob.IY & "  SITECLASS " &
                        Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Seismic Drift X
                    suffEQ_D_X = "    TOPSTORY " & Chr(34) &
                    Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                    "PROGCALC" & Chr(34) & "   CTTYPE 0  R " & gJob.R & "  OMEGA " & gJob.Omega & "  CD " & gJob.Cd & "  I " & gJob.I & "  SITECLASS " &
                    Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Seismic Drift Y
                    suffEQ_D_Y = "    TOPSTORY " & Chr(34) &
                    Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                    "PROGCALC" & Chr(34) & "   CTTYPE 0  R " & gJob.RY & "  OMEGA " & gJob.OmegaY & "  CD " & gJob.CdY & "  I " & gJob.IY & "  SITECLASS " &
                    Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Wind X
                    SuffixStringWind = Chr(34) & Designcode & Chr(34) & "  EXPOSUREFROM " & Chr(34) & "DIAPHRAGMS" & Chr(34) & "    USERCP " & Chr(34) & "No" & Chr(34) & "  TOPSTORY " & Chr(34) &
                Stories(0) & Chr(34) & "  BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "  SPEED " & gJob.WindSpeed & "  EXPOSURE " & Chr(34) & gJob.ExposureType & Chr(34) &
                "   KE " & gJob.F_groundEle & "  KZT " & gJob.Kzt & "  GUSTFACTOR " & gJob.F_gust & "  KD " & gJob.Kd & "  Case ALL  E1 0.15  E2 0.15 WINDEXPOSURE " & Chr(34)
                    Continue Do
                End If
                '
                'Overwrite load patterns
                If str.StartsWith("$ LOAD PATTERNS") Then
                    lstText.Add(str)
                    Do
                        str = reader.ReadLine
                        'ignore old load pattern
                        IsMyKey = False
                        For Each PattStr As String In myListPatts
                            If str.Contains(PattStr) Then
                                IsMyKey = True
                                Exit For
                            End If
                        Next
                        'Write all old pattern
                        If IsMyKey = False And str.StartsWith("  LOADPATTERN") Then lstText.Add(str)
                    Loop Until str.StartsWith("$")
                    'Write my pattern
                    lstText.Add("  LOADPATTERN ""DL""  TYPE  ""Super Dead""  SELFWEIGHT  1")
                    lstText.Add("  LOADPATTERN ""EQX""  TYPE  ""Seismic""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQX_D""  TYPE  ""Seismic (Drift)""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQXNY""  TYPE  ""Seismic""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQXNY_D""  TYPE  ""Seismic (Drift)""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQXPY""  TYPE  ""Seismic""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQXPY_D""  TYPE  ""Seismic (Drift)""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQY""  TYPE  ""Seismic""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQY_D""  TYPE  ""Seismic (Drift)""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQYNX""  TYPE  ""Seismic""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQYNX_D""  TYPE  ""Seismic (Drift)""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQYPX""  TYPE  ""Seismic""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""EQYPX_D""  TYPE  ""Seismic (Drift)""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""LL""  TYPE  ""Live""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""LR""  TYPE  ""Roof Live""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""NLX""  TYPE  ""Notional""  SELFWEIGHT  0  NOTIONALLOAD ""DL""  NOTIONALFACTOR 0.002  NOTIONALDIR ""X""  ")
                    lstText.Add("  LOADPATTERN ""NLY""  TYPE  ""Notional""  SELFWEIGHT  0  NOTIONALLOAD ""DL""  NOTIONALFACTOR 0.002  NOTIONALDIR ""Y""  ")
                    lstText.Add("  LOADPATTERN ""RL""  TYPE  ""Other""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""SL""  TYPE  ""Snow""  SELFWEIGHT  0")
                    lstText.Add("  LOADPATTERN ""W""  TYPE  ""Wind""  SELFWEIGHT  0")
                    'lstText.Add("  LOADPATTERN ""WY""  TYPE  ""Wind""  SELFWEIGHT  0")
                    lstText.Add("  SEISMIC ""EQX""  """ & Designcode & """    DIR ""X""" & suffEQ_X)
                    lstText.Add("  SEISMIC ""EQXNY""  """ & Designcode & """    DIR ""X-ECC""  ECC 0.05" & suffEQ_X)
                    lstText.Add("  SEISMIC ""EQXPY""  """ & Designcode & """    DIR ""X+ECC""  ECC 0.05" & suffEQ_X)
                    lstText.Add("  SEISMIC ""EQY""  """ & Designcode & """    DIR ""Y""" & suffEQ_Y)
                    lstText.Add("  SEISMIC ""EQYNX""  """ & Designcode & """    DIR ""Y-ECC""  ECC 0.05" & suffEQ_Y)
                    lstText.Add("  SEISMIC ""EQYPX""  """ & Designcode & """    DIR ""Y+ECC""  ECC 0.05" & suffEQ_Y)
                    lstText.Add("  SEISMIC ""EQX_D""  """ & Designcode & """    DIR ""X""" & suffEQ_D_X)
                    lstText.Add("  SEISMIC ""EQXNY_D""  """ & Designcode & """    DIR ""X-ECC""  ECC 0.05" & suffEQ_D_X)
                    lstText.Add("  SEISMIC ""EQXPY_D""  """ & Designcode & """    DIR ""X+ECC""  ECC 0.05" & suffEQ_D_X)
                    lstText.Add("  SEISMIC ""EQY_D""  """ & Designcode & """    DIR ""Y""" & suffEQ_D_Y)
                    lstText.Add("  SEISMIC ""EQYNX_D""  """ & Designcode & """    DIR ""Y-ECC""  ECC 0.05" & suffEQ_D_Y)
                    lstText.Add("  SEISMIC ""EQYPX_D""  """ & Designcode & """    DIR ""Y+ECC""  ECC 0.05" & suffEQ_D_Y)
                    lstText.Add("  WIND ""W"" " & SuffixStringWind & "W""   ""SET""  ""1""  ANGLE 0 ")
                    'lstText.Add("  WIND ""WY"" " & SuffixStringWind & "WY""   ""SET""  ""1""  ANGLE 0 ")
                End If
                'Modify Load Case
                If str.StartsWith("$ LOAD CASES") Then
                    lstText.Add(str)
                    IsMyKey = False
                    Do
                        str = reader.ReadLine
                        '
                        If str.StartsWith("  LOADCASE ""PDELTA""  TYPE") Then
                            lstText.Add(str)
                            IsMyKey = True
                        ElseIf str.StartsWith("  LOADCASE ""PDELTA""  NLGEOMTYPE") Then
                            lstText.Add("  LOADCASE ""PDELTA""  NLGEOMTYPE  ""PDelta""")
                            IsMyKey = False
                        ElseIf Not str.StartsWith("$") Then
                            lstText.Add(str)
                        End If
                    Loop Until str.StartsWith("$")
                    '
                    If IsMyKey Then
                        lstText.Insert(lstText.Count - 4, "  LOADCASE ""PDELTA""  NLGEOMTYPE  ""PDelta""")
                    End If
                End If

                'Overwrite Seismic function
                If str.StartsWith("$ FUNCTIONS") Then
                    lstText.Add(str)
                    Do
                        str = reader.ReadLine
                        'ignore old Function
                        IsMyKey = False
                        If str.Contains("  FUNCTION ""SPEC""") Then
                            IsMyKey = True
                        End If
                        'Write all old Function
                        If IsMyKey = False And str.StartsWith("  FUNCTION") Then lstText.Add(str)
                    Loop Until str.StartsWith("$")
                    'Write my function
                    If gJob.ASCE Like "*16" Then
                        lstText.Add("  FUNCTION ""SPEC""  FUNCTYPE ""SPECTRUM""  DAMPRATIO 0.05  SPECTYPE ""ASCE716""    SS " & gJob.Ss & "  S1 " & gJob.S1 & "  TL " & gJob.TL & "  SITECLASS " & gJob.SiteClass)
                    ElseIf gJob.ASCE Like "*05" Then
                        lstText.Add("  FUNCTION ""SPEC""  FUNCTYPE ""SPECTRUM""  DAMPRATIO 0.05  SPECTYPE ""IBC2006""    OPTION USER  SS " & gJob.Ss & "  S1 " & gJob.S1 & "  TL " & gJob.TL & "  SITECLASS " & gJob.SiteClass)
                    Else '7-10 default
                        lstText.Add("  FUNCTION ""SPEC""  FUNCTYPE ""SPECTRUM""  DAMPRATIO 0.05  SPECTYPE ""ASCE710""    OPTION USER  SS " & gJob.Ss & "  S1 " & gJob.S1 & "  TL " & gJob.TL & "  SITECLASS " & gJob.SiteClass)
                    End If
                End If
                'Overwrite Steel preferences function
                If str.StartsWith("$ STEEL") Then
                    lstText.Add(str)
                    Do
                        str = reader.ReadLine
                        '
                        If str.StartsWith("  STEELPREFERENCE  CODE """) Then
                            Dim Arr() As String = str.Split("""")
                            lstText.Add("  STEELPREFERENCE  CODE """ & gJob.DesignCode & """  THDESIGN """ & Arr(3) & """  FRAMETYPE """ & frTyp & """")
                        ElseIf str.StartsWith("  STEELPREFERENCE  SDC """) Then
                            lstText.Add("  STEELPREFERENCE  SDC """ & gJob.SiteClass & """  IMPORTANCEFACTOR " & gJob.I & " SYSTEMRHO " & gJob.Rho & " SYSTEMSDS " & gJob.Sds)
                        ElseIf str.StartsWith("  STEELPREFERENCE  SYSTEMR") Then
                            lstText.Add("  STEELPREFERENCE  SYSTEMR " & gJob.R & " OMEGA0 " & gJob.Omega & " SYSTEMCD " & gJob.Cd)
                        ElseIf str.StartsWith("  STEELPREFERENCE  PROVISION") Then
                            lstText.Add("  STEELPREFERENCE  PROVISION ""LRFD""  ANALYSISMETHOD ""DIRECT ANALYSIS""  SECONDORDERMETHOD ""GENERAL 2ND ORDER""  STIFFNESSREDUCTIONMETHOD ""TAU-B FIXED""  AANOTIONALTOLATERALCOMBOS ""NO""  BETA 1.3 BETAOMEGA 1.6 ")
                        ElseIf Not str.StartsWith("$") Then
                            lstText.Add(str)
                        End If
                    Loop Until str.StartsWith("$")
                End If
                'add others
                lstText.Add(str)
            Loop Until str.Equals("$ END OF MODEL FILE")
            lstText.Add(str)
            reader.Close()
            '
            Dim wr As New StreamWriter(filePath, False)
            'write rows to excel file
            For i As Integer = 0 To (lstText.Count) - 1
                wr.WriteLine(lstText(i))
            Next
            'close file
            wr.Close()
            '
            lbStatus.Text = "E2K file has been updated!"
        Catch ex As Exception
            MsgBox(ex.Message & ex.StackTrace, MsgBoxStyle.Critical, toolName)
        End Try
    End Sub
    Private Sub btnModifyS2k_Click(sender As Object, e As EventArgs) Handles btnModifyS2k.Click
        Try
            'Seismic parameter
            Dim SS As Double, S1 As Double, siteclass As Char, TL As Double, Designcode, frTyp As String
            SS = gJob.Ss
            S1 = gJob.S1
            siteclass = gJob.SiteClass
            TL = gJob.TL
            Designcode = gJob.ASCE
            frTyp = gJob.FrType
            'Wind parameter
            '
            Dim openfileDialog1 As New OpenFileDialog()
            With openfileDialog1
                .FileName = "Select a s2k file"
                .Filter = "Text files (*.s2k)|*.s2k"
                .Title = toolName & ": Open file"
            End With

            If openfileDialog1.ShowDialog <> DialogResult.OK Then Return
            '
            Dim IsMyKey As Boolean
            Dim filePath As String = openfileDialog1.FileName
            Dim lstText As New List(Of String)
            Dim myListPatts As New List(Of String)
            myListPatts.Add("LoadPat=DL")
            myListPatts.Add("LoadPat=LL")
            myListPatts.Add("LoadPat=LR")
            myListPatts.Add("LoadPat=SL")
            myListPatts.Add("LoadPat=RL")
            myListPatts.Add("LoadPat=W-1")
            myListPatts.Add("LoadPat=W-2")
            myListPatts.Add("LoadPat=W-3")
            myListPatts.Add("LoadPat=W-4")
            myListPatts.Add("LoadPat=W-5")
            myListPatts.Add("LoadPat=W-6")
            myListPatts.Add("LoadPat=W-7")
            myListPatts.Add("LoadPat=W-8")
            myListPatts.Add("LoadPat=W-9")
            myListPatts.Add("LoadPat=W-10")
            myListPatts.Add("LoadPat=W-11")
            myListPatts.Add("LoadPat=W-12")
            myListPatts.Add("LoadPat=NLX")
            myListPatts.Add("LoadPat=NLY")
            myListPatts.Add("LoadPat=EQX")
            myListPatts.Add("LoadPat=EQX_D")
            myListPatts.Add("LoadPat=EQXNY")
            myListPatts.Add("LoadPat=EQXNY_D")
            myListPatts.Add("LoadPat=EQXPY")
            myListPatts.Add("LoadPat=EQXPY_D")
            myListPatts.Add("LoadPat=EQY")
            myListPatts.Add("LoadPat=EQY_D")
            myListPatts.Add("LoadPat=EQYNX")
            myListPatts.Add("LoadPat=EQYNX_D")
            myListPatts.Add("LoadPat=EQYPX")
            myListPatts.Add("LoadPat=EQYPX_D")

            'Dim myListCombs As New List(Of String)
            'myListCombs.Add("ComboName=EL")
            'myListCombs.Add("ComboName=EL_Omega")
            'myListCombs.Add("ComboName=SST_LC8")
            'myListCombs.Add("ComboName=SST_LC25")
            'myListCombs.Add("ComboName=SST_LC26")
            'myListCombs.Add("ComboName=SST_LC27")
            'myListCombs.Add("ComboName=SST_LC28")
            'myListCombs.Add("ComboName=SST_LC29")
            'myListCombs.Add("ComboName=SST_LC30")
            'myListCombs.Add("ComboName=SST_LC31")
            'myListCombs.Add("ComboName=SST_LC32")
            'myListCombs.Add("ComboName=SST_LC33")
            'myListCombs.Add("ComboName=SST_LC34")
            'myListCombs.Add("ComboName=SST_LC35")
            'myListCombs.Add("ComboName=SST_LC36")
            'myListCombs.Add("ComboName=SST_LC37")
            'myListCombs.Add("ComboName=SST_LC38")
            'myListCombs.Add("ComboName=SST_LC39")
            'myListCombs.Add("ComboName=SST_LC40")

            'While objWriter.
            Dim str As String = ""
            Dim reader As StreamReader
            reader = My.Computer.FileSystem.OpenTextFileReader(filePath)
            '
            Dim Stories As New List(Of String)
            Dim tmpArr() As String
            Dim suffEQ_X, suffEQ_D_X, suffEQ_Y, suffEQ_D_Y As String
            Dim SuffixStringWind As String
            '
            Do
                str = reader.ReadLine
                '
                If str.StartsWith("$ STORIES") Then
                    lstText.Add(str)
                    '
                    Do
                        str = reader.ReadLine
                        tmpArr = str.Split(Chr(34))
                        If tmpArr.Length > 1 Then
                            Stories.Add(tmpArr(1))
                        End If
                        '
                        lstText.Add(str)
                    Loop Until str.StartsWith("$")
                    'Seismic X
                    suffEQ_X = "    TOPSTORY " & Chr(34) &
                        Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                        "APPROXIMATE" & Chr(34) & "   CTTYPE 0  R " & gJob.R & "  OMEGA " & gJob.Omega & "  CD " & gJob.Cd & "  I " & gJob.I & "  SITECLASS " &
                        Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Seismic Y
                    suffEQ_Y = "    TOPSTORY " & Chr(34) &
                        Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                        "APPROXIMATE" & Chr(34) & "   CTTYPE 0  R " & gJob.RY & "  OMEGA " & gJob.OmegaY & "  CD " & gJob.CdY & "  I " & gJob.IY & "  SITECLASS " &
                        Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Seismic Drift X
                    suffEQ_D_X = "    TOPSTORY " & Chr(34) &
                    Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                    "PROGCALC" & Chr(34) & "   CTTYPE 0  R " & gJob.R & "  OMEGA " & gJob.Omega & "  CD " & gJob.Cd & "  I " & gJob.I & "  SITECLASS " &
                    Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Seismic Drift Y
                    suffEQ_D_Y = "    TOPSTORY " & Chr(34) &
                    Stories(0) & Chr(34) & "    BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "   PERIODTYPE " & Chr(34) &
                    "PROGCALC" & Chr(34) & "   CTTYPE 0  R " & gJob.RY & "  OMEGA " & gJob.OmegaY & "  CD " & gJob.CdY & "  I " & gJob.IY & "  SITECLASS " &
                    Chr(34) & siteclass & Chr(34) & "    Ss " & SS & "  S1 " & S1 & "  TL " & TL
                    'Wind X
                    SuffixStringWind = Chr(34) & Designcode & Chr(34) & "  EXPOSUREFROM " & Chr(34) & "DIAPHRAGMS" & Chr(34) & "    USERCP " & Chr(34) & "No" & Chr(34) & "  TOPSTORY " & Chr(34) &
                Stories(0) & Chr(34) & "  BOTTOMSTORY " & Chr(34) & Stories(Stories.Count - 1) & Chr(34) & "  SPEED " & gJob.WindSpeed & "  EXPOSURE " & Chr(34) & gJob.ExposureType & Chr(34) &
                "   KE " & gJob.F_groundEle & "  KZT " & gJob.Kzt & "  GUSTFACTOR " & gJob.F_gust & "  KD " & gJob.Kd & "  Case ALL  E1 0.15  E2 0.15 WINDEXPOSURE " & Chr(34)
                    Continue Do
                End If
                '
                'Overwrite load patterns
                If str.StartsWith("TABLE:  ""LOAD PATTERN DEFINITIONS""") Then
                    lstText.Add(str)
                    Do
                        str = reader.ReadLine
                        'ignore old load pattern
                        IsMyKey = False
                        For Each PattStr As String In myListPatts
                            If str.Contains(PattStr) Then
                                IsMyKey = True
                                Exit For
                            End If
                        Next
                        'Write all old pattern
                        If IsMyKey = False And str.StartsWith("   LoadPat=") Then lstText.Add(str)
                    Loop Until str.StartsWith("TABLE:")
                    'Write my pattern
                    lstText.Add("   LoadPat=DL   DesignType=Dead   SelfWtMult=1   GUID=9294aaec-872f-4ef2-8bd6-03852cdab162")
                    lstText.Add("   LoadPat=EQX   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=d0ee8d82-207d-4080-9587-c7230c72d445")
                    lstText.Add("   LoadPat=EQX_D   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=f98d1298-bad6-4fb4-8180-8491ebee2824")
                    lstText.Add("   LoadPat=EQXNY   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=ecf2ea06-eedf-4347-bfd7-7f47f039c0c4")
                    lstText.Add("   LoadPat=EQXNY_D   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=b137ac06-06f8-48b6-9296-ecb35e92fd36")
                    lstText.Add("   LoadPat=EQXPY   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=49bb9fea-71f0-4329-87a0-182b689c6b54")
                    lstText.Add("   LoadPat=EQXPY_D   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=8259618d-814f-4d63-966e-f45cd93717d0")
                    lstText.Add("   LoadPat=EQY   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=ef6aeb3d-9292-48f2-b07e-2eebc343f858")
                    lstText.Add("   LoadPat=EQY_D   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=146ece15-296d-455c-b619-3889f926a557")
                    lstText.Add("   LoadPat=EQYNX   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=53948db5-3b29-4551-ae77-3a798f643868")
                    lstText.Add("   LoadPat=EQYNX_D   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=08e3cb2e-8499-43e4-b878-ca97a50df8d9")
                    lstText.Add("   LoadPat=EQYPX   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=b2fddd13-c161-44b1-9c2a-c87064445fe7")
                    lstText.Add("   LoadPat=EQYPX_D   DesignType=Quake   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=d64f532f-6a49-4a52-8a4e-6710aa75b49b")
                    lstText.Add("   LoadPat=LL   DesignType=Live   SelfWtMult=0   GUID=be99e693-8d58-4310-a833-8455335bddbc")
                    lstText.Add("   LoadPat=LR   DesignType=""Roof Live""   SelfWtMult=0   GUID=d15be15a-ba34-4d83-805a-4d58083be925")
                    lstText.Add("   LoadPat=NLX   DesignType=Notional   SelfWtMult=0   NotBasePat=DL   NotRatio=0.003   NotDir=""Global X""   GUID=fdc6526b-5c6c-4335-b9cd-566b3509a9bb")
                    lstText.Add("   LoadPat=NLY   DesignType=Notional   SelfWtMult=0   NotBasePat=DL   NotRatio=0.003   NotDir=""Global Y""   GUID=bfb2c065-aa6f-4c0f-ab0e-d525f494c18e")
                    lstText.Add("   LoadPat=RL   DesignType=Other   SelfWtMult=0   GUID=ce724fd2-73d4-4e71-bb0e-8e0691db1239")
                    lstText.Add("   LoadPat=SL   DesignType=Snow   SelfWtMult=0   GUID=a495cfb4-af1d-4051-bd35-c301de07d761")
                    lstText.Add("   LoadPat=W-1   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=ed521cf7-4a46-4aa7-88c2-932a548b22af")
                    lstText.Add("   LoadPat=W-2   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=366cd8a4-0802-4ffe-bf42-0af11b07f48d")
                    lstText.Add("   LoadPat=W-3   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=7dd15898-b1fe-4800-bba4-957481ff96b9")
                    lstText.Add("   LoadPat=W-4   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=f04ed9f9-b309-4877-8760-0c6b0b203680")
                    lstText.Add("   LoadPat=W-5   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=3b0610d7-d590-4e18-b73d-4e1687c0273d")
                    lstText.Add("   LoadPat=W-6   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=02569f1e-638c-46b7-8e8f-7e8657a76c02")
                    lstText.Add("   LoadPat=W-7   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=2fc59349-5bc0-4302-9e6a-ad85034b8383")
                    lstText.Add("   LoadPat=W-8   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=1a0a7392-c4b4-43ec-a422-88f117c3c1af")
                    lstText.Add("   LoadPat=W-9   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=8faa9830-cd48-4bee-8805-5606267bf734")
                    lstText.Add("   LoadPat=W-10   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=60206ac5-cb41-4fd0-9232-4cefbb22003d")
                    lstText.Add("   LoadPat=W-11   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=3aae04fd-fda8-4657-bff9-b41896ecd004")
                    lstText.Add("   LoadPat=W-12   DesignType=Wind   SelfWtMult=0   AutoLoad=""" & Designcode & """   GUID=7110c5ec-ec83-4f24-af61-fc11ce5a7f2a")
                    'If Designcode = "ASCE 7-10" Then
                    '    lstText.Add("TABLE:  ""AUTO SEISMIC - " & Designcode & """")
                    'Else
                    '    lstText.Add("TABLE:  ""AUTO SEISMIC - " & Designcode & """")
                    'End If
                    lstText.Add("TABLE:  ""AUTO SEISMIC - " & Designcode & """")
                    lstText.Add("   LoadPat=EQX   Dir=X   PercentEcc=0   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQX_D   Dir=X   PercentEcc=0   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXNY   Dir=X   PercentEcc=-0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXNY_D   Dir=X   PercentEcc=-0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXPY   Dir=X   PercentEcc=0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXPY_D   Dir=X   PercentEcc=0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQY   Dir=Y   PercentEcc=0   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQY_D   Dir=Y   PercentEcc=0   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYNX   Dir=Y   PercentEcc=-0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYNX_D   Dir=Y   PercentEcc=-0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYPX   Dir=Y   PercentEcc=0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYPX_D   Dir=Y   PercentEcc=0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    'If Designcode = "ASCE 7-10" Then
                    '    lstText.Add("TABLE:  ""AUTO WIND - ASCE7-10""")
                    'Else
                    '    lstText.Add("TABLE:  ""AUTO WIND - " & Designcode & """")
                    'End If
                    lstText.Add("TABLE:  ""AUTO WIND - " & Designcode & """")
                    lstText.Add("   LoadPat=W-1   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=1   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-2   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=1   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-3   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-4   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=-0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-5   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-6   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=-0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-7   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=3   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-8   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=3   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-9   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=0.15   E2=0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-10   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=-0.15   E2=-0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-11   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=0.15   E2=0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-12   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=-0.15   E2=-0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                End If
                'Overwrite load patterns: AUTO SEISMIC
                If str.StartsWith("TABLE:  ""AUTO SEISMIC -") Then
                    'str = reader.ReadLine
                    'If Not str.Contains("ASCE 7-16") Then
                    '    lstText.Add("TABLE:  ""AUTO SEISMIC - " & Designcode & """")
                    'End If
                    'lstText.Add(str)
                    Do
                        str = reader.ReadLine
                        'ignore old load pattern
                        IsMyKey = False
                        For Each PattStr As String In myListPatts
                            If str.Contains(PattStr) Then
                                IsMyKey = True
                                Exit For
                            End If
                        Next
                        'Write all old pattern
                        If IsMyKey = False And str.StartsWith("   LoadPat") Then lstText.Add(str)
                    Loop Until str.StartsWith("TABLE:")
                    'Write my pattern
                    '    lstText.Add("TABLE:  ""AUTO SEISMIC - " & Designcode & """")
                    lstText.Add("TABLE:  ""AUTO SEISMIC - " & Designcode & """")
                    lstText.Add("   LoadPat=EQX   Dir=X   PercentEcc=0   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQX_D   Dir=X   PercentEcc=0   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXNY   Dir=X   PercentEcc=-0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXNY_D   Dir=X   PercentEcc=-0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXPY   Dir=X   PercentEcc=0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQXPY_D   Dir=X   PercentEcc=0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.R & "   Omega=" & gJob.Omega & "   Cd=" & gJob.Cd & "   I=" & gJob.I & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQY   Dir=Y   PercentEcc=0   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028Ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQY_D   Dir=Y   PercentEcc=0   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYNX   Dir=Y   PercentEcc=-0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYNX_D   Dir=Y   PercentEcc=-0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYPX   Dir=Y   PercentEcc=0.05   EccOverride=No   PeriodCalc=Approximate   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   LoadPat=EQYPX_D   Dir=Y   PercentEcc=0.05   EccOverride=No   PeriodCalc=""Prog Calc""   CtAndX=""0.028ft, 0.8""   UserZ=No   R=" & gJob.RY & "   Omega=" & gJob.OmegaY & "   Cd=" & gJob.CdY & "   I=" & gJob.IY & "   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.7   SDS=" & gJob.Sds & "   SD1=1")
                End If
                'Overwrite load patterns: AUTO WIND
                If str.StartsWith("TABLE:  ""AUTO WIND -") Then
                    'str = reader.ReadLine
                    'If Not str.Contains("ASCE 7-16") Then
                    '    lstText.Add("TABLE:  ""AUTO WIND - " & Designcode & """")
                    'End If

                    'lstText.Add(str)
                    Do
                        str = reader.ReadLine
                        'ignore old load pattern
                        IsMyKey = False
                        For Each PattStr As String In myListPatts
                            If str.Contains(PattStr) Then
                                IsMyKey = True
                                Exit For
                            End If
                        Next
                        'Write all old pattern
                        If IsMyKey = False And str.StartsWith("   LoadPat") Then lstText.Add(str)
                    Loop Until str.StartsWith("TABLE: ")
                    'Write my pattern
                    lstText.Add("TABLE:  ""AUTO WIND - " & Designcode & """")
                    lstText.Add("   LoadPat=W-1   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=1   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-2   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=1   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-3   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-4   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=-0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-5   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-6   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=2   E1=-0.15   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-7   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=3   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-8   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=3   E1=0   E2=0   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-9   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=0.15   E2=0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-10   ExposeFrom=Diaphragms   Angle=0   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=-0.15   E2=-0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-11   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=0.15   E2=0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                    lstText.Add("   LoadPat=W-12   ExposeFrom=Diaphragms   Angle=90   WindwardCp=0.8   LeewardCp=0.5   ASCECase=4   E1=-0.15   E2=-0.15   UserZ=No   MaxZ=0   MinZ=0   WindSpeed=" & gJob.WindSpeed & "   Exposure=" & gJob.ExposureType & "   I=" & gJob.F_groundEle & "   Kzt=" & gJob.Kzt & "   GustFactor=" & gJob.F_gust & "   Kd=" & gJob.Kd & "   ExpWidth=""From diaphs""")
                End If

                ''Modify Load Case
                'If str.StartsWith("$ LOAD CASES") Then
                '    lstText.Add(str)
                '    IsMyKey = False
                '    Do
                '        str = reader.ReadLine
                '        '
                '        If str.StartsWith("  LOADCASE ""PDELTA""  TYPE") Then
                '            lstText.Add(str)
                '            IsMyKey = True
                '        ElseIf str.StartsWith("  LOADCASE ""PDELTA""  NLGEOMTYPE") Then
                '            lstText.Add("  LOADCASE ""PDELTA""  NLGEOMTYPE  ""PDelta""")
                '            IsMyKey = False
                '        ElseIf Not str.StartsWith("$") Then
                '            lstText.Add(str)
                '        End If
                '    Loop Until str.StartsWith("$")
                '    '
                '    If IsMyKey Then
                '        lstText.Insert(lstText.Count - 4, "  LOADCASE ""PDELTA""  NLGEOMTYPE  ""PDelta""")
                '    End If
                'End If

                'Overwrite Seismic function
                If str.StartsWith("TABLE:  ""FUNCTION - RESPONSE SPECTRUM - IBC2006""") Then
                    'lstText.Add(str)
                    'Do
                    'str = reader.ReadLine
                    '    'ignore old Function
                    '    'IsMyKey = False
                    '    'If str.Contains("   Name=") Then
                    '    '    IsMyKey = True
                    '    'End If
                    '    ''Write all old Function
                    '    'If IsMyKey = False And str.StartsWith("   Name=") Then lstText.Add(str)
                    '    If str.StartsWith("   Name=") Then lstText.Add(str)
                    'Loop Until str.StartsWith("TABLE:")
                    'Write my function
                    'If gJob.ASCE Like "*16" Then
                    '    lstText.Add("  FUNCTION ""SPEC""  FUNCTYPE ""SPECTRUM""  DAMPRATIO 0.05  SPECTYPE ""ASCE716""    SS " & gJob.Ss & "  S1 " & gJob.S1 & "  TL " & gJob.TL & "  SITECLASS " & gJob.SiteClass)
                    'ElseIf gJob.ASCE Like "*05" Then
                    '    lstText.Add("  FUNCTION ""SPEC""  FUNCTYPE ""SPECTRUM""  DAMPRATIO 0.05  SPECTYPE ""IBC2006""    OPTION USER  SS " & gJob.Ss & "  S1 " & gJob.S1 & "  TL " & gJob.TL & "  SITECLASS " & gJob.SiteClass)
                    'Else '7-10 default
                    '    lstText.Add("  FUNCTION ""SPEC""  FUNCTYPE ""SPECTRUM""  DAMPRATIO 0.05  SPECTYPE ""ASCE710""    OPTION USER  SS " & gJob.Ss & "  S1 " & gJob.S1 & "  TL " & gJob.TL & "  SITECLASS " & gJob.SiteClass)
                    'End If
                    Do
                        str = reader.ReadLine
                        'ignore old load pattern
                        IsMyKey = False
                        If str.Contains("Name = SPEC") Then
                            IsMyKey = True
                        End If
                    Loop Until str.StartsWith("TABLE:")

                    lstText.Add("TABLE:   ""FUNCTION - RESPONSE SPECTRUM - IBC2006""")
                    lstText.Add("   Name=SPEC   Period=0   Accel=0.533333333333333   FuncDamp=0.05   AccOption=User   Ss=" & gJob.Ss & "   S1=" & gJob.S1 & "   TL=" & gJob.TL & "   SiteClass=" & gJob.SiteClass & "   Fa=1   Fv=1.5   SDS=" & gJob.Sds & "   SD1=1")
                    lstText.Add("   Name=SPEC   Period=0.15   Accel=1.33333333333333")
                    lstText.Add("   Name=SPEC   Period=0.75   Accel=1.33333333333333")
                    lstText.Add("   Name=SPEC   Period=1   Accel=1")
                    lstText.Add("   Name=SPEC   Period=1.2   Accel=0.833333333333333")
                    lstText.Add("   Name=SPEC   Period=1.4   Accel=0.714285714285714")
                    lstText.Add("   Name=SPEC   Period=1.6   Accel=0.625")
                    lstText.Add("   Name=SPEC   Period=2   Accel=0.5")
                    lstText.Add("   Name=SPEC   Period=2.5   Accel=0.4")
                    lstText.Add("   Name=SPEC   Period=3   Accel=0.333333333333333")
                    lstText.Add("   Name=SPEC   Period=3.5   Accel=0.285714285714286")
                    lstText.Add("   Name=SPEC   Period=4   Accel=0.25")
                    lstText.Add("   Name=SPEC   Period=4.5   Accel=0.222222222222222")
                    lstText.Add("   Name=SPEC   Period=5   Accel=0.2")
                    lstText.Add("   Name=SPEC   Period=5.5   Accel=0.181818181818182")
                    lstText.Add("   Name=SPEC   Period=6   Accel=0.166666666666667")
                    lstText.Add("   Name=SPEC   Period=6.5   Accel=0.153846153846154")
                    lstText.Add("   Name=SPEC   Period=7   Accel=0.142857142857143")
                    lstText.Add("   Name=SPEC   Period=7.5   Accel=0.133333333333333")
                    lstText.Add("   Name=SPEC   Period=8   Accel=0.125")
                    lstText.Add("   Name=SPEC   Period=8.5   Accel=0.110726643598616")
                    lstText.Add("   Name=SPEC   Period=9   Accel=0.0987654320987654")
                    lstText.Add("   Name=SPEC   Period=9.5   Accel=0.0886426592797784")
                    lstText.Add("   Name=SPEC   Period=10   Accel=0.08")
                End If
                ''Overwrite Load Combinations
                'If str.StartsWith("TABLE:  ""COMBINATION DEFINITIONS""") Then
                '    lstText.Add(str)
                '    Do
                '        str = reader.ReadLine
                '        'ignore old load pattern
                '        IsMyKey = False
                '        For Each CombsStr As String In myListCombs
                '            If str.Contains(CombsStr) Then
                '                IsMyKey = True
                '                Exit For
                '            End If
                '        Next
                '        'Write all old pattern
                '        If IsMyKey = False And str.StartsWith("   ComboName=") Then lstText.Add(str)
                '    Loop Until str.StartsWith("TABLE:")
                '    'Write my pattern
                '    Dim factor1 As Double = 1.2 + 0.2 * gJob.Sds
                '    Dim factor2 As Double = 0.9 - 0.2 * gJob.Sds
                '    lstText.Add("   ComboName=EL   ComboType=Envelope   AutoDesign=No   CaseType=""Linear Static""   CaseName=EQX   ScaleFactor=" & gJob.Rho & "   SteelDesign=None   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=5d4aee4b-3728-443d-b214-98ff5504c2ee")
                '    lstText.Add("   ComboName=EL   CaseType=""Linear Static""   CaseName=EQXPY   ScaleFactor=" & gJob.Rho)
                '    lstText.Add("   ComboName=EL   CaseType=""Linear Static""   CaseName=EQXNY   ScaleFactor=" & gJob.Rho)
                '    lstText.Add("   ComboName=EL   CaseType=""Linear Static""   CaseName=EQY   ScaleFactor=" & gJob.RhoY)
                '    lstText.Add("   ComboName=EL   CaseType=""Linear Static""   CaseName=EQYPX    ScaleFactor=" & gJob.RhoY)
                '    lstText.Add("   ComboName=EL   CaseType=""Linear Static""   CaseName=EQYNX   ScaleFactor=" & gJob.RhoY)
                '    lstText.Add("   ComboName=EL_Omega   ComboType=Envelope   AutoDesign=No   CaseType=""Linear Static""   CaseName=EQX   ScaleFactor=" & gJob.Omega & "   SteelDesign=None   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=90889f48-5192-4392-9a76-a3355adc859e")
                '    lstText.Add("   ComboName=EL_Omega   CaseType=""Linear Static""   CaseName=EQXPY   ScaleFactor=" & gJob.Omega)
                '    lstText.Add("   ComboName=EL_Omega   CaseType=""Linear Static""   CaseName=EQXNY   ScaleFactor=" & gJob.Omega)
                '    lstText.Add("   ComboName=EL_Omega   CaseType=""Linear Static""   CaseName=EQY   ScaleFactor=" & gJob.OmegaY)
                '    lstText.Add("   ComboName=EL_Omega   CaseType=""Linear Static""   CaseName=EQYPX    ScaleFactor=" & gJob.OmegaY)
                '    lstText.Add("   ComboName=EL_Omega   CaseType=""Linear Static""   CaseName=EQYNX   ScaleFactor=" & gJob.OmegaY)
                '    lstText.Add("   ComboName=SST_LC8   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor1 & "   SteelDesign=None   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=ede16d50-ae3c-4d99-835c-4ffa13e22b98")
                '    lstText.Add("   ComboName=SST_LC8   CaseType=""Linear Static""   CaseName=LL   ScaleFactor=" & gJob.f1)
                '    lstText.Add("   ComboName=SST_LC8   CaseType=""Linear Static""   CaseName=SL   ScaleFactor=" & gJob.f2)
                '    lstText.Add("   ComboName=SST_LC25   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor1 & "   SteelDesign=None   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=18720a0a-03ca-4556-9234-8bf054601272")
                '    lstText.Add("   ComboName=SST_LC25   CaseType=""Linear Static""   CaseName=LL   ScaleFactor=" & gJob.f1)
                '    lstText.Add("   ComboName=SST_LC25   CaseType=""Linear Static""   CaseName=SL   ScaleFactor=" & gJob.f2)
                '    lstText.Add("   ComboName=SST_LC25   CaseType=""Response Combo""   CaseName=EL   ScaleFactor=1")
                '    lstText.Add("   ComboName=SST_LC26   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor1 & "   SteelDesign=None   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=c9d254df-5375-42a5-bfa6-f3669e18888b")
                '    lstText.Add("   ComboName=SST_LC26   CaseType=""Linear Static""   CaseName=LL   ScaleFactor=" & gJob.f1)
                '    lstText.Add("   ComboName=SST_LC26   CaseType=""Linear Static""   CaseName=SL   ScaleFactor=" & gJob.f2)
                '    lstText.Add("   ComboName=SST_LC26   CaseType=""Response Combo""   CaseName=EL   ScaleFactor=-1")
                '    lstText.Add("   ComboName=SST_LC27   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor2 & "   SteelDesign=None   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=400522c5-33f4-47eb-9cdb-51d560c0607c")
                '    lstText.Add("   ComboName=SST_LC27   CaseType=""Response Combo""   CaseName=EL   ScaleFactor=1")
                '    lstText.Add("   ComboName=SST_LC28   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor2 & "   SteelDesign=None   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=68d30827-5dbc-4f85-9079-867bf66f2607")
                '    lstText.Add("   ComboName=SST_LC28   CaseType=""Response Combo""   CaseName=EL   ScaleFactor=-1")
                '    lstText.Add("   ComboName=SST_LC29   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor1 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=e5245b57-4f35-410e-9901-0a841920dc25")
                '    lstText.Add("   ComboName=SST_LC29   CaseType=""Linear Static""   CaseName=LL   ScaleFactor=" & gJob.f1)
                '    lstText.Add("   ComboName=SST_LC29   CaseType=""Linear Static""   CaseName=SL   ScaleFactor=" & gJob.f2)
                '    lstText.Add("   ComboName=SST_LC29   CaseType=""Response Combo""   CaseName=EL_D   ScaleFactor=1")
                '    lstText.Add("   ComboName=SST_LC30   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor1 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=4ffa37de-fd3b-4528-89e4-679568228624")
                '    lstText.Add("   ComboName=SST_LC30   CaseType=""Linear Static""   CaseName=LL   ScaleFactor=" & gJob.f1)
                '    lstText.Add("   ComboName=SST_LC30  CaseType=""Linear Static""   CaseName=SL   ScaleFactor=" & gJob.f2)
                '    lstText.Add("   ComboName=SST_LC30   CaseType=""Response Combo""   CaseName=EL_D   ScaleFactor=-1")
                '    lstText.Add("   ComboName=SST_LC31   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor2 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=1f4b8500-813e-4561-be54-62d57d3d74b4")
                '    lstText.Add("   ComboName=SST_LC31   CaseType=""Response Combo""   CaseName=EL_D   ScaleFactor=1")
                '    lstText.Add("   ComboName=SST_LC32   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor2 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=a95dd870-19a9-4f62-927c-ca29dbadd0bb")
                '    lstText.Add("   ComboName=SST_LC32   CaseType=""Response Combo""   CaseName=EL_D   ScaleFactor=-1")
                '    lstText.Add("   ComboName=SST_LC33   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor1 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=a7a2db37-c2fa-463e-a05a-a4cce59ac2c8")
                '    lstText.Add("   ComboName=SST_LC33   CaseType=""Linear Static""   CaseName=LL   ScaleFactor=" & gJob.f1)
                '    lstText.Add("   ComboName=SST_LC33   CaseType=""Linear Static""   CaseName=SL   ScaleFactor=" & gJob.f2)
                '    lstText.Add("   ComboName=SST_LC33   CaseType=""Response Combo""   CaseName=EL_Omega   ScaleFactor=1")
                '    lstText.Add("   ComboName=SST_LC34   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor1 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=123f11be-b6cc-4fe0-bef7-816024bc0430")
                '    lstText.Add("   ComboName=SST_LC34   CaseType=""Linear Static""   CaseName=LL   ScaleFactor=" & gJob.f1)
                '    lstText.Add("   ComboName=SST_LC34  CaseType=""Linear Static""   CaseName=SL   ScaleFactor=" & gJob.f2)
                '    lstText.Add("   ComboName=SST_LC34   CaseType=""Response Combo""   CaseName=EL_Omega   ScaleFactor=-1")
                '    lstText.Add("   ComboName=SST_LC35   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor2 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=b5a2ecbd-1d21-4066-ac71-cd3a21edfea4")
                '    lstText.Add("   ComboName=SST_LC35   CaseType=""Response Combo""   CaseName=EL_Omega   ScaleFactor=1")
                '    lstText.Add("   ComboName=SST_LC36   ComboType=""Linear Add""   AutoDesign=No   CaseType=""Linear Static""   CaseName=DL   ScaleFactor=" & factor2 & "   SteelDesign=Strength   ConcDesign=None   AlumDesign=None   ColdDesign=None   GUID=7969bc2d-6cec-4709-a593-0deabd6b554b")
                '    lstText.Add("   ComboName=SST_LC36   CaseType=""Response Combo""   CaseName=EL_Omega   ScaleFactor=-1")
                'End If
                'Overwrite Steel preferences function
                If str.StartsWith("TABLE:  ""PREFERENCES - STEEL DESIGN") Then
                    Dim I_design As Double = MaxAll(gJob.I, gJob.IY)
                    Dim Rho_design As Double = MaxAll(gJob.Rho, gJob.RhoY)
                    Dim R_design As Double = MaxAll(gJob.R, gJob.RY)
                    Dim Cd_design As Double = MaxAll(gJob.Cd, gJob.CdY)
                    Dim Omega_design As Double = MaxAll(gJob.Omega, gJob.OmegaY)
                    'lstText.Add(str)
                    Do
                        str = reader.ReadLine
                        'ignore old load pattern
                        IsMyKey = False
                        If str.Contains("   THDesign=Envelopes") Then
                            IsMyKey = True
                        End If
                    Loop Until str.StartsWith("TABLE:")
                    '
                    lstText.Add("TABLE:  ""PREFERENCES - STEEL DESIGN")
                    lstText.Add("   THDesign=Envelopes   FrameType=OMF   PatLLF=0.75   SRatioLimit=0.95   MaxIter=1   SDC=D   SeisCode=No   SeisLoad=No   ImpFactor=" & I_design & "   SystemRho=" & Rho_design & "   SystemSds=" & gJob.Sds & "   SystemR=" & R_design & "   SystemCd=" & Cd_design & "   Omega0=" & Omega_design & "   Provision=LRFD _
                    AMethod=""Direct Analysis""   SOMethod=""General 2nd Order""   SRMethod=""Tau-b Fixed""   NLCoeff=0.002   PhiB=0.9   PhiC=0.9   PhiTY=0.9   PhiTF=0.75   PhiV=0.9   PhiVRolledI=1   PhiVT=0.9   PlugWeld=Yes   HSSWelding=ERW _
                    HSSReduceT=No   CheckDefl=No   DLRat=120   SDLAndLLRat=120   LLRat=360   TotalRat=240   NetRat=240")

                End If
                'add others
                lstText.Add(str)
            Loop Until str.Equals("END TABLE DATA")
            lstText.Add(str)
            reader.Close()
            '
            Dim wr As New StreamWriter(filePath, False)
            'write rows to excel file
            For i As Integer = 0 To (lstText.Count) - 1
                wr.WriteLine(lstText(i))
            Next
            'close file
            wr.Close()
            '
            lbStatus.Text = "S2K file has been updated!"
        Catch ex As Exception
            MsgBox(ex.Message & ex.StackTrace, MsgBoxStyle.Critical, toolName)
        End Try
    End Sub
    Private Function GetSeismicDriftLimit() As Double
        If gJob.DCRLimits.DriftLimitSelect = DriftLimit.Hx10 Then
            Return 0.01
        ElseIf gJob.DCRLimits.DriftLimitSelect = DriftLimit.Hx15 Then
            Return 0.015
        ElseIf gJob.DCRLimits.DriftLimitSelect = DriftLimit.Hx20 Then
            Return 0.02
        ElseIf gJob.DCRLimits.DriftLimitSelect = DriftLimit.Hx25 Then
            Return 0.025
        End If
    End Function
    Private Function GetWindDriftLimit() As Double
        Dim ret As Double
        Double.TryParse(gJob.DCRLimits.WindDriftLimitSelect.ToString.Remove(0, 2), ret)
        Return ret
    End Function

    Private Sub btnByGridlines_CheckedChanged(sender As Object, e As EventArgs) Handles btnByGridlines.CheckedChanged
        If Not isUserEdit Then Return
        btnByGroupNames.Checked = Not btnByGridlines.Checked
        gJob.isByGroupName = btnByGroupNames.Checked
        '
    End Sub
    Private Sub btnByGroupNames_CheckedChanged(sender As Object, e As EventArgs) Handles btnByGroupNames.CheckedChanged
        If Not isUserEdit Then Return
        btnByGridlines.Checked = Not btnByGroupNames.Checked
        gJob.isByGroupName = btnByGroupNames.Checked
        '
        btnRefresh_Click(sender, e)
    End Sub
    Private Sub btnDesignParameters_Click(sender As Object, e As EventArgs) Handles btnDesignPara2.Click
        'Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Dim frm As New frmDesignParams(gJob)
        frm.ShowDialog()
        If Not frm.curJob Is Nothing Then
            gJob.ASCE = frm.curJob.ASCE
            gJob.Ss = frm.curJob.Ss
            gJob.S1 = frm.curJob.S1
            gJob.TL = frm.curJob.TL
            gJob.SiteClass = frm.curJob.SiteClass
            gJob.userSds = frm.curJob.userSds
            gJob.Sds = frm.curJob.Sds
            '
            If frm.curJob.FrType = "MF (R=3)" Then
                gJob.FrType = "OMF"
            Else
                gJob.FrType = frm.curJob.FrType
            End If
            gJob.FrType = frm.curJob.FrType
            gJob.R = frm.curJob.R
            gJob.Omega = frm.curJob.Omega
            gJob.Cd = frm.curJob.Cd
            gJob.I = frm.curJob.I
            gJob.Rho = frm.curJob.Rho
            gJob.f1 = frm.curJob.f1
            gJob.f2 = frm.curJob.f2
            '
            If frm.curJob.FrTypeY = "MF (R=3)" Then
                gJob.FrTypeY = "OMF"
            Else
                gJob.FrTypeY = frm.curJob.FrTypeY
            End If
            gJob.RY = frm.curJob.RY
            gJob.OmegaY = frm.curJob.OmegaY
            gJob.CdY = frm.curJob.CdY
            gJob.IY = frm.curJob.IY
            gJob.RhoY = frm.curJob.RhoY
            ''gJob.f1Y = frm.curJob.f1Y
            ''gJob.f2Y = frm.curJob.f2Y
            '
            gJob.DesignCode = frm.curJob.DesignCode
            '
            gJob.WindSpeed = frm.curJob.WindSpeed
            gJob.ExposureType = frm.curJob.ExposureType
            gJob.F_groundEle = frm.curJob.F_groundEle
            gJob.Kzt = frm.curJob.Kzt
            gJob.F_gust = frm.curJob.F_gust
            gJob.Kd = frm.curJob.Kd
            '''  
            ''gJob.WindSpeedY = frm.curJob.WindSpeedY
            ''gJob.ExposureTypeY = frm.curJob.ExposureTypeY
            ''gJob.F_groundEleY = frm.curJob.F_groundEleY
            ''gJob.KztY = frm.curJob.KztY
            ''gJob.F_gustY = frm.curJob.F_gustY
            ''gJob.KdY = frm.curJob.KdY
            '
            clsSE.SetDesignPref(gJob)
            '
            isKeepRows = True
            isPrevent = True
            Update_Step1(False)
            isPrevent = False
            '''redesign
            ''If MsgBox("The design parameters have been changed. Would you like to redesign members?", MsgBoxStyle.YesNo + MsgBoxStyle.Question) = MsgBoxResult.Yes Then
            ''    If clsSE.isModelLocked = False Then
            ''        Run_Analysis(False)
            ''    End If
            ''    ''
            ''    BtnDesign_Click(sender, e)
            ''End If
            '
            If frm.isOK Then
                If g_isEtab = True Then
                    MsgBox("Some design parameters cannot be applied to new load patterns due to Application Programming Interface (API) limitations." & vbLf & vbLf &
                  "Please use 'Modify .e2k file' and 'Create LoadCombos' features to ensure correct values are applied.", MsgBoxStyle.Exclamation, "Yield-Link� Connection")
                Else
                    MsgBox("Some design parameters cannot be applied to new load patterns due to Application Programming Interface (API) limitations." & vbLf & vbLf &
                  "Please use 'Modify .s2k file' and 'Create LoadCombos' features to ensure correct values are applied.", MsgBoxStyle.Exclamation, "Yield-Link� Connection")
                End If
                'lbStatus.Text = "Warning: Design parameters have been changed. Please redesign to update Design Results!"
            End If
        End If
    End Sub
    Private Sub btnDesignParameters() '_Click(sender As Object, e As EventArgs) Handles btnDesignPara2.Click, btnDesignPara.Click
        Dim frm As New frmDesignInfo(gJob, frmType.DesignParameters)
        frm.ShowDialog()
        If Not frm.curJob Is Nothing Then
            gJob.ASCE = frm.curJob.ASCE
            gJob.Ss = frm.curJob.Ss
            gJob.S1 = frm.curJob.S1
            gJob.TL = frm.curJob.TL
            gJob.SiteClass = frm.curJob.SiteClass
            gJob.userSds = frm.curJob.userSds
            gJob.Sds = frm.curJob.Sds
            '
            If frm.curJob.FrType = "MF (R=3)" Then
                gJob.FrType = "OMF"
            Else
                gJob.FrType = frm.curJob.FrType
            End If
            'gJob.FrType = frm.curJob.FrType
            gJob.R = frm.curJob.R
            gJob.Omega = frm.curJob.Omega
            gJob.Cd = frm.curJob.Cd
            gJob.I = frm.curJob.I
            gJob.Rho = frm.curJob.Rho
            gJob.f1 = frm.curJob.f1
            gJob.f2 = frm.curJob.f2
            '
            gJob.DesignCode = frm.curJob.DesignCode
            '
            clsSE.SetDesignPref(gJob)
            '
            isKeepRows = True
            isPrevent = True
            Update_Step1(False)
            isPrevent = False
            '''redesign
            ''If MsgBox("The design parameters have been changed. Would you like to redesign members?", MsgBoxStyle.YesNo + MsgBoxStyle.Question) = MsgBoxResult.Yes Then
            ''    If clsSE.isModelLocked = False Then
            ''        Run_Analysis(False)
            ''    End If
            ''    ''
            ''    BtnDesign_Click(sender, e)
            ''End If
            lbStatus.Text = "Warning: Design parameters have been changed. Please redesign to update Design Results!"
        End If
    End Sub
    'Updated Axial load estimate For Rigid Diaphragm
    Private Sub BaseOnInPlaneShearForces_CheckedChange(sender As Object, e As EventArgs) Handles BaseOnInPlaneShearForcesToolStripMenuItem.CheckedChanged
        If Not isUserEdit Then Return
        BaseOnLinkSizeAndRValueToolStripMenuItem.Checked = Not BaseOnInPlaneShearForcesToolStripMenuItem.Checked
        gJob.isBaseOnLinkSizeAndRValue = BaseOnLinkSizeAndRValueToolStripMenuItem.Checked
    End Sub
    'Updated Axial load estimate for Rigid Diaphragm
    Private Sub BaseOnLinkSizeAndR_Value_CheckedChange(sender As Object, e As EventArgs) Handles BaseOnLinkSizeAndRValueToolStripMenuItem.CheckedChanged
        If Not isUserEdit Then Return
        BaseOnInPlaneShearForcesToolStripMenuItem.Checked = Not BaseOnLinkSizeAndRValueToolStripMenuItem.Checked
        gJob.isBaseOnInPlaneShearForce = BaseOnInPlaneShearForcesToolStripMenuItem.Checked
    End Sub

End Class