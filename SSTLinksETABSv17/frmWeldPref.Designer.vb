﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWeldPref
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbDbl2Web = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbDbl2Flg = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbPlugWeld = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lbDes1 = New System.Windows.Forms.Label()
        Me.lbDes2 = New System.Windows.Forms.Label()
        Me.picW4 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picW4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(457, 406)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(376, 406)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(196, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Doubler PL to col web/Cont plate Weld:"
        '
        'cbDbl2Web
        '
        Me.cbDbl2Web.FormattingEnabled = True
        Me.cbDbl2Web.Items.AddRange(New Object() {"Option_1", "Option_2A", "Option_2B"})
        Me.cbDbl2Web.Location = New System.Drawing.Point(225, 29)
        Me.cbDbl2Web.Name = "cbDbl2Web"
        Me.cbDbl2Web.Size = New System.Drawing.Size(80, 21)
        Me.cbDbl2Web.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(70, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(149, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Doubler PL to col flange weld:"
        '
        'cbDbl2Flg
        '
        Me.cbDbl2Flg.FormattingEnabled = True
        Me.cbDbl2Flg.Items.AddRange(New Object() {"Option_1", "Option_2"})
        Me.cbDbl2Flg.Location = New System.Drawing.Point(225, 56)
        Me.cbDbl2Flg.Name = "cbDbl2Flg"
        Me.cbDbl2Flg.Size = New System.Drawing.Size(80, 21)
        Me.cbDbl2Flg.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(39, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(180, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Use of Plug-weld for Doubler Plates?"
        '
        'cbPlugWeld
        '
        Me.cbPlugWeld.FormattingEnabled = True
        Me.cbPlugWeld.Items.AddRange(New Object() {"No", "Yes"})
        Me.cbPlugWeld.Location = New System.Drawing.Point(225, 83)
        Me.cbPlugWeld.Name = "cbPlugWeld"
        Me.cbPlugWeld.Size = New System.Drawing.Size(80, 21)
        Me.cbPlugWeld.TabIndex = 8
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbPlugWeld)
        Me.GroupBox1.Controls.Add(Me.cbDbl2Flg)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cbDbl2Web)
        Me.GroupBox1.Controls.Add(Me.lbDes1)
        Me.GroupBox1.Controls.Add(Me.lbDes2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(520, 115)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Connection Welding Perferences:"
        '
        'lbDes1
        '
        Me.lbDes1.AutoSize = True
        Me.lbDes1.Location = New System.Drawing.Point(311, 32)
        Me.lbDes1.Name = "lbDes1"
        Me.lbDes1.Size = New System.Drawing.Size(160, 13)
        Me.lbDes1.TabIndex = 7
        Me.lbDes1.Text = "Doubler without continuity plates"
        '
        'lbDes2
        '
        Me.lbDes2.AutoSize = True
        Me.lbDes2.Location = New System.Drawing.Point(311, 59)
        Me.lbDes2.Name = "lbDes2"
        Me.lbDes2.Size = New System.Drawing.Size(54, 13)
        Me.lbDes2.TabIndex = 7
        Me.lbDes2.Text = "CJP Weld"
        '
        'picW4
        '
        Me.picW4.Location = New System.Drawing.Point(12, 124)
        Me.picW4.Name = "picW4"
        Me.picW4.Size = New System.Drawing.Size(520, 263)
        Me.picW4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picW4.TabIndex = 10
        Me.picW4.TabStop = False
        '
        'frmWeldPref
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(544, 441)
        Me.Controls.Add(Me.picW4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(560, 480)
        Me.MinimizeBox = False
        Me.Name = "frmWeldPref"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Yield-Link® Connection"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picW4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnOK As Windows.Forms.Button
    Friend WithEvents btnCancel As Windows.Forms.Button
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents cbDbl2Web As Windows.Forms.ComboBox
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents cbDbl2Flg As Windows.Forms.ComboBox
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents cbPlugWeld As Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents lbDes1 As Windows.Forms.Label
    Friend WithEvents lbDes2 As Windows.Forms.Label
    Friend WithEvents picW4 As Windows.Forms.PictureBox
End Class
