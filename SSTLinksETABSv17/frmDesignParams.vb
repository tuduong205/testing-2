﻿Imports System.Drawing
Imports System.Windows.Forms
Imports YieldLinkLib

Public Class frmDesignParams
    Public curJob As clsMRSJob
    Private isUserEdit As Boolean
    Private lstSs As New List(Of String)
    Public isOK As Boolean

    Sub New(_curJob As clsMRSJob)
        ' This call is required by the designer.
        InitializeComponent()
        '
        curJob = _curJob
        Save_Load_Data(False)
        ' Add any initialization after the InitializeComponent() call.
        lbtabDetails.Visible = g_isEtab
        '
        '
        isUserEdit = True
    End Sub
    Private Sub frmInfo_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Escape Then
            curJob = Nothing
            Me.Close()
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        isOK = False
        curJob = Nothing
        Me.Close()
    End Sub

    Private Sub SdsUser_MouseDown(sender As Object, e As Windows.Forms.MouseEventArgs) Handles val_SdsUser.MouseDown
        cCB.Items.Clear()
        Dim btn As New ToolStripMenuItem
        btn.Text = "YES"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "NO"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '  
        For Each it As ToolStripMenuItem In cCB.Items
            it.Checked = it.Text = sender.text
        Next
        cCB.Show(sender, e.Location)
    End Sub
    'Private Sub FrameType_GotFocus(sender As Object, e As EventArgs) Handles val_FTypeX.GotFocus, val_FTypeY.GotFocus
    Private Sub FrameType_MouseDown(sender As Object, e As MouseEventArgs) Handles val_FTypeX.MouseDown, val_FTypeY.MouseDown
        cCB.Items.Clear()
        Dim btn As New ToolStripMenuItem
        btn.Text = "SMF"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "IMF"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '
        btn = New ToolStripMenuItem
        btn.Text = "OMF"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '
        btn = New ToolStripMenuItem
        btn.Text = "MF (R=3)"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '
        btn = New ToolStripMenuItem
        btn.Text = "BRBF"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '
        btn = New ToolStripMenuItem
        btn.Text = "SCBF"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '
        btn = New ToolStripMenuItem
        btn.Text = "OCBF"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        ''
        'btn = New ToolStripMenuItem
        'btn.Text = "Other"
        'cCB.Items.Add(btn)
        'AddHandler btn.Click, AddressOf btn_Click
        ''        
        'sender.ContextMenuStrip = cCB
        'sender.ContextMenuStrip.Show(Me.PointToScreen(New Point(sender.Location.X + cCB.Width, sender.Location.Y + cCB.Height / 2)))
        'cCB.Show(sender, Me.PointToScreen(New Point(sender.Location.X + cCB.Width, sender.Location.Y + cCB.Height / 2))) ' 
        For Each it As ToolStripMenuItem In cCB.Items
            it.Checked = it.Text = sender.text
        Next
        cCB.Show(sender, e.Location)
    End Sub
    Private Sub DesignCode_MouseDown(sender As Object, e As MouseEventArgs) Handles val_DesignCode.MouseDown
        cCB.Items.Clear()
        Dim btn As New ToolStripMenuItem
        btn.Text = "AISC 360-16"
        cCB.Items.Add(btn)
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "AISC 360-10"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "AISC 360-05"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '    
        For Each it As ToolStripMenuItem In cCB.Items
            it.Checked = it.Text = sender.text
        Next
        cCB.Show(sender, e.Location)
    End Sub
    Private Sub STD_MouseDown(sender As Object, e As MouseEventArgs) Handles val_ASCE.MouseDown, val_ASCE_W.MouseDown
        cCB.Items.Clear()
        Dim btn As New ToolStripMenuItem
        btn.Text = "ASCE 7-16"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "ASCE 7-10"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "ASCE 7-05"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "ASCE 7-02"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        ' 
        For Each it As ToolStripMenuItem In cCB.Items
            it.Checked = it.Text = sender.text
        Next
        cCB.Show(sender, e.Location)
    End Sub

    Private Sub Rho_MouseDown(sender As Object, e As MouseEventArgs) Handles val_RhoX.MouseDown, val_RhoY.MouseDown
        cCB.Items.Clear()
        Dim btn As New ToolStripMenuItem
        btn.Text = "1.0"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "1.3"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '      
        For Each it As ToolStripMenuItem In cCB.Items
            it.Checked = it.Text = sender.text
        Next
        cCB.Show(sender, e.Location)
    End Sub

    Private Sub SiteClass_MouseDown(sender As Object, e As MouseEventArgs) Handles val_SiteClass.MouseDown
        cCB.Items.Clear()
        Dim btn As New ToolStripMenuItem
        btn.Text = "A"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "B"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "C"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "D"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "E"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "F"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '  
        For Each it As ToolStripMenuItem In cCB.Items
            it.Checked = it.Text = sender.text
        Next
        cCB.Show(sender, e.Location)
    End Sub

    Private Sub Exposure_MouseDown(sender As Object, e As MouseEventArgs) Handles val_ExposureX.MouseDown
        cCB.Items.Clear()
        Dim btn As New ToolStripMenuItem
        btn.Text = "B"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "C"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        btn = New ToolStripMenuItem
        btn.Text = "D"
        cCB.Items.Add(btn)
        AddHandler btn.Click, AddressOf btn_Click
        '   
        For Each it As ToolStripMenuItem In cCB.Items
            it.Checked = it.Text = sender.text
        Next
        cCB.Show(sender, e.Location)
    End Sub


    Sub btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim txtBox As Windows.Forms.TextBox = cCB.SourceControl
        txtBox.Text = sender.text
        txtBox.SelectAll()
    End Sub
    Private Sub val_KdY_KeyPress(sender As Object, e As KeyPressEventArgs) Handles val_KztX.KeyPress, val_KdX.KeyPress, val_GustFactorX.KeyPress, val_GroundFactorX.KeyPress, val_TL.KeyPress,
        val_Ss.KeyPress, val_Sds.KeyPress, val_S1.KeyPress, val_Ry.KeyPress, val_Rx.KeyPress, val_f2X.KeyPress, val_f1X.KeyPress,
        val_RhoX.KeyPress, val_RhoY.KeyPress, val_Ix.KeyPress, val_Iy.KeyPress, val_CdX.KeyPress, val_CdY.KeyPress, val_OmegaX.KeyPress, val_OmegaY.KeyPress, val_ExposureX.KeyPress, val_WindSpeedX.KeyPress
        If ModifierKeys <> Windows.Forms.Keys.Control Then
            Dim ValidChar As String = "0123456789." + Convert.ToChar(8).ToString()
            If Not ValidChar.Contains(e.KeyChar) Then
                e.Handled = True
            End If
        ElseIf e.KeyChar = Chr(22) Then
            e.Handled = Not IsNumeric(Clipboard.GetText)
        End If
    End Sub

    Sub Save_Load_Data(isSave As Boolean)
        If isSave Then
            curJob.ASCE = val_ASCE.Text
            curJob.Ss = val_Ss.Text
            curJob.S1 = val_S1.Text
            curJob.TL = val_TL.Text
            curJob.SiteClass = val_SiteClass.Text
            curJob.userSds = val_SdsUser.Text
            curJob.Sds = val_Sds.Text
            '
            curJob.FrType = val_FTypeX.Text
            curJob.R = val_Rx.Text
            curJob.Omega = val_OmegaX.Text
            curJob.Cd = val_CdX.Text
            curJob.I = val_Ix.Text
            curJob.Rho = val_RhoX.Text
            curJob.f1 = val_f1X.Text
            curJob.f2 = val_f2X.Text
            '
            curJob.FrTypeY = val_FTypeY.Text
            curJob.RY = val_Ry.Text
            curJob.OmegaY = val_OmegaY.Text
            curJob.CdY = val_CdY.Text
            curJob.IY = val_Iy.Text
            curJob.RhoY = val_RhoY.Text
            'curJob.f1Y = val_f1Y.Text
            'curJob.f2Y = val_f2Y.Text
            '
            curJob.DesignCode = val_DesignCode.Text
            '
            curJob.ASCE = val_ASCE_W.Text
            curJob.WindSpeed = val_WindSpeedX.Text
            curJob.ExposureType = val_ExposureX.Text
            curJob.F_groundEle = val_GroundFactorX.Text
            curJob.Kzt = val_KztX.Text
            curJob.F_gust = val_GustFactorX.Text
            curJob.Kd = val_KdX.Text
            '''
            ''curJob.WindSpeedY = val_WindSpeedY.Text
            ''curJob.ExposureTypeY = val_ExposureY.Text
            ''curJob.F_groundEleY = val_GroundFactorY.Text
            ''curJob.KztY = val_KztY.Text
            ''curJob.F_gustY = val_GustFactorY.Text
            ''curJob.KdY = val_KdY.Text
        Else
            val_ASCE.Text = curJob.ASCE
            val_Ss.Text = Format(curJob.Ss, "0.0###") '  curJob.Ss
            val_S1.Text = Format(curJob.S1, "0.0###") '  curJob.S1
            val_TL.Text = Format(curJob.TL, "0.0###") '  curJob.TL
            val_SiteClass.Text = curJob.SiteClass
            val_SdsUser.Text = curJob.userSds
            val_Sds.Text = Format(curJob.Sds, "0.0###") '  curJob.Sds
            '= '
            If curJob.R = 3.0 Then
                val_FTypeX.Text = "MF (R=3)"
            Else
                val_FTypeX.Text = curJob.FrType
            End If
            'val_FTypeX.Text = curJob.FrType
            val_Rx.Text = curJob.R
            val_OmegaX.Text = Format(curJob.Omega, "0.0###") ' curJob.Omega
            val_CdX.Text = Format(curJob.Cd, "0.0###") ' curJob.Cd
            val_Ix.Text = Format(curJob.I, "0.0###") ' curJob.I
            val_RhoX.Text = Format(curJob.Rho, "0.0###")
            val_f1X.Text = Format(curJob.f1, "0.0###") ' curJob.f1
            val_f2X.Text = Format(curJob.f2, "0.0###") ' curJob.f2
            '= '
            If curJob.RY <= 3.0 Then
                val_FTypeY.Text = "MF (R=3)"
            Else
                val_FTypeY.Text = curJob.FrTypeY
            End If
            'val_FTypeY.Text = curJob.FrTypeY
            val_Ry.Text = curJob.RY
            val_OmegaY.Text = Format(curJob.OmegaY, "0.0###") '  curJob.OmegaY
            val_CdY.Text = Format(curJob.CdY, "0.0###") ' curJob.CdY
            val_Iy.Text = Format(curJob.IY, "0.0###") '  curJob.IY
            val_RhoY.Text = Format(curJob.RhoY, "0.0###") '  curJob.RhoY
            'val_f1Y.Text = curJob.f1Y
            'val_f2Y.Text = curJob.f2Y
            '= '
            val_DesignCode.Text = curJob.DesignCode
            '= '
            val_ASCE_W.Text = curJob.ASCE
            val_WindSpeedX.Text = Format(curJob.WindSpeed, "0.0###") ' curJob.WindSpeed
            val_ExposureX.Text = curJob.ExposureType
            val_GroundFactorX.Text = Format(curJob.F_groundEle, "0.0###") ' curJob.F_groundEle
            val_KztX.Text = Format(curJob.Kzt, "0.0###") ' curJob.Kzt
            val_GustFactorX.Text = Format(curJob.F_gust, "0.0###") ' curJob.F_gust
            val_KdX.Text = Format(curJob.Kd, "0.0###") ' curJob.Kd
            ''= '
            'val_WindSpeedY.Text = curJob.WindSpeedY
            'val_ExposureY.Text = curJob.ExposureTypeY
            'val_GroundFactorY.Text = curJob.F_groundEleY
            'val_KztY.Text = curJob.KztY
            'val_GustFactorY.Text = curJob.F_gustY
            'val_KdY.Text = curJob.KdY
        End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Save_Load_Data(True)
        isOK = True
        Me.Close()
    End Sub

    Private Sub val_FTypeX_TextChanged(sender As Object, e As EventArgs) Handles val_FTypeX.TextChanged, val_FTypeY.TextChanged
        Dim tbR, tbCd, tbOmega As TextBox
        If sender.name Like "*X" Then
            tbR = val_Rx
            tbCd = val_CdX
            tbOmega = val_OmegaX
        Else
            tbR = val_Ry
            tbCd = val_CdY
            tbOmega = val_OmegaY
        End If

        tbR.ReadOnly = sender.text <> "MF (R=3)"
        tbCd.ReadOnly = sender.text <> "MF (R=3)"
        tbOmega.ReadOnly = sender.text <> "MF (R=3)"
        '
        If isUserEdit = False Then Return
        '
        If sender.text = "SMF" Then
            'R
            With tbR
                .Text = "8.0"
                .ForeColor = Drawing.Color.Blue
            End With
            'Omega
            With tbOmega
                .Text = "3.0"
                .ForeColor = Drawing.Color.Black
            End With
            'Cd
            With tbCd
                .Text = "5.5"
                .ForeColor = Drawing.Color.Blue
            End With
        ElseIf sender.text = "IMF" Then
            'R
            With tbR
                .Text = "4.5"
                .ForeColor = Drawing.Color.Black
            End With
            'Omega
            With tbOmega
                .Text = "3.0"
                .ForeColor = Drawing.Color.Black
            End With
            'Cd
            With tbCd
                .Text = "4.0"
                .ForeColor = Drawing.Color.Black
            End With
        ElseIf sender.text = "OMF" Then
            'R
            With tbR
                .Text = "3.5"
                .ForeColor = Drawing.Color.Blue
            End With
            'Omega
            With tbOmega
                .Text = "3.0"
                .ForeColor = Drawing.Color.Blue
            End With
            'Cd
            With tbCd
                .Text = "3.0"
                .ForeColor = Drawing.Color.Blue
            End With
        ElseIf sender.text = "MF (R=3)" Then
            'R
            With tbR
                .Text = "3.0"
                .ForeColor = Drawing.Color.Blue
            End With
            'Omega
            With tbOmega
                .Text = "3.0"
                .ForeColor = Drawing.Color.Blue
            End With
            'Cd
            With tbCd
                .Text = "3.0"
                .ForeColor = Drawing.Color.Blue
            End With
        ElseIf sender.text = "MRBF" Then
            'R
            With tbR
                .Text = "8.0"
                .ForeColor = Drawing.Color.Black
            End With
            'Omega
            With tbOmega
                .Text = "2.5"
                .ForeColor = Drawing.Color.Black
            End With
            'Cd
            With tbCd
                .Text = "5.0"
                .ForeColor = Drawing.Color.Black
            End With
        ElseIf sender.text = "SCBF" Then
            'R
            With tbR
                .Text = "6.0"
                .ForeColor = Drawing.Color.Black
            End With
            'Omega
            With tbOmega
                .Text = "2.0"
                .ForeColor = Drawing.Color.Black
            End With
            'Cd
            With tbCd
                .Text = "5.0"
                .ForeColor = Drawing.Color.Black
            End With
        ElseIf sender.text = "OCBF" Then
            'R
            With tbR
                .Text = "3.25"
                .ForeColor = Drawing.Color.Black
            End With
            'Omega
            With tbOmega
                .Text = "2.0"
                .ForeColor = Drawing.Color.Black
            End With
            'Cd
            With tbCd
                .Text = "3.25"
                .ForeColor = Drawing.Color.Black
            End With
        End If
    End Sub


    Private Sub val_SdsUser_TextChanged(sender As Object, e As EventArgs) Handles val_SdsUser.TextChanged, val_ASCE.TextChanged, val_Ss.TextChanged, val_SiteClass.TextChanged, val_ASCE_W.TextChanged
        If isUserEdit = False Then Return
        '
        If val_SiteClass.Text = "E" Or val_SiteClass.Text = "F" Then
            val_SdsUser.Text = "YES"
        ElseIf val_SdsUser.Text = "NO" Then
            'calc Sds
            Dim is7_16 As Boolean = (val_ASCE.Text = "ASCE 7-16")
            Dim Ss As Double
            Double.TryParse(val_Ss.Text, Ss)
            Dim siteClass As String = val_SiteClass.Text
            '
            If lstSs.Count <= 0 Then
                lstSs.Add("A_0.25_0.8_0.8")
                lstSs.Add("A_0.5_0.8_0.8")
                lstSs.Add("A_0.75_0.8_0.8")
                lstSs.Add("A_1_0.8_0.8")
                lstSs.Add("A_1.25_0.8_0.8")
                lstSs.Add("A_1.5_0.8_0.8")
                lstSs.Add("B_0.25_1")
                lstSs.Add("B_0.5_1_0.9")
                lstSs.Add("B_0.75_1_0.9")
                lstSs.Add("B_1_1_0.9")
                lstSs.Add("B_1.25_1_0.9")
                lstSs.Add("B_1.5_1_0.9")
                lstSs.Add("C_0.25_1.2_1.3")
                lstSs.Add("C_0.5_1.2_1.3")
                lstSs.Add("C_0.75_1.1_1.2")
                lstSs.Add("C_1_1_1.2")
                lstSs.Add("C_1.25_1_1.2")
                lstSs.Add("C_1.5_1_1.2")
                lstSs.Add("D_0.25_1.6_1.6")
                lstSs.Add("D_0.5_1.4_1.4")
                lstSs.Add("D_0.75_1.2_1.2")
                lstSs.Add("D_1_1.1_1.1")
                lstSs.Add("D_1.25_1_1")
                lstSs.Add("D_1.5_1_1")
                lstSs.Add("E_0.25_2.5_2.4")
                lstSs.Add("E_0.5_1.7_1.7")
                lstSs.Add("E_0.75_1.2_1.3")
                lstSs.Add("E_1_0.9_N/A")
                lstSs.Add("E_1.25_0.9_N/A")
                lstSs.Add("E_1.5_0.9_N/A")
            End If
            Dim Arr() As String
            Dim curX, curY, resVal_Ss, resVal_S1 As Double
            Dim tmpLst = lstSs.FindAll(Function(x) x Like siteClass & "*")

            For i As Integer = 0 To tmpLst.Count - 1
                Arr = tmpLst(i).Split("_")
                If Ss < 0.25 Then
                    Arr = tmpLst(0).Split("_")
                    resVal_Ss = IIf(is7_16, Arr(3), Arr(2))
                    Exit For
                ElseIf Ss >= 1.5 Then
                    Arr = tmpLst(tmpLst.Count - 1).Split("_")
                    resVal_Ss = IIf(is7_16, Arr(3), Arr(2))
                    Exit For
                ElseIf Ss < Arr(1) Then
                    resVal_Ss = curY + (Ss - curX) / (Arr(1) - curX) * (IIf(is7_16, Arr(3), Arr(2)) - curY)
                    Exit For
                End If
                curX = Arr(1)
                curY = IIf(is7_16, Arr(3), Arr(2))
            Next
            '
            val_Sds.Text = Format(2 / 3 * resVal_Ss * Ss, "0.000")
        End If
        '
        If val_SdsUser.Text = "NO" Then
            With val_Sds
                .ReadOnly = True
                .BackColor = Drawing.SystemColors.Control
                .ForeColor = Drawing.Color.Black
            End With
        Else
            With val_Sds
                .ReadOnly = False
                .BackColor = Drawing.SystemColors.Window
                .ForeColor = Drawing.Color.Blue
            End With
        End If
    End Sub
    Private Sub tabInfo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabInfo.SelectedIndexChanged
        If tabInfo.SelectedIndex = 1 Then
            lbtabDetails.Text = ""
        Else
            lbtabDetails.Text = "Warning: Cannot set ""Design Frame Type"" using API. Please make it through ETABS U" & "I"
        End If
    End Sub


End Class