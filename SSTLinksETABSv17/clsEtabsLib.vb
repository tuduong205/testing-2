﻿Imports ETABSv17
Imports System.Linq
Imports YieldLinkLib

Public Class clsSAP_Etabs  '*********************************************************************ETABS ONLY
    Protected ParentPluginObject As cPlugin
    Protected mySapModel As cSapModel
    Protected ISapPlugin As cPluginCallback
    '
    Public Stories As SortedList(Of Single, String)
    Public Grids_X As List(Of GridInfo)
    Public Grids_Y As List(Of GridInfo)
    Public Grids_Any As List(Of GridInfo)
    Private ret As Long
    '
    'Public SSTLinks As SSTLinks
    'Public AllWsections As AllWsections
    'Public SSTOthers As SSTOthers
    Public AllFrames As New List(Of clsSEFrame)
    '
    Sub New()
    End Sub

    Public Sub setParentPluginObject(ByRef inParentPluginObject As cPlugin)
        ParentPluginObject = inParentPluginObject
    End Sub

    Public Sub setSapModel(ByRef inSapModel As cSapModel, ByRef inISapPlugin As cPluginCallback)
        mySapModel = inSapModel
        ISapPlugin = inISapPlugin
    End Sub

    Public Sub FindStory(Z_frame As Double, ByRef curSty As String, ByRef StyAbove As String, ByRef topSty As String,
                              ByRef LevelHeight As Double, Optional Pt_uName As String = "")
        'reset parameter
        curSty = "NA"
        StyAbove = "NA"
        topSty = "NA"
        LevelHeight = 0
        '
        If Pt_uName <> "" Then
            Dim x1, y1, z1 As Double
            ret = mySapModel.PointObj.GetCoordCartesian(Pt_uName, x1, y1, z1)
            Z_frame = z1
        End If
        If Stories Is Nothing Then
            Stories = GetAllStoryData()
        End If
        '
        If Stories.Count <= 0 Then Return
        '
        topSty = Stories.Values(Stories.Count - 1)
        '
        For i As Integer = 0 To Stories.Count - 1
            If Math.Abs(Z_frame - Stories.Keys(i)) < 1 Then
                curSty = Stories.Values(i)
                LevelHeight = Stories.Keys(i)
                '
                If i < Stories.Count - 1 Then
                    StyAbove = Stories.Values(i + 1)
                End If
                '
                Exit For
            End If
        Next
    End Sub

    Public Function GetAllStoryData() As SortedList(Of Single, String)
        Dim reUnit As Integer

        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))
            Dim NumberStories, i As Integer
            Dim StoryNames() As String
            Dim StoryHeights() As Double
            Dim StoryElevations() As Double
            Dim IsMasterStory() As Boolean
            Dim SimilarToStory() As String
            Dim SpliceAbove() As Boolean
            Dim SpliceHeight() As Double
            '
            ret = mySapModel.Story.GetStories(NumberStories, StoryNames, StoryElevations, StoryHeights, IsMasterStory, SimilarToStory, SpliceAbove, SpliceHeight)
            Dim stl As New SortedList(Of Single, String)
            '
            For i = 0 To NumberStories
                stl.Add(Math.Round(StoryElevations(i), 4), Left(StoryNames(i), Math.Min(8, StoryNames(i).Length)).Trim)
            Next
            Return stl
        Catch ex As Exception
            MsgBox(ex.Message) ' & ex.StackTrace)
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
    End Function
    Private Function GetAllGrids(axis As String) As List(Of GridInfo)
        Dim reUnit As Integer
        reUnit = mySapModel.GetPresentUnits()
        ret = mySapModel.SetPresentUnits((3))

        Dim NumberNames, i As Integer
        Dim MyName() As String

        Dim Xo As Double,
        Yo As Double,
        Rz As Double,
        GridSysType As String,
        NumXLines As Integer,
        NumYLines As Integer,
        GridLineIDX As String(),
        GridLineIDY As String(),
        OrdinateX As Double(),
        OrdinateY As Double(),
        VisibleX As Boolean(),
        VisibleY As Boolean(),
        BubbleLocX As String(),
        BubbleLocY As String()
        ' ''Get unit
        ''Dim forceUnits As eForce
        ''Dim lengthUnits As eLength
        ''Dim temperatureUnits As eTemperature
        ''ret = SapModel.GetDatabaseUnits_2(forceUnits, lengthUnits, temperatureUnits)

        'Get Grid name
        ret = mySapModel.GridSys.GetNameList(NumberNames, MyName)


        'add general grid
        Dim StoryRangeIsDefault As Boolean
        Dim TopStory As String
        Dim BottomStory As String
        Dim BubbleSize As Double
        Dim GridColor As Integer
        Dim NumGenLines As Integer
        Dim GridLineIDGen As String()
        Dim GenOrdX1 As Double()
        Dim GenOrdY1 As Double()
        Dim GenOrdX2 As Double()
        Dim GenOrdY2 As Double()
        Dim VisibleGen As Boolean()
        Dim BubbleLocGen As String()
        '
        Dim tmpInfo As GridInfo
        Dim stl As New List(Of GridInfo)
        '
        For Each name As String In MyName
            ret = mySapModel.GridSys.GetGridSysCartesian(name, Xo, Yo, Rz, StoryRangeIsDefault, TopStory, BottomStory, BubbleSize, GridColor, NumXLines, GridLineIDX, OrdinateX, VisibleX,
        BubbleLocX, NumYLines, GridLineIDY, OrdinateY, VisibleY, BubbleLocY, NumGenLines, GridLineIDGen, GenOrdX1, GenOrdY1, GenOrdX2, GenOrdY2, VisibleGen, BubbleLocGen)
            If axis.Equals("X", StringComparison.CurrentCultureIgnoreCase) Then
                For i = 0 To NumXLines - 1
                    tmpInfo = New GridInfo(GridLineIDX(i), OrdinateX(i), 0)
                    If Not stl.Contains(tmpInfo) Then
                        stl.Add(tmpInfo)
                    End If
                    ''If Not stl.ContainsKey(GridLineIDX(i)) Then stl.Add(GridLineIDX(i), OrdinateX(i))
                Next
                For i = 0 To NumGenLines - 1
                    If Math.Abs(GenOrdX2(i) - GenOrdX1(i)) < 1 Then
                        tmpInfo = New GridInfo(GridLineIDGen(i), GenOrdX1(i), 0)
                        If Not stl.Contains(tmpInfo) Then
                            stl.Add(tmpInfo)
                        End If
                        'If Not stl.ContainsKey(GridLineIDGen(i)) Then stl.Add(GridLineIDGen(i), GenOrdX1(i))
                    End If
                Next
            ElseIf axis.Equals("Y", StringComparison.CurrentCultureIgnoreCase) Then
                For i = 0 To NumYLines - 1
                    tmpInfo = New GridInfo(GridLineIDY(i), 0, OrdinateY(i))
                    If Not stl.Contains(tmpInfo) Then
                        stl.Add(tmpInfo)
                    End If
                    'If Not stl.ContainsKey(GridLineIDY(i)) Then stl.Add(GridLineIDY(i), OrdinateY(i))
                Next
                For i = 0 To NumGenLines - 1
                    If Math.Abs(GenOrdY2(i) - GenOrdY1(i)) < 1 Then
                        tmpInfo = New GridInfo(GridLineIDGen(i), 0, GenOrdY1(i))
                        If Not stl.Contains(tmpInfo) Then
                            stl.Add(tmpInfo)
                        End If
                        'If Not stl.ContainsKey(GridLineIDGen(i)) Then stl.Add(GridLineIDGen(i), GenOrdY1(i))
                    End If
                Next
            Else
                For i = 0 To NumGenLines - 1
                    'convert X,Y to global
                    While (Rz < 0)
                        Rz += 360
                    End While
                    'Covert to radian
                    Dim rad = Rz * (Math.PI / 180.0)
                    '
                    Dim X1 = Xo + GenOrdX1(i) * Math.Cos(rad) - GenOrdY1(i) * Math.Sin(rad)
                    Dim Y1 = Yo + GenOrdX1(i) * Math.Sin(rad) + GenOrdY1(i) * Math.Cos(rad)
                    '                                                                  
                    Dim X2 = Xo + GenOrdX2(i) * Math.Cos(rad) - GenOrdY2(i) * Math.Sin(rad)
                    Dim Y2 = Yo + GenOrdX2(i) * Math.Sin(rad) + GenOrdY2(i) * Math.Cos(rad)

                    tmpInfo = New GridInfo(GridLineIDGen(i), X1, Y1, X2, Y2)
                    If Not stl.Contains(tmpInfo) Then
                        stl.Add(tmpInfo)
                    End If
                    'If Not stl.ContainsKey(GridLineIDGen(i)) Then stl.Add(GridLineIDGen(i), GenOrdY1(i))
                Next
            End If
        Next
        ret = mySapModel.SetPresentUnits(reUnit)
        Return stl
    End Function
    Private Function Find_Grid_Elevation(X As Double, Y As Double, axisName As String, colAngle As Double) As String
        If axisName.Equals("X", StringComparison.CurrentCultureIgnoreCase) Then
            If Grids_X Is Nothing Then
                Grids_X = GetAllGrids("X")
            End If
            For Each item As GridInfo In Grids_X
                If Math.Abs(X - item.GridX1) < 1 Then
                    Return item.GridName
                End If
            Next
        ElseIf axisName.Equals("Y", StringComparison.CurrentCultureIgnoreCase) Then
            If Grids_Y Is Nothing Then
                Grids_Y = GetAllGrids("Y")
            End If
            For Each item As GridInfo In Grids_Y
                If Math.Abs(Y - item.GridY1) < 1 Then
                    Return item.GridName
                End If
            Next
        Else
            If Grids_Any Is Nothing Then
                Grids_Any = GetAllGrids("DiagonalGrid")
            End If
            For Each item As GridInfo In Grids_Any
                'Dim aa = (X - item.GridX1) * (item.GridY2 - item.GridY1) / ((item.GridX2 - item.GridX1) * (Y - item.GridY1))
                Dim a As Single = (item.GridY2 - item.GridY1) / (item.GridX2 - item.GridX1)

                Dim deg As Double = RoundAngle(Math.Atan(a) * 180 / Math.PI)
                Dim b As Single = item.GridY1 - a * item.GridX1
                If Math.Abs(Y - (a * X + b)) < 0.1F AndAlso Math.Abs(colAngle - deg) < 5 Then
                    Return item.GridName
                End If
            Next
        End If
        ''X > Math.Min(item.GridX1, item.GridX2) - 1 AndAlso
        ''X < Math.Max(item.GridX1, item.GridX2) + 1 AndAlso
        ''Y > Math.Min(item.GridY1, item.GridY2) - 1 AndAlso
        ''Y < Math.Max(item.GridY1, item.GridY2) + 1

        Return "NA"
    End Function

    Public Function Get_Link(ByRef curJob As clsMRSJob, oldK As Double, Dbm As Double, uName As String, ByRef changedBeams As String) As LinkInfo
        If oldK < 0 Then
            Return curJob.SSTLinks.Links.Find(Function(x) x.Size = defaultLink)
        Else
            For Each LKInfo As LinkInfo In curJob.SSTLinks.Links
                'Cal K_rot
                'Dim AF11 = LKInfo.Py_link / LKInfo.Keff
                'Dim AE11 As Double = LKInfo.Py_link * (LKInfo.tstem + Dbm)
                'Dim AG11 As Double = AF11 / ((LKInfo.tstem + Dbm) / 2)
                Dim tmpVal As Double = Cal_Krot(LKInfo, Dbm)
                If Double.IsNaN(tmpVal) Then tmpVal = 0

                If Math.Abs(tmpVal - oldK) < 10 Then
                    Return LKInfo
                End If
            Next
            '
            Dim Arr() As String
            For Each item As String In curJob.Dgv1Data
                ' 'Step 1
                Arr = Split(item, curJob.splChar)
                If Arr(3) = uName Then 'Check unique name
                    If Not changedBeams.Contains(uName) Then changedBeams += uName & ", "
                    '
                    Return curJob.SSTLinks.Links.Find(Function(x) x.Size.Equals(Arr(5), StringComparison.CurrentCultureIgnoreCase))
                End If
            Next
        End If
        '
        Return Nothing
    End Function

    Private Sub GetAllBeamColumn()
        AllFrames.Clear()
        '
        Dim defUnit As Integer
        Try
            defUnit = mySapModel.GetDatabaseUnits()
            ret = mySapModel.SetPresentUnits((3))
            '
            Dim i As Integer
            Dim Len As Double
            Dim isCol As Boolean
            Dim iPtName, jPtName As String
            '
            Dim NumberNames As Integer,
            MyName As String(),
            PropName As String(),
            StoryName As String(),
            PointName1 As String(),
            PointName2 As String(),
            Point1X As Double(),
            Point1Y As Double(),
            Point1Z As Double(),
            Point2X As Double(),
            Point2Y As Double(),
            Point2Z As Double(),
            Angle As Double(),
            Offset1X As Double(),
            Offset2X As Double(),
            Offset1Y As Double(),
            Offset2Y As Double(),
            Offset1Z As Double(),
            Offset2Z As Double(),
            CardinalPoint As Integer()
            '
            mySapModel.FrameObj.GetAllFrames(NumberNames, MyName, PropName, StoryName, PointName1, PointName2, Point1X, Point1Y, Point1Z, Point2X, Point2Y, Point2Z,
                                             Angle, Offset1X, Offset2X, Offset1Y, Offset2Y, Offset1Z, Offset2Z, CardinalPoint)
            For i = 0 To NumberNames - 1
                'point I =min X or min Y or min Z
                'point J =max X or max Y or max Z
                Len = Math.Sqrt((Point1X(i) - Point2X(i)) ^ 2 + (Point1Y(i) - Point2Y(i)) ^ 2 + (Point1Z(i) - Point2Z(i)) ^ 2)
                If Len < 50 Then Continue For
                '
                isCol = Math.Abs(Point2Z(i) - Point1Z(i)) > 10
                '
                If isCol Then
                    'Only get top point, don't add bottom point
                    If Point2Z(i) > Point1Z(i) Then
                        iPtName = "NA"
                        jPtName = PointName2(i)
                    Else
                        iPtName = "NA"
                        jPtName = PointName1(i)
                    End If
                Else
                    If Point2X(i) - Point1X(i) < 1 Then 'Y direction
                        iPtName = PointName1(i)
                        jPtName = PointName2(i)
                    ElseIf Point1X(i) - Point2X(i) < 1 Then 'Y direction
                        iPtName = PointName2(i)
                        jPtName = PointName1(i)
                    ElseIf Point2Y(i) - Point1Y(i) < 1 Then 'X direction
                        iPtName = PointName1(i)
                        jPtName = PointName2(i)
                    ElseIf Point1Y(i) - Point2Y(i) < 1 Then 'X direction
                        iPtName = PointName2(i)
                        jPtName = PointName1(i)
                    Else
                        Continue For
                    End If
                End If
                '
                AllFrames.Add(New clsSEFrame(StoryName(i), MyName(i), isCol, Len, iPtName, jPtName))
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            ret = mySapModel.SetPresentUnits(defUnit)
        End Try
    End Sub

    Public Sub GetSelectedFrames(ByRef curJob As clsMRSJob, ByRef ss As List(Of clsSEFrame))
        Dim defUnit As Integer

        Try
            defUnit = mySapModel.GetDatabaseUnits()
            ret = mySapModel.SetPresentUnits((3))
            Dim Namenum As Integer, NameType() As Integer, NameList() As String
            Dim SAuto As String = "" : Dim PropName As String = ""
            ReDim NameList(0)
            ReDim NameType(0)
            ret = mySapModel.SelectObj.GetSelected(Namenum, NameType, NameList)
            ''
            Dim i As Long
            Dim eFDO As eFrameDesignOrientation
            Dim Wsec As Wsec
            ''Dim tmpFr As clsSEFrame
            ''Dim tmpFrIDs As New List(Of String)
            Dim StPX, StPY As Double
            '
            For i = 0 To Namenum - 1
                'beam info
                If NameType(i) <> 2 Then Continue For 'get frame only
                'curBmId = NameList(i)
                ret = mySapModel.FrameObj.GetDesignOrientation(NameList(i), eFDO)
                Dim isCol As Boolean = (eFDO = eFrameDesignOrientation.Column)
                Dim iPt As String = "NA", jPt As String = "NA"
                ' Get Point1=I-End, Point2=J-End
                Dim x1, y1, z1, x2, y2, z2, Len As Double
                ret = mySapModel.FrameObj.GetPoints(NameList(i), iPt, jPt)
                ret = mySapModel.PointObj.GetCoordCartesian(iPt, x1, y1, z1)
                ret = mySapModel.PointObj.GetCoordCartesian(jPt, x2, y2, z2)
                ' Calculation Distance
                Len = Math.Sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2 + (z2 - z1) ^ 2)
                'Filter short frame
                If Len < 48 Then Continue For
                '
                Dim ElevID As String = "NA"
                Dim ElevID_WeakAxis As String = "NA"
                Dim GridID_LeftSide As String = "NA"
                Dim GridID_RightSide As String = "NA"
                Dim NumberGroups As Integer
                Dim Groups() As String
                Dim grName As String = "NA"
                '
                If curJob.isByGroupName Then
                    ret = mySapModel.FrameObj.GetGroupAssign(NameList(i), NumberGroups, Groups)
                    For Each frID As String In Groups
                        If frID Like "YL*" Or frID Like "MF*" Or frID Like "GL*" Then '1 member co the nam trong nhieu group nen can phai co rule cho group MF
                            grName = frID
                            Exit For
                        End If
                    Next
                End If
                '
                'If curJob.isByGroupName = False Then
                Dim frameAngle As Double
                If isCol Then
                    Dim ColAngle As Double
                    Dim Advanced As Boolean
                    ret = mySapModel.FrameObj.GetLocalAxes(NameList(i), ColAngle, Advanced)
                    ColAngle = RoundAngle(ColAngle)
                    frameAngle = ColAngle
                    '
                    If ColAngle > 85 AndAlso ColAngle < 95 Then
                        'Y axis
                        ElevID = IIf(curJob.isByGroupName, grName, Find_Grid_Elevation(x1, x1, "X", -1))
                        If ElevID = "NA" Then 'find again in general Grid system
                            ElevID = Find_Grid_Elevation(x1, y1, "NA", ColAngle)
                        End If
                        ElevID_WeakAxis = Find_Grid_Elevation(y1, y1, "Y", -1)
                        GridID_LeftSide = Find_Grid_Elevation(Math.Min(x1, x2), Math.Min(y1, y2), "Y", -1)
                        GridID_RightSide = GridID_LeftSide
                        StPX = Math.Min(y1, y2)
                        StPY = z1
                    ElseIf ColAngle > -5 AndAlso ColAngle < 5 Then
                        'X axis
                        ElevID = IIf(curJob.isByGroupName, grName, Find_Grid_Elevation(y1, y1, "Y", -1))
                        If ElevID = "NA" Then 'find again in general Grid system
                            ElevID = Find_Grid_Elevation(x1, y1, "NA", ColAngle)
                        End If
                        ElevID_WeakAxis = Find_Grid_Elevation(x1, x1, "X", -1)
                        GridID_LeftSide = Find_Grid_Elevation(Math.Min(x1, x2), Math.Min(y1, y2), "X", -1)
                        GridID_RightSide = GridID_LeftSide
                        StPX = Math.Min(x1, x2)
                        StPY = z1
                    Else
                        ElevID = IIf(curJob.isByGroupName, grName, Find_Grid_Elevation(x1, y1, "NA", ColAngle))
                        ElevID_WeakAxis = Find_Grid_Elevation(y1, y1, "Y", -1)
                        GridID_LeftSide = "NA" ' Find_Grid_Elevation(Math.Min(x1, x2), Math.Min(y1, y2), "Diagonal")
                        GridID_RightSide = "NA" 'Find_Grid_Elevation(Math.Max(x1, x2), Math.Max(y1, y2), "Diagonal")
                        StPX = Math.Min(y1, y2)
                        StPY = z1
                    End If
                Else
                    Dim tmpAngle As Double = (y2 - y1) / (x2 - x1)
                    Dim BeamAngle As Double = RoundAngle(Math.Atan(tmpAngle) * 180 / Math.PI)
                    frameAngle = BeamAngle

                    If Math.Abs(x1 - x2) < 0.1 Then 'Y axis
                        ElevID = IIf(curJob.isByGroupName, grName, Find_Grid_Elevation(x1, x1, "X", -1))
                        If ElevID = "NA" Then 'find again in general Grid system
                            ElevID = Find_Grid_Elevation(x1, y1, "NA", BeamAngle)
                        End If
                        GridID_LeftSide = Find_Grid_Elevation(Math.Min(x1, x2), Math.Min(y1, y2), "Y", -1)
                        GridID_RightSide = Find_Grid_Elevation(Math.Max(x1, x2), Math.Max(y1, y2), "Y", -1)
                        StPX = Math.Min(y1, y2)
                        StPY = z1
                    ElseIf Math.Abs(y1 - y2) < 0.1 Then 'X axis
                        ElevID = IIf(curJob.isByGroupName, grName, Find_Grid_Elevation(y1, y1, "Y", -1))
                        If ElevID = "NA" Then 'find again in general Grid system
                            ElevID = Find_Grid_Elevation(x1, y1, "NA", BeamAngle)
                        End If
                        GridID_LeftSide = Find_Grid_Elevation(Math.Min(x1, x2), Math.Min(y1, y2), "X", -1)
                        GridID_RightSide = Find_Grid_Elevation(Math.Max(x1, x2), Math.Max(y1, y2), "X", -1)
                        StPX = Math.Min(x1, x2)
                        StPY = z1
                    Else
                        ElevID = IIf(curJob.isByGroupName, grName, Find_Grid_Elevation(x1, y1, "NA", BeamAngle))
                        GridID_LeftSide = "NA" ' Find_Grid_Elevation(Math.Min(x1, x2), Math.Min(y1, y2), "Diagonal")
                        GridID_RightSide = "NA" 'Find_Grid_Elevation(Math.Max(x1, x2), Math.Max(y1, y2), "Diagonal")
                        StPX = Math.Min(y1, y2)
                        StPY = z1
                    End If
                End If
                'Else
                ''get frame object groups
                'Dim NumberGroups As Integer
                'Dim Groups() As String
                'ret = mySapModel.FrameObj.GetGroupAssign(NameList(i), NumberGroups, Groups)
                'For Each frID As String In Groups
                '    If frID Like "YL*" Or frID Like "MF*" Or frID Like "GL*" Then '1 member co the nam trong nhieu group nen can phai co rule cho group MF
                '        ElevID = frID
                '        GridID_LeftSide = "NA"
                '        GridID_RightSide = "NA"
                '        StPX = Math.Min(y1, y2)
                '        StPY = z1
                '        '
                '        Exit For
                '    End If
                'Next
                'End If
                '
                Dim curSty As String = "NA"
                Dim styAbove As String = "NA"
                Dim topSty As String = "NA"
                Dim LevelHeight As Double = 0
                FindStory(Math.Max(z1, z2), curSty, styAbove, topSty, LevelHeight)

                ret = mySapModel.FrameObj.GetSection(NameList(i), PropName, SAuto)
                Wsec = curJob.AllWsections.Wsections.Find(Function(x) x.Size = PropName)
                If Not Wsec Is Nothing Then
                    If isCol Then
                        If z1 > z2 Then
                            'switch point to make sure iPt is always bottom point
                            Dim tmpPt As String
                            tmpPt = jPt
                            jPt = iPt
                            iPt = tmpPt
                            StPY = z2
                        End If
                        'need to check elev and grid for column
                        ss.Add(New clsSEFrame(curSty, LevelHeight, ElevID, ElevID_WeakAxis, GridID_LeftSide, GridID_RightSide, NameList(i), isCol, frameAngle,
                                              Len, iPt, jPt, Wsec, -1, -1, styAbove, topSty, StPX, StPY))
                    Else
                        'Get old K
                        Dim oldKi As Double
                        Dim oldKj As Double
                        'If isCol = False Then
                        Dim ii(5) As Boolean
                        Dim jj(5) As Boolean
                        Dim StartValue(5) As Double
                        Dim EndValue(5) As Double
                        ret = mySapModel.FrameObj.GetReleases(NameList(i), ii, jj, StartValue, EndValue)
                        oldKi = IIf(ii(5), StartValue(5), -1)
                        oldKj = IIf(jj(5), EndValue(5), -1)
                        '
                        'ret = mySapModel.FrameObj.SetDesignProcedure(NameList(i), 1)
                        ss.Add(New clsSEFrame(curSty, LevelHeight, ElevID, ElevID_WeakAxis, GridID_LeftSide, GridID_RightSide, NameList(i), isCol, frameAngle,
                                              Len, iPt, jPt, Wsec, oldKi, oldKj, "", "", StPX, StPY))
                    End If
                End If
            Next
        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            ret = mySapModel.SetPresentUnits(defUnit)
        End Try
    End Sub

    Function ReSelectFrame(ByRef namenum As Integer, ByRef NameType() As Integer, ByRef namelist() As String)
        Dim i, j As Integer
        For i = 0 To namenum - 1
            If NameType(i) <> 2 Then
                For j = i To namenum - 2
                    namelist(j) = namelist(j + 1)
                    NameType(j) = NameType(j + 1)
                Next
                namenum = namenum - 1
                i = i - 1
            End If
        Next
        Return Nothing
    End Function

    Function ApplyKvalue_Test()
        Dim reUnit As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))
            mySapModel.SetModelIsLocked(False)
            ''SapModel.SelectObj.ClearSelection()
            Dim ii() As Boolean
            Dim jj() As Boolean
            Dim StartValue() As Double
            Dim EndValue() As Double
            ''
            ReDim ii(5)
            ReDim jj(5)
            ReDim StartValue(5)
            ReDim EndValue(5)

            ii(5) = True
            jj(5) = True
            StartValue(5) = 1234
            EndValue(5) = 5678

            ret = mySapModel.FrameObj.SetReleases("", ii, jj, StartValue, EndValue, eItemType.SelectedObjects)

            ret = mySapModel.View.RefreshView()


        Catch ex As Exception
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
        '
        Return "Ready"
    End Function

    Function ApplyKvalue(_sBeam As List(Of clsSSTBeam)) As String
        Dim reUnit, i, cnt As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3)) '6=kN.m
            mySapModel.SetModelIsLocked(False)
            ''SapModel.SelectObj.ClearSelection()
            Dim ii() As Boolean
            Dim jj() As Boolean
            Dim StartValue() As Double
            Dim EndValue() As Double
            Dim tmpKi, tmpKj As Double
            Dim MsgText As String = "Cannot assign Yield-Link® to weak axis of column. The tool will automatically apply pinned connection (K=0) to these locations. Please recheck some beams with unique name:" & vbLf
            Dim cntErrBm As Integer = 0
            ''
            For i = 0 To _sBeam.Count - 1
                ''
                ReDim ii(5)
                ReDim jj(5)
                ReDim StartValue(5)
                ReDim EndValue(5)
                'Check if ok
                'ret = mySapModel.FrameObj.GetReleases(_sBeam(i).uName, ii, jj, StartValue, EndValue)
                '
                'If _sBeam(i).status_S1 = "OK" Then
                If _sBeam(i).IsAsignEndI_K Then
                    If _sBeam(i).EndI_IsWeak Then
                        StartValue(5) = 0
                        ii(5) = True
                        tmpKi = 0
                    Else
                        StartValue(5) = _sBeam(i).K_rot
                        ii(5) = True
                        tmpKi = StartValue(5)
                    End If
                Else
                    StartValue(5) = 0
                    ii(5) = True
                    tmpKi = 0
                End If

                If _sBeam(i).IsAsignEndJ_K Then
                    If _sBeam(i).EndJ_IsWeak Then
                        EndValue(5) = 0
                        jj(5) = True
                        tmpKj = 0
                    Else
                        EndValue(5) = _sBeam(i).K_rot
                        jj(5) = True
                        tmpKj = EndValue(5)
                    End If

                Else
                    EndValue(5) = 0
                    jj(5) = True
                    tmpKj = 0
                End If
                '
                If _sBeam(i).EndI_IsWeak Or _sBeam(i).EndJ_IsWeak Then
                    MsgText = MsgText & _sBeam(i)._uName & ", "
                    cntErrBm += 1
                End If

                ret = mySapModel.FrameObj.SetReleases(_sBeam(i)._uName, ii, jj, StartValue, EndValue)
                ret = mySapModel.FrameObj.SetDesignProcedure(_sBeam(i)._uName, 1)
                '
                If ret = 0 Then
                    cnt += 1
                End If
                'End If
            Next
            If cntErrBm > 0 Then
                MsgText = Left(MsgText, Len(MsgText) - 1) '& "due to connect to column weak-axis. "
                MsgBox(MsgText, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "Warning")
            End If
            ret = mySapModel.View.RefreshView()
            '
            Return "Warning: " & cnt & "/" & _sBeam.Count & " are assigned sucessfully"
        Catch ex As Exception
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
            ret = mySapModel.SelectObj.PreviousSelection
        End Try
        '
        Return "Ready"
    End Function
    Function ApplyKvalue1Beam(_sBeam As clsSSTBeam) As String
        Dim reUnit, i, cnt As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))
            mySapModel.SetModelIsLocked(False)
            ''SapModel.SelectObj.ClearSelection()
            Dim ii() As Boolean
            Dim jj() As Boolean
            Dim StartValue() As Double
            Dim EndValue() As Double
            Dim tmpKi, tmpKj As Double
            ''
            ' For i = 0 To _sBeam.Count - 1
            ''
            ReDim ii(5)
            ReDim jj(5)
            ReDim StartValue(5)
            ReDim EndValue(5)

            'Check if ok
            'ret = mySapModel.FrameObj.GetReleases(_sBeam(i).uName, ii, jj, StartValue, EndValue)
            '
            'If _sBeam(i).status_S1 = "OK" Then
            If _sBeam.IsAsignEndI_K Then
                StartValue(5) = _sBeam.K_rot
                ii(5) = True
                tmpKi = StartValue(5)
            Else
                StartValue(5) = 0
                ii(5) = True
                tmpKi = 0
            End If

            If _sBeam.IsAsignEndJ_K Then
                EndValue(5) = _sBeam.K_rot
                jj(5) = True
                tmpKj = EndValue(5)
            Else
                EndValue(5) = 0
                jj(5) = True
                tmpKj = 0
            End If
            '
            ret = mySapModel.FrameObj.SetReleases(_sBeam._uName, ii, jj, StartValue, EndValue)
            ret = mySapModel.FrameObj.SetDesignProcedure(_sBeam._uName, 1)
            '
            If ret = 0 Then
                cnt += 1
            End If
            'End If

            'Next
            ' ret = mySapModel.View.RefreshView()
            '
            ' Return "Warning: " & cnt & "/" & _sBeam.Count & " are assigned sucessfully"
        Catch ex As Exception
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
        '
        Return "Ready"
    End Function

    Private Function GetSectionDepth(ByRef curJob As clsMRSJob, uName As String, isCol As Boolean) As clsSSTBeam
        Dim reUnit As Integer
        reUnit = mySapModel.GetPresentUnits()
        ret = mySapModel.SetPresentUnits((3))
        ' Get beam propname
        Dim SAuto As String = ""
        Dim PropName As String = ""
        ret = mySapModel.FrameObj.GetSection(uName, PropName, SAuto)
        Dim iPoint As String = "", jPoint As String = ""
        ' Get Point1=I-End, Point2=J-End
        ret = mySapModel.FrameObj.GetPoints(uName, iPoint, jPoint)
        Dim x1, y1, z1, x2, y2, z2, Len As Double
        ret = mySapModel.PointObj.GetCoordCartesian(iPoint, x1, y1, z1)
        ret = mySapModel.PointObj.GetCoordCartesian(jPoint, x2, y2, z2)
        ' Calculation Distance

        If x2 - x1 > 0 Or y2 - y1 > 0 Then
            Len = Math.Sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2 + (z2 - z1) ^ 2)
        Else
            Len = 0
        End If
        'Alway get top of column
        If isCol AndAlso z2 < z1 Then
            Dim tmp As String = iPoint
            iPoint = jPoint
            jPoint = tmp
        End If
        Dim curW As Wsec = curJob.AllWsections.Wsections.Find(Function(x) x.Size = PropName)
        ret = mySapModel.SetPresentUnits(reUnit)
        'Return New clsSEFrame(uName, isCol, Len, iPoint, jPoint, curW)
    End Function

    Private Function Get_SectionDepth(frameID As String, ByRef PropName As String, ByRef secDepth As Double, ByRef sectf As Double, ByRef secbf As Double, ByRef sectw As Double)
        Dim reUnit As Integer
        reUnit = mySapModel.GetPresentUnits()
        ret = mySapModel.SetPresentUnits((3))
        PropName = ""
        secDepth = 0
        ' Get beam propname
        Dim SAuto As String = ""
        ret = mySapModel.FrameObj.GetSection(frameID, PropName, SAuto)

        Dim NameInFile = "", FileName = "", MatProp = ""
        Dim PropType As eFramePropType
        ret = mySapModel.PropFrame.GetNameInPropFile(PropName, NameInFile, FileName, MatProp, PropType)
        ' Get Beam Height
        Dim t3 As Double, t2 As Double
        Dim Color As Long, Notes = "", GUID = ""
        Dim tf, tw, t2b, tfb As Double
        ret = mySapModel.PropFrame.GetISection(PropName, FileName, MatProp, t3, t2, tf, tw, t2b, tfb, Color, Notes, GUID)

        sectf = tf
        secbf = t2
        sectw = tw
        ret = mySapModel.SetPresentUnits(reUnit)
        Return t3
    End Function

    Private Function GetBeamLength(Framename As String, ByRef x1#, ByRef y1#, ByRef z1#, ByRef x2#, ByRef y2#, ByRef z2#) As Double
        Dim reUnit As Integer
        reUnit = mySapModel.GetPresentUnits()
        ret = mySapModel.SetPresentUnits((3))
        Dim Point1 As String = "", Point2 As String = ""
        ' Get Point1=I-End, Point2=J-End
        ret = mySapModel.FrameObj.GetPoints(Framename, Point1, Point2)
        'Dim x1, y1, z1, x2, y2, z2 As Double
        ret = mySapModel.PointObj.GetCoordCartesian(Point1, x1, y1, z1)
        ret = mySapModel.PointObj.GetCoordCartesian(Point2, x2, y2, z2)
        ret = mySapModel.SetPresentUnits(reUnit)
        ' Calculation Distance
        'If x2 - x1 > 0.1 Or y2 - y1 > 0.1 Then
        If Math.Abs(z2 - z1) < 1 Then 'beam
            Return Math.Sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2 + (z2 - z1) ^ 2)
        Else
            Return 0
        End If
    End Function
    Private Function GetColumnLength(Framename As String) As Double
        Dim reUnit As Integer
        reUnit = mySapModel.GetPresentUnits()
        ret = mySapModel.SetPresentUnits((3))
        Dim Point1 As String = "", Point2 As String = ""
        ' Get Point1=I-End, Point2=J-End
        ret = mySapModel.FrameObj.GetPoints(Framename, Point1, Point2)
        Dim x1, y1, z1, x2, y2, z2 As Double
        ret = mySapModel.PointObj.GetCoordCartesian(Point1, x1, y1, z1)
        ret = mySapModel.PointObj.GetCoordCartesian(Point2, x2, y2, z2)
        ' Calculation Distance
        ret = mySapModel.SetPresentUnits(reUnit)
        Return Math.Sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2 + (z2 - z1) ^ 2)
    End Function
    Public Function IsWeakAxis(Bm_Uname As String, Column_Uname As String) As Boolean
        Try
            Dim ret As String
            ret = False
            'assume column rotate=0 -> weak axis as Y
            'Get Beam Axis
            Dim BeamAxis, ColAxis As String
            Dim Point1 As String = "", Point2 As String = ""
            Dim x1, x2, y1, y2, z1, z2 As Double
            Dim BeamAngle As Double, ColAngle As Double, Advanced As Boolean
            ' Get Point1=I-End, Point2=J-End
            ret = mySapModel.FrameObj.GetPoints(Bm_Uname, Point1, Point2)
            'Dim x1, y1, z1, x2, y2, z2 As Double
            ret = mySapModel.PointObj.GetCoordCartesian(Point1, x1, y1, z1)
            ret = mySapModel.PointObj.GetCoordCartesian(Point2, x2, y2, z2)

            BeamAngle = RoundAngle(Math.Atan2(y2 - y1, x2 - x1) * 180 / Math.PI)
            '''
            ''If Math.Abs(x1 - x2) < 0.1 Then
            ''    BeamAxis = "Y"
            ''ElseIf Math.Abs(y1 - y2) < 0.1 Then
            ''    BeamAxis = "X"
            ''Else
            ''    'diagonal grid
            ''    ret = mySapModel.FrameObj.GetLocalAxes(Bm_Uname, BeamAngle, Advanced)
            ''End If
            ' GetColumn Axis
            ret = mySapModel.FrameObj.GetLocalAxes(Column_Uname, ColAngle, Advanced)
            ColAngle = RoundAngle(ColAngle)

            ''If Math.Abs(ColAngle - 0) < 1 Or Math.Abs(ColAngle - 180) < 1 Then
            ''    ColAxis = "X"
            ''Else
            ''    ColAxis = "Y"
            ''End If

            ''If BeamAxis <> ColAxis Then
            ''    ret = True
            ''End If
            ''Return ret
            Return Math.Abs(ColAngle - BeamAngle) > 5
        Catch
            Return False
        End Try
    End Function
    Public Sub run_analysis(ByRef combcheck As Integer)
        ret = mySapModel.FrameObj.SetEndLengthOffset("1", True, 1, 1, 1, eItemType.SelectedObjects)
        ret = mySapModel.Analyze.RunAnalysis
        Dim numbername As Integer, i As Integer
        Dim myname() As String
        ret = mySapModel.RespCombo.GetNameList(numbername, myname)
        For i = 0 To numbername - 1
            If myname(i) Like "SST_LC*" And myname(i) <> "SST_LC20" Then
                combcheck = combcheck + 1
            End If
        Next
        For i = 0 To numbername - 1
            ret = mySapModel.Results.Setup.SetComboSelectedForOutput(myname(i), True)
        Next
        ret = mySapModel.SetPresentUnits((3))
    End Sub
    Public Sub run_Design()
        Dim NumberNames As Long
        Dim MyName() As String
        Dim Selected As Boolean
        Dim i As Integer
        'add default steel design combos
        '     ret = SapModel.RespCombo.AddDesignDefaultCombos(False, False, False, False)

        'get combo names
        ret = mySapModel.RespCombo.GetNameList(NumberNames, MyName)
        'select combos for steel strength design
        For i = 0 To NumberNames - 1
            If MyName(i) Like "SST_LC*" And MyName(i) <> "SST_LC20" Then
                Selected = True
            Else : Selected = False
            End If
            ret = mySapModel.DesignSteel.SetComboStrength(MyName(i), Selected)
        Next i
        ret = mySapModel.DesignSteel.StartDesign

    End Sub
    Public Function P_Axial(name As String, ByRef COMB As String, beam_depth As Double, linkthick As Double) As Double
        Dim pu As Double = 0, i As Integer
        Dim reUnit As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))
            Dim NumberResults As Integer
            Dim Obj() As String
            Dim ObjSta() As Double
            Dim Elm() As String
            Dim ElmSta() As Double
            Dim LoadCase() As String
            Dim StepType() As String
            Dim StepNum() As Double
            Dim P() As Double
            Dim V2() As Double
            Dim V3() As Double
            Dim T() As Double
            Dim M2() As Double
            Dim M3() As Double

            ret = mySapModel.Results.FrameForce(name, eItemTypeElm.ObjectElm, NumberResults, Obj, ObjSta, Elm, ElmSta, LoadCase, StepType, StepNum, P, V2, V3, T, M2, M3)
            If ret = 0 Then
                For i = 0 To NumberResults - 1
                    If ObjSta(i) = ObjSta(0) Or ObjSta(i) = ObjSta(NumberResults - 1) Then
                        If pu < Math.Abs(M3(i)) * 2 / (beam_depth + linkthick) Then
                            pu = Math.Abs(M3(i)) * 2 / (beam_depth + linkthick)
                            COMB = LoadCase(i)
                        End If
                    End If
                Next
            Else
                pu = Math.Exp(20)
            End If
        Catch
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
        Return pu
    End Function
    Public Sub BeamReac(ID As String, ByRef maxshear As Double, ByRef maxAxial As Double, ByRef maxMoment As Double)
        Dim reUnit As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))
            Dim i As Integer
            maxshear = 0 : maxAxial = 0 : maxMoment = 0
            Dim checkLcm As Boolean = False
            Dim NumberResults As Integer
            Dim Obj() As String
            Dim ObjSta() As Double
            Dim Elm() As String
            Dim ElmSta() As Double
            Dim LoadCase() As String
            Dim StepType() As String
            Dim StepNum() As Double
            Dim P() As Double
            Dim V2() As Double
            Dim V3() As Double
            Dim T() As Double
            Dim M2() As Double
            Dim M3() As Double

            ret = mySapModel.Results.FrameForce(ID, eItemTypeElm.ObjectElm, NumberResults, Obj, ObjSta, Elm, ElmSta, LoadCase, StepType, StepNum, P, V2, V3, T, M2, M3)
            If ret = 0 Then
                For i = 0 To NumberResults - 1
                    Dim isCheckCase As Boolean
                    isCheckCase = (LoadCase(i) = "SST_LC33" Or LoadCase(i) = "SST_LC34" Or LoadCase(i) = "SST_LC35" Or LoadCase(i) = "SST_LC36")

                    If isCheckCase Then
                        checkLcm = True
                        If ObjSta(i) = ObjSta(0) Or ObjSta(i) = ObjSta(NumberResults - 1) Then
                            If maxshear < Math.Abs(V2(i)) Then
                                maxshear = Math.Abs(V2(i))
                            End If
                            If maxAxial < Math.Abs(P(i)) Then
                                maxAxial = Math.Abs(P(i))
                            End If
                            If maxMoment < Math.Abs(M3(i)) Then
                                maxMoment = Math.Abs(M3(i))
                            End If
                        End If
                    End If
                Next
            End If
            If checkLcm = False Then ret = 1
        Catch
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
    End Sub
    Public Sub Finishcheck()
        ret = mySapModel.SetModelIsLocked(False)
        ret = mySapModel.SelectObj.PreviousSelection
    End Sub

    Private Function Find_Krot(ByRef curJob As clsMRSJob, Kval As Double, Dbm As Double) As LinkInfo
        Dim lk As LinkInfo
        For Each lk In curJob.SSTLinks.Links
            Dim AF11 = lk.Py_link / lk.Keff
            Dim AE11 As Double = lk.Py_link * (lk.tstem + Dbm)
            Dim AG11 As Double = AF11 / ((lk.tstem + Dbm) / 2)
            If Math.Abs(AE11 / AG11 - Kval) < 1 Then
                Return lk
            End If
        Next
        Return curJob.SSTLinks.Links(0)
    End Function
    Private Function Find_BRP(ByRef curJob As clsMRSJob, lkSize As String) As BRPInfo
        Return curJob.SSTLinks.BRPs.Find(Function(x) x.Size = lkSize)
    End Function

    Public Function RunAnalysis(RunIfUnlock As Boolean) As Boolean
        Dim isLocked = isModelLocked()
        If RunIfUnlock AndAlso Not isLocked Then
            ret = mySapModel.Analyze.RunAnalysis()
        End If
        Return isLocked
    End Function
    Public Function SetEndLengthOffsetforSAP(beamID$) As Boolean 'No need for ETABS
        ''ret = mySapModel.FrameObj.SetEndLengthOffset(beamID, True, 0, 0, 0)
    End Function
    Public Function GetEndLengthOffsetforSAP(beamID$) As Boolean
        ''Dim offset_I#, offset_J#
        ''Dim isAuto As Boolean
        ''ret = mySapModel.FrameObj.GetEndLengthOffset(beamID, isAuto, offset_I, offset_J, 0)
        ''Return isAuto Or offset_I * offset_J > 0
    End Function
    Public Function isModelLocked() As Boolean
        Try
            Return mySapModel.GetModelIsLocked
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Sub Get_Mu_Pu_Vu(name As String, ByRef maxMij33#, ByRef maxPu_design#, ByRef maxPu_omega#,
                            ByRef maxVu_gravity#, ByRef maxVu_col_EL#, ByRef maxVu_33_36#, isDebug As Boolean)
        If isDebug Then
            Select Case name
                Case "81" : maxMij33 = 501 : maxPu_design = 50 : maxVu_gravity = 123 : maxVu_col_EL = 23 : maxVu_33_36 = 50
                Case "82" : maxMij33 = 502 : maxPu_design = 40 : maxVu_gravity = 86 : maxVu_col_EL = 54 : maxVu_33_36 = 66
                Case "122" : maxMij33 = 503 : maxPu_design = 44 : maxVu_gravity = 54 : maxVu_col_EL = 76 : maxVu_33_36 = 43
                Case "123" : maxMij33 = 504 : maxPu_design = 48 : maxVu_gravity = 96 : maxVu_col_EL = 33 : maxVu_33_36 = 224
                Case "163" : maxMij33 = 505 : maxPu_design = 41 : maxVu_gravity = 24 : maxVu_col_EL = 76 : maxVu_33_36 = 65
                Case "164" : maxMij33 = 506 : maxPu_design = 59 : maxVu_gravity = 68 : maxVu_col_EL = 22 : maxVu_33_36 = 34
                Case "294" : maxMij33 = 507 : maxPu_design = 45 : maxVu_gravity = 42 : maxVu_col_EL = 76 : maxVu_33_36 = 85
                Case "295" : maxMij33 = 508 : maxPu_design = 65 : maxVu_gravity = 34 : maxVu_col_EL = 43 : maxVu_33_36 = 35
                Case "298" : maxMij33 = 509 : maxPu_design = 100 : maxVu_gravity = 66 : maxVu_col_EL = 78 : maxVu_33_36 = 33
                Case "299" : maxMij33 = 5010 : maxPu_design = 126 : maxVu_gravity = 64 : maxVu_col_EL = 26 : maxVu_33_36 = 47

                Case "300" : maxMij33 = 509 : maxPu_design = 125 : maxVu_gravity = 88 : maxVu_col_EL = 66 : maxVu_33_36 = 25
                Case "296" : maxMij33 = 5010 : maxPu_design = 120 : maxVu_gravity = 54 : maxVu_col_EL = 43 : maxVu_33_36 = 54
                Case "205" : maxMij33 = 509 : maxPu_design = 43 : maxVu_gravity = 99 : maxVu_col_EL = 88 : maxVu_33_36 = 34
                Case "204" : maxMij33 = 5010 : maxPu_design = 55 : maxVu_gravity = 73 : maxVu_col_EL = 33 : maxVu_33_36 = 22
            End Select
        Else
            Dim reUnit As Integer
            Try
                reUnit = mySapModel.GetPresentUnits()
                ret = mySapModel.SetPresentUnits((3)) 'KN-m-C

                Dim NumberResults As Integer
                Dim Obj() As String
                Dim ObjSta() As Double
                Dim Elm() As String
                Dim ElmSta() As Double
                Dim LoadCase() As String
                Dim StepType() As String
                Dim StepNum() As Double
                Dim P() As Double
                Dim V2() As Double
                Dim V3() As Double
                Dim T() As Double
                Dim M2() As Double
                Dim M3() As Double
                Dim i As Integer
                Dim NumberNames As Long
                maxMij33 = 0
                maxPu_design = 0
                maxPu_omega = 0
                maxVu_gravity = 0
                maxVu_col_EL = 0
                maxVu_33_36 = 0
                '
                Dim comEndI = ""
                Dim comEndJ = ""
                Dim DL, LL, S As Double
                '
                'clear all case and combo output selections
                ret = mySapModel.Results.Setup.DeselectAllCasesAndCombosForOutput
                'get combo names
                ret = mySapModel.RespCombo.GetNameList(NumberNames, LoadCase)
                'set case and combo output selections
                For i = 0 To NumberNames - 1
                    ret = mySapModel.Results.Setup.SetComboSelectedForOutput(LoadCase(i))
                Next

                ret = mySapModel.Results.FrameForce(name, eItemTypeElm.ObjectElm, NumberResults, Obj, ObjSta, Elm, ElmSta, LoadCase, StepType, StepNum, P, V2, V3, T, M2, M3)
                '
                Dim lcmNo As Integer
                '
                If ret = 0 Then
                    For i = 0 To NumberResults - 1
                        If ObjSta(i) = ObjSta(0) Or ObjSta(i) = ObjSta(NumberResults - 1) Then
                            If LoadCase(i) Like "SST_LC*" Then
                                Integer.TryParse(LoadCase(i).Remove(0, "SST_LC".Length), lcmNo)
                                'Mu for link LC1-7, 9-28
                                If lcmNo > 0 AndAlso lcmNo <> 8 AndAlso lcmNo <> 23 AndAlso lcmNo <> 24 AndAlso lcmNo <= 28 Then
                                    maxMij33 = Math.Max(maxMij33, Math.Abs(M3(i)))
                                End If
                                'Pu column for column check
                                If lcmNo > 0 AndAlso lcmNo <> 8 AndAlso (lcmNo <= 22 Or lcmNo >= 29) Then
                                    maxPu_design = Math.Max(maxPu_design, Math.Abs(P(i)))
                                    If lcmNo >= 29 Then
                                        maxPu_omega = Math.Max(maxPu_omega, Math.Abs(P(i)))
                                    End If
                                End If
                                'LC33-36 - tang len do add combos
                                If lcmNo > 0 AndAlso lcmNo <> 8 AndAlso lcmNo >= 33 AndAlso lcmNo <= 36 Then
                                    maxVu_33_36# = Math.Max(maxVu_33_36#, Math.Abs(V2(i)))
                                End If
                                ''Maxshear from Gravity combos
                                If lcmNo = 8 Then maxVu_gravity = MaxAll(maxVu_gravity, Math.Abs(V2(i)))
                            ElseIf LoadCase(i) = "EL" Then
                                maxVu_col_EL = Math.Max(maxVu_col_EL, Math.Abs(V2(i)))
                            End If
                            'Combo
                            comEndI = LoadCase(i)
                        End If
                    Next
                End If
                '
                maxMij33 = Math.Round(maxMij33, 2)
                maxPu_design = Math.Round(maxPu_design, 2)
                maxPu_omega = Math.Round(maxPu_omega, 2)
                maxVu_gravity = Math.Round(maxVu_gravity, 2)
                maxVu_col_EL = Math.Round(maxVu_col_EL, 2) 'use for step 3
                maxVu_33_36 = Math.Round(maxVu_33_36, 2) 'use for calculation P_axial shear tab
            Catch ex As Exception
                'MsgBox(ex.Message & ex.StackTrace)
            Finally
                ret = mySapModel.SetPresentUnits(reUnit)
            End Try
        End If
    End Sub

    Public Function Set_Get_ProjInfo(modelGUI As String) As String
        Dim i As Integer
        Dim NumberItems As Integer
        Dim Item() As String
        Dim Data() As String
        ret = mySapModel.GetProjectInfo(NumberItems, Item, Data)
        For i = 0 To NumberItems - 1
            If Item(i) = "Project Name" Then
                Return Data(i)
            End If
        Next
        '
        ret = mySapModel.SetProjectInfo("Project Name", modelGUI)
        '
        Return modelGUI
    End Function
    'Create CreateELFLoadConbination
    Public Function CreateELFLoadConbination(ByRef curJob As clsMRSJob, Optional CreateELF10030 As Boolean = False, Optional keepLoadPatt As Boolean = True) As String
        Try
            Dim cnt As Integer
            Dim LoadCombos() As String
            Dim NoLCombos As Integer
            '
            Dim SS As Double, S1 As Double, siteclass As Char, TL, Ie, Rframe, Omega, Cd As Double
            Dim windSpeed, EleFactor, Kzt, GustFactor, Kd As Double
            Dim exposureType As String
            SS = curJob.Ss
            S1 = curJob.S1
            siteclass = curJob.SiteClass
            TL = curJob.TL
            Ie = curJob.I
            Rframe = curJob.R
            Omega = curJob.Omega
            Cd = curJob.Cd
            windSpeed = curJob.WindSpeed
            EleFactor = curJob.F_groundEle
            Kzt = curJob.Kzt
            GustFactor = curJob.F_gust
            Kd = curJob.Kd
            If curJob.ExposureType = "B" Then
                exposureType = 1
            ElseIf curJob.ExposureType = "C" Then
                exposureType = 2
            Else
                exposureType = 3
            End If
            ''''''''''''
            Dim myPDELTA As String = "PDELTA"
            'delete load pat
            Dim isOverwrite, isExist, isLocked As Boolean
            Dim isskips As Boolean = False
            Dim NoLPatts, NoLCaces As Integer
            Dim lockedNote As String
            Dim LoadCases(), LoadPatts() As String
            Dim MyLoadType() As String
            Dim MyLoadName() As String
            Dim MySF() As Double
            '
            'Define Parameters
            Dim myListPatts As New SortedList(Of String, eLoadPatternType)
            myListPatts.Add("DL", eLoadPatternType.Dead)
            myListPatts.Add("LL", eLoadPatternType.Live)
            myListPatts.Add("LR", eLoadPatternType.Rooflive)
            myListPatts.Add("SL", eLoadPatternType.Snow)
            myListPatts.Add("RL", eLoadPatternType.Other)
            ''Use only W
            'myListPatts.Add("WX", eLoadPatternType.Wind)
            'myListPatts.Add("WY", eLoadPatternType.Wind)
            myListPatts.Add("W", eLoadPatternType.Wind)
            myListPatts.Add("NLX", eLoadPatternType.Notional)
            myListPatts.Add("NLY", eLoadPatternType.Notional)
            myListPatts.Add("EQX", eLoadPatternType.Quake)
            myListPatts.Add("EQXPY", eLoadPatternType.Quake)
            myListPatts.Add("EQXNY", eLoadPatternType.Quake)
            myListPatts.Add("EQY", eLoadPatternType.Quake)
            myListPatts.Add("EQYPX", eLoadPatternType.Quake)
            myListPatts.Add("EQYNX", eLoadPatternType.Quake)
            '
            myListPatts.Add("EQX_D", 37)
            myListPatts.Add("EQXPY_D", 37)
            myListPatts.Add("EQXNY_D", 37)
            myListPatts.Add("EQY_D", 37)
            myListPatts.Add("EQYPX_D", 37)
            myListPatts.Add("EQYNX_D", 37)
            '
            'Dim myELF10030LoadCases() As String
            Dim myLcms() As String
            '
            If Not CreateELF10030 Then
                myLcms = {"NL", "WL", "EL", "EL_Omega", "EL_D",
                          "SST_LC1", "SST_LC2", "SST_LC3", "SST_LC4", "SST_LC5", "SST_LC6", "SST_LC7",
                          "SST_LC8",
                          "SST_LC9", "SST_LC10", "SST_LC11", "SST_LC12", "SST_LC13", "SST_LC14", "SST_LC15", "SST_LC16", "SST_LC17", "SST_LC18", "SST_LC19", "SST_LC20", "SST_LC21", "SST_LC22", "SST_LC23", "SST_LC24",
                          "SST_LC25", "SST_LC26", "SST_LC27", "SST_LC28",
                          "SST_LC29", "SST_LC30", "SST_LC31", "SST_LC32",
                          "SST_LC33", "SST_LC34", "SST_LC35", "SST_LC36"}
            Else
                myLcms = {"NL", "WL", "EL", "EL_Omega", "EL_D", "EL_100+30",
                          "ELF 100XPY+30Y", "ELF 100XNY+30Y", "ELF 100XPY-30Y", "ELF 100XNY-30Y",
                          "ELF -100XPY+30Y", "ELF -100XNY+30Y", "ELF -100XPY-30Y", "ELF -100XNY-30Y", "ELF 30X+100YPX", "ELF 30X+100YNX", "ELF 30X-100YPX",
                          "ELF 30X-100YNX", "ELF -30X+100YPX", "ELF -30X+100YNX", "ELF -30X-100YPX", "ELF -30X-100YNX",
                          "SST_LC1", "SST_LC2", "SST_LC3", "SST_LC4", "SST_LC5", "SST_LC6", "SST_LC7",
                          "SST_LC8",
                          "SST_LC9", "SST_LC10", "SST_LC11", "SST_LC12", "SST_LC13", "SST_LC14", "SST_LC15", "SST_LC16", "SST_LC17", "SST_LC18", "SST_LC19", "SST_LC20", "SST_LC21", "SST_LC22", "SST_LC23", "SST_LC24",
                          "SST_LC25", "SST_LC26", "SST_LC27", "SST_LC28",
                          "SST_LC29", "SST_LC30", "SST_LC31", "SST_LC32",
                          "SST_LC33", "SST_LC34", "SST_LC35", "SST_LC36",
                          "SST_LC37", "SST_LC38", "SST_LC39", "SST_LC40"}
            End If
            '
            ret = mySapModel.LoadPatterns.GetNameList(NoLPatts, LoadPatts)
            ret = mySapModel.LoadCases.GetNameList(NoLCaces, LoadCases)
            ret = mySapModel.RespCombo.GetNameList(NoLCombos, LoadCombos)
            '
            If LoadPatts Is Nothing Then LoadPatts = {"DefaultLoadPath"}
            If LoadCases Is Nothing Then LoadCases = {"DefaultLoadCases"}
            If LoadCombos Is Nothing Then LoadCombos = {"DefaultLoadCombos"}
            'Check if LoadPattern existed
            For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                'Load patt
                If Not keepLoadPatt Then
                    If LoadPatts.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                        isExist = True
                        Exit For
                    End If
                End If
                'Load case
                If LoadCases.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                    isExist = True
                    Exit For
                End If
            Next

            '''Check if LoadCombos existed
            If isOverwrite = False Then
                For Each lcm As String In myLcms
                    If LoadCombos.Contains(lcm, StringComparer.CurrentCultureIgnoreCase) Then
                        isExist = True
                        Exit For
                    End If
                Next
            End If
            'Question
            isLocked = mySapModel.GetModelIsLocked
            '
            If isExist = True Then
                If MsgBox("Some " & IIf(keepLoadPatt, "", "LoadPatterns, ") & "LoadCases or Load Combinations have already existed." & If(isLocked, "Also, This model is locked.", "") &
                          vbLf & "Would you like to" & If(isLocked, "unlock and", "") & " overwrite all of them?",
                                      MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirm") = MsgBoxResult.Yes Then
                    mySapModel.SetModelIsLocked(False)
                    isOverwrite = True
                Else
                    Return "Cancelled"
                End If
            End If
            '-------------------------------------------------------------------DELETE
            If isOverwrite Then
                '------------------------------------Delete Load combonations
                For Each lcm As String In myLcms
                    If LoadCombos.Contains(lcm, StringComparer.CurrentCultureIgnoreCase) Then
                        ret = mySapModel.RespCombo.Delete(lcm)
                    End If
                Next
                If Not CreateELF10030 Then
                    ret = mySapModel.RespCombo.Delete("SST_LC37")
                    ret = mySapModel.RespCombo.Delete("SST_LC38")
                    ret = mySapModel.RespCombo.Delete("SST_LC39")
                    ret = mySapModel.RespCombo.Delete("SST_LC40")
                End If
                '------------------------------------Delete load case
                For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                    If LoadCases.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                        'ret = Delete_load(curLPatt.Key, 2)
                        mySapModel.LoadCases.Delete(curLPatt.Key)
                    End If
                Next

                '"PDELTA" case
                ret = mySapModel.LoadCases.Delete(myPDELTA)
                'ret = Delete_load(myPDELTA, 2)
                '------------------------------------Delete Load Pattern
                If Not keepLoadPatt Then
                    For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                        If LoadPatts.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                            'ret = Delete_load(curLPatt.Key, 1)
                            ret = mySapModel.LoadPatterns.Delete(curLPatt.Key)
                        End If
                    Next
                End If
            End If
            '-------------------------------------------------------------------CREATE
            'reset array
            LoadPatts = {}
            LoadCases = {}
            LoadCombos = {}
            ''reload all info
            ret = mySapModel.LoadPatterns.GetNameList(NoLPatts, LoadPatts)
            ret = mySapModel.LoadCases.GetNameList(NoLCaces, LoadCases)
            ret = mySapModel.RespCombo.GetNameList(NoLCombos, LoadCombos)
            If LoadPatts Is Nothing Then LoadPatts = {"DefaultLoadPath"}
            If LoadCases Is Nothing Then LoadCases = {"DefaultLoadCases"}
            If LoadCombos Is Nothing Then LoadCombos = {"DefaultLoadCombos"}
            'Create Load Patt and Load case
            For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                'Load Pattern
                If Not keepLoadPatt Then
                    If Not LoadPatts.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                        If curLPatt.Key = "DL" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 1)
                        ElseIf curLPatt.Key Like "NL*" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 0)
                            'Need add Auto for Notional loads
                        ElseIf curLPatt.Key Like "EQ*_D" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 0)
                            ret = mySapModel.LoadPatterns.AutoSeismic.SetIBC2006(curLPatt.Key, 1, 0.05, 2, 0, 0, False, 0, 0, Rframe, Omega, Cd, Ie, 2, 37.8652, -122.257, 94704, SS, S1, TL, Asc(siteclass) - 64, 1, 1)
                        ElseIf curLPatt.Key Like "EQ*" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 0)
                            ret = mySapModel.LoadPatterns.AutoSeismic.SetIBC2006(curLPatt.Key, 1, 0.05, 1, 0, 0, False, 0, 0, Rframe, Omega, Cd, Ie, 2, 37.8652, -122.257, 94704, SS, S1, TL, Asc(siteclass) - 64, 1, 1)
                        Else
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value)
                        End If
                    End If
                End If
                'Load Case
                If Not LoadCases.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                    ret = mySapModel.LoadCases.StaticLinear.SetCase(curLPatt.Key)
                    'set load data
                    ReDim MyLoadType(0)
                    ReDim MyLoadName(0)
                    ReDim MySF(0)
                    MyLoadType(0) = "Load"
                    MyLoadName(0) = curLPatt.Key
                    MySF(0) = 1
                    ret = mySapModel.LoadCases.StaticLinear.SetLoads(curLPatt.Key, 1, MyLoadType, MyLoadName, MySF)
                End If
            Next
            'Create "PDELTA" case
            If Not LoadCases.Contains(myPDELTA, StringComparer.CurrentCultureIgnoreCase) Then
                'add PDELTA load
                ret = mySapModel.LoadCases.StaticNonlinear.SetCase(myPDELTA)
                'set load data
                ReDim MyLoadType(4)
                ReDim MyLoadName(4)
                ReDim MySF(4)
                MyLoadType(0) = "Load"
                MyLoadName(0) = "DL"
                MySF(0) = 1
                MyLoadType(1) = "Load"
                MyLoadName(1) = "LL"
                MySF(1) = 0.5
                MyLoadType(2) = "Load"
                MyLoadName(2) = "LR"
                MySF(2) = 0.5
                MyLoadType(3) = "Load"
                MyLoadName(3) = "SL"
                MySF(3) = 0.5
                'Rainload
                MyLoadType(4) = "Load"
                MyLoadName(4) = "RL"
                MySF(4) = 0.5
                ret = mySapModel.LoadCases.StaticNonlinear.SetLoads(myPDELTA, 5, MyLoadType, MyLoadName, MySF)
                ret = mySapModel.LoadCases.StaticNonlinear.SetGeometricNonlinearity(myPDELTA, 1)
            End If
            ' 'Set Non-Linear case “PDelta” for nonlinear stiffness.
            Dim EQCases() As String = {"EQX", "EQXPY", "EQXNY", "EQY", "EQYPX", "EQYNX", "EQX_D", "EQXPY_D", "EQXNY_D", "EQY_D", "EQYPX_D", "EQYNX_D"}
            '
            For Each curCase As String In EQCases
                ret = mySapModel.LoadCases.StaticLinear.SetInitialCase(curCase, myPDELTA)
            Next
            '' 'Set Non-Linear case “PDelta” for Wind.
            ret = mySapModel.LoadCases.StaticLinear.SetInitialCase("W", myPDELTA)

            '------------------------------------------------------------------------------------
            'Create Load combination
            Dim lstLcm As New List(Of clsELFCombos)
            cnt = 0
            '
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "NL", 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "WL", 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "EL", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, curJob.Rho, curJob.Rho, curJob.Rho, curJob.RhoY, curJob.RhoY, curJob.RhoY, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "EL_Omega", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, curJob.Omega, curJob.Omega, curJob.Omega, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "EL_D", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            If CreateELF10030 Then
                'add here
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 100XPY+30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 100XNY+30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 100XPY-30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, -0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 100XNY-30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -100XPY+30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -100XNY+30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -100XPY-30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -100XNY-30Y", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 30X+100YPX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 30X+100YNX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 30X-100YPX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF 30X-100YNX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -30X+100YPX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.3, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -30X+100YNX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.3, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -30X-100YPX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.3, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "ELF -30X-100YNX", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.3, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                'add this combo after other
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "EL_100+30", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                       curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega,
                                                       curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, 0))
            End If
            '
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC1", 1.4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC2", 1.2, 1.6, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC3", 1.2, 1.6, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC4", 1.2, 1.6, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC5", 1.2, 0.5, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC6", 1.2, 0.5, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC7", 1.2, 0.5, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC8", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            'change wind factors from 1.6 to 1.0 for AISC-10 and 7-16
            Dim windFactor As Double = 1.6
            Dim WL_factor As Double = 0.8
            If curJob.ASCE Like "*10" Or curJob.ASCE Like "*16" Then
                windFactor = 1.0
                WL_factor = 0.5
            End If
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC9", 1.2, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC10", 1.2, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC11", 1.2, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC12", 1.2, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC13", 1.2, 0, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC14", 1.2, 0, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC15", 1.2, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC16", 1.2, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC17", 1.2, 0.5, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC18", 1.2, 0.5, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC19", 1.2, 0.5, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC20", 1.2, 0.5, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC21", 0.9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC22", 0.9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC23", 1.0, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC24", 1.0, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC25", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC26", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC27", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC28", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC29", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC30", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC31", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC32", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC33", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC34", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC35", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC36", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

            If CreateELF10030 Then
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC37", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC38", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC39", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1))
                cnt += 1 : lstLcm.Add(New clsELFCombos(cnt, "SST_LC40", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1))
            End If
            '
            For Each lcm As clsELFCombos In lstLcm
                If Not LoadCombos.Contains(lcm.Name, StringComparer.CurrentCultureIgnoreCase) Then
                    If lcm.Name = "NL" Or lcm.Name = "WL" Or lcm.Name Like "EL*" Then
                        'add load combination
                        ret = mySapModel.RespCombo.Add(lcm.Name, 1)
                        '
                        If lcm.fNLX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "NLX", lcm.fNLX)
                        If lcm.fNLY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "NLY", lcm.fNLY)
                        '
                        If lcm.fW <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "W", lcm.fW)
                        'If lcm.fWY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "WY", lcm.fWY)
                        '
                        If lcm.fEQX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQX", lcm.fEQX)
                        If lcm.fEQXPY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXPY", lcm.fEQXPY)
                        If lcm.fEQXNY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXNY", lcm.fEQXNY)
                        '
                        If lcm.fEQY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQY", lcm.fEQY)
                        If lcm.fEQYPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYPX", lcm.fEQYPX)
                        If lcm.fEQYNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYNX", lcm.fEQYNX)
                        '
                        If lcm.fEQX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQX_D", lcm.fEQX_D)
                        If lcm.fEQXPY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXPY_D", lcm.fEQXPY_D)
                        If lcm.fEQXNY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXNY_D", lcm.fEQXNY_D)
                        '
                        If lcm.fEQY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQY_D", lcm.fEQY_D)
                        If lcm.fEQYPX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYPX_D", lcm.fEQYPX_D)
                        If lcm.fEQYNX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYNX_D", lcm.fEQYNX_D)
                        '
                        If CreateELF10030 Then
                            If lcm.fELF100XPY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 100XPY+30Y", lcm.fELF100XPY30Y)
                            If lcm.fELF100XNY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 100XNY+30Y", lcm.fELF100XNY30Y)
                            If lcm.fELF100XPY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 100XPY-30Y", lcm.fELF100XPY_30Y)
                            If lcm.fELF100XNY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 100XNY-30Y", lcm.fELF100XNY_30Y)
                            If lcm.fELF_100XPY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -100XPY+30Y", lcm.fELF_100XPY30Y)
                            If lcm.fELF_100XNY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -100XNY+30Y", lcm.fELF_100XNY30Y)
                            If lcm.fELF_100XPY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -100XPY-30Y", lcm.fELF_100XPY_30Y)
                            If lcm.fELF_100XNY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -100XNY-30Y", lcm.fELF_100XNY_30Y)
                            If lcm.fELF30X100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 30X+100YPX", lcm.fELF30X100YPX)
                            If lcm.fELF30X100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 30X+100YNX", lcm.fELF30X100YNX)
                            If lcm.fELF30X_100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 30X-100YPX", lcm.fELF30X_100YPX)
                            If lcm.fELF30X_100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF 30X-100YNX", lcm.fELF30X_100YNX)
                            If lcm.fELF_30X100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -30X+100YPX", lcm.fELF_30X100YPX)
                            If lcm.fELF_30X100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -30X+100YNX", lcm.fELF_30X100YNX)
                            If lcm.fELF_30X_100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -30X-100YPX", lcm.fELF_30X_100YPX)
                            If lcm.fELF_30X_100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "ELF -30X-100YNX", lcm.fELF_30X_100YNX)
                        End If
                    Else
                        'add load combination
                        ret = mySapModel.RespCombo.Add(lcm.Name, 0)
                        If lcm.fDL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "DL", lcm.fDL)
                        If lcm.fLL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "LL", lcm.fLL)
                        If lcm.fLR <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "LR", lcm.fLR)
                        If lcm.fSL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SL", lcm.fSL)
                        If lcm.fRL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "RL", lcm.fRL)
                        'LoadCombo
                        If lcm.fWL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "WL", lcm.fWL)
                        If lcm.fNL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "NL", lcm.fNL)
                        If lcm.fEL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL", lcm.fEL)
                        If lcm.fEL_Omega <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL_Omega", lcm.fEL_Omega)
                        If lcm.fEL_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL_D", lcm.fEL_D)
                        If lcm.fEL_Omega_130 <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL_100+30", lcm.fEL_Omega_130)
                    End If
                End If
            Next
            '
            Return "All load patterns, load cases and SST load combinations have been created"
        Catch ex As Exception
            Return "Error!"
        End Try
    End Function
    Public Function CreateMRSALoadConbination(ByRef curJob As clsMRSJob, Optional CreateMRSA10030 As Boolean = False, Optional keepLoadPatt As Boolean = True) As String
        Try
            Dim cnt As Integer
            Dim LoadCombos() As String
            Dim NoLCombos As Integer
            '
            Dim SS As Double, S1 As Double, siteclass As Char, TL, Ie, Rframe, Omega, Cd As Double
            Dim windSpeed, EleFactor, Kzt, GustFactor, Kd As Double
            Dim exposureType As String
            SS = curJob.Ss
            S1 = curJob.S1
            siteclass = curJob.SiteClass
            TL = curJob.TL
            Ie = curJob.I
            Rframe = curJob.R
            Omega = curJob.Omega
            Cd = curJob.Cd
            windSpeed = curJob.WindSpeed
            EleFactor = curJob.F_groundEle
            Kzt = curJob.Kzt
            GustFactor = curJob.F_gust
            Kd = curJob.Kd
            If curJob.ExposureType = "B" Then
                exposureType = 1
            ElseIf curJob.ExposureType = "C" Then
                exposureType = 2
            Else
                exposureType = 3
            End If
            '
            Dim myPDELTA As String = "PDELTA"
            'delete load pat
            Dim isOverwrite, isExist, isLocked As Boolean
            Dim isskips As Boolean = False
            Dim NoLPatts, NoLCaces As Integer
            Dim lockedNote As String
            Dim LoadCases(), LoadPatts() As String
            Dim MyLoadType() As String
            Dim MyLoadName() As String
            Dim MySF() As Double
            'Define function for Response Spectrum
            'ret = mySapModel.Func.FuncRS.
            '.SetNTC2008("SPEC", 3, 45.9, 12.6, 1, 3, 2, 50, 0.2, 2.4, 0.3, 3, 2, 1, 1, 5, 1)

            'Define Parameters
            Dim myListPatts As New SortedList(Of String, eLoadPatternType)

            myListPatts.Add("DL", eLoadPatternType.Dead)
            myListPatts.Add("LL", eLoadPatternType.Live)
            myListPatts.Add("LR", eLoadPatternType.Rooflive)
            myListPatts.Add("SL", eLoadPatternType.Snow)
            myListPatts.Add("RL", eLoadPatternType.Other)
            myListPatts.Add("W", eLoadPatternType.Wind)
            myListPatts.Add("NLX", eLoadPatternType.Notional)
            myListPatts.Add("NLY", eLoadPatternType.Notional)
            'Approximate Period
            myListPatts.Add("EQX", eLoadPatternType.Quake)
            myListPatts.Add("EQXPY", eLoadPatternType.Quake)
            myListPatts.Add("EQXNY", eLoadPatternType.Quake)

            myListPatts.Add("EQY", eLoadPatternType.Quake)
            myListPatts.Add("EQYPX", eLoadPatternType.Quake)
            myListPatts.Add("EQYNX", eLoadPatternType.Quake)
            'Program Calculated
            myListPatts.Add("EQX_D", IIf(curJob.isEtab, 37, eLoadPatternType.Quake))
            myListPatts.Add("EQXPY_D", IIf(curJob.isEtab, 37, eLoadPatternType.Quake))
            myListPatts.Add("EQXNY_D", IIf(curJob.isEtab, 37, eLoadPatternType.Quake))

            myListPatts.Add("EQY_D", IIf(curJob.isEtab, 37, eLoadPatternType.Quake))
            myListPatts.Add("EQYPX_D", IIf(curJob.isEtab, 37, eLoadPatternType.Quake))
            myListPatts.Add("EQYNX_D", IIf(curJob.isEtab, 37, eLoadPatternType.Quake))
            '
            Dim myLcms() As String
            'Dim myMRSA10030LoadCases() As String
            Dim myMRSA_SPECLoadCase() As String = {"SPECX", "SPECX_PY", "SPECX_NY",
                                                   "SPECY", "SPECY_PX", "SPECY_NX",
                                                   "SPECX_D", "SPECX_PY_D", "SPECX_NY_D",
                                                   "SPECY_D", "SPECY_PX_D", "SPECY_NX_D"}
            If Not CreateMRSA10030 Then
                myLcms = {"NL", "WL", "EL", "EL_Omega", "EL_D",
                          "SST_LC1", "SST_LC2", "SST_LC3", "SST_LC4", "SST_LC5", "SST_LC6", "SST_LC7",
                          "SST_LC8",
                          "SST_LC9", "SST_LC10", "SST_LC11", "SST_LC12", "SST_LC13", "SST_LC14", "SST_LC15", "SST_LC16", "SST_LC17", "SST_LC18", "SST_LC19", "SST_LC20", "SST_LC21", "SST_LC22", "SST_LC23", "SST_LC24",
                           "SST_LC25", "SST_LC26", "SST_LC27", "SST_LC28",
                           "SST_LC29", "SST_LC30", "SST_LC31", "SST_LC32",
                           "SST_LC33", "SST_LC34", "SST_LC35""SST_LC36"}
            Else
                myLcms = {"NL", "WL", "EL", "EL_Omega", "EL_D", "EL_100+30",
                          "MRSA 100XPY+30Y", "MRSA 100XNY+30Y", "MRSA 100XPY-30Y", "MRSA 100XNY-30Y",
                          "MRSA -100XPY+30Y", "MRSA -100XNY+30Y", "MRSA -100XPY-30Y", "MRSA -100XNY-30Y", "MRSA 30X+100YPX", "MRSA 30X+100YNX",
                          "MRSA 30X-100YPX", "MRSA 30X-100YNX", "MRSA -30X+100YPX", "MRSA -30X+100YNX", "MRSA -30X-100YPX", "MRSA -30X-100YNX",
                          "SST_LC1", "SST_LC2", "SST_LC3", "SST_LC4", "SST_LC5", "SST_LC6", "SST_LC7",
                          "SST_LC8",
                          "SST_LC9", "SST_LC10", "SST_LC11", "SST_LC12", "SST_LC13", "SST_LC14", "SST_LC15", "SST_LC16", "SST_LC17", "SST_LC18", "SST_LC19", "SST_LC20", "SST_LC21", "SST_LC22", "SST_LC23", "SST_LC24",
                           "SST_LC25", "SST_LC26", "SST_LC27", "SST_LC28",
                           "SST_LC29", "SST_LC30", "SST_LC31", "SST_LC32",
                           "SST_LC33", "SST_LC34", "SST_LC35", "SST_LC36",
                           "SST_LC37", "SST_LC38", "SST_LC39", "SST_LC40"}
            End If
            '
            ret = mySapModel.LoadPatterns.GetNameList(NoLPatts, LoadPatts)
            ret = mySapModel.LoadCases.GetNameList(NoLCaces, LoadCases)
            ret = mySapModel.RespCombo.GetNameList(NoLCombos, LoadCombos)
            If LoadPatts Is Nothing Then LoadPatts = {"DefaultLoadPath"}
            If LoadCases Is Nothing Then LoadCases = {"DefaultLoadCases"}
            If LoadCombos Is Nothing Then LoadCombos = {"DefaultLoadCombos"}
            'Check if Any existed
            For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                'Load patt
                If Not keepLoadPatt Then
                    If LoadPatts.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                        isExist = True
                        Exit For
                    End If
                End If
                'Load case
                If LoadCases.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                    isExist = True
                    Exit For
                End If
            Next
            '''Check if LoadCombos existed
            If isOverwrite = False Then
                For Each lcm As String In myLcms
                    If LoadCombos.Contains(lcm, StringComparer.CurrentCultureIgnoreCase) Then
                        isExist = True
                        Exit For
                    End If
                Next

            End If
            'Question
            isLocked = mySapModel.GetModelIsLocked
            '
            If isExist = True Then
                If MsgBox("Some LoadPatterns, LoadCases or Load Combinations have already existed." & If(isLocked, "Also, This model is locked.", "") &
                          vbLf & "Would you like to" & If(isLocked, "unlock and", "") & " overwrite all of them?",
                                      MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirm") = MsgBoxResult.Yes Then
                    mySapModel.SetModelIsLocked(False)
                    isOverwrite = True
                Else
                    Return "Cancelled"
                End If
            End If
            '-------------------------------------------------------------------DELETE
            If isOverwrite Then
                '------------------------------------Delete Load combonations
                For Each lcm As String In myLcms
                    If LoadCombos.Contains(lcm, StringComparer.CurrentCultureIgnoreCase) Then
                        'ret = Delete_load(LC, 3)
                        ret = mySapModel.RespCombo.Delete(lcm)
                    End If
                Next
                '
                If Not CreateMRSA10030 Then
                    ret = mySapModel.RespCombo.Delete("SST_LC37")
                    ret = mySapModel.RespCombo.Delete("SST_LC38")
                    ret = mySapModel.RespCombo.Delete("SST_LC39")
                    ret = mySapModel.RespCombo.Delete("SST_LC40")
                End If
                '------------------------------------Delete load case same name with Load partten
                For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                    If LoadCases.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                        'ret = Delete_load(curLPatt.Key, 2)
                        mySapModel.LoadCases.Delete(curLPatt.Key)
                    End If
                Next
                '"PDELTA" case
                'ret = Delete_load(myPDELTA, 2)
                ret = mySapModel.LoadCases.Delete(myPDELTA)
                '------------------------------------Delete Spec load case
                For Each curCase As String In myMRSA_SPECLoadCase
                    If LoadCases.Contains(curCase, StringComparer.CurrentCultureIgnoreCase) Then
                        'ret = Delete_load(curCase, 2)
                        ret = mySapModel.LoadCases.Delete(curCase)
                    End If
                Next
                '------------------------------------Delete Load Pattern
                If Not keepLoadPatt Then
                    For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                        If LoadPatts.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                            'ret = Delete_load(curLPatt.Key, 1)
                            ret = mySapModel.LoadPatterns.Delete(curLPatt.Key)
                        End If
                    Next
                End If
            End If
            '-------------------------------------------------------------------CREATE
            'reset array
            LoadPatts = {}
            LoadCases = {}
            LoadCombos = {}
            ''reload all info
            ret = mySapModel.LoadPatterns.GetNameList(NoLPatts, LoadPatts)
            ret = mySapModel.LoadCases.GetNameList(NoLCaces, LoadCases)
            ret = mySapModel.RespCombo.GetNameList(NoLCombos, LoadCombos)
            If LoadPatts Is Nothing Then LoadPatts = {"DefaultLoadPath"}
            If LoadCases Is Nothing Then LoadCases = {"DefaultLoadCases"}
            If LoadCombos Is Nothing Then LoadCombos = {"DefaultLoadCombos"}
            'Create Load Patt and Load case
            For Each curLPatt As KeyValuePair(Of String, eLoadPatternType) In myListPatts
                'Load Pattern
                If Not keepLoadPatt Then
                    If Not LoadPatts.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                        If curLPatt.Key = "DL" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 1)
                        ElseIf curLPatt.Key Like "NL*" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 0)
                            'Need add Auto for Notional loads
                        ElseIf curLPatt.Key Like "EQ*_D" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 0)
                            ret = mySapModel.LoadPatterns.AutoSeismic.SetIBC2006(curLPatt.Key, 1, 0.05, 2, 0, 0, False, 0, 0, Rframe, Omega, Cd, Ie, 2, 37.8652, -122.257, 94704, SS, S1, TL, Asc(siteclass) - 64, 1, 1)
                        ElseIf curLPatt.Key Like "EQ*" Then
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value, 0)
                            ret = mySapModel.LoadPatterns.AutoSeismic.SetIBC2006(curLPatt.Key, 1, 0.05, 1, 0, 0, False, 0, 0, Rframe, Omega, Cd, Ie, 2, 37.8652, -122.257, 94704, SS, S1, TL, Asc(siteclass) - 64, 1, 1)
                        Else
                            ret = mySapModel.LoadPatterns.Add(curLPatt.Key, curLPatt.Value)
                        End If
                    End If
                End If
                'Load Case
                If Not LoadCases.Contains(curLPatt.Key, StringComparer.CurrentCultureIgnoreCase) Then
                    ret = mySapModel.LoadCases.StaticLinear.SetCase(curLPatt.Key)
                    'set load data
                    ReDim MyLoadType(0)
                    ReDim MyLoadName(0)
                    ReDim MySF(0)
                    MyLoadType(0) = "Load"
                    MyLoadName(0) = curLPatt.Key
                    MySF(0) = 1
                    ret = mySapModel.LoadCases.StaticLinear.SetLoads(curLPatt.Key, 1, MyLoadType, MyLoadName, MySF)
                End If
            Next

            'Create "PDELTA" case
            If Not LoadCases.Contains(myPDELTA, StringComparer.CurrentCultureIgnoreCase) Then
                'add PDELTA load
                ret = mySapModel.LoadCases.StaticNonlinear.SetCase(myPDELTA)
                'set load data
                ReDim MyLoadType(4)
                ReDim MyLoadName(4)
                ReDim MySF(4)
                MyLoadType(0) = "Load"
                MyLoadName(0) = "DL"
                MySF(0) = 1
                MyLoadType(1) = "Load"
                MyLoadName(1) = "LL"
                MySF(1) = 0.5
                MyLoadType(2) = "Load"
                MyLoadName(2) = "LR"
                MySF(2) = 0.5
                MyLoadType(3) = "Load"
                MyLoadName(3) = "SL"
                MySF(3) = 0.5
                'Rainload
                MyLoadType(4) = "Load"
                MyLoadName(4) = "RL"
                MySF(4) = 0.5
                ret = mySapModel.LoadCases.StaticNonlinear.SetLoads(myPDELTA, 5, MyLoadType, MyLoadName, MySF)
                ret = mySapModel.LoadCases.StaticNonlinear.SetGeometricNonlinearity(myPDELTA, 1)
            End If
            ' 'Set Non-Linear case “PDelta” for nonlinear stiffness.
            Dim EQCases() As String = {"EQX", "EQXPY", "EQXNY", "EQY", "EQYPX", "EQYNX", "EQX_D", "EQXPY_D", "EQXNY_D", "EQY_D", "EQYPX_D", "EQYNX_D"}
            '
            For Each curCase As String In EQCases
                ret = mySapModel.LoadCases.StaticLinear.SetInitialCase(curCase, myPDELTA)
            Next
            '' 'Set Non-Linear case “PDelta” for Wind.
            ret = mySapModel.LoadCases.StaticLinear.SetInitialCase("W", myPDELTA)
            '
            Dim lstMRSASPECLoadCases As New List(Of clsRSLoadCase)
            Dim initSF As Double = 386.09
            cnt = 0
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECX", "U1", "SPEC", initSF, 0))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECX_PY", "U1", "SPEC", initSF, 0.05))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECX_NY", "U1", "SPEC", initSF, -0.05))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECY", "U2", "SPEC", initSF, 0))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECY_PX", "U2", "SPEC", initSF, 0.05))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECY_NX", "U2", "SPEC", initSF, -0.05))
            '
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECX_D", "U1", "SPEC", initSF, 0))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECX_PY_D", "U1", "SPEC", initSF, 0.05))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECX_NY_D", "U1", "SPEC", initSF, -0.05))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECY_D", "U2", "SPEC", initSF, 0))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECY_PX_D", "U2", "SPEC", initSF, 0.05))
            cnt += 1 : lstMRSASPECLoadCases.Add(New clsRSLoadCase(cnt, "SPECY_NX_D", "U2", "SPEC", initSF, -0.05))

            '''Add MRSA function
            ''ret = mySapModel.Func.FuncRS.SetNTC2008("SPEC", 1, 45.9, 12.6, 1, 3, 2, 50, 0.2, 2.4, 0.3, 3, 2, 1, 1, 5, 1)
            'Add SPEC load case
            Dim RSALoadName() As String
            Dim RSAFunction() As String
            Dim RSASF() As Double
            Dim RSACSys() As String
            Dim RSAAng() As Double

            For Each MRSA_SPECcase As clsRSLoadCase In lstMRSASPECLoadCases
                If Not LoadCases.Contains(MRSA_SPECcase.Name, StringComparer.CurrentCultureIgnoreCase) Then
                    ret = mySapModel.LoadCases.ResponseSpectrum.SetCase(MRSA_SPECcase.Name)
                    ReDim RSALoadName(0)
                    ReDim RSAFunction(0)
                    ReDim RSASF(0)
                    ReDim RSACSys(0)
                    ReDim RSAAng(0)
                    RSALoadName(0) = MRSA_SPECcase.LoadName
                    RSAFunction(0) = MRSA_SPECcase.FunctName
                    RSASF(0) = MRSA_SPECcase.ScaleFactor
                    RSACSys(0) = "GLOBAL"
                    RSAAng(0) = 0
                    ret = mySapModel.LoadCases.ResponseSpectrum.SetLoads(MRSA_SPECcase.Name, 1, RSALoadName, RSAFunction, RSASF, RSACSys, RSAAng)
                    ret = mySapModel.LoadCases.ResponseSpectrum.SetEccentricity(MRSA_SPECcase.Name, MRSA_SPECcase.EccRatio)

                    'mySapModel.LoadCases.ResponseSpectrum.GetLoads()
                End If
            Next

            'Create Load combination
            Dim lstLcm As New List(Of clsMRSACombos)
            cnt = 0
            '
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "NL", 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "WL", 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "EL", curJob.Rho, curJob.Rho, curJob.Rho, curJob.RhoY, curJob.RhoY, curJob.RhoY, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "EL_Omega", curJob.Omega, curJob.Omega, curJob.Omega, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "EL_D", 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1))
            If CreateMRSA10030 Then
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 100XPY+30Y", 0, 1, 0, 0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 100XNY+30Y", 0, 0, 1, 0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 100XPY-30Y", 0, 1, 0, -0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 100XNY-30Y", 0, 0, 1, -0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -100XPY+30Y", 0, -1, 0, 0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -100XNY+30Y", 0, 0, -1, 0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -100XPY-30Y", 0, -1, 0, -0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -100XNY-30Y", 0, 0, -1, -0.3, 0, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 30X+100YPX", 0.3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 30X+100YNX", 0.3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 30X-100YPX", 0.3, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA 30X-100YNX", 0.3, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -30X+100YPX", -0.3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -30X+100YNX", -0.3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -30X-100YPX", -0.3, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "MRSA -30X-100YNX", -0.3, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0))
                '
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "EL_100+30", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                        curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega, curJob.Omega,
                                                        curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, curJob.OmegaY, 0))
            End If
            '
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC1", 1.4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC2", 1.2, 1.6, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC3", 1.2, 1.6, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC4", 1.2, 1.6, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC5", 1.2, 0.5, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC6", 1.2, 0.5, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC7", 1.2, 0.5, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC8", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            'change wind factors from 1.6 to 1.0 for AISC-10 and 7-16
            Dim windFactor As Double = 1.6
            Dim WL_factor As Double = 0.8
            If curJob.ASCE Like "*10" Or curJob.ASCE Like "*16" Then
                windFactor = 1.0
                WL_factor = 0.5
            End If
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC9", 1.2, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC10", 1.2, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC11", 1.2, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC12", 1.2, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC13", 1.2, 0, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC14", 1.2, 0, 0, 0, 1.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -WL_factor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC15", 1.2, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC16", 1.2, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC17", 1.2, 0.5, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC18", 1.2, 0.5, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC19", 1.2, 0.5, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC20", 1.2, 0.5, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC21", 0.9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC22", 0.9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -windFactor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC23", 1.0, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC24", 1.0, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC25", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC26", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC27", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC28", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC29", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC30", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC31", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC32", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            '
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC33", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC34", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC35", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
            cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC36", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

            If CreateMRSA10030 Then
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC37", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC38", 1.2 + 0.2 * curJob.Sds, curJob.f1, 0, curJob.f2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC39", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1))
                cnt += 1 : lstLcm.Add(New clsMRSACombos(cnt, "SST_LC40", 0.9 - (0.2 * curJob.Sds), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1))
            End If
            '
            For Each lcm As clsMRSACombos In lstLcm
                If Not LoadCombos.Contains(lcm.Name, StringComparer.CurrentCultureIgnoreCase) Then
                    If lcm.Name = "NL" Or lcm.Name = "WL" Or lcm.Name Like "EL*" Or lcm.Name Like "MRSA*" Then
                        'add load combination
                        ret = mySapModel.RespCombo.Add(lcm.Name, 1)
                        '
                        If lcm.fNLX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "NLX", lcm.fNLX)
                        If lcm.fNLY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "NLY", lcm.fNLY)
                        '
                        If lcm.fW <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "W", lcm.fW)
                        '
                        If lcm.fEQX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQX", lcm.fEQX)
                        If lcm.fEQXPY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXPY", lcm.fEQXPY)
                        If lcm.fEQXNY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXNY", lcm.fEQXNY)
                        '
                        If lcm.fEQY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQY", lcm.fEQY)
                        If lcm.fEQYPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYPX", lcm.fEQYPX)
                        If lcm.fEQYNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYNX", lcm.fEQYNX)
                        '
                        If lcm.fEQX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQX_D", lcm.fEQX_D)
                        If lcm.fEQXPY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXPY_D", lcm.fEQXPY_D)
                        If lcm.fEQXNY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQXNY_D", lcm.fEQXNY_D)
                        '
                        If lcm.fEQY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQY_D", lcm.fEQY_D)
                        If lcm.fEQYPX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYPX_D", lcm.fEQYPX_D)
                        If lcm.fEQYNX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "EQYNX_D", lcm.fEQYNX_D)
                        '
                        If lcm.fSPECX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECX", lcm.fSPECX)
                        If lcm.fSPECX_PY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECX_PY", lcm.fSPECX_PY)
                        If lcm.fSPECX_NY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECX_NY", lcm.fSPECX_NY)
                        If lcm.fSPECY <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECY", lcm.fSPECY)
                        If lcm.fSPECY_PX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECY_PX", lcm.fSPECY_PX)
                        If lcm.fSPECY_NX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECY_NX", lcm.fSPECY_NX)
                        '
                        If lcm.fSPECX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECX_D", lcm.fSPECX_D)
                        If lcm.fSPECX_PY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECX_PY_D", lcm.fSPECX_PY_D)
                        If lcm.fSPECX_NY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECX_NY_D", lcm.fSPECX_NY_D)
                        If lcm.fSPECY_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECY_D", lcm.fSPECY_D)
                        If lcm.fSPECY_PX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECY_PX_D", lcm.fSPECY_PX_D)
                        If lcm.fSPECY_NX_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SPECY_NX_D", lcm.fSPECY_NX_D)
                        '
                        If CreateMRSA10030 Then
                            If lcm.fMRSA100XNY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 100XPY+30Y", lcm.fMRSA100XNY30Y)
                            If lcm.fMRSA100XNY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 100XNY+30Y", lcm.fMRSA100XNY_30Y)
                            If lcm.fMRSA100XPY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 100XPY-30Y", lcm.fMRSA100XPY30Y)
                            If lcm.fMRSA100XPY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 100XNY-30Y", lcm.fMRSA100XPY_30Y)
                            If lcm.fMRSA30X100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -100XPY+30Y", lcm.fMRSA30X100YNX)
                            If lcm.fMRSA30X100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -100XNY+30Y", lcm.fMRSA30X100YPX)
                            If lcm.fMRSA30X_100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -100XPY-30Y", lcm.fMRSA30X_100YNX)
                            If lcm.fMRSA30X_100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -100XNY-30Y", lcm.fMRSA30X_100YPX)
                            If lcm.fMRSA_100XNY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 30X+100YPX", lcm.fMRSA_100XNY30Y)
                            If lcm.fMRSA_100XNY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 30X+100YNX", lcm.fMRSA_100XNY_30Y)
                            If lcm.fMRSA_100XPY30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 30X-100YPX", lcm.fMRSA_100XPY30Y)
                            If lcm.fMRSA_100XPY_30Y <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA 30X-100YNX", lcm.fMRSA_100XPY_30Y)
                            If lcm.fMRSA_30X100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -30X+100YPX", lcm.fMRSA_30X100YNX)
                            If lcm.fMRSA_30X100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -30X+100YNX", lcm.fMRSA_30X100YPX)
                            If lcm.fMRSA_30X_100YNX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -30X-100YPX", lcm.fMRSA_30X_100YNX)
                            If lcm.fMRS_30X_100YPX <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "MRSA -30X-100YNX", lcm.fMRS_30X_100YPX)
                        End If
                    Else
                        'add load combination
                        ret = mySapModel.RespCombo.Add(lcm.Name, 0)
                        If lcm.fDL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "DL", lcm.fDL)
                        If lcm.fLL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "LL", lcm.fLL)
                        If lcm.fLR <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "LR", lcm.fLR)
                        If lcm.fSL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "SL", lcm.fSL)
                        If lcm.fRL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCase, "RL", lcm.fRL)
                        'LoadCombo
                        If lcm.fWL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "WL", lcm.fWL)
                        If lcm.fNL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "NL", lcm.fNL)
                        If lcm.fEL <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL", lcm.fEL)
                        If lcm.fEL_D <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL_D", lcm.fEL_D)
                        If lcm.fEL_Omega <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL_Omega", lcm.fEL_Omega)
                        If lcm.fEL_Omega_130 <> 0 Then ret = mySapModel.RespCombo.SetCaseList(lcm.Name, eCNameType.LoadCombo, "EL_100+30", lcm.fEL_Omega_130)
                    End If
                End If
            Next
            '
            Return "All load patterns, load cases and SST load combinations have been created"
        Catch ex As Exception
            Return "Error!"
        End Try
    End Function
    'Create MRSA and MRSA10030 load combination -CANADA
    Public Function Delete_load_odd(name As String, type As Integer) As Long
        'type=1 as load pattern,2 as load case, 3 as load combination
        Try
            ''Dim i As Long
            ''Dim NumberNames As Integer
            ''Dim MyName() As String
            Select Case type
                Case 1
                    ''ret = mySapModel.LoadPatterns.GetNameList(NumberNames, MyName)
                    ''If Array.Exists(MyName, Function(element)
                    ''                            Return element.Equals(name)
                    ''                        End Function) Then
                    ''    ret = mySapModel.LoadPatterns.Delete(name)
                    ''    Return ret
                    ''End If
                    Return mySapModel.LoadPatterns.Delete(name)
                Case 2
                    ''ret = mySapModel.LoadCases.GetNameList(NumberNames, MyName)
                    ''If Array.Exists(MyName, Function(element)
                    ''                            Return element.Equals(name)
                    ''                        End Function) Then
                    ''    ret = mySapModel.LoadCases.Delete(name)
                    ''    Return ret
                    ''End If
                    Return mySapModel.LoadCases.Delete(name)
                Case Else
                    ''ret = mySapModel.RespCombo.GetNameList(NumberNames, MyName)
                    ''If Array.Exists(MyName, Function(element)
                    ''                            Return element.Equals(name)
                    ''                        End Function) Then
                    ''    ret = mySapModel.RespCombo.Delete(name)
                    ''    Return ret
                    ''End If
                    Return mySapModel.RespCombo.Delete(name)
            End Select
            Return 1
        Catch ex As Exception
            Return 1
        End Try
    End Function

    Sub FinishPlugIn()
        ISapPlugin.Finish(0)
    End Sub

    Function GetLineDrift(beamname As String, JointName As String, JointName_bot As String, ByRef Displ#,
                          ByRef Drift#, isSeismic As Boolean) As Long
        Dim reUnit As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))
            Dim isX As Boolean
            ' ''name=beam name
            Dim Point1 As String = "", Point2 As String = "", i As Long
            isX = False
            ' Get Point1=I-End, Point2=J-End
            ret = mySapModel.FrameObj.GetPoints(beamname, Point1, Point2)
            Dim x1, y1, z1, x2, y2, z2 As Double
            ret = mySapModel.PointObj.GetCoordCartesian(Point1, x1, y1, z1)
            ret = mySapModel.PointObj.GetCoordCartesian(Point2, x2, y2, z2)
            '
            If Math.Abs(x2 - x1) > Math.Abs(y2 - y1) Then isX = True

            'get displcement
            'clear all case and combo output selections
            Dim NumberResults As Long
            Dim lstLoadCase As New List(Of String)
            Dim LoadCase() As String
            Dim StepType() As String
            Dim StepNum() As Double
            Dim NumberNames As Long
            Dim Obj As String()
            Dim Elm As String()
            Dim U1 As Double()
            Dim U2 As Double()
            Dim U3 As Double()
            Dim R1 As Double()
            Dim R2 As Double()
            Dim R3 As Double()

            Dim jDriftX As Double()
            Dim jDriftY As Double()
            Dim disptop As Double = 0, dispbot As Double = 0
            ret = mySapModel.Results.Setup.DeselectAllCasesAndCombosForOutput
            If isSeismic Then
                lstLoadCase.Clear()
                For i = 1 To 7
                    lstLoadCase.Add("SST_LC" & i)
                Next
                For i = 29 To 32
                    lstLoadCase.Add("SST_LC" & i)
                Next
            Else
                lstLoadCase.Clear()
                lstLoadCase.Add("SST_LC23")
                lstLoadCase.Add("SST_LC24")
            End If
            '
            For i = 0 To lstLoadCase.Count - 1
                If lstLoadCase(i) <> "" Then
                    ret = mySapModel.Results.Setup.SetComboSelectedForOutput(lstLoadCase(i))
                End If
            Next
            'top
            ret = mySapModel.Results.JointDispl(JointName, eItemTypeElm.ObjectElm, NumberResults, Obj, Elm, LoadCase, StepType, StepNum, U1, U2, U3, R1, R2, R3)
            If isX Then
                disptop = Math.Max(Math.Abs(U1.Max), Math.Abs(U1.Min))
            Else
                disptop = Math.Max(Math.Abs(U2.Max), Math.Abs(U2.Min))
            End If
            'bottom
            ret = mySapModel.Results.JointDispl(JointName_bot, eItemTypeElm.ObjectElm, NumberResults, Obj, Elm, LoadCase, StepType, StepNum, U1, U2, U3, R1, R2, R3)
            If ret <> 0 Then
                'Invalid node below per user input
                Displ = 0
                Drift = 0
                Return -2
            End If
            '
            If isX Then
                dispbot = Math.Max(Math.Abs(U1.Max), Math.Abs(U1.Min))
            Else
                dispbot = Math.Max(Math.Abs(U2.Max), Math.Abs(U2.Min))
            End If

            Displ = Math.Round(Math.Abs(disptop), 3)
            Drift = Math.Round(Math.Abs(Math.Abs(disptop) - Math.Abs(dispbot)), 3)
            Return 0
        Catch ex As Exception
            Return 1
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
    End Function

    Function GetStoryDrift2(ByRef Story() As String, ByRef displX() As Double, ByRef displY() As Double) As Long
        Dim reUnit As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))
            Dim i As Long, j As Long
            Dim LoadCase() As String
            Dim X As Double()
            Dim Y As Double()
            Dim Z As Double()
            Dim NumberNames As Long
            ret = mySapModel.Results.Setup.DeselectAllCasesAndCombosForOutput
            'get combo names
            ret = mySapModel.RespCombo.GetNameList(NumberNames, LoadCase)
            'set case and combo output selections
            For i = 0 To NumberNames - 1
                ret = mySapModel.Results.Setup.SetComboSelectedForOutput(LoadCase(i))
            Next
            Dim NumberResults As Integer

            Dim StepType() As String
            Dim StepNum() As Double
            Dim jstory() As String
            Dim jname() As String
            Dim label() As String
            Dim DisplacementX As Double()
            Dim DisplacementY As Double()
            Dim jDriftX As Double()
            Dim jDriftY As Double()

            ret = mySapModel.Results.JointDrifts(NumberResults, jstory, label, jname, LoadCase, StepType, StepNum, DisplacementX, DisplacementY, jDriftX, jDriftY)
            If ret = 1 Then Return 1
            ReDim Story(1)
            ReDim displX(1)
            ReDim displY(i)
            Dim nstory As Long = 0
            Story(nstory) = jstory(0)
            For i = 0 To NumberResults - 1
                If jstory(i) = Story(nstory) Then
                    displX(nstory) = DisplacementX(i)
                    displY(nstory) = DisplacementY(i)
                    j = i
                    While jstory(j) = Story(nstory)
                        If Math.Abs(DisplacementX(j)) > Math.Abs(displX(nstory)) Then displX(nstory) = DisplacementX(j)
                        If Math.Abs(DisplacementY(j)) > Math.Abs(displY(nstory)) Then displY(nstory) = DisplacementY(j)
                        j = j + 1
                        If j = NumberResults - 1 Then Exit While
                    End While

                    If j < NumberResults - 1 Then
                        nstory = nstory + 1
                        ReDim Preserve Story(nstory + 1)
                        ReDim Preserve displX(nstory + 1)
                        ReDim Preserve displY(nstory + 1)
                        Story(nstory) = jstory(j)
                        i = j - 1
                    Else
                        i = j
                    End If
                End If
            Next
            Return 0
        Catch ex As Exception
            Return 1
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
    End Function
    Function ReAssignMembersize(stlFrames As SortedList(Of String, String)) As Long
        Try
            'unlock model if model is locking
            If mySapModel.GetModelIsLocked() = True Then mySapModel.SetModelIsLocked(False)
            ' Check if size are existing
            Dim NumberNames As Integer
            Dim MyName() As String
            'get frame property names
            ret = mySapModel.PropFrame.GetNameList(NumberNames, MyName)
            For Each item As KeyValuePair(Of String, String) In stlFrames
                If MyName.Contains(item.Value) = False Then
                    'import new frame section property
                    ret = mySapModel.PropFrame.ImportProp(item.Value, "A992Fy50", "AISC14.xml", item.Value)
                End If
                ret = mySapModel.FrameObj.SetSection(item.Key, item.Value)
            Next
            '
            ret = mySapModel.View.RefreshView()
            Return 0
        Catch ex As Exception
            Return 1
        End Try
    End Function

    Sub LoadFrameSections(ByRef curJob As clsMRSJob)
        For Each sec As Wsec In curJob.AllWsections.Wsections
            ret = mySapModel.PropFrame.ImportProp(sec.Size, "A992Fy50", "AISC14.xml", sec.Size)
        Next
    End Sub

    Function IsExistLcm35() As Boolean
        Try
            Dim NumberNames As Long
            Dim LoadCase() As String
            Dim Selected As Boolean
            Dim LcmDesign As String
            'ret = mySapModel.RespCombo.AddDesignDefaultCombos(False, False, False, False)
            'select combos for steel strength design
            ret = mySapModel.RespCombo.GetNameList(NumberNames, LoadCase)
            Return LoadCase.Contains("SST_LC40") '37-40
        Catch
            Return False
        End Try
    End Function

    Function SetDesignPref(ByRef curJob As clsMRSJob)
        '''start steel 
        ''ret = mySapModel.DesignSteel.SetCode(curJob.DesignCode)
        ''If curJob.DesignCode Like "*16" Then
        ''ret = mySapModel.DesignSteel.AISC360_16.SetPreference(1, 3)
        ''    ''3 = Design provision
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(3, 1) '1 = LRFD; 2=ASD
        ''    ''set analysis method
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(27, 2) '2-=effective length
        ''    ''set Stiffness Reduction Method
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(29, 3) '3 = No Modification
        ''    ''30 = Importance Factor
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(30, curJob.I)
        ''    ''31 = Design System Rho
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(31, curJob.Rho)
        ''    ''32 = Design System Sds
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(32, curJob.Sds)
        ''    ''33 = Design System R
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(33, curJob.R)
        ''    ''34 =  Design System Omega0
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(34, curJob.Omega)
        ''    ''35 = Design System Cd
        ''    'ret = mySapModel.DesignSteel.AISC360_16.SetPreference(35, curJob.Cd)
        ''ElseIf curJob.DesignCode Like "*10" Then
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(1, 3)
        ''    ''3 = Design provision
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(3, 1) '1 = LRFD; 2=ASD
        ''    ''set analysis method
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(27, 2) '2-=effective length
        ''    ''set Stiffness Reduction Method
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(29, 3) '3 = No Modification
        ''    ''30 = Importance Factor
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(30, curJob.I)
        ''    ''31 = Design System Rho
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(31, curJob.Rho)
        ''    ''32 = Design System Sds
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(32, curJob.Sds)
        ''    ''33 = Design System R
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(33, curJob.R)
        ''    ''34 =  Design System Omega0
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(34, curJob.Omega)
        ''    ''35 = Design System Cd
        ''    'ret = mySapModel.DesignSteel.AISC360_10.SetPreference(35, curJob.Cd)
        ''ElseIf curJob.DesignCode Like "*05" Then
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(1, 3)
        ''    '3 = Design provision
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(3, 1) '1 = LRFD; 2=ASD
        ''    'set analysis method
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(27, 2) '2-=effective length
        ''    'set Stiffness Reduction Method
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(29, 3) '3 = No Modification
        ''    '30 = Importance Factor
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(30, curJob.I)
        ''    '31 = Design System Rho
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(31, curJob.Rho)
        ''    '32 = Design System Sds
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(32, curJob.Sds)
        ''    '33 = Design System R
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(33, curJob.R)
        ''    '34 =  Design System Omega0
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(34, curJob.Omega)
        ''    '35 = Design System Cd
        ''    ret = mySapModel.DesignSteel.AISC360_05_IBC2006.SetPreference(35, curJob.Cd)
        ''End If
    End Function
    Function RunSteelDesign(ByRef curJob As clsMRSJob, isBeam As Boolean) As Long
        Try
            Dim NumberNames As Long
            Dim LoadCase() As String
            Dim Selected As Boolean
            Dim LcmDesign As String
            'ret = mySapModel.RespCombo.AddDesignDefaultCombos(False, False, False, False)

            'select combos for steel strength design
            ret = mySapModel.RespCombo.GetNameList(NumberNames, LoadCase)
            '
            Dim lcmNo As Integer
            Dim i As Long
            'select combos for steel strength design
            For i = 0 To NumberNames - 1
                'Only get SST load combos
                If Not LoadCase(i) Like "SST_LC*" Then Continue For
                Integer.TryParse(LoadCase(i).Remove(0, "SST_LC".Length), lcmNo)
                '
                If isBeam Then
                    Selected = lcmNo <> 8 AndAlso lcmNo <> 23 AndAlso lcmNo <> 24 AndAlso lcmNo <> 25 AndAlso lcmNo <> 26 AndAlso lcmNo <> 27 AndAlso lcmNo <> 28 AndAlso lcmNo <= 36
                Else
                    Selected = lcmNo <> 8 AndAlso lcmNo <> 23 AndAlso lcmNo <> 24 AndAlso lcmNo <> 25 AndAlso lcmNo <> 26 AndAlso lcmNo <> 27 AndAlso lcmNo <> 28
                End If
                'Select combo
                ret = ret + mySapModel.DesignSteel.SetComboStrength(LoadCase(i), Selected)
            Next
            'Not use but required by etabs
            ret = ret + mySapModel.DesignSteel.SetComboDeflection("EL", True) 'SST_LC22D
            '
            ret = mySapModel.DesignSteel.SetCode(curJob.DesignCode)
            'Run design
            ret = ret + mySapModel.DesignSteel.StartDesign

            Return 0
        Catch ex As Exception
            Return 1
        End Try
    End Function
    Function getBeamDesignResult(ID As String, ByRef Lcm As String, ByRef Mu As Double, ByRef MuMid As Double, ByRef AxialDCR As Double, ByRef B_maj As Double, ByRef B_min As Double, ByRef PMM As Double) As Long
        'Reset DCR
        Mu = 0 : MuMid = 0 : AxialDCR = 0 : B_maj = 0 : B_min = 0 : PMM = 0
        '
        Dim reUnit As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))

            Dim NumberItems As Integer
            Dim FrameType As String()
            Dim DesignSect As String()
            Dim Status As String()
            Dim PMMCombo As String()
            Dim PMMRatio As Double()
            Dim PRatio As Double()
            Dim MMajRatio As Double()
            Dim MMinRatio As Double()
            Dim VMajCombo As String()
            Dim VMajRatio As Double()
            Dim VMinCombo As String()
            Dim VMinRatio As Double()

            ret = mySapModel.DesignSteel.GetSummaryResults_2(ID, NumberItems, FrameType, DesignSect, Status, PMMCombo, PMMRatio, PRatio, MMajRatio, MMinRatio, VMajCombo, VMajRatio, VMinCombo, VMinRatio, eItemType.Objects)
            Dim PMMmax As Double, i As Long
            If ret = 0 Then
                PMMmax = PMMRatio(0)
                For i = 0 To NumberItems - 1
                    If PMMRatio(i) >= PMMmax Then
                        Lcm = Left(PMMCombo(i), Len(PMMCombo(i)) - 3)
                        AxialDCR = PRatio(i)
                        B_maj = MMajRatio(i)
                        B_min = MMinRatio(i)
                        PMM = PMMmax
                    End If
                Next
            Else
                Return 1
            End If
            Dim NumberNames As Integer
            Dim NumberResults As Integer
            Dim Obj() As String
            Dim ObjSta() As Double
            Dim Elm() As String
            Dim ElmSta() As Double
            Dim LoadCase() As String
            Dim StepType() As String
            Dim StepNum() As Double
            Dim P() As Double
            Dim V2() As Double
            Dim V3() As Double
            Dim T() As Double
            Dim M2() As Double
            Dim M3() As Double
            'get Mu
            'clear all case and combo output selections
            ret = mySapModel.Results.Setup.DeselectAllCasesAndCombosForOutput
            'get combo names
            ret = mySapModel.RespCombo.GetNameList(NumberNames, LoadCase)
            'set case and combo output selections
            Dim lcmNo As Integer
            'select combos for steel strength design
            For i = 0 To NumberNames - 1
                'Only get SST load combos
                If Not LoadCase(i) Like "SST_LC*" Then Continue For
                If Integer.TryParse(LoadCase(i).Remove(0, "SST_LC".Length), lcmNo) Then
                    If lcmNo <> 8 AndAlso lcmNo <> 23 AndAlso lcmNo <> 24 AndAlso lcmNo <> 25 AndAlso lcmNo <> 26 AndAlso lcmNo <> 27 AndAlso lcmNo <> 28 AndAlso lcmNo <= 36 Then
                        ret = mySapModel.Results.Setup.SetComboSelectedForOutput(LoadCase(i))
                    End If
                End If
            Next

            ''For i = 0 To NumberNames - 1
            ''    If LoadCase(i) Like "SST_LC*" Then
            ''        ret = mySapModel.Results.Setup.SetComboSelectedForOutput(LoadCase(i))
            ''    End If
            ''Next
            ret = mySapModel.Results.FrameForce(ID, eItemTypeElm.ObjectElm, NumberResults, Obj, ObjSta, Elm, ElmSta, LoadCase, StepType, StepNum, P, V2, V3, T, M2, M3)
            '
            If ret = 0 Then
                For i = 0 To NumberResults - 1
                    'If LoadCase(i) = "SST_LC31" Or LoadCase(i) = "SST_LC32" Or LoadCase(i) = "SST_LC33" Or LoadCase(i) = "SST_LC34" Then
                    If ObjSta(i) = ObjSta(0) Or ObjSta(i) = ObjSta(NumberResults - 1) Then
                        Mu = Math.Max(Mu, Math.Abs(M3(i)))
                    Else
                        MuMid = Math.Max(MuMid, Math.Abs(M3(i)))
                    End If
                    'End If
                Next
            Else
                Return 1
            End If
            Return 0
        Catch ex As Exception
            Return 1
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
    End Function
    Function getColumnDesignResult(ID As String, ByRef Lcm_maxDCR As String, ByRef Mu As Double, ByRef MuTop As Double, ByRef MuBot As Double,
                                   ByRef Pu_design As Double, ByRef AxialDCR As Double, ByRef B_maj As Double, ByRef B_min As Double) As Long
        'Reset DCR
        Mu = 0 : MuTop = 0 : MuBot = 0 : Pu_design = 0 : AxialDCR = 0 : B_maj = 0 : B_min = 0
        '
        Dim reUnit As Integer
        Try
            reUnit = mySapModel.GetPresentUnits()
            ret = mySapModel.SetPresentUnits((3))

            Dim NumberItems As Integer
            Dim FrameType As String()
            Dim DesignSect As String()
            Dim Status As String()
            Dim PMMCombo As String()
            Dim PMMRatio As Double()
            Dim PRatio As Double()
            Dim MMajRatio As Double()
            Dim MMinRatio As Double()
            Dim VMajCombo As String()
            Dim VMajRatio As Double()
            Dim VMinCombo As String()
            Dim VMinRatio As Double()

            ret = mySapModel.DesignSteel.GetSummaryResults_2(ID, NumberItems, FrameType, DesignSect, Status, PMMCombo, PMMRatio, PRatio, MMajRatio, MMinRatio, VMajCombo, VMajRatio, VMinCombo, VMinRatio, eItemType.Objects)
            Dim PMMmax As Double, i As Long
            If ret = 0 Then
                PMMmax = PMMRatio(0)
                For i = 0 To NumberItems - 1
                    If PMMRatio(i) >= PMMmax Then
                        Lcm_maxDCR = Left(PMMCombo(i), Len(PMMCombo(i)) - 3)
                        AxialDCR = PRatio(i)
                        B_maj = MMajRatio(i)
                        B_min = MMinRatio(i)
                        'PMM = PMMmax
                    End If
                Next
            Else
                Return 1
            End If
            Dim NumberNames As Integer
            Dim NumberResults As Integer
            Dim Obj() As String
            Dim ObjSta() As Double
            Dim Elm() As String
            Dim ElmSta() As Double
            Dim LoadCase() As String
            Dim StepType() As String
            Dim StepNum() As Double
            Dim P() As Double
            Dim V2() As Double
            Dim V3() As Double
            Dim T() As Double
            Dim M2() As Double
            Dim M3() As Double
            'get Mu
            'clear all case and combo output selections
            ret = mySapModel.Results.Setup.DeselectAllCasesAndCombosForOutput
            'get combo names
            ret = mySapModel.RespCombo.GetNameList(NumberNames, LoadCase)
            'set case and combo output selections
            For i = 0 To NumberNames - 1
                If LoadCase(i) Like "SST_LC*" Then
                    ret = mySapModel.Results.Setup.SetComboSelectedForOutput(LoadCase(i))
                End If
            Next
            ret = mySapModel.Results.FrameForce(ID, eItemTypeElm.ObjectElm, NumberResults, Obj, ObjSta, Elm, ElmSta, LoadCase, StepType, StepNum, P, V2, V3, T, M2, M3)
            '
            If ret = 0 Then
                For i = 0 To NumberResults - 1
                    If ObjSta(i) = ObjSta(0) Or ObjSta(i) = ObjSta(NumberResults - 1) Then
                        Dim isValidCombos As Boolean
                        'Lay Mu_omega thay vi LC_maxDCR
                        isValidCombos = (LoadCase(i) = "SST_LC33" Or LoadCase(i) = "SST_LC34" Or LoadCase(i) = "SST_LC35" Or LoadCase(i) = "SST_LC36" Or
                           LoadCase(i) = "SST_LC37" Or LoadCase(i) = "SST_LC38" Or LoadCase(i) = "SST_LC39" Or LoadCase(i) = "SST_LC40")
                        If isValidCombos Then
                            Mu = Math.Max(Mu, Math.Abs(M3(i)))
                            Pu_design = Math.Max(Pu_design, Math.Abs(P(i)))
                            If ObjSta(i) = ObjSta(0) Then
                                MuBot = Math.Max(MuBot, Math.Abs(M3(i)))
                            End If
                            If ObjSta(i) = ObjSta(NumberResults - 1) Then
                                MuTop = Math.Max(MuTop, Math.Abs(M3(i)))
                            End If
                        End If
                    End If
                    '
                Next
            Else
                Return 1
            End If
            Return 0
        Catch ex As Exception
            Return 1
        Finally
            ret = mySapModel.SetPresentUnits(reUnit)
        End Try
    End Function

    Function Get_ETABS_Folderpath() As String
        Return (mySapModel.GetModelFilepath)
    End Function

    Function GetDiaphragm(ByRef curJob As clsMRSJob, isBeam As Boolean) As Long
        Dim ret As Long = -1
        Try
            Dim NumberNames As Integer
            Dim MyName As String()
            Dim SemiRigid As Boolean

            '
            ret += mySapModel.Diaphragm.GetNameList(NumberNames, MyName)
            For Each name As String In MyName
                ret += mySapModel.Diaphragm.GetNameList(NumberNames, MyName)
                ret = mySapModel.Diaphragm.GetDiaphragm(name, SemiRigid)
                '

            Next


            'get area diaphragm assignment
            'ret = mySapModel.AreaObj.GetDiaphragm("4", DiaphragmName)
            'get area object names
            ret = mySapModel.AreaObj.GetNameList(NumberNames, MyName)

            'get area object names
            ret = mySapModel.AreaObj.GetNameListOnStory("Story2", NumberNames, MyName)


        Catch ex As Exception
            Return ret
        End Try
    End Function

    Function Get_RSA_Factor(ByRef SFX#, ByRef SFY#, ByRef SFX_D#, ByRef SFY_D#,
                            ByRef NewSFX#, ByRef NewSFY#, ByRef NewSFX_D#, ByRef NewSFY_D#) As Integer
        SFX = 1
        SFY = 1
        SFX_D = 1
        SFY_D = 1
        NewSFX = 386.09
        NewSFY = 386.09
        NewSFX_D = 386.09
        NewSFY_D = 386.09
        '
        Dim RSALoadName() As String
        Dim RSAFunction() As String
        Dim RSASF() As Double
        Dim RSACSys() As String
        Dim RSAAng() As Double
        Dim NumberLoads As Integer
        Dim NumberResults As Integer
        Dim LoadCase() As String
        Dim StepType() As String
        Dim StepNum() As Double
        Dim Fx() As Double
        Dim Fy() As Double
        Dim Fz() As Double
        Dim Mx() As Double
        Dim ParamMy() As Double
        Dim Mz() As Double
        Dim gx As Double
        Dim gy As Double
        Dim gz As Double
        '
        Dim EQX, EQY, EQX_D, EQY_D, SPECX, SPECY, SPECX_D, SPECY_D As Double
        Dim reUnit As Integer
        Dim RSAFlag As Integer = 0

        If mySapModel.GetModelIsLocked Then
            Try
                reUnit = mySapModel.GetPresentUnits()
                ret = mySapModel.SetPresentUnits((3))
                '
                Dim NumName As Integer
                Dim Names As String()
                ret += mySapModel.Func.GetNameList(NumName, Names)
                If Names.Contains("SPEC") Then
                    ret += mySapModel.LoadCases.ResponseSpectrum.GetLoads("SPECX", NumberLoads, RSALoadName, RSAFunction, RSASF, RSACSys, RSAAng)
                    If NumberLoads > 0 Then SFX = RSASF(0)
                    ret += mySapModel.LoadCases.ResponseSpectrum.GetLoads("SPECY", NumberLoads, RSALoadName, RSAFunction, RSASF, RSACSys, RSAAng)
                    If NumberLoads > 0 Then SFY = RSASF(0)
                    ret += mySapModel.LoadCases.ResponseSpectrum.GetLoads("SPECX_D", NumberLoads, RSALoadName, RSAFunction, RSASF, RSACSys, RSAAng)
                    If NumberLoads > 0 Then SFX_D = RSASF(0)
                    ret += mySapModel.LoadCases.ResponseSpectrum.GetLoads("SPECY_D", NumberLoads, RSALoadName, RSAFunction, RSASF, RSACSys, RSAAng)
                    If NumberLoads > 0 Then SFY_D = RSASF(0)


                    '
                    RSAFlag = ret
                    '
                    'clear all case and combo output selections
                    ret += mySapModel.Results.Setup.DeselectAllCasesAndCombosForOutput
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("EQX")
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("EQY")
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("EQX_D")
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("EQY_D")
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("SPECX")
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("SPECY")
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("SPECX_D")
                    ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("SPECY_D")

                    ret += mySapModel.Results.BaseReact(NumberResults, LoadCase, StepType, StepNum, Fx, Fy, Fz, Mx, ParamMy, Mz, gx, gy, gz)
                    Dim NoLoad
                    If ret = 0 Then 'if has RSA case
                        If NumberResults = 8 Then
                            For i As Long = 0 To NumberResults - 1
                                If LoadCase(i) = "EQX" Then : EQX = Fx(i)
                                ElseIf LoadCase(i) = "EQY" Then : EQY = Fy(i)
                                ElseIf LoadCase(i) = "EQX_D" Then : EQX_D = Fx(i)
                                ElseIf LoadCase(i) = "EQY_D" Then : EQY_D = Fy(i)
                                ElseIf LoadCase(i) = "SPECX" Then : SPECX = Fx(i)
                                ElseIf LoadCase(i) = "SPECY" Then : SPECY = Fy(i)
                                ElseIf LoadCase(i) = "SPECX_D" Then : SPECX_D = Fx(i)
                                ElseIf LoadCase(i) = "SPECY_D" Then : SPECY_D = Fy(i)
                                End If
                            Next
                        Else
                            RSAFlag = -2
                        End If
                        '
                        If SPECX <> 0 Then
                            NewSFX = Math.Abs(EQX / SPECX)
                            If (NewSFX < 0.85 Or NewSFX > 1.17) Then 'set 85% per AISC
                                NewSFX = Math.Round(NewSFX * SFX, 2)
                            Else
                                NewSFX = SFX
                            End If
                        End If
                        If SPECY <> 0 Then
                            NewSFY = Math.Abs(EQY / SPECY)
                            If NewSFY < 0.85 Or NewSFY > 1.17 Then
                                NewSFY = Math.Round(NewSFY * SFY, 2)
                            Else
                                NewSFY = SFY
                            End If
                        End If
                        If SPECX_D <> 0 Then
                            NewSFX_D = Math.Abs(EQX_D / SPECX_D)
                            If NewSFX_D < 0.85 Or NewSFX_D > 1.17 Then
                                NewSFX_D = Math.Round(NewSFX_D * SFX_D, 2)
                            Else
                                NewSFX_D = SFX_D
                            End If
                        End If
                        If SPECY_D <> 0 Then
                            NewSFY_D = Math.Abs(EQY_D / SPECY_D)
                            If NewSFY_D < 0.85 Or NewSFY_D > 1.17 Then
                                NewSFY_D = Math.Round(NewSFY_D * SFY_D, 2)
                            Else
                                NewSFY_D = SFY_D
                            End If
                        End If
                    End If
                Else
                    RSAFlag = -3 'No need
                End If
            Catch ex As Exception
                RSAFlag = -1
            Finally
                ret = mySapModel.SetPresentUnits(reUnit)
            End Try
        End If
        '
        Return RSAFlag
    End Function

    Function Modify_RSA_Factor(NewSFX#, NewSFY#, NewSFX_D#, NewSFY_D#) As Boolean
        Dim RSALoadName() As String
        Dim RSAFunction() As String
        Dim RSASF() As Double
        Dim RSACSys() As String
        Dim RSAAng() As Double
        Dim NoLCaces As Integer
        Dim LoadCases() As String
        'get current load case
        ret += mySapModel.LoadCases.GetNameList(NoLCaces, LoadCases)
        '
        ReDim RSALoadName(0)
        ReDim RSAFunction(0)
        ReDim RSASF(0)
        ReDim RSACSys(0)
        ReDim RSAAng(0)
        '   
        RSAFunction(0) = "SPEC"
        For Each RSCase As String In LoadCases
            If RSCase Like "SPECX*D" Then
                RSALoadName(0) = "U1" 'X=U1, Y=U2
                RSASF(0) = NewSFX_D
            ElseIf RSCase Like "SPECY*D" Then
                RSALoadName(0) = "U2"
                RSASF(0) = NewSFY_D
            ElseIf RSCase Like "SPECX*" Then
                RSALoadName(0) = "U1"
                RSASF(0) = NewSFX
            ElseIf RSCase Like "SPECY*" Then
                RSALoadName(0) = "U2"
                RSASF(0) = NewSFY
            Else
                Continue For
            End If
            RSACSys(0) = "GLOBAL"
            RSAAng(0) = 0
            ret += mySapModel.LoadCases.ResponseSpectrum.SetLoads(RSCase, 1, RSALoadName, RSAFunction, RSASF, RSACSys, RSAAng)
        Next
        '
        ret += mySapModel.SetModelIsLocked(False)
        Return ret = 0
    End Function

    Function Check_ModalMassParticipation() As Integer
        Dim NumberResults As Integer
        Dim LoadCase() As String
        Dim StepType() As String
        Dim StepNum() As Double
        Dim Period() As Double
        Dim Ux() As Double
        Dim Uy() As Double
        Dim Uz() As Double
        Dim SumUx() As Double
        Dim SumUy() As Double
        Dim SumUz() As Double
        Dim Rx() As Double
        Dim Ry() As Double
        Dim Rz() As Double
        Dim SumRx() As Double
        Dim SumRy() As Double
        Dim SumRz() As Double
        'get modal participating mass ratios
        If mySapModel.GetModelIsLocked Then
            'deselect all cases and combos
            ret += mySapModel.Results.Setup.DeselectAllCasesAndCombosForOutput()
            'check if exist Model case in Define\Model case

            'set case selected for output
            ret += mySapModel.Results.Setup.SetCaseSelectedForOutput("MODAL")
            If ret > 0 Then
                Return 1 'No Model case
            End If

            ret += mySapModel.Results.ModalParticipatingMassRatios(NumberResults, LoadCase, StepType, StepNum, Period, Ux, Uy, Uz, SumUx, SumUy, SumUz, Rx, Ry, Rz, SumRx, SumRy, SumRz)
            '
            If ret = 0 Then
                For i As Integer = 0 To NumberResults - 1
                    If SumUx(i) >= 0.9 AndAlso SumUy(i) >= 0.9 Then
                        Return 0
                    End If
                Next
                ''MsgBox("")
                ''mySapModel.LoadCases.ModalEigen.SetNumberModes("MODAL", 12, 1)
                Return 2
            Else
                Return 3
            End If
        Else
            Return -1
        End If
    End Function

    Function CheckFileSave() As Boolean
        Return IO.Directory.Exists(mySapModel.GetModelFilepath())
    End Function

End Class
