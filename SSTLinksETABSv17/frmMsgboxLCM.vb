﻿Public Class frmMsgboxLcm

    Public Property resVal As Integer
    Public Property iskeepLP_LC As Boolean = True
    Sub New(isEtab As Boolean)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        resVal = -1
        'If Not isEtab Then
        '    ckKeepLoadPatts_LC.Checked = False
        '    ckKeepLoadPatts_LC.Visible = False
        'End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        resVal = -1
        For Each ctrl As Windows.Forms.RadioButton In GroupBox1.Controls
            If ctrl.Checked Then
                Integer.TryParse(ctrl.Tag, resVal)
                Exit For
            End If
        Next
        '
        If resVal = 3 Or resVal = 4 Then
            MsgBox("Response Spectrum will be created as per seismic parameters defined using the ""Design Parameters"" feature." & vbLf & vbLf &
                   "Scale factor (set to 1.0g) will be updated after running analysis.", MsgBoxStyle.Exclamation, "Yield-Link® Connection")
        End If
        '
        iskeepLP_LC = ckKeepLoadPatts_LC.Checked
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        resVal = -1
        Me.Close()
    End Sub

    Private Sub frmMsgbox_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Escape Then
            btnCancel_Click(sender, e)
        End If
    End Sub
    Private Sub ckKeepLoadPatts_LC_CheckedChanged(sender As Object, e As EventArgs) Handles ckKeepLoadPatts_LC.CheckedChanged
        If Not ckKeepLoadPatts_LC.Checked Then
            If g_isEtab = True Then
                MsgBox("Some design parameters cannot be applied to new load patterns due to Application Programming Interface (API) limitations." & vbLf & vbLf &
                       "Please use 'Modify .e2k file' feature to ensure correct values are applied.", MsgBoxStyle.Exclamation, "Yield-Link® Connection")
            Else
                MsgBox("Some design parameters cannot be applied to new load patterns due to Application Programming Interface (API) limitations." & vbLf & vbLf &
                   "Please use 'Modify .s2k file' feature to ensure correct values are applied.", MsgBoxStyle.Exclamation, "Yield-Link® Connection")
            End If
        End If
    End Sub
End Class