﻿'frmDCRLimits
Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmDesignParams
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.grName = New System.Windows.Forms.GroupBox()
        Me.tabInfo = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.val_DFType = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox63 = New System.Windows.Forms.TextBox()
        Me.TextBox65 = New System.Windows.Forms.TextBox()
        Me.TextBox60 = New System.Windows.Forms.TextBox()
        Me.TextBox61 = New System.Windows.Forms.TextBox()
        Me.TextBox56 = New System.Windows.Forms.TextBox()
        Me.TextBox57 = New System.Windows.Forms.TextBox()
        Me.TextBox53 = New System.Windows.Forms.TextBox()
        Me.TextBox49 = New System.Windows.Forms.TextBox()
        Me.TextBox45 = New System.Windows.Forms.TextBox()
        Me.TextBox41 = New System.Windows.Forms.TextBox()
        Me.TextBox37 = New System.Windows.Forms.TextBox()
        Me.TextBox33 = New System.Windows.Forms.TextBox()
        Me.TextBox34 = New System.Windows.Forms.TextBox()
        Me.TextBox30 = New System.Windows.Forms.TextBox()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.TextBox21 = New System.Windows.Forms.TextBox()
        Me.TextBox29 = New System.Windows.Forms.TextBox()
        Me.TextBox28 = New System.Windows.Forms.TextBox()
        Me.val_DesignCode = New System.Windows.Forms.TextBox()
        Me.val_Sds = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.val_TL = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.val_S1 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.val_SdsUser = New System.Windows.Forms.TextBox()
        Me.val_SiteClass = New System.Windows.Forms.TextBox()
        Me.val_ASCE = New System.Windows.Forms.TextBox()
        Me.val_Ss = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox24 = New System.Windows.Forms.TextBox()
        Me.TextBox23 = New System.Windows.Forms.TextBox()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox26 = New System.Windows.Forms.TextBox()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox36 = New System.Windows.Forms.TextBox()
        Me.TextBox52 = New System.Windows.Forms.TextBox()
        Me.TextBox48 = New System.Windows.Forms.TextBox()
        Me.TextBox44 = New System.Windows.Forms.TextBox()
        Me.TextBox40 = New System.Windows.Forms.TextBox()
        Me.val_f2X = New System.Windows.Forms.TextBox()
        Me.val_f1X = New System.Windows.Forms.TextBox()
        Me.val_RhoY = New System.Windows.Forms.TextBox()
        Me.val_RhoX = New System.Windows.Forms.TextBox()
        Me.val_Iy = New System.Windows.Forms.TextBox()
        Me.val_Ix = New System.Windows.Forms.TextBox()
        Me.val_CdY = New System.Windows.Forms.TextBox()
        Me.val_CdX = New System.Windows.Forms.TextBox()
        Me.val_OmegaY = New System.Windows.Forms.TextBox()
        Me.val_OmegaX = New System.Windows.Forms.TextBox()
        Me.val_Ry = New System.Windows.Forms.TextBox()
        Me.val_Rx = New System.Windows.Forms.TextBox()
        Me.val_FTypeY = New System.Windows.Forms.TextBox()
        Me.val_FTypeX = New System.Windows.Forms.TextBox()
        Me.TextBox64 = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.val_ASCE_W = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.TextBox27 = New System.Windows.Forms.TextBox()
        Me.TextBox83 = New System.Windows.Forms.TextBox()
        Me.TextBox84 = New System.Windows.Forms.TextBox()
        Me.TextBox85 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.val_KdX = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.val_GustFactorX = New System.Windows.Forms.TextBox()
        Me.TextBox31 = New System.Windows.Forms.TextBox()
        Me.TextBox32 = New System.Windows.Forms.TextBox()
        Me.val_KztX = New System.Windows.Forms.TextBox()
        Me.TextBox66 = New System.Windows.Forms.TextBox()
        Me.TextBox67 = New System.Windows.Forms.TextBox()
        Me.val_WindSpeedX = New System.Windows.Forms.TextBox()
        Me.val_GroundFactorX = New System.Windows.Forms.TextBox()
        Me.TextBox70 = New System.Windows.Forms.TextBox()
        Me.TextBox71 = New System.Windows.Forms.TextBox()
        Me.TextBox74 = New System.Windows.Forms.TextBox()
        Me.val_ExposureX = New System.Windows.Forms.TextBox()
        Me.TextBox77 = New System.Windows.Forms.TextBox()
        Me.TextBox78 = New System.Windows.Forms.TextBox()
        Me.TextBox79 = New System.Windows.Forms.TextBox()
        Me.TextBox80 = New System.Windows.Forms.TextBox()
        Me.TextBox81 = New System.Windows.Forms.TextBox()
        Me.TextBox82 = New System.Windows.Forms.TextBox()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCB = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ASCE7_16 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ASCE7_10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ASCE7_05 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ASCE7_02 = New System.Windows.Forms.ToolStripMenuItem()
        Me.lbtabDetails = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.grName.SuspendLayout()
        Me.tabInfo.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.cCB.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(348, 516)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 25
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(429, 516)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.grName)
        Me.Panel1.Location = New System.Drawing.Point(10, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(494, 472)
        Me.Panel1.TabIndex = 6
        '
        'grName
        '
        Me.grName.Controls.Add(Me.tabInfo)
        Me.grName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grName.Location = New System.Drawing.Point(0, 0)
        Me.grName.Name = "grName"
        Me.grName.Size = New System.Drawing.Size(494, 472)
        Me.grName.TabIndex = 4
        Me.grName.TabStop = False
        Me.grName.Text = "Strength Limits"
        '
        'tabInfo
        '
        Me.tabInfo.Controls.Add(Me.TabPage1)
        Me.tabInfo.Controls.Add(Me.TabPage2)
        Me.tabInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabInfo.ItemSize = New System.Drawing.Size(50, 18)
        Me.tabInfo.Location = New System.Drawing.Point(3, 16)
        Me.tabInfo.Name = "tabInfo"
        Me.tabInfo.SelectedIndex = 0
        Me.tabInfo.Size = New System.Drawing.Size(488, 453)
        Me.tabInfo.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tabInfo.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TextBox3)
        Me.TabPage1.Controls.Add(Me.val_DFType)
        Me.TabPage1.Controls.Add(Me.TextBox7)
        Me.TabPage1.Controls.Add(Me.TextBox63)
        Me.TabPage1.Controls.Add(Me.TextBox65)
        Me.TabPage1.Controls.Add(Me.TextBox60)
        Me.TabPage1.Controls.Add(Me.TextBox61)
        Me.TabPage1.Controls.Add(Me.TextBox56)
        Me.TabPage1.Controls.Add(Me.TextBox57)
        Me.TabPage1.Controls.Add(Me.TextBox53)
        Me.TabPage1.Controls.Add(Me.TextBox49)
        Me.TabPage1.Controls.Add(Me.TextBox45)
        Me.TabPage1.Controls.Add(Me.TextBox41)
        Me.TabPage1.Controls.Add(Me.TextBox37)
        Me.TabPage1.Controls.Add(Me.TextBox33)
        Me.TabPage1.Controls.Add(Me.TextBox34)
        Me.TabPage1.Controls.Add(Me.TextBox30)
        Me.TabPage1.Controls.Add(Me.TextBox20)
        Me.TabPage1.Controls.Add(Me.TextBox21)
        Me.TabPage1.Controls.Add(Me.TextBox29)
        Me.TabPage1.Controls.Add(Me.TextBox28)
        Me.TabPage1.Controls.Add(Me.val_DesignCode)
        Me.TabPage1.Controls.Add(Me.val_Sds)
        Me.TabPage1.Controls.Add(Me.TextBox17)
        Me.TabPage1.Controls.Add(Me.TextBox18)
        Me.TabPage1.Controls.Add(Me.TextBox14)
        Me.TabPage1.Controls.Add(Me.TextBox15)
        Me.TabPage1.Controls.Add(Me.val_TL)
        Me.TabPage1.Controls.Add(Me.TextBox11)
        Me.TabPage1.Controls.Add(Me.TextBox12)
        Me.TabPage1.Controls.Add(Me.val_S1)
        Me.TabPage1.Controls.Add(Me.TextBox8)
        Me.TabPage1.Controls.Add(Me.TextBox9)
        Me.TabPage1.Controls.Add(Me.val_SdsUser)
        Me.TabPage1.Controls.Add(Me.val_SiteClass)
        Me.TabPage1.Controls.Add(Me.val_ASCE)
        Me.TabPage1.Controls.Add(Me.val_Ss)
        Me.TabPage1.Controls.Add(Me.TextBox5)
        Me.TabPage1.Controls.Add(Me.TextBox6)
        Me.TabPage1.Controls.Add(Me.TextBox24)
        Me.TabPage1.Controls.Add(Me.TextBox23)
        Me.TabPage1.Controls.Add(Me.TextBox22)
        Me.TabPage1.Controls.Add(Me.TextBox2)
        Me.TabPage1.Controls.Add(Me.TextBox26)
        Me.TabPage1.Controls.Add(Me.TextBox25)
        Me.TabPage1.Controls.Add(Me.TextBox1)
        Me.TabPage1.Controls.Add(Me.TextBox36)
        Me.TabPage1.Controls.Add(Me.TextBox52)
        Me.TabPage1.Controls.Add(Me.TextBox48)
        Me.TabPage1.Controls.Add(Me.TextBox44)
        Me.TabPage1.Controls.Add(Me.TextBox40)
        Me.TabPage1.Controls.Add(Me.val_f2X)
        Me.TabPage1.Controls.Add(Me.val_f1X)
        Me.TabPage1.Controls.Add(Me.val_RhoY)
        Me.TabPage1.Controls.Add(Me.val_RhoX)
        Me.TabPage1.Controls.Add(Me.val_Iy)
        Me.TabPage1.Controls.Add(Me.val_Ix)
        Me.TabPage1.Controls.Add(Me.val_CdY)
        Me.TabPage1.Controls.Add(Me.val_CdX)
        Me.TabPage1.Controls.Add(Me.val_OmegaY)
        Me.TabPage1.Controls.Add(Me.val_OmegaX)
        Me.TabPage1.Controls.Add(Me.val_Ry)
        Me.TabPage1.Controls.Add(Me.val_Rx)
        Me.TabPage1.Controls.Add(Me.val_FTypeY)
        Me.TabPage1.Controls.Add(Me.val_FTypeX)
        Me.TabPage1.Controls.Add(Me.TextBox64)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(480, 427)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Seismic"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox3.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox3.Location = New System.Drawing.Point(45, 401)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(250, 20)
        Me.TextBox3.TabIndex = 59
        Me.TextBox3.TabStop = False
        Me.TextBox3.Text = "     Design Frame Type="
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'val_DFType
        '
        Me.val_DFType.BackColor = System.Drawing.SystemColors.Control
        Me.val_DFType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.val_DFType.Location = New System.Drawing.Point(294, 401)
        Me.val_DFType.Name = "val_DFType"
        Me.val_DFType.ReadOnly = True
        Me.val_DFType.Size = New System.Drawing.Size(180, 20)
        Me.val_DFType.TabIndex = 57
        Me.val_DFType.Text = "OMF"
        Me.val_DFType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(6, 401)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(40, 20)
        Me.TextBox7.TabIndex = 58
        Me.TextBox7.TabStop = False
        Me.TextBox7.Text = "2"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox63
        '
        Me.TextBox63.Location = New System.Drawing.Point(45, 384)
        Me.TextBox63.Name = "TextBox63"
        Me.TextBox63.ReadOnly = True
        Me.TextBox63.Size = New System.Drawing.Size(250, 20)
        Me.TextBox63.TabIndex = 56
        Me.TextBox63.TabStop = False
        Me.TextBox63.Text = "Design Code ="
        Me.TextBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox65
        '
        Me.TextBox65.Location = New System.Drawing.Point(6, 384)
        Me.TextBox65.Name = "TextBox65"
        Me.TextBox65.ReadOnly = True
        Me.TextBox65.Size = New System.Drawing.Size(40, 20)
        Me.TextBox65.TabIndex = 55
        Me.TextBox65.TabStop = False
        Me.TextBox65.Text = "1"
        Me.TextBox65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox60
        '
        Me.TextBox60.Location = New System.Drawing.Point(45, 348)
        Me.TextBox60.Name = "TextBox60"
        Me.TextBox60.ReadOnly = True
        Me.TextBox60.Size = New System.Drawing.Size(250, 20)
        Me.TextBox60.TabIndex = 51
        Me.TextBox60.TabStop = False
        Me.TextBox60.Text = "Snow Load Factor, f2="
        Me.TextBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox61
        '
        Me.TextBox61.Location = New System.Drawing.Point(6, 348)
        Me.TextBox61.Name = "TextBox61"
        Me.TextBox61.ReadOnly = True
        Me.TextBox61.Size = New System.Drawing.Size(40, 20)
        Me.TextBox61.TabIndex = 50
        Me.TextBox61.TabStop = False
        Me.TextBox61.Text = "8"
        Me.TextBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox56
        '
        Me.TextBox56.Location = New System.Drawing.Point(45, 329)
        Me.TextBox56.Name = "TextBox56"
        Me.TextBox56.ReadOnly = True
        Me.TextBox56.Size = New System.Drawing.Size(250, 20)
        Me.TextBox56.TabIndex = 47
        Me.TextBox56.TabStop = False
        Me.TextBox56.Text = "Live Load Factor, f1="
        Me.TextBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox57
        '
        Me.TextBox57.Location = New System.Drawing.Point(6, 329)
        Me.TextBox57.Name = "TextBox57"
        Me.TextBox57.ReadOnly = True
        Me.TextBox57.Size = New System.Drawing.Size(40, 20)
        Me.TextBox57.TabIndex = 46
        Me.TextBox57.TabStop = False
        Me.TextBox57.Text = "7"
        Me.TextBox57.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox53
        '
        Me.TextBox53.Location = New System.Drawing.Point(6, 310)
        Me.TextBox53.Name = "TextBox53"
        Me.TextBox53.ReadOnly = True
        Me.TextBox53.Size = New System.Drawing.Size(40, 20)
        Me.TextBox53.TabIndex = 42
        Me.TextBox53.TabStop = False
        Me.TextBox53.Text = "6"
        Me.TextBox53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox49
        '
        Me.TextBox49.Location = New System.Drawing.Point(6, 291)
        Me.TextBox49.Name = "TextBox49"
        Me.TextBox49.ReadOnly = True
        Me.TextBox49.Size = New System.Drawing.Size(40, 20)
        Me.TextBox49.TabIndex = 38
        Me.TextBox49.TabStop = False
        Me.TextBox49.Text = "5"
        Me.TextBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox45
        '
        Me.TextBox45.Location = New System.Drawing.Point(6, 272)
        Me.TextBox45.Name = "TextBox45"
        Me.TextBox45.ReadOnly = True
        Me.TextBox45.Size = New System.Drawing.Size(40, 20)
        Me.TextBox45.TabIndex = 34
        Me.TextBox45.TabStop = False
        Me.TextBox45.Text = "4"
        Me.TextBox45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox41
        '
        Me.TextBox41.Location = New System.Drawing.Point(6, 253)
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.ReadOnly = True
        Me.TextBox41.Size = New System.Drawing.Size(40, 20)
        Me.TextBox41.TabIndex = 30
        Me.TextBox41.TabStop = False
        Me.TextBox41.Text = "3"
        Me.TextBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox37
        '
        Me.TextBox37.Location = New System.Drawing.Point(6, 234)
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.ReadOnly = True
        Me.TextBox37.Size = New System.Drawing.Size(40, 20)
        Me.TextBox37.TabIndex = 26
        Me.TextBox37.TabStop = False
        Me.TextBox37.Text = "2"
        Me.TextBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox33
        '
        Me.TextBox33.Location = New System.Drawing.Point(45, 215)
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.ReadOnly = True
        Me.TextBox33.Size = New System.Drawing.Size(250, 20)
        Me.TextBox33.TabIndex = 23
        Me.TextBox33.TabStop = False
        Me.TextBox33.Text = "Lateral Force Resisting System="
        Me.TextBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox34
        '
        Me.TextBox34.Location = New System.Drawing.Point(6, 215)
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.ReadOnly = True
        Me.TextBox34.Size = New System.Drawing.Size(40, 20)
        Me.TextBox34.TabIndex = 22
        Me.TextBox34.TabStop = False
        Me.TextBox34.Text = "1"
        Me.TextBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox30
        '
        Me.TextBox30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox30.Location = New System.Drawing.Point(384, 196)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.ReadOnly = True
        Me.TextBox30.Size = New System.Drawing.Size(90, 20)
        Me.TextBox30.TabIndex = 21
        Me.TextBox30.TabStop = False
        Me.TextBox30.Text = "Y-Dir"
        Me.TextBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(45, 139)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.ReadOnly = True
        Me.TextBox20.Size = New System.Drawing.Size(250, 20)
        Me.TextBox20.TabIndex = 19
        Me.TextBox20.TabStop = False
        Me.TextBox20.Text = "Sds - User defined="
        Me.TextBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox21
        '
        Me.TextBox21.Location = New System.Drawing.Point(6, 139)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.ReadOnly = True
        Me.TextBox21.Size = New System.Drawing.Size(40, 20)
        Me.TextBox21.TabIndex = 18
        Me.TextBox21.TabStop = False
        Me.TextBox21.Text = "6"
        Me.TextBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox29
        '
        Me.TextBox29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox29.Location = New System.Drawing.Point(294, 196)
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.ReadOnly = True
        Me.TextBox29.Size = New System.Drawing.Size(91, 20)
        Me.TextBox29.TabIndex = 17
        Me.TextBox29.TabStop = False
        Me.TextBox29.Text = "X-Dir"
        Me.TextBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox28
        '
        Me.TextBox28.Location = New System.Drawing.Point(6, 196)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.ReadOnly = True
        Me.TextBox28.Size = New System.Drawing.Size(289, 20)
        Me.TextBox28.TabIndex = 16
        Me.TextBox28.TabStop = False
        Me.TextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'val_DesignCode
        '
        Me.val_DesignCode.BackColor = System.Drawing.SystemColors.Info
        Me.val_DesignCode.ForeColor = System.Drawing.Color.Blue
        Me.val_DesignCode.Location = New System.Drawing.Point(294, 384)
        Me.val_DesignCode.Name = "val_DesignCode"
        Me.val_DesignCode.ReadOnly = True
        Me.val_DesignCode.Size = New System.Drawing.Size(180, 20)
        Me.val_DesignCode.TabIndex = 24
        Me.val_DesignCode.Text = "-"
        Me.val_DesignCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_Sds
        '
        Me.val_Sds.ForeColor = System.Drawing.Color.Blue
        Me.val_Sds.Location = New System.Drawing.Point(294, 158)
        Me.val_Sds.Name = "val_Sds"
        Me.val_Sds.Size = New System.Drawing.Size(180, 20)
        Me.val_Sds.TabIndex = 7
        Me.val_Sds.Text = "-"
        Me.val_Sds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(45, 158)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(250, 20)
        Me.TextBox17.TabIndex = 16
        Me.TextBox17.TabStop = False
        Me.TextBox17.Text = "Design Spectral Resp. Accel at Short Period, Sds="
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(6, 158)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.Size = New System.Drawing.Size(40, 20)
        Me.TextBox18.TabIndex = 15
        Me.TextBox18.TabStop = False
        Me.TextBox18.Text = "7"
        Me.TextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(45, 120)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(250, 20)
        Me.TextBox14.TabIndex = 13
        Me.TextBox14.TabStop = False
        Me.TextBox14.Text = "Site Class="
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(6, 120)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(40, 20)
        Me.TextBox15.TabIndex = 12
        Me.TextBox15.TabStop = False
        Me.TextBox15.Text = "5"
        Me.TextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_TL
        '
        Me.val_TL.ForeColor = System.Drawing.Color.Blue
        Me.val_TL.Location = New System.Drawing.Point(294, 101)
        Me.val_TL.Name = "val_TL"
        Me.val_TL.Size = New System.Drawing.Size(180, 20)
        Me.val_TL.TabIndex = 4
        Me.val_TL.Text = "-"
        Me.val_TL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(45, 101)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.ReadOnly = True
        Me.TextBox11.Size = New System.Drawing.Size(250, 20)
        Me.TextBox11.TabIndex = 10
        Me.TextBox11.TabStop = False
        Me.TextBox11.Text = "Long-Period Transition Period, TL="
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(6, 101)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.Size = New System.Drawing.Size(40, 20)
        Me.TextBox12.TabIndex = 9
        Me.TextBox12.TabStop = False
        Me.TextBox12.Text = "4"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_S1
        '
        Me.val_S1.ForeColor = System.Drawing.Color.Blue
        Me.val_S1.Location = New System.Drawing.Point(294, 82)
        Me.val_S1.Name = "val_S1"
        Me.val_S1.Size = New System.Drawing.Size(180, 20)
        Me.val_S1.TabIndex = 3
        Me.val_S1.Text = "-"
        Me.val_S1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(45, 82)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(250, 20)
        Me.TextBox8.TabIndex = 7
        Me.TextBox8.TabStop = False
        Me.TextBox8.Text = "1.0 Sec Spectral Accel, S1="
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(6, 82)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(40, 20)
        Me.TextBox9.TabIndex = 6
        Me.TextBox9.TabStop = False
        Me.TextBox9.Text = "3"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_SdsUser
        '
        Me.val_SdsUser.BackColor = System.Drawing.SystemColors.Info
        Me.val_SdsUser.ForeColor = System.Drawing.Color.Blue
        Me.val_SdsUser.Location = New System.Drawing.Point(294, 139)
        Me.val_SdsUser.Name = "val_SdsUser"
        Me.val_SdsUser.ReadOnly = True
        Me.val_SdsUser.Size = New System.Drawing.Size(180, 20)
        Me.val_SdsUser.TabIndex = 6
        Me.val_SdsUser.Text = "-"
        Me.val_SdsUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_SiteClass
        '
        Me.val_SiteClass.BackColor = System.Drawing.SystemColors.Info
        Me.val_SiteClass.ForeColor = System.Drawing.Color.Blue
        Me.val_SiteClass.Location = New System.Drawing.Point(294, 120)
        Me.val_SiteClass.Name = "val_SiteClass"
        Me.val_SiteClass.ReadOnly = True
        Me.val_SiteClass.Size = New System.Drawing.Size(180, 20)
        Me.val_SiteClass.TabIndex = 5
        Me.val_SiteClass.Text = "-"
        Me.val_SiteClass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_ASCE
        '
        Me.val_ASCE.BackColor = System.Drawing.SystemColors.Info
        Me.val_ASCE.ForeColor = System.Drawing.Color.Blue
        Me.val_ASCE.Location = New System.Drawing.Point(294, 44)
        Me.val_ASCE.Name = "val_ASCE"
        Me.val_ASCE.ReadOnly = True
        Me.val_ASCE.Size = New System.Drawing.Size(180, 20)
        Me.val_ASCE.TabIndex = 1
        Me.val_ASCE.Text = "-"
        Me.val_ASCE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_Ss
        '
        Me.val_Ss.ForeColor = System.Drawing.Color.Blue
        Me.val_Ss.Location = New System.Drawing.Point(294, 63)
        Me.val_Ss.Name = "val_Ss"
        Me.val_Ss.Size = New System.Drawing.Size(180, 20)
        Me.val_Ss.TabIndex = 2
        Me.val_Ss.Text = "-"
        Me.val_Ss.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(45, 63)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(250, 20)
        Me.TextBox5.TabIndex = 4
        Me.TextBox5.TabStop = False
        Me.TextBox5.Text = "0.2 Sec Spectral Accel, Ss="
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(6, 63)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(40, 20)
        Me.TextBox6.TabIndex = 3
        Me.TextBox6.TabStop = False
        Me.TextBox6.Text = "2"
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox24
        '
        Me.TextBox24.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox24.Location = New System.Drawing.Point(294, 6)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.ReadOnly = True
        Me.TextBox24.Size = New System.Drawing.Size(180, 20)
        Me.TextBox24.TabIndex = 2
        Me.TextBox24.TabStop = False
        Me.TextBox24.Text = "Data"
        Me.TextBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox23
        '
        Me.TextBox23.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox23.Location = New System.Drawing.Point(45, 6)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.ReadOnly = True
        Me.TextBox23.Size = New System.Drawing.Size(250, 20)
        Me.TextBox23.TabIndex = 1
        Me.TextBox23.TabStop = False
        Me.TextBox23.Text = "Item"
        Me.TextBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox22
        '
        Me.TextBox22.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox22.Location = New System.Drawing.Point(6, 6)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.ReadOnly = True
        Me.TextBox22.Size = New System.Drawing.Size(40, 20)
        Me.TextBox22.TabIndex = 0
        Me.TextBox22.TabStop = False
        Me.TextBox22.Text = "No."
        Me.TextBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(45, 44)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(250, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.TabStop = False
        Me.TextBox2.Text = "Standard="
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox26
        '
        Me.TextBox26.BackColor = System.Drawing.Color.White
        Me.TextBox26.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox26.Location = New System.Drawing.Point(6, 177)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.ReadOnly = True
        Me.TextBox26.Size = New System.Drawing.Size(468, 20)
        Me.TextBox26.TabIndex = 0
        Me.TextBox26.TabStop = False
        Me.TextBox26.Text = "    Factors"
        '
        'TextBox25
        '
        Me.TextBox25.BackColor = System.Drawing.Color.White
        Me.TextBox25.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox25.Location = New System.Drawing.Point(6, 25)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.ReadOnly = True
        Me.TextBox25.Size = New System.Drawing.Size(468, 20)
        Me.TextBox25.TabIndex = 0
        Me.TextBox25.TabStop = False
        Me.TextBox25.Text = "     Seismics Coefficients"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(6, 44)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(40, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "1"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox36
        '
        Me.TextBox36.Location = New System.Drawing.Point(45, 234)
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.ReadOnly = True
        Me.TextBox36.Size = New System.Drawing.Size(250, 20)
        Me.TextBox36.TabIndex = 27
        Me.TextBox36.TabStop = False
        Me.TextBox36.Text = "Response Modification Coefficient, R="
        Me.TextBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox52
        '
        Me.TextBox52.Location = New System.Drawing.Point(45, 310)
        Me.TextBox52.Name = "TextBox52"
        Me.TextBox52.ReadOnly = True
        Me.TextBox52.Size = New System.Drawing.Size(250, 20)
        Me.TextBox52.TabIndex = 43
        Me.TextBox52.TabStop = False
        Me.TextBox52.Text = "Redundancy Factor, Rho="
        Me.TextBox52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox48
        '
        Me.TextBox48.Location = New System.Drawing.Point(45, 291)
        Me.TextBox48.Name = "TextBox48"
        Me.TextBox48.ReadOnly = True
        Me.TextBox48.Size = New System.Drawing.Size(250, 20)
        Me.TextBox48.TabIndex = 39
        Me.TextBox48.TabStop = False
        Me.TextBox48.Text = "Occupancy Importance, I="
        Me.TextBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox44
        '
        Me.TextBox44.Location = New System.Drawing.Point(45, 272)
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.ReadOnly = True
        Me.TextBox44.Size = New System.Drawing.Size(250, 20)
        Me.TextBox44.TabIndex = 35
        Me.TextBox44.TabStop = False
        Me.TextBox44.Text = "Deflection Amplification factor, Cd="
        Me.TextBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox40
        '
        Me.TextBox40.Location = New System.Drawing.Point(45, 253)
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.ReadOnly = True
        Me.TextBox40.Size = New System.Drawing.Size(250, 20)
        Me.TextBox40.TabIndex = 31
        Me.TextBox40.TabStop = False
        Me.TextBox40.Text = "Overstrength Factor, Omega="
        Me.TextBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'val_f2X
        '
        Me.val_f2X.ForeColor = System.Drawing.Color.Blue
        Me.val_f2X.Location = New System.Drawing.Point(294, 348)
        Me.val_f2X.Name = "val_f2X"
        Me.val_f2X.Size = New System.Drawing.Size(180, 20)
        Me.val_f2X.TabIndex = 22
        Me.val_f2X.Text = "-"
        Me.val_f2X.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_f1X
        '
        Me.val_f1X.ForeColor = System.Drawing.Color.Blue
        Me.val_f1X.Location = New System.Drawing.Point(294, 329)
        Me.val_f1X.Name = "val_f1X"
        Me.val_f1X.Size = New System.Drawing.Size(180, 20)
        Me.val_f1X.TabIndex = 20
        Me.val_f1X.Text = "-"
        Me.val_f1X.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_RhoY
        '
        Me.val_RhoY.BackColor = System.Drawing.SystemColors.Info
        Me.val_RhoY.ForeColor = System.Drawing.Color.Blue
        Me.val_RhoY.Location = New System.Drawing.Point(384, 310)
        Me.val_RhoY.Name = "val_RhoY"
        Me.val_RhoY.ReadOnly = True
        Me.val_RhoY.Size = New System.Drawing.Size(90, 20)
        Me.val_RhoY.TabIndex = 19
        Me.val_RhoY.Text = "-"
        Me.val_RhoY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_RhoX
        '
        Me.val_RhoX.BackColor = System.Drawing.SystemColors.Info
        Me.val_RhoX.ForeColor = System.Drawing.Color.Blue
        Me.val_RhoX.Location = New System.Drawing.Point(294, 310)
        Me.val_RhoX.Name = "val_RhoX"
        Me.val_RhoX.ReadOnly = True
        Me.val_RhoX.Size = New System.Drawing.Size(91, 20)
        Me.val_RhoX.TabIndex = 18
        Me.val_RhoX.Text = "-"
        Me.val_RhoX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_Iy
        '
        Me.val_Iy.ForeColor = System.Drawing.Color.Blue
        Me.val_Iy.Location = New System.Drawing.Point(384, 291)
        Me.val_Iy.Name = "val_Iy"
        Me.val_Iy.Size = New System.Drawing.Size(90, 20)
        Me.val_Iy.TabIndex = 17
        Me.val_Iy.Text = "-"
        Me.val_Iy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_Ix
        '
        Me.val_Ix.ForeColor = System.Drawing.Color.Blue
        Me.val_Ix.Location = New System.Drawing.Point(294, 291)
        Me.val_Ix.Name = "val_Ix"
        Me.val_Ix.Size = New System.Drawing.Size(91, 20)
        Me.val_Ix.TabIndex = 16
        Me.val_Ix.Text = "-"
        Me.val_Ix.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_CdY
        '
        Me.val_CdY.ForeColor = System.Drawing.Color.Blue
        Me.val_CdY.Location = New System.Drawing.Point(384, 272)
        Me.val_CdY.Name = "val_CdY"
        Me.val_CdY.ReadOnly = True
        Me.val_CdY.Size = New System.Drawing.Size(90, 20)
        Me.val_CdY.TabIndex = 15
        Me.val_CdY.Text = "-"
        Me.val_CdY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_CdX
        '
        Me.val_CdX.ForeColor = System.Drawing.Color.Blue
        Me.val_CdX.Location = New System.Drawing.Point(294, 272)
        Me.val_CdX.Name = "val_CdX"
        Me.val_CdX.ReadOnly = True
        Me.val_CdX.Size = New System.Drawing.Size(91, 20)
        Me.val_CdX.TabIndex = 14
        Me.val_CdX.Text = "-"
        Me.val_CdX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_OmegaY
        '
        Me.val_OmegaY.ForeColor = System.Drawing.Color.Blue
        Me.val_OmegaY.Location = New System.Drawing.Point(384, 253)
        Me.val_OmegaY.Name = "val_OmegaY"
        Me.val_OmegaY.ReadOnly = True
        Me.val_OmegaY.Size = New System.Drawing.Size(90, 20)
        Me.val_OmegaY.TabIndex = 13
        Me.val_OmegaY.Text = "-"
        Me.val_OmegaY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_OmegaX
        '
        Me.val_OmegaX.ForeColor = System.Drawing.Color.Blue
        Me.val_OmegaX.Location = New System.Drawing.Point(294, 253)
        Me.val_OmegaX.Name = "val_OmegaX"
        Me.val_OmegaX.ReadOnly = True
        Me.val_OmegaX.Size = New System.Drawing.Size(91, 20)
        Me.val_OmegaX.TabIndex = 12
        Me.val_OmegaX.Text = "-"
        Me.val_OmegaX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_Ry
        '
        Me.val_Ry.ForeColor = System.Drawing.Color.Blue
        Me.val_Ry.Location = New System.Drawing.Point(384, 234)
        Me.val_Ry.Name = "val_Ry"
        Me.val_Ry.ReadOnly = True
        Me.val_Ry.Size = New System.Drawing.Size(90, 20)
        Me.val_Ry.TabIndex = 11
        Me.val_Ry.Text = "-"
        Me.val_Ry.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_Rx
        '
        Me.val_Rx.ForeColor = System.Drawing.Color.Blue
        Me.val_Rx.Location = New System.Drawing.Point(294, 234)
        Me.val_Rx.Name = "val_Rx"
        Me.val_Rx.ReadOnly = True
        Me.val_Rx.Size = New System.Drawing.Size(91, 20)
        Me.val_Rx.TabIndex = 10
        Me.val_Rx.Text = "-"
        Me.val_Rx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_FTypeY
        '
        Me.val_FTypeY.BackColor = System.Drawing.SystemColors.Info
        Me.val_FTypeY.ForeColor = System.Drawing.Color.Blue
        Me.val_FTypeY.Location = New System.Drawing.Point(384, 215)
        Me.val_FTypeY.Name = "val_FTypeY"
        Me.val_FTypeY.ReadOnly = True
        Me.val_FTypeY.Size = New System.Drawing.Size(90, 20)
        Me.val_FTypeY.TabIndex = 9
        Me.val_FTypeY.Text = "-"
        Me.val_FTypeY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_FTypeX
        '
        Me.val_FTypeX.BackColor = System.Drawing.SystemColors.Info
        Me.val_FTypeX.ForeColor = System.Drawing.Color.Blue
        Me.val_FTypeX.Location = New System.Drawing.Point(294, 215)
        Me.val_FTypeX.Name = "val_FTypeX"
        Me.val_FTypeX.ReadOnly = True
        Me.val_FTypeX.Size = New System.Drawing.Size(91, 20)
        Me.val_FTypeX.TabIndex = 8
        Me.val_FTypeX.Text = "-"
        Me.val_FTypeX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox64
        '
        Me.TextBox64.BackColor = System.Drawing.Color.White
        Me.TextBox64.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox64.Location = New System.Drawing.Point(6, 367)
        Me.TextBox64.Name = "TextBox64"
        Me.TextBox64.ReadOnly = True
        Me.TextBox64.Size = New System.Drawing.Size(468, 20)
        Me.TextBox64.TabIndex = 54
        Me.TextBox64.TabStop = False
        Me.TextBox64.Text = "     Member Design"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.val_ASCE_W)
        Me.TabPage2.Controls.Add(Me.TextBox19)
        Me.TabPage2.Controls.Add(Me.TextBox27)
        Me.TabPage2.Controls.Add(Me.TextBox83)
        Me.TabPage2.Controls.Add(Me.TextBox84)
        Me.TabPage2.Controls.Add(Me.TextBox85)
        Me.TabPage2.Controls.Add(Me.TextBox4)
        Me.TabPage2.Controls.Add(Me.val_KdX)
        Me.TabPage2.Controls.Add(Me.TextBox13)
        Me.TabPage2.Controls.Add(Me.TextBox16)
        Me.TabPage2.Controls.Add(Me.val_GustFactorX)
        Me.TabPage2.Controls.Add(Me.TextBox31)
        Me.TabPage2.Controls.Add(Me.TextBox32)
        Me.TabPage2.Controls.Add(Me.val_KztX)
        Me.TabPage2.Controls.Add(Me.TextBox66)
        Me.TabPage2.Controls.Add(Me.TextBox67)
        Me.TabPage2.Controls.Add(Me.val_WindSpeedX)
        Me.TabPage2.Controls.Add(Me.val_GroundFactorX)
        Me.TabPage2.Controls.Add(Me.TextBox70)
        Me.TabPage2.Controls.Add(Me.TextBox71)
        Me.TabPage2.Controls.Add(Me.TextBox74)
        Me.TabPage2.Controls.Add(Me.val_ExposureX)
        Me.TabPage2.Controls.Add(Me.TextBox77)
        Me.TabPage2.Controls.Add(Me.TextBox78)
        Me.TabPage2.Controls.Add(Me.TextBox79)
        Me.TabPage2.Controls.Add(Me.TextBox80)
        Me.TabPage2.Controls.Add(Me.TextBox81)
        Me.TabPage2.Controls.Add(Me.TextBox82)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(480, 427)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Wind"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'val_ASCE_W
        '
        Me.val_ASCE_W.BackColor = System.Drawing.SystemColors.Info
        Me.val_ASCE_W.ForeColor = System.Drawing.Color.Blue
        Me.val_ASCE_W.Location = New System.Drawing.Point(294, 62)
        Me.val_ASCE_W.Name = "val_ASCE_W"
        Me.val_ASCE_W.Size = New System.Drawing.Size(180, 20)
        Me.val_ASCE_W.TabIndex = 75
        Me.val_ASCE_W.Text = "-"
        Me.val_ASCE_W.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(45, 62)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ReadOnly = True
        Me.TextBox19.Size = New System.Drawing.Size(250, 20)
        Me.TextBox19.TabIndex = 77
        Me.TextBox19.TabStop = False
        Me.TextBox19.Text = "Standard="
        Me.TextBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox27
        '
        Me.TextBox27.Location = New System.Drawing.Point(6, 62)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.ReadOnly = True
        Me.TextBox27.Size = New System.Drawing.Size(40, 20)
        Me.TextBox27.TabIndex = 76
        Me.TextBox27.TabStop = False
        Me.TextBox27.Text = "1"
        Me.TextBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox83
        '
        Me.TextBox83.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox83.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox83.Location = New System.Drawing.Point(294, 6)
        Me.TextBox83.Name = "TextBox83"
        Me.TextBox83.ReadOnly = True
        Me.TextBox83.Size = New System.Drawing.Size(180, 20)
        Me.TextBox83.TabIndex = 74
        Me.TextBox83.TabStop = False
        Me.TextBox83.Text = "Data"
        Me.TextBox83.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox84
        '
        Me.TextBox84.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox84.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox84.Location = New System.Drawing.Point(45, 6)
        Me.TextBox84.Name = "TextBox84"
        Me.TextBox84.ReadOnly = True
        Me.TextBox84.Size = New System.Drawing.Size(250, 20)
        Me.TextBox84.TabIndex = 73
        Me.TextBox84.TabStop = False
        Me.TextBox84.Text = "Item"
        Me.TextBox84.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox85
        '
        Me.TextBox85.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox85.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox85.Location = New System.Drawing.Point(6, 6)
        Me.TextBox85.Name = "TextBox85"
        Me.TextBox85.ReadOnly = True
        Me.TextBox85.Size = New System.Drawing.Size(40, 20)
        Me.TextBox85.TabIndex = 72
        Me.TextBox85.TabStop = False
        Me.TextBox85.Text = "No."
        Me.TextBox85.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(45, 98)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(250, 20)
        Me.TextBox4.TabIndex = 63
        Me.TextBox4.TabStop = False
        Me.TextBox4.Text = "Exposure Type="
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'val_KdX
        '
        Me.val_KdX.ForeColor = System.Drawing.Color.Blue
        Me.val_KdX.Location = New System.Drawing.Point(294, 174)
        Me.val_KdX.Name = "val_KdX"
        Me.val_KdX.Size = New System.Drawing.Size(180, 20)
        Me.val_KdX.TabIndex = 11
        Me.val_KdX.Text = "-"
        Me.val_KdX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(45, 174)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ReadOnly = True
        Me.TextBox13.Size = New System.Drawing.Size(250, 20)
        Me.TextBox13.TabIndex = 71
        Me.TextBox13.TabStop = False
        Me.TextBox13.Text = "Directionality Factor, Kd="
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(6, 174)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(40, 20)
        Me.TextBox16.TabIndex = 70
        Me.TextBox16.TabStop = False
        Me.TextBox16.Text = "7"
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_GustFactorX
        '
        Me.val_GustFactorX.ForeColor = System.Drawing.Color.Blue
        Me.val_GustFactorX.Location = New System.Drawing.Point(294, 155)
        Me.val_GustFactorX.Name = "val_GustFactorX"
        Me.val_GustFactorX.Size = New System.Drawing.Size(180, 20)
        Me.val_GustFactorX.TabIndex = 9
        Me.val_GustFactorX.Text = "-"
        Me.val_GustFactorX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox31
        '
        Me.TextBox31.Location = New System.Drawing.Point(45, 155)
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.ReadOnly = True
        Me.TextBox31.Size = New System.Drawing.Size(250, 20)
        Me.TextBox31.TabIndex = 69
        Me.TextBox31.TabStop = False
        Me.TextBox31.Text = "Gust Factor="
        Me.TextBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox32
        '
        Me.TextBox32.Location = New System.Drawing.Point(6, 155)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.ReadOnly = True
        Me.TextBox32.Size = New System.Drawing.Size(40, 20)
        Me.TextBox32.TabIndex = 68
        Me.TextBox32.TabStop = False
        Me.TextBox32.Text = "6"
        Me.TextBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_KztX
        '
        Me.val_KztX.ForeColor = System.Drawing.Color.Blue
        Me.val_KztX.Location = New System.Drawing.Point(294, 136)
        Me.val_KztX.Name = "val_KztX"
        Me.val_KztX.Size = New System.Drawing.Size(180, 20)
        Me.val_KztX.TabIndex = 7
        Me.val_KztX.Text = "-"
        Me.val_KztX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox66
        '
        Me.TextBox66.Location = New System.Drawing.Point(45, 136)
        Me.TextBox66.Name = "TextBox66"
        Me.TextBox66.ReadOnly = True
        Me.TextBox66.Size = New System.Drawing.Size(250, 20)
        Me.TextBox66.TabIndex = 67
        Me.TextBox66.TabStop = False
        Me.TextBox66.Text = "Topographical Factor, Kzt="
        Me.TextBox66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox67
        '
        Me.TextBox67.Location = New System.Drawing.Point(6, 136)
        Me.TextBox67.Name = "TextBox67"
        Me.TextBox67.ReadOnly = True
        Me.TextBox67.Size = New System.Drawing.Size(40, 20)
        Me.TextBox67.TabIndex = 66
        Me.TextBox67.TabStop = False
        Me.TextBox67.Text = "5"
        Me.TextBox67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_WindSpeedX
        '
        Me.val_WindSpeedX.ForeColor = System.Drawing.Color.Blue
        Me.val_WindSpeedX.Location = New System.Drawing.Point(294, 79)
        Me.val_WindSpeedX.Name = "val_WindSpeedX"
        Me.val_WindSpeedX.Size = New System.Drawing.Size(180, 20)
        Me.val_WindSpeedX.TabIndex = 5
        Me.val_WindSpeedX.Text = "-"
        Me.val_WindSpeedX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_GroundFactorX
        '
        Me.val_GroundFactorX.ForeColor = System.Drawing.Color.Blue
        Me.val_GroundFactorX.Location = New System.Drawing.Point(294, 117)
        Me.val_GroundFactorX.Name = "val_GroundFactorX"
        Me.val_GroundFactorX.Size = New System.Drawing.Size(180, 20)
        Me.val_GroundFactorX.TabIndex = 5
        Me.val_GroundFactorX.Text = "-"
        Me.val_GroundFactorX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox70
        '
        Me.TextBox70.Location = New System.Drawing.Point(45, 117)
        Me.TextBox70.Name = "TextBox70"
        Me.TextBox70.ReadOnly = True
        Me.TextBox70.Size = New System.Drawing.Size(250, 20)
        Me.TextBox70.TabIndex = 65
        Me.TextBox70.TabStop = False
        Me.TextBox70.Text = "Ground Elevation Factor="
        Me.TextBox70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox71
        '
        Me.TextBox71.Location = New System.Drawing.Point(6, 117)
        Me.TextBox71.Name = "TextBox71"
        Me.TextBox71.ReadOnly = True
        Me.TextBox71.Size = New System.Drawing.Size(40, 20)
        Me.TextBox71.TabIndex = 64
        Me.TextBox71.TabStop = False
        Me.TextBox71.Text = "4"
        Me.TextBox71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox74
        '
        Me.TextBox74.Location = New System.Drawing.Point(6, 98)
        Me.TextBox74.Name = "TextBox74"
        Me.TextBox74.ReadOnly = True
        Me.TextBox74.Size = New System.Drawing.Size(40, 20)
        Me.TextBox74.TabIndex = 62
        Me.TextBox74.TabStop = False
        Me.TextBox74.Text = "3"
        Me.TextBox74.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'val_ExposureX
        '
        Me.val_ExposureX.BackColor = System.Drawing.SystemColors.Info
        Me.val_ExposureX.ForeColor = System.Drawing.Color.Blue
        Me.val_ExposureX.Location = New System.Drawing.Point(294, 98)
        Me.val_ExposureX.Name = "val_ExposureX"
        Me.val_ExposureX.ReadOnly = True
        Me.val_ExposureX.Size = New System.Drawing.Size(180, 20)
        Me.val_ExposureX.TabIndex = 1
        Me.val_ExposureX.Text = "-"
        Me.val_ExposureX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox77
        '
        Me.TextBox77.Location = New System.Drawing.Point(45, 79)
        Me.TextBox77.Name = "TextBox77"
        Me.TextBox77.ReadOnly = True
        Me.TextBox77.Size = New System.Drawing.Size(250, 20)
        Me.TextBox77.TabIndex = 61
        Me.TextBox77.TabStop = False
        Me.TextBox77.Text = "Wind Speed (mph)="
        Me.TextBox77.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox78
        '
        Me.TextBox78.Location = New System.Drawing.Point(6, 79)
        Me.TextBox78.Name = "TextBox78"
        Me.TextBox78.ReadOnly = True
        Me.TextBox78.Size = New System.Drawing.Size(40, 20)
        Me.TextBox78.TabIndex = 60
        Me.TextBox78.TabStop = False
        Me.TextBox78.Text = "2"
        Me.TextBox78.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox79
        '
        Me.TextBox79.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox79.Location = New System.Drawing.Point(384, 43)
        Me.TextBox79.Name = "TextBox79"
        Me.TextBox79.ReadOnly = True
        Me.TextBox79.Size = New System.Drawing.Size(90, 20)
        Me.TextBox79.TabIndex = 59
        Me.TextBox79.TabStop = False
        Me.TextBox79.Text = "Y-Dir"
        Me.TextBox79.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox80
        '
        Me.TextBox80.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox80.Location = New System.Drawing.Point(294, 43)
        Me.TextBox80.Name = "TextBox80"
        Me.TextBox80.ReadOnly = True
        Me.TextBox80.Size = New System.Drawing.Size(91, 20)
        Me.TextBox80.TabIndex = 56
        Me.TextBox80.TabStop = False
        Me.TextBox80.Text = "X-Dir"
        Me.TextBox80.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox81
        '
        Me.TextBox81.Location = New System.Drawing.Point(6, 43)
        Me.TextBox81.Name = "TextBox81"
        Me.TextBox81.ReadOnly = True
        Me.TextBox81.Size = New System.Drawing.Size(289, 20)
        Me.TextBox81.TabIndex = 53
        Me.TextBox81.TabStop = False
        Me.TextBox81.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox82
        '
        Me.TextBox82.BackColor = System.Drawing.Color.White
        Me.TextBox82.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox82.Location = New System.Drawing.Point(6, 24)
        Me.TextBox82.Name = "TextBox82"
        Me.TextBox82.ReadOnly = True
        Me.TextBox82.Size = New System.Drawing.Size(468, 20)
        Me.TextBox82.TabIndex = 44
        Me.TextBox82.TabStop = False
        Me.TextBox82.Text = "    Wind Coefficients"
        '
        'Column3
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column3.FillWeight = 15.0!
        Me.Column3.HeaderText = "Min"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column2
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column2.FillWeight = 15.0!
        Me.Column2.HeaderText = "Max"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'C2
        '
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Blue
        Me.C2.DefaultCellStyle = DataGridViewCellStyle3
        Me.C2.FillWeight = 30.0!
        Me.C2.HeaderText = "Data"
        Me.C2.Name = "C2"
        Me.C2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column1
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.DarkBlue
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.DarkBlue
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column1.FillWeight = 30.0!
        Me.Column1.HeaderText = "Item"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column25
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.DarkRed
        Me.Column25.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column25.FillWeight = 10.0!
        Me.Column25.HeaderText = "No."
        Me.Column25.Name = "Column25"
        Me.Column25.ReadOnly = True
        Me.Column25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cCB
        '
        Me.cCB.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ASCE7_16, Me.ASCE7_10, Me.ASCE7_05, Me.ASCE7_02})
        Me.cCB.Name = "cCB"
        Me.cCB.Size = New System.Drawing.Size(129, 92)
        '
        'ASCE7_16
        '
        Me.ASCE7_16.Name = "ASCE7_16"
        Me.ASCE7_16.Size = New System.Drawing.Size(128, 22)
        Me.ASCE7_16.Text = "ASCE 7-16"
        '
        'ASCE7_10
        '
        Me.ASCE7_10.Name = "ASCE7_10"
        Me.ASCE7_10.Size = New System.Drawing.Size(128, 22)
        Me.ASCE7_10.Text = "ASCE 7-10"
        '
        'ASCE7_05
        '
        Me.ASCE7_05.Name = "ASCE7_05"
        Me.ASCE7_05.Size = New System.Drawing.Size(128, 22)
        Me.ASCE7_05.Text = "ASCE 7-05"
        '
        'ASCE7_02
        '
        Me.ASCE7_02.Name = "ASCE7_02"
        Me.ASCE7_02.Size = New System.Drawing.Size(128, 22)
        Me.ASCE7_02.Text = "ASCE 7-02"
        '
        'lbtabDetails
        '
        Me.lbtabDetails.AutoSize = True
        Me.lbtabDetails.ForeColor = System.Drawing.Color.DarkRed
        Me.lbtabDetails.Location = New System.Drawing.Point(20, 487)
        Me.lbtabDetails.Name = "lbtabDetails"
        Me.lbtabDetails.Size = New System.Drawing.Size(423, 13)
        Me.lbtabDetails.TabIndex = 0
        Me.lbtabDetails.Text = "Warning: Cannot set ""Design Frame Type"" using API. Please make it through ETABS U" &
    "I"
        Me.lbtabDetails.Visible = False
        '
        'frmDesignParams
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(516, 551)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.lbtabDetails)
        Me.Controls.Add(Me.btnCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDesignParams"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Show/Modify Design Parameters"
        Me.Panel1.ResumeLayout(False)
        Me.grName.ResumeLayout(False)
        Me.tabInfo.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.cCB.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents grName As GroupBox
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents C2 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column25 As DataGridViewTextBoxColumn
    Friend WithEvents Panel1 As Panel
    Friend WithEvents tabInfo As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TextBox20 As TextBox
    Friend WithEvents TextBox21 As TextBox
    Friend WithEvents val_Sds As TextBox
    Friend WithEvents TextBox17 As TextBox
    Friend WithEvents TextBox18 As TextBox
    Friend WithEvents TextBox14 As TextBox
    Friend WithEvents TextBox15 As TextBox
    Friend WithEvents val_TL As TextBox
    Friend WithEvents TextBox11 As TextBox
    Friend WithEvents TextBox12 As TextBox
    Friend WithEvents val_S1 As TextBox
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents val_Ss As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents TextBox24 As TextBox
    Friend WithEvents TextBox23 As TextBox
    Friend WithEvents TextBox22 As TextBox
    Friend WithEvents TextBox25 As TextBox
    Friend WithEvents TextBox29 As TextBox
    Friend WithEvents TextBox28 As TextBox
    Friend WithEvents TextBox26 As TextBox
    Friend WithEvents TextBox30 As TextBox
    Friend WithEvents val_FTypeY As TextBox
    Friend WithEvents val_FTypeX As TextBox
    Friend WithEvents TextBox33 As TextBox
    Friend WithEvents TextBox34 As TextBox
    Friend WithEvents val_f2X As TextBox
    Friend WithEvents TextBox60 As TextBox
    Friend WithEvents TextBox61 As TextBox
    Friend WithEvents val_f1X As TextBox
    Friend WithEvents TextBox56 As TextBox
    Friend WithEvents TextBox57 As TextBox
    Friend WithEvents val_RhoY As TextBox
    Friend WithEvents val_RhoX As TextBox
    Friend WithEvents TextBox52 As TextBox
    Friend WithEvents TextBox53 As TextBox
    Friend WithEvents val_Iy As TextBox
    Friend WithEvents val_Ix As TextBox
    Friend WithEvents TextBox48 As TextBox
    Friend WithEvents TextBox49 As TextBox
    Friend WithEvents val_CdY As TextBox
    Friend WithEvents val_CdX As TextBox
    Friend WithEvents TextBox44 As TextBox
    Friend WithEvents TextBox45 As TextBox
    Friend WithEvents val_OmegaY As TextBox
    Friend WithEvents val_OmegaX As TextBox
    Friend WithEvents TextBox40 As TextBox
    Friend WithEvents TextBox41 As TextBox
    Friend WithEvents val_Ry As TextBox
    Friend WithEvents val_Rx As TextBox
    Friend WithEvents TextBox36 As TextBox
    Friend WithEvents TextBox37 As TextBox
    Friend WithEvents TextBox63 As TextBox
    Friend WithEvents TextBox64 As TextBox
    Friend WithEvents TextBox65 As TextBox
    Friend WithEvents cCB As ContextMenuStrip
    Friend WithEvents ASCE7_16 As ToolStripMenuItem
    Friend WithEvents ASCE7_10 As ToolStripMenuItem
    Friend WithEvents ASCE7_05 As ToolStripMenuItem
    Friend WithEvents ASCE7_02 As ToolStripMenuItem
    Friend WithEvents val_DesignCode As TextBox
    Friend WithEvents val_ASCE As TextBox
    Friend WithEvents val_SdsUser As TextBox
    Friend WithEvents val_SiteClass As TextBox
    Friend WithEvents TextBox83 As TextBox
    Friend WithEvents TextBox84 As TextBox
    Friend WithEvents TextBox85 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents val_KdX As TextBox
    Friend WithEvents TextBox13 As TextBox
    Friend WithEvents TextBox16 As TextBox
    Friend WithEvents val_GustFactorX As TextBox
    Friend WithEvents TextBox31 As TextBox
    Friend WithEvents TextBox32 As TextBox
    Friend WithEvents val_KztX As TextBox
    Friend WithEvents TextBox66 As TextBox
    Friend WithEvents TextBox67 As TextBox
    Friend WithEvents val_GroundFactorX As TextBox
    Friend WithEvents TextBox70 As TextBox
    Friend WithEvents TextBox71 As TextBox
    Friend WithEvents TextBox74 As TextBox
    Friend WithEvents TextBox77 As TextBox
    Friend WithEvents TextBox78 As TextBox
    Friend WithEvents TextBox79 As TextBox
    Friend WithEvents TextBox80 As TextBox
    Friend WithEvents TextBox81 As TextBox
    Friend WithEvents TextBox82 As TextBox
    Friend WithEvents val_WindSpeedX As TextBox
    Friend WithEvents val_ExposureX As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents val_DFType As TextBox
    Friend WithEvents val_ASCE_W As TextBox
    Friend WithEvents TextBox19 As TextBox
    Friend WithEvents TextBox27 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents lbtabDetails As Label
End Class
