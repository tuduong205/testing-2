Imports CSiAPIv1
Imports SAP2000v1

'<ComClass(cPlugin.ClassId, cPlugin.InterfaceId, cPlugin.EventsId)>
Public Class cPlugin

#Region "COM GUIDs"
    Public Const ClassId As String = "9503BE48-A66F-4A7E-81E0-B35D85DB8A22"
    Public Const InterfaceId As String = "9bee4c2e-cf7a-4da2-9352-8513e5ed3124"
    Public Const EventsId As String = "969057fd-358c-4506-88fc-72977bdabd1a"
#End Region
    Public Sub New()
        MyBase.New()
    End Sub
    Private Shared modality As String = "Non-Modal"
    Public Function Info(ByRef Text As String) As Integer

        Try
            Text = "Yield-Link� Connection SAP2000v22"
        Catch ex As Exception
        End Try
        Return 0
    End Function
    Public Sub Main(ByRef SapModel As cSapModel, ByRef ISapPlugin As cPluginCallback)
        Dim SE As New clsSAP_Etabs
        Dim iError As Integer
        SE.setParentPluginObject(Me)
        SE.setSapModel(SapModel, ISapPlugin)
        Dim aForm As New YieldLinkConnectionPlugin(SE, g_isEtab)
        Try
            If StrComp(modality, "Non-Modal", CompareMethod.Text) = 0 Then
                aForm.Show()
            Else
            aForm.ShowDialog()
            End If
            '
            ISapPlugin.Finish(iError)
            '
        Catch ex As Exception
            MsgBox("The following error terminated the Plugin:" & vbCrLf & ex.Message)
            ' call Finish to inform CSI program that the PlugIn has terminated
            Try
                ISapPlugin.Finish(1)
            Catch ex1 As Exception
            End Try
        End Try
        Return
    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class


