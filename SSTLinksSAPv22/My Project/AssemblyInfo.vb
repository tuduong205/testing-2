﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Yield-Link® Connection")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Simpson Strong-Tie")> 
<Assembly: AssemblyProduct("Yield-Link® Connection")> 
<Assembly: AssemblyCopyright("Copyright © Simpson Strong-Tie")> 
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(True)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("7bbc9645-e847-40af-b7e1-5600f4f82a99")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.0")>
<Assembly: AssemblyFileVersion("3.0.0.0")>
