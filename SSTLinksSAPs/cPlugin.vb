Imports SAP2000v1
Imports SAP2000v20

<ComClass(cPlugin.ClassId, cPlugin.InterfaceId, cPlugin.EventsId)> _
Public Class cPlugin
#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "9503BE48-A66F-4A7E-81E0-B35D85DB8A21"
    Public Const InterfaceId As String = "9bee4c2e-cf7a-4da2-9352-8513e5ed3124"
    Public Const EventsId As String = "969057fd-358c-4506-88fc-72977bdabd1a"

#End Region

    ' A creatable COM class must have a Public Sub New() 
    ' with no parameters, otherwise, the class will not be 
    ' registered in the COM registry and cannot be created 
    ' via CreateObject.

    Public Sub New()
        MyBase.New()
    End Sub
    Private Shared modality As String = "Non-Modal"
    ''Friend Shared VersionString As String = "Version " _
    ''                                        & System.Reflection.Assembly.GetExecutingAssembly.GetName().Version.ToString _
    ''                                        & " , .NET Only , Compiled as " _
    ''                                        & System.Reflection.Assembly.GetExecutingAssembly.GetName().ProcessorArchitecture.ToString _
    ''                                        & " , " & modality
    Public Function Info(ByRef Text As String) As Integer

        Try
            Text = "Yield-Link� Connection SAP2000"
        Catch ex As Exception
        End Try

        Return 0

    End Function

    Public Sub Main(ByRef SapModel As cSapModel, ByRef ISapPlugin As cPluginCallback)

        Dim SE As New clsSAP_Etabs
        SE.setParentPluginObject(Me)
        SE.setSapModel(SapModel, ISapPlugin)
        Dim aForm As New YieldLinkConnectionPlugin(SE, g_isEtab)
        Try
            If StrComp(modality, "Non-Modal", CompareMethod.Text) = 0 Then
                ' Non-modal form, allows graphics refresh operations in CSI program, 
                ' but Main will return to CSI program before the form is closed.
                aForm.Show()
            Else
                ' Modal form, will not return to CSI program until form is closed,
                ' but may cause errors when refreshing the view.
                aForm.ShowDialog()
            End If


            ' It is very important to call ISapPlugin.Finish(iError) when form closes, !!!
            ' otherwise, CSI program will wait and be hung !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            ' this must be done inside the closing event for the form itself, not here !!!

            ' if you simply have algorithmic code here without any forms, 
            ' then call ISapPlugin.Finish(iError) here before returning to CSI program

            ' if your code will run for more than a few seconds, you should exercise
            ' the Windows messaging loop to keep the program responsive. You may 
            ' also want to provide an opportunity for the user to cancel operations.
            ISapPlugin.Finish(1)
        Catch ex As Exception
            MsgBox("The following error terminated the Plugin:" & vbCrLf & ex.Message)

            ' call Finish to inform CSI program that the PlugIn has terminated
            Try
                ISapPlugin.Finish(1)
            Catch ex1 As Exception
            End Try
        End Try

        Return

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class


